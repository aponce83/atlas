# Documentación para instalación inicial del Atlas de la Lectura #

En el siguiente documento encontrará la información necesaria para instalar, configurar e implementar el Atlas de la Lectura.

## ¿Qué incluye el Atlas de la Lectura? ##

* Template con del Atlas de la Lectura
* Documentos de los programas del Atlas de la Lectura
* Imágenes y maquetas de los botones en CSS

## Pasos iniciales ##

* Creación de las bases de datos necesarias
* Configuración del servidor
* Instalación de la aplicación

### Creación de las bases de datos necesarias ###

El Catálogo Público de LibrosMéxico requiere una base de datos MySQL:

* **librosmexico_atlas** Base de datos principal del catálogo

Para descargar la estructura de ambas bases de datos puede hacerlo a través del siguiente enlace: [http://pro.librosmexico.mx/documentos/librosmexicoatlasDB.zip](http://pro.librosmexico.mx/documentos/librosmexicoatlasDB.zip)

### Configuración del servidor ###

La configuración default para el servidor Apache es la siguiente:

```
El Atlas de la Lectura está desarrollado para operar en un ambiente Web seguro (https) por lo que recomendamos utilizar certificados válidos para este fin en el servidor que se implementará el sistema.

La configuración default para el servidor Apache (SSL) es la siguiente:

```
        DocumentRoot "/home/atlas.mx"
        ServerName atlas.mx
        <Directory "/home/atlas.mx">
                AllowOverride All
                Order allow,deny
                Allow from all
                RewriteEngine On
                Options +FollowSymlinks
        </Directory>
        SSLEngine on
        SSLCipherSuite !EDH:!ADH:!DSS:!RC4:HIGH:+3DES
        SSLProtocol all -SSLv2 -SSLv3
        SSLCertificateFile /etc/httpd/ssl/XxXxXxXxXxXxXxX.crt
        SSLCertificateKeyFile /etc/httpd/ssl/XxXxXxXxXxXxXxX.key
        ErrorLog /var/log/httpd/atlas.mx.log
        LogLevel warn
```

### Configuración de la aplicación ###

La aplicación utiliza el Framwork Kohana como base. Por lo tanto contaremos con las configuraciones personalizadas según el ambiente en el que deseamos ejecutar la aplicación

El archivo de configuración se puede localizar en **/application/config/database.php** y tiene la siguiente estructura:

```
if (Kohana::$environment == Kohana::PRODUCTION) {
        $db['default'] = array
                (
                        'type'       => 'mysql',
                        'connection' => array(
                                'hostname'   => 'localhost',
                                'database'   => 'XxXxXxXxXxXxXxX',
                                'username'   => 'XxXxXxXxXxXxXxX',
                                'password'   => 'XxXxXxXxXxXxXxX',
                                'persistent' => FALSE,
                        ),
                        'table_prefix' => '',
                        'charset'      => 'utf8',
                        'caching'      => FALSE,
                        'profiling'    => FALSE,
                );
}
elseif (Kohana::$environment == Kohana::STAGING)
{
        $db['default'] =  array
                (
                        'type'       => 'mysql',
                        'connection' => array(
                                'hostname'   => 'localhost',
                                'database'   => 'XxXxXxXxXxXxXxX',
                                'username'   => 'XxXxXxXxXxXxXxX',
                                'password'   => 'XxXxXxXxXxXxXxX',
                                'persistent' => FALSE,
                        ),
                        'table_prefix' => '',
                        'charset'      => 'utf8',
                        'caching'      => FALSE,
                        'profiling'    => TRUE,
                );
}
elseif (Kohana::$environment == Kohana::DEVELOPMENT)
{
        $db['default'] =  array
                (
                        'type'       => 'mysql',
                        'connection' => array(
                                'hostname'   => 'localhost',
                                'database'   => 'XxXxXxXxXxXxXxX',
                                'username'   => 'XxXxXxXxXxXxXxX',
                                'password'   => 'XxXxXxXxXxXxXxX',
                                'persistent' => FALSE,
                        ),
                        'table_prefix' => '',
                        'charset'      => 'utf8',
                        'caching'      => FALSE,
                        'profiling'    => TRUE,
                );
}
```

Una vez configurada la aplicación e instalada la estructura de la base de datos podrá acceder al sistema desde la URL root donde se instaló la aplicación.