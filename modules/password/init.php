<?php defined('SYSPATH') or die('No direct script access.');
/**
 * This is a module to support the new password functions in PHP >= 5.5
 * URL: https://github.com/ircmaxell/password_compat
 */
require Kohana::find_file('lib', 'password');

