<?php defined('SYSPATH') OR die('No direct access allowed.');

class LibrosMexicoResponse {
	private $data;
	private $valid;
	public $header;
	public $body;
	function __construct($data) {
		if (!is_array($data)) {
			$data = array($data);
		}
		
		$header = $data[0]["encabezado"];
		$message = $header[0]["mensaje"];
		$code = $header[1]["codigo"];
		$this->header = array("message" => $message, "code" => $code, "registros" => 0);
		if (array_key_exists(2, $header)) {
			if (array_key_exists("registros", $header[2])) {
				$this->header["registros"] = $header[2]["registros"];
			}
		}
		$this->valid = ($this->header["code"] == 200);
		
		$this->body = $data[0]["cuerpo"];
	}

	/**
	 * Returns true if the request is valid, if it's not valid then it LOGS the 
	 * error using Kohana Log
	 * @return boolean
	 */
	public function isValid() {
		return $this->valid;
	}
}