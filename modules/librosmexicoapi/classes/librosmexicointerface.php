<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Class used for comunication with the Libros Mexico API
 */
class LibrosMexicoInterface {
	/**
	 * Connection configuration
	 * @var array
	 */
	protected $config;

	/**
	 * Object Constructor
	 */
	function __construct() {
		$pro_api = Kohana::$config->load('librosmexicoapi')->as_array();
		$local_api = Kohana::$config->load('librosmexicolocalapi')->as_array();
		$this->config = array_merge($pro_api, $local_api);
	}

	/**
	 * Makes a raw request in the db
	 * @param  string Name of the method in the API (e.g.: buscaLibros)
	 * @param  array Params that are going to be sended in the request, this params
	 * are sended through POST request
	 * @param  boolean Verbose using  Kohana LOG api
	 * @param  boolean $cache cache the response
	 * @param  int $expiration_seconds expire time in seconds of the cache (if set)
	 * @return array Raw response of the request
	 */
	public function makeRawRequest($endpoint, $params = array(), $verbose = false, $cache = true, $expiration_seconds = 3600) {
		if ($cache) {
			$rediska_key = LM::getRedisKey("s_{$endpoint}", $params);
			$redis = Rediska_Manager::get();
			if ($redis->exists($rediska_key)) {
				$result = $redis->get($rediska_key);
				$result = json_decode($result, true);
				return $result;
			}
		}

		$auth = $this->getRequestAuthParams();
		$params = array_merge($auth, $params);

		$strParams = LM::getParamsAsString($params);

		$endpoint = "{$this->config['url']}{$endpoint}";

		if (Kohana::$environment == Kohana::DEVELOPMENT) {
			Log::instance()->add(Log::INFO, "LibrosMexicoInterface:: Preparing request");
			Log::instance()->add(Log::INFO, "LibrosMexicoInterface:: URL: {$endpoint}");
			Log::instance()->add(Log::INFO, "LibrosMexicoInterface:: Params: {$strParams}");
			LM::log("LibrosMexicoInterface:: Preparing request");
			LM::log("LibrosMexicoInterface:: URL: {$endpoint}");
			LM::log("LibrosMexicoInterface:: Params: {$strParams}");
		}

		$lmapi_log = array();
		$start_time = date('Y-m-d H:i:s');
		$lmt = new LM;
		$lmt->get_time_elapsed();

		$conn = curl_init();
		curl_setopt($conn, CURLOPT_URL, $endpoint);
		curl_setopt($conn, CURLOPT_POST, 1);
		curl_setopt($conn, CURLOPT_POSTFIELDS, $strParams);
		curl_setopt($conn, CURLOPT_RETURNTRANSFER, 1);
		$raw = curl_exec($conn);
		$end_time = date('Y-m-d H:i:s');
		curl_close($conn);

		if (Kohana::$environment == Kohana::STAGING ||
			Kohana::$environment == Kohana::DEVELOPMENT)
		{
			$lmapi_log['endpoint'] = $endpoint;
			$lmapi_log['data'] = $strParams;
			$lmapi_log['data_json'] = json_encode($params, true);
			$lmapi_log['userfront_id'] = null;
			$user = Session::instance()->get('user_front');
			if ($user != null) {
				$lmapi_log['userfront_id'] = isset($user['id']) ? $user['id'] : null;
			}
			$lmapi_log['start_time'] = $start_time;
			$lmapi_log['end_time'] = $end_time;
			$lmapi_log['elapsed_time'] = $lmt->get_time_elapsed();
			$lmapi_log['result_json'] = $raw;

			Model::factory('lmapi')->insert($lmapi_log);
		}

		if ($cache) {
			$key = new Rediska_Key($rediska_key);
			$key->setAndExpire($raw, $expiration_seconds);
			$redis->quit();
		}

		$res = json_decode($raw, true);

		if (Kohana::$environment == Kohana::DEVELOPMENT) {
			Log::instance()->add(Log::INFO, "LibrosMexicoInterface:: Result of {$endpoint} request:");
			Log::instance()->add(Log::INFO, "{$raw}");
			LM::log("LibrosMexicoInterface:: Result of {$endpoint} request:");
			LM::log($raw);
		}

		return $res;
	}

	/**
	 * Makes a request in the db
	 * @param  string Name of the method in the API (e.g.: buscaLibros)
	 * @param  array Params that are going to be sended in the request, this params
	 * are sended through POST request
	 * @param  boolean Verbose using  Kohana LOG api
	 * @param  boolean $cache cache the response
	 * @param  int $expiration_seconds expire time in seconds of the cache (if set)
	 * @return LibrosMexicoResponse Response of the request
	 */
	public function makeRequest($endpoint, $params = array(), $verbose = false, $cache = true, $expiration_seconds = 3600) {
		unset($params['todos']);

		$origin = array('origen'=>'L|E');
		$params = array_merge($origin,$params);

		$data = $this->makeRawRequest($endpoint, $params, $verbose, $cache, $expiration_seconds);
		$res = new LibrosMexicoResponse($data);
		return $res;
	}

	/**
	 * Makes a raw request to the atlas db
	 * @param  string Name of the method in the API (e.g.: buscaLibros)
	 * @param  array Params that are going to be sended in the request, this params
	 * are sended through POST request
	 * @param  boolean Verbose using  Kohana LOG api
	 * @param  boolean $cache cache the response
	 * @param  int $expiration_seconds expire time in seconds of the cache (if set)
	 * @return array Raw response of the request
	 */
	public function makeRawLocalRequest($endpoint,
		$params = array(),
		$verbose = false,
		$cache = true,
		$expiration_seconds = 3600)
	{
		$config_url = 'https://127.0.0.1/api/v1/';
		$lm_own_api_id = $this->config['lm_local_api_id'];
		$lm_own_api_key = $this->config['lm_local_api_key'];
		$current_date = date('YmdHis');

		$lm_own_api_signature = md5("{$current_date}{$lm_own_api_id}{$lm_own_api_key}");

		$auth = array(
			"api_id" => $lm_own_api_id,
			"api_signature" => $lm_own_api_signature,
			"current_date" => $current_date
		);
		$params = array_merge($auth, $params);

		$strParams = LM::getParamsAsString($params);
		$endpoint = "{$config_url}{$endpoint}";

		$conn = curl_init();
		curl_setopt($conn, CURLOPT_URL, $endpoint);
		curl_setopt($conn, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($conn, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($conn, CURLOPT_POST, true);
		curl_setopt($conn, CURLOPT_POSTFIELDS, $strParams);
		curl_setopt($conn, CURLOPT_RETURNTRANSFER, true);
		$raw = curl_exec($conn);
		$end_time = date('Y-m-d H:i:s');

		curl_close($conn);
		$res = json_decode($raw, true);

		return $res;
	}

	/**
	 * Returns an array with the params used for the authentication in the API
	 * @return array
	 */
	protected function getRequestAuthParams() {
		$currentDate = date("YmdHis");
		$signature =  md5("{$currentDate}{$this->config['integrador']}{$this->config['llave']}");
		$params = array(
			"fecha" => $currentDate,
			"integrador" => $this->config["integrador"],
			"firma" => $signature
			);
		return $params;
	}
}

