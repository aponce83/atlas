<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @package    Kohana
 * @category   Exceptions
 * @author     Kohana Team
 * @copyright  (c) 2009-2011 Kohana Team
 * @license    http://kohanaframework.org/license
 */
class View_Exception extends Kohana_View_Exception {
	public function __construct(){
		// No se envía nada en el constructor y se deja que el sistema
    // regrese su excepción
	}
}