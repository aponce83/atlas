<?php
/**
 * Documentación de las rutas:
 * Todas las rutas del administrador están en routes-admin.php y tienen como
 * prefijo de la ruta, "admin".
 *
 * Todas las rutas de Ajax deben de estar en el controlador de la clase asociada
 * a la operación que se está ejecutando, por ejemplo si queremos obtener la
 * información de un libro a través de Ajax, la acción tiene que estar en book
 * (controller) y no en Ajax, esto con el objetivo de tener las operaciones
 * en el lugar que corresponden, de otra manera Ajax se convierte en un
 * controlador de un montón de operaciones no asociadas a ella.
 *
 * Todas las rutas de ajax debe de tener como dirección:
 * ajax/<controlador>/<función>
 *
 * Por ejemplo:
 * ajax/book/get/<book_id>
 *
 * Para la creación del nombre de la ruta se va a ocupar la misma ruta solo que
 * en vez de '/' se van a poner guiones bajo, por ejemplo:
 * Ruta: ajax/book/get/<book_id>
 * Nombre: ajax_book_get
 *
 * Para las acciones con ajax se va a llamar así la acción:
 * ajax_<función>
 *
 * Por ejemplo: ajax_get
 *
 * Un ejemplo de todo lo descrito anteriormente:
 * Route::set('ajax_book_get', 'ajax/book/get/<book_id>', array())
 *  ->defaults(array(
 *		'directory'  => 'frontend',
 *		'controller' => 'book',
 *		'action'     => 'ajax_get',
 *	));
 *
 */

Route::set('ajax-save-data', 'ajax-save-data')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'save_data'
	));

Route::set('homepage', '(<lang>/<section>)', array('lang' => '(esp|eng)', 'section' => 'inicio'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'homepage',
		'action'     => 'alternative', //index
	));

/**
 * Prueba del home
 */
Route::set('homepage_alternative', 'homepage/alternative', array())
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'homepage',
		'action'     => 'alternative',
	));

/**
 * Prueba de las trivias por ajax en el home
 */
Route::set('ajax_homepage_trivias', 'ajax/homepage/trivias', array())
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'homepage',
		'action'     => 'ajax_trivias',
	));

// Ajax para obtener el contenido de los libros
Route::set('ajax_homepage_new_releases', 'ajax/homepage/new_releases', array())
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'homepage',
		'action' => 'ajax_new_releases'
		));

// Ajax para obtener el contenido de los libros
Route::set('ajax_homepage_highlights', 'ajax/homepage/highlights', array())
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'homepage',
		'action' => 'ajax_hightlights'
		));

// Ajax para obtener los libros recomendados
Route::set('ajax_homepage_recommended', 'ajax/homepage/recommended', array())
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'ajax',
		'action' => 'ajax_recommended'
		));

/**
 * Listado de los libros destacados
 * librosmexico.mx/contenido/destacados
 */
Route::set('renowned', '(<section>/<display>)', array('section' => 'contenido','display'=>'[a-zA-Z0-9\-=]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'homepage',
		'action'     => 'contenido',
	));

Route::set('error_404', '(error/<codigo>)', array('codigo' => '[a-zA-Z0-9\-=]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'error',
		'action'     => 'error',
	));

Route::set('verify-pass', 'verify-pass')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'validate_current_pass',
	));
Route::set('delete-acount', 'delete-acount')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'delete_acount',
	));

Route::set('verify-email-register', 'verify-email-register')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'verify_email_register',
	));

Route::set('verify-handle-register', 'verify-handle-register')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'verify_handle_register',
	));

//Stream routes

//--------------> Mis amigos
Route::set('stream_my_friends', '(<section>)', array('section' => 'mis-amigos'))
		->defaults(array(
			'directory'  => 'frontend',
			'controller' => 'friend',
			'action'     => 'my_friends',
		));

/**
 * Mi Perfil (mi-actividad)
 */
Route::set('stream_my_circle', '(<section>)', array('section' => 'perfil-publico|mi-actividad'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'stream',
		'action'     => 'my_circle',
	));

//--------------> Circulo_Mis amigos
Route::set('my_friends_circle', '(<section>/<friend_id>/<detail>)', array('section' => 'mis-amigos', 'friend_id'=>'[a-zA-Z0-9\-=]+', 'detail'=>'circulo|actividad'))
		->defaults(array(
			'directory'  => 'frontend',
			'controller' => 'stream',
			'action'     => 'my_friends_circle',
		));

/**
 * Vista a detalle de un perfil
 */
Route::set('reader', '(<section>/<friend_id>/<detail>)', array('section' => 'lector', 'friend_id'=>'[a-zA-Z0-9\-=]+', 'detail'=>'circulo|actividad'))
		->defaults(array(
			'directory'  => 'frontend',
			'controller' => 'stream',
			'action'     => 'reader',
		));

/**
 * Vista a detalle de un perfil por HANDLE
 */
Route::set('reader_handle', '(<section>/<handle>)', array('section' => 'u', 'handle'=>'[a-zA-Z0-9\-=]+'))
		->defaults(array(
			'directory'  => 'frontend',
			'controller' => 'stream',
			'action'     => 'reader',
		));

/**
 * Vista de una actividad
 */
Route::set('activity', '(<section>/<activity_id>)', array('section' => 'actividad', 'friend_id'=>'[a-zA-Z0-9\-=]+'))
		->defaults(array(
			'directory'  => 'frontend',
			'controller' => 'stream',
			'action'     => 'activity',
		));

/**
 * Mi muro (actualizaciones)
 */
Route::set('updates', 'actualizaciones')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'stream',
		'action'     => 'updates',
	));

//API stream routes
Route::set('social_api', '(api/social/<action>)')
		->defaults(array(
			'directory'  => 'api',
			'controller' => 'social',
			'action'     => 'index',
		));

Route::set('content_api', '(api/content/<action>)')
		->defaults(array(
			'directory'  => 'api',
			'controller' => 'content',
			'action'     => 'index',
		));

Route::set('atlas_api', '(api/atlas/<action>)')
		->defaults(array(
			'directory'  => 'api',
			'controller' => 'atlas',
			'action'     => 'index',
		));

	//ajax

Route::set('ajax', '(user_in_book)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'user_in_book',
	));
	//ajax-get book data
Route::set('ajax_get_book_data', '(get_book_data)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'get_book_data',
	));
//ajax - check if has file
Route::set('ajax_file_download', '(has_file_to_download)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'has_file_to_download',
	));

//ajax - Save book summary
Route::set('ajax_save_book_summary', '(save_book_summary)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'save_book_summary',
	));

//ajax-send-email
Route::set('ajax-send-email', 'send_email')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'send_email',
	));

//ajax-send-email
Route::set('ajax-report-data', 'ajax-report-data')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'report_data',
	));

//userfront_login
Route::set('userfront_login', 'login')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'userfront',
		'action'     => 'login',
	));

//userfront_login ajax
Route::set('register-ajax', 'register-ajax')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'register',
	));

//userfront_user_login
Route::set('userfront_view_login', '<section>', array('section' => 'inicio-sesion'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'userfront',
		'action'     => 'view_login',
	));

//userfront_user_recover
Route::set('userfront_view_recover', '<section>', array('section' => 'recuperar-password'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'userfront',
		'action'     => 'view_recover',
	));

//userfront_fb_login
Route::set('register-fb-ajax', '(register-fb-ajax)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'register_fb',
	));
//userfront_fb_login
Route::set('login-ajax', '(login-ajax)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'login_ajax',
	));
	Route::set('login-fb-ajax', '(login-fb-ajax)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'login_fb_ajax',
	));
Route::set('verify-fb-ajax', '(verify-fb-ajax)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'verify_fb_account',
	));
	//userfront_rec_password
Route::set('userfront_rec_password', '(recover_password)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'userfront',
		'action'     => 'recover_password',
	));
//userfront_logout
Route::set('userfront_logout', '(logout)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'userfront',
		'action'     => 'logout',
	));

//userfront_register
Route::set('userfront_register', '(<register>)',array('register'=> 'registro_usuario|registro-usuario'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'userfront',
		'action'     => 'register',
	));
//save-user
Route::set('save-user', 'userfront/save')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'userfront',
		'action'     => 'save',
	));
Route::set('userfront_welcome', '<section>', array('section' => 'user_welcome'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'userfront',
		'action'     => 'view_welcome',
	));
//userfront_profile
Route::set('userfront_profile', '<section>', array('section' => 'mi-perfil'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'userfront',
		'action'     => 'profile',
	));

//userfront_activate
Route::set('userfront_activate', '(activar_cuenta/<data>)', array('data' => '[a-zA-Z0-9\-=]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'userfront',
		'action'     => 'activate',
	));

//========================== DETALLE DEL LIBRO =================================
/**
 * Detalle del libro
 * librosmexico.mx/libros/{id}
 */
Route::set('book_detail', '(libros/<detail>)', array('detail' => '[a-zA-Z0-9\-]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'book',
		'action'     => 'detail',
	));

/**
 * Tracking de libro, funciona a través de ajax
 */
Route::set('ajax-book-tracking', 'libros/<detail>/seguimiento')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'book',
		'action' => 'ajax_tracking'
	));

/**
 * Encuentra donde comprar los libros, funciona a través de ajax
 */
Route::set('ajax-book-where-to-buy', 'libros/<detail>/donde_comprar')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'book',
		'action'     => 'ajax_where_to_buy',
	));

/**
 * Encuentra los libros relacionados, funciona a través de ajax
 */
Route::set('ajax-book-related', 'libros/<detail>/relacionados')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'book',
		'action'     => 'ajax_related_books',
	));

/**
 * Crea la discusión en el detalle del libro, funciona a través de ajax
 */
Route::set('ajax-create-discuss', 'ajax-create-discuss')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'create_discuss',
	));

/**
 * Crea la discusión por defecto, funciona a través de ajax
 */
Route::set('ajax-create-discuss-default', 'ajax-create-discuss-default')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'create_discuss_default',
	));

/**
 * Obtiene la información de una discusión, funciona a través de ajax
 */
Route::set('ajax-retrieve-discuss', 'ajax-retrieve-discuss')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'retrieve_discuss',
	));

/**
 * Obtiene la información de un comentario, funciona a través de ajax
 */
Route::set('ajax-retrieve-comment', 'ajax-retrieve-comment')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'retrieve_comment',
	));

/**
 * Obtiene la información de una respuesta a un comentario,
 * funciona a través de ajax
 */
Route::set('ajax-retrieve-response', 'ajax-retrieve-response')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'retrieve_response',
	));

/**
 * Se utiliza para darle like a una discusión, funciona a través de ajax
 */
Route::set('ajax-like-discuss', 'ajax-like-discuss')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'like_discuss',
	));

/**
 * Se utiliza para quitarle el like a una discusión, funciona a través de ajax
 */
Route::set('ajax-unlike-discuss', 'ajax-unlike-discuss')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'unlike_discuss',
	));

/**
 * Crea el comentario a una discusión, funciona a través de ajax
 */
Route::set('ajax-comment-discuss', 'ajax-comment-discuss')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'comment_discuss',
	));

/**
 * Obtiene las calificaciones de los libros
 */
Route::set('ajax-retrieve-ratings', 'ajax-retrieve-ratings')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'retrieve_ratings',
	));

//=========================/ DETALLE DEL LIBRO =================================

// pagina contacto
Route::set('contacto', '(<section>(/<page>))', array('section' => 'contacto', 'page' => '[a-z0-9\-]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'contact',
		'action'     => 'index',
	));

//contacto - gracias
Route::set('contacto_thanks', '(<section>/gracias/<email>)', array('section' => 'contacto', 'email' => '[a-zA-Z0-9\-=]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'contact',
		'action'     => 'thanks',
	));

// pagina ayuda
Route::set('ayuda', 'ayuda', array())
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'help',
		'action'     => 'index',
	));

// simple content
Route::set('pages', '(<section>(/<page>))', array('section' => 'politicas-de-privacidad|terminos-y-condiciones', 'page' => '[a-z0-9\-]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'pages',
		'action'     => 'index',
	));

//biblioteca general
Route::set('general_library', '(<section>)', array('section' => 'biblioteca_general'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'library',
		'action'     => 'general_library',
));

/**
 * biblioteca_lector
 */
Route::set('reader_library', '(<section>(/<lector>))', array('section' => 'biblioteca_lector'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'library',
		'action'     => 'reader_library',
));

//Trivias
Route::set('trivias_list_view', 'trivias(/page/<page>)')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'index',
	));

Route::set('trivias_create_view', 'trivias/crear')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'create',
	));

Route::set('trivias_create_make', 'trivias/crear_commit')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'create_commit',
	));
Route::set('trivias_answered_make', 'trivias/contestar_commit')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'trivia_answered_commit',
	));

Route::set('trivias_search_make', 'trivias/search(/<page>(/<busqueda>))')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'index_search',
	));

Route::set('trivias_unique', 'trivias/findid/<id>')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'index_unique',
	));

Route::set('trivia_selected_view', 'trivias/select/<id>')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'show'
		));

Route::set('trivia_like', 'trivias/like')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'like_commit'
		));

Route::set('trivia_dislike', 'trivias/dislike')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'dislike_commit'
		));

Route::set('trivia_ajax_ver_resultados_trivia', 'trivias/verresults')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'trivia_ajax_get_trivia_answers'
		));


//Listas
Route::set('lists', '(<section>(/<user_id>)/<list_slug>)', array('section' => 'lista', 'user_id' => '[0-9]+', 'list_slug' => '[a-z0-9\-]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'list',
		'action'     => 'list_details',
));

// Carga de listas destacadas
Route::set('ajax-featured-lists', 'ajax/listas_destacadas/<amount>', array('amount' => '[0-9]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'list',
		'action'     => 'ajax_featured_lists',
	));


// Lista de todos los libros en listas por usuario
Route::set('list_all_contents', 'lista/<user_id>',
	array('section' => 'lista',
		'user_id' => '[a-zA-Z0-9\-=]+')
	)
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'list',
		'action'     => 'list_all',
));

Route::set('remove_book_from_list', '(<section>/<user_id>/<list_slug>/<delete>/<book_id>)', array('section' => 'lista', 'user_id' => '[0-9]+', 'list_slug' => '[a-z0-9\-]+', 'delete' => 'del', 'book_id' => '[0-9]+'))
  ->defaults(array(
    'directory'  => 'frontend',
    'controller' => 'list',
    'action'     => 'remove_book_from_list',
));

Route::set('ajax_advanced_search_book', '(advanced_search_book)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'advanced_search_book',
));

Route::set('ajax_search_filter', '(search_filter)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'search_filter',
));

Route::set('ajax_search_author_filter', '(search_author_filter)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'search_author_filter',
));

Route::set('ajax_search_editorial_filter', '(search_editorial_filter)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'search_editorial_filter',
));

Route::set('ajax_search_type_filter', '(search_type_filter)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'search_type_filter',
));

Route::set('ajax_search_colofon_filter', '(search_colofon_filter)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'search_colofon_filter',
));

Route::set('ajax_search_keywords_filter', '(search_keywords_filter)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'search_keywords_filter',
));

Route::set('ajax_search_coleccion_filter', '(search_coleccion_filter)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'search_coleccion_filter',
));

//formulario de contacto
Route::set('submit_contact_form', '(submit-contact)')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'contact',
		'action'     => 'submit',
));

//búsqueda de libro
Route::set('normal_book_search', '(buscar/libro(/<pag>))', array('pag' => '[0-9]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'normalsearch',
		'action'     => 'book',
));

//búsueda de Editoriales
Route::set('editorial_search', '(editoriales(/<pag>))', array('pag' => '[0-9]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'normalsearch',
		'action'     => 'editorial',
));

// Ver a mis seguidores
Route::set('followers', 'seguidores')
    ->defaults(array(
        'directory'  => 'frontend',
        'controller' => 'relationship',
        'action'     => 'followers',
));

//============================ MODULO DE SEGUIDORES ===========================//
// Relationships

// Ver a la gente que sigo
Route::set('following', 'siguiendo')
	->defaults(array(
	    'directory'  => 'frontend',
	    'controller' => 'relationship',
	    'action'     => 'following',
));

// Obtengo a los siguientes seguidores
Route::set('get_next_following', 'get_next_following')
	->defaults(array(
	    'directory'  => 'frontend',
	    'controller' => 'relationship',
	    'action'     => 'get_next_following',
		));

// Obtengo a mis seguidores
Route::set('get_next_followers', 'get_next_followers')
	->defaults(array(
	    'directory'  => 'frontend',
	    'controller' => 'relationship',
	    'action'     => 'get_next_followers',
		));

// Ajax petición de seguimiento
Route::set('ajax-follow-user', 'follow-user')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'relationship',
		'action'     => 'ajax_follow_user',
	));

// Ajax dejar de seguir
Route::set('ajax-unfollow-user', 'unfollow-user')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'relationship',
		'action'     => 'ajax_unfollow_user',
	));

// Ajax para aceptar la solicitud de seguimiento
Route::set('ajax-accept-follow-user', 'accept-follow-user')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'relationship',
		'action' => 'ajax_accept_follow_user'
	));

Route::set('ajax-reject-follow-user', 'reject-follow-user')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'relationship',
		'action' => 'ajax_reject_follow_user'
	));

// Recibe la importación de ms
Route::set('importcontacts-mss', 'importcontacts-ms')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'relationship',
		'action'     => 'followers',
	));

// Ajax del envío de invitaciones
Route::set('ajax-send-invites', 'send-invites')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'relationship',
		'action'     => 'ajax_send_invites',
	));

// Obtiene el estatus de la relación a través de ajax
Route::set('ajax-relationship-get-relationship-status',
	'relationship/get_relationship_status')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'relationship',
		'action' => 'ajax_get_relationship_status'
	));

// Regresa la cantidad de peticiones pendientes
Route::set('ajax-relationship-pending-qty-requests',
	'pending-qty-requests')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'relationship',
		'action' => 'ajax_pending_qty_requests'
		));

// Ajax buscar amigos
Route::set('ajax-search-user', 'search-user')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'userfront',
		'action'     => 'ajax_search_user',
	));


//============================ MODULO DE SEGUIDORES ===========================//

Route::set('ajax_rank_book', '(rank_book)')
    ->defaults(array(
        'directory'  => 'frontend',
        'controller' => 'ajax',
        'action'     => 'rank_book',
));

Route::set('ajax_rank_location', '(rank_location)')
    ->defaults(array(
        'directory'  => 'frontend',
        'controller' => 'ajax',
        'action'     => 'rank_location',
));

Route::set('ajax_rank_location_subject', '(rank_location_subject/<subject>)')
    ->defaults(array(
        'directory'  => 'frontend',
        'controller' => 'ajax',
        'action'     => 'rank_location_subject',
));

Route::set('ajax_tooltip', '(ajax_tooltip)')
    ->defaults(array(
        'directory'  => 'frontend',
        'controller' => 'ajax',
        'action'     => 'tooltip',
));
Route::set('ajax_add_to_list', '(add_to_list)')
    ->defaults(array(
        'directory'  => 'frontend',
        'controller' => 'ajax',
        'action'     => 'add_book_to_list',
));
Route::set('ajax_remove_to_list', '(remove_to_list)')
    ->defaults(array(
        'directory'  => 'frontend',
        'controller' => 'ajax',
        'action'     => 'remove_book_to_list',
));

/**
 * Creación de listas utilizando una llamada por ajax
 */
Route::set('ajax_create_list', 'ajax_create_list')
    ->defaults(array(
        'directory'  => 'frontend',
        'controller' => 'ajax',
        'action'     => 'create_list',
));

Route::set('ajax_delete_list', '(delete_list)')
    ->defaults(array(
        'directory'  => 'frontend',
        'controller' => 'ajax',
        'action'     => 'delete_list',
));

//Trivias
Route::set('trivias_list_view', 'trivias(/page/<page>)')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'index',
	));

Route::set('trivias_create_view', 'trivias/crear')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'create',
	));

Route::set('trivias_create_make', 'trivias/crear_commit')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'create_commit',
	));
Route::set('trivias_answered_make', 'trivias/contestar_commit')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'trivia_answered_commit',
	));

Route::set('trivias_search_make', 'trivias/search(/<page>(/<busqueda>))')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'index_search',
	));

Route::set('trivias_unique', 'trivias/findid/<id>')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'index_unique',
	));

Route::set('trivia_selected_view', 'trivias/select/<id>')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'show'
		));

Route::set('trivia_like', 'trivias/like')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'like_commit'
		));

Route::set('trivia_dislike', 'trivias/dislike')
	->defaults(array(
		'directory' => 'frontend',
		'controller' => 'trivias',
		'action' => 'dislike_commit'
		));

// Listas
Route::set('list_api', '(api/listas/<action>)')
		->defaults(array(
			'directory'  => 'api',
			'controller' => 'listas',
			'action'     => 'index',
		));

Route::set('list-view', 'listas')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'list',
		'action'     => 'get_all_list',
	));

Route::set('ajax_review_book', '(review_book)')
    ->defaults(array(
        'directory'  => 'frontend',
        'controller' => 'ajax',
        'action'     => 'review_book',
));
Route::set('ajax_like_comment', '(like-comment)')
    ->defaults(array(
        'directory'  => 'frontend',
        'controller' => 'ajax',
        'action'     => 'like_comment',
));
Route::set('ajax_like_list', '(like-list)')
    ->defaults(array(
        'directory'  => 'frontend',
        'controller' => 'ajax',
        'action'     => 'like_list',
));
Route::set('ajax_rank_book', '(rank_book)')
    ->defaults(array(
        'directory'  => 'frontend',
        'controller' => 'ajax',
        'action'     => 'rank_book',
));

Route::set('empty-response', 'empty-response')
  ->defaults(array(
  	'directory' => 'frontend',
    'controller' => 'ajax',
    'action' => 'empty_response',
));

  Route::set('email-test', 'email/send')
  ->defaults(array(
  	'directory' => 'frontend',
    'controller' => 'normalsearch',
    'action' => 'email',
));

  Route::set('set-denounce', 'set-denounce')
  ->defaults(array(
  	'directory' => 'frontend',
    'controller' => 'ajax',
    'action' => 'set_denounce',
));


Route::set('check-notification', 'check-notification')
  ->defaults(array(
  	'directory' => 'frontend',
    'controller' => 'ajax',
    'action' => 'check_for_notification',
));

Route::set('unset-notification', 'unset-notification')
  ->defaults(array(
  	'directory' => 'frontend',
    'controller' => 'ajax',
    'action' => 'unset_notification',
));


Route::set('old-libros', '<section>(/<news>)', array('section' => 'inicio|noticias|clubes_de_lectura','news'=>'[a-z0-9\-]+'))
  ->defaults(array(
  	'directory' => 'frontend',
    'controller' => 'homepage',
    'action' => 'index',
));
//Ligar cuenta con FB
	Route::set('link-facebook', 'link-facebook')
	  ->defaults(array(
	  	'directory' => 'frontend',
	    'controller' => 'ajax',
	    'action' => 'link_facebook'
));

//ruta para appendList
Route::set('compare_appendList', 'appendList/<id_theirlist>/<id_mylist>', array( 'id_theirlist' => '[0-9]+', 'id_mylist' => '[0-9]+' ))
  ->defaults(array(
  	'directory' => 'frontend',
    'controller' => 'ajax',
    'action' => 'appendList',
));

//ruta para appendNew
Route::set('compare_appendNew', 'appendNew/<id_mylist>/<id_theirlist>', array( 'id_mylist' => '[0-9]+', 'id_theirlist' => '[0-9]+' ))
  ->defaults(array(
  	'directory' => 'frontend',
    'controller' => 'ajax',
    'action' => 'appendNew',
));

//ruta para copyList
Route::set('compare_copyList', 'copyList/<id_theirlist>', array( 'id_theirlist' => '[0-9]+' ))
  ->defaults(array(
  	'directory' => 'frontend',
    'controller' => 'ajax',
    'action' => 'copyList',
));

//comparación de listas 'libros en comun'
Route::set('compare-comun', '(listas/comparar/<id_mine>/<id_other>/<comparing_section>/<current_pag>/<res_per_page>)', array( 'id_mine' => '[0-9]+' , 'id_other' => '[0-9]+' , 'comparing_section' => 'en-comun' ,'current_pag' => '[0-9]+', 'res_per_page' => '[0-9]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'list',
		'action'     => 'common_books',
));

//comparación de listas 'libros que yo no tengo'
Route::set('compare-ihave-not', '(listas/comparar/<id_mine>/<id_other>/<comparing_section>/<current_pag>/<res_per_page>)', array( 'id_mine' => '[0-9]+' , 'id_other' => '[0-9]+' , 'comparing_section' => 'no-tengo' ,'current_pag' => '[0-9]+', 'res_per_page' => '[0-9]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'list',
		'action'     => 'notengo_books',
));


//comparación de listas 'libros que solo yo tengo'
Route::set('compare-ihave', '(listas/comparar/<id_mine>/<id_other>/<comparing_section>/<current_pag>/<res_per_page>)', array( 'id_mine' => '[0-9]+' , 'id_other' => '[0-9]+' , 'comparing_section' => 'solo-yo-tengo' ,'current_pag' => '[0-9]+', 'res_per_page' => '[0-9]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'list',
		'action'     => 'soloyotengo_books',
));

Route::set('libros-delete-cache', 'books/delete')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'book',
		'action'     => 'delete_cache',
	));

Route::set('achievements', '<section>', array( 'section' => 'mis-logros' ))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'userfront',
		'action'     => 'my_achievements',
	));

//tablas de posiciones
Route::set('top_users', '<section>/<users>', array( 'section' => 'tabla-posiciones', 'users' => '[a-z]+' ))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'userfront',
		'action'     => 'top_users',
	));


Route::set('sharefb_notify', 'sharefb-notify')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'share_fb_notify',
	));

Route::set('sharetw_notify', 'sharetw-notify')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'ajax',
		'action'     => 'share_tw_notify',
	));

//--------------> ATLAS de lectura
Route::set('atlas', '(<section>)', array('section' => 'atlas-de-lectura'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'index',
	));

	//buscar ubicacion
Route::set('location_search', '(<section>/busqueda)', array('section' => 'atlas-de-lectura'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'search',
	));

//buscar ubicacion
Route::set('location_detail', '(<section>/detalle/<id>)', array('section' => 'atlas-de-lectura'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'detail',
	));

//obtener evaluados
Route::set('location_eval', '(<section>/eval)', array('section' => 'atlas-de-lectura'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'show_location_evaluated',
	));

// obtener las discusiones
Route::set('atlas_ajax_get_discussions', 'atlas-de-lectura/discusiones')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'ajax_get_discussions',
	));

// crear una discusión creada con un robot
Route::set('atlas_ajax_create_discussion_robot', 'atlas-de-lectura/discusiones/crear-robot')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'ajax_create_discussion_robot',
	));

// crear una discusión creada por una persona
Route::set('atlas_ajax_create_discussion', 'atlas-de-lectura/discusiones/crear')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'ajax_create_discussion',
	));

// Comentar una discusión
Route::set('atlas_ajax_comment_discussion', 'atlas-de-lectura/discusiones/comentar-discusion')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'ajax_comment_discussion',
	));

// Comentar un comentario
Route::set('atlas_ajax_comment_comment', 'atlas-de-lectura/discusiones/comentar-comentario')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'ajax_comment_comment',
	));

// dar like a una discusión creada por una persona
Route::set('atlas_ajax_discussion_like', 'atlas-de-lectura/discusion/me-gusta')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'ajax_discussion_like',
	));

	// dar like a un comentario creada por una persona
Route::set('atlas_ajax_comment_like', 'atlas-de-lectura/comment/me-gusta')
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'ajax_comment_like',
	));

Route::set('atlas_near_places', '(<section>)', array('section' => 'near_places'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'near_places',
	));

Route::set('atlas_more_commented', '(<section>)/<sub_section>/<current_pag>/<res_per_page>', array('section' => 'atlas-de-lectura', 'sub_section' => 'sitios-mas-comentados','current_pag' => '[0-9]+', 'res_per_page' => '[0-9]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'more_commented',
	));

Route::set('atlas_popular', '(<section>)/<sub_section>/<current_pag>/<res_per_page>', array('section' => 'atlas-de-lectura', 'sub_section' => 'lugares-populares','current_pag' => '[0-9]+', 'res_per_page' => '[0-9]+'))
	->defaults(array(
		'directory'  => 'frontend',
		'controller' => 'atlas',
		'action'     => 'popular_sites',
	));

// atlas submit location
Route::set('api-atlas-submit', 'api/atlas/submit')
	->defaults(array(
		'directory'  => 'api',
		'controller' => 'atlas',
		'action'     => 'submit',
	));

//Get images (Simple HTML DOM)
Route::set('simplehtmldom', '(<section>)/<sub_section>', array('section' => 'simplehtmldom', 'sub_section' => 'get-images'))
  ->defaults(array(
    'directory'  => 'frontend',
    'controller' => 'simplehtmldom',
    'action'     => 'get_images',
));
Route::set('simplehtmldom', '(<section>)/<sub_section>', array('section' => 'simplehtmldom', 'sub_section' => 'set-images'))
  ->defaults(array(
    'directory'  => 'frontend',
    'controller' => 'simplehtmldom',
    'action'     => 'set_urlimg',
));

//crear handle de usuarios
/*Route::set('prueba', '(<section>)', array('section' => 'prueba'))
	->defaults(array(
		'directory'  => 'frontend',
    'controller' => 'userfront',
    'action'     => 'get_handle',
));*/

//Send mail
Route::set('enviar_mail_de_prueba', '(<section>)', array('section' => 'enviar_mail_de_prueba'))
	->defaults(array(
		'directory'  => 'frontend',
    'controller' => 'userfront',
    'action'     => 'send_mail',
));

?>