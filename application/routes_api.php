<?php 
/**
 * Todas las rutas llevan como prefijo admin!
 */
define(PI_API_V1, 'api/v1');
define(PI_API_V1_ALT, 'api_v1');

/****************************** Documentación *********************************/
// Página inicial
Route::set(PI_API_V1_ALT.'', PI_API_V1.'')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'doc',
    'action'     => 'index'
  ));

// Documentación de usuarios
Route::set(PI_API_V1_ALT.'_doc_usuarios', PI_API_V1.'/doc/usuarios')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'doc',
    'action'     => 'usuarios'
  ));

// Documentación de atlas
Route::set(PI_API_V1_ALT.'_doc_atlas', PI_API_V1.'/doc/atlas')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'doc',
    'action'     => 'atlas'
  ));

// Documentación de atlas
Route::set(PI_API_V1_ALT.'_doc_libros', PI_API_V1.'/doc/libros')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'doc',
    'action'     => 'libros'
  ));

/****************************** Usuarios del API ******************************/
// Registro de usuario del API
Route::set(PI_API_V1_ALT.'_apiuser_register', PI_API_V1.'/apiuser/register')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'apiuser',
    'action'     => 'register'
  ));

// Crear al usuario
Route::set(PI_API_V1_ALT.'_apiuser_create', PI_API_V1.'/apiuser/create')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'apiuser',
    'action'     => 'create'
  ));

// Página de inicio de sesión
Route::set(PI_API_V1_ALT.'_apiuser_login', PI_API_V1.'/apiuser/login')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'apiuser',
    'action'     => 'login'
  ));

// Iniciar sesión
Route::set(PI_API_V1_ALT.'_apiuser_do_login', PI_API_V1.'/apiuser/do_login')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'apiuser',
    'action'     => 'do_login'
  ));

// Cerrar sesión
Route::set(PI_API_V1_ALT.'_apiuser_logout', PI_API_V1.'/apiuser/logout')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'apiuser',
    'action'     => 'logout'
  ));

// Perfil de usuario
Route::set(PI_API_V1_ALT.'_apiuser_profile', PI_API_V1.'/apiuser/profile')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'apiuser',
    'action'     => 'profile'
  ));

/****************************** Servicios *************************************/

/******* Usuarios ********/
// Obtener información de un usuario
Route::set(PI_API_V1_ALT.'_userfront_id', PI_API_V1.'/userfront/<userfront_id>', array('userfront_id' => '[0-9]+'))
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'userfront',
    'action'     => 'get'
  ));

/******* Atlas ********/
// Obtener información de un lugar
Route::set(PI_API_V1_ALT.'_atlas_id', PI_API_V1.'/atlas/<atlas_id>', array('atlas_id' => '[0-9]+'))
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'get'
  ));

// Obtener información de varios lugares
Route::set(PI_API_V1_ALT.'_atlas_get_locations', PI_API_V1.'/atlas/get_locations')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'get_locations'
  ));

// Obtener información de varios lugares según las limitaciones
Route::set(PI_API_V1_ALT.'_atlas_get_location_page_by_bounds', PI_API_V1.'/atlas/get_location_page_by_bounds')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'get_location_page_by_bounds'
  ));

// Contar la cantidad de lugares según las limitaciones
Route::set(PI_API_V1_ALT.'_atlas_count_locations_by_coords', PI_API_V1.'/atlas/count_locations_by_coords')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'count_locations_by_coords'
  ));

// Obtener información de varios lugares según las limitaciones de un radio
Route::set(PI_API_V1_ALT.'_atlas_get_location_page_by_bounds_radius', PI_API_V1.'/atlas/get_location_page_by_bounds_radius')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'get_location_page_by_bounds_radius'
  ));

// Contar la cantidad de lugares según las limitaciones de un radio
Route::set(PI_API_V1_ALT.'_atlas_count_locations_by_coords_radius', PI_API_V1.'/atlas/count_locations_by_coords_radius')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'count_locations_by_coords_radius'
  ));

// Obtener la pagina de información de un lugar
Route::set(PI_API_V1_ALT.'_atlas_detail_id', PI_API_V1.'/atlas/detail/<atlas_id>', array('atlas_id' => '[0-9]+'))
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'get_Location_Page_By_Id'
  ));

// Buscar lugares
Route::set(PI_API_V1_ALT.'_atlas_search_longitude_latitude_page', 
  PI_API_V1.'/atlas/search/<latitude>/<longitude>', 
  array('latitude' => '[-0-9.]+', 
    'longitude' => '[-0-9.]+'))
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'search'
  ));

//Obtener el rating 
Route::set(PI_API_V1_ALT.'_rating', PI_API_V1.'/atlas/rating/<atlas_id>', array('atlas_id' => '[0-9]+'))
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'get_rating'
  ));

//Obtener los tags existentes
Route::set(PI_API_V1_ALT.'_tags', PI_API_V1.'/atlas/tags', array())
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'get_tags'
  ));

// Contar la cantidad total de ubicaciones
Route::set(PI_API_V1_ALT.'_atlas_get_total', PI_API_V1.'/atlas/get_total')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'get_total'
  ));

// Indica si un lugar está abierto o cerrado
Route::set(PI_API_V1_ALT.'_atlas_is_location_open', 
  PI_API_V1.'/atlas/is_location_open/<atlas_id>/<day>', 
  array('atlas_id' => '[0-9]+', 
    'day' => '[0-9]'))
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'is_location_open'
  ));

// Obtiene los lugares cercanos
Route::set(PI_API_V1_ALT.'_atlas_get_near_places', PI_API_V1.'/atlas/get_near_places')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'get_near_places'
  ));

// Obtiene los ids de los lugares cercanos
Route::set(PI_API_V1_ALT.'_atlas_get_near_places_ids', 
  PI_API_V1.'/atlas/get_near_places_ids')
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'atlas',
    'action'     => 'get_near_places_ids'
  ));

/******* Libros ********/
// Obtener información de un libro
Route::set(PI_API_V1_ALT.'_book', PI_API_V1.'/book/<book_id>', array('book_id' => '[0-9]+'))
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'book',
    'action'     => 'get'
  ));

// 
Route::set(PI_API_V1_ALT.'_book_amount_of_views', PI_API_V1.'/book/amount_of_views/<book_id>', array('book_id' => '[0-9]+'))
  ->defaults(array(
    'directory'  => PI_API_V1,
    'controller' => 'book',
    'action'     => 'amount_of_views'
  ));
