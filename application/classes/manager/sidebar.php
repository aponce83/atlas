<?php defined('SYSPATH') or die('No direct script access.');

class Manager_Sidebar {
  private $userfront_id = NULL;
  private $userfront = NULL;

  public function __construct () {
    $this->userfront = Session::instance()->get('user_front', NULL);
    if ($this->userfront) {
      $this->userfront_id = $this->userfront['id'];
    }
  }

  public function get_trivia_sidebar ($n = 3, $args = array()) {
    //var_dump($this->userfront_id);
    $default = array('size' => $n);
    $args = array_merge($default, $args);

    $sidebar_trivias = Model::factory('trivia')->get_sidebar_trivias($args['size']);

    //obtenemos las trivias resueltas por el usuario de las que hay en total
    $trivias_resueltas_x_user = array();
    if ($this->userfront_id) {
      $trivias_resueltas_x_user = Model::factory('trivia')->get_trivias_results_for_user($this->userfront_id);  
    }
    
    //pero hay que limpiar el arreglom, porque asi como viene, me trae un arreglo de arreglos
    $trivias_resueltas_limpio = array();
    foreach ($trivias_resueltas_x_user as $trivia_resuelta) 
    {
      array_push($trivias_resueltas_limpio, $trivia_resuelta['trivia_id']);
    }

    //creamos arreglos para las preguntas como para las respuestas de cada trivia
    $sidebar_preguntas = array();
    $sidebar_respuestas = array();
    //las llenamos
    foreach ($sidebar_trivias as $key => $trivia) {
      $preguntas_x_trivia = Model::factory('question')->read_questions_from_trivia($trivia['trivia_id']);
      array_push($sidebar_preguntas, $preguntas_x_trivia);
      $respuestas_x_trivia = array();
      foreach ($preguntas_x_trivia as $preguntas) {
        $respuestas_x_pregunta = Model::factory('answers')->read_answers_from_question($preguntas['question_id']);
        array_push($respuestas_x_trivia, $respuestas_x_pregunta);
      }
      array_push($sidebar_respuestas, $respuestas_x_trivia);
      $sidebar_trivias[$key]['books'] = Model::factory('trivia')->get_trivia_book_list($trivia['trivia_id']);
      //Agregamos un campo que nos dira si el usuario ha resuelto la trivia en cuestion

      if ($trivia['is_special'] == '1' || $this->userfront == null) 
      {
        $sidebar_trivias[$key]['solved'] = false;
      } 
      else 
      {
        //var_dump(in_array($trivia['trivia_id'], $trivias_resueltas_limpio,true));
        $sidebar_trivias[$key]['solved'] = in_array($trivia['trivia_id'], $trivias_resueltas_limpio,true);
        //if (in_array($trivia['trivia_id'], $trivias_resueltas_limpio,true))
        if ($sidebar_trivias[$key]['solved'])
                {
                    $sidebar_trivias[$key]['answers'] = Model::factory('trivia')->get_trivia_answers_for_result($this->userfront_id, $trivia['trivia_id']);
                }
                else
                {
                    $sidebar_trivias[$key]['answers'] = array();
                }
      }
      //Agregamos otro campo que nos dirá si el usuario ha dado like a la trivia en cuestion
      if (isset($this->userfront['id']))
      {
          $trivias_con_like = Model::factory('trivia')->trivias_liked_by_user($this->userfront['id']);
      }
      if (in_array($trivia['trivia_id'], $trivias_con_like,true))
      {
          $sidebar_trivias[$key]['liked'] = true;
      }
      else
      {
          $sidebar_trivias[$key]['liked'] = false;
      }
    }

    $result = array();
    $result['trivias_trivias'] = $sidebar_trivias;
    $result["trivias_preguntas"] = $sidebar_preguntas;
    $result["trivias_respuestas"] = $sidebar_respuestas;

    return $result;
  }
}