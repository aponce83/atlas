<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Sys_User model. 
 *
 * @package    Contento
 * @category   Models
 * @author     Abargon
 * @copyright  (c) 2013 Abargon
 * @license    http://abargon.com
 */
class Model_Sys_User_Merit extends Model {
	
	const CHALLENGE_INIT   = 1;
	const CHALLENGE_EASY   = 2;
	const CHALLENGE_MEDIUM = 3;
	const CHALLENGE_HARD   = 4;
	const COURSE_IN_AREA   = 5;
	const COURSE_OUT_AREA  = 6;
	const QUESTION         = 7;
	const ANSWER           = 8;
	
	public function add($user_id, $action_id, $item_id)
	{
		$check = DB::query(Database::SELECT, "
				SELECT id
				FROM sys_user_merit
				WHERE user_id = :user_id AND action_id = :action_id AND item_id = :item_id
				LIMIT 1
			")
			->parameters(array(
				':user_id'   => $user_id,
				':action_id' => $action_id,
				':item_id'   => $item_id,
			))
			->execute()
			->get('id', 0);			   
		
		if ( ! $check)
		{
			$points = DB::query(Database::SELECT, "SELECT points FROM merit WHERE id = :id")->param(':id', $action_id)->execute()->get('points', 0);
			
			DB::query(Database::INSERT, "
					INSERT INTO sys_user_merit (user_id, action_id, item_id, timestamp)
					VALUES (:user_id, :action_id, :item_id, :timestamp)
				")
				->parameters(array(
					':user_id'   => $user_id,
					':action_id' => $action_id,
					':item_id'   => $item_id,
					':timestamp' => time(),
				))
				->execute();
			
			DB::query(Database::UPDATE, "UPDATE sys_user SET merits = merits + :points WHERE id = :id")
				->parameters(array(
					':points' => $points,
					':id'     => $user_id,
				))
				->execute();
		}
	}
	
}