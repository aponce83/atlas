<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Sys_Registry model. 
 *
 * @package    Contento
 * @category   Models
 * @author     Abargon
 * @copyright  (c) 2013 Abargon
 * @license    http://abargon.com
 */
class Model_Sys_Registry extends Model {
	
	protected $_module_id = 5;
	
	public function fetch_all($params)
	{
		$settings = array();
		$parameters = array();

		if (ctype_digit( (string) $params['language_id']))
		{
			$sql .= " AND language_id = :language_id";
			$parameters[':language_id'] = $params['language_id'];
		}
		
		if (ctype_digit( (string) $params['type']))
		{
			$sql .= " AND type = :type";
			$parameters[':type'] = $params['type'];
		}
		
		return DB::query(Database::SELECT, "
				SELECT name, description, var, value
				FROM sys_registry
				WHERE 1 ".$sql." 
				ORDER BY position ASC
			")->parameters($parameters)->execute();
			
	}
	
	public function save($data)
	{
		unset($data['csrf_token']);
		
		foreach ($data as $var => $value)
		{
			
			
			DB::update('sys_registry')->set(array('value' => $value))->where('var', '=', $var)->where('language_id', '=', Session::instance()->get('language_id'))->execute();
		}
		//$this->_log_transaction($this->_module_id, 1, 'Variables', $data, 2);
		
		return TRUE;
	}
	
}