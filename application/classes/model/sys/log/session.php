<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Sys_Log_Session model. 
 *
 * @package    Contento
 * @category   Models
 * @author     Abargon
 * @copyright  (c) 2013 Abargon
 * @license    http://abargon.com
 */
class Model_Sys_Log_Session extends Model {
	
	public function save($user_id, $action, $remote_address, $user_agent, $timestamp)
	{
		if ( ! $user_id)
			return FALSE;

		// Sometimes remote address can be on IPv6, if you are using localhost
		// the problem will be that when the query converts the IP to number 
		// using INET_ATON() in MySQL will return NULL which can be stored
		if ($remote_address == "::1") {
			$remote_address = "127.0.0.1";
		}
		
		return DB::query(Database::INSERT, "
				INSERT INTO sys_log_session (user_id, action, remote_address, user_agent, timestamp)
				VALUES (:user_id, :action, INET_ATON(:remote_address), :user_agent, :timestamp)
			")
			->parameters(array(
				':user_id' => $user_id,
				':action' => $action,
				':remote_address' => $remote_address,
				':user_agent' => $user_agent,
				':timestamp' => $timestamp,
			))
			->execute();
	}
	
	public function latest($num, $user_id)
	{
		return DB::select('user_agent', array(DB::expr('INET_NTOA(remote_address)'), 'remote_address'), 'timestamp', 'action')
			->from('sys_log_session')
			->where('user_id', '=', $user_id)
			->order_by('timestamp', 'DESC')
			->limit($num)
			->offset(0)
			->execute();
	}
	
}