<?php defined('SYSPATH') or die('No direct script access.');

class Model_Menu extends Model {
	
	public static function menu($name)
	{
		$query = 'SELECT * from pi_menu where placement = \''.$name.'\'';
		$menu = DB::query(Database::SELECT, $query)->execute()->as_array();
		return $menu;
	}

  /**
   * Obtener todos los menus
   */
  public function get_all() {
    $query = <<< EOT
    SELECT * 
    FROM pi_menu
    WHERE 1
EOT;
    $result = DB::query(Database::SELECT, $query)
      ->execute()
      ->as_array();
    return $result;
  }

  /**
   * Actualiza el menu a partir de su ID
   */
  public function updateValue($id, $name, $value) {
    $result = DB::update('pi_menu')
      ->set(array($name => $value))
      ->where('id', '=', $id)
      ->execute();
    return $result;
  }
}