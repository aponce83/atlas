<?php defined('SYSPATH') or die('No direct script access.');

class Model_Autologin extends Model {

	public function __construct() {

	}

	/**
	 * Genera una cadena aleatoria
	 * @param  integer $length tamaño de la cadena
	 * @return string          cadena aleatoria
	 */
	private function generateRandomString($length = 32) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
	}

	/**
	 * Obtiene una sal para la cadena blowfish
	 * @return string cadena para la salt
	 */
	private function getBlowfishSalt() {
		$blowfish_salt = bin2hex(openssl_random_pseudo_bytes(22));
		$blowfish_salt = '$2y$12$'.$blowfish_salt;
		return $blowfish_salt;
	}

	/**
	 * Obtiene un hash
	 * @param  string $obscure_this cadena a obscurecer
	 * @return string               resultado
	 */
	private function getHash($obscure_this) {
		$salt = $this->getBlowfishSalt();
		$hash = crypt($obscure_this, $salt);
		// $hash = Cookie::salt('lmx_autologin', $hash).'~'.$hash;
		return $hash;
	}

	/**
	 * 
	 */
	private function getExpirationDate() {
		$today = date_create();
		date_add($today, date_interval_create_from_date_string('10 days'));
		$expiration_date = date_format($today, 'Y-m-d H:i:s');
		return $expiration_date;
	}

	/**
	 * Recuerda al usuario, regresa una cookie
	 * @param  int $user_id id del usuario
	 * @return string          regresa la cookie con la que se recordará al usuario
	 */
	public function remember_me($user_id) {
		$randomString = $this->generateRandomString();
		$randomString .= '__'.$user_id;
		$hash = $this->getHash($randomString);
		$expiration_date = $this->getExpirationDate();
		$created_on = date('Y-m-d H:i:s');
		$updated_on = date('Y-m-d H:i:s');

		$query = <<<EOT
		INSERT INTO userfront_autologin (userfront_id, cookie, expires_on, created_on, updated_on)
		VALUES(:userfront_id, :cookie, :expires_on, :created_on, :updated_on);
EOT;

		$params = array(
			':userfront_id' => $user_id,
			':cookie' => $hash,
			':expires_on' => $expiration_date,
			':created_on' => $created_on,
			':updated_on' => $updated_on
			);
		$query = DB::query( Database::INSERT, $query)
			->parameters($params);
		$query->execute();
		return $hash;
	}

	/**
	 * Indica si los datos de la sesión son válidos, si la llave ya ha expirado
	 * entonces borra la sesión
	 * @param  string  $autologin_data datos de la sesión
	 * @return boolean                 indica si son válidos o no son válidos
	 */
	public function is_valid_autologin_data($autologin_data) {
		$query = <<< EOT
		SELECT id, userfront_id, expires_on
		FROM userfront_autologin
		WHERE cookie = :cookie
		LIMIT 1
EOT;
		$params = array(':cookie' => $autologin_data);
		$result = DB::query(Database::SELECT, $query)
			->parameters($params)
			->execute()
			->as_array();

		$valid = false;
		if ($result) {
			$autologin = $result[0];
			$autologin_date = new DateTime($autologin['expires_on']);
			$today = new DateTime();
			if ($today < $autologin_date) {
				$valid = true;
			} else {
				$this->do_delete($autologin['id']);
			}
		}
		return $valid;
	}

	/**
	 * Regresa el ID del usuario y renueva la concesión
	 * @param  string $autologin_data datos del autologin
	 * @return mixed
	 */
	public function get_userfront_by_autologin_and_renew_data($autologin_data) {
		$query = <<< EOT
		SELECT id, userfront_id, expires_on
		FROM userfront_autologin
		WHERE cookie = :cookie
		LIMIT 1
EOT;
		$params = array(':cookie' => $autologin_data);
		$result = DB::query(Database::SELECT, $query)
			->parameters($params)
			->execute()
			->as_array();
		
		if ($result) {
			$result[0]['new_hash'] = $this->renew_data($result[0]['id'], $result[0]['userfront_id']);
		}
		return $result[0];
	}

	/**
	 * Borra la información del id enviado
	 * @param  int $id id del dato a eliminar
	 * @return boolean     falso|verdadero
	 */
	private function do_delete($id) {
		$result = DB::delete('userfront_autologin')
			->where('id', '=', $id)
			->execute();
		return $result;
	}

	/**
	 * Borra la información del cookie enviado
	 * @param  string cookie
	 * @return boolean     falso|verdadero
	 */
	public function do_delete_by_cookie($cookie) {
		$result = DB::delete('userfront_autologin')
			->where('cookie', '=', $cookie)
			->execute();
		return $result;
	}

	/**
	 * Renueva el hash de una sesión
	 * @param  int $id      id de la sesión anterior
	 * @param  int $user_id id del usuario
	 * @return string 			nuevo hash
	 */	
	private function renew_data($id, $user_id) {
		$randomString = $this->generateRandomString();
		$randomString .= '__'.$user_id;
		$hash = $this->getHash($randomString);

		$updated_on = date('Y-m-d H:i:s');

		$query = <<<EOT
		UPDATE userfront_autologin 
		SET cookie = :cookie, updated_on = :updated_on 
		WHERE id = :id;
EOT;

		$params = array(
			':id' => $id,
			':cookie' => $hash,
			':updated_on' => $updated_on
			);

		$result = DB::query( Database::UPDATE, $query)
			->parameters($params)
			->execute();
		
		return $hash;
	}
}