<?php defined('SYSPATH') or die('No direct script access.');
/**
 * List Model. 
 *
 * @package    Contento
 * @category   Models
 * @author     
 * @copyright  
 * @license    
 */
class Model_List extends Model {

  const MI_BIBLIOTECA_SLUG = 'mi-biblioteca';
  const YA_LO_LEI_SLUG = 'ya-lo-lei';
  const LO_ESTOY_LEYENDO_SLUG = 'lo-estoy-leyendo';
  const LO_QUIERO_LEER_SLUG = 'lo-quiero-leer';

  public function __construct() {
    $this->db = Database::instance('default');
    require_once Kohana::find_file('classes', 'list_obj');
  }

  /**
   * 
   * Create List
   *
   * @param string $name  List Name
   * @param int $userfront_id  User id
   * @param string $description  List Description
   * @param string $slug  List url
   * @param enum $type  dynamic or classic
   * @param string $type_list  "A" = USER_ONLY, "B" = CLASSIC, "C" = DYNAMIC
   * @param boolean $locked  It allows you to edit or modify the list
   * @param enum $privacy  public or private
   * @param string $img  image path 
   * @param array $params  params (autores, editorial, fecha_colofon_1, tipo_producto, keywords, idioma_original, temas)
   * @return boolean
   */
  public function create_list( $name, $userfront_id, $description='', $slug='', $type='classic', $type_list="B", $locked=0, $privacy='public', $img='', $key_words='', $params=array() )
  {
    $description = strip_tags($description);
    $name = strip_tags($name);

    $db = Database::instance();
    $db->begin();  //Iniciar Transacción
    try
    {
      if ( ! $this->validate_name( $name ) ) return false; //Validate name
      if ( ! $this->validate_user( $userfront_id ) ) return false; //Validate user
      if ( ! $this->validate_type( $type ) ) return false; //Validate Type
      if ( ! $this->validate_privacy( $privacy ) ) return false; //Validate Privacy
      if ( ! $this->validate_locked( $locked ) ) return false; //Validate Locked

      $slug = ( $slug == '' ) ? $this->string_for_url( $name ) : $this->string_for_url( $slug );
      $slug = $this->validate_slug( $slug, $userfront_id ); //Validate Slug

      if( $slug == self::MI_BIBLIOTECA_SLUG OR 
        $slug == self::LO_ESTOY_LEYENDO_SLUG OR 
        $slug == self::YA_LO_LEI_SLUG OR 
        $slug == self::LO_QUIERO_LEER_SLUG ){
        $type_list="A";
      }elseif( $type == 'dynamic' ){
        $type_list="C";
      }
      
      $insert_list = DB::insert('pi_list', array('name', 'slug', 'description', 'type', 'locked', 'privacy', 'img', 'key_words', 'created_on', 'updated_on', 'userfront_id', 'type_list'))
        ->values(array($name, $slug, $description, $type, $locked, $privacy, $img, $key_words, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'), $userfront_id, $type_list))
        ->execute();

      $list_id = $insert_list[0];
      
      if ( $type == 'dynamic' ) {
        //se agrega trofeo por crear una lista clásica
        $awardKey = 'A37';
        $insertAdward = Model::factory('score')->addAward( $userfront_id, $awardKey );
        if ($insertAdward != false) {
          $type = 'award';
            $notStatus = 'pendig';
            Model::factory('notification')->pushNotification( $userfront_id, $notStatus, $type, $awardKey );

            //se notifica en el muro que gano trofeo
            $type = 'award';
            $id1 = 37;
            $id2 = 0;
            Model::factory('social')->addActivity( $type, $id1, $id2, $userfront_id );
        }

        $save_array = serialize( $params );
        
        //Create Dynamic List
        $insert_list_dynamic = DB::insert('pi_list_dynamic', array('query', 'pi_list_id', 'created_on', 'updated_on'))
          ->values(array( $save_array, $list_id, NULL, NULL ))
          ->execute();

        //Add books
        $search_result = $this->search_books_from_dynamic_list( $list_id );
        /*$order = 0;
        foreach ($search_result['cuerpo'] as $book) {
          $this->add_book_to_list( $list_id, $book['id_libro'], $order );
          $order++;
        }*/
        $awardKey = 'A37';
        Model::factory('score')->addAward( $userfront_id, $awardKey );
      }

      if( $slug != self::MI_BIBLIOTECA_SLUG OR 
        $slug != self::LO_ESTOY_LEYENDO_SLUG OR 
        $slug != self::YA_LO_LEI_SLUG OR 
        $slug != self::LO_QUIERO_LEER_SLUG ) 
      {
        Model::factory('social')->addActivity('add_list',$list_id,0,$userfront_id);
      }

      $db->commit(); //Terminar Transacción

      return $insert_list[0];
    } catch( Exception $e ) {
      Log::instance()->add(Log::ERROR, "Create List: " . $e->getMessage() );
      $db->rollback();
      return false;
    }
  }

  /**
   * 
   * Update List
   *
   * @param int $list_id  List id
   * @param array $data  List data (name, userfront_id, description, slug, type, locked, privacy, img )
   * @return boolean
   */
  public function update_list( $list_id, $data )
  {
    try
    {
      if ( ! $this->validate_list( $list_id ) ) return false; //Validate List

      if( isset( $data['name'] ) )
        if ( ! $this->validate_name( $data['name'] ) ) return false; //Validate name
      
      if( isset( $data['userfront_id'] ) )
        if ( ! $this->validate_user( $data['userfront_id'] ) ) return false; //Validate user
      
      if( isset( $data['type'] ) )
        if ( ! $this->validate_type( $data['type'] ) ) return false; //Validate Type
      
      if( isset( $data['privacy'] ) )
        if ( ! $this->validate_privacy( $data['privacy'] ) ) return false; //Validate Privacy
      
      if( isset( $data['locked'] ) )
        if ( ! $this->validate_locked( $data['locked'] ) ) return false; //Validate Locked

      if( isset( $data['slug'] ) )
      {
        $userfront_id = DB::select()->from('pi_list')->where('id', '=', $list_id)->execute();
        $user_id = $userfront_id[0]['userfront_id'];
        $slug = $this->validate_slug( $slug, $user_id ); //Validate Slug
      }
        
      $timestamp = date('Y-m-d H:i:s');  
      $data['updated_on'] = $timestamp;

      $query = DB::update('pi_list')->set( $data )->where('id', '=', $list_id)->execute();

      return true;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Update List: " . $e->getMessage() );
      return false;
    }
  }
  /*
  *
  * Get list count per user
  *
  * @param int $user_id
  *
  */
  public function get_list_count ($user_id) {
    if( intval($user_id) ){
      $total = DB::query( Database::SELECT,
                "SELECT COUNT(id) as total
                  FROM pi_list
                  WHERE  userfront_id = :user_id")
              ->parameters( array( ":user_id" => $user_id ) )->execute()->get("total");
      return $total;
    }
    return 0;
  }
  /**
   * 
   * Delete List
   *
   * @param int $list_id  List id
   * @return boolean
   */
  public function delete_list( $list_id )
  {
    $db = Database::instance();
    $db->begin();  //Inisiar Transacción
    try
    {
      if ( ! $this->validate_list( $list_id ) ) return false; //Validate List

      $query = DB::delete('pi_list_dynamic')->where('pi_list_id', '=', $list_id)->execute();
      $query = DB::delete('pi_list_has_book')->where('pi_list_id', '=', $list_id)->execute();
      $query = DB::delete('pi_list')->where('id', '=', $list_id)->execute();

      $db->commit(); //Terminar Transacción

      return true;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Delete List: " . $e->getMessage() );
      $db->rollback();
      return false;
    }
  }


  /**
   * 
   * Obtiene el id del usuario según el id de la lista que se pase como parámetro
   * @param $idList       id de la lista
   * @return int $user_id id de usuario propietario de la lista
   */
  public function getOwnerByIdList( $idList ) 
  {
    $user_id = DB::query( Database::SELECT,
            "SELECT userfront_id
              FROM pi_list
              WHERE  id = :idList")
    ->parameters( array( ":idList" => $idList ) )->execute();
    return $user_id[0]['userfront_id'];
  }

  /**
   * 
   * Obtiene solo el nombre de la lista a través de su id
   * @param $idList       id de la lista
   * @return string $ist_name de la lista
   */
  public function getListNameById( $idList ) 
  {
    $list_name = DB::query( Database::SELECT,
            "SELECT name
              FROM pi_list
              WHERE  id = :idList")
    ->parameters( array( ":idList" => $idList ) )->execute();
    return $list_name[0]['name'];
  }

    /**
   * 
   * Obtiene solo el slug de la lista a través de su id
   * @param $idList       id de la lista
   * @return string $ist_name de la lista
   */
  public function getListSlugById( $idList ) 
  {
    $list_name = DB::query( Database::SELECT,
            "SELECT slug
              FROM pi_list
              WHERE  id = :idList")
    ->parameters( array( ":idList" => $idList ) )->execute();
    return $list_name[0]['slug'];
  }

  /**
   * 
   * Get List id By User and Slug
   *
   * @param int $userfront_id
   * @param string $slug  
   * @return int $list_id (or false if the list doesn't exist)
   */
  public function get_listid_by_user_slug( $userfront_id, $slug )
  {
    try
    {
      $list_id = DB::select()->from('pi_list')->where('slug', '=', $slug)->and_where('userfront_id', '=', $userfront_id )->execute();
      return $list_id[0]['id'];
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Get List id By User and Slug: " . $e->getMessage() );
      return false;
    }
  }

  /**
   * Compares different lists
   * @param  int $myListId    my list
   * @param  int $theirListId their lists
   * @return array              same = the books that both have, 
   * booksInMyListNotInTheirs = the books that are on my list and not in theirs
   * booksInTheirListNotInMine = the books that are on their list and not in mine
   */
  public function compareLists($myListId, $theirListId) {
    $result = array();
    $result["same"] = $this->getSameBooksBetweenLists($myListId, $theirListId);
    $result["booksInMyListNotInTheirs"] = $this->getDifferentBooksBetweenLists($myListId, $theirListId);
    $result["booksInTheirListNotInMine"] = $this->getDifferentBooksBetweenLists($theirListId, $myListId);
    return $result;
  }

  /**
   * Retrieves the same books between list
   * @param  int $myListId    my list id
   * @param  int $theirListId their list id
   * @return array              same books
   */
  public function getSameBooksBetweenLists($myListId, $theirListId, $current_pag = 1, $res_per_page = 10) {

    //parámetros para la paginación
    $num_rec_per_page = $res_per_page;
    $page             = $current_pag;
    $start_from       = ( $page-1 ) * $num_rec_per_page;

    $query = <<<EOT
SELECT mine.book_id
FROM pi_list_has_book mine
WHERE mine.pi_list_id = :mine
AND mine.book_id IN (
  SELECT them.book_id
  FROM pi_list_has_book them
  WHERE them.pi_list_id = :them
) LIMIT $start_from, $num_rec_per_page;
EOT;

    $params = array( ":mine" => $myListId, ":them" => $theirListId);
    $result = DB::query(Database::SELECT, $query)
      ->parameters($params)
      ->execute()
      ->as_array();
    return $result;
  }


  /**
   * Regresa el total de resultados
   * @param  int $myListId    my list id
   * @param  int $theirListId their list id
   */
  public function getSameBooksBetweenListsCount($myListId, $theirListId) {

    $query = <<<EOT
SELECT mine.book_id
FROM pi_list_has_book mine
WHERE mine.pi_list_id = :mine
AND mine.book_id IN (
  SELECT them.book_id
  FROM pi_list_has_book them
  WHERE them.pi_list_id = :them
);
EOT;

    $params = array( ":mine" => $myListId, ":them" => $theirListId);
    $result = DB::query(Database::SELECT, $query)
      ->parameters($params)
      ->execute()
      ->as_array();
    return count($result);
  }





  /**
   * Regresa todos los libros que se encuentren en ambas listas, (lisbros de lista A más libros de lista B). 
   * => si hay libros repetidos se regresa solamente uno de ellos
   * @param  int $idListA    id de la lista A
   * @param  int $idListB    id de la lista B
   * @return array           libros que están en las dos listas, excepto repeticiones
   * el query regresa la unión ya sin la repetición de libros
   */
  public function unionLists($idListA, $idListB) {

    $query = <<<EOT
      SELECT mine.book_id
      FROM pi_list_has_book mine
      WHERE mine.pi_list_id = :listA
      UNION 
      SELECT mine.book_id
      FROM pi_list_has_book mine
      WHERE mine.pi_list_id = :listB
      ;
EOT;
    
  $params = array( ":listA" => $idListA, ":listB" => $idListB);
  $result = DB::query(Database::SELECT, $query)
      ->parameters($params)
      ->execute()
      ->as_array();
    return $result;
  }

  /**
   * Regresa todos los libros que se encuentran en una lista específica
   * @param  int $idList    id de la lista 
   * @return array           libros que están en la lista solicitada
   */
  public function getBooksByListId($idList) {

    $query = <<<EOT
      SELECT mine.book_id
      FROM pi_list_has_book mine
      WHERE mine.pi_list_id = :list
      ;
EOT;
  $params = array( ":list" => $idList);
  $result = DB::query(Database::SELECT, $query)
      ->parameters($params)
      ->execute()
      ->as_array();
    return $result;

  }


  /**
   * Hace la unión de mi lista y de su lista y el resultado se guarda en mi lista 
   * @param  int $myListId        id de mi lista
   * @param  int $theirListId     id de su lista
   * @return el append no regresa nada, ya que solamente hace el insert de los nuevos libros
   * myList contendrá ahora su contenido más theirList
   */
  public function appendList($myListId, $theirListId) {
    //Log::instance()->add(Log::ERROR, "INSERTAR LIBROS EN LISTA CON ID: " . $myListId );     
    //obtener los libros de su lista que no estén en mi lista
    //newBooks es el array con todos los nuevos id's
    $newBooks = $this->getDifferentBooksBetweenLists( $theirListId, $myListId );

    
    //se hace insert de los id's en la lista cuyo id sea $myListId
    foreach ($newBooks as $newBook) {
      $this->add_book_to_list( $myListId, $newBook['book_id']);
       
    }
  }

  /**
   * Crea una lista nueva a partir de mi lista y su lista
   * @param  int $myListId        id de mi lista
   * @param  int $theirListId     id de su lista
   * @param  int $newData     información para la creación de la nueva lista
   * @return el appendNew no regresa nada, ya que crea la nueva lista y hace el insert en ella de los nuevos libros
   */
  public function appendNew($myListId, $theirListId, $newData) {
    //se crea la nueva lista con la información del $newData
    $newListId = $this->create_list( $name=$newData['list_name'], $userfront_id=$newData['list_userId'], $description=$newData['list_description'] , $slug='', $type=$newData['list_type'], $type_list=$newData['list_typeList'], $locked=$newData['list_locked'], $privacy=$newData['list_privacy'], $img='', $key_words=$newData['list_keywords'], $params=array()  );

    //se obtiene array con los id's de la unión de ambas listas
    $newBooks = $this->unionLists( $myListId, $theirListId );

    //se insertan los libros en la nueva lista
    foreach( $newBooks as $newBook ) {
      $this->add_book_to_list( $newListId, $newBook['book_id'], 0, 0 );
    }
  }



  /**
   * Crea una nueva lista con el contenido de otra
   * @param  int $listId        id de la lista a copiar
   * @param  int $newData     información para la creación de la nueva lista
   */
  public function copyList($listId, $newData) {
    //se crea la nueva lista con la información del $newData
    $newListId = $this->create_list( $name=$newData['list_name'], $userfront_id=$newData['list_userId'], $description=$newData['list_description'] , $slug='', $type=$newData['list_type'], $type_list=$newData['list_typeList'], $locked=$newData['list_locked'], $privacy=$newData['list_privacy'], $img='', $key_words=$newData['list_keywords'], $params=array()  );

    //se obtiene array con los id's de la lista solicitada
    $newBooks = $this->getBooksByListId( $listId );

    //se insertan los libros en la nueva lista
    foreach( $newBooks as $newBook ) {
      $this->add_book_to_list( $newListId, $newBook['book_id'], 0, 0 );
    } 
  }




  /**
   * Retrieves the different books between my list and their list, the result
   * is the list of my books (myListId) that are not in their list
   * @param  int $myListId    my list id
   * @param  int $theirListId their list id
   * @return array            books that are on $myListId that are not on $theirListId
   */
  public function getDifferentBooksBetweenLists($myListId, $theirListId, $current_pag, $res_per_page ) {
    //parámetros para la paginación
    $num_rec_per_page = $res_per_page;
    $page             = $current_pag;
    $start_from       = ( $page-1 ) * $num_rec_per_page;

    if(!$current_pag)
      $start_from =NULL;
    else
      $start_from = 'LIMIT '.$start_from;

    if($res_per_page)
        $num_rec_per_page =", ".$num_rec_per_page;
      else
        $num_rec_per_page = NULL;


    $query = <<<EOT
SELECT mine.book_id
FROM pi_list_has_book mine
WHERE mine.pi_list_id = $myListId 
AND mine.book_id NOT IN (
  SELECT them.book_id
  FROM pi_list_has_book them
  WHERE them.pi_list_id = $theirListId
) $start_from $num_rec_per_page;
EOT;
    $result = DB::query(Database::SELECT, $query)
      ->execute()
      ->as_array();
    //Log::instance()->add(Log::ERROR, "Result".$query);      
    return $result;
  }


  /**
   * Regresa el total de registros
   * is the list of my books (myListId) that are not in their list
   * @param  int $myListId    my list id
   * @param  int $theirListId their list id
   */
  public function getDifferentBooksBetweenListsCount($myListId, $theirListId ) {
    
    $query = <<<EOT
SELECT mine.book_id
FROM pi_list_has_book mine
WHERE mine.pi_list_id = :mine
AND mine.book_id NOT IN (
  SELECT them.book_id
  FROM pi_list_has_book them
  WHERE them.pi_list_id = :them
);
EOT;
    $params = array( ":mine" => $myListId, ":them" => $theirListId);
    $result = DB::query(Database::SELECT, $query)
      ->parameters($params)
      ->execute()
      ->as_array();
    return count($result);
  }

  /**
   * 
   * Add Book to List
   *
   * @param int $list_id
   * @param int $book_id
   * @param int $order (optional)
   * @return boolean
   */
  public function add_book_to_list( $list_id, $book_id, $order = 0, $is_new = 0 )
  {
    $db = Database::instance();
    $db->begin();  //Inisiar Transacción
    try
    {
      if ( ! $this->validate_list( $list_id ) ) return false; //Validate List
      
      $book = $this->validate_book( $book_id );
      if ( ! $book ) 
        return false; //Validate book
      else
      {
        $book_isbn = $book[0]['isbn'][0];

      }
        

      //se valida que el libro no esté ya en la lista
      $exist = $this->exist_book_in_list( $list_id, $book_id );
      if(  $exist )
      {
        // ya no se agrega el libro
        return false;
      }
        
      
      // if ( 3==45)
      // {
      //   Log::instance()->add(Log::ERROR, "Add Book to List: The book already exists in the list");
      //   return false;
      // };

      if( $order == 0 or !is_int($order) )
      {
        $order = $this->count_list( $list_id ) + 1;
      }
      
      $query = DB::insert('pi_list_has_book', array('book_id', 'book_isbn', 'order', 'is_new', 'created_on', 'updated_on', 'pi_list_id'))
        ->values(array( $book_id, $book_isbn, $order, $is_new, date('Y-m-d H:i:s'), date('Y-m-d H:i:s'), $list_id))
        ->execute();

      $user = Session::instance()->get('user_front');

      $social = Model::factory('social');
      switch ($this->getList($list_id)->slug) {
        case 'ya-lo-lei':
          $social->addActivity('read',$book_id,0,$user['id']);
        break;
        case 'lo-estoy-leyendo':
          $social->addActivity('is_reading',$book_id,0,$user['id']);
        break;
        default:
          $social->addActivity('add_book',$book_id,$list_id,$user['id']);
        break;
      }


      //verificar que la lista no sea dinámica
      $dynamic_list = $this->is_dynamic_list( $list_id );

      if( $dynamic_list == false )
      {
          //se agregan puntos al usaurio por agregar libro a una lista
          $activityKey = 'S5';
          Model::factory('score')->addPoints( $user['id'], $activityKey, -1 ); 
      }

      $db->commit(); //Terminar Transacción

      return true;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Add Book to List: ".$e->getMessage() );
      $db->rollback();
      return false;
    }
  }

  /**
   * 
   * Remove Book from List
   *
   * @param int $list_id
   * @param int $book_id
   * @return boolean
   */
  public function remove_book_from_list( $list_id, $book_id )
  {
    try
    {
      if ( ! $this->validate_list( $list_id ) ) return false; //Validate List
      if ( ! $this->exist_book_in_list( $list_id, $book_id )) return false;

      $query = DB::delete('pi_list_has_book')->where('pi_list_id', '=', $list_id)->and_where('book_id', '=', $book_id)->execute();
      return true;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Remove Book from List: ".$e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Set Order
   *
   * @param int $list_id
   * @param int $book_id
   * @param int $order
   * @return boolean
   */
  public function set_order( $list_id, $book_id, $order )
  {
    try
    {
      if ( ! $this->validate_list( $list_id ) ) return false; //Validate List
      if ( ! $this->exist_book_in_list( $list_id, $book_id )) return false;

      $book = $this->validate_book( $book_id );
      if ( ! $book ) 
        return false; //Validate book

      $data = array('order' => $order );

      $timestamp = date('Y-m-d H:i:s');  
      $data['updated_on'] = $timestamp;

      $query = DB::update('pi_list_has_book')->set( $data )->where('pi_list_id', '=', $list_id)->and_where('book_id', '=', $book_id)->execute();

      return true;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "List Set Order: ".$e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Count List
   *
   * @param int $list_id
   * @return int (number of books on the list) or false
   */
  public function count_list( $list_id )
  {
    try
    {
      if ( ! $this->validate_list( $list_id ) ) return false; //Validate List

      $count = DB::select()->from('pi_list_has_book')->where('pi_list_id', '=', $list_id)->execute()->count();
      return $count;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Count List: ".$e->getMessage() );
      return false;
    }
  }

  /**
   * Retrieves the amount of readed books of a user
   * @param  int $userfront_id user id
   * @return int               amount of readed books
   */
  public function getAmountOfReadedBooksByUserId($id) {  
    $amount = $this->getAmountOfBooksByUserIdAndSlug($id, self::YA_LO_LEI_SLUG);
    return $amount;
  }

  /**
   * Retrieves the amount of books on a list given the user id and the
   * list slug
   * @param  int $id   user id
   * @param  string $slug list slug
   * @return int       amount of books
   */
  public function getAmountOfBooksByUserIdAndSlug($id, $slug) {
    $query = <<<EOT
SELECT l.userfront_id, l.id as pi_list_id, count(l.id) as amount
FROM pi_list l, pi_list_has_book lhb
WHERE slug = :slug
AND l.id = lhb.pi_list_id
AND l.userfront_id = :id
GROUP BY l.id;
EOT;
    $params = array( ":id" => $id,
      ":slug" => $slug);
    $result = DB::query(Database::SELECT, $query)
      ->parameters($params)
      ->execute()
      ->as_array();
    $amount = $result[0]["amount"] == null ? 0 : $result[0]["amount"];
    return $amount;
  }

  /**
   * 
   * Search Dynamic List
   *
   * @param int $list_id  List id
   * @return array (with all the books from the list)
   */
  public function search_books_from_dynamic_list( $list_id )
  {
    try {
      if ( ! $this->validate_list( $list_id ) ) return false; //Validate List

      $data = DB::select('query')->from('pi_list_dynamic')->where('pi_list_id', '=', $list_id)->execute();
      
      if( count($data) == 0 )
      {
        Log::instance()->add(Log::ERROR, "Search Books From Dynamic List: The list is not dynamic or has no parameters");
        return false;
      }
      
      $params = unserialize( $data[0]['query'] );

      //---Se pide un solo libro para recuperar del encabezado el total de la consulta
      $params['registros_por_pagina'] = 1;
      $lmx1 = new LibrosMexicoInterface();
      $request = $lmx1->makeRequest("busquedaAvanzada", $params, false, false);
      if ($request->isValid()) {
        $book['encabezado'] = $request->header;
        $total_de_consulta = $book['encabezado']['registros'];
      }

      //---Se pide el total de libros
      $params['registros_por_pagina'] = $total_de_consulta;
      $params['attr'] = 'id_libro'; //Sólo regresa el id de cada libro

      $lmx = new LibrosMexicoInterface();

      $request = $lmx->makeRequest("busquedaAvanzada", $params, false, false);
      if ($request->isValid()) {
        $book['encabezado'] = $request->header;
        $book['cuerpo'] = $request->body;
      }
      return $book;
    } catch( Exception $e ) {
      Log::instance()->add(Log::ERROR, "Search Books From Dynamic List: " . $e->getMessage() );
      return false;
    }

  }

  /**
   * 
   * Get Params for Dynamic List
   *
   * @param int $list_id  List id
   * @return array (with all the params from the list)
   */
  public function get_parms_for_dynamic_list( $list_id )
  {
    try
    {
      if ( ! $this->validate_list( $list_id ) ) return false; //Validate List
      $data = DB::select('query')->from('pi_list_dynamic')->where('pi_list_id', '=', $list_id)->execute();
      
      if( count($data) == 0 )
      {
        Log::instance()->add(Log::ERROR, "Get Params From Dynamic List: The list is not dynamic or has no parameters");
        return false;
      }
      $params = unserialize( $data[0]['query'] );

      return $params;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Get Params From Dynamic List: " . $e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Auto Populate Dynamic List
   *
   * @param int $list_id  List id
   * @return array (with all the NEW books from the list)
   */
  public function auto_populate_dynimic_list( $list_id )
  {
    try
    {
      if ( ! $this->validate_list( $list_id ) ) return false; //Validate List

      $list_books = DB::select('book_id')->from('pi_list_has_book')->where( 'pi_list_id', '=', $list_id )->execute();
      foreach ($list_books as $book) {
        $ids_list_books[] = (int)$book['book_id'];
      }

      $query_books = $this->search_books_from_dynamic_list( $list_id );
      $order = $this->count_list( $list_id );

      foreach ($query_books['cuerpo'] as $book) {
        $ids_query_books[] = $book['id_libro'];
        if( ! in_array( $book['id_libro'], $ids_list_books) ){
          $new_books[] = $book['id_libro'];
          $order++;
          $this->add_book_to_list( $list_id, $book['id_libro'], $order, true ); //true indicates that it is new 
        }
      }
      return $new_books;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Auto Poplate Dynamic List: " . $e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Is New Book in List
   *
   * @param int $book_id 
   * @param int $list_id 
   * @return boolean
   */
  public function is_new_book_in_list( $book_id, $list_id )
  {
    try
    {
      if ( ! $this->validate_list( $list_id ) ) return false; //Validate List
      if ( ! $this->is_dynamic_list( $list_id ) ) return false; //Validate Dynamic List

      $data = DB::select('is_new')->from('pi_list_has_book')->where('pi_list_id', '=', $list_id)->and_where('book_id', '=', $book_id)->execute();
      if( $data[0]['is_new'] == '1' )
        return true;
      else
        return false;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Is new the book: " . $e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Is New Book in List
   *
   * @param int $list_id 
   * @return boolean
   */
  
  public function how_many_new_books_are_there( $list_id )
  {
    try
    {
      if ( ! $this->validate_list( $list_id ) ) return false; //Validate List

      $data = DB::select('book_id')->from('pi_list_has_book')->where('pi_list_id', '=', $list_id)->and_where('is_new', '=', 1)->execute();
      
      $new_books = array();

      foreach ($data as $value) {
        $new_books[] = $value['book_id'];
      }

      return $new_books;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "How many new books are there: " . $e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * It is no longer a new book
   *
   * @param int $book_id 
   * @param int $list_id 
   * @return boolean
   */
  public function its_no_longer_a_new_book( $book_id, $list_id )
  {
    try
    {
      if ( ! $this->validate_list( $list_id ) ) return false; //Validate List
      if ( ! $this->is_dynamic_list( $list_id ) ) return false; //Validate Dynamic List

      $query = DB::update('pi_list_has_book')->set(array('is_new' => 0))->where('pi_list_id', '=', $list_id)->and_where('book_id', '=', $book_id)->execute();

      return $query; //int(1) si se efectuo el cambio, int(0) si no hubo cambio
    }
    catch( Exception $e)
    {
      Log::instance()->add(Log::ERROR, "It is no longer a new book: " . $e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Is Dynamic List
   *
   * @param int $list_id 
   * @return boolean
   */
  public function is_dynamic_list( $list_id )
  {
    try
    {
      if ( ! $this->validate_list( $list_id ) ) return false; //Validate List

      $type = DB::select('type')->from('pi_list')->where('id', '=', $list_id)->execute();
      
      if( $type[0]['type'] == 'dynamic' )
        return true;
      else
        return false;
      
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Is Dynamic List: " . $e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Get Book Rating
   *
   * @param int $book_id 
   * @return float raiting average
   */
  public function get_book_rating( $book_id )
  {
    try
    {
      $all_rating_book = DB::select()->from('pi_rating')->where( 'resource', '=', 'book' )->and_where( 'resource_id', '=', $book_id )->execute();
      
      if( count($all_rating_book) == 0 )
        return 0;

      $rating_sum = 0;
      $rating_count = 0;

      foreach ($all_rating_book as $rating)
      {
        $rating_sum +=   $rating['rating'];
        $rating_count++;
      }

      if( $rating_count != 0)
        $rating_average = $rating_sum / $rating_count;
      else
        $rating_average = 0;

      return $rating_average;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Get Book Rating: " . $e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Get Added Date Book
   *
   * @param int $list_id
   * @param int $book_id
   * @return string date format ('Y-m-d H:i:s')
   */
  public function get_added_date_book( $list_id, $book_id )
  {
    try
    {
      $added_date_book = DB::select('created_on')->from('pi_list_has_book')->where( 'pi_list_id', '=', $list_id )->and_where( 'book_id', '=', $book_id )->execute();
      
      if ( $added_date_book[0]['created_on'] == NULL)
        return '1970-01-01';
      else
        return $added_date_book[0]['created_on'];
    
    }
    catch( Exception $e)
    {
      Log::instance()->add(Log::ERROR, "Get Added Date Book: " . $e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Advanced Search
   *
   * @param array search params
   * @return array (with all the books from the list)
   */
  public function advanced_search( $params )
  {
    try
    {
      $params = array_merge($params, array("todos" => "0"));
      $lmx = new LibrosMexicoInterface();

      $request = $lmx->makeRequest("busquedaAvanzada", $params, false);
      if ($request->isValid()) {
        //error_log(json_encode(($request->header["registros"])));
        $book['encabezado'] = $request->header;
        $book['cuerpo'] = $request->body;
      }

      return $book;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Advanced Search: " . $e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Editoriales
   *
   * @param array search params
   * @return array (with all the editorials)
   */
  public function editorial_search( $params = array() )
  {
    try
    {
      $lmx = new LibrosMexicoInterface();

      $request = $lmx->makeRequest("listarEditoriales", $params, false);
      if ($request->isValid()) {
        //error_log(json_encode(($request->header["registros"])));
        $editoriales['encabezado'] = $request->header;
        $editoriales['cuerpo'] = $request->body;
      }

      return $editoriales;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Editorial Search: " . $e->getMessage() );
      return false;
    }
  }

  /*-------------------------- Ivan ----------------------------------*/
  /**
  * 
  * likeComment Insert a new like for a comment
  *
  * @param int $comment Id Comment
  * @param int $userfront_id user id
  * @return boolean
  */   
  public function likeComment($userfront_id,$comment){
      if($insert_like = DB::insert('pi_like', array('resource', 'resource_id','created_on', 'updated_on', 'userfront_id'))->values(array('comment', $comment,date('Y-m-d H:i:s'), date('Y-m-d H:i:s'), $userfront_id))->execute())
        return true; 
      else 
        return false;
  }
  /**
  * 
  * unlikeComment Remove a like for a comment
  *
  * @param int $comment Id Comment
  * @param int $userfront_id user id
  * @return boolean
  */   
  public function unlikeComment($userfront_id,$comment){
    if($unlike = DB::query(Database::DELETE,"DELETE FROM pi_like WHERE userfront_id='$userfront_id' AND resource_id='$comment' AND resource='comment' ")->execute())
      return true;
    else  
      return false;
  }
  /**
  * 
  * likeList Insert a new like for a list
  *
  * @param int $list Id list
  * @param int $userfront_id user id
  * @return boolean
  */   
  public function likeList($userfront_id,$list){
      if($insert_like = DB::insert('pi_like', array('resource', 'resource_id','created_on', 'updated_on', 'userfront_id'))->values(array('list', $list,date('Y-m-d H:i:s'), date('Y-m-d H:i:s'), $userfront_id))->execute())
      {

        //se actualiza campo likes en la tabla list
        $totalLikes = 0;
        $query = DB::select()
              ->from('pi_like')
              ->where('resource', '=', 'list')
              ->and_where( 'resource_id', '=', $list )
              ->execute()->as_array();

        //se obtiene total de likes
        $totalLikes = count($query);

        //se hace un update a la tabla pi_list campo likes
        $updateLikes = DB::update('pi_list')
            ->set( array('likes' => $totalLikes ) )
            ->where('id', '=', $list)
            ->execute();
        //Log::instance()->add(Log::NOTICE, 'TOTAL LIKES '.$totalLikes);

        return true; 
      }
        
      else 
        return false;
  }
  /**
  * 
  * unlikeList Delete a new like for a list
  *
  * @param int $list Id list
  * @param int $userfront_id user id
  * @return boolean
  */     
  public function unlikeList($userfront_id,$list){
    if($unlike = DB::query(Database::DELETE,"DELETE FROM pi_like WHERE userfront_id='$userfront_id' AND resource_id='$list' AND resource='list' ")->execute())
        return true; 
    else 
      return false;
  }

  /**
   * Creates the default lists given the user_id, if the user already has the lists
   * then they won't be created.
   * @param  int $userfront_id user id
   * @return boolean true if everything was ok, false if there was a problem
   * creating the list
   */
  public function defaultList($userfront_id) {
    if ( !$this->get_listid_by_user_slug($userfront_id, self::MI_BIBLIOTECA_SLUG) ) {
      if( !$this->create_list('Mi biblioteca',$userfront_id,'','','classic',"A",1,'public','', '','')) {
        return false;
      }
    }
    if ( !$this->get_listid_by_user_slug($userfront_id, self::LO_ESTOY_LEYENDO_SLUG) ) {
      if( !$this->create_list('Lo estoy leyendo',$userfront_id,'','','classic',"A",1,'public','', '','')) {
        return false;
      }
    }
    if ( !$this->get_listid_by_user_slug($userfront_id, self::YA_LO_LEI_SLUG) ) {
      if( !$this->create_list('Ya lo leí',$userfront_id,'','','classic',"A",1,'public','', '','')) {
        return false;
      }
    }
    if ( !$this->get_listid_by_user_slug($userfront_id, self::LO_QUIERO_LEER_SLUG) ) {
      if( !$this->create_list('Lo quiero leer',$userfront_id,'','','classic',"A",1,'public','', '','')) {
        return false;
      }
    }
    return true;
  }
   /**
   * 
   * getUserListLikes Returns list of lists that the user gives a like
   *
   * @param int $userfront_id user id
   * @return array list of lists
   */   
  public function getUserListLikes($userfront_id)
  {
    $like=DB::query(Database::SELECT,"SELECT resource_id AS list_like FROM pi_like WHERE userfront_id='$userfront_id' AND resource='list'")->execute();
    return $like;
  }  
   /**
   * 
   * getListLikes Returns Number of likes of list
   *
   * @param int $userfront_id user id
   * @param string $slug list slug
   * @return number of likes
   */   
  public function getListLikes($userfront_id,$slug)
  {
    $reviews= DB::query(Database::SELECT,"SELECT likes FROM pi_list_likes WHERE slug='$slug'")->execute();
    return $reviews;
  }
   /**
   * 
   * get_famous_lists Returns the lists with the maximum number of likes and
   * the lists that have:
   *   more than 5 books
   *   are not a default lists
   *
   * @param int $userfront_id user id
   * @param string $slug list slug
   * @return number of likes
   */   
  public function get_famous_lists($limit = 5) 
  {
    $lists= DB::query(Database::SELECT,"
    SELECT pl.id as list_id, name, slug, description, likes, userfront_id as user, ph.books_id, pl.likes, ph.size
    FROM pi_list as pl,
      (SELECT phb.pi_list_id,
      SUBSTRING_INDEX(GROUP_CONCAT(phb.book_id SEPARATOR '|'),'|',5) as books_id,
      count(phb.book_id) as size                     
      FROM pi_list_has_book as phb
      WHERE 1
      GROUP BY phb.pi_list_id
      HAVING count(phb.book_id) >= 5) as ph
    WHERE ph.pi_list_id = pl.id
    AND pl.type_list <> 'A'
    AND pl.type = 'classic'
    AND pl.privacy = 'public'
    ORDER BY pl.likes DESC
    LIMIT $limit")->execute();

    $lmx = new LibrosMexicoInterface();

    $data = null;
    foreach ($lists as $lk => $lv) {
      $books = array();
      if ($lv["books_id"] != null) {
        $request = $lmx->makeRequest("busquedaAvanzada", 
          array("id_libro" => $lv["books_id"], 
            "attr" => "id_libro, imagen")
          );
        if ($request->isValid()) {
          $books = $request->body;
          $exp = explode("|", $id_libro);
        }
      }  
      $listObject = $this->getListOnly($lv["list_id"]);

      $data[] = new list_obj($lv["list_id"],
        $lv["name"],
        $lv["slug"],
        $listObject["userfront_id"],
        $lv["description"],
        "0",
        "public",
        "",
        $lv["size"],
        "",
        $books);
    }

    return $data;    
  } 

  /**
   * Get the list by its Id, this list just contains the information
   * in the pi_list table
   * @param  int $list_id id of the list
   * @return array        if its null, then list wasn't found.
   */
  public function getListOnly($list_id) {
    $list = DB::query(Database::SELECT,"
      SELECT l.*
      FROM pi_list as l
      WHERE l.id = :list_id")
      ->parameters(array(":list_id" => $list_id))
        ->execute()
          ->as_array();
    if (count($list) == 0) {
      return null;
    }
    return $list[0];
  }
  /**
   * Get the list by its Id, this list just contains the information
  *
   * @param  int $list_id id of the list
   * @return array if its null, then list wasn't found.
   */
  public function getList($id){

    $lists= DB::query(Database::SELECT,"
            SELECT pl.id as list_id, name, slug, description,privacy, key_words,
            userfront_id as user, type, locked,
            GROUP_CONCAT(ph.book_id SEPARATOR '|') as books_id, 
            ph.order,
            count(ph.id) as size
            FROM pi_list as pl 
            LEFT JOIN pi_list_has_book as ph
            ON pl.id = ph.pi_list_id
            WHERE pl.id = '$id'")->execute();

    $lmx = new LibrosMexicoInterface();

    foreach ($lists as $lk => $lv) {
      $book_counter = 0;      
      $books = array();
      if ($lv["books_id"] != null) {
        $request = $lmx->makeRequest("busquedaAvanzada", 
          array("id_libro" => $lv["books_id"],
            "registros_por_pagina" => $lv["size"]), true);

        if ($request->isValid()) {
            $books = $request->body;
            $exp = explode("|", $id_libro);
            $book_counter = count($exp)+1;           
        }
      }  

      $data = new list_obj($lv["list_id"],
      $lv["name"],
      $lv["slug"],
      $lv["user"],
      $lv["description"],
      $lv["locked"],
      $lv["privacy"],
      $lv["type"],
      $book_counter,
      $lv["key_words"],
      $books);
    }

    return $data;    
  }

  /**
   * Retrieves the list given the user id and the slug
   * @param  int $userfront_id userfront id
   * @param  string $slug         slug of the list
   * @return list
   */
  public function get_by_userfront_id_and_slug($userfront_id, $slug) {
    $num_books=0;
    $lists= DB::query(Database::SELECT,"
            SELECT pl.id as list_id, name, slug, description, privacy, 
            userfront_id as user, type, locked, key_words,
            GROUP_CONCAT(ph.book_id SEPARATOR '|') as books_id, ph.order,
            count(ph.id) as size
            FROM pi_list as pl 
            LEFT JOIN pi_list_has_book as ph
            ON pl.id = ph.pi_list_id
            WHERE slug = :slug 
            AND pl.userfront_id = :userfront_id GROUP BY pl.id")
      ->parameters(array(":userfront_id" => $userfront_id,
        ":slug" => $slug))
        ->execute()
          ->as_array();
    
    $lmx = new LibrosMexicoInterface();

    foreach ($lists as $lk => $lv) {
    $book_counter = 0;
      $books = array();
      if ($lv["books_id"] != null) {
        $request = $lmx->makeRequest("busquedaAvanzada", array("id_libro" => $lv["books_id"], "registros_por_pagina" => $lv["size"]), true);
        if ($request->isValid()) {
          $books = $request->body;

          $exp = explode("|", $id_libro);
          $book_counter = count($exp)+1;           
        }
      }  
      $data[] = new list_obj($lv["list_id"],
        $lv["name"],
        $lv["slug"],
        $lv["user"],
        $lv["description"],
        $lv["locked"],
        $lv["privacy"],
        $lv["type"],
        $book_counter,
        $lv["key_words"],
        $books);
    }
    if (count($data) > 0) {
      return $data[0];
    }
    return null;     
  }
  /**
   * Get the list by its Id, this list just contains the information
   * in the pi_list table
   * @param  int $list_id id of the list
   * @return array        if its null, then list wasn't found.
   */
  public function getByHandle($handle)
  {
    $num_books=0;
    $lists= DB::query(Database::SELECT,"
            SELECT pl.id as list_id, name, slug, description,privacy, 
            userfront_id as user, type, locked, key_words,
            GROUP_CONCAT(ph.book_id SEPARATOR '|') as books_id, ph.order
            FROM pi_list as pl 
            LEFT JOIN pi_list_has_book as ph
            ON pl.id = ph.pi_list_id
            WHERE slug ='$handle' GROUP BY pl.id")->execute();

    $lmx = new LibrosMexicoInterface();

    foreach ($lists as $lk => $lv) {
    $book_counter = 0;      
      $books = array();
      if ($lv["books_id"] != null) {
        $request = $lmx->makeRequest("busquedaAvanzada", array("id_libro" => $lv["books_id"]), true);
        if ($request->isValid()) {
          $books = $request->body;

          $exp = explode("|", $id_libro);
          $book_counter = count($exp)+1;           
        }
      }  
      $data[] = new list_obj($lv["list_id"],
        $lv["name"],
        $lv["slug"],
        $lv["user"],
        $lv["description"],
        $lv["locked"],
        $lv["privacy"],
        $lv["type"],
        $book_counter,
        $lv["key_words"],
        $books);
    }

    return $data;       
  }

  /**
   * Get the list by its Id, this list just contains the information
   * in the pi_list table
   * @param  int $list_id id of the list
   * @return array        if its null, then list wasn't found.
   */
  public function getAllBooks($user)
  {
    $lists= DB::query(Database::SELECT,"
    SELECT COUNT(DISTINCT book_id) AS total_books
    FROM pi_list_has_book as lh
    LEFT JOIN pi_list as pl 
    ON lh.pi_list_id = pl.id
    WHERE pl.userfront_id = '$user'")->execute();

    return $lists[0]['total_books'];
  }

  /**
   * Get all  list by its Id
   * @param  int $list_id id of the list
   * @return array if its null, then list wasn't found.
   */  
  public function getByUser($user) {

    $data = array();

    $query = <<<EOT
    SELECT pl.id as list_id,
    name, 
    slug, 
    description,
    privacy, 
    userfront_id as user, 
    type,
    key_words,
    locked, 
    SUBSTRING_INDEX(GROUP_CONCAT(ph.book_id SEPARATOR '|'),'|',7) as books_id,
    count(ph.id) as size
    FROM pi_list as pl 
    LEFT JOIN pi_list_has_book as ph ON pl.id = ph.pi_list_id 
    WHERE userfront_id = {$user} 
    GROUP BY pl.id  Order By type_list, pl.created_on;
EOT;

    // Este query regresa la lista de libros, separada por una coma
    $lists = DB::query(Database::SELECT, $query)->execute()->as_array();

    $lmx = new LibrosMexicoInterface();

    foreach ($lists as $lk => $lv) {
      $book_counter = 0;      
      $books = array();
      if ($lv["books_id"] != null) {
        $id_libro = $lv["books_id"];
        $request = $lmx->makeRequest("busquedaAvanzada", array("id_libro" => $id_libro), true);
        if ($request->isValid()) {
          $books = $request->body;

          $exp = explode("|", $id_libro);
          $book_counter = count($exp);           
        }
      }  

      $data[] = new list_obj($lv["list_id"],
        $lv["name"],
        $lv["slug"],
        $lv["user"],
        $lv["description"],
        $lv["locked"],
        $lv["privacy"],
        $lv["type"],
        $lv["size"],
        $lv["key_words"],        
        $books);
    }
    return $data;
  } 

  /**
   * Retrieves a collection of lists given the id, the lists are formed
   * only by their id, name, slug and privacy settings. Use this function
   * only when you want to retrieve this data.
   * If only $userfront_id is stablished then you'll get all it's public-classic
   * lists.
   * @param  int $userfront_id  user id
   * @param  int $locked        if null (default) retrieves all lists, locked 
   * or not.
   * @param  string $privacy    if null then it is not filtered, available
   * values are: public   = public lists
   *             private  = private lists
   * @param  string $type       default is classic, if null then it is not 
   * filtered
   * @param  int $limit         limit the quantity of lists that you can retrieve
   * @return array              lists of the user.
   */
  public function getListInfoByUserId($userfront_id, 
    $locked = null,
    $privacy = "public",
    $type = 'classic',
    $limit = null) 
  {

    $query = <<<EOT
  SELECT pl.id as list_id, pl.name, pl.slug, pl.type_list, count(phb.id) as size, pl.userfront_id as userfront_id
  FROM pi_list pl LEFT JOIN pi_list_has_book phb ON
  pl.id = phb.pi_list_id
  WHERE userfront_id = :userfront_id
EOT;
    if ($locked !== null) {
      $query .= ' AND locked = :locked';
    }
    if ($privacy !== null) {
      $query .= ' AND privacy = :privacy';
    }
    if ($type !== null) {
      $query .= ' AND type = :type';
    }
    $query .= ' GROUP BY pl.id';
    $query .= ' ORDER BY pl.type_list, pl.created_on';
    if ($limit != null) {
      $query .= ' LIMIT :limit';
    }
    $params = array(
        ":userfront_id" => $userfront_id,
        ":locked" => $locked,
        ":privacy" => $privacy,
        ":type" => $type,
        ":limit" => $limit
      );
    $lists= DB::query(Database::SELECT,
      $query)
        ->parameters($params)
          ->execute()
            ->as_array();
    return $lists;
  }

  /**
   * Retrieves a collection of lists given the id, the lists are formed
   * only by their id, name, slug and privacy settings. Use this function
   * only when you want to retrieve this data.
   * If only $userfront_id is stablished then you'll get all it's public-classic
   * lists.
   * @param  int $userfront_id  user id
   * @param  string $slug       slug of the list
   * @return array              lists of the user.
   */
  public function getListInfoByUserIdAndSlug($userfront_id, $slug) 
  {
    $query = <<<EOT
  SELECT pl.id as list_id, pl.name, pl.slug, pl.type_list, count(phb.id) as size, pl.userfront_id as userfront_id
  FROM pi_list pl LEFT JOIN pi_list_has_book phb ON
  pl.id = phb.pi_list_id
  WHERE userfront_id = :userfront_id
  AND pl.slug = :slug
  GROUP BY pl.id
EOT;
    $params = array(
        ":userfront_id" => $userfront_id,
        ":slug" => $slug
      );
    $lists= DB::query(Database::SELECT,
      $query)
        ->parameters($params)
          ->execute()
            ->as_array();
    if (count($lists) > 0) {
      return $lists[0];
    }
    return null;
  }

  /**
   * Obtiene la información de la lista por id
   * @param  int $list_id id de la lista
   * @return mixed          descripción de la lista
   */
  public function getListInfo($list_id) {
    $query = <<<EOT
  SELECT pl.id as list_id, pl.name, pl.slug, pl.type_list, count(phb.id) as size, pl.userfront_id as userfront_id
  FROM pi_list pl LEFT JOIN pi_list_has_book phb ON
  pl.id = phb.pi_list_id
  WHERE pl.id = :list_id
  GROUP BY pl.id
EOT;
    $params = array(
        ":list_id" => $list_id
      );
    $lists= DB::query(Database::SELECT,
      $query)
        ->parameters($params)
          ->execute()
            ->as_array();
    if (count($lists) > 0) {
      return $lists[0];
    }
    return null;
  }

  /**
   * Get all  list by its Id only whit
   * @param  int $list_id id of the list
   * @return array if its null, then list wasn't found.
   */  
  public function getByUserSimple($user) 
  {
      $lists= DB::query(Database::SELECT,"
      SELECT pl.id as list_id, name, slug, GROUP_CONCAT(ph.book_id SEPARATOR '|') as books_id 
      FROM pi_list as pl 
      LEFT JOIN pi_list_has_book as ph
      ON pl.id = ph.pi_list_id
      WHERE userfront_id = '$user' AND type_list<>'C'
      GROUP BY pl.id
      Order By type_list, pl.created_on")->execute();

    foreach ($lists as $lk => $lv) {
    $book_counter = 0;      
      $books = array();
      if ($lv["books_id"] != null) {
        $id_libro = str_replace(",", "|", $lv["books_id"]);      
          $books = explode("|", $id_libro);
          $book_counter = count($books);           
    }  

      $data[] = new list_obj($lv["list_id"],
        $lv["name"],
        $lv["slug"],
        $lv["user"],
        $lv["description"],
        $lv["locked"],
        $lv["privacy"],
        $lv["type"],
        $book_counter,
        $lv["key_words"],        
        $books);
    }

    return $data;
  } 
  /**
   * Get all  list by its Id only whit
   * @param  int $list_id id of the list
   * @return array if its null, then list wasn't found.
   */  
  public function getByUserCustom($user) 
  {
      $lists= DB::query(Database::SELECT,"
      SELECT pl.id as list_id, name, slug, GROUP_CONCAT(ph.book_id SEPARATOR '|') as books_id 
      FROM pi_list as pl 
      LEFT JOIN pi_list_has_book as ph
      ON pl.id = ph.pi_list_id
      WHERE userfront_id = '$user' AND type_list<>'C' AND type_list <>'A'
      GROUP BY pl.id
      Order By type_list, pl.created_on")->execute();

    foreach ($lists as $lk => $lv) {
    $book_counter = 0;      
      $books = array();
      if ($lv["books_id"] != null) {
        $id_libro = str_replace(",", "|", $lv["books_id"]);      
          $books = explode("|", $id_libro);
          $book_counter = count($books);           
    }  

      $data[] = new list_obj($lv["list_id"],
        $lv["name"],
        $lv["slug"],
        $lv["user"],
        $lv["description"],
        $lv["locked"],
        $lv["privacy"],
        $lv["type"],
        $book_counter,
        $lv["key_words"],        
        $books);
    }

    return $data;
  } 

  //-------- end Ivan ----------------
  /**
   * 
   * String for url - Converts a string to be used as url
   *
   * @param string $string  
   * @return string $string
   */
  private function string_for_url($string)
  { 
    $string = trim($string);
    $string = str_replace( array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'), array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'), $string );
    $string = str_replace( array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'), array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'), $string );
    $string = str_replace( array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'), array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'), $string );
    $string = str_replace( array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'), array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'), $string );
    $string = str_replace( array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'), array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'), $string );
    $string = str_replace( array('ñ', 'Ñ', 'ç', 'Ç'), array('n', 'N', 'c', 'C',), $string );
    $string = str_replace(" ", "-", $string);
    //This part is responsible for eliminating any extraneous characters
    $string = str_replace( array("\\", "¨", "º", "~", "#", "@", "|", "!", "\"", "·", "$", "%", "&", "/", "(", ")", "?", "'", "¡", "¿", "[", "^", "`", "]", "+", "*", "}", "{", "¨", "´", ">“, “< ", ";", ",", ":", ".", " "), '', $string );
    $string = strtolower($string);

    return $string;
  }

  /**
   * 
   * Validate Name
   *
   * @param string $name  The list's Name
   * @return boolean: True if is valid
   */
  private function validate_name( &$name )
  {
    $name = strip_tags($name);
    if( strlen($name) <= 3 ) 
    {
      Log::instance()->add(Log::ERROR, "Model List: List Name too short (les than 3 characters) ");
      return false;
    }
    return true;
  }

  /**
   * 
   * Validate User
   *
   * @param int $userfront_id  User id
   * @return boolean: True if is valid id
   */
  private function validate_user( $userfront_id )
  {
    try
    {
      $exists_user = DB::select()->from('userfront')->where('id', '=', $userfront_id)->execute()->count();
      if( !$exists_user )
      {
        Log::instance()->add(Log::ERROR, "Model List: The user does not exist");
        return false;
      }
      return true;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Model List (function validate_user): " . $e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Validate Slug
   *
   * @param string $slug 
   * @param int $userfront_id  User id
   * @return string: The new slug
   */
  private function validate_slug( $slug, $userfront_id )
  {
    /* TODO: check algorithm */
    try
    {
      $exists_slug = DB::select()->from('pi_list')->where('slug', '=', $slug)->and_where('userfront_id', '=', $userfront_id )->execute()->count();
      if( $exists_slug )
      {
        do
        {
          $slug = $slug.'-1';
          $exists_slug = DB::select()->from('pi_list')->where('slug', '=', $slug)->execute()->count();
        }while( $exists_slug );
      }
      return $slug;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Model List (function validate_slug): " . $e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Validate Type
   *
   * @param string $type 
   * @return boolean
   */
  private function validate_type( $type )
  {
    if( !($type == 'dynamic' OR $type == 'classic') )
    {
      Log::instance()->add(Log::ERROR, "Model List: The Type value must be: dynamic or classic");
      return false;
    }
    return true;
  }

  /**
   * 
   * Validate Privacy
   *
   * @param string $privacy 
   * @return boolean
   */
  private function validate_privacy( $privacy )
  {
    if( !($privacy == 'public' OR $privacy == 'private') )
    {
      Log::instance()->add(Log::ERROR, "Model List: the Privacy value must be: public or private");
      return false;
    }
    return true;
  }

  /**
   * 
   * Validate Locked
   *
   * @param string $locked 
   * @return boolean
   */
  private function validate_locked( $locked )
  {
    if( !in_array($locked, array(true, false, 1, 0), true) )
    {
      Log::instance()->add(Log::ERROR, "Model List: the Locked value must be: true or false");
      return false;
    }
    return true;
  }

  /**
   * 
   * Validate List
   *
   * @param int $list_id 
   * @return boolean
   */
  private function validate_list( $list_id )
  {
    try
    {
      $exists_list = DB::select()->from('pi_list')->where('id', '=', $list_id)->execute()->count();
      if( !$exists_list )
      {
        Log::instance()->add(Log::ERROR, "Model List: The list does not exist");
        return false;
      }
      return true;
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Model List: " . $e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Exist Book in List
   *
   * @param int $list_id 
   * @param int $book_id 
   * @return boolean
   */
  private function exist_book_in_list( $list_id, $book_id )
  {
    try
    {
      $exist_book_in_list = DB::select()->from('pi_list_has_book')->where('pi_list_id', '=', $list_id)->and_where('book_id', '=', $book_id)->execute()->count();
      if( $exist_book_in_list > 0)
        return true;
      else{
        Log::instance()->add(Log::ERROR, "Model List: The book does not exist in the list");
        return false;
      }
    }
    catch( Exeption $e )
    {
      Log::instance()->add(Log::ERROR, "Model List: " . $e->getMessage() );
      return false;
    }
  }

  /**
   * 
   * Validate Book
   *
   * @param int $book_id 
   * @return Array of book data, False if the book does not exist
   */
  private function validate_book($book_id)
  {
    $lmx = new LibrosMexicoInterface();

    try{
      $request = $lmx->makeRequest("busquedaAvanzada", array('id_libro' => $book_id, 'attr' => 'id_libro, isbn' ), true);
      if ($request->isValid()) {
          $book= $request->body;
      }
      return $book;
    }
    catch( Exception $e ){
      Log::instance()->add(Log::ERROR, "Validate Book: " . $e->getMessage() );
      return false;
    }
  }

   public function getAllList($book_id){
    $lists= DB::query(Database::SELECT,"
            SELECT pl.id as list_id, name, slug, description,privacy, 
            userfront_id as user, type, locked, key_words,
            GROUP_CONCAT(ph.book_id SEPARATOR '|') as books_id, ph.order
            FROM pi_list as pl 
            LEFT JOIN pi_list_has_book as ph
            ON pl.id = ph.pi_list_id
            WHERE pl.type = 'classic' 
            AND pl.locked='0'
            AND pl.privacy= 'public' GROUP BY pl.id")->execute();
    $lmx = new LibrosMexicoInterface();

    foreach ($lists as $lk => $lv) {
    $book_counter = 0;      
      $books = array();
      if ($lv["books_id"] != null) {
        $request = $lmx->makeRequest("busquedaAvanzada", array("id_libro" => $lv["books_id"]), true);
        if ($request->isValid()) {
          $books = $request->body;

          $exp = explode("|", $id_libro);
          $book_counter = count($exp);           
        }
      }  

      $data[] = new list_obj($lv["list_id"],
        $lv["name"],
        $lv["slug"],
        $lv["user"],
        $lv["description"],
        $lv["locked"],
        $lv["privacy"],
        $lv["type"],
        $book_counter,
        $lv["key_words"],        
        $books);
    }
     return $data;
  }

  /**
   * Retrieves all the books that the user have, the result is in the following
   * cols.
   *   book_id = id of the book
   *   userfront_id = id of the user
   *   email = email of the user
   *   list_ids = ids of the lists where this book appears separated by '|'
   *   list_slugs = slugs of the lists where this book appears
   *   list_names = names of the lists where this book appears
   * @param  int $userfront_id id of the user
   * @param  int $limit limit of the page (default: 10)
   * @param  int $page  page that you viewing (default: 1)
   * @return array
   */
  public function getAllBooksInLists($userfront_id, $limit = 10, $page = 1) {
    $query = <<<EOT
SELECT SQL_CALC_FOUND_ROWS lhb.book_id book_id, 
u.id userfront_id, 
u.email email,
GROUP_CONCAT(l.id ORDER BY l.type_list SEPARATOR '|') as list_ids,
GROUP_CONCAT(l.slug ORDER BY l.type_list SEPARATOR '|') as list_slugs,
GROUP_CONCAT(l.name ORDER BY l.type_list SEPARATOR '|') as list_names
FROM pi_list_has_book lhb, userfront u, pi_list l
WHERE
  u.id = :userfront_id
  AND u.id = l.userfront_id
  AND l.id = lhb.pi_list_id
GROUP BY lhb.book_id
ORDER BY lhb.updated_on DESC
EOT;
    $limitstr = $limit;
    if ($limit && $page) {
      $offset = ($page - 1) * $limit;
      $limitstr = " LIMIT {$offset}, {$limit}";
    }
    $query .= $limitstr;
    $params = array(
      ":userfront_id" => $userfront_id
    );
    $lists= DB::query(Database::SELECT,
      $query)
        ->parameters($params)
          ->execute()
            ->as_array();
    $lists['found_rows']  = DB::query(Database::SELECT,
      "SELECT FOUND_ROWS() as found_rows")
      ->execute()
        ->as_array();
    return $lists;
  }

  /**
   * Retrieves all the books that the user have, the result is in the following
   * cols.
   *   book_id = id of the book
   *   userfront_id = id of the user
   *   email = email of the user
   *   list_ids = ids of the lists where this book appears separated by '|'
   *   list_slugs = slugs of the lists where this book appears
   *   list_names = names of the lists where this book appears
   * @param  int $userfront_id id of the user
   * @param  int $limit limit of the page (default: 10)
   * @param  int $page  page that you viewing (default: 1)
   * @return array
   */
  public function countAllBooksInLists($userfront_id) {

    $query = <<<EOT
SELECT count(*) as c
FROM pi_list_has_book lhb, userfront u, pi_list l
WHERE
  u.id = :userfront_id
  AND u.id = l.userfront_id
  AND l.id = lhb.pi_list_id
GROUP BY lhb.book_id
EOT;
    $params = array(
      ":userfront_id" => $userfront_id
    );
    $res= DB::query(Database::SELECT,
      $query)
        ->parameters($params)
          ->execute()
            ->as_array();
    error_log(json_encode($res,true));
    return $res[0]['c'];
  }

 /**
   * Get the last lists
   * @param  array  $order_by array on the form of array($param => "size desc"))
   * @param  integer $limit   size of page
   * @param  integer $page    page
   * @return list_obj
   */
  public function get_lists_by_page($search_string='', $order_by = null, $limit = 10, $page = 1) {

    $query = "
SELECT pl.id as list_id, name, slug, description, privacy, likes, userfront_id as user, ph.books_id, ph.size as size, pl.created_on as created_on, type
FROM pi_list as pl,
  (SELECT phb.pi_list_id,
  SUBSTRING_INDEX(GROUP_CONCAT(phb.book_id SEPARATOR '|'),'|',7) as books_id,
  count(phb.book_id) as size                     
  FROM pi_list_has_book as phb
  WHERE 1
  GROUP BY phb.pi_list_id) as ph
WHERE ph.pi_list_id = pl.id AND pl.privacy='public'
AND pl.type_list <>'A' AND size > 0";

if ($search_string!=='') {
  $query .= ' AND INSTR(`name`, "'.$search_string.'") > 0 ';
}

    
    // Stablishes the order by elements
    if (count($order_by) > 0) {
      $query .= ' ORDER BY '.implode(",", $order_by);
    }
    
    // Stablishes the limits
    $limitstr = $limit;
    if ($limit && $page) {
      $offset = ($page - 1) * $limit;
      $limitstr = " LIMIT ".$limit." OFFSET ".$offset;
    }
    $query .= $limitstr;

    $lists = DB::query(Database::SELECT, $query)->execute();

      $lmx = new LibrosMexicoInterface();

      foreach ($lists as $lk => $lv) {
        $books = array();
        if ($lv["books_id"] != null) {
          $request = $lmx->makeRequest("busquedaAvanzada", array("id_libro" => $lv["books_id"], "attr" => "id_libro,imagen"), true);
          if ($request->isValid()) {

            $books = $request->body;           
          }
        }  
        $data[] = new list_obj($lv["list_id"],
          $lv["name"],
          $lv["slug"],
          $lv["user"],
          $lv["description"],
          "0",
          "1",
          $lv["type"],
          $lv["size"],
          "",
          $books);
      }

      $retVal = array();
      $retVal['items'] = $data;
      $retVal['itemCount'] = count($data);

      return $retVal;   
  
  }

  /**
   * Get the last lists
   * @param  array  $order_by array on the form of array($param => "size desc"))
   * @param  integer $limit   size of page
   * @param  integer $page    page
   * @return list_obj
   */
  public function get_lists_by_page_count($search_string='', $order_by = null, $limit = 10, $page = 1) {

    $query = "
SELECT pl.id as list_id, name, slug, description, privacy, likes, userfront_id as user, ph.books_id, ph.size as size, pl.created_on as created_on, type
FROM pi_list as pl,
  (SELECT phb.pi_list_id,
  SUBSTRING_INDEX(GROUP_CONCAT(phb.book_id SEPARATOR '|'),'|',7) as books_id,
  count(phb.book_id) as size                     
  FROM pi_list_has_book as phb
  WHERE 1
  GROUP BY phb.pi_list_id) as ph
WHERE ph.pi_list_id = pl.id AND pl.privacy='public'
AND pl.type_list <>'A' AND size > 0";

if ($search_string!=='') {
  $query .= ' AND INSTR(`name`, "'.$search_string.'") > 0 ';
}

    
    // Stablishes the order by elements
    if (count($order_by) > 0) {
      $query .= ' ORDER BY '.implode(",", $order_by);
    }
    
    // Stablishes the limits
    /*$limitstr = $limit;
    if ($limit && $page) {
      $offset = ($page - 1) * $limit;
      $limitstr = " LIMIT ".$limit." OFFSET ".$offset;
    }
    $query .= $limitstr;*/

    $lists = DB::query(Database::SELECT, $query)->execute()->as_array();

    return count($lists);
  }
  
  /**
   * Get the last lists
   * @param  array  $order_by array on the form of array($param => "size desc"))
   * @param  integer $limit   size of page
   * @param  integer $page    page
   * @return list_obj
   */
  public function get_last_lists($order_by = null, $limit = 10, $page = 1) {

    $query = <<< EOT
SELECT pl.id as list_id, name, slug, description, likes, userfront_id as user, ph.books_id, ph.size as size, pl.created_on as created_on, type
FROM pi_list as pl,
  (SELECT phb.pi_list_id,
  SUBSTRING_INDEX(GROUP_CONCAT(phb.book_id SEPARATOR '|'),'|',7) as books_id,
  count(phb.book_id) as size                     
  FROM pi_list_has_book as phb
  WHERE 1
  GROUP BY phb.pi_list_id) as ph
WHERE ph.pi_list_id = pl.id
AND pl.type_list <>'A'
EOT;
    
    // Stablishes the order by elements
    if (count($order_by) > 0) {
      $query .= ' ORDER BY '.implode(",", $order_by);
    }
    
    // Stablishes the limits
    $limitstr = $limit;
    if ($limit && $page) {
      $offset = ($page - 1) * $limit;
      $limitstr = " LIMIT {$offset}, {$limit}";
    }
    $query .= $limitstr;

    $lists = DB::query(Database::SELECT, $query)->execute();

      $lmx = new LibrosMexicoInterface();

      foreach ($lists as $lk => $lv) {
        $books = array();
        if ($lv["books_id"] != null) {
          $request = $lmx->makeRequest("busquedaAvanzada", array("id_libro" => $lv["books_id"], "attr" => "id_libro,imagen"), true);
          if ($request->isValid()) {

            $books = $request->body;           
          }
        }  
        $data[] = new list_obj($lv["list_id"],
          $lv["name"],
          $lv["slug"],
          $lv["user"],
          $lv["description"],
          "0",
          "1",
          $lv["type"],
          $lv["size"],
          "",
          $books);
      }

      return $data;   
  }

  /**
   * Obtiene la cantidad de listas creadas por los usuarios
   */
  public function get_amount_of_lists_created_by_users() {
$query = <<<EOT
    SELECT count(*) c
    FROM pi_list 
    WHERE 1
    AND (type_list = 'B' OR type_list = 'C');
EOT;
    $result = DB::query(DATABASE::SELECT, $query)
      ->execute()
      ->current();
    $amount = $result['c'];
    return $amount;
  }

   /**
   * Obtiene la cantidad de listas creadas por el usuario
   */
  public function get_amount_of_lists_by_user($userfront_id) {
    $query = <<<EOT
      SELECT count(*) c
      FROM pi_list
      WHERE 1
      AND userfront_id = :userfront_id
EOT;
    $parameters = array(':userfront_id' => $userfront_id);
    $result = DB::query(DATABASE::SELECT, $query)
      ->parameters($parameters)
      ->execute()
      ->current();
    $amount = $result['c'];
    return $amount;
  }


  //se obtiene el total de likes de la lista
  public function countListLikes( $list_id )
  {
      $totalLikes = DB::select('id', 'likes')
            ->from('pi_list')
            ->where('id', '=', $list_id)
            ->execute()->as_array();

        $totalLikes = (int)$totalLikes[0]['likes'];

        return $totalLikes;
  }


  public function idOwnerList( $list_id )
  {
    $owner_id = DB::select( 'id', 'userfront_id')
            ->from('pi_list')
            ->where('id', '=', $list_id)
            ->execute()->as_array();

        $owner_id = $owner_id[0]['userfront_id'];

        return $owner_id;
  }





}