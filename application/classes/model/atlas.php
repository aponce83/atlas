<?php defined('SYSPATH') or die('No direct script access.');

class Model_Atlas extends Model {
	
	/**
	 * Regresa una página a partir de los valores la latitud mínima y máxima, así
	 * como los filtra según el tag dado.
	 * @param  float  $min_latitude  latitud mínima
	 * @param  float  $max_latitude  latitud máxima
	 * @param  float  $min_longitude longitud mínima
	 * @param  float  $max_longitude longitud máxima
	 * @param  array  $tag_filters   filtros del tag
	 * @param  integer  $page_offset   paginación
	 * @param  integer $page_size     tamaño de la página
	 * @param  reference $count_total si se envía entonces se coloca aquí la 
	 * información de cuantos hay en total
	 * @return array                 resultados
	 */
	public function getLocationPageByBounds ($min_latitude, 
		$max_latitude, 
		$min_longitude, 
		$max_longitude, 
		$tag_filters, 
		$page_offset, 
		$page_size = 24,
		&$count_total = null) 
	{
		$params = array();
		$params['min_latitude'] = $min_latitude;
		$params['max_latitude'] = $max_latitude;
		$params['min_longitude'] = $min_longitude;
		$params['max_longitude'] = $max_longitude;
		if ($page_offset == 0) {
			$page_offset = 1;
		} else {
			$page_offset++;
		}
		$params['page_number'] = $page_offset;
		$params['page_size'] = $page_size;

		$tag_filters_str = LM::array_to_string_separated($tag_filters);
		$params['tag_filters'] = $tag_filters_str;

		$lmx = new LibrosMexicoInterface();
		$request = $lmx->makeRequest('atlasBuscarUbicacion', $params);
		if ($request->isValid()) {
			$temp_locations = $request->body;
			foreach ($temp_locations as $temp_location) {
				$location = $this->getCustomLocationInfoByArrayFromPro($temp_location);
				$locations[] = $location;
			}
			$count_total = $request->header['registros'];
		}
		return $locations;
	}
	 
	
	/**
	 * Devuelve una pagina de resultados de busqueda de localidades
	 * @param  float  $min_latitude  latitud mínima
	 * @param  float  $max_latitude  latitud máxima
	 * @param  float  $min_longitude longitud mínima
	 * @param  float  $max_longitude longitud máxima
	 * @param  array  $tag_filters   filtros	
	 * @param  float $radius        radio
	 * @param  integer  $page_offset   paginación
	 * @param  integer $page_size     tamaño de la página
	 * @return array                 resultados
	 */
	public function getLocationPageByBoundsRadius ($min_latitude, 
		$max_latitude, 
		$min_longitude, 
		$max_longitude, 
		$tag_filters, 
		$radius, 
		$page_offset, 
		$page_size=24) 
	{

		$params = array();
		$params['min_latitude'] = $min_latitude;
		$params['max_latitude'] = $max_latitude;
		$params['min_longitude'] = $min_longitude;
		$params['max_longitude'] = $max_longitude;
		$params['radius'] = $radius;
		if ($page_offset == 0) {
			$page_offset = 1;
		} else {
			$page_offset++;
		}
		$params['page_number'] = $page_offset;
		$params['page_size'] = $page_size;

		$tag_filters_str = LM::array_to_string_separated($tag_filters);
		$params['tag_filters'] = $tag_filters_str;

		$lmx = new LibrosMexicoInterface();
		$request = $lmx->makeRequest('atlasBuscarCercanos', $params);
		if ($request->isValid()) {
			$temp_locations = $request->body;
			foreach ($temp_locations as $temp_location) {
				$location = $this->getCustomLocationInfoByArrayFromPro($temp_location);
				$locations[] = $location;
			}
			$count_total = $request->header['registros'];
		}
		return $locations;
	}

	/**
	 * Obtiene el total de ubicaciones almacenadas en el sistema
	 * @return integer total de ubicaciones
	 */
	public function countTotalLocations(){
		$result = 0;
		$lmx = new LibrosMexicoInterface();
		$res = $lmx->makeRawLocalRequest('atlas/get_total');
		if ($res["body"]["total"]) {
			$result = $res["body"]["total"];
			if ($result == null) {
				$result = 0;
			}
		}
    return $result;
	}

	/**
	 * Obtiene las ubicaciones a partir de la cantidad de comentarios que tengan
	 * @param  int $page_offset tamaño de la página
	 * @param  int $page_size   tamaño de la página
	 * @return array              ubicaciones
	 */
	public function getLocationPageByComments ( $page_offset, $page_size ) {
		$locations = array();

		$ids = DB::query(Database::SELECT, 
			'SELECT location.id FROM pi_location as location ORDER BY (SELECT COUNT(id) AS comments FROM pi_discuss WHERE resource_type = "location" AND resource_id = location.id) DESC LIMIT '.$page_size.' OFFSET '.($page_size*($page_offset-1)))
		  ->execute()
		  ->as_array();

		$vals = array();
		foreach ($ids as $id) {
			$vals[] = $id['id'];
		}
		$atlas_ids = LM::array_to_string_separated($vals);

		$params = array();
		$params['location_id'] = $atlas_ids;

		$lmx = new LibrosMexicoInterface();
		$request = $lmx->makeRequest('atlasUbicacion', $params);
		if ($request->isValid()) {
			$temp_locations = $request->body;
			foreach ($temp_locations as $temp_location) {
				$location = $this->getCustomLocationInfoByArrayFromPro($temp_location);
				$locations[] = $location;
			}
		}

		return $locations;
	}

	/**
	 * Obtiene las locaciones ordenadas por su rating
	 * @param  int $page_offset cantidad de ubicaciones
	 * @param  int $page_size   cantidad de ubicaciones por página
	 * @return array ubicaciones ordenadas
	 */
	public function getLocationPageByRating ($page_offset, $page_size) {

		$locations = array();

		$ids = DB::select('id')
		  ->from('pi_location')
		  ->order_by('rating', 'DESC')
		  ->limit($page_size)
		  ->offset($page_offset * $page_size)
		  ->execute()
		  ->as_array();
		$vals = array();
		foreach ($ids as $id) {
			$vals[] = $id['id'];
		}

		$atlas_ids = LM::array_to_string_separated($vals);

		$params = array();
		$params['location_id'] = $atlas_ids;

		$lmx = new LibrosMexicoInterface();
		$request = $lmx->makeRequest('atlasUbicacion', $params);
		if ($request->isValid()) {
			$temp_locations = $request->body;
			foreach ($temp_locations as $temp_location) {
				$location = $this->getCustomLocationInfoByArrayFromPro($temp_location);
				$locations[] = $location;
			}
		}

		return $locations;
	}

	/**
	 * Obtiene las discusiones que el usuario le ha dado like
	 * @param  integer $discussion id de la discusión
	 * @param  integer $user       id del usuario
	 * @return boolean
	 */
	public function discussionLikedByUser ($discussion, $user) {
		$results = DB::select()
		  ->from('pi_like')
		  ->where('userfront_id', '=', $user)
		  ->and_where('resource','=','discuss')
		  ->and_where('resource_id','=',$discussion)
		  ->execute()
		  ->as_array();
		$liked = count($results) == 0 ? false : true;
		return $liked;
	}

	/**
	 * El usuario le da like a una discusión
	 * @param  integer $user       id del usuario
	 * @param  integer $discussion id de la discusión
	 * @return id
	 */
	public function likeDiscussion ($user, $discussion) {
		$results = DB::select()
		  ->from('pi_like')
		  ->where('userfront_id', '=', $user)
		  ->and_where('resource','=','discuss')
		  ->and_where('resource_id','=',$discussion)
		  ->execute()
		  ->as_array();
		if (count($results)==0) {
			$query = DB::insert('pi_like', 
				array('resource','resource_id','userfront_id'))
			  ->values(
			  	array('discuss', $discussion, $user));
			$result = $query->execute();
			if ($result) {
				$update = 'UPDATE pi_discuss set likes = (SELECT count(id) FROM pi_like WHERE resource="discuss" AND resource_id='.$discussion.') where id = '.$discussion.';';
				$result = DB::query( Database::UPDATE,$update)->execute();
			}
      
			$results = DB::select()
			  ->from('pi_like')
			  ->where('userfront_id', '=', $user)
			  ->and_where('resource','=','discuss')
			  ->and_where('resource_id','=',$discussion)
			  ->execute()
			  ->as_array();
		}
		return $results[0]['id'];
	}

	/**
	 * Le quita el like a la discusión
	 * @param  integer $user       id del usuario
	 * @param  integer $discussion id de la discusión
	 * @return boolean
	 */
	public function dislikeDiscussion ($user, $discussion) {
		$result = DB::delete('pi_like')
		 ->where('userfront_id', '=', $user)
		 ->and_where('resource','=','discuss')
		 ->and_where('resource_id','=',$discussion)
		 ->execute();
		if ($result) {
			$update = 'UPDATE pi_discuss set likes = (SELECT count(id) FROM pi_like WHERE resource="discuss" AND resource_id='.$discussion.') where id = '.$discussion.';';
			$result = DB::query( Database::UPDATE,$update)->execute();
		}
	}

	/**
	 * Obtiene la cantidad de likes de una discusión
	 * @param  integer $id de la discusión
	 * @return integer     cantidad de likes
	 */
	function getLikesDiscussion ($id) {
		$results = DB::select()
		  ->from('pi_like')
		  ->where('resource','=','discuss')
		  ->and_where('resource_id','=',$id)
		  ->execute()
		  ->as_array();

		return count($results);
	}

	/**
	 * Obtiene si el comentario que le ha gustado a un usuario
	 * @param  integer $discussion id de la discusión
	 * @param  integer $user       usuario de la discusión
	 * @return boolean             indica si le gusto o no al usuario
	 */
	public function commentLikedByUser ($discussion, $user) 
	{
		$results = DB::select()
		  ->from('pi_like')
		  ->where('userfront_id', '=', $user)
		  ->and_where('resource','=','comment')
		  ->and_where('resource_id','=',$discussion)
		  ->execute()
		  ->as_array();

		$liked = count($results) == 0 ? false : true;
		
		return $liked;
	}

	/**
	 * Le da like al comentario
	 * @param  integer $user      id del usuario
	 * @param  integer $discussion id de la descripción
	 * @return integer id del comentario
	 */
	public function likeComment ($user, $discussion) {
		$results = DB::select()
		  ->from('pi_like')
		  ->where('userfront_id', '=', $user)
		  ->and_where('resource','=','comment')
		  ->and_where('resource_id','=',$discussion)
		  ->execute()
		  ->as_array();
		if (count($results)==0) {
			$query = DB::insert('pi_like', 
				array('resource','resource_id','userfront_id'))
			  ->values(array('comment',$discussion,$user));
			$result = $query->execute();
			if ($result) {
				$update = 'UPDATE pi_comment set likes = (SELECT count(id) FROM pi_like WHERE resource="comment" AND resource_id='.$discussion.') where id = '.$discussion.';';  
				$result = DB::query( Database::UPDATE,$update)->execute();
			}
			$results = DB::select()
			  ->from('pi_like')
			  ->where('userfront_id', '=', $user)
			  ->and_where('resource','=','comment')
			  ->and_where('resource_id','=',$discussion)
			  ->execute()
			  ->as_array();
		}
		return $results[0]['id'];
	}

	/**
	 * Le quita el like a una discusión
	 * @param  integer $user       id del usuario
	 * @param  intege $discussion id de la discusión
	 */
	public function dislikeComment ($user,$discussion) {
		$result = DB::delete('pi_like')
      ->where('userfront_id', '=', $user)
      ->and_where('resource','=','comment')
      ->and_where('resource_id','=',$discussion)
      ->execute();
		if ($result) {
			$update = 'UPDATE pi_comment set likes = (SELECT count(id) FROM pi_like WHERE resource="comment" AND resource_id='.$discussion.') where id = '.$discussion.';';  
			$result = DB::query( Database::UPDATE,$update)->execute();
		}
	}

	/**
	 * Obtiene la cantidad de likes de un comentario
	 * @param  integer $id [description]
	 */
	function getLikesComment ($id) {
		$results = DB::select()
		  ->from('pi_like')
		  ->where('resource','=','comment')
		  ->and_where('resource_id','=',$id)
		  ->execute()
		  ->as_array();
		$amount = count($results);
		return $amount;
	}

	/**
	 * Realiza el like a una ubicación
	 * @param  integer $user     id del usuario
	 * @param  integer $location id de la locación
	 * @return integer id del like
	 */
	public function likeLocation ($user, $location) {
		$results = DB::select()
		  ->from('pi_like')
		  ->where('userfront_id', '=', $user)
		  ->and_where('resource','=','location')
		  ->and_where('resource_id','=',$location)
		  ->execute()
		  ->as_array();
		if (count($results)==0) {
			$query = DB::insert('pi_like', 
				array('resource','resource_id','userfront_id'))
			  ->values(array('location', $location, $user));
			$result = $query->execute();
			$results = DB::select()
			  ->from('pi_like')
			  ->where('userfront_id', '=', $user)
			  ->and_where('resource','=','location')
			  ->and_where('resource_id','=',$location)
			  ->execute()
			  ->as_array();
		}
		return $results[0]['id'];
	}

	/**
	 * Quita el like a una ubicación
	 * @param  integer $user     id del usuario
	 * @param  integer $location id de la locación
	 * @return integer id del like
	 */
	public function dislikeLocation ($user,$location) {
		DB::delete('pi_like')
			->where('userfront_id', '=', $user)
			->and_where('resource','=','location')
			->and_where('resource_id','=',$location)
			->execute();
	}

	/**
	 * Realiza la búsqueda de una ubicación por su id
	 * @param  int $id id de la ubicación
	 * @return custom_location     información de la ubicación
	 */
	public function getLocationPageById ($id) {
		$response = null;
		$lmx = new LibrosMexicoInterface();
		$request = $lmx->makeRequest("atlasUbicacion", array("location_id" => $id));
		if ($request->isValid()) {
			$location = $request->body;
			if (count($location) != 0) {
				$response = $this->getCustomLocationInfoByArrayFromPro($location[0]);	
			}
		}
		return $response;
	}

	/**
	 * Obtiene la información de una locación según los datos obtenidos desde pro
	 * @param  array $location locación
	 * @return CustomLocationInfo result
	 */
	public function getCustomLocationInfoByArrayFromPro($location) {
		$custom_tags = array();
		foreach ($location['tags'] as $k => $v) {
			$tag = new custom_taginfo($k['id'], $v['name'], $v['slug']);
			$custom_tags[] = $tag;
		}

		$custom_schedules = array();
		foreach ($location['schedules'] as $i => $schedule) {
			$times = array();
			foreach ($schedule['times'] as $j => $time) {
				$timeInfo = new custom_timeinfo($j['id'],
					$time['begin_time'],
					$time['end_time']);
				$times[] = $timeInfo;
			}
			$scheduleInfo = new custom_scheduleinfo($i['id'],
				$schedule['day'],
				$schedule['location_id'],
				$times);
			$custom_schedules[] = $scheduleInfo;
		}

		$rating = $this->getRating($location['id']);
		$location['evals'] = $rating['evals'];
		$location['rating'] = $rating['rating'];

		$amount_of_likes = $this->getLikes($location['id']);
		$amount_of_comments = $this->getComments($location['id']);

		$response = new custom_locationinfo($location['id'], 
			$location['name'], 
			$location['description'], 
			$location['street'], 
			$location['int_number'], 
			$location['ext_number'], 
			$location['town'], 
			$location['municipal_office'], 
			$location['city'], 
			$location['state'], 
			$location['zip'], 
			$location['country'], 
			$location['latitude'], 
			$location['longitude'], 
			$location['phone'], 
			$location['email'], 
			$location['image'],	
			$location['webpage'], 
			$location['facebook'], 
			$location['twitter'], 
			$location['contact'], 
			$location['attributes'], 
			$location['rating'], 
			$location['meta'], 
			$location['slug'], 
			$location['owner_id'], 
			$location['creator_id'], 
			$location['created_on'], 
			$location['updated_on'], 
			$custom_tagInfos, 
			$custom_schedules,
			$location['evals'],
			$location['amount_of_likes'],
			$location['amount_of_comments'],
			$location['raw_distance'],
			$location['distance']);

		return $response;
	}

	/**
	 * Transforma un arreglo de location en un objeto custom_locationinfo
	 * @param  array $location información de la ubicación
	 * @return custom_locationinfo           información de la ubicación en el
	 * formato solicitado
	 */
	public function getCustomLocationInfoByArray($location) {
		$custom_tagInfos = array();
		foreach($location['tags'] as $tag) {
			$tagInfo = new custom_taginfo($tag['id'], $tag['name'], $tag['slug']);
			$custom_tagInfos[] = $tagInfo;
		}

		$custom_schedules = array();
		foreach ($location['schedules'] as $schedule) {

			$times = array();
			foreach ($schedule['times'] as $time) {
				$timeInfo = new custom_timeinfo($time['id'],
					$time['begin_time'],
					$time['end_time']);
				$times[] = $timeInfo;
			}

			$scheduleInfo = new custom_scheduleinfo($schedule['id'],
				$schedule['day'],
				$schedule['location_id'],
				$times);

			$custom_schedules[] = $scheduleInfo;
		}

		$rating = $this->getRating($location['id']);
		$location['evals'] = $rating['evals'];
		$location['rating'] = $rating['rating'];

		$amount_of_likes = $this->getLikes($location['id']);
		$amount_of_comments = $this->getComments($location['id']);

		$response = new custom_locationinfo($location['id'], 
			$location['name'], 
			$location['description'], 
			$location['street'], 
			$location['int_number'], 
			$location['ext_number'], 
			$location['town'], 
			$location['municipal_office'], 
			$location['city'], 
			$location['state'], 
			$location['zip'], 
			$location['country'], 
			$location['latitude'], 
			$location['longitude'], 
			$location['phone'], 
			$location['email'], 
			$location['image'],	
			$location['webpage'], 
			$location['facebook'], 
			$location['twitter'], 
			$location['contact'], 
			$location['attributes'], 
			$location['rating'], 
			$location['meta'], 
			$location['slug'], 
			$location['owner_id'], 
			$location['creator_id'], 
			$location['created_on'], 
			$location['updated_on'], 
			$custom_tagInfos, 
			$custom_schedules,
			$location['evals'],
			$location['amount_of_likes'],
			$location['amount_of_comments'],
			$location['raw_distance'],
			$location['distance']);

		return $response;
	}

	/**
	 * Cuenta el numero total de ubicaciones
	 * @param  float $min_latitude  latitud mínima
	 * @param  float $max_latitude  latitud máxima
	 * @param  float $min_longitude longitud mínima
	 * @param  float $max_longitude longitud máxima
	 * @param  array $tag_filters   filtros
	 * @return integer cantidad
	 */
	public function countLocationsByCoords ($min_latitude, 
		$max_latitude, 
		$min_longitude, 
		$max_longitude, 
		$tag_filters) 
	{
		$params = array();
		$params['min_latitude'] = $min_latitude;
		$params['max_latitude'] = $max_latitude;
		$params['min_longitude'] = $min_longitude;
		$params['max_longitude'] = $max_longitude;

		$tag_filters_str = LM::array_to_string_separated($tag_filters);
		$params['tag_filters'] = $tag_filters_str;

		$count = 0;
		$lmx = new LibrosMexicoInterface();
		$res = $lmx->makeRawLocalRequest("atlas/count_locations_by_coords", $params);
		if ($res["body"]["count"]) {
			$count = $res["body"]["count"];
			if ($count === null) {
				$count = 0;
			}
		}
		return $count;
	}

	/**
	 * Funcion que cuenta el numero total de lugares según el radio
	 * @param  float $min_latitude  latitud mínima
	 * @param  float $max_latitude  latitud máxima
	 * @param  float $min_longitude longitud mínima
	 * @param  float $max_longitude longitud máxima
	 * @param  array $tag_filters   filtros
	 * @param  float $radius   radio
	 * @return integer cantidad
	 */
	public function countLocationsByCoordsRadius ($min_latitude, 
		$max_latitude, 
		$min_longitude, 
		$max_longitude, 
		$tag_filters,
		$radius) 
	{
		$params = array();
		$params['min_latitude'] = $min_latitude;
		$params['max_latitude'] = $max_latitude;
		$params['min_longitude'] = $min_longitude;
		$params['max_longitude'] = $max_longitude;
		$params['radius'] = $radius;

		$tag_filters_str = LM::array_to_string_separated($tag_filters);
		$params['tag_filters'] = $tag_filters_str;

		$count = null;
		$lmx = new LibrosMexicoInterface();
		$res = $lmx->makeRawLocalRequest("atlas/count_locations_by_coords_radius", $params);
		if ($res["body"]["count"]) {
			$count = $res["body"]["count"];
		}
		return $count;
	}

	/**
	 * Determina si el local esta abierto o cerrado
	 * @param  integer  $location lugar
	 * @param  integer  $day      dia
	 * @return boolean           abierto o cerrado
	 */
	public function isLocationOpen ($location, $day) {
		$open = false;
		$lmx = new LibrosMexicoInterface();
		$res = $lmx->makeRawLocalRequest("atlas/is_location_open/{$location}/{$day}");
		if ($res["body"]["open"]) {
			$open = $res["body"]["open"];
			$open = (bool)$open;
		}
		return $open;
	}

	/**
	 * Obtiene los tags dentro del sistema
	 * @return array tags
	 */
	public function getTags () {
		$lmx = new LibrosMexicoInterface();
		$res = $lmx->makeRawLocalRequest("atlas/tags");
		$tags = $res['body']['tags'];
		return $tags;
	}

	/**
	 * Obtengo el rating de un lugar
	 * @param  int $id id del lugar
	 * @return array     cantidad de evaluaciones y promedio
	 */
	public function getRating ($id){
		$results = DB::select()
		  ->from('pi_rating')
		  ->where('resource','=','location')
		  ->and_where('resource_id','=',$id)
		  ->execute()
		  ->as_array();
		$average = 0;

		foreach ($results as $result) {
			$average += $result['rating'];
		}

		$evals = count($results);

		if ( $evals > 0 ) {
			$average = round($average/$evals); 
		}

		$retVal = array('evals' => $evals, 'rating' => $average);
		return $retVal;
	}

	/**
	 * Obtiene el rating del usuario
	 * @param  integer $user     id del usuario
	 * @param  integer $location id de la ubicación
	 * @return integer           rating
	 */
	function userRating ($user, $location) {
		$results = DB::select('rating')
		  ->from('pi_rating')
		  ->where('resource','=','location')
		  ->and_where('resource_id','=',$location)
		  ->and_where('userfront_id','=',$user)
		  ->execute()
		  ->as_array();
		return $results[0];
  }

  /**
   * Obtiene los likes de una ubicación
   * @param  int $id id de la ubicación
   * @return int     cantidad de likes
   */
	function getLikes ($id) {
		$results = DB::select()
		  ->from('pi_like')
		  ->where('resource','=','location')
		  ->and_where('resource_id','=',$id)
		  ->execute()
		  ->as_array();
		$amount = count($results);
		if ($amount == null ) {
			$amount = 0;
		}
		return $amount;
	}

	/**
	 * Indica si el usuario le ha dado like a la ubicación
	 * @param  int $user     id del usuario
	 * @param  int $location id de la ubicación
	 * @return bool
	 */
	function userLikesLocation ($user, $location) {
		$results = DB::select()
		  ->from('pi_like')
		  ->where('resource','=','location')
		  ->and_where('resource_id','=',$location)
		  ->and_where('userfront_id','=',$user)
		  ->execute()
		  ->as_array();
		return (count($results) > 0);
	}

	/**
	 * Obtiene la cantidad de comentarios para una ubicación
	 * @param  int $id id de la ubicación
	 * @return int     cantidad de comentarios
	 */
	function getComments ($id) {
		$results = DB::select()
			->from('pi_comment')
			->where('resource','=','location')
			->and_where('resource_id','=',$id)
			->execute()
			->as_array();

		$amount = count($results);
		return $amount;
	}

	/**
	 * Crea el slug
	 * @param  string $str       texto a convertir
	 * @param  string $delimiter delimitador (deprecated)
	 * @return string            texto limpio
	 */
	function toAscii($str, $delimiter='-') {
		$clean = LM::slugify($str);
		return $clean;
	}

	/**
	 * Obtiene los lugares cercanos
	 * @param  float $latitude latitud en decimales
	 * @param  float $longitude longitud en decimales
	 * @param  float $radius radio
	 * @param  float $page_size cantidad de resultados por página
	 * @param  float $page_number página
	 * @return array             locations
	 */
	public function getNearPlaces( $latitude, 
		$longitude, 
		$radius, 
		$page_size = 3,
		$page_number = 1) 
	{
		$locations = null;

		$params = array();
		$params['latitude'] = $latitude;
		$params['longitude'] = $longitude;
		$params['radius'] = $radius;
		$params['page_size'] = $page_size;
		$params['page_number'] = $page_number;

		$lmx = new LibrosMexicoInterface();
		$request = $lmx->makeRequest('atlasBuscarRadio', $params);

		if ($request->isValid()) {
			$temp_locations = $request->body;
			foreach ($temp_locations as $temp_location) {
				$location = $this->getCustomLocationInfoByArrayFromPro($temp_location);
				$locations[] = $location;
			}
		}

		return $locations;
	}

	/**
	 * Obtiene el id de los lugares cercanos
	 * @param  float $latitude  latitud
	 * @param  float $longitude longitud
	 * @return array            id y distancia de los lugares cerceanos
	 */
	function getNearPlacesIds( $latitude, $longitude, $radius = 1) {
		$results = null;

		$params = array();
		$params['latitude'] = $latitude;
		$params['longitude'] = $longitude;
		$params['radius'] = $radius;
		$params['page_size'] = 3;

		$response = null;
		$lmx = new LibrosMexicoInterface();
		$request = $lmx->makeRequest("atlasBuscarRadio", $params);
		error_log("near_places_ids");
		if ($request->isValid()) {
			$location = $request->body;
			
		}

		return $results;
	}

	/**
	 * Calculaa la distancia entre dos puntos basado en latitud y longitud
	 * @param  float $lat1 latitud
	 * @param  float $lon1 longitud
	 * @param  float $lat2 latitud
	 * @param  float $lon2 longitud
	 * @param  string $unit unidades
	 * @return float       distancia
	 */
	function distance($lat1, $lon1, $lat2, $lon2, $unit) {
	  $theta = $lon1 - $lon2;
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  $dist = acos($dist);
	  $dist = rad2deg($dist);
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);

	  if ($unit == "K") {
	    return ($miles * 1.609344);
	  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
      return $miles;
    }
	}

	/**
	 * Obtiene la distancia en crudo de una ubicación a la otra
	 * @param  float $centerLat latitud
	 * @param  float $centerLng longitud
	 * @param  float $lat       latitud
	 * @param  float $lng       longitud
	 * @return mixed
	 */
	function rawDistance ($centerLat, $centerLng, $lat, $lng) {
		$retVal = rad2deg(acos(sin(deg2rad($centerLat)) * sin(deg2rad($lat)) + cos(deg2rad($centerLat)) * cos(deg2rad($lat)) * cos(deg2rad(abs($centerLng - $lng))))) * 69.172;
		return $retVal;
	}

	/**
   * Se coloca la calificación al lugar
   *
   * @param int $book_id Book id
   * @param int $rank Rate of location
   * @param int $userfront_id user id
   * @return array (with all the locations from the list)
   */		
	public function rankLocation($location_id, $rank, $userfront_id)
	{
    if ($userfront_id == 0) {
		  $exists_list = DB::select()
		    ->from('pi_rating')
		    ->where('userfront_id', 'IS',  NULL)
		    ->and_where('resource_id','=',(int)$location_id)
		    ->and_where('resource','=','location')
		    ->execute()
		    ->count();  

		  if(!$exists_list 
		  	|| $exists_list == 0) 
		  {  
			  $insert_list = DB::insert('pi_rating', 
			  	array('rating', 'resource', 'resource_id','created_on', 'updated_on'))
			    ->values(array($rank,'location', $location_id,date('Y-m-d H:i:s'), date('Y-m-d H:i:s')))
			    ->execute();
			  $update_rating = $lists = DB::query(Database::UPDATE,
			  	"UPDATE pi_location SET rating='$rank' WHERE id='$location_id'")
			    ->execute();
			  return true;
		  } else {
		  	$rating = $this->getUserLocationRank($location_id, $userfront_id);
		  	if ($rank != 0) {
					$lists= DB::query(Database::UPDATE,"UPDATE pi_rating SET rating='$rating' WHERE resource_id='$location_id' AND resource='location' AND userfront_id IS NULL")->execute();	
				}
			
				$rating = round($this->getLocationRankPonderated($location_id, $userfront_id));
				$update_rating= DB::query(Database::UPDATE,
					"UPDATE pi_location SET rating='$rating' WHERE id='$location_id'")
			    ->execute();
				return true;
		  }
		}

		$exists_list = DB::select()
		  ->from('pi_rating')
		  ->where('userfront_id', '=', (int)$userfront_id)
		  ->and_where('resource_id','=',(int)$location_id)
		  ->and_where('resource','=','location')
		  ->execute()
		  ->count();    

		if(!$exists_list || 
			$exists_list == 0)
		{
		  $insert_list = DB::insert('pi_rating', 
		  	array('rating', 'resource', 'resource_id','created_on', 'updated_on', 'userfront_id'))
		    ->values(array($rank,'location', $location_id,date('Y-m-d H:i:s'), date('Y-m-d H:i:s'), $userfront_id))
		    ->execute();
		  $update_rating = $lists= DB::query(Database::UPDATE,
		  	"UPDATE pi_location SET rating='$rank' WHERE id='$location_id'")
		    ->execute();
		  return true;
		} else {
			if ($rank != 0) {
				$lists = DB::query(Database::UPDATE,
					"UPDATE pi_rating SET rating='$rank' WHERE resource_id='$location_id' AND resource='location' AND userfront_id='$userfront_id'")
				  ->execute();	
			} else {
				$lists = DB::query(Database::DELETE,
					"DELETE FROM pi_rating WHERE resource_id='$location_id' AND resource='location' AND userfront_id='$userfront_id'")
				  ->execute();
			}

			$rating = round($this->getLocationRankPonderated($location_id));
			$update_rating = DB::query(Database::UPDATE,
				"UPDATE pi_location SET rating='$rating' WHERE id='$location_id'")
			  ->execute();
			
			return true;
		}  
	}

	/**
   * 
   * getLocationRank Returns de AVG from Locations ranking
   *
   * @param int $location_id location id
   * @return Avg from Location
   */			
	public function getLocationRank($location_id)
	{
		$lists = DB::query(Database::SELECT,
			"SELECT AVG(rating) as rating FROM pi_rating WHERE resource_id='$location_id'")
		  ->execute();
		return (int)$lists[0]['rating']; 
	}

	/**
	 * Obtiene el ranking ponderado
	 * @param  integer $location_id id de la ubicación
	 * @return int              ranking
	 */
	public function getLocationRankPonderated($location_id)
	{
		$lists = DB::query(Database::SELECT,
			"SELECT AVG(rating) as rating FROM pi_rating WHERE resource_id='$location_id' AND userfront_id IS NOT NULL")
		  ->execute();
		$rank_users = (int)$lists[0]['rating']; 
		$lists = DB::query(Database::SELECT,
			"SELECT AVG(rating) as rating FROM pi_rating WHERE resource_id='$location_id' AND userfront_id IS NULL")
		  ->execute();
		$rank_announymous = (int)$lists[0]['rating']; 
		if($rank_users == 0) {
			$rank = round(($rank_announymous));
		} else {
			if ($rank_announymous == 0) {
				$rank = round(($rank_users));
			} else {
				$rank = round(($rank_users * 0.8)+($rank_announymous *0.2));
			}
		}
		
		return (int)$rank; 
	}
  /**
   * 
   * getUserLocationRank Returns rank of user location
   *
   * @param int $book_id Book id
   * @param int $userfront_id user id
   * @return int User rating
   */	
	public function getUserLocationRank ($location_id, $userfront_id) {
		$rating = 0;
		$query = "";
		if ($userfront_id != 0) {
			$query = "SELECT rating FROM pi_rating WHERE resource_id='$location_id' AND userfront_id='$userfront_id'";
		} else {
			$query = "SELECT AVG(rating) as rating FROM pi_rating_location WHERE resource_id='$location_id' AND userfront_id IS NULL";
		}

		$lists = DB::query(Database::SELECT,$query)->execute();
		$rating = $lists[0]['rating'];
		return $rating;
	}

	/**
   * 
   * Se coloca la calificación de un rubro al lugar
   *
   * @param int $book_id Book id
   * @param int $rank Rate of location
   * @param int $userfront_id user id
   * @return array (with all the locations from the list)
   */		
	public function rankLocationRubro($location_id,$rank,$userfront_id,$rubro,$clase)
	{
		$subject = 'rubro'.$rubro;
		if ($userfront_id == 0) {
		  $insert_list = DB::insert('pi_rating_location', 
		  	array('rating', 'subject', 'resource_id','created_on', 'updated_on', 'location_class'))
		    ->values(array($rank,
		    	$subject, 
		    	$location_id,
		    	date('Y-m-d H:i:s'), 
		    	date('Y-m-d H:i:s'), 
		    	$clase))
		    ->execute();
		  return true;
		}

		$exists_list = DB::select()
		  ->from('pi_rating_location')
		  ->where('userfront_id', '=', (int)$userfront_id)
		  ->and_where('resource_id','=',(int)$location_id)
		  ->and_where('subject','=',$subject)
		  ->and_where('location_class','=',$clase)
		  ->execute()
		  ->count();    
		
		if(!$exists_list || 
			$exists_list == 0)
		{
		  $insert_list = DB::insert('pi_rating_location', 
		  	array('rating', 'subject', 'resource_id','created_on', 'updated_on', 'userfront_id', 'location_class'))
		    ->values(array($rank,
		    	$subject, 
		    	$location_id,
		    	date('Y-m-d H:i:s'), 
		    	date('Y-m-d H:i:s'), 
		    	$userfront_id, 
		    	$clase))
		    ->execute();
		  return true;
		} else {
			if ($rank != 0) {
				$lists= DB::query(Database::UPDATE,
					"UPDATE pi_rating_location SET rating='$rank' WHERE resource_id='$location_id' AND subject='$subject' AND userfront_id='$userfront_id' AND location_class='$clase'")
				  ->execute();	
			} else {
				$lists= DB::query(Database::DELETE,
					"DELETE FROM pi_rating_location WHERE resource_id='$location_id' AND subject='$subject' AND userfront_id='$userfront_id' AND location_class='$clase'")
				  ->execute();
			}
			return true;
		}  
	}

	/**
   * 
   * getUserLocationRank Returns rank of user location in the subject
   *
   * @param int $book_id Book id
   * @param int $userfront_id user id
   * @return int User rating
   */	
	public function getUserLocationRankByRubro($location_id,$userfront_id,$rubro)
	{
		$rubron = 'rubro'.$rubro;
		$lists= DB::query(Database::SELECT,
			"SELECT rating FROM pi_rating_location WHERE resource_id='$location_id' AND userfront_id='$userfront_id' AND subject='$rubron'")
		  ->execute();
		return $lists[0]['rating']; 
	}

	/**
   * 
   * geLocationRankByRubro Returns rank of user location in the subject
   *
   * @param int $book_id Book id
   * @param int $userfront_id user id
   * @return int User rating
   */	
	public function getLocationRankByRubro($location_id,$rubro)
	{
		$rubron = 'rubro'.$rubro;
		$lists = DB::query(Database::SELECT,
			"SELECT AVG(rating) as rating FROM pi_rating_location WHERE resource_id='$location_id' AND subject='$rubron'")
		  ->execute();
		return (int)$lists[0]['rating']; 
	}

	/**
	 * Obtiene los rubros a evaluar a partir de su tipo de slug
	 * @param  string $slug tipo de slug
	 * @return array       slugs a utilizar
	 */
	public function getLocationSubjectsToQualifybySlug($slug) {
		$subjects = array('Espacio', 'Acervo', ' Atención');
		if ($slug === "sala-de-lectura") {
			$subjects = array('Acervo', 'Atención');
		} else if ($slug === "paralibro") {
			$subjects = array('Espacio', 'Acervo');
		}		
		return $subjects;
	}

	/**
	 * Obtiene las ubicaciones ya evaluadas
	 * @return array ubicaciones
	 */
	public function getLocationsAlreadyEvaluated()
	{
		$list = DB::query(Database::SELECT,
			"select * from pi_location where id in (select resource_id from pi_rating where resource='location')")
		  ->execute()
		  ->as_array();
		return $list;
	}

	/**
	 * Obtiene las ubicaciones apartir de una latitud y una longitud
	 * @param  float  $centerLat   latitud
	 * @param  float  $centerLng   longitud
	 * @param  string  $tag_filters filtros
	 * @param  float  $radius      radio
	 * @param  integer  $page_offset página
	 * @param  integer $page_size   tamaño de la página
	 * @return array               de Custom Location
	 */
  public function getLocationsByLatitudeAndLongitude ($centerLat, 
		$centerLng,
		$radius = 5000,
		$page_offset = 0, 
		$page_size = 100)
  {
		$lmx = new LibrosMexicoInterface();
		$res = $lmx->makeRawLocalRequest("atlas/locations");
		$tags = $res['body']['tags'];
		return $tags;
  }

	/************************************************************************************/
	/* 	Funciones creadas para realizar la migración al API, solo estás funciones       */
	/*  pueden tener acceso a la base de datos.																					*/
	/*  Estas funciones solamente pueden ser llamadas por el controlador de la API    	*/
	/************************************************************************************/

	/**
	 * Obtiene la información de una ubicación
	 * @param  int $location_id id de la ubicación
	 * @return array            datos de la ubicación
	 */
	public function getLocationAPI ($location_id) {
		$results = DB::select()
	    ->from('pi_location')
	    ->where('id', '=', $location_id)
	    ->execute()
	    ->as_array();

    if (count($results) != 1) {
    	return null;
    }
    $result = $results[0];
    $result['tags'] = $this->getTagsOfLocationAPI($location_id);
    $result['schedules'] = $this->getScheduleOfLocationAPI($location_id);
    return $result;
	}

	/**
	 * Obtiene los tags de una ubicación
	 * @param  int $location_id id de la ubicación
	 * @return array tags de la ubicación con valores de id, name y slug
	 */
	public function getTagsOfLocationAPI ($location_id) {
		$tags = array();
		$location_has_tags_ids = DB::select('location_tag_id')
		  ->from('pi_location_has_tag')
		  ->where('location_id', '=', $location_id)
		  ->execute()
		  ->as_array();
		if (count($location_has_tags_ids) > 0) {
			$location_tags = DB::select()
				->from('pi_location_tag')
				->where('id', 'IN', $location_has_tags_ids)
				->execute()
				->as_array();
			foreach ($location_tags as $tag) {
				$tags[] = array('id' => $tag['id'], 
					'name' => $tag['name'], 
					'slug' => $tag['slug']);
			}
		}
		return $tags;
	}

	/**
	 * Obtiene el horario de una ubicación
	 * @param  int $location_id id de la ubicación
	 * @return array horarios de la ubicación
	 */
	public function getScheduleOfLocationAPI ($location_id) {
		$schedules = array();
		$location_schedules = DB::select()
		  ->from('pi_location_schedule')
		  ->where('location_id', '=', $location_id)
		  ->execute()
		  ->as_array();
		if (count($location_schedules) > 0) {
			foreach ($location_schedules as $location_schedule) {
				$times = array();
				$location_times = DB::select()
				  ->from('pi_location_time')
				  ->where('id', '=', $location_schedule['location_time_id'])
				  ->execute()
				  ->as_array();
				foreach ($location_times as $location_time) {
					$times[] = array('id' => $location_time['id'],
						'begin_time' => $location_time['begin_time'],
						'end_time' => $location_time['end_time']);
				}
				$schedules[] = array('id' => $location_schedule['id'],
					'day' => $location_schedule['day'],
					'location_id' => $location_schedule['location_id'],
					'times' => $times);
			}
		}
		return $schedules;
	}

	/**
	 * Wrapper para el acceso a la base de datos
	 * @param  int $id id del lugar
	 * @return array     location
	 */
	public function getLocationPageByIdAPI($id) {
		$result = $this->getLocationAPI($id);
		return $result;
	}

	/**
	 * Obtiene la información de todos los ids de las ubicaciones enviadas
	 * @param  array $ids ids de las ubicaciones que se quieren consultar
	 * @return array      ubiaciones
	 */
	public function getLocationsAPI($ids) {
		$results = DB::select()
	    ->from('pi_location')
	    ->where('id', 'IN', $ids)
	    ->execute()
	    ->as_array();

	  foreach ($results as &$result) {
	  	$result['tags'] = $this->getTagsOfLocationAPI($location_id);
    	$result['schedules'] = $this->getScheduleOfLocationAPI($location_id);
	  }
	  unset($result);
    
    return $results;
	}

	public function getTagsAPI () {
		$results = DB::select()->from('pi_location_tag')->execute()->as_array();
		$retVal = array();
		foreach ($results as $result) {
			array_push($retVal,$result['name']);
		}
		return $retVal;
	}

	public function getLocationsByLatitudeAndLongitudeAPI ($centerLat, 
		$centerLng,
		$radius = 5000,
		$page_offset = 0, 
		$page_size = 100)
  {
  	error_log("Funcion getLocationsByLatitudeAndLongitude()");
  	$centerLat = (float)$centerLat;
  	$centerLng = (float)$centerLng;
		
		$results = DB::select(array('pi_location.id', 'id'),
			array('pi_location.name', 'name'), 
			array('pi_location.description', 'description'), 
			array('pi_location.street', 'street'), 
			array('pi_location.int_number', 'int_number'), 
			array('pi_location.ext_number', 'ext_number'), 
			array('pi_location.town', 'town'),
			array('pi_location.municipal_office', 'municipal_office'), 
			array('pi_location.city', 'city'), 
			array('pi_location.state', 'state'), 
			array('pi_location.zip', 'zip'), 
			array('pi_location.country', 'country'), 
			array('pi_location.latitude', 'latitude'), 
			array('pi_location.longitude', 'longitude'), 
			array('pi_location.phone', 'phone'), 
			array('pi_location.email', 'email'), 
			array('pi_location.image', 'image'), 
			array('pi_location.webpage', 'webpage'), 
			array('pi_location.facebook', 'facebook'), 
			array('pi_location.twitter', 'twitter'), 
			array('pi_location.contact', 'contact'), 
			array('pi_location.attributes', 'attributes'), 
			array('pi_location.rating', 'rating'),
			array('pi_location.meta', 'meta'), 
			array('pi_location.slug', 'slug'), 
			array('pi_location.reference_id', 'reference_id'), 
			array('pi_location.owner_id', 'owner_id'), 
			array('pi_location.creator_id', 'creator_id'), 
			array('pi_location.initial_space', 'initial_space'), 
			array('pi_location.state_inegi_key', 'state_inegi_key'), 
			array('pi_location.munic_inegi_key', 'munic_inegi_key'), 
			array('pi_location.consecutive', 'consecutive'), 
			array('pi_location.session_day', 'session_day'), 
			array('pi_location.session_schedule', 'session_schedule'), 
			array('pi_location.status', 'status'), 
			array('pi_location.location_type', 'location_type'), 
			array('pi_location.created_on', 'created_on'), 
			array('pi_location.updated_on', 'updated_on'))
				->from('pi_location')
				->where(DB::expr('degrees(acos(sin(radians('.$centerLat.')) * sin(radians(`latitude`)) + cos(radians('.$centerLat.')) * cos(radians(`latitude`)) * cos(radians(abs('.$centerLng.' - `longitude`))))) * 69.172'),'<=',$radius)
				->group_by('pi_location.name')
				->order_by(DB::expr('degrees(acos(sin(radians('.$centerLat.')) * sin(radians(`latitude`)) + cos(radians('.$centerLat.')) * cos(radians(`latitude`)) * cos(radians(abs('.$centerLng.' - `longitude`))))) * 69.172'),"ASC")
				->limit($page_size)
				->offset($page_offset * $page_size)
				->execute()
				->as_array();

		$locations = array();

		foreach ($results as $result) {
			$tagMatches = DB::select('location_tag_id')->from('pi_location_has_tag')->where('location_id','=',$result['id'])->execute()->as_array();
			$tags = array();

			if (count($tagMatches)>0) {
				$tagResults = DB::select()->from('pi_location_tag')->where('id','IN',$tagMatches)->execute()->as_array();				
				foreach ($tagResults as $tagResult) {
					array_push($tags, new custom_taginfo($tagResult['id'],$tagResult['name'],$tagResult['slug']));
				}
			}

			$scheduleMatches = DB::select()->from('pi_location_schedule')->where('location_id','=',$result['id'])->execute()->as_array();
			$schedules = array();

			if (count($scheduleMatches)>0) {
				foreach ($scheduleMatches as $scheduleMatch) { 
					$timeResults = DB::select()->from('pi_location_time')->where('id','=',$scheduleMatch['location_time_id'])->execute()->as_array();
					$times = array();
					foreach ($times as $time) {
						array_push($times,new custom_timeinfo($time['id'],$time['begin_time'],$time['end_time']));
					}

					array_push($schedules, new custom_scheduleinfo($scheduleMatch['id'],$scheduleMatch['day'],$scheduleMatch['location_id'],$times));
				}
			}

			$rating = $this->getRating($result['id']);

			$locations[] = new custom_locationinfo($result['id'], 
				$result['name'], 
				$result['description'], 
				$result['street'], 
				$result['int_number'], 
				$result['ext_number'], 
				$result['town'],
				$result['municipal_office'],
				$result['city'],
				$result['state'],
				$result['zip'],
				$result['country'],
				$result['latitude'],
				$result['longitude'],
				$result['phone'],
				$result['email'],
				$result['image'],
				$result['webpage'],
				$result['facebook'],
				$result['twitter'],
				$result['contact'], 
				$result['attributes'],
				$rating['rating'],
				$result['meta'],
				$result['slug'],
				$result['owner_id'],
				$result['creator_id'],
				$result['created_on'], 
				$result['updated_on'],
				$tags,
				$schedules,
				$rating['evals'],
				$this->getLikes($result['id']),
				$this->getComments($result['id']),
				$this->rawDistance($centerLat,$centerLng,(double)$result['latitude'],(double)$result['longitude']),
				$this->distance($centerLat,$centerLng,(double)$result['latitude'],(double)$result['longitude'],"K")
				);
		}
		return $locations;
	}

	/**
	 * Regresa una página a partir de los valores la latitud mínima y máxima, así
	 * como los filtra según el tag dado.
	 * @param  float  $min_latitude  [description]
	 * @param  float  $max_latitude  [description]
	 * @param  float  $min_longitude [description]
	 * @param  float  $max_longitude [description]
	 * @param  array  $tag_filters   [description]
	 * @param  integer  $page_offset   [description]
	 * @param  integer $page_size     [description]
	 * @return array                 resultados
	 */
	public function getLocationPageByBoundsAPI ($min_latitude, 
		$max_latitude, 
		$min_longitude, 
		$max_longitude, 
		$tag_filters, 
		$page_offset, 
		$page_size=24) 
	{
		$tag_ids = DB::select('id')
		  ->from('pi_location_tag')
		  ->where('name', 'IN', $tag_filters)
		  ->execute()
		  ->as_array();

		if (count($tag_ids)==0) {
			return array();
		}

		$centerLat = ($min_latitude + $max_latitude)/2;
		$centerLng = ($min_longitude + $max_longitude)/2;

		$results = DB::select(array('pi_location.id', 'id'),
			array('pi_location.name', 'name'), 
			array('pi_location.description', 'description'), 
			array('pi_location.street', 'street'), 
			array('pi_location.int_number', 'int_number'), 
			array('pi_location.ext_number', 'ext_number'), 
			array('pi_location.town', 'town'),
			array('pi_location.municipal_office', 'municipal_office'), 
			array('pi_location.city', 'city'), 
			array('pi_location.state', 'state'), 
			array('pi_location.zip', 'zip'), 
			array('pi_location.country', 'country'), 
			array('pi_location.latitude', 'latitude'), 
			array('pi_location.longitude', 'longitude'), 
			array('pi_location.phone', 'phone'), 
			array('pi_location.email', 'email'), 
			array('pi_location.image', 'image'), 
			array('pi_location.webpage', 'webpage'), 
			array('pi_location.facebook', 'facebook'), 
			array('pi_location.twitter', 'twitter'), 
			array('pi_location.contact', 'contact'), 
			array('pi_location.attributes', 'attributes'), 
			array('pi_location.rating', 'rating'),
			array('pi_location.meta', 'meta'), 
			array('pi_location.slug', 'slug'), 
			array('pi_location.reference_id', 'reference_id'), 
			array('pi_location.owner_id', 'owner_id'), 
			array('pi_location.creator_id', 'creator_id'), 
			array('pi_location.initial_space', 'initial_space'), 
			array('pi_location.state_inegi_key', 'state_inegi_key'), 
			array('pi_location.munic_inegi_key', 'munic_inegi_key'), 
			array('pi_location.consecutive', 'consecutive'), 
			array('pi_location.session_day', 'session_day'), 
			array('pi_location.session_schedule', 'session_schedule'), 
			array('pi_location.status', 'status'), 
			array('pi_location.location_type', 'location_type'), 
			array('pi_location.created_on', 'created_on'), 
			array('pi_location.updated_on', 'updated_on'), 
			array('pi_location_has_tag.location_tag_id', 'location_tag_id'),
			array('pi_location_has_tag.location_id', 'location_id'))
		    ->from('pi_location')
		    ->join('pi_location_has_tag')
		    ->on('pi_location.id', '=', 'pi_location_has_tag.location_id')
		    ->where('pi_location_has_tag.location_tag_id', 'IN', $tag_ids)
		    ->and_where('pi_location.latitude','<',$max_latitude)
		    ->and_where('pi_location.latitude','>',$min_latitude)
		    ->and_where('pi_location.longitude','>',$min_longitude)
		    ->and_where('pi_location.longitude','<',$max_longitude)
		    ->group_by('pi_location.name')
		    ->order_by(DB::expr('degrees(acos(sin(radians('.$centerLat.')) * sin(radians(`latitude`)) + cos(radians('.$centerLat.')) * cos(radians(`latitude`)) * cos(radians(abs('.$centerLng.' - `longitude`))))) * 69.172'),"ASC")
		    ->limit($page_size)
		    ->offset($page_offset*$page_size)
		    ->execute()
		    ->as_array();

		foreach ($results as &$result) {
	  	$result['tags'] = $this->getTagsOfLocationAPI($location_id);
    	$result['schedules'] = $this->getScheduleOfLocationAPI($location_id);
    	$result['raw_distance'] = $this->rawDistance($centerLat,
    		$centerLng,
    		(double)$result['latitude'],
    		(double)$result['longitude']);
    	$result['distance'] = $this->distance($centerLat,
    		$centerLng,
    		(double)$result['latitude'],
    		(double)$result['longitude'],
    		"K");
	  }
	  unset($result);

		return $results;
	}

	/**
	 * Obtiene la cantidad de ubicaciones dadas las coordenadas
	 * @param  [type] $min_latitude  [description]
	 * @param  [type] $max_latitude  [description]
	 * @param  [type] $min_longitude [description]
	 * @param  [type] $max_longitude [description]
	 * @param  [type] $tag_filters   [description]
	 * @return [type]                [description]
	 */
	public function countLocationsByCoordsAPI ($min_latitude, 
		$max_latitude, 
		$min_longitude, 
		$max_longitude, 
		$tag_filters) 
	{
		$tag_ids = DB::select('id')
		  ->from('pi_location_tag')
		  ->where('name', 'IN', $tag_filters)
		  ->execute()
		  ->as_array();

		if (count($tag_ids)==0) {
			return 0;
		}

		$results = DB::select('pi_location.id')
		  ->from('pi_location')
		  ->join('pi_location_has_tag')
		  ->on('pi_location.id', '=', 'pi_location_has_tag.location_id')
		  ->where('pi_location_has_tag.location_tag_id', 'IN', $tag_ids)
		  ->and_where('pi_location.latitude','<',$max_latitude)
		  ->and_where('pi_location.latitude','>',$min_latitude)
		  ->and_where('pi_location.longitude','>',$min_longitude)
		  ->and_where('pi_location.longitude','<',$max_longitude)
		  ->group_by('pi_location.name')
		  ->execute()
		  ->as_array();

		return count($results);
	}

	/**
	 * Devuelve una pagina de resultados de busqueda de localidades
	 * @param  [type]  $min_latitude  [description]
	 * @param  [type]  $max_latitude  [description]
	 * @param  [type]  $min_longitude [description]
	 * @param  [type]  $max_longitude [description]
	 * @param  [type]  $tag_filters   [description]
	 * @param  [type]  $radius        [description]
	 * @param  [type]  $page_offset   [description]
	 * @param  integer $page_size     [description]
	 * @return [type]                 [description]
	 */
	public function getLocationPageByBoundsRadiusAPI ($min_latitude, 
		$max_latitude, 
		$min_longitude, 
		$max_longitude, 
		$tag_filters, 
		$radius, 
		$page_offset, 
		$page_size=24)
	{
		$tag_ids = DB::select('id')
			->from('pi_location_tag')
			->where('name', 'IN', $tag_filters)
			->execute()
			->as_array();

		if (count($tag_ids)==0) {
			return array();
		}

		$centerLat = ($min_latitude + $max_latitude)/2;
		$centerLng = ($min_longitude + $max_longitude)/2;

		$results = DB::select(array('pi_location.id', 'id'),
			array('pi_location.name', 'name'), 
			array('pi_location.description', 'description'), 
			array('pi_location.street', 'street'), 
			array('pi_location.int_number', 'int_number'), 
			array('pi_location.ext_number', 'ext_number'), 
			array('pi_location.town', 'town'),
			array('pi_location.municipal_office', 'municipal_office'), 
			array('pi_location.city', 'city'), 
			array('pi_location.state', 'state'), 
			array('pi_location.zip', 'zip'), 
			array('pi_location.country', 'country'), 
			array('pi_location.latitude', 'latitude'), 
			array('pi_location.longitude', 'longitude'), 
			array('pi_location.phone', 'phone'), 
			array('pi_location.email', 'email'), 
			array('pi_location.image', 'image'), 
			array('pi_location.webpage', 'webpage'), 
			array('pi_location.facebook', 'facebook'), 
			array('pi_location.twitter', 'twitter'), 
			array('pi_location.contact', 'contact'), 
			array('pi_location.attributes', 'attributes'), 
			array('pi_location.rating', 'rating'),
			array('pi_location.meta', 'meta'), 
			array('pi_location.slug', 'slug'), 
			array('pi_location.reference_id', 'reference_id'), 
			array('pi_location.owner_id', 'owner_id'), 
			array('pi_location.creator_id', 'creator_id'), 
			array('pi_location.initial_space', 'initial_space'), 
			array('pi_location.state_inegi_key', 'state_inegi_key'), 
			array('pi_location.munic_inegi_key', 'munic_inegi_key'), 
			array('pi_location.consecutive', 'consecutive'), 
			array('pi_location.session_day', 'session_day'), 
			array('pi_location.session_schedule', 'session_schedule'), 
			array('pi_location.status', 'status'), 
			array('pi_location.location_type', 'location_type'), 
			array('pi_location.created_on', 'created_on'), 
			array('pi_location.updated_on', 'updated_on'), 
			array('pi_location_has_tag.location_tag_id', 'location_tag_id'),
			array('pi_location_has_tag.location_id', 'location_id'))
				->from('pi_location')
				->join('pi_location_has_tag')
				->on('pi_location.id', '=', 'pi_location_has_tag.location_id')
				->where('pi_location_has_tag.location_tag_id', 'IN', $tag_ids)
				->and_where('pi_location.latitude','<',$max_latitude)
				->and_where('pi_location.latitude','>',$min_latitude)
				->and_where('pi_location.longitude','>',$min_longitude)
				->and_where('pi_location.longitude','<',$max_longitude)
				->and_where(DB::expr('degrees(acos(sin(radians('.$centerLat.')) * sin(radians(`latitude`)) + cos(radians('.$centerLat.')) * cos(radians(`latitude`)) * cos(radians(abs('.$centerLng.' - `longitude`))))) * 69.172'),'<=',$radius)
				->group_by('pi_location.name')
				->order_by(DB::expr('degrees(acos(sin(radians('.$centerLat.')) * sin(radians(`latitude`)) + cos(radians('.$centerLat.')) * cos(radians(`latitude`)) * cos(radians(abs('.$centerLng.' - `longitude`))))) * 69.172'),"ASC")
				->limit($page_size)
				->offset($page_offset*$page_size)
				->execute()
				->as_array();

		foreach ($results as &$result) {
	  	$result['tags'] = $this->getTagsOfLocationAPI($location_id);
    	$result['schedules'] = $this->getScheduleOfLocationAPI($location_id);
    	$result['raw_distance'] = $this->rawDistance($centerLat,
    		$centerLng,
    		(double)$result['latitude'],
    		(double)$result['longitude']);
    	$result['distance'] = $this->distance($centerLat,
    		$centerLng,
    		(double)$result['latitude'],
    		(double)$result['longitude'],
    		"K");
	  }
	  unset($result);

		return $results;
	}

	/**
	 * Devuelve una pagina de resultados de busqueda de localidades
	 * @param  [type]  $min_latitude  [description]
	 * @param  [type]  $max_latitude  [description]
	 * @param  [type]  $min_longitude [description]
	 * @param  [type]  $max_longitude [description]
	 * @param  [type]  $tag_filters   [description]
	 * @param  [type]  $radius        [description]
	 * @return [type]                 [description]
	 */
	public function countLocationsByCoordsRadiusAPI ($min_latitude, 
		$max_latitude, 
		$min_longitude, 
		$max_longitude, 
		$tag_filters, 
		$radius)
	{
		$tag_ids = DB::select('id')
			->from('pi_location_tag')
			->where('name', 'IN', $tag_filters)
			->execute()
			->as_array();

		if (count($tag_ids)==0) {
			return array();
		}

		$centerLat = ($min_latitude + $max_latitude)/2;
		$centerLng = ($min_longitude + $max_longitude)/2;

		$results = DB::select('pi_location.id')
				->from('pi_location')
				->join('pi_location_has_tag')
				->on('pi_location.id', '=', 'pi_location_has_tag.location_id')
				->where('pi_location_has_tag.location_tag_id', 'IN', $tag_ids)
				->and_where('pi_location.latitude','<',$max_latitude)
				->and_where('pi_location.latitude','>',$min_latitude)
				->and_where('pi_location.longitude','>',$min_longitude)
				->and_where('pi_location.longitude','<',$max_longitude)
				->and_where(DB::expr('degrees(acos(sin(radians('.$centerLat.')) * sin(radians(`latitude`)) + cos(radians('.$centerLat.')) * cos(radians(`latitude`)) * cos(radians(abs('.$centerLng.' - `longitude`))))) * 69.172'),'<=',$radius)
				->group_by('pi_location.name')
				->execute()
				->as_array();

		return count($results);
	}

	/**
	 * Obtiene el total de ubicaciones almacenadas en el sistema
	 * @return integer total de ubicaciones
	 */
	public function countTotalLocationsAPI(){
		$totalLocations = 0;
    $queryRow = DB::select('id')
        ->from('pi_location')
        ->execute()
        ->as_array();
    $totalLocations = count($queryRow);
    return $totalLocations;
	}

	/**
	 * Determina si el local esta abierto o cerrado
	 * @param  integer  $location lugar
	 * @param  integer  $day      dia
	 * @return boolean           abierto o cerrado
	 */
	public function isLocationOpenAPI ($location, $day) {
		$timeMatches = DB::select('id')
		  ->from('pi_location_time')
		  ->where('begin_time','<=',DB::expr('CURTIME()'))
		  ->and_where('end_time','>',DB::expr('CURTIME()'))
		  ->execute()
		  ->as_array();

		if (count($timeMatches)==0) {
			return false;
		}

		$schedules = DB::select()
		  ->from('pi_location_schedule')
		  ->where('day','=',$day)
		  ->and_where('location_id','=',$location)
		  ->and_where('location_time_id','IN',$timeMatches)
		  ->execute()
		  ->as_array();

		if (count($schedules)>0) {
			return true;
		}
		return false;
	}

	/**
	 * Obtiene los lugares cercanos
	 * @param  array $string_ids 
	 * @param  array $array_ids  
	 * @return array lugares cercanos
	 */
	public function getNearPlacesAPI( $string_ids, $array_ids ) {
		$query = <<<EOT
SELECT * FROM pi_location WHERE id IN :array_ids ORDER BY FIND_IN_SET(id, :string_ids )
EOT;
		$params = array( ":string_ids" => $string_ids, 
			":array_ids" => $array_ids);
		$results = DB::query(Database::SELECT, $query)
			->parameters($params)
			->execute()
			->as_array();
		foreach ($results as &$result) {
	  	$result['tags'] = $this->getTagsOfLocationAPI($location_id);
    	$result['schedules'] = $this->getScheduleOfLocationAPI($location_id);
	  }
	  unset($result);
	  return $results;
	}

	/**
	 * Obtiene los IDS de las ubicaciones cercanas
	 * @param  float $latitude  latitud
	 * @param  float $longitude longitud
	 * @return array            lugares
	 */
	function getNearPlacesIdsAPI( $latitude, $longitude) {
		$query = <<<EOT
      SELECT id, ( 3959 * acos( cos( radians(19.491800) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(-99.131548) ) + sin( radians(19.491800) ) * sin( radians( latitude ) ) ) ) 
      AS distance 
      FROM pi_location 
      ORDER BY distance 
      LIMIT 3;
EOT;
		$params = array( ":latitude" => $latitude, ":longitude" => $longitude);
		$results = DB::query(Database::SELECT, $query)
		  ->execute()
		  ->as_array();
    return $results;
	}

	/************************************************************************************/
	/* 	Funciones creadas para realizar la administración de las ubicaciones            */
	/*  estas funciones se van a eliminar una vez que ya se tenga el webservice.				*/
	/************************************************************************************/

	/**
	 * Inserta la localidad
	 * @param string $name             nombre de la localidad
	 * @param string $description      descripción de la localidad
	 * @param string $street           calle
	 * @param string $int_number       número interior
	 * @param string $ext_number       número exterior
	 * @param string $town             colonia
	 * @param string $municipal_office municipio
	 * @param string $city             ciudad
	 * @param string $state            estado
	 * @param string $zip              código postal
	 * @param float $latitude          latitud
	 * @param float $longitude         longitud
	 * @param string $phone            teléfonos
	 * @param string $email            correo electrónico
	 * @param string $image            ruta a la imagen
	 * @param string $webpage          página web
	 * @param string $facebook         facebook
	 * @param string $twitter          twitter
	 * @param string $contact          información de contacto
	 * @param string $attributes       otros atributos formateados como json
	 * @param integer $rating          rating
	 * @param string $meta             otros atributos formateados como json
	 * @param integer $owner_id        id dueño del lugar
	 * @param integer $creator_id      id del creador de la ubicación
	 */
	public function addLocation ($name, 
		$description, 
		$street, 
		$int_number, 
		$ext_number, 
		$town, 
		$municipal_office, 
		$city, 
		$state, 
		$zip, 
		$latitude, 
		$longitude, 
		$phone, 
		$email, 
		$image, 
		$webpage, 
		$facebook,
		$twitter, 
		$contact, 
		$attributes, 
		$rating, 
		$meta,
		$reference_id,
		$owner_id, 
		$creator_id) 
	{
		$created_on = date('Y-m-d H:i:s');
		$updated_on = date('Y-m-d H:i:s');
		$query = DB::insert('pi_location', 
			array('name',
				'description',
				'street',
				'int_number',
				'ext_number',
				'town',
				'municipal_office',
				'city',
				'state',
				'zip',
				'latitude',
				'longitude',
				'phone',
				'email',
				'image',
				'webpage',
				'facebook',
				'twitter',
				'contact',
				'attributes',
				'rating',
				'meta',
				'slug',
				'reference_id',
				'owner_id',
				'creator_id',
				'created_on',
				'updated_on'))
		->values(
			array($name,
				$description,
				$street,
				$int_number,
				$ext_number,
				$town,
				$municipal_office,
				$city,
				$state,
				$zip,
				$latitude,
				$longitude,
				$phone,
				$email,
				$image,
				$webpage,
				$facebook,
				$twitter,
				$contact,
				$attributes,
				$rating,
				$meta,
				$this->toAscii($name),
				$reference_id,
				$owner_id,
				$creator_id,
				$created_on,
				$updated_on));
		echo $query.";\n";
		$result = 0;
		return $result;
	}

	/**
	 * Edita la localidad
	 * @param integer $id              id de la ubicación
	 * @param string $name             nombre de la localidad
	 * @param string $description      descripción de la localidad
	 * @param string $street           calle
	 * @param string $int_number       número interior
	 * @param string $ext_number       número exterior
	 * @param string $town             colonia
	 * @param string $municipal_office municipio
	 * @param string $city             ciudad
	 * @param string $state            estado
	 * @param string $zip              código postal
	 * @param float $latitude          latitud
	 * @param float $longitude         longitud
	 * @param string $phone            teléfonos
	 * @param string $email            correo electrónico
	 * @param string $image            ruta a la imagen
	 * @param string $webpage          página web
	 * @param string $facebook         facebook
	 * @param string $twitter          twitter
	 * @param string $contact          información de contacto
	 * @param string $attributes       otros atributos formateados como json
	 * @param integer $rating          rating
	 * @param string $meta             otros atributos formateados como json
	 * @param integer $owner_id        id dueño del lugar
	 * @param integer $creator_id      id del creador de la ubicación
	 */
	public function editLocation ($id, 
		$name, 
		$description, 
		$street, 
		$int_number, 
		$ext_number, 
		$town, 
		$municipal_office, 
		$city, 
		$state, 
		$zip, 
		$latitude, 
		$longitude, 
		$phone, 
		$email, 
		$image, 
		$webpage, 
		$facebook,
		$twitter,
		$contact, 
		$attributes, 
		$rating, 
		$meta, 
		$owner_id, 
		$creator_id) 
	{
		$query = DB::update('pi_location')
		  ->set(
		  	array('name' => $name,
		  		'description' => $description,
		  		'street' => $street,
		  		'int_number' => $int_number,
		  		'ext_number' => $ext_number,
		  		'town' => $town,
		  		'municipal_office' => $municipal_office,
					'city' => $city,
					'state' => $state,
					'zip' => $zip,
					'latitude' => $latitude,
					'longitude' => $longitude,
					'phone' => $phone,
					'email' => $email,
					'image' => $image,
					'webpage' => $webpage,
					'facebook' => $facebook,
					'twitter' => $twitter,
					'contact' => $contact,
					'attributes' => $attributes,
					'rating' => $rating,
					'meta' => $meta,
					'slug' => $this->toAscii($name),
					'owner_id' => $owner_id,
					'creator_id' => $creator_id))
		  ->where('id','=',$id);
																			 
		$query->execute();
	}

	public function addLocationTag ($name) {
		$query = DB::insert('pi_location_tag', array('name','slug'))->values(array($name,$this->toAscii($name)));
		$query->execute();
	}

  public function editLocationTag ($id, $name) {
		$query = DB::update('pi_location_tag')->set(array('name' => $name,'slug' => $this->toAscii($name)))->where('id','=',$id);
		$query->execute();
	}

	public function addTagToLocation ($tag_id,$location_id) {
		$query = DB::insert('pi_location_has_tag', array('location_tag_id','location_id'))->values(array($tag_id,$location_id));
		$query->execute();
	}

	public function editTagToLocation ($id, $tag_id,$location_id) {
		$query = DB::update('pi_location_has_tag')->set(array('location_tag_id' => $tag_id,'location_id'=> $location_id))->where('id','=',$id);
		$query->execute();
	}

	/**
	 * Funciones para insertar/editar lugares
	 * @param [type] $begin_time [description]
	 * @param [type] $end_time   [description]
	 */
	public function addLocationTime ($begin_time, $end_time) {
		$query = DB::insert('pi_location_time', array('begin_time','end_time'))->values(array($begin_time,$end_time));
		$query->execute();
	}	
	
	public function editLocationTime ($id, $begin_time, $end_time) {
		$query = DB::update('pi_location_time')->set(array('begin_time' => $begin_time,'end_time' =>$end_time))->where('id','=',$id);
		$query->execute();
	}	

	public function addLocationScedule ($day, $location_id, $location_time) {
		$query = DB::insert('pi_location_schedule', array('day','location_id','location_time_id'))->values(array($day,$location_id,$location_time_id));
		$query->execute();
	}

	public function editLocationScedule ($id, $day, $location_id, $location_time) {
		$query = DB::update('pi_location_schedule')->set(array('day' => $day,'location_id' => $location_id,'location_time_id' => $location_time_id))->where('id','=',$id);
		$query->execute();
	}
}