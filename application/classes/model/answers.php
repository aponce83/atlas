<?php defined('SYSPATH') OR die('No direct access allowed.');
 /**
 * Answer Model. 
 *
 * @package    Contento
 * @category   Models
 * @author     
 * @copyright  
 * @license    
 */
class Model_Answers extends Model
{
    private $answer_table;
    //private $genre_table; 
    //private $db;
  
    public function __construct()
    {
        //parent::__construct();

        $this->db = Database::instance();
        $this->answer_table = 'pi_answers';
    }
   
    public function read_answers_from_question($id_question)
    {
        //$this->db->where('id', $id);
        //$query = $this->db->get($this->album_table); 

        $query = DB::select()->from('pi_answers')->where('question_id', '=', (int)$id_question);
        //var_dump($query->execute());
        $result = $query->execute()->as_array();
        return $result;
    }
     
    public function delete($id)
    {
        echo $id;
         $query = DB::delete('pi_questions')->where('id', '=', (int)$id)->execute();
    }
   
    public function update($id,$data)
    {
        echo 'id = '.$id;
        $id = $data['album_id'];
        echo 'id otra vez = '.$id;
         foreach ($data as $d) 
        {
            echo "<br>";
            echo $d;
        }
        $query = DB::update('pi_questions')->set(array('name'=>$data['name'],'author'=>$data['author'],'genre_id'=>$data['genre_id']))->where('id', '=', (int)$id)->execute();
        //$this->db->update($this->album_table, $data, array('id' => $id));
    }
     
    public function create($data)
    {
        //$this->db->insert($this->album_table, $data);
        //$query = DB::insert('pi_trivia', array('trivia_name', 'trivia_description', 'keywords', 'trivia_score'))->values(array($data['name'], $data['description'], $data['keywords'], $data['score']))->execute();
        foreach ($data as $d) 
        {
            echo "<br>";
            echo $d;
        }
        echo "<br>".$data['name']."";
    }
     
    /**
    * Obtiene todas las respuestas de las trivias
    * @return array arreglo con las respuestas de las trivias
    */
    public function get_all() {
        $query = <<<EOT
            SELECT * 
            FROM pi_answers as t
            ORDER BY answer_id DESC
EOT;
        $response = DB::query(Database::SELECT, $query)
            ->execute()
            ->as_array();
        return $response;
    }

  /**
   * Actualización desde el administrador
   */
  /**
   * Actualiza la trivia a partir de su ID
   */
    public function updateValue($id, $name, $value) {
        $result = DB::update('pi_answers')
            ->set(array($name => $value))
            ->where('answer_id', '=', $id)
            ->execute();
        return $result;
    }
    
}