<?php defined('SYSPATH') or die('No direct script access.');

class Model_Api_User extends Model {

	/**
	 * Revisamos que el correo sea único
	 * @param  string  $email correo electrónico
	 * @return boolean        indica si el correo es único
	 */
	public function is_email_unique($email) {
		$query = DB::select()
		->from('pi_api_user')
		->where('email', '=', $email);
		$result = $query->execute();
		if (count($result) > 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Obtiene el usuario a partir de su correo electrónico
	 * @param  string $email correo electrónico
	 * @return array        estructura del elemento
	 */
	public function get_by_email($email) {
		$query = DB::select()
			->from('pi_api_user')
			->where('email', '=', $email)
			->limit(1);
		$result = $query->execute();
		return $result[0];
	}

	/**
	 * Obtiene el usuario a partir de su id
	 * @param  int id del usuario
	 * @return array        estructura del elemento
	 */
	public function get_by_id($id) {
		$query = DB::select()
			->from('pi_api_user')
			->where('id', '=', $id)
			->limit(1);
		$result = $query->execute();
		return $result[0];
	}

	/**
	 * Obtener todos los usuarios del api
	 * @return array todos los usuarios registrados
	 */
	public function get_all() {
		$query = DB::select()
			->from('pi_api_user');
		$result = $query->execute();
		return $result;
	}

	/**
	 * Obtiene todos los usuarios del api con estatus pendiente
	 * @return array usuarios con estatus pendiente
	 */
	public function get_pending() {
		$query = DB::select()
			->from('pi_api_user')
			->where('status','=','en espera');
		$result = $query->execute();
		return $result;
	}

	/**
	 * Realiza la aprobación del usuario
	 */
	public function make_approval($api_user_id) {
		$result = $this->updateValue($api_user_id, 'status', 'aprobado');
		return $result;
	}

	/**
	 * Guarda al usuario en la base
	 * @param  array $data datos del usuario
	 * @return int id del usuario
	 */
	public function insert($data) {
		list($id) = DB::query(Database::INSERT, "
				INSERT INTO `pi_api_user` (`fullname`, `email`, `password`, `description`, `status`, `created_on`, `updated_on`)
				VALUES (:fullname, :email, :password, :description, :status, :created_on, :updated_on)
			")
			->parameters(array(
				':fullname' => $data['fullname'],
				':email' => $data['email'],
				':password' => $data['password'],
				':description' => $data['description'],
				':status' => 'en espera',
				':created_on' => date('Y-m-d H:i:s'),
				':updated_on' => date('Y-m-d H:i:s')
			))
			->execute();
		return $id;
	}

	/**
   * Actualiza la información del usuario del api
   */
  public function updateValue($id, $name, $value) {
    $result = DB::update('pi_api_user')
      ->set(array($name => $value,
      	'updated_on' => date('Y-m-d H:i:s')))
      ->where('id', '=', $id)
      ->execute();
    return $result;
  }
}