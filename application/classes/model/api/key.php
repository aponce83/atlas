<?php defined('SYSPATH') or die('No direct script access.');

class Model_Api_Key extends Model {

	/**
	 * Obtiene todas las llaves de un usuario
	 * @param  int $api_user_id id del usuario
	 */
	public function get_keys_by_user($api_user_id) {
		$query = DB::select()
		->from('pi_api_user_key')
		->where('pi_api_user_id', '=', $api_user_id);
		$result = $query->execute()->as_array();
		return $result;
	}

	// Obtiene la llave del usuario
	public function get_valid_api_key($api_id) {
		$query = DB::select('pi_api_user_key.*')
			->from('pi_api_user_key')
			->join('pi_api_user')
			->on('pi_api_user_key.pi_api_user_id', '=', 'pi_api_user.id')
			->where('pi_api_user.status', '=', 'aprobado')
			->and_where('pi_api_user_key.api_id', '=', $api_id)
			->limit(1);
		$result = $query->execute()->as_array();
		return $result[0];
	}

	/**
	 * Le genera un set de llaves al usuario
	 */
	public function create_api_keys($api_user_id, $created_on) {
		$api_id = time();
		$api_key = Text::random('alnum', 32);
		list($id) = DB::query(Database::INSERT, "
				INSERT INTO `pi_api_user_key` (`api_id`, `api_key`, `created_on`, `updated_on`, `pi_api_user_id`)
				VALUES (:api_id, :api_key, :created_on, :updated_on, :pi_api_user_id);
			")
			->parameters(array(
				':api_id' => $api_id,
				':api_key' => $api_key,
				':pi_api_user_id' => $api_user_id,
				':created_on' => date('Y-m-d H:i:s'),
				':updated_on' => date('Y-m-d H:i:s')
			))
			->execute();
		return $id;
	}

}