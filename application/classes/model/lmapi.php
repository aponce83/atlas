<?php defined('SYSPATH') or die('No direct script access.');

class Model_Lmapi extends Model {
	
	/**
	 * Obtiene todos los datos
	 */
	public function get_all() {
		$query = <<<EOT
      SELECT id, elapsed_time, endpoint, data, data_json, start_time, end_time, result_json, userfront_id
      FROM lmapi_log
      WHERE 1
      ORDER BY id DESC;
EOT;
  $result = DB::query(Database::SELECT, $query)
      ->execute()
      ->as_array();
    return $result;
	}

	/**
	 * Inserta los datos
	 * @param  array $data datos del objeto Lmapi
	 * @return int      id del objeto
	 */
	public function insert($data) {
		list($id) = DB::query(Database::INSERT, " 
				INSERT INTO `lmapi_log` (`endpoint`, `data`, `data_json`, `userfront_id`, `start_time`, `end_time`, `elapsed_time`, `result_json`, `created_on`, `updated_on`) VALUES (:endpoint, :data, :data_json, :userfront_id, :start_time, :end_time, :elapsed_time, :result_json, :created_on, :updated_on)
			")	->parameters(array(
				':endpoint' => $data['endpoint'],
				':data' => $data['data'],
				':data_json' => $data['data_json'],
				':userfront_id' => $data['userfront_id'],
				':start_time' => $data['start_time'],
				':end_time' => $data['end_time'],
				':elapsed_time' => $data['elapsed_time'],
				':result_json' => $data['result_json'],
				':created_on' => date('Y-m-d H:i:s'),
				':updated_on' => date('Y-m-d H:i:s')
			))->execute();
		return $id;
	}
}