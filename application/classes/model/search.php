<?php defined('SYSPATH') or die('No direct script access.');
/**
 * List Model. 
 *
 * @package    
 * @category   Models
 * @author     Reynaldo Esparza
 * @copyright  
 * @license    
 */
class Model_Search extends Model {

  public function __construct(){
    $this->db = Database::instance('default');
  }

  /**
   * Almacena el seguimiento de las búsquedas
   * @param string $query      parametros de busqueda
   * @param string $source     origen de la búsqueda (header o página de búsquedas)
   * @param int $userfront_id  id del usuario que consulto
   * @param string $user_agent Navegador utilizado
   * @param string $par parametro buscado 
   * @param string $val valor de búsqueda
   */
  public function set_search_tracking($query, 
    $source, 
    $userfront_id = null, 
    $user_agent = '',
    $par = null,
    $val = null)
  {
    try
    {
      DB::insert('pi_search_tracking', 
          array('query', 
            'source',
            'userfront_id',
            'user_agent',
            'par', 
            'val', 
            'created_on', 
            'updated_on'))
        ->values(array(
          $query, 
          $source, 
          $userfront_id, 
          $user_agent,
          $par,
          $val,
          date('Y-m-d H:i:s'), 
          date('Y-m-d H:i:s')) )
        ->execute();
    }
    catch( Exception $e )
    {
      Log::instance()->add(Log::ERROR, "Track Search: " . $e->getMessage() );
      return false;
    }
  }

  public function get_search_info_in_groups($from = null,
    $to = null,
    $par = null,
    $location = null)
  {
    $parameters = array();
    $query = <<<EOT
SELECT val, COUNT(*) as c
FROM pi_search_tracking
WHERE 1
EOT;
    if ($from !== null && $to !== null) {
      $from = $from.' 00:00:00';
      $to = $to.' 23:59:59';
      $parameters[':to'] = $to;
      $parameters[':from'] = $from;
      $query .= " AND created_on > :from AND created_on < :to";
    }
    if ($par !== null) {
      $parameters[':par'] = $par;
      $query .= " AND par = :par";
    }
    if ($location !== null) {
      $parameters[':source'] = $location;
      $query .= " AND source = :source";
    }
    $query .= " GROUP BY val";
    $query .= " ORDER By created_on desc;";

    $result = DB::query(Database::SELECT, $query)
      ->parameters($parameters)
      ->execute()
      ->as_array();
    
    return $result;
  }
/*
  public function update_all_the_problems() {
    $queryProblems = <<<EOT
    SELECT * 
    FROM pi_search_tracking
    WHERE 1
    AND par IS NULL;
EOT;
    $queryUpdate = <<<EOT
    UPDATE pi_search_tracking
    SET par = :par, val = :val, query = :query
    WHERE id = :id
EOT;
    $result = DB::query(Database::SELECT, $queryProblems)
      ->execute()
      ->as_array();
    foreach ($result as $r) {
      $theQuery = $r['query'];
      $re = "/\"(.*?)\"/";
      preg_match_all($re, $theQuery, $matches);
      if (count($matches) == 2) {
        $theQueryCorrect = array(
          'par' => str_replace('"', "", $matches[0][0]),
          'val' => str_replace('"', "", $matches[0][1])
          );
        $newQuery = array($theQueryCorrect['par'], $theQueryCorrect['val']);
        $stmt = DB::query(Database::UPDATE, $queryUpdate)
          ->parameters(
              array(':par' => $theQueryCorrect['par'],
                ':val' => $theQueryCorrect['val'],
                ':query' => json_encode($newQuery, true),
                ':id' => $r['id']
              )
            );
        $stmt->execute();
      }
    }
  }
*/
}