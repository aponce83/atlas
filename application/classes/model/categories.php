<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Categories model. 
 *
 * @package    Contento
 * @category   Models
 * @author     Abargon
 * @copyright  (c) 2013 Abargon
 * @license    http://abargon.com
 */
class Model_Categories extends Model {
	public function __construct(){ $this->db = Database::instance('default'); }	

	public function fetch_categories() //Obtiene las categorias del Dewey
	{
		return DB::query(Database::SELECT, "
		SELECT * FROM pi_category")->execute();
	}	
}