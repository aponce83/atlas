<?php defined('SYSPATH') or die('No direct script access.');

/**
 * General helper functions just for the views
 */
class LMView {

	/**
	 * Realiza la inyección de archivos js o css en el layout
	 * @param  array $files archivos
	 * @param  string $type  tipos de archivos
	 */
	public static function inject_cssjs_files($files, $type) {
		if (is_array($files)) {
			$type = strtolower($type);
			foreach ($files as $file) {
				if ($type == 'css') {
					echo '<link rel="stylesheet" type="text/css" href="'.$file.'">';
				} else if ($type == 'js') {
					echo '<script src="'.$file.'"></script>';
				}
			}
		}
	}

	/**
	 * Realiza la inyección de código, la función envuelve el código
	 * dentro de la etiqueta script y realiza el echo del código
	 * enviado
	 * @param  string $jscode código
	 */
	public static function inject_jscode($jscode) {
		$code = '';
		if ($jscode != null) {
			$code = '<script>'.$jscode.'</script>';
		}
		echo $code;
	}
}