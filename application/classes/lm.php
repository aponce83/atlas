<?php defined('SYSPATH') or die('No direct script access.');


/**
 * General helper functions
 */
class LM {
	/**
	 * Time elapsed since this function was last called, prints on error_log
	 * 
	 * rsc: http://stackoverflow.com/questions/11235369/measuring-the-elapsed-time-between-code-segments-in-php
	 * @param  string $comment comment
	 */
	function time_elapsed($comment = "", $print_type = 'error_log') {
	  static $time_elapsed_last = null;
	  static $time_elapsed_start = null;
	  $unit="s"; $scale=1000000; // output in seconds
	  // $unit="ms"; $scale=1000; // output in milliseconds
	  //$unit="μs"; $scale=1; // output in microseconds
	  $now = microtime(true);
	  if ($time_elapsed_last != null) {
			$log = "";
			$log .= "\n";
			$log .= "[!-- ";
			$log .= "$comment: Time elapsed: ";
			$log .= round(($now - $time_elapsed_last)*1000000)/$scale;
			$log .= " $unit, total time: ";
			$log .= round(($now - $time_elapsed_start)*1000000)/$scale;
			$log .= " $unit --]";
			if ($print_type == 'error_log') {
				$log .= "\n";
				error_log($log);
			} else if ($print_type == 'echo') {
				$log .= "<br>";
				echo $log;
			}
	  } else {
			$time_elapsed_start=$now;
	  }
	  $time_elapsed_last = $now;
  }

  /**
	 * Time elapsed since this function was last called, returns value
	 * 
	 * rsc: http://stackoverflow.com/questions/11235369/measuring-the-elapsed-time-between-code-segments-in-php
	 */
	function get_time_elapsed() {
	  static $get_time_elapsed_last = null;
	  static $get_time_elapsed_start = null;
	  $unit="s"; $scale=1000000; // output in seconds
	  // $unit="ms"; $scale=1000; // output in milliseconds
	  //$unit="μs"; $scale=1; // output in microseconds
	  $now = microtime(true);
	  $elapsed_time = 0;
	  if ($get_time_elapsed_last != null) {
	  	$elapsed_time = round(($now - $get_time_elapsed_last)*1000000)/$scale;
	  } else {
			$get_time_elapsed_start = $now;
	  }
	  $get_time_elapsed_last = $now;
	  return $elapsed_time;
  }

  public static function get_time_elapsed_string($datetime) {
  	$tTime = strtotime($datetime) * 1000;
    $cTime = time() * 1000;
    $sinceMin = round( abs($cTime-$tTime) / 60000);

    if ( $sinceMin == 0 ) {
      $since = 'ahora';
    } else if ( $sinceMin == 1 ) {
      $since = 'Hace 1 minuto';
    } else if ( $sinceMin < 45 ) {
      $since = 'Hace '.$sinceMin.' minutos';
    } else if ($sinceMin > 44 && $sinceMin < 60) {
      $since = 'Hace 1 hora';
    } else if ($sinceMin < 1440) {
        $sinceHr = round($sinceMin/60);
        if ($sinceHr == 1) {
          $since = 'Hace 1 hora';
        } else {
          $since =' Hace '.$sinceHr.' horas';
        }
    } else if( $sinceMin > 1439 &&
    	$sinceMin < 2880 )
    {
      $since = 'Hace 1 día';
    } else {
      $sinceDay = round($sinceMin/1440);
      $since = 'Hace '.$sinceDay.' días';
      if ($sinceDay > 10) {
        $phpdate = strtotime( $datetime );
        $mysqldate = date( 'd/m/Y', $phpdate );         
        $since = $mysqldate;
      }
    }

    return $since;
  }

	/**
	 *Returns an slug version of string 
	 * 
	 * @param  string $text string to slugify
	 * @return string string slugified
	 */
	public static function slugify($text) { 
		  // replace non letter or digits by -
		  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
		  // trim
		  $text = trim($text, '-');
		  // transliterate
		  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		  // lowercase
		  $text = strtolower($text);
		  // remove unwanted characters
		  $text = preg_replace('~[^-\w]+~', '', $text);
		  if (empty($text)) {
		    return 'n-a';
		  }
		  return $text;
	}

	/**
	 *Returns all params into string 
	 * 
	 * @param  array $params
	 * @return string params into string
	 */
	public static function getParamsAsString($params, $eq = "=", $amp = "&") {
		$i = 1;
		$cParams = count($params);
		$strParams = "";
		foreach ($params as $k => $v) {
			$v = urlencode($v);
			if ($cParams == $i) {
				$amp = "";
			}
			$strParams .= "{$k}{$eq}{$v}{$amp}";
			$i++;
		}
		return $strParams;
	}

	/**
	 * Returns a well-formed url given the parameters and the base url
	 *
	 * @param string $url url to append
	 * @param  array $params
	 * @return string well-formed url
	 */
	public static function getURLWithParams($url, $params = array()) {
		$result = $url;
		if (count($params) > 0) {
			$paramsStr = LM::getParamsAsString($params);
			$result .= '?'.$paramsStr;
		}
		return $result;
	}

	/**
	 * Returns redis key equal prefix_key=value&  
	 * All keys have a prefix:
	 * 	s_ : raw requests
	 * 	m_ : used only for functions created in models
	 * 
	 * @param  array $params string $prefix
	 * @return string params as rediskey 
	 */
	public static function getRedisKey($prefix, $params) {
		$paramsStr = "{$prefix}_";
		$paramsStr .= LM::getParamsAsString($params, "_", "-");
		$redisKey = LM::slugify($paramsStr);
		return $redisKey;
	}

	/**
	 * Receives a parameter and retrieves a readable and correct version of it
	 * for example: Titulo -> título, keyword -> palabra clave
	 * @param string $param
	 * @return string redeable version
	 */
	public static function paramToSpanish($param) {
		$dictionary = array(
			"id_libro" => "id del libro",
			"tipo_producto" => "tipo",
			"isbn" => "isbn",
			"codigo_barras" => "códito de barras",
			"titulo" => "título",
			"subtitulo" => "subtítulo",
			"sinopsis" => "sinopsis",
			"editorial" => "editorial",
			"coleccion" => "colección",
			"encuadernacion" => "encuadernación",
			"edicion" => "edición",
			"reimpresion" => "reimpresión",
			"no_volumen" => "no. volumen",
			"ilustraciones_1" => "ilustraciones (min)",
			"ilustraciones_2" => "ilustraciones (max)",
			"idioma_original" => "idioma original",
			"curso" => "curso",
			"peso_1" => "peso (min)",
			"peso_2" => "peso (max)",
			"alto_1" => "alto (min)",
			"alto_2" => "alto (max)",
			"ancho_1" => "ancho (min)",
			"ancho_2" => "ancho (max)",
			"grosor_1" => "grosor (min)",
			"grosor_2" => "grosor (max)",
			"paginas_1" => "páginas (min)",
			"paginas_2" => "páginas (max)",
			"nacional" => "nacional",
			"pais" => "país",
			"iva" => "IVA",
			"autores" => "autores",
			"temas" => "temas",
			"keywords" => "palabras clave",
			"estado" => "estado",
			"disponibilidad" => "disponibilidad",
			"audiencia" => "audiencia",
			"nivel_lectura" => "nivel de lectura",
			"nivel_academico" => "nivel académico",
			"curso" => "curso",
			"asignatura" => "asignatura",
			"fecha_colofon_1" => "fecha de colofón (min)",
			"fecha_colofon_2" => "fecha de colofón (max)",
			"fecha_publicacion_1" => "fecha de publicación (min)",
			"fecha_publicacion_2" => "fecha de publicación (max)",
			"fecha_actualizacion_1" => "fecha de actualización (min)",
			"fecha_actualizacion_2" => "fecha de actualización (max)",
			"min" => "min",
			"pagina" => "página",
			"registros_por_pagina" => "registros por página",
			"attr" => "atributos",
			"orden" => "orden",
			"orden_dir" => "dirección del ordenamiento",
			"temas" => "clasificación Dewey",
			"tema-nombre" => "tema",
			"cadena" => "Todos los campos",
			"precio_vigente" => "Precio único",
		);
		$translation = $param;
		if (array_key_exists($param, $dictionary)) {
			$translation = $dictionary[$param];
		}
		return $translation;
	}

	/**
	 * returns an array with the types of products
	 * (key => type)
	 * @param 
	 * @return array 
	 */
	public static function tipo_clave_producto(){
		$tipos = array(
			'10' => 'Libro',
			'11' => 'Colección',
			'20' => 'CD',
			'30' => 'Libros electrónicos',
			'40' => 'Cd-ROM',
			'50' => 'DVD',
			'60' => 'Libro + CD',
			'70' => 'Audio Libro',
			'80' => 'Revista',
			'95' => 'Otros'
		);
		return $tipos;
	}

	public static function clave_si_no(){
		$claves = array(
			'0' => 'No',
			'1' => 'Si',
		);
		return $claves;
	}

	public static function precio_unico(){
		$claves = array(
			'0' => 'No vigente',
			'1' => 'Vigente',
		);
		return $claves;
	}

	/**
	 * returns an array with the types of products (reverse of tipo_clave_producto)
	 * (type => key)
	 * @param 
	 * @return array 
	 */
	public static function tipo_producto_clve(){
		$tipos_clave = LM::tipo_clave_producto();
		foreach ($tipos_clave as $key => $value) {
			$tipos[$value] = $key; 
		}
		return $tipos;
	}

	/**
	 * Tipo de Encuadernacion
	 * (key => type)
	 * @param 
	 * @return array 
	 */
	public static function catalogo_encuadernacion(){
		$tipos = array(
			'1'  => 'Rústica',
			'2'  => 'Pasta dura',
			'3'  => 'Tela',
			'4'  => 'Troquelado',
			'5'  => 'Espiral',
			'6'  => 'Wireo',
			'7'  => 'Engrapado',
			'8'  => 'Fascículo encuadenable',
			'9'  => 'Pasta dura - sobrecubierta',
			'10' => 'Anillas',
			'11' => 'Otros'
		);
		return $tipos;
	}

	/**
	 * returns the text with the ellipsis
	 * You send the following text: "this is 26 characters long"
	 * and with the limit 10
	 * it will return "this is 26..."
	 * it will cut at the first space character from end to finish
	 * @param  string  $text  text to cut
	 * @param  integer $cut_at of characters
	 * @return string         text with ellipsis if needed
	 */
	public static function text_to_ellipsis($text, $cut_at = 30) {
		$text = trim($text);
		if (strlen($text) > $cut_at) {
			$text = mb_substr($text, 0, $cut_at,  'UTF-8')."...";
		}
		return $text;
	}

	/**
	 * Regresa el protocolo de la petición
	 * @return string https o http
	 */
	public static function get_protocol() {
		$isSecure = false;
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
	    $isSecure = true;
		} else if (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
	    $isSecure = true;
		} 
		$protocol = $isSecure ? 'https' : 'http';
		return $protocol;
	}

	/**
	 * Obtiene una url de referencia de registro
	 * @param  int $id id del usuario que hace la referencia
	 * @return strin     url de registro
	 */
	public static function get_referral_url($id) {
		$encoded_id = base64_encode($id);
		$url = "http://librosmexico.mx/registro-usuario?lmxid={$encoded_id}";
		return $url;
	}

	public static function send_raw_email($to, $subject, $message) {
		
		if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
	    return false;
		}

		$final_message = $message;

		$email = Email::factory($subject, $final_message, 'text/html')
			->from('no-reply@librosmexico.mx', 'LIBROSMÉXICO.MX')
			->to($to);

		$response = $email->send();
		return $response;
	}

	public static function send_mail($to, $subject, $message, $subject_html = NULL) {
		
		if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
	    return false;
		}

		if ($subject_html === NULL) {
			$subject_html = $subject;
		}

		$final_message = <<<EOT
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>{$subject}</title>
      </head>
      <body>
        <table style="margin:20px auto 0 ;" width="600" align="center"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>  
              <a style="display:block; clear:both; margin:0 0 30px;" href="http://librosmexico.mx" target="_blank">
              	<img src="http://librosmexico.mx/assets/images/libros_logo.png" />
              </a>
              <h2 style="font-weight:normal;">
              	<font style="font-family:Arial, Helvetica, sans-serif; font-size:18px;">
              		{$subject}
            		</font>
          		</h2>
							{$message}
              <p>
              	<font style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">
              		Atentamente,<br />
              		<a href="https://librosmexico.mx" target="_blank">LIBROSMÉXICO.MX</a>
              	</font>
              </p>
							<div style="margin:40px 0 0 0; border-top:solid 2px #354f5e; padding:10px 0 0;">              
								<p>
									<font style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#606366;">
										Este correo electrónico se generó automáticamente. Por favor, no lo respondas.
									</font>
								</p>
							</div>
						</td>
					</tr>
				</table>
			</body>
		</html>
EOT;
		
		$email = Email::factory($subject, $final_message, 'text/html')
			->from('no-reply@librosmexico.mx', 'LIBROSMÉXICO.MX')
			->to($to);

		$response = $email->send();
		return $response;
	}

	/**
	 * Recibe un tiempo en tiempo en millis y lo convierte en una fecha que se puede leer
	 * @param string $millis tiempo en millis
	 * @return string tiempo que se puede leer (Y-m-d H:m:s)
	 */
	public static function millis_to_readable($millis, $format = 'Y-m-d H:i:s') {
		$seconds = $millis / 1000;
		$readable_date = date($format, $seconds);
		return $readable_date;
	}

	/**
	 * Recibe un tiempo en tiempo en unix_timestamp y lo convierte en una fecha que se puede leer
	 * @param string $unix_timestamp tiempo en unix
	 * @return string tiempo que se puede leer (Y-m-d H:m:s)
	 */
	public static function unix_timestamp_to_readable($unix_timestamp, $format = 'Y-m-d H:i:s') {
		$readable_date = gmdate($format, $unix_timestamp);
		return $readable_date;
	}

	/**
	 * Crea un string a partir de los valores de un arreglo, separado por pipes
	 * @param  array $arr       arreglo
	 * @param  string $separator separador, por defecto |
	 * @return string            representación en string
	 */
	public static function array_to_string_separated($arr, $separator = '|') {
		$result = '';
		$sep = '';
		foreach ($arr as $value) {
			$result .= $sep.$value;
			$sep = $separator;
		}
		return $result;
	}

	/**
	 * Realiza una función similar al var dump en la consola
	 * Resource: http://softkube.com/blog/capture-output-vardump-string
	 * @param  mixed $var variable
	 */
	public static function error_var_dump($var) {
		ob_start();
    var_dump($var);
    $result = ob_get_clean();
    error_log($result);
	}

	/**
	 * Realiza el loging de las funciones en el log de error
	 * @param  mixed $v variable a imprimir
	 */
	public static function log($v, $v_name = '') {
		if (is_string($v)) {
			error_log("LM::log {$v_name} -> {$v}");
		} else {
			error_log("LM::log {$v_name} ->");
			LM::error_var_dump($v);
		}
	}

	// original code: http://www.daveperrett.com/articles/2008/03/11/format-json-with-php/
	// adapted to allow native functionality in php version >= 5.4.0
	/**
	* Format a flat JSON string to make it more human-readable
	*
	* @param string $json The original JSON string to process
	*        When the input is not a string it is assumed the input is RAW
	*        and should be converted to JSON first of all.
	* @return string Indented version of the original JSON string
	*/
	function json_format($json) {
	  if (!is_string($json)) {
	    if (phpversion() && phpversion() >= 5.4) {
	      return json_encode($json, JSON_PRETTY_PRINT);
	    }
	    $json = json_encode($json);
	  }
	  $result      = '';
	  $pos         = 0;               // indentation level
	  $strLen      = strlen($json);
	  $indentStr   = "\t";
	  $newLine     = "\n";
	  $prevChar    = '';
	  $outOfQuotes = true;
	  for ($i = 0; $i < $strLen; $i++) {
	    // Speedup: copy blocks of input which don't matter re string detection and formatting.
	    $copyLen = strcspn($json, $outOfQuotes ? " \t\r\n\",:[{}]" : "\\\"", $i);
	    if ($copyLen >= 1) {
	      $copyStr = substr($json, $i, $copyLen);
	      // Also reset the tracker for escapes: we won't be hitting any right now
	      // and the next round is the first time an 'escape' character can be seen again at the input.
	      $prevChar = '';
	      $result .= $copyStr;
	      $i += $copyLen - 1;      // correct for the for(;;) loop
	      continue;
	    }
	    
	    // Grab the next character in the string
	    $char = substr($json, $i, 1);
	    
	    // Are we inside a quoted string encountering an escape sequence?
	    if (!$outOfQuotes && $prevChar === '\\') {
	      // Add the escaped character to the result string and ignore it for the string enter/exit detection:
	      $result .= $char;
	      $prevChar = '';
	      continue;
	    }
	    // Are we entering/exiting a quoted string?
	    if ($char === '"' && $prevChar !== '\\') {
	      $outOfQuotes = !$outOfQuotes;
	    }
	    // If this character is the end of an element,
	    // output a new line and indent the next line
	    else if ($outOfQuotes && ($char === '}' || $char === ']')) {
	      $result .= $newLine;
	      $pos--;
	      for ($j = 0; $j < $pos; $j++) {
	        $result .= $indentStr;
	      }
	    }
	    // eat all non-essential whitespace in the input as we do our own here and it would only mess up our process
	    else if ($outOfQuotes && false !== strpos(" \t\r\n", $char)) {
	      continue;
	    }
	    // Add the character to the result string
	    $result .= $char;
	    // always add a space after a field colon:
	    if ($outOfQuotes && $char === ':') {
	      $result .= ' ';
	    }
	    // If the last character was the beginning of an element,
	    // output a new line and indent the next line
	    else if ($outOfQuotes && ($char === ',' || $char === '{' || $char === '[')) {
	      $result .= $newLine;
	      if ($char === '{' || $char === '[') {
	        $pos++;
	      }
	      for ($j = 0; $j < $pos; $j++) {
	        $result .= $indentStr;
	      }
	    }
	    $prevChar = $char;
	  }
	  return $result;
	}

	/**
	 * Obtiene el mes según el mes enviado
	 * @param  integer $month_int mes en enteros donde 1 es enero y 12 es diciembre
	 * @return string            nombre del mes en texto
	 */
	function get_month_int_to_string($month_int) {
	  $mont_str = '';
	  switch ($month_int) {
	    case 1:
	      $mont_str = "enero";
	      break;
	    case 2:
	      $mont_str = "febrero";
	      break;
	    case 3:
	      $mont_str = "marzo";
	      break;
	    case 4:
	      $mont_str = "abril";
	      break;
	    case 5:
	      $mont_str = "mayo";
	      break;
	    case 6:
	      $mont_str = "junio";
	      break;
	    case 7:
	      $mont_str = "julio";
	      break;
	    case 8:
	      $mont_str = "agosto";
	      break;
	    case 9:
	      $mont_str = "septiembre";
	      break;
	    case 10:
	      $mont_str = "octubre";
	      break;
	    case 11:
	      $mont_str = "noviembre";
	      break;
	    case 12:
	      $mont_str = "diciembre";
	      break;
	  }
	  return $mont_str;
	}
}