<?php defined('SYSPATH') or die('No direct script access.');
/** 
 * Contento Frontend Homepage controller.
 *
 * @package    Contento/Frontend
 * @category   Controllers
 * @author     Abargon
 * @copyright  (c) 2013 Abargon
 * @license    http://abargon.com
 */
class Controller_Api_Atlas extends Controller_Frontend_Template {
	
	public function __construct(Request $request, Response $response){
	  parent::__construct($request, $response);
	}

	public function action_index(){
		$this->response->body("");
	}

	public function action_like_location () {
		$response = new stdClass;

			try{
				if( !isset($_POST['user'])  ) throw new Exception("Error, 1 not found", 1);
				if( !isset($_POST['like_state']) ) throw new Exception("Error, 2 not found", 1);
				if( !isset($_POST['location'])  ) throw new Exception("Error, 3 not found", 1);

				$user = (int)$_POST['user'];
				$like_state = $_POST['like_state'];
				$location = (int)$_POST['location'];
				if ($like_state === "like") {
					$likeId = Model::factory('atlas')->likeLocation($user,$location);
					Model::factory('social')->addActivity('like', $likeId, 0, $user);
					$response->like_state="Ya no me gusta";
				}
				else {
					Model::factory('atlas')->dislikeLocation($user,$location);
					$response->like_state="Me gusta";	
				}
				$response->total_likes = Model::factory('atlas')->getLikes ($location);
				$response->status = true;
				$response->l_id = $location;
			} catch( Exception $e ){

				$response->status = false;
				$response->message = $e->getMessage();
				log::instance()->add(Log::NOTICE, "// ERROR: ".$e->getMessage());

			}

			print json_encode( $response );
			exit;
	}

	

	public function action_retrieve_page () {
		$response = new stdClass;

			try {
				if( !isset($_POST['tags'])  ) throw new Exception("Error, 1 not found", 1);
				//if( !isset($_POST['cLat']) ) throw new Exception("Error, 2 not found", 1);
				//if( !isset($_POST['cLng'])  ) throw new Exception("Error, 3 not found", 1);
				if( !isset($_POST['latitude']) ) throw new Exception("Error, 2 not found", 1);
				if( !isset($_POST['longitude'])  ) throw new Exception("Error, 3 not found", 1);
				if( !isset($_POST['latitudeM']) ) throw new Exception("Error, 2.5 not found", 1);
				if( !isset($_POST['longitudeM'])  ) throw new Exception("Error, 3.5 not found", 1);
				if( !isset($_POST['reference']) ) throw new Exception("Error, 5 not found", 1);
				if( !isset($_POST['page']) ) throw new Exception("Error, 6 not found", 1);

				$tag_filters = explode(',',$_POST['tags']);

				$lat = (double)$_POST['latitude'];
				$lng = (double)$_POST['longitude'];
				$mlat = (double)$_POST['latitudeM'];
				$mlng = (double)$_POST['longitudeM'];
				$cPage = (int)$_POST['page'];

				if (!isset($_POST['rad']) || 
					((double)$_POST['rad'])<0) 
				{
					$count_total = null;
					$results = Model::factory('atlas')->getLocationPageByBounds ($lat, 
						$mlat, 
						$lng, 
						$mlng,
						$tag_filters, 
						$cPage, 
						24,
						$count_total);
					$total = $count_total;
				} else {
					$count_total = null;
					$results = Model::factory('atlas')->getLocationPageByBoundsRadius ($lat, 
						$mlat, 
						$lng, 
						$mlng,
						$tag_filters, 
						((double)$_POST['rad']), 
						$cPage, 
						24,
						$count_total);

					$total = $count_total;
				}

				$p = ((int)$_POST['page'])+1;
				$v = View::factory('frontend/atlas/results_page');
				$v->locations = $results;
				$v->total_results = $total;
				$v->total_pages = ceil($total/24);
				$v->current_page = $p;
				$v->reference = $_POST['reference'];
				$v->user = $this->user;

				$response->status = true;
				$response->message = "resultados!";
				$response->html = $v->render();
				$response->pages = ceil($total/24);
				$response->total = $total;
				
			} catch( Exception $e ){
				$response->status = false;
				$response->message = $e->getMessage();
				log::instance()->add(Log::NOTICE, "// ERROR: ".$e->getMessage());
			}

			print json_encode( $response );
			exit;
	}	
	public function action_retrieve_near_list () 
	{
		$response = new stdClass;

			try{
				if( !isset($_POST['tags'])  ) throw new Exception("Error, 1 not found", 1);
				//if( !isset($_POST['cLat']) ) throw new Exception("Error, 2 not found", 1);
				//if( !isset($_POST['cLng'])  ) throw new Exception("Error, 3 not found", 1);
				if( !isset($_POST['latitude']) ) throw new Exception("Error, 2 not found", 1);
				if( !isset($_POST['longitude'])  ) throw new Exception("Error, 3 not found", 1);
				if( !isset($_POST['latitudeM']) ) throw new Exception("Error, 2.5 not found", 1);
				if( !isset($_POST['longitudeM'])  ) throw new Exception("Error, 3.5 not found", 1);
				if( !isset($_POST['reference']) ) throw new Exception("Error, 5 not found", 1);
				if( !isset($_POST['page']) ) throw new Exception("Error, 6 not found", 1);

				$tag_filters = explode(',',$_POST['tags']);

				$lat = (double)$_POST['latitude'];
				$lng = (double)$_POST['longitude'];
				$mlat = (double)$_POST['latitudeM'];
				$mlng = (double)$_POST['longitudeM'];
				$cPage = (int)$_POST['page'];
				//$results = Model::factory('atlas')->getLocationPageByBounds (18, 20, -100,-90, $tag_filters, 0, 2);
				$count_total = null;
				$results = Model::factory('atlas')->getLocationPageByBounds ($lat, $mlat, $lng, $mlng, $tag_filters, 0, 2, $count_total);

				$total = $count_total;

				$p = ((int)$_POST['page'])+1;

				$v =  View::factory('frontend/atlas/near_list_detail');

				$v->lista_near = $results;

				$v->locations = $results;

				$v->total_results = $total;

				$v->total_pages = ceil($total/24);

				$v->current_page = $p;

				$v->reference = $_POST['reference'];

				$v->user = $this->user;

				$response->status = true;
				$response->message = "resultados!";
				$response->html = $v->render();
				$response->pages = ceil($total/24);
				$response->total = $total;
				
				//log::instance()->add(Log::NOTICE, "......................................................");
			} catch( Exception $e ){

				$response->status = false;
				$response->message = $e->getMessage();
				log::instance()->add(Log::NOTICE, "// ERROR: ".$e->getMessage());

			}

			print json_encode( $response );
			exit;
	}	

	/**
	 * Realiza el envío de la ubicación
	 */
	public function action_submit() {

		$result = array();
		$result['status'] = 'ERROR';
		$result['message'] = 'Muchas gracias por la información en breve revisaremos tu mensaje';
		$data = isset($_POST) ? $_POST : null;

		if (!$data) {
		  print json_encode($response);
		  exit;
		}

		$message = <<< EOT
		  Hola, <br>
		  Alguien ha enviado una nueva ubicación
EOT;
		$message .= "<table>";
		foreach ($data['contact'] as $key => $value) {
		  $message .= "<tr>";
		  $message .=   "<th style='width: 150px'>";
		  $message .=     "{$key}";
		  $message .=   "</th>";
		  $message .=   "<td>";
		  $message .=   "{$value}";
		  $message .=   "</td>";
		  $message .= "</tr>";
		}
		$message .= "</table>";

		$title = 'Contacto';

		$mail_response = LM::send_mail("contacto@librosmexico.mx", $title, $message);
		$result['data'] = $data;
		$result['mail_response'] = $mail_response;
		$result['status'] = 'SUCCESS';

		$params = [];
		$params['contacto'] = $data['contact'];
		$lmx = new LibrosMexicoInterface();
		$request = $lmx->makeRequest('submit_location', $params);

		echo json_encode($result, true);
		exit;
	}
}