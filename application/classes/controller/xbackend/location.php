<?php defined('SYSPATH') or die('No direct script access.');

class controller_xbackend_location extends Controller_Xbackend_Template {
  
  public function before() {
    parent::before();
  }

  public $google_api = 'AIzaSyA0Yne0tFk8_WDSBep5EWFPmdZitPa8SHI';

  /**
   * Obtiene la latitud y longitud a partir de una dirección
   * @param  string $address dirección a buscar
   * @return array          latitude y longitude
   */
  private function get_latlong_by_address(string $address) {
    $address_encode = urlencode($address);
    $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$address_encode}&key={$this->google_api}";
    $result_str = file_get_contents($url);
    $result_json = json_decode($result_str, true);
    $result_latlong = array('latitude' => null, 'longitude' => null);
    if ($result_json['status'] == 'OK') {
      if ($result_json['partial_match']) {
        echo "<span style='color:red;'>Es posible que tenga problemas :( </span>";
      }
      $result_latlong['latitude'] = $result_json['geometry']['location']['lat'];
      $result_latlong['longitude'] = $result_json['geometry']['location']['lng'];
    } else {
      echo "Hubo un problema";
      var_dump($result_json);
    }
    return $result_latlong;
  }

  /**
   * Limpia la cadena
   * @param  string $str cadena a limpiar
   * @return string      cadena limpiada
   */
  private function clean_import_str($str) {
    $str = trim($str);
    $str = str_replace('<br>', '|', $str);
    $str = iconv("UTF-8", "UTF-8//IGNORE", $str);
    $str = preg_replace('/(?>[\x00-\x1F]|\xC2[\x80-\x9F]|\xE2[\x80-\x8F]{2}|\xE2\x80[\xA4-\xA8]|\xE2\x81[\x9F-\xAF])/', ' ', $str);
    $str = preg_replace('/\s+/', ' ', $str);
    return $str;
  }

  /**
   * Limpia la cadena de la calle
   * @param  string $str calle
   * @return array      arreglo con las posiciones de la calle
   */
  private function clean_import_street($str) {
    $str = $this->clean_import_str($str);
    $matches = array();
    preg_match_all('/(.*)(\s)(\d+$)/', $str, $matches, PREG_SET_ORDER);
    return $matches;
  }

  /**
   * Parsea una URL de locaciones de bibliotecas
   * @param  string $library_url url de locaciones de bibliotecas
   * @return array               arreglo de ubicaciones
   */
  private function parse_libraries_json_into_locations($library_url) {
    $locations = array();
    $biblioteca_str = file_get_contents($library_url);
    if ($biblioteca_str !== false) {
      $bibliotecas = json_decode($biblioteca_str, true);
      foreach ($bibliotecas as $biblioteca) {
        $location = array();
        $location['name'] = $this->clean_import_str($biblioteca['biblioteca_nombre']);
        $location['slug'] = LM::slugify($location['name']);
        $location['description'] = null;
        $street = $this->clean_import_str($biblioteca['biblioteca_calle_numero']);
        $int_number = null;
        $ext_number = null;

        $temp_matches = $this->clean_import_street($street);
        if ($temp_matches >= 4) {
          if ($temp_matches[0][1] != null) {
            $street = $temp_matches[0][1];
          }
          if ($temp_matches[0][3] != null) {
            $ext_number = $temp_matches[0][3];
          }
        }

        $location['street'] = $street;
        $location['int_number'] = $int_number;
        $location['ext_number'] = $ext_number;
        $location['town'] = $this->clean_import_str($biblioteca['biblioteca_colonia']);
        $location['municipal_office'] = $this->clean_import_str($biblioteca['nom_mun']);
        $location['city'] = null;
        $location['state'] = $this->clean_import_str($biblioteca['nom_ent']);
        $location['zip'] = $this->clean_import_str($biblioteca['biblioteca_cp']);
        $latitude = null;
        $longitude = null;
        if ($biblioteca['gmaps_latitud'] != 0) {
          $latitude = $biblioteca['gmaps_latitud'];
        }
        if ($biblioteca['gmaps_longitud'] != 0) {
          $longitude = $biblioteca['gmaps_longitud'];
        }
        /*
        if ($latitude == null) {
          $readable_address = "{$street} {$ext_number}, {$location['town']}, {$location['municipal_office']}, {$location['state']} México, {$location['zip']}";
          echo "revisando: ".$location['name']."{$readable_address} <br>";
          $geocoding = $this->get_latlong_by_address($readable_address);
          var_dump($geocoding);
          sleep (5);
        }
        */
        $location['latitude'] = $latitude;
        $location['longitude'] = $longitude;
        $location['phone'] = $this->clean_import_str($biblioteca['biblioteca_telefono1']);
        $location['email'] = $this->clean_import_str($biblioteca['email']);
        $location['image'] = null;
        $location['webpage'] = $this->clean_import_str($biblioteca['biblioteca_pagina_web']);
        $location['facebook'] = null;
        $location['twitter'] = null;
        $location['contact'] = null;
        $location['attributes'] = null;
        $location['rating'] = null;
        $location['meta'] = null;
        $location['owner_id'] = null;
        $location['creator_id'] = null;
        $location['reference_id'] = "biblioteca_".$biblioteca['biblioteca_id'];

        $location['tags'] = array(1);
        $locations[] = $location;
      }
    }
    return $locations;
  }

  /**
   * Parsea una URL de locaciones de librerias
   * @param  string $bookshop_url url de locaciones de librerias
   * @return array               arreglo de ubicaciones
   */
  private function parse_bookshop_json_into_locations($bookshop_url) {
    header('Content-type: text/plain; charset=utf-8');
    $locations = array();
    $bookshop_str = file_get_contents($bookshop_url);
    if ($bookshop_str !== false) {
      $bookshops = json_decode($bookshop_str, true);
      foreach ($bookshops as $bookshop) {
        $location = array();
        $location['name'] = $this->clean_import_str($bookshop['libreria_nombre']);
        $location['slug'] = LM::slugify($location['name']);
        $location['description'] = null;
        $street = $this->clean_import_str($bookshop['libreria_calle_numero']);
        $int_number = null;
        $ext_number = null;

        $temp_matches = $this->clean_import_street($street);
        if ($temp_matches >= 4) {
          if ($temp_matches[0][1] != null) {
            $street = $temp_matches[0][1];
          }
          if ($temp_matches[0][3] != null) {
            $ext_number = $temp_matches[0][3];
          }
        }

        $location['street'] = $street;
        $location['int_number'] = $int_number;
        $location['ext_number'] = $ext_number;
        $location['town'] = $this->clean_import_str($bookshop['libreria_colonia']);
        $location['municipal_office'] = $this->clean_import_str($bookshop['nom_mun']);
        $location['city'] = null;
        $location['state'] = $this->clean_import_str($bookshop['nom_ent']);
        $location['zip'] = $this->clean_import_str($bookshop['libreria_cp']);
        $latitude = null;
        $longitude = null;
        if ($bookshop['gmaps_latitud'] != 0) {
          $latitude = $bookshop['gmaps_latitud'];
        }
        if ($bookshop['gmaps_longitud'] != 0) {
          $longitude = $bookshop['gmaps_longitud'];
        }
        /*
        if ($latitude == null) {

        }
        */
        $location['latitude'] = $latitude;
        $location['longitude'] = $longitude;
        $location['phone'] = $this->clean_import_str($bookshop['libreria_telefono1']);
        $location['email'] = $this->clean_import_str($bookshop['email']);
        $location['image'] = null;
        $location['webpage'] = $this->clean_import_str($bookshop['libreria_pagina_web']);
        $location['facebook'] = null;
        $location['twitter'] = null;
        $location['contact'] = null;
        $location['attributes'] = null;
        $location['rating'] = null;
        $location['meta'] = null;
        $location['owner_id'] = null;
        $location['creator_id'] = null;
        $location['reference_id'] = "libreria_".$bookshop['libreria_id'];
        $location['tags'] = array(2);
        $locations[] = $location;
      }
    }
    return $locations;
  }

  /**
   * Parsea el archivo de salas de lectura
   * @return array               arreglo de ubicaciones
   */
  private function parse_salas_de_lectura_json_into_location() {
    $locations = array();
    $salas_str = file_get_contents('/Applications/AMPPS/www/librosmexico.mx/application/classes/controller/xbackend/salas_de_lectura.json');

    if ($salas_str !== false) {
      $salas_temp = json_decode($salas_str, true);
      $salas_json = $salas_temp['features'];

      foreach ($salas_json as $sala) {

        $location = array();
        $location['name'] = $this->clean_import_str($sala['properties']['name']);
        $location['slug'] = LM::slugify($sala['properties']['name']);
        $location['description'] = $sala['properties']['description'];

        $matches = array();
        preg_match_all('/<br><br>(.*)<br><br>(.*)/', $location['description'] , $matches, PREG_SET_ORDER);
        var_dump($matches);
        
        /*
        $street = $this->clean_import_str($bookshop['libreria_calle_numero']);
        $int_number = null;
        $ext_number = null;

        $temp_matches = $this->clean_import_street($street);
        if ($temp_matches >= 4) {
          if ($temp_matches[0][1] != null) {
            $street = $temp_matches[0][1];
          }
          if ($temp_matches[0][3] != null) {
            $ext_number = $temp_matches[0][3];
          }
        }

        $location['street'] = $street;
        $location['int_number'] = $int_number;
        $location['ext_number'] = $ext_number;
        $location['town'] = $this->clean_import_str($bookshop['libreria_colonia']);
        $location['municipal_office'] = $this->clean_import_str($bookshop['nom_mun']);
        $location['city'] = null;
        $location['state'] = $this->clean_import_str($bookshop['nom_ent']);
        $location['zip'] = $this->clean_import_str($bookshop['libreria_cp']);
        $latitude = null;
        $longitude = null;
        if ($bookshop['gmaps_latitud'] != 0) {
          $latitude = $bookshop['gmaps_latitud'];
        }
        if ($bookshop['gmaps_longitud'] != 0) {
          $longitude = $bookshop['gmaps_longitud'];
        }

        if ($latitude == null) {

        }

        $location['latitude'] = $latitude;
        $location['longitude'] = $longitude;
        $location['phone'] = $this->clean_import_str($bookshop['libreria_telefono1']);
        $location['email'] = $this->clean_import_str($bookshop['email']);
        $location['image'] = null;
        $location['webpage'] = $this->clean_import_str($bookshop['libreria_pagina_web']);
        $location['facebook'] = null;
        $location['twitter'] = null;
        $location['contact'] = null;
        $location['attributes'] = null;
        $location['rating'] = null;
        $location['meta'] = null;
        $location['owner_id'] = null;
        $location['creator_id'] = null;
        $location['id_helper'] = "libreria_".$bookshop['libreria_id'];
        $location['tags'] = array(2); */
        $locations[] = $location;
      }
    }
    return $locations;
  }

  /**
   * Realiza la importación de todos los datos
   */
  public function action_import_all() {
    // header('Content-Type: application/json; charset=UTF-8');
    $locations = array();
    // 0_biblioteca_directorio.json -> 33_biblioteca_directorio.json (34)
    for ($i = 0; $i < 1; $i++) {
      $library_url = "http://sic.gob.mx/opendata/d/{$i}_biblioteca_directorio.json";
      $temp_locations = $this->parse_libraries_json_into_locations($library_url);
      // var_dump($temp_locations);
      $locations = array_merge($locations, $temp_locations);
    }
    
    // 0_libreria_directorio.json -> 32_libreria_directorio.json
    for ($i = 0; $i < 33; $i++) {
      $library_url = "http://sic.gob.mx/opendata/d/{$i}_libreria_directorio.json";
      $temp_locations = $this->parse_bookshop_json_into_locations($library_url);
      // var_dump($temp_locations);
      $locations = array_merge($locations, $temp_locations);
    }

    // $locations = $this->parse_salas_de_lectura_json_into_location();

    $result['locations'] = $locations;
    
    $results = array();
    $result['inserted_ids'] = array();
    foreach ($locations as $location) {
      list($location_id, $affected_rows) = Model::factory('atlas')->addLocation($location['name'], 
        $location['description'], 
        $location['street'], 
        $location['int_number'], 
        $location['ext_number'], 
        $location['town'], 
        $location['municipal_office'], 
        $location['city'], 
        $location['state'], 
        $location['zip'], 
        $location['latitude'], 
        $location['longitude'], 
        $location['phone'], 
        $location['email'], 
        $location['image'], 
        $location['webpage'], 
        $location['facebook'],
        $location['twitter'], 
        $location['contact'],
        null,
        null, 
        null, 
        $location['reference_id'], 
        null, 
        null);
      if ($location_id) {
        foreach ($location['tags'] as $tag) {
          // Model::factory('atlas')->addTagToLocation($tag, $location_id);
        }
      }
      $result['inserted_ids'][] = $location_id;
    }
    // print json_encode($result, true);
    exit;
  }
}