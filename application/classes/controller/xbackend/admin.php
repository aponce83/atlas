<?php defined('SYSPATH') or die('No direct script access.');

class controller_xbackend_admin extends Controller_Xbackend_Template {
  
  public function before() {
    parent::before();
  }
  
  public function action_login() {
    if ($this->user) {
      Request::current()->redirect('/admin/home');
    }
    $this->template = 'xbackend/admin/login';
    $this->template = View::factory($this->template);
    $data['error'] = Session::instance()->get_once('admin_login_error');
    $this->template->data = $data;
  }

  public function action_do_login() {
    $username = isset($_POST['username']) ? $_POST['username'] : NULL;
    $password = isset($_POST['password']) ? $_POST['password'] : NULL;
    if ($username === NULL ||
      $password === NULL ) {
      Request::current()->redirect('/admin');
    }
    if (Auth::instance()->login($username, $password, false)) {
      Security::token(TRUE);
      Request::current()->redirect('/admin/home');
    } else {
      Session::instance()->set('admin_login_error', 'not_found');
      Request::current()->redirect('/admin');
    }
  }

  public function action_logout() {
    Auth::instance()->logout();
    Request::current()->redirect('/admin/login');
  }

  public function action_home() {
    $data['page_title'] = 'Dashboard';
    $data['user'] = $this->user;
    $this->template->data = $data;
    $this->template->content->data = $data;
  }
}