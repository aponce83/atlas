<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Xbackend_Template extends Controller_Template {
  // Definimos el template principal
  public $template = 'xbackend/layout';

  public function __construct(Request $request, Response $response) {
    parent::__construct($request, $response);
    $this->user = Auth::instance()->get_user();
    // Obtenemos el directorio
    $this->_directory  = Request::$current->directory();
    $this->_controller = Request::$current->controller();
    $this->_action     = Request::$current->action();
    // Si no tiene sesión redirecciona al usuario
    if (!$this->user && 
      ($this->_action != 'login' && $this->_action != 'do_login')) {
      Session::instance()->set('admin_login_error', 'not_found');
      Request::current()->redirect('/admin');
    }
  }

  public function before() {
    parent::before();
    // Si la petición no es por ajax, entonces renderear
    $this->auto_render = !$this->request->is_ajax();
    if ($this->auto_render) {
      // Busco el view en la ruta
      $view = Request::$current->directory().'/'.Request::$current->controller().'/'.Request::$current->action();
      if (Kohana::find_file('views', $view)) {
        // Asigno el usuario
        $this->template->user = $this->user;
        // Creo el contenido
        $this->template->content = View::factory($view);
        // Envío el usuario al contenido
        $this->template->content->user = $this->user;
      }
    }
  }

  public function after() {
    parent::after();
  }
}