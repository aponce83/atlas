<?php defined('SYSPATH') or die('No direct script access.');

class controller_xbackend_api extends Controller_Xbackend_Template {
  
  public function before() {
    parent::before();
  }

  /**
   * Indice
   */
  public function action_index() {
    $data = array();
    $data['page_title'] = 'API';
    $data['api_users'] = Model::factory('Api_User')->get_all();

    $data['dont_show'] = array('password');
    $data['dont_edit'] = array('trivia_id',
      'img_preview',
      'img_display',
      'created_on',
      'updated_on');

    $this->template->data = $data;
    $this->template->content->data = $data;
  }

  /**
   * Usuarios en espera de aprobación
   */
  public function action_waiting() {
    $data = array();
    $data['page_title'] = 'API Pendientes';
    $data['api_users'] = Model::factory('Api_User')->get_pending();

    $this->template->data = $data;
    $this->template->content->data = $data;
  }

  /**
   * Aprobación de usuarios
   */
  public function action_approval() {
    $api_user_id = $this->request->post('api_user_id');
    if ($api_user_id == null) {
      Request::current()->redirect('/admin/api/waiting');
    }
    Model::factory('Api_User')->make_approval($api_user_id);
    $api_user = Model::factory('Api_User')->get_by_id($api_user_id);

    Model::factory('Api_Key')->create_api_keys($api_user['id'], $api_user['created_on']);

    Request::current()->redirect('/admin/api');
  }
}