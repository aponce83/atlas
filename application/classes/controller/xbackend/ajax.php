<?php defined('SYSPATH') or die('No direct script access.');

class controller_xbackend_ajax extends Controller_Xbackend_Template {
  
  public function before() {
    parent::before();
  }
  /**
   * Regresa la cantidad de usuarios registrados en 
   * un intervalode fechas dado
   */
  public function action_get_amount_of_users_by_date() {

    $result = array();

    $to_string = isset($_GET['to']) ? $_GET['to'] : null;
    $from_string = isset($_GET['from']) ? $_GET['from'] : null;

    if ($to_string === null || $from_string === null) {
      $today = new DateTime();
      $to_string = $today->format('Y-m-d');
      $interval = new DateInterval('P1M');
      $from_string = $today->sub($interval)->format('Y-m-d');
    }

    $data = Model::factory('Userfront')->get_amount_of_users_by_date($from_string, $to_string);

    $result['args'] = array('from' => $from_string,
      'to' => $to_string);
    $result['res'] = $data;
    print json_encode($result, true);
    exit;
  }

  /**
   * Regresa la cantidad de libros vistos en una fecha
   */
  public function action_get_book_tracking_by_date() {
    $result = array();

    $to_string = isset($_GET['to']) ? $_GET['to'] : null;
    $from_string = isset($_GET['from']) ? $_GET['from'] : null;

    if ($to_string === null || $from_string === null) {
      $today = new DateTime();
      $to_string = $today->format('Y-m-d');
      $interval = new DateInterval('P1M');
      $from_string = $today->sub($interval)->format('Y-m-d');
    }

    $data = Model::factory('Book')->get_book_tracking($from_string, $to_string);
    foreach ($data as &$book) {
      $book['link'] = '<a href="'.URL::base().'libros/'.$book['book_id'].'" target="_blank">'.$book['book_id'].'</a>';
      $meta = Model::factory('Book')->getBookLimitedInfo($book['book_id'], array('titulo'));
      $book['titulo'] = null;
      if ($meta) {
        $book['titulo'] = $meta[0]['titulo'];  
      }
    }
    unset($book);
    
    $result['args'] = array('from' => $from_string,
      'to' => $to_string);
    $result['res'] = $data;
    print json_encode($result, true);
    exit;
  }

  /**
   * Regresa las métricas de la plataforma
   */
  public function action_get_platform_metrics() {
    $data = array();
    $data['amount_of_users'] = Model::factory('userfront')->get_amount_of_users();
    $data['amount_of_discussions'] = Model::factory('discuss')->get_amount_of_discussions();
    $data['amount_of_trivias'] = Model::factory('trivia')->get_amount_of_answered_trivias();
    $data['amount_of_lists'] = Model::factory('list')->get_amount_of_lists_created_by_users();
    $result = array();
    $result["res"] = $data;
    print json_encode($result, true);
    exit;
  }

  /**
   * Regresa la información de las búsquedas en la plataforma agrupada
   * por la cadena
   */
  public function action_get_search_info_in_groups() {
    $result = array();

    $to_string = isset($_GET['to']) ? $_GET['to'] : null;
    $from_string = isset($_GET['from']) ? $_GET['from'] : null;
    $par = isset($_GET['par']) ? $_GET['par'] : null;
    $location = isset($_GET['location']) ? $_GET['location'] : null;

    if ($to_string === null || $from_string === null) {
      $today = new DateTime();
      $to_string = $today->format('Y-m-d');
      $interval = new DateInterval('P1M');
      $from_string = $today->sub($interval)->format('Y-m-d');
    }
    
    $data = Model::factory('search')->get_search_info_in_groups($from_string,
      $to_string,
      $par,
      $location);

    $result['args'] = array('from' => $from_string,
      'to' => $to_string,
      'par' => $par,
      'location' => $location);
    $result['res'] = $data;
    print json_encode($result, true);
    // Model::factory('search')->update_all_the_problems();
    exit;
  }

}