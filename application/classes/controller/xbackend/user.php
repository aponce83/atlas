<?php defined('SYSPATH') or die('No direct script access.');

class controller_xbackend_user extends Controller_Xbackend_Template {
  
  public function before() {
    parent::before();
  }

  /**
   * Indice
   */
  public function action_index() {
    $data = array();
    $data['page_title'] = 'Usuarios';
    $data['userfronts'] = Model::factory('userfront')->get_all();

    $data['is_super'] = strtolower($this->user['role']['name']) === 'super';

    $data['dont_show'] = array('calle', 
      'num_exterior', 
      'num_interior', 
      'id_estado', 
      'id_municipio', 
      'colonia', 
      'cod_postal', 
      'phone', 
      'birthday', 
      'work');

    if (!$data['is_super']) {
      $data['dont_show'] = array_merge($data['dont_show'], array(
        'type',
        'fbid',
        'password',
        'score',
        'editorial',
        'bio',
        'privacy_id',
        'last_modified',
        'date_created',
        'status',
        'deleted',
        'created',
        'last_session',
        'last_login',
        'last_login2',
        'tooltip_status',
        'robot',
        'log_id'
        ));
    }

    $data['dont_edit'] = array('id',
      'fbid',
      'password',
      'last_modified',
      'date_created',
      'status',
      'deleted',
      'created',
      'last_session',
      'last_login',
      'last_login2',
      'tooltip_status');
    if (!$data['is_super']) {
      $data['dont_edit'] = array_merge($data['dont_edit'], array(
        'fullname',
        'email',
        'image',
        ));
    }

    $this->template->data = $data;
    $this->template->content->data = $data;
  }

  /**
   * Actualización de la información
   */
  public function action_update() {
    $pk = $_POST['pk'];
    $name = $_POST['name'];
    $value = $_POST['value'];
    
    if (strtolower($name) != 'id') {
      $response = Model::factory('userfront')->updateValue($pk, $name, $value);
      if (!$response) {
        $this->response->status(400);
        echo "No se pudo actualizar";
      }
    } else {
      $this->response->status(400);
      echo "No se puede actualizar el id";
    }
  }

  /**
   * Migrar la tabla de amigos a relaciones
   */
  public function action_migrate_friends() {
    
    $query = <<<EOT
    SELECT * 
    FROM pi_friend
    WHERE 1
EOT;
    $result = DB::query(Database::SELECT, $query)
      ->execute()
      ->as_array();

    foreach ($result as $res) {
      if ($res['status'] == 'aceptado') {
        echo "Model::factory('relationship')->do_follow_user({$res['userfront_id']}, {$res['friend_id']}); <br>";
        Model::factory('relationship')->do_follow_user($res['userfront_id'], $res['friend_id']);
        echo "Model::factory('relationship')->do_follow_user({$res['friend_id']}, {$res['userfront_id']}); <br>";
        Model::factory('relationship')->do_follow_user($res['friend_id'], $res['userfront_id']);
      } else if ($res['status'] == 'en espera') {
        echo "Model::factory('relationship')->do_follow_user({$res['userfront_id']}, {$res['friend_id']}); <br>";
        Model::factory('relationship')->do_follow_user($res['userfront_id'], $res['friend_id']);
      }
    }

    exit;
  }

  /**
   * Le asigna los trofeos a todos los usuarios
   */
  public function action_give_trophies() {
    $userfronts = Model::factory('userfront')->get_all();
    $relationship_model = Model::factory('relationship');
    $i = 0;
    foreach ($userfronts as $userfront) {
      $amount_of_followers = $relationship_model->get_amount_of_followers($userfront['id']);
      if ($amount_of_followers >= 1 && $amount_of_followers <= 9) {
        Model::factory('score')->addAward( $userfront['id'], 'A17');
      }
      if ($amount_of_followers >= 10 && $amount_of_followers <= 49) {
        Model::factory('score')->addAward( $userfront['id'], 'A18');
      } 
      if ($amount_of_followers >= 50 && $amount_of_followers <= 99) {
        Model::factory('score')->addAward( $userfront['id'], 'A19');
      }
      if ($amount_of_followers >= 100) {
        Model::factory('score')->addAward( $userfront['id'], 'A20');
      }
      if ($amount_of_followers > 0) {
        echo "{$i} = $amount_of_followers<br>";
        $i++;
      }
    }
    exit;
  }
}