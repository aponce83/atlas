<?php defined('SYSPATH') or die('No direct script access.');

class controller_xbackend_menu extends Controller_Xbackend_Template {
  
  public function before() {
    parent::before();
  }

  /**
   * Indice de los menus
   */
  public function action_index() {
    $data = array();
    $data['page_title'] = 'Menú';

    $data['menu'] = Model::factory('menu')->get_all();

    $data['is_super'] = strtolower($this->user['role']['name']) === 'super';

    $this->template->data = $data;
    $this->template->content->data = $data;
  }

  /**
   * Actualización de la información
   */
  public function action_update() {
    $pk = $_POST['pk'];
    $name = $_POST['name'];
    $value = $_POST['value'];
    
    if (strtolower($name) != 'id') {
      $response = Model::factory('menu')->updateValue($pk, $name, $value);
      if (!$response) {
        $this->response->status(400);
        echo "No se pudo actualizar";
      }
    } else {
      $this->response->status(400);
      echo "No se puede actualizar el id";
    }
  }
}