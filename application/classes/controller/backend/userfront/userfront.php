<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Contento Backend Pages controller.
 *
 * @package    Contento/Backend
 * @category   Controllers
 * @author     Abargon
 * @copyright  (c) 2013 Abargon
 * @license    http://abargon.com
 */
class Controller_Backend_Userfront_Userfront extends Controller_Backend_Template {
	
	public function before()
	{
		parent::before();
		
		$this->model = new Model_Userfront;
		$this->site_id = 1; // ID site CMM
	}
	
	public function action_index(){
		$data['all_users'] = $this->model->get_all();
		$this->template->content->data = $data;
	}

	public function action_form() {
		$response = array('status' => 'error');
		if ($_POST) {
			$userfront_id = $_POST['userfront_id'];
			$fullname = $_POST['fullname'];
			$result = $this->model->update_fullname(array('id' => $userfront_id, 
				'fullname' => $fullname));
			if ($result == 1) {
				$response['status'] = 'success';
			}
		}
		print json_encode($response);
		exit;
	}

}