<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Contento Backend Pages controller.
 *
 * @package    Contento/Backend
 * @category   Controllers
 * @author     Abargon
 * @copyright  (c) 2013 Abargon
 * @license    http://abargon.com
 */
class Controller_Backend_Dashboard_Dashboard extends Controller_Backend_Template {
	
	public function before()
	{
		parent::before();
		
		$this->model = new Model_Dashboard;
	}
	
	public function action_index(){
		$text     = Arr::get($_GET, 'text', '');
		$status   = Arr::get($_GET, 'status', -1);

		$userfront_model = Model::factory('Userfront');
		$data['amount_of_users_by_date']  = $userfront_model->get_amount_of_users_by_date();
		$data['last_popular_books'] = Model::factory('Book')->admin_get_popular();

		$this->template->content->data = $data;
	}
	
	public function action_language(){
		$data['language_id'] = ($data['language_id'])?: $_GET['language_id'];
		Session::instance()->set('language_id', $data['language_id']);
		Request::current()->redirect($_SERVER['HTTP_REFERER']);
	}
	

}