<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Contento Frontend Homepage controller.
 *
 * @package    Contento/Frontend
 * @category   Controllers
 * @author     Abargon
 * @copyright  (c) 2013 Abargon
 * @license    http://abargon.com
 */
class Controller_Frontend_Book extends Controller_Frontend_Template {
	public function before(){
		parent::before();
		$this->book_model = Model::factory('book');
		$this->list_model = Model::factory('list');
		$this->discuss_model = Model::factory('discuss');
		$this->quote_model = Model::factory('quote');
		$this->user = Session::instance()->get('user_front');
	}

	public function action_detail(){
		$data = array();
		$id = $this->request->param('detail');
		$type = 1;

		if(!ctype_digit($id)){
			Request::current()->redirect('/');	
		}
		$fields = 'referencia, isbn, codigo_barras, subtitulo, sinopsis_txt, sinopsis, coleccion, editorial, coleccion, encuadernacion, fecha_publicacion, paginas';
		$fields = $fields.', referencia, peso, alto, ancho, grosor, edicion, reimpresion, isbn_spl, isbn_coleccion, isbn_coeditor, isbn_fasciculo, no_volumen, no_ilustraciones, curso, asignatura, nacional, origen';

		$data['images_path'] = 'http://pro.librosmexico.mx/portadas/';
		
		$data['book'] = $this->book_model->get_book(array('book_id' => $id,'fields' => $fields, 'user_id' => $this->user['id']));

		$device = Model::factory('device')->detect_device();
		if($device==1)	$data['book']['imagen'] = $data['book']['img_chica'];
		if($device==2)	$data['book']['imagen'] = $data['book']['img_mediana'];
		if($device==3)	$data['book']['imagen'] = $data['book']['img_grande'];

		//si no hay contenido, entonces se redirige al home
		if( !isset($data['book']['id']) )
		{
			Request::current()->redirect('/');
		}
		if (isset($data['book']['origen'])) {
			$origin = trim($data['book']['origen']);
			$origen = strtoupper($origin);
			if ( $origin != 'L' &&
			 	$origin != 'E' ) {
				Request::current()->redirect('/');
			}
		}

		$data['list'] = $this->list_model->getByUserSimple($this->user['id']);
		$data['rank'] = $this->book_model->getBookRank($data['book']['id']);
		$data['random_quote'] = $this->quote_model->fetch_random_quote();			
		$data['user-rank'] = $this->book_model->getUserBookRank($data['book']['id'],$this->user['id']);

		$data['discuss-counter'] = $this->discuss_model->countDiscuss($data['book']['id']);

		$data['lang'] = $this->request->param('lang');	
		$data['files_book_path'] = 'http://pro.librosmexico.mx/adjuntos/';	

		// Agrega el libro a los populares
		$popular_type = ($this->user) ? 2 : 1;
		$this->book_model->add_popular(array('book_id' => $data['book']['id'], 
			'type' => $popular_type));

		// Obtiene el listado de usuarios robot
		$robot_users = Model::factory('userfront')->get_robot_users();
		$data['robot_users'] = $robot_users;
		$robot_discussions = $this->discuss_model->getRobotDiscussions($data['book']['id']);
		$data['robot_discussions'] = $robot_discussions;
		
		$head_data = $this->template->head->data;
		$head_data["main_title"] = "{$data['book']['titulo']}";
		$head_data["og_title"] = "{$data['book']['titulo']} | LIBROSMÉXICO.MX";
		if ($data['book']['sinopsis']) {
			$head_data["main_description"] = "{$data['book']['sinopsis']}";
			$head_data["og_description"] = "{$data['book']['sinopsis']}";
		}

		if (filter_var($data['book']['img_grande'], FILTER_VALIDATE_URL)) {
			$head_data["og_image"] = $data['book']['img_grande'];
			$head_data["og_image_width"] = "400";
			$head_data["og_image_height"] = "600";
		}
		$head_data["og_url"] = URL::base(true)."libros/{$data['book']['id']}";
		$head_data["og_type"] = 'book';
		$authors_string = $this->get_authors_string($data['book']);
		$head_data['additional_metas'] = array(
				array('property' => 'book:author', 'content' => $authors_string),
				array('property' => 'book:isbn', 'content' => $data['book']['isbn']),
				array('property' => 'book:release_date', 'content' => $data['book']['fecha_colofon'])
			);

		foreach ($data['book']['keywords'] as $value) {
			$key = $value['palabra'];
			$head_data['additional_metas'][] = array(
				'property' => 'book:tag',
				'content' => $key);
		}

		$last_ranked = Model::factory('book')->getLastRatedBooks(3);

		$this->template->head->data = $head_data;

		$this->template->content->data = $data;
		$this->template->content->list_ranked = $last_ranked;

	}

	public function action_ajax_where_to_buy () {
		$result = array("status" => "OK",
			"message" => "");
		$data = array();
		$id = $this->request->param('detail');
		
		if(!ctype_digit($id)){
			$result = array("status" => "ERROR",
			"message" => "No existe el libro");
			print json_encode($result);
			exit;
		}
		$result['where_to_buy'] = $this->book_model->get_book_sources(array('book_id' => $id)); 
		print json_encode($result);
		exit;
	}

	public function action_ajax_related_books () {
		$result = array("status" => "OK",
			"message" => "");
		$data = array();
		$id = $this->request->param('detail');
		
		if(!ctype_digit($id)){
			$result = array("status" => "ERROR",
			"message" => "No existe el libro");
			print json_encode($result);
			exit;
		}
		$result['related_books'] = $this->book_model->getRelatedBooks(array('id' => $id)); 
		print json_encode($result);
		exit;
	}

	public function action_ajax_tracking () {
		$result = array("status" => "OK",
			"message" => "");
		$data = array();
		$id = $this->request->param('detail');
		
		if(!ctype_digit($id)){
			$result = array("status" => "ERROR",
			"message" => "No existe el libro");
			print json_encode($result);
			exit;
		}
		
		$userfront_id = null;
		if ($this->user) {
			$userfront_id = $this->user['id'];
		}

		$book = $this->book_model->get_book($id);
		if ($book) {
			$isbn = $book['isbn'];
			$cd = new CrawlerDetect;
			// revisa si no es un robot, sino lo es entonces 
			// almacena su consulta
			if(!$cd->isCrawler()) {
				$user_agent = $cd->getUserAgent();
			 	$this->book_model->set_book_tracking($id, $isbn, $userfront_id, $user_agent); 
			}
		}
		
		print json_encode($result);
		exit;
	}

	/**
	 * Borra la información del libro o libros
	 */
	public function action_delete_cache () {
		header('Content-Type: application/json');
		$result = array();
		$result['status'] = 'ERROR';
		$result['error'] = NULL;
		$books_id = $this->request->post('books_id');

		// Crea el arreglo de libros en caso de que no se haya enviado como tal
		if (!is_array($books_id)) {
			$books_id = array($books_id);
		}
		$result['books_id'] = $books_id;

		/**
		 * $currentDate = date("YmdHis");
		 * $id = 10;
		 * $seed = "5bf52f999f2eb610a60142bae42e8b80";
		 * $signature = md5("{$currentDate}{$id}{$llave}");
		 */
		

		$id = $this->request->post('id');
		$signature = $this->request->post('signature');
		$date = $this->request->post('date');

		if (!$id || !$signature || !$date) {
			Request::current()->redirect('/');
		}


		$valid = true;
		$valid = $this->_auth($id, $signature, $date);
		// Indica si es un usuario válido
		if (!$valid) {
			$result['error']['code'] = 1000;
			$result['error']['message'] = 'AUTH ERROR';
			print json_encode($result, true);
			exit;
		}
		
		$redis = Rediska_Manager::get();
		$redis_keys_to_delete = array();
		$deleted_books = array();
		foreach ($books_id as $book_id) {
			if (ctype_digit($book_id)) {
				$redis_key = LM::getRedisKey('m_get_book',array("id_libro"=> $book_id));
				if ($redis->exists($redis_key)) {
					$redis_keys_to_delete[] = $redis_key;
					$deleted_books[] = $book_id;
				}
			}
		}
		if ($redis_keys_to_delete) {
			$redis->delete($redis_keys_to_delete);
		}

		$result['status'] = 'SUCCESS';
		$result['deleted_books'] = $deleted_books;
		
		print json_encode($result, true);
		exit;
	}

	private function _auth($the_id, $the_signature, $the_date) {
		// El id de ellos
		if ($the_id != 10) {
			return false;
		}
		// Los datos que debería de tener en la base de datos
		$my_id = 10;
		$my_key = "5bf52f999f2eb610a60142bae42e8b80";
		$my_date = $the_date;

		// La firma
		$my_signature = md5("{$my_date}{$my_id}{$my_key}");
		
		// La llave
		$result = ($the_signature == $my_signature);
		return $result;
	}

	/**
	 * Obtiene un string con los autores de un libro
	 * @param  array $book l
	 * @return string       autores del libro
	 */
	private function get_authors_string($book) {
		$autores = "";
		$autor_comp = NULL;
		$author_count = 0;

		foreach ($book['autores'] as $autor) {
	    if ($autor['rol'] == 'Autor') {
				if ($author_count<3) {
				  if ($autores != NULL) {
			      $autores = $autores.', '.$autor['nombre'];
			      $autores_comp = $autores_comp.', '.$autor['nombre'];
				  } else { 
			      $autores = $autor['nombre'];
			      $autores_comp = $autor['nombre'];
				  }
				  $author_count++;
				} else {
				  $autores_comp = $autores_comp.', '.$autor['nombre'];                    
				  $author_count++;
				}   
	    }
		}

		if ($author_count > 3 &&
			$author_count < 6)
		{
		  $autores = $autores_comp;
		} else if ($author_count>3) {
			$autores = $autores.' y otros';
		}
		return $autores;
	}

}
