<?php 

session_start();

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\GraphUser;
use Facebook\GraphObject;
use Facebook\FacebookRequestException;

FacebookSession::setDefaultApplication($config['app_id'], $config['app_secret']); 
$_SESSION['secion_facebook'] = 0;
$redirect_url = URL::base(true).'registro-usuario';
$helper = new FacebookRedirectLoginHelper($redirect_url);
//$helper = new FacebookRedirectLoginHelper('http://localhost/librosmexico.mx/registro-usuario');

try {
	$session = $helper->getSessionFromRedirect();
	if ($session):
		$_SESSION['facebook'] = $session->getToken();
		header('Location: index.php');
	endif;
	if (isset($_SESSION['facebook'])):
		$session = new FacebookSession($_SESSION['facebook']);
		$request = new FacebookRequest($session, 'GET', '/me',
			  array(
			    'fields' => 'address,birthday,email,name'
			  ));
		$response = $request->execute();
		$graphObjectClass = $response->getGraphObject(GraphUser::className());
		$facebook_user = $graphObjectClass;
		$_SESSION['secion_facebook'] = 1;
		$_SESSION['base_url'] = URL::site(Request::detect_uri(), TRUE);
		$_SESSION['base_url'] = str_replace('/registro-usuario',"", $_SESSION['base_url']); 
		//Log::instance()->add(Log::NOTICE, $_SESSION['base_url']);
	endif;
} catch(FacebookRequestException $ex) {
  // When Facebook returns an error
} catch(\Exception $ex) {
  // When validation fails or other local issues
}