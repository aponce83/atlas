<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Frontend_Ajax extends Controller_Frontend_Template {

	public function before(){
		parent::before();
		$this->book_model = Model::factory('Book');
		$this->list_model = Model::factory('List');
		$this->discuss_model = Model::factory('Discuss');
		$this->denounce_model = Model::factory('Denounce');
		$this->userfront_model = Model::factory('Userfront');
		$this->user = Session::instance()->get('user_front');
	}

	public function action_link_facebook(){
		$response = array();
		$response['status'] = 'no';
		if($_POST){
      		//Log::instance()->add(Log::DEBUG,"FBID: ". $_POST['fbid']);
			$y = $this->userfront_model->linkFB($this->user['id'],$_POST['fbid']);
			if ($y) {
				$response['status'] = 'yes';
			}
		}
		print json_encode($response);
		exit;
	}

	/**
	 * Crea una discusión en el detalle del libro
	 */
	public function action_create_discuss(){
		$result = array();
		$result['status'] = 'no';
		$result['error'] = array('code' => null, 'message' => null);
		try {
			$topic = $this->request->post('topic');
			$description = $this->request->post('description');
			$book_id = $this->request->post('book_id');

			if (!($topic) ||
			!($description) ||
			!($book_id)) {
				throw new Exception("Faltan datos", 1001);
			}

			$userfront_id = null;
			if ($this->user) {
				$userfront_id = $this->user['id'];
			}

			$y = $this->discuss_model->create_discussion($topic, $description, 'book', $book_id, $userfront_id, false);

			if ($y) {
				$result['id'] = $y;
				$result['status'] = 'yes';
				$result['anonymous'] = ($userfront_id == null);
			}
		} catch (Exception $e) {
			$result['error']['code'] = $e->getCode();
			$result['error']['message'] = $e->getMessage();
		}
		print json_encode($result, true);
		exit;
	}

	/**
	 * Crea una discusión que fue iniciada por los robots
	 */
	public function action_create_discuss_default() {
		$result = array();
		$result['status'] = 'no';
		$result['error'] = array('code' => null, 'message' => null);
		try {
			$topic = $this->request->post('topic');
			$description = $this->request->post('description');
			$book_id = $this->request->post('book_id');
			$robot = $this->request->post('robot');
			$robotname = $this->request->post('robotname');
			$robotimage = $this->request->post('robotimage');

			if (!($topic) ||
			!($description) ||
			!($book_id)) {
				throw new Exception("Faltan datos", 1001);
			}
			$robot_id = $robot;

			$user = $this->userfront_model->get_user_by_id($robot_id);
			if ($user['robot'] == 1) {
				$userfront_id = $user['id'];
				$y = $this->discuss_model->create_discussion($topic, $description, 'book', $book_id, $userfront_id, false);
				if ($y) {
					$result['id'] = $y;
					$result['status'] = 'yes';
				}
			}
		} catch (Exception $e) {
			$result['error']['code'] = $e->getCode();
			$result['error']['message'] = $e->getMessage();
		}
		print json_encode($result, true);
		exit;
	}

	/**
	 * Obtiene una discusión
	 */
	public function action_retrieve_discuss(){
		$data['resource'] = 'discuss';
		$response = array();
		$response['status'] = 'no';
		if($_POST){
			$result =$this->discuss_model->getBookDiscuss($_POST['book'],$_POST['page'],$_POST['offset']);
			if ($result) {
				$response['status']  = 'yes';
				$response['discuss'] = $result;
				$response['likes'] = $this->discuss_model->retrieveLikes($data,$this->user['id']);
			}
		}
		print json_encode($response);
		exit;
	}

	/**
	 * Obtiene un comentario de una discusión
	 */
	public function action_retrieve_comment(){
		$data['resource'] = 'comment';
		$response = array();
		$response['status'] = 'no';
		if($_POST){
			$result =$this->discuss_model->getCommentDiscuss($_POST['discuss_id']);
			if ($result) {
				$response['status']  = 'yes';
				$response['comment'] = $result;
				$response['likes'] = $this->discuss_model->retrieveLikes($data,$this->user['id']);
			}
		}
		print json_encode($response);
		exit;
	}

	/**
	 * Obtiene las respuestas de un comentario
	 */
	public function action_retrieve_response(){
		$data['resource'] = 'comment';
		$response = array();
		$response['status'] = 'no';
		if($_POST){
			$result =$this->discuss_model->getResponseComment($_POST['comment_id']);
			if ($result) {
				$response['status']  = 'yes';
				$response['response'] = $result;
				$response['likes'] = $this->discuss_model->retrieveLikes($data,$this->user['id']);
			}
		}
		print json_encode($response);
		exit;
	}
	public function action_like_discuss(){
		$response = array();
		$response['status'] = 'no';
		if($_POST){
			$result =$this->discuss_model->likeDiscuss($_POST,$this->user['id']);
			if ($result) {
				$response['status']  = 'yes';
			}
		}
		print json_encode($response);
		exit;
	}
	public function action_unlike_discuss(){
		$response = array();
		$response['status'] = 'no';
		if($_POST){
			$result =$this->discuss_model->unlikeDiscuss($_POST,$this->user['id']);
			if ($result) {
				$response['status']  = 'yes';
			}
		}
		print json_encode($response);
		exit;
	}

	/**
	 * Comentar la discusión
	 */
	public function action_comment_discuss(){
		$result = array();
		$result['status'] = 'no';
		$result['error'] = array('code' => null, 'message' => null);
		try {
			if ($_POST) {

				$comment = $this->request->post('comment');
				$type = $this->request->post('type');
				$resource = $this->request->post('resource');

				if (!$comment ||
					!$type ||
					!$resource )
				{
					throw new Exception("Hacen falta datos", 1002);
				}

				$userfront_id = null;
				if ($this->user) {
					$userfront_id = $this->user['id'];
				}
				$resource_id = $resource;

				$y = $this->discuss_model->comment_discussion($comment, $resource_id, $userfront_id, $type);
				if ($y) {
					$result['id'] = $y;
					$result['status'] = 'yes';
					$result['anonymous'] = ($userfront_id == null);
				}
			} else {
				throw new Exception("La petición no incluía información en los encabezados", 1001);
			}
		} catch (Exception $e) {
			$result['error']['code'] = $e->getCode();
			$result['error']['message'] = $e->getMessage();
		}
		print json_encode($result, true);
		exit;
	}

	public function action_retrieve_ratings() {
		$result = array();
		$result['status'] = 'ERROR';
		$result['ratings'] = null;
		$book_id = isset($_POST['book_id']) ? $_POST['book_id'] : null;
		if ($book_id) {
			$limit = isset($_POST['limit']) ? $_POST['limit'] : null;
			$ratings = $this->book_model->getLastRatings($book_id, $limit);
			$result['status'] = 'SUCCESS';
			$result['ratings'] = $ratings;
		}
		print json_encode($result, true);
		exit;
	}

	public function action_tooltip() {
		if( isset($_POST['omitir']) )
		{
			//hacer el update del tooltip_status a 0
			Model::factory('userfront')->unsetTooltips($_SESSION['user_id']);
		}
		if( isset($_POST['update-tooltip']) )
		{
			//obtener el valor del update-tooltip
			$update_value = (int)$_POST['update-tooltip'];
			Model::factory('userfront')->updateTooltips( $_SESSION['user_id'], $update_value );
		}
	}

	public function action_has_file_to_download(){
		if($_POST && $_POST['book_id']){
			$y = $this->book_model->download_file($_POST['book_id']);
			if($y) print 'yes';
				else print 'no';
		}else print 'no';
	}

	public function action_validate_current_pass(){
		$response = array();
		$response['status'] = 'no';
		if($_POST){
			$y=Model::factory('Userfront')->verifyPass($this->user['id'],sha1(parent::x . $_POST['password']));
			if ($y) {
				$response['status'] = 'yes';
			}
		}
		print json_encode($response);
		exit;
	}

	public function action_verify_email_register() {
		$response = array();
		$response['status'] = 'no';
		if ($_POST) {
			$y=Model::factory('Userfront')->user_exists_by_email($_POST['email']);

			if (!$y) {
				$response['status'] = 'yes';
			}
		}
		print json_encode($response);
		exit;
	}

	public function action_verify_handle_register() {
		$response = array();
		$response['status'] = 'no';
		if ($_POST) {
			$y=Model::factory('Userfront')->user_exists_by_handle($_POST['handle']);

			if (!$y) {
				$response['status'] = 'yes';
			}
		}
		print json_encode($response);
		exit;
	}

	public function action_report_data() {
		$response = array();
		$response['status'] = 'no';

		if ($_POST) {
			$paraph = '';
			if($_POST['author'])
				$paraph = '<p><font style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><strong style="color:#3db29c" >Correción autor: </strong>'.$_POST['author'].'</font></p>';
			if($_POST['title'])
				$paraph = $paraph.'<p><font style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><strong style="color:#3db29c" >Correción titulo: </strong>'.$_POST['title'].'</font></p>';
			if($_POST['edit'])
				$paraph = $paraph.'<p><font style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><strong style="color:#3db29c" >Correción editorial: </strong>'.$_POST['edit'].'</font></p>';
			if($_POST['date'])
				$paraph = $paraph.'<p><font style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><strong style="color:#3db29c" >Correción fecha: </strong>'.$_POST['date'].'</font></p>';
			if($_POST['other'])
				$paraph = $paraph.'<p><font style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;"><strong style="color:#3db29c">Otras correcciones: </strong>'.$_POST['other'].'</font></p>';

			$subject = 'Reporte de Datos Incorrectos en LIBROSMÉXICO.MX';
			$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	                      <html xmlns="http://www.w3.org/1999/xhtml">
	                      <head>
	                      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	                      <title>Untitled Document</title>
	                      </head>
	                      <body>
	                          <table style="margin:20px auto 0 ;" width="600" align="center"  border="0" cellspacing="0" cellpadding="0">
	                        <tr>
	                        <td>
	                          <a style="display:block; clear:both; margin:0 0 30px;" href="http://librosmexico.mx" target="_blank"><img src="http://librosmexico.mx/assets/images/libros_logo.png" /></a>
	                            <h2 style="font-weight:normal;"><font style="font-family:Arial, Helvetica, sans-serif; font-size:18px;">¡Reporte de Datos Incorrectos LIBROSMÉXICO.MX</font></h2>
	                          <p><font style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">Se reportaron datos incorrectos del libro:<a style="color:#354f5e" href="http://librosmexico.mx/libros/'.$_POST['book'].'">'.$_POST['ogtitle'].'</a></font></p>
	                		  '.$paraph.'
	                        <a href="http://librosmexico.mx" target="_blank" style="color:#3db29c;text-decoration:none">http://www.librosmexico.mx</a>
	                          <p><font style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">Atentamente <br />
	                      LIBROSMÉXICO.MX</font></p>
	                         <div style="margin:40px 0 0 0; border-top:solid 2px #354f5e; padding:10px 0 0;">
	                           <p><font style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#606366;">Este correo electrónico se generó automáticamente. Por favor, no lo respondas.</font></p>
	                         </div>
	                         </td>
	                        </tr>
	                      </table>
	                      </body>
	                      </html>';
			$email = Email::factory($subject, $message, 'text/html')->from('no-reply@librosmexico.com.mx', 'LIBROSMÉXICO.MX')->to('contacto@librosmexico.mx');
			$email->send();


			if (!$y) {
				$response['status'] = 'yes';
			}
		}
		print json_encode($response);
		exit;
	}

	public function action_verify_fb_account() {
		$response = array();
		$response['status'] = 'no';
		if ($_POST) {
			$y=$this->userfront_model->verifyFB($_POST['fbid']);

			if ($y) {
				$response['status'] = 'yes';
			}
		}
		print json_encode($response);
		exit;
	}

	public function action_login_ajax() {
		$response = array();
		$response['status'] = 'no';
		if ($_POST) {
			$y = $this->userfront_model->login($_POST['email'], sha1(parent::x . $_POST['password']), null, null, 2);

			if ($y) {
				// revisamos si el usuario definió que se almacenara su sesión
				$remember_me = isset($_POST['remember_me']) ? $_POST['remember_me'] : false;
				if ($remember_me) {
					// se obtiene la información del usuario que acaba de loguearse
					$user = Session::instance()->get('user_front');
					$user_id = $user['id'];
					$autologin_identifier = Model::factory('autologin')->remember_me($user_id);
					Cookie::set('lmx_autologin_type', 'form', 864000);
					Cookie::set('lmx_autologin', $autologin_identifier, 864000);
				}

				$response['status'] = 'yes';
			}
		}
		print json_encode($response);
		exit;
	}

	public function action_login_fb_ajax() {
		$response = array();
		$response['status'] = 'no';
		if ($_POST) {
			$data['fullname'] = isset($_POST['name']) ? $_POST['name'] : null;
			$data['fbid'] = isset($_POST['fbid']) ? $_POST['fbid'] : null;
			$data['email'] = isset($_POST['email']) ? $_POST['email'] : null;


			if (!$data['fbid']) {
				$response['status'] = 'no';
				print json_encode($response);
				exit;
			}

			$data['image'] = "https://graph.facebook.com/v2.2/{$data['fbid']}/picture?type=large";

			// El usuario ya se ha creado entonces ahora iniciar la sesión
			if ($this->userfront_model->user_exists_by_fbid($data['fbid'])) {
				$login = $this->userfront_model->login($data['email'], null, $data['fullname'], $data['fbid'], 1);
				// Si se pudo loguear
				if ($login) {
					// Obtener la variable de sesión
					$user = Session::instance()->get('user_front');

					$user_id = $user['id'];
					$autologin_identifier = Model::factory('autologin')->remember_me($user_id);
					$secure_only = true;
					if (Kohana::$environment == Kohana::DEVELOPMENT) {
						$secure_only = false;
					}

					Cookie::set('lmx_autologin_type', 'facebook', 864000);
					Cookie::set('lmx_autologin', $autologin_identifier, 864000);

					// Actualizar los datos con los de facebook
					$this->userfront_model->update($user['id'],
						array('image' => $data['image'], 'fullname' => $data['fullname']));
					// Resetear la sesión actual
					$this->userfront_model->reset_current_session();
					// Responder positivo
					$response['status'] = 'yes';
				}
			} else {
				$response['status'] = 'doesnt_exists';
			}
		}
		print json_encode($response);
		exit;
	}
	public function action_delete_acount(){
		$response = array();
		$response['status'] = 'no';
		if($_POST){
			if($_POST['email'] == $this->user['email']) {
				if($_POST['set']){
				   $validacion = $this->userfront_model->verifyPass( $this->user['id'],sha1(parent::x . $_POST['password']));
					if ($validacion) {
						$user_status = $this->userfront_model->delete_user($this->user['id']);
						$this->userfront_model->logout();
						$response['status'] = 'yes';
					}
				}
				else{
					$deleted = $this->userfront_model->delete_user($this->user['id']);
					$this->userfront_model->logout();
					$response['status'] = 'yes';
				}
			}
		}
		print json_encode($response);
		exit;
	}
	public function action_save_data() {
		$response = array();
		$response['status'] = 'no';
		if ($_POST) {

			if($_POST['set']){
				if($_POST['type']=='password'){
					$_POST['old']=sha1(parent::x . $_POST['old']);
					$_POST['password']=sha1(parent::x . $_POST['password']);
				}
			}
			if($_POST['bio']) {
				//se cambia descripcion personal, si es la primera vez se debe agregar trofeo
				//trofeo por añadir una descripcion al perfil
	            $awardKey = 'A3';
	           	$insertAdward = Model::factory('score')->addAward( $this->user['id'], $awardKey );

	           	if($insertAdward!=false)
	           	{
	           		$type = 'award';
           			$notStatus = 'pendig';
           			Model::factory('notification')->pushNotification( $this->user['id'], $notStatus, $type, $awardKey );

           			//se notifica en el muro que gano trofeo
	           		$type = 'award';
	           		$id1 = 3;
	           		$id2 = 0;
	           		Model::factory('social')->addActivity( $type, $id1, $id2, $this->user['id'] );
	           	}

			}
	        $y = $this->userfront_model->update($this->user['id'],$_POST);
			if ($y) {
				$response['status'] = 'yes';
			}
		}
		print json_encode($response);
		exit;
	}

	public function action_register_fb(){
		$response = array();
		$response['status'] = 'no';
		if ($_POST) {

			$data['fullname'] = isset($_POST['name']) ? $_POST['name'] : null;
			$data['fbid'] = isset($_POST['fbid']) ? $_POST['fbid'] : null;
			$data['email'] = isset($_POST['email']) ? trim($_POST['email']) : null;

			if (!$data['fullname'] ||
				!$data['fbid'] ) {
				print json_encode($response);
				exit;
			}

			$data['image'] = "https://graph.facebook.com/v2.2/{$data['fbid']}/picture?type=large";

			// El usuario no existe ni con su fbid ni con su correo
			if (!$this->userfront_model->user_exists_by_fbid($data['fbid'])) {
				$user_data = $this->userfront_model->insert($data); //regresa id del nuevo usuario
				$result = $this->list_model->defaultList($user_data);

				$query = DB::update('userfront')->set(array('handle' => 'lector-'.base64_encode($user_data)))->where('id', '=', $user_data)->execute();


				$awardKey = 'A1';
				Model::factory('score')->addAward( $user_data, $awardKey );
				$activityKey = 'S1';
				Model::factory('score')->addPoints( $user_data, $activityKey, -1 );


				$type = 'award';
				$notStatus = 'pendig';
				Model::factory('notification')->pushNotification( $user_data, $notStatus, $type, $awardKey );

				// Enviar el correo electrónico de bienvenida
			} else if ($data['email'] !== null || $data['email'] != '') {
				if ($this->userfront_model->user_exists_by_email($data['email'])) {
					// El usuario solo existe con su correo
					$user = $this->userfront_model->get_user_by_email($data['email']);
					// Conectar su cuenta
					if ($user) {
						$this->userfront_model->linkFB($user['id'], $data['fbid']);
					}
				}
			}

			// El usuario ya se ha creado entonces ahora iniciar la sesión
			if ($this->userfront_model->user_exists_by_fbid($data['fbid'])) {
				$login = $this->userfront_model->login($data['email'], null, $data['fullname'], $data['fbid'], 1);
				// Si se pudo loguear
				if ($login) {
					// Obtener la variable de sesión
					$user = Session::instance()->get('user_front');
					// Actualizar los datos con los de facebook
					$this->userfront_model->update($user['id'],
						array('image' => $data['image'], 'fullname' => $data['fullname']));
					// Resetear la sesión actual
					$this->userfront_model->reset_current_session();
					// Si viene de un referido le envía la invitación
					$lmxid = isset($_POST['lmxid']) ? $_POST['lmxid'] : NULL;
					if ($lmxid) {
						$lmxid = base64_decode($lmxid);
						if (ctype_digit($lmxid)) {
							if ($this->userfront_model->get_user_by_id($lmxid)) {
								$user = Session::instance()->get('user_front');
								Model::factory('relationship')->do_follow_user($user['id'], $lmxid);
								Model::factory('relationship')->do_follow_user($lmxid, $user['id']);
							}
						}
					}

					// Autologear al usuario
					$autologin_identifier = Model::factory('autologin')->remember_me($user['id']);
					Cookie::set('lmx_autologin_type', 'facebook', 864000);
					Cookie::set('lmx_autologin', $autologin_identifier, 864000);

					// Responder positivo
					$response['status'] = 'yes';
				}
			}
		}
		print json_encode($response);
		exit;
	}

	public function action_register() {
		$response = array();
		$response['status'] = 'no';
		$response['error'] = array('code' => null, 'message' => null);

		if ($_POST) {
			$data['fullname'] = isset($_POST['name']) ? $_POST['name'] : null;
			$data['email'] = isset($_POST['email']) ? $_POST['email'] : null;
			$thePassword = isset($_POST['password']) ? $_POST['password'] : null;

			if ($data['fullname'] == null ||
				$data['email'] == null ||
				$thePassword == null)
			{
				$response['error']['code'] = 1002;
				$response['error']['message'] = 'No se enviaron todos los campos';
			}

			if ($this->userfront_model->get_user_by_email($data['email'])) {
				$response['error']['code'] = 1001;
				$response['error']['message'] = 'El correo electrónico ya se encuentra asignado a otro usuario';
			}

			// No se encontró ningún error
			if ($response['error']['code'] === null) {
				$data['password'] = sha1(parent::x . $thePassword);
				$user_data = $this->userfront_model->insert($data);
				$result = $this->list_model->defaultList($user_data);
				if($user_data) {

					//se asigna trofeo por el registro en la plataforma
					//se notifica en el muro el trofeo obtenido
					$login = $this->userfront_model->login($data['email'], sha1(parent::x . $_POST['password']), null, null, 2);
					$autologin_identifier = Model::factory('autologin')->remember_me($user_data);
					Cookie::set('lmx_autologin_type', 'form', 864000);
					Cookie::set('lmx_autologin', $autologin_identifier, 864000);
					$subject2 = 'Ingresa a tu cuenta en LIBROSMÉXICO.MX';
					$message2 = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			                      <html xmlns="http://www.w3.org/1999/xhtml">
			                      <head>
			                      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			                      <title>Untitled Document</title>
			                      </head>

			                      <body>
			                          <table style="margin:20px auto 0 ;" width="600" align="center"  border="0" cellspacing="0" cellpadding="0">
			                        <tr>
			                        <td>
			                          <a style="display:block; clear:both; margin:0 0 30px;" href="https://librosmexico.mx" target="_blank"><img src="//librosmexico.mx/assets/images/libros_logo.png" /></a>
			                            <h2 style="font-weight:normal;"><font style="font-family:Arial, Helvetica, sans-serif; font-size:18px;">¡Bienvenido a LIBROSMÉXICO.MX, <strong>'.$data['fullname'].'!</strong></font></h2>
			                          <p><font style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">¡Gracias por registrarte! Tu cuenta ha sido Activada, ingresa a ella para comenzar a disfrutar de los beneficios que te da
			                      LIBROSMÉXICO.MX.</font></p>

			                        <a href="https://librosmexico.mx" target="_blank" style="color:#3db29c;text-decoration:none">https://www.librosmexico.mx</a>
			                          <p><font style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">Atentamente <br />
			                      LIBROSMÉXICO.MX</font></p>
			                         <div style="margin:40px 0 0 0; border-top:solid 2px #354f5e; padding:10px 0 0;">
			                           <p><font style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#606366;">Este correo electrónico se generó automáticamente. Por favor, no lo respondas.</font></p>
			                         </div>
			                         </td>
			                        </tr>
			                      </table>
			                      </body>
			                      </html>
								';
					//$email = Email::factory($subject2, $message2, 'text/html')->from('no-reply@librosmexico.com.mx', 'LIBROSMÉXICO.MX')->to($data['email']);
						//$email->send();

					/*$userId = $user_data;
					$points = 100;

					Model::factory('score')->addPoints( $userId, $points );

					$awardId = 1;
					Model::factory('score')->addAward( $userId, $awardId );*/

					$awardKey = 'A1';
					Model::factory('score')->addAward( $user_data, $awardKey );

					$activityKey = 'S1';
					Model::factory('score')->addPoints( $user_data, $activityKey, -1 );

					$type = 'award';
					$notStatus = 'pendig';
					Model::factory('notification')->pushNotification( $user_data, $notStatus, $type, $awardKey );

					//se notifica en el muro que gano trofeo
					$type = 'award';
					$id1 = 1;
					$id2 = 0;
					Model::factory('social')->addActivity( $type, $id1, $id2, $user_data );
				}
				if ($result) {
					$response['status'] = 'yes';
				}
			}
		}
		print json_encode($response);
		exit;
	}

		public function action_appendList(){

			$theirListId = $this->request->param('id_mylist');
			$myListId = $this->request->param('id_theirlist');
			//Log::instance()->add(Log::ERROR, "EN ACTION APPENDLIST EL ID ES: " . $myListId );
			$y=Model::factory('List')->appendList( $myListId, $theirListId );

			if($y) print 'yes';
				else print 'no';

	}



	public function action_copyList(){
		if($_POST){
				$theirListId = $this->request->param('id_theirlist');

				//=> parámetros que se deben enviar a appendNew y a copyList
				//se llenan valores de prueba para el $newData
			    $user_id = $this->user['id'];
			    $newData = array();
			    $newData['list_name'] = $_POST['name'];
			    $newData['list_description'] = $_POST['description'];
			    $newData['list_keywords'] = $_POST['keywords'];
			    $newData['list_type'] = 'classic';
			    $newData['list_locked'] = 0;
			    $newData['list_privacy'] = $_POST['privacy'];
			    $newData['list_userId'] = $user_id;
			    $newData['list_typeList'] = 'B';

				$y=Model::factory('List')->copyList( $theirListId , $newData );

				if($y) print 'yes';
					else print 'no';
		}
		else print 'no';

	}

	public function action_appendNew(){
		if($_POST){

				$myListId = $this->request->param('id_mylist');
				$theirListId = $this->request->param('id_theirlist');

				//=> parámetros que se deben enviar a appendNew y a copyList
				//se llenan valores de prueba para el $newData
			    $user_id = $this->user['id'];
			    $newData = array();
			    $newData['list_name'] = $_POST['name'];
			    $newData['list_description'] = $_POST['description'];
			    $newData['list_keywords'] = $_POST['keywords'];
			    $newData['list_type'] = 'classic';
			    $newData['list_locked'] = 0;
			    $newData['list_privacy'] = $_POST['privacy'];
			    $newData['list_userId'] = $user_id;
			    $newData['list_typeList'] = 'B';

				Model::factory('List')->appendNew( $myListId, $theirListId, $newData );

		return true;

				if($y) print 'yes';
					else print 'no';
		}
		else print 'no';

	}

	public function action_like_list(){
		if($_POST){
			if($_POST['option']=="0") {
				$y=$this->list_model->likeList($this->user['id'],$_POST['list']);

			}
			else
				$y=$this->list_model->unlikeList($this->user['id'],$_POST['list']);
			if($y) {
				if ($_POST['option']=="0") {
					$id = DB::select()->from('pi_like')->where('resource','=','list')->and_where('userfront_id','=',$this->user['id'])->execute()->as_array('userfront_id')[$this->user['id']];

					//se obtiene el id del usuario propietario de la lista
					$owner_id = Model::factory('list')->idOwnerList( $_POST['list'] );

					$likes = Model::factory("list")->countListLikes( $_POST['list'] );

					$award_found = false;
					$awardKey = '';
					$id1 = 0;
					switch( $likes )
					{
						case 10:
							$award_found = true;
							$awardKey = 'A21';
							$id1 = 21;
							break;
						case 25:
							$award_found = true;
							$awardKey = 'A22';
							$id1 = 22;
							break;
						case 50:
							$award_found = true;
							$awardKey = 'A23';
							$id1 = 23;
							break;
						case 100:
							$award_found = true;
							$awardKey = 'A24';
							$id1 = 24;
							break;
					}
					if( $award_found == true )
					{
						$insertAdward = Model::factory('score')->addAward( $owner_id, $awardKey );
						if($insertAdward!=false)
						{
							//Log::instance()->add(Log::NOTICE, 'ENTRA AL INSERT');
							$type = 'award';
				           	$notStatus = 'pendig';
				           	Model::factory('notification')->pushNotification( $owner_id, $notStatus, $type, $awardKey );

			           		//se notifica en el muro que gano trofeo
					        $type = 'award';
					        $id2 = 0;
					        Model::factory('social')->addActivity( $type, $id1, $id2, $owner_id );
						}
					}

					Model::factory('social')->addActivity("like", $id['id'], 0, $this->user['id']);
				}
				print 'yes';
			}
			else
				print 'no';
		}else
			print 'no';
	}

	public function action_like_comment(){
		if($_POST){
			if($_POST['option']=="0")
				$y=$this->list_model->likeComment($this->user['id'],$_POST['comment']);
			else
				$y=$this->list_model->unlikeComment($this->user['id'],$_POST['comment']);
			if($y)
				print 'yes';
			else
				print 'no';
		}else
			print 'no';
	}
	public function action_rank_book(){
		if($_POST){

			//revisar que no haya un registro de ese rank en la tabla userfront_has_pi_score_list
				$rankDone = Model::factory('score')->checkRankDone( $this->user['id'], $_POST['book'] );
			if(!$rankDone)
			{
				//se agregan puntos al usuario por calificar un libro
			    $activityKey = 'S4';
				Model::factory('score')->addPoints( (int)$this->user['id'], $activityKey, -1 );

				//se registra el rank para saber que ya no se deben agregar mas puntos a ese recurso
				Model::factory('score')->registerRankScore((int)$this->user['id'], 4, 'rank', $_POST['book']);

				//trofeo al usuario por califica un libro
					//revisar cuantos registros hay por calificacion de un libro
					$totalRankBooks = Model::factory('score')->totalRankBooksByUser( $this->user['id'] );

					switch( $totalRankBooks )
					{
						case 1:
							//se califica el primer libro. Se agrega trofeo
							$awardKey = 'A9';
           					Model::factory('score')->addAward( $this->user['id'], $awardKey );
           					//se notifica en el muro que gano trofeo
           					$type = 'award';
           					$id1 = 9;
           					$id2 = 0;
           					Model::factory('social')->addActivity( $type, $id1, $id2, $this->user['id'] );
           					//se manda registro para notificacion de pantalla
           					$notStatus = 'pendig';
			                $type = 'award';
			                Model::factory('notification')->pushNotification( $this->user['id'], $notStatus, $type, $awardKey );
							break;
						case 10:
							//se califican 10 libros
							$awardKey = 'A10';
           					Model::factory('score')->addAward( $this->user['id'], $awardKey );
           					//se notifica en el muro que gano trofeo
           					$type = 'award';
           					$id1 = 10;
           					$id2 = 0;
           					Model::factory('social')->addActivity( $type, $id1, $id2, $this->user['id'] );
           					//se manda registro para notificacion de pantalla
           					$notStatus = 'pendig';
			                $type = 'award';
			                Model::factory('notification')->pushNotification( $this->user['id'], $notStatus, $type, $awardKey );
							break;
						case 50:
							//se califican 50 libros
							$awardKey = 'A11';
           					Model::factory('score')->addAward( $this->user['id'], $awardKey );
           					//se notifica en el muro que gano trofeo
           					$type = 'award';
           					$id1 = 11;
           					$id2 = 0;
           					Model::factory('social')->addActivity( $type, $id1, $id2, $this->user['id'] );
           					//se manda registro para notificacion de pantalla
           					$notStatus = 'pendig';
			                $type = 'award';
			                Model::factory('notification')->pushNotification( $this->user['id'], $notStatus, $type, $awardKey );
							break;
						case 100:
							//se califican 100 libros
							$awardKey = 'A12';
           					Model::factory('score')->addAward( $this->user['id'], $awardKey );
           					//se notifica en el muro que gano trofeo
           					$type = 'award';
           					$id1 = 12;
           					$id2 = 0;
           					Model::factory('social')->addActivity( $type, $id1, $id2, $this->user['id'] );
           					//se manda registro para notificacion de pantalla
           					$notStatus = 'pendig';
			                $type = 'award';
			                Model::factory('notification')->pushNotification( $this->user['id'], $notStatus, $type, $awardKey );
							break;

					}

			}



			$y = $this->book_model->rankBook($_POST['book'],$_POST['ranking'],$this->user['id'],'');
			$rank = $this->book_model->getBookRank($_POST['book']);

			$opResult = Model::factory('social')->getRatingForUserResource($this->user['id'],$_POST['book'],'book');
			Model::factory('social')->addActivity('rating',$opResult['id'],$_POST['ranking'],$this->user['id']);


			if($y)
			{
				print json_encode($rank);
			}

			else
			{
				print 'no';
			}
		}
		else
		{
			print 'no';
		}
	}

	public function action_rank_location(){
		if($_POST)
		{
			$y = Model::factory('atlas')->rankLocation($_POST['location'],$_POST['ranking'],$this->user['id']);
			$rank = Model::factory('atlas')->getLocationRankPonderated($_POST['location']);

			//$opResult = Model::factory('social')->getRatingForUserResource($this->user['id'],$_POST['location'],'location');
			//Model::factory('social')->addActivity('rating',$opResult['id'],$_POST['ranking'],$this->user['id']);

			if($y)
			{
				print json_encode(round($rank));
			}

			else
			{
				print 'no';
			}
		}
		else
		{
			print 'no';
		}
	}

	public function action_rank_location_subject()
	{
		$subject = $this->request->param('subject');
		if($_POST)
		{
			$y = Model::factory('atlas')->rankLocationRubro($_POST['location'],$_POST['ranking'],$this->user['id'],$subject, $_POST['loc_class']);
			$rank = Model::factory('atlas')->getLocationRankByRubro($_POST['location'],$subject);

			//$opResult = Model::factory('social')->getRatingForUserResource($this->user['id'],$_POST['location'],'location');
			//Model::factory('social')->addActivity('rating',$opResult['id'],$_POST['ranking'],$this->user['id']);

			if($y)
			{
				print json_encode($rank);
			}

			else
			{
				print 'no';
			}
		}
		else
		{
			print 'no';
		}
	}

	public function action_unset_notification() {
		if( $_POST )
		{
			//se se marca como mostrada la notificacion del usuario
			$notyId = $_POST["noty_id"];
			$newStatus = 'ok';

			Model::factory('notification')->updateNotificationStatus( $notyId, $newStatus );
		}
	}

	public function action_check_for_notification() {
		if($_POST) {

			//se revisa en el registro del usuario si hay notificacion pendiente por mostrar
			$userId = $_POST["user_id"];
			$resultNotification = Model::factory('notification')->checkForNotification( $userId );

			$form_data = array();
			$form_data['flag'] = 0;

			if( count($resultNotification) > 0 )
			{
				//hay notificaciones pendientes por mostrar
				$form_data['flag'] = 1;
				$form_data['content'] = $resultNotification[0]['content'];
				$form_data['idNoty'] = $resultNotification[0]['id'];
			}
			echo json_encode($form_data);
		}
	}


	public function action_set_denounce(){
		if($_POST){
			$y = $this->denounce_model->set_denounce($_POST['type'],$_POST['resource_id'],$this->user['id'],$_POST['motivate'],$_POST['link']);
			if($y)
				print 'yes';
			else
				print 'no';
		}else
			print 'no';
	}
	public function action_review_book(){
		if($_POST){
			$y = $this->book_model->setReview($_POST['comment'],$_POST['book_id'],$this->user['id']);
			$id = DB::select()->from('pi_comment')->where('resource','=','book')->and_where('userfront_id','=',$this->user['id'])->and_where('resource_id','=',$_POST['book_id'])->execute()->as_array('userfront_id')[$this->user['id']];
			Model::factory('social')->addActivity("review", $id['id'], 0, $this->user['id']);
			print json_encode($y);
		}else
			print 'no';
	}
	public function action_add_book_to_list(){
		if($_POST){
			//Log::instance()->add(Log::DEBUG,"POST LIST: ". $_POST['list']);
			$y = $this->list_model->add_book_to_list($_POST['list'],$_POST['book_id']);
			if($y)
			{

				//se agrega trofeo por añadir un libro a mi biblioteca
				$id_list = $_POST['list'];
				$my_library_list = Model::factory('userfront')->checkMyLibraryList( $_POST['list'] );
				if( $my_library_list==true )
				{
					//se agrega trofeo por agregar un libro a mi-biblioteca
		            $awardKey = 'A4';
		           	$insertAdward = Model::factory('score')->addAward( $this->user['id'], $awardKey );
		           	if($insertAdward!=false)
		           	{
		           		$type = 'award';
	           			$notStatus = 'pendig';
	           			Model::factory('notification')->pushNotification( $this->user['id'], $notStatus, $type, $awardKey );

	           			//se notifica en el muro que gano trofeo
		           		$type = 'award';
		           		$id1 = 4;
		           		$id2 = 0;
		           		Model::factory('social')->addActivity( $type, $id1, $id2, $this->user['id'] );
		           	}
				}

				//se agrega trofeo por agregar un libro a mi-biblioteca
				$lo_estoy_leyendo_list = Model::factory('userfront')->checkLoEstoyLeyendoList( $_POST['list'] );
				if( $lo_estoy_leyendo_list==true )
				{
					//se agrega trofeo por agregar un libro a lo-estoy-leyendo
		            $awardKey = 'A5';
		           	$insertAdward = Model::factory('score')->addAward( $this->user['id'], $awardKey );
		           	if($insertAdward!=false)
		           	{
		           		$type = 'award';
	           			$notStatus = 'pendig';
	           			Model::factory('notification')->pushNotification( $this->user['id'], $notStatus, $type, $awardKey );

	           			//se notifica en el muro que gano trofeo
		           		$type = 'award';
		           		$id1 = 5;
		           		$id2 = 0;
		           		Model::factory('social')->addActivity( $type, $id1, $id2, $this->user['id'] );
		           	}
				}




				//se agrega trofeo por agregar 1, 10, 50 y 100 libros a ya-lo-lei
				$ya_lo_lei_list = Model::factory('userfront')->checkYaLoLeiList( $_POST['list'] );
				if( $ya_lo_lei_list==true )
				{
					//se agrega libro a lista ya-lo-lei
					//determinar cuantos libros se han agregado a esa lista
					$count_books = Model::factory('list')->count_list( $_POST['list'] );
					$award_found = false;
					$awardKey = '';
					$id1 = 0;
					switch( $count_books )
					{
						case 1:
							$award_found = true;
							$awardKey = 'A13';
							$id1 = 13;
							break;
						case 10:
							$award_found = true;
							$awardKey = 'A14';
							$id1 = 14;
							break;
						case 50:
							$award_found = true;
							$awardKey = 'A15';
							$id1 = 15;
							break;
						case 100:
							$award_found = true;
							$awardKey = 'A16';
							$id1 = 16;
							break;
					}
					if( $award_found == true )
					{
						$insertAdward = Model::factory('score')->addAward( $this->user['id'], $awardKey );
						if($insertAdward!=false)
						{
							$type = 'award';
	           				$notStatus = 'pendig';
	           				Model::factory('notification')->pushNotification( $this->user['id'], $notStatus, $type, $awardKey );

	           				//se notifica en el muro que gano trofeo
			           		$type = 'award';
			           		$id2 = 0;
			           		Model::factory('social')->addActivity( $type, $id1, $id2, $this->user['id'] );
						}
					}

				}



				print 'yes';



			}
			else
				print 'no';
		}else
			print 'no';
	}
	public function action_remove_book_to_list(){
		if($_POST){
			$y = $this->list_model->remove_book_from_list($_POST['list'],$_POST['book_id']);
			if($y)
				print 'yes';
			else
				print 'no';
		}else
			print 'no';
	}
	public function action_delete_list(){
		if($_POST){
			$y = $this->list_model->delete_list($_POST['list']);
			if($y)
				print 'yes';
			else
				print 'no';
		}
		else print 'no';
	}

	/**
	 * Función para realizar la creación de las listas
	 * Contesta a ajax/create_list
	 * @return
	 */
	public function action_create_list(){
		if ($_POST) {
			$session = Session::instance();
			$sess = $session->as_array();
			$params = array();
			$result = null;
			// notificación al crear lista
			if ($_POST['type']=='mini') {
				$result = $this->list_model->create_list($_POST['name'],$this->user['id']);
				// se agrega trofeo por crear una lista clásica
				$awardKey = 'A8';
				$insertAdward = Model::factory('score')
					->addAward( $this->user['id'], $awardKey );

				if ($insertAdward != false) {
				  $type = 'award';
			    $notStatus = 'pendig';
			    Model::factory('notification')
			    	->pushNotification( $this->user['id'], $notStatus, $type, $awardKey );

			    //se notifica en el muro que gano trofeo
			    $type = 'award';
			    $id1 = 8;
			    $id2 = 0;
			    Model::factory('social')->addActivity( $type, $id1, $id2, $this->user['id'] );
				}
			} else if($_POST['type']=='dynamic') {

				if (array_key_exists('search', $sess)) {
					foreach ($sess['search'] as $param => $value) {
						if( $param != 'min' &&
							$param != 'pagina' && 
							$param != 'registros_por_pagina' && 
							$param != 'attr' && 
							$param != 'orden' && 
							$param != 'orden_dir' )
						{
							if( $value != '' ) {
								$params[$param] = $value;
							}
						}
					}
				}
				
				$result = $this->list_model->create_list($_POST['name'],
					$this->user['id'],
					$_POST['description'],
					'',
					'dynamic',
					"C",
					0,
					$_POST['privacy'], 
					'', 
					$_POST['keywords'], 
					$params);
			} else {
				// se crea lista clásica
				$result = $this->list_model->create_list($_POST['name'], 
					$this->user['id'], 
					$_POST['description'],
					'',
					'classic',
					"B",
					0,
					$_POST['privacy'],
					'',
					$_POST['keywords'],
					'');

				// se agrega trofeo por crear una lista clásica
				$awardKey = 'A8';
        $insertAdward = Model::factory('score')
        	->addAward( $this->user['id'], $awardKey );

      	if ($insertAdward != false) {
      		$type = 'award';
       		$notStatus = 'pendig';
       		Model::factory('notification')
       			->pushNotification( $this->user['id'], $notStatus, $type, $awardKey );

       		//se notifica en el muro que gano trofeo
         	$type = 'award';
         	$id1 = 8;
         	$id2 = 0;
         	Model::factory('social')->addActivity( $type, $id1, $id2, $this->user['id'] );
				}
			}
			print json_encode($result);
		} else {
			print 'no';
		}
	}

	public function action_get_book_data() {
		if ($_POST && $_POST['book_title']) {
			$y = $this->book_model->get_book(array('book_id' => $_POST['book_title']),false);
			if ($y) {
				print json_encode(array(
				'id' => $y['id'],
				'titulo' => $y['titulo'],
				'isbn' => $y['isbn'],
				'editorial' => $y['editorial'],
				'autor' => $y['autores'][0]['nombre'],
				));
			} else {
				print 'error';
			}
		} else {
			print 'error';
		}
		exit;
	}
	public function action_save_book_summary(){
		if($_POST){
			if(isset($_POST['save_book_summary'])){
				if(isset($_POST['book_id']) && isset($_POST['user_id']) && isset($_POST['review'])){
					if($this->user['imgfb']) $_POST['fb_id'] = 1; else $_POST['fb_id']=0;
					$updated = $this->book_model->set_review(array('user_id' => $_POST['user_id'], 'book_id' => $_POST['book_id'], 'review' => $_POST['review'],'fb_id'=>$_POST['fb_id']));
					if($updated) {
						print 'ok';
						$data['book'] = $this->book_model->get_book(array('book_id' => $_POST['book_id']));
							if($data['book']){
								$log_data['id']= $data['book']['id'];
								$log_data['titulo']= $data['book']['titulo'];
								$log_data['autor']= $data['the_authors'];
								$log_data['imagen']= $data['book']['img_chica'];
								$log_data['tipo']= $data['book']['tipo_producto'];
							}
						$log_data['review']=$_POST['review'];
						$log_data['fb_id']=$_POST['fb_id'];

						Model::factory('Stream')->add_stream_log($_POST['user_id'], 1,5, $_POST['book_id'],$log_data);
					}
						else  print 'not updated';
				}else print 'not all fields';
			}else print 'not save_book_summary';
		}else print 'not post';
		exit;
	}
	public function action_save_editor_session(){
		if(isset($_POST['editor_session'])){
			//var_dump($_POST); exit;
			Session::instance()->set('editor_session', $_POST['data']);
			print 'session_saved';
		}
		exit;
	}
	public function action_user_in_book(){

		if(isset($_POST)){
			//add review
			$d = $_POST;
			$data['section'] = $this->request->param('section');
			if($this->user){
				$data['book'] = $this->book_model->get_book(array('book_id' => $d['book_id'], 'user_id' => $d['user_id']));
				if(count($data['book']['autores'])){
					$tmp = array();
					foreach($data['book']['autores'] as $collab){
						if($collab['rol'] == 'Autor'){
							$tmp[] = $collab['nombre'];
						}
					}
					$data['all_the_authors'] = count($tmp)?implode('; ', $tmp):'';
					if(count($tmp)>0){
						$i = 1;
						$tmp2 = array();
						foreach($tmp as $author){
							$tmp2[] = $author;
							if($i == 2) break;
							$i++;
						}
					}
				}
				$data['the_authors'] = count($tmp2)?implode('; ', $tmp2):'';
				if($data['book']){
					$log_data['id']= $d['book_id'];
					$log_data['titulo']= $data['book']['titulo'];
					$log_data['autor']= $data['the_authors'];
					$log_data['imagen']= $data['book']['img_chica'];
					$log_data['tipo']= $data['book']['tipo_producto'];
				}
				if($this->user['imgfb']) $log_data['fb_id'] = 1;
				$exists_book_in_library = $this->book_model->exists_book_in_library(array('user_id' => $d['user_id'], 'book_id' => $d['book_id']));
				if(!$exists_book_in_library) $added_id = $this->book_model->add_book_to_library(array('user_id' => $d['user_id'], 'book_id' => $d['book_id']));
				if(isset($_POST['review_by_ajax'])&&!empty($_POST['review_by_ajax'])){
					$exist_review = $this->book_model->get_review(array('user_id' => $d['user_id'], 'book_id' => $d['book_id']));
					if($this->user['imgfb']) $d['fb_id']=1; else $d['fb_id']=0;
					//if(empty($exist_review)||!$exist_review){
						$id_inserted = $this->book_model->set_review(array('user_id' => $d['user_id'], 'review' => $d['review'], 'book_id' => $d['book_id'], 'fb_id'=>$d['fb_id']));
						$log_data['review']=$d['review'];
						Model::factory('Stream')->add_stream_log($d['user_id'], 1,5, $d['book_id'],$log_data);
						print trim('added');
					//}
					exit;
				}
				//want to read
				if(isset($_POST['want_to_read_by_ajax'])&&!empty($_POST['want_to_read_by_ajax'])){
					$updated = $this->book_model->update_userfrontlibrary(array('user_id' => $d['user_id'], 'field' => 'want_to_read', 'book_id' => $d['book_id']));
					Model::factory('Stream')->add_stream_log($d['user_id'], 1,1, $d['book_id'],$log_data);
					$updated ?print trim('yes')  : print trim('no') ;
					exit;
				}

				//already read
				if(isset($_POST['already_read_by_ajax'])&&!empty($_POST['already_read_by_ajax'])){
					$updated = $this->book_model->update_userfrontlibrary(array('user_id' => $d['user_id'], 'field' => 'already_read', 'book_id' => $d['book_id']));
					Model::factory('Stream')->add_stream_log($d['user_id'], 1,2, $d['book_id'],$log_data);
					if($d['book_id']) $this->book_model->add_popular(array('book_id' => $d['book_id'], 'type' => 3));
					$updated ? print trim('yes') : print trim('no') ;
					exit;
				}

				//recommend
				if(isset($_POST['recommend_by_ajax'])&&!empty($_POST['recommend_by_ajax'])){
					$updated = $this->book_model->update_userfrontlibrary(array('user_id' => $d['user_id'], 'field' => 'recommend', 'book_id' => $d['book_id']));
					Model::factory('Stream')->add_stream_log($d['user_id'], 1,3, $d['book_id'],$log_data);
					$updated ? print trim('yes') : print trim('no') ;
					exit;
				}
			}
		}
	}
	public function action_club_comment_denounce(){
		if(isset($_POST['send_denounce'])){
			if(ctype_digit($_POST['cause']) && ctype_digit($_POST['comment_id']) && ctype_digit($_POST['status'])){
				$inserted = $this->club_model->create_denounce(array('status' =>$_POST['status'], 'comment_id' =>$_POST['comment_id'], 'cause' =>$_POST['cause']));
				$inserted ? print trim('yes') : print trim('no') ;
			}

		}
	}
	
	public function action_send_email() {
		$result = array();
		$result['status'] = 'ERROR';
		$result['message'] = 'Muchas gracias por la información en breve revisaremos tu mensaje';
		$data = isset($_POST) ? $_POST : null;

		if (!$data) {
		  print json_encode($response);
		  exit;
		}

		$message = <<< EOT
		  Hola, <br>
		  Tienes un nuevo contacto, los datos que contacto son:
EOT;
		$message .= "<table>";
		foreach ($data['contact'] as $key => $value) {
		  $message .= "<tr>";
		  $message .=   "<th style='width: 150px'>";
		  $message .=     "{$key}";
		  $message .=   "</th>";
		  $message .=   "<td>";
		  $message .=   "{$value}";
		  $message .=   "</td>";
		  $message .= "</tr>";
		}
		$message .= "</table>";

		$title = 'Contacto';

		$mail_response = LM::send_mail("contacto@librosmexico.mx", $title, $message);
		$result['data'] = $data;
		$result['mail_response'] = $mail_response;
		$result['status'] = 'SUCCESS';

		echo json_encode($result, true);
		exit;
	}

	public function action_advanced_search_book(){
    if($_POST)
    {
      $book_param = $_POST['book_param'];
      $word_search = $_POST['word_search'];

			$params = array( $book_param => $word_search );

			$params['registros_por_pagina'] = 30;

      $result = Model::factory('List')->advanced_search( $params );

      if( is_null($result) ){
				$varnull = true;
				print 'error';
				exit;
			}

			if($result['cuerpo']) {
			  print json_encode($result);
      }
      else print 'error';

    }else print 'error';
    exit;
  }

  public function action_search_filter(){
		if($_POST)
		{
			$params = $this::get_params( $_POST , 'filtros' );

			$result = Model::factory('List')->advanced_search( $params );

			if( is_null($result) ){
				$varnull = true;
				print 'error: action_search_filter null result';
				exit;
			}

			if($result['cuerpo']) {
				print json_encode($result);
			}
			else print 'error: action_search_filter empty body';

		}else print 'error: action_search_filter empty _POST';
		exit;
	}

	public function action_search_author_filter(){
		if($_POST)
		{
			$params = $this::get_params( $_POST , 'autores' );
			$result = Model::factory('List')->advanced_search( $params );

			if($result['cuerpo'])
			{
				$unique_authors = array(); //array para guardar los autores sin repetir
				foreach ($result['cuerpo'] as $key => $book)
				{
					foreach ($book['autores'][1] as $autor)  //recorre todos los autores de cada libro
					{
						if( !in_array( $autor['nombre'], $unique_authors) ) //si el autor no existe en el array
							$unique_authors[] = $autor['nombre']; //se agrega
					}
				}
				print json_encode($unique_authors);
      }else print 'error: action_search_author_filter empty body';
    }else print 'error: action_search_author_filter empty _POST';
		exit;
	}

	public function action_search_editorial_filter(){
		if($_POST)
		{
			$params = $this::get_params( $_POST , 'editorial' );
			$result = Model::factory('List')->advanced_search( $params );

			if($result['cuerpo'])
			{
				$unique_editorials = array(); //array para guardar las editoriales sin repetir
				foreach ($result['cuerpo'] as $key => $book)
				{
					foreach ($book['editorial'] as  $edit) {
						if( !in_array($edit['nombre'], $unique_editorials))
							$unique_editorials[] = $edit['nombre'];
					}
				}
				print json_encode($unique_editorials);
			}else print 'error';
		}else print 'error';
		exit;
	}

	public function action_search_keywords_filter(){
		if($_POST)
		{
			$params = $this::get_params( $_POST , 'keywords' );
			$result = Model::factory('List')->advanced_search( $params );

			if($result['cuerpo']) {

					$unique_keywords = array(); //array para guardar las palabras clave sin repetir
					$keywords_count_books = array(); //array para contar los libros de cada autor

					foreach ($result['cuerpo'] as $key => $book) {
						foreach ($book['keywords'] as $keyword) { //recorre todos las palabras clave de cada libro

							if( !in_array( $keyword, $unique_keywords) && $keyword != '' ) //si la palabra clave no existe en el array
							{
								$unique_keywords[] = $keyword; //se agrega
							}
							$i++;
						}
					}

					$keywords = array();
					foreach ($unique_keywords as $id => $name) {
						$keywords[] = array('id' => $id, 'name' => $name, 'total_books' => $keywords_count_books[$id]);
					}

	      	print json_encode($unique_keywords);

	      }
	      else print 'error';

			}else print 'error';
			exit;
	}

	public function action_search_coleccion_filter(){
		if($_POST)
		{
			$params = $this::get_params( $_POST , 'coleccion' );
			$result = Model::factory('List')->advanced_search( $params );

			if($result['cuerpo'])
			{
				$unique_colecciones = array(); //array para guardar las colecciones sin repetir
				foreach ($result['cuerpo'] as $key => $book){
					if( !in_array($book['coleccion'], $unique_colecciones) && $book['coleccion'] != '' )
						$unique_colecciones[] = $book['coleccion'];
				}
				print json_encode($unique_colecciones);
			}else print 'error';
		}else print 'error';
		exit;
	}

	public function action_search_type_filter(){
		if($_POST)
		{
			$tipos = LM::tipo_producto_clve(); //tipos de producto (Libro, audiolibro, cd, dvd, etc.)
			$params = $this::get_params( $_POST , 'tipo_producto' );
			$result = Model::factory('List')->advanced_search( $params );

			if($result['cuerpo'])
			{
				$unique_tipo_prod = array(); //array para guardar los tipos de producto sin repetir
				foreach ($result['cuerpo'] as $book) //recorre todos los libros del resultado
				{
					if( !in_array($book['tipo_producto'], $unique_tipo_prod))
					{
						$tipo = $tipos[ $book['tipo_producto'] ];
						$unique_tipo_prod[ $tipo ] = $book['tipo_producto'];
					}
				}
				$product_type = array();
				foreach ($unique_tipo_prod as $id => $name) {
					$product_type[] = array('id' => $id, 'name' => $name, 'total_books' => $tipo_prod_count_books[$id] );
				}
				print json_encode($product_type);
			}else print 'error';
    }else print 'error';
		exit;
	}

	public function action_search_colofon_filter(){
		if($_POST)
		{
			$params = $this::get_params( $_POST , 'fecha_colofon' );

			$result = Model::factory('List')->advanced_search( $params );
			if($result['cuerpo'])
			{
					$current_year = date('Y');
					$min = $current_year;
					$max = 0;

					$rango_colofon = array('fecha_colofon_1' => $min, 'fecha_colofon_2' => $max);
					//$rango_colofon = array();
					foreach ($result['cuerpo'] as $book)  //recorre todos los libros del resultado
					{
						if( $book['fecha_colofon'] != null )
						{
							$year = substr( $book['fecha_colofon'] , 0 , 4 );
							$year = (int)$year;

							if( $year > 1455 && $year <= $current_year )
							{
								if( $year < $min )
									$min = $year;

								if( $year > $max )
									$max = $year;
							}
						}
					}

					$rango_colofon['fecha_colofon_1'] = $min;
					$rango_colofon['fecha_colofon_2'] = $max;

	      	print json_encode($rango_colofon);
      }
      else print 'error';

		}else print 'error';
		exit;
	}

	private function get_params( $post, $filter ){
		$session = Session::instance();

		if( isset($post['titulo']) && $post['titulo'] != null ) $params['titulo'] = $post['titulo'];
		if( isset($post['autores']) && $post['autores'] != null ) $params['autores'] = $post['autores'];
		if( isset($post['editorial']) && $post['editorial'] != null ) $params['editorial'] = $post['editorial'];
		if( isset($post['tipo_producto']) && $post['tipo_producto'] != null ) $params['tipo_producto'] = $post['tipo_producto'];
		if( isset($post['isbn']) && $post['isbn'] != null ) $params['isbn'] = $post['isbn'];
		if( isset($post['keywords']) && $post['keywords'] != null ) $params['keywords'] = $post['keywords'];
		if( isset($post['temas']) && $post['temas'] != null ) $params['temas'] = $post['temas'];
		if( isset($post['cadena']) && $post['cadena'] != null ) $params['cadena'] = $post['cadena'];
		if( isset($post['fecha_colofon_1']) && $post['fecha_colofon_1'] != null ) $params['fecha_colofon_1'] = $post['fecha_colofon_1'];
		if( isset($post['fecha_colofon_2']) && $post['fecha_colofon_2'] != null ) $params['fecha_colofon_2'] = $post['fecha_colofon_2'];
		if( isset($post['coleccion']) && $post['coleccion'] != null ) $params['coleccion'] = $post['coleccion'];

		if( isset($post['subtitulo']) && $post['subtitulo'] != null ) $params['subtitulo'] = $post['subtitulo'];
		if( isset($post['sinopsis']) && $post['sinopsis'] != null ) $params['sinopsis'] = $post['sinopsis'];
		if( isset($post['edicion']) && $post['edicion'] != null ) $params['edicion'] = $post['edicion'];
		if( isset($post['encuadernacion']) && $post['encuadernacion'] != null ) $params['encuadernacion'] = $post['encuadernacion'];
		if( isset($post['no_volumen']) && $post['no_volumen'] != null ) $params['no_volumen'] = $post['no_volumen'];
		if( isset($post['nacional']) && $post['nacional'] != null ) $params['nacional'] = $post['nacional'];

		if( isset($post['ilustraciones_1']) && $post['ilustraciones_1'] != null ) $params['ilustraciones_1'] = $post['ilustraciones_1'];
		if( isset($post['ilustraciones_2']) && $post['ilustraciones_2'] != null ) $params['ilustraciones_2'] = $post['ilustraciones_2'];
		if( isset($post['peso_1']) && $post['peso_1'] != null ) $params['peso_1'] = $post['peso_1'];
		if( isset($post['peso_2']) && $post['peso_2'] != null ) $params['peso_2'] = $post['peso_2'];
		if( isset($post['alto_1']) && $post['alto_1'] != null ) $params['alto_1'] = $post['alto_1'];
		if( isset($post['alto_2']) && $post['alto_2'] != null ) $params['alto_2'] = $post['alto_2'];
		if( isset($post['ancho_1']) && $post['ancho_1'] != null ) $params['ancho_1'] = $post['ancho_1'];
		if( isset($post['ancho_2']) && $post['ancho_2'] != null ) $params['ancho_2'] = $post['ancho_2'];
		if( isset($post['grosor_1']) && $post['grosor_1'] != null ) $params['grosor_1'] = $post['grosor_1'];
		if( isset($post['grosor_2']) && $post['grosor_2'] != null ) $params['grosor_2'] = $post['grosor_2'];
		if( isset($post['paginas_1']) && $post['paginas_1'] != null ) $params['paginas_1'] = $post['paginas_1'];
		if( isset($post['paginas_2']) && $post['paginas_2'] != null ) $params['paginas_2'] = $post['paginas_2'];

		if( isset($post['fecha_publicacion_1']) && $post['fecha_publicacion_1'] != null ) $params['fecha_publicacion_1'] = $post['fecha_publicacion_1'];
		if( isset($post['fecha_publicacion_2']) && $post['fecha_publicacion_2'] != null ) $params['fecha_publicacion_2'] = $post['fecha_publicacion_2'];
		if( isset($post['fecha_actualizacion_1']) && $post['fecha_actualizacion_1'] != null ) $params['fecha_actualizacion_1'] = $post['fecha_actualizacion_1'];
		if( isset($post['fecha_actualizacion_2']) && $post['fecha_actualizacion_2'] != null ) $params['fecha_actualizacion_2'] = $post['fecha_actualizacion_2'];

		if( isset($post['iva']) && $post['iva'] != null ) $params['iva'] = $post['iva'];
		if( isset($post['disponibilidad']) && $post['disponibilidad'] != null ) $params['disponibilidad'] = $post['disponibilidad'];
		if( isset($post['audiencia']) && $post['audiencia'] != null ) $params['audiencia'] = $post['audiencia'];
		if( isset($post['nivel_lectura']) && $post['nivel_lectura'] != null ) $params['nivel_lectura'] = $post['nivel_lectura'];
		if( isset($post['nivel_academico']) && $post['nivel_academico'] != null ) $params['nivel_academico'] = $post['nivel_academico'];

		if( isset($post['idioma_original']) && $post['idioma_original'] != null ) $params['idioma_original'] = $post['idioma_original'];
		if( isset($post['pais']) && $post['pais'] != null ) $params['pais'] = $post['pais'];
		if( isset($post['precio_vigente']) && $post['precio_vigente'] != null ) $params['precio_vigente'] = $post['precio_vigente'];
		if( isset($post['con_portada']) && $post['con_portada'] != null ) $params['con_portada'] = $post['con_portada'];

		if( isset($post['reimpresion']) && $post['reimpresion'] != null ) $params['reimpresion'] = $post['reimpresion'];
		if( isset($post['curso']) && $post['curso'] != null ) $params['curso'] = $post['curso'];
		if( isset($post['estado']) && $post['estado'] != null ) $params['estado'] = $post['estado'];
		if( isset($post['asignatura']) && $post['asignatura'] != null ) $params['asignatura'] = $post['asignatura'];
		$params['origen'] ='L|E';
		/*if( $params['titulo'] == 'e' && $params['editorial'] != ''  )
			$param['titulo'] = '';*/

		if( isset($post['registros_por_pagina']) && $post['registros_por_pagina'] != null ) $params['registros_por_pagina'] = $post['registros_por_pagina'];
		//if( isset($post['todos']) && $post['todos'] != null ) $params['todos'] = $post['todos'];

		if( isset($post['pagina']) && $post['pagina'] != null ) $params['pagina'] = $post['pagina'];

		if( $filter == 'filtros' ){
			if( isset($post['orden']) && $post['orden'] != null ) $params['orden'] = $post['orden'];
			if( isset($post['orden_dir']) && $post['orden_dir'] != null ) $params['orden_dir'] = $post['orden_dir'];
			$params['attr'] = 'id_libro, titulo, tipo, tipo_producto, temas, editorial, autores, keywords, fecha_colofon, imagen, isbn';

			$session->set('search', $params);
		}
		elseif( $filter == 'autores' ){
			if( isset($post['pagina']) && $post['pagina'] != null ) $params['pagina'] = $post['pagina'];
			$params['attr'] = 'id_libro, titulo, autores';
		}
		elseif( $filter == 'editorial' ){
			if( isset($post['pagina']) && $post['pagina'] != null ) $params['pagina'] = $post['pagina'];
			$params['attr'] = 'id_libro, titulo, editorial';
		}
		elseif( $filter == 'tipo_producto' ){
			if( isset($post['pagina']) && $post['pagina'] != null ) $params['pagina'] = $post['pagina'];
			$params['attr'] = 'tipo_producto';
		}
		elseif( $filter == 'fecha_colofon' ){
			$params['attr'] = 'fecha_colofon';
		}
		elseif( $filter == 'keywords' ){
			$params['attr'] = 'id_libro, keywords';
		}
		elseif( $filter == 'coleccion' ){
			if( isset($post['pagina']) && $post['pagina'] != null ) $params['pagina'] = $post['pagina'];
			$params['attr'] = 'id_libro, titulo, coleccion';
		}

		return $params;
	}

	public function action_empty_response() {
		print_r(json_encode($_REQUEST, true));
		exit;
	}



	public function action_share_fb_notify() {

		//se agregan puntos por compartir un libro en facebook
		$activityKey = 'S6';
		Model::factory('score')->addPoints( $this->user['id'], $activityKey, -1 );

		$awardKey = 'A6';
		$insertAdward = Model::factory('score')->addAward( $this->user['id'], $awardKey );
		if($insertAdward!=false)
		{
		    $type = 'award';
	        $notStatus = 'pendig';
	        Model::factory('notification')->pushNotification( $this->user['id'], $notStatus, $type, $awardKey );

	        //se notifica en el muro que gano trofeo
		    $type = 'award';
		    $id1 = 6;
		    $id2 = 0;
		    Model::factory('social')->addActivity( $type, $id1, $id2, $this->user['id'] );
		}
	}



	public function action_share_tw_notify() {

		//se agregan puntos por compartir un libro en twitter
		$activityKey = 'S6';
		Model::factory('score')->addPoints( $this->user['id'], $activityKey, -1 );

		$awardKey = 'A7';
		$insertAdward = Model::factory('score')->addAward( $this->user['id'], $awardKey );
		if($insertAdward!=false)
		{
		    $type = 'award';
	        $notStatus = 'pendig';
	        Model::factory('notification')->pushNotification( $this->user['id'], $notStatus, $type, $awardKey );

	        //se notifica en el muro que gano trofeo
		    $type = 'award';
		    $id1 = 7;
		    $id2 = 0;
		    Model::factory('social')->addActivity( $type, $id1, $id2, $this->user['id'] );
		}
	}


	/**
	 * Función de ajax que regresa los libros recomendados
	 */
	public function action_ajax_recommended() {
		$result = array('status' => 'ERROR',
			'error' => array('code' => 0, 'message' => ''));
		$result['status'] = 'OK';

		$response = new stdClass;
		$mList = Model::factory('list');

		//Si es usuario logueado y tiene libros leidos, agregar libros recomendados
		if ( $this->user ) {
			$response->readList = $mList->getList($mList->get_listid_by_user_slug($this->user['id'], 'ya-lo-lei'));
			if (count($response->readList->books)<=0) {
				//Si no tiene libros leidos, muestra novedades
				$response->novelties = Model::factory('book')->fetch_recent_interval('-10 year', 1, 4);
			}
		}
		else { //Si no esta logueado muestra novedades en lugar de libros recomendados
			$response->novelties = Model::factory('book')->fetch_recent_interval('-10 year', 1, 4);
		}

		$mBook = Model::factory('book');

		shuffle($response->readList->books);
		if (count($response->readList->books)>4) {
			$response->readList->books = array_slice($response->readList->books,0,4);
		}
		//Agregar libros relacionados (incluyendo el rating)
		foreach ($response->readList->books as &$rBook) {
			$tmp = array();
			$tmp['book_id'] = $rBook['id_libro'];
			$rBook['id'] = $rBook['id_libro'];
			$rBook['relacionados'] = $mBook->get_book($tmp)['libros_relacionados'];

			foreach ($rBook['relacionados'] as &$relacionado) {
				$relacionado['rating']=$mBook->getAverageRating($relacionado['id']);
			}
		}
		//Agregar rating a novedades
		foreach ($response->novelties as &$nbook) {
			$nbook['rating']=$mBook->getAverageRating($nbook['id_libro']);
		}

		$result['recommended'] = $response;

		print json_encode($result, true);
		exit;
	}


}