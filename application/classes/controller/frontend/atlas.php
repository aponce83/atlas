 <?php defined('SYSPATH') or die('No direct script access.');
/**
 * Contento Frontend Homepage controller.
 *
 * @package    Contento/Frontend
 * @category   Controllers
 * @author     Abargon
 * @copyright  (c) 2013 Abargon
 * @license    http://abargon.com
 */
class Controller_Frontend_Atlas extends Controller_Frontend_Template {
	public function before(){
		parent::before();
		$this->device = Model::factory('device')->detect_device();
	}

	public function action_near_places() {

		$response = array();
		// $response['status'] = 'no';
		$latitude = $_POST['latitude'];
		$longitude = $_POST['longitude'];
		$radius = 1;

		// se obtiene información de los lugares cercanos
		$near_places = Model::factory('atlas')->getNearPlaces( $latitude, $longitude, $radius);

		foreach( $near_places as &$near_place ) {
			if ($this->user) {
				$near_place->evaluated_by_user =  Model::factory('atlas')->userLikesLocation($this->user['id'], $near_place->id);
			} else {
				$near_place->evaluated_by_user = NULL;
			}

			if ($near_place->likes == 0 || $near_place->likes == null) {
				$near_place->likes = 0;
			}
		}
		unset($near_place);

		$response['near_places'] = $near_places;
		print json_encode($response);
		exit;	
	}

	public function action_popular_sites() {
		$response = new stdClass;
		
		try {
			$display = Arr::get($_GET, 'acceso', null);
			$parameters = new stdClass;
		} catch(Exception $e) {
			switch($e->getCode()){
				case 1 : Request::current()->redirect('/');
				default: var_dump($e->getMessage());
			}
		}
		$response->tags = Model::factory('atlas')->getTags();
    $response->atlas_searchbar = View::factory('frontend/atlas/searchbar');
    $response->atlas_searchbar->tags = $response->tags;

    $response->total_locations = Model::factory('atlas')->countTotalLocations();

    //página actual
    $current_page = $this->request->param('current_pag');
    //cantidad de elementos a mostrar
    $res_per_page = $this->request->param('res_per_page');

    $response->res_per_page = $res_per_page;
    $response->current_page = $current_page;

    //se calcula el total de páginas
		$response->total_pages = ceil( $response->total_locations / $res_per_page );
		
		$response->more_comment_places = Model::factory('atlas')->getLocationPageByRating ($current_page-1, 
			$res_per_page );
    $response->user = $this->user['id'];

		$head_data = $this->template->head->data;
		$head_data["main_title"] = "Atlas de lectura";
		// Este es el título que tendrá la página para Facebook y Twitter
		$head_data["og_title"] = "Atlas de lectura | LIBROSMÉXICO.MX";

		$this->template->head->data = $head_data;

		$this->template->content->device = $this->device;
		$this->template->content->display = $display;
		$this->template->content->data = $response;	
	}

	public function action_more_commented() {
		$response = new stdClass;
		
		try{
			$display = Arr::get($_GET, 'acceso', null);
			$parameters = new stdClass;
		} catch(Exception $e) {
			switch($e->getCode()){
				case 1 : Request::current()->redirect('/');
				default: var_dump($e->getMessage());
			}
		}
		$response->tags = Model::factory('atlas')->getTags();
    $response->atlas_searchbar = View::factory('frontend/atlas/searchbar');
    $response->atlas_searchbar->tags = $response->tags;

    $response->total_locations = Model::factory('atlas')->countTotalLocations();

    //página actual
    $current_page = $this->request->param('current_pag');
    //cantidad de elementos a mostrar
    $res_per_page = $this->request->param('res_per_page');

    $response->res_per_page = $res_per_page;
    $response->current_page = $current_page;

    //se calcula el total de páginas
		$response->total_pages = ceil( $response->total_locations / $res_per_page );
		
		$response->more_comment_places = Model::factory('atlas')
			->getLocationPageByComments ($current_page, $res_per_page );
		$response->user = $this->user['id'];

		$head_data = $this->template->head->data;
		$head_data["main_title"] = "Atlas de lectura";
		// Este es el título que tendrá la página para Facebook y Twitter
		$head_data["og_title"] = "Atlas de lectura | LIBROSMÉXICO.MX";

		$this->template->head->data = $head_data;

		$this->template->content->device = $this->device;
		$this->template->content->display = $display;
		$this->template->content->data = $response;	
	}

	public function action_index(){
		$response = new stdClass;
		try {
			$display = Arr::get($_GET, 'acceso', null);
			$parameters = new stdClass;
		} catch(Exception $e) {
			switch($e->getCode()){
				case 1 : Request::current()->redirect('/');
				default: var_dump($e->getMessage());
			}
		}
		$response->tags = Model::factory('atlas')->getTags();
		
    $response->atlas_searchbar = View::factory('frontend/atlas/searchbar');
    $response->atlas_searchbar->tags = $response->tags;

    $page_size = 3;
    $response->popular_places = Model::factory('atlas')->getLocationPageByRating ( 0, $page_size );
    $response->more_comment_places = Model::factory('atlas')->getLocationPageByComments ( 1, $page_size );

    $response->user = $this->user['id'];

		$head_data = $this->template->head->data;
		$head_data["main_title"] = "Atlas de lectura";
		// Este es el título que tendrá la página para Facebook y Twitter
		$head_data["og_title"] = "Atlas de lectura | LIBROSMÉXICO.MX";

		$this->template->head->data = $head_data;

		$this->template->content->device = $this->device;
		$this->template->content->display = $display;
		$this->template->content->data = $response;		
	}

	public function action_search(){

		$response = new stdClass;
		
		try{
	
			//if(!$this->user) throw new Exception("Not user logged", 1);
			$display = Arr::get($_GET, 'acceso', null);
			$parameters = new stdClass;
			
			$response->tags = Model::factory('atlas')->getTags();

			
			/* TODO: Replantear el parametro tag para aceptar multiples argumentos */
			if(!isset($_GET['mypos']) && empty($_GET['mypos'])) {
				if (count($_GET)>0)
					$response->use_my_pos = false;
				else
					$response->use_my_pos = true;
			}
			else 
				$response->use_my_pos = true;

			if(!isset($_GET['tag']) && empty($_GET['tag'])) {
				$response->tag_filters = $response->tags;
				$response->tag_string = "";
				$helper = 0;
				foreach ($response->tag_filters as $tag_f) {
					if ($helper=0) {
						$response->tag_string.=$tag_f;
					}
					else {
						$response->tag_string.=(','.$tag_f);
					}
					$helper++;
				}
			}
			else {
				$response->tag_filters = explode(',',$_GET['tag']);
				$response->tag_string = $_GET['tag'];
			}

			
			/* TODO: Replantear el parametro tag para aceptar multiples argumentos */

			if (!isset($_GET['lat']) && empty($_GET['lat'])) {
				$response->search_lat = 19.390519;
			} else {
				$response->search_lat = str_replace(",", ".", $_GET['lat']);
			}

			if (!isset($_GET['lng']) && empty($_GET['lng'])) {
				$response->search_lng = -99.4238064;
			} else {
				$response->search_lng = str_replace(",", ".", $_GET['lng']);
			}

			if(!isset($_GET['reference']) && empty($_GET['reference'])) {
				$response->search_reference = "";
			} else {
				$response->search_reference = $_GET['reference'];
			}

			$response->atlas_searchbar = View::factory('frontend/atlas/searchbar');
			$response->atlas_searchbar->tags = $response->tags;
			$response->user = $this->user['id'];
		} catch(Exception $e) {
			switch($e->getCode()) {
				case 1 : Request::current()->redirect('/');
				default: var_dump($e->getMessage());
			}
		}

		$head_data = $this->template->head->data;
		$head_data["main_title"] = "Atlas de lectura";
		// Este es el título que tendrá la página para Facebook y Twitter
		$head_data["og_title"] = "Atlas de lectura | LIBROSMÉXICO.MX";

		$this->template->head->data = $head_data;

		$this->template->content->device = $this->device;
		$this->template->content->display = $display;
		$this->template->content->data = $response;	/*Test*/	
	}

	public function action_detail()
	{
		$id = $this->request->param('id');

		//Obtengo los datos del usuario
		$userfront = Session::instance()->get('user_front');

		$response = new stdClass;
		
		try{
			$display = Arr::get($_GET, 'acceso', null);
			$parameters = new stdClass;
		}catch(Exception $e){
			switch($e->getCode()){
				case 1 : Request::current()->redirect('/');
				default: var_dump($e->getMessage());
			}
		}

		$location = Model::factory('atlas')->getLocationPageById((int)$id);

		$location->user_rating = Model::factory('atlas')->getUserLocationRank((int)$id, $this->user['id']);
		//Vamos a ver que es por el slug de su label
		$location->rating_rubro_label = Model::factory('atlas')->getLocationSubjectsToQualifybySlug($location->tags[0]->slug);
		if (count($location->rating_rubro_label) == 2)
		{
			$location->user_rating_rubro[1] =Model::factory('atlas')->getUserLocationRankByRubro((int)$id, $this->user['id'], 1);
			$location->user_rating_rubro[2] =Model::factory('atlas')->getUserLocationRankByRubro((int)$id, $this->user['id'], 2);
			$location->rating_rubro[1] = Model::factory('atlas')->getLocationRankByRubro((int)$id, 1);
			$location->rating_rubro[2] = Model::factory('atlas')->getLocationRankByRubro((int)$id, 2);

		}
		else
		{
			$location->user_rating_rubro[1] =Model::factory('atlas')->getUserLocationRankByRubro((int)$id, $this->user['id'], 1);
			$location->user_rating_rubro[2] =Model::factory('atlas')->getUserLocationRankByRubro((int)$id, $this->user['id'], 2);
			$location->user_rating_rubro[3] =Model::factory('atlas')->getUserLocationRankByRubro((int)$id, $this->user['id'], 3);
			$location->rating_rubro[1] = Model::factory('atlas')->getLocationRankByRubro((int)$id, 1);
			$location->rating_rubro[2] = Model::factory('atlas')->getLocationRankByRubro((int)$id, 2);
			$location->rating_rubro[3] = Model::factory('atlas')->getLocationRankByRubro((int)$id, 3);
		}

		$location->userLikesThis = Model::factory('atlas')->userLikesLocation((int)$this->user['id'], (int)$id);
		
		$head_data = $this->template->head->data;
		$head_data["main_title"] = "Atlas de lectura";
		// Este es el título que tendrá la página para Facebook y Twitter
		$head_data["og_title"] = "Atlas de lectura | LIBROSMÉXICO.MX";

		//Saco la lista de populares
		$lugares_pop = Model::factory('atlas')->getLocationPageByRating (0, 2);

		//Saco lista de comentados
		$lugares_com = Model::factory('atlas')->getLocationPageByComments (1,2);

		//saco la lista de cercanos
		$tag_filters = Model::factory('atlas')->getTags();
		$lugares_cer = Model::factory('atlas')->getLocationPageByBounds (18, 20, -100,-90, $tag_filters, 0, 2);

		$this->template->content = View::factory('frontend/atlas/detail')
			->bind('lista_pop',$lugares_pop)
			->bind('lista_com',$lugares_com)
			->bind('lista_near',$lugares_cer)
			->bind('comentarios',$comments)
			->bind('user_id',$this->user['id'])
			->bind('user', $this->user)
			->bind('lugar',$lugar)
			->bind('location',$location)
			->bind('tags',$tag_filters)
            ->bind('id',$id);

    $response->tags = Model::factory('atlas')->getTags();

		$response->user = $this->user['id'];

		$this->template->content->device = $this->device;
		$this->template->content->display = $display;
		$this->template->content->data = $response;		
		$this->template->head->data = $head_data;

	}

	/**
	 * actualizar likes
	 */
	public function action_ajax_discussion_like()
	{
		$response = new stdClass;

			try{
				if( !isset($_POST['user'])  ) throw new Exception("Error, 1 not found", 1);
				if( !isset($_POST['like_state']) ) throw new Exception("Error, 2 not found", 1);
				if( !isset($_POST['discuss'])  ) throw new Exception("Error, 3 not found", 1);
				if( !isset($_POST['location'])  ) throw new Exception("Error, 4 not found", 1);

				$user = (int)$_POST['user'];
				$like_state = $_POST['like_state'];
				$discussion = (int)$_POST['discuss'];
				$location = (int)$_POST['location'];
				if ($like_state === "like") {
					$likeId = Model::factory('atlas')->likeDiscussion($user,$discussion);
					Model::factory('social')->addActivity('like', $likeId, $location, $user);
					$response->like_state="Ya no me gusta";
				}
				else {
					Model::factory('atlas')->dislikeDiscussion($user,$discussion);
					$response->like_state="Me gusta";	
				}
				$response->total_likes = Model::factory('atlas')->getLikesDiscussion ($discussion);
				$response->status = true;
				$response->l_id = $discussion;
			} catch( Exception $e ){

				$response->status = false;
				$response->message = $e->getMessage();
				log::instance()->add(Log::NOTICE, "// ERROR: ".$e->getMessage());

			}

			print json_encode( $response );
			exit;

	}

	public function action_ajax_comment_like()
	{
		$response = new stdClass;

			try{
				if( !isset($_POST['user'])  ) throw new Exception("Error, 1 not found", 1);
				if( !isset($_POST['like_state']) ) throw new Exception("Error, 2 not found", 1);
				if( !isset($_POST['discuss'])  ) throw new Exception("Error, 3 not found", 1);
				if( !isset($_POST['location'])  ) throw new Exception("Error, 4 not found", 1);

				$user = (int)$_POST['user'];
				$like_state = $_POST['like_state'];
				$discussion = (int)$_POST['discuss'];
				$location = (int)$_POST['location'];
				if ($like_state === "like") {
					$likeId = Model::factory('atlas')->likeComment($user,$discussion);
					Model::factory('social')->addActivity('like', $likeId, $location, $user);
					$response->like_state="Ya no me gusta";
				}
				else {
					Model::factory('atlas')->dislikeComment($user,$discussion);
					$response->like_state="Me gusta";	
				}
				$response->total_likes = Model::factory('atlas')->getLikesComment ($discussion);
				$response->status = true;
				$response->l_id = $discussion;
			} catch( Exception $e ){

				$response->status = false;
				$response->message = $e->getMessage();
				log::instance()->add(Log::NOTICE, "// ERROR: ".$e->getMessage());

			}

			print json_encode( $response );
			exit;

	}
	
	/**
	 * Obtiene las discusiones de un lugar
	 */
	public function action_ajax_get_discussions() {
		$result = array();
		$result['status'] = 'ERROR';
		$result['error'] = array('code' => null, 'message' => null);
		try {
			// Obtenenemos el ID de la ubicación
			$location_id = $this->request->post('location_id');

			// Buscamos la ubicación
			$location = Model::factory('atlas')->getLocationPageById($location_id);

			// Revisamos que el lugar exista
			if (count($location) != 1) {
				throw new Exception("No se ha encontrado la ubicación o bien la ubicación está repetida", 1001);
			}

			// Obtenemos sus discusiones
			$discussions = Model::factory('discuss')->get_discussions_by_type_and_id('location', $location_id);
			foreach($discussions as &$pd)
			{
				$tmp = Model::factory('atlas')->discussionLikedByUser($pd['id'], $this->user['id']);
				$pd['es_liked'] = $tmp;
				if ($pd['es_liked'])
				{
					$pd['like_txt'] = "Ya no me gusta";
				}
				else
				{
					if ($this->user)
					{
						$pd['like_txt'] = "Me gusta";
					}
					else
					{
						$pd['like_txt'] = "";
					}
				}
				if ((int)$pd['likes']!= 0)
				{
					if ($this->user)
					{
						$pd['like_count'] = " | ".$pd['likes']." me gusta";
					}
					else
					{
						$pd['like_count'] = " ".$pd['likes']." me gusta";
					}
				}
				else
				{
					$pd['like_count'] = "";
				}

				foreach ($pd['comments'] as &$com)
					{
						$tmp = Model::factory('atlas')->commentLikedByUser($com['id'], $this->user['id']);
						$com['es_liked'] = $tmp;
						if ($com['es_liked'])
						{
							$com['like_txt'] = "Ya no me gusta";
						}
						else
						{
							if ($this->user)
							{
								$com['like_txt'] = "Me gusta";
							}
							else
							{
								$com['like_txt'] = "";
							}
						}
						if ((int)$com['likes']!= 0)
						{
							$com['like_count'] = " | ".$com['likes']." me gusta";
						}
						else
						{
							$com['like_count'] = "";
						}
					}
			}

			// Revisamos si necesita discusiones robots
			$robot_discussions_i = 3 - count($discussions);

			// Si necesita las discusiones robots las solicitamos
			$robot_discussions = null;
			if ($robot_discussions_i > 0) {
				$current_robot_dicussions = Model::factory('discuss')->get_robot_discussions('location', $location_id);
				$robot_discussions = $this->get_random_discussions($robot_discussions_i, $current_robot_dicussions);
				foreach ($robot_discussions as &$rd) {
					$rd['resource_type'] = 'location';
					$rd['resource_id'] = $location_id;
				}
			}
			$result['robot_discussions'] = $robot_discussions;
			$result['discussions'] = $discussions;
			$result['status'] = 'SUCCESS';
			
		} catch (Exception $e) {
			$result['error']['code'] = $e->getCode();
			$result['error']['message'] = $e->getMessage();
		}
		print json_encode($result, true);
		exit;
	}

	/**
	 * Obtiene las discusiones aleatorias de los robots
	 * @param  integer $amount cantidad de discusiones aleatorias
	 * @return array con las discusiones de los robots
	 */
	private function get_random_discussions($amount, &$robot_discussions) {
		$robots = Model::factory('userfront')->get_robot_users();
		$random_discussions = array(
				array(
					'random_id' => 1,
					'topic' => '',
					'description' => '¿Recomendarías este lugar a los demás?',
					'robot_id' => 0
				), 
				array(
					'random_id' => 2,
					'topic' => '',
					'description' => '¿Qué le falta a este lugar para que sea tu favorito?',
					'robot_id' => 1
				),
				array(
					'random_id' => 3,
					'topic' => '',
					'description' => '¿Cuándo vas a ir?', 
					'robot_id' => 2
				),
				array(
					'random_id' => 4,
					'topic' => '',
					'description' => '¿Qué es lo que más te gustó?',
					'robot_id' => 2
				),
				array(
					'random_id' => 5,
					'topic' => '',
					'description' => '¿Qué es lo que menos te gustó?',
					'robot_id' => 0
				)
			);

		$not_repeated_discussions = array();
		for ($i = 0; $i < count($random_discussions); $i++) {
			$rd = $random_discussions[$i];
			if (!$this->is_description_the_same($rd['description'], $robot_discussions)) {
				$not_repeated_discussions[] = $rd;
			}
		}

		$discussions = array();
		for ($i = 0; $i < $amount; $i++) {
			$rd = $not_repeated_discussions[$i];
			$rd['user'] = $robots[$rd['robot_id']];
			$discussions[] = $rd;
		}
		return $discussions;
	}

	/**
	 * La descripción que se está buscando
	 * @param  string  &$looking    buscando
	 * @param  array  &$discussion arreglo de discusiones
	 * @return boolean
	 */
	private function is_description_the_same(&$looking, &$discussions) {
		$result = false;
		for( $i = 0; $i < count($discussions); $i++ ) {
			if ($discussions[$i]['description'] == $looking) {
				$result = true;
			}
		}
		return $result;
	}

	/**
	 * Crea la discusión del robot
	 * @return integer descripcion
	 */
	public function action_ajax_create_discussion_robot() {
		$result = array();
		$result['status'] = 'ERROR';
		$result['error'] = array('code' => null, 'message' => null);
		try {
			// Obtenenemos el ID de la ubicación
			$resource_type = $this->request->post('resource_type');
			$resource_id = $this->request->post('resource_id');
			$robot_id = $this->request->post('robot_id');
			$robot_comment = $this->request->post('robot_comment');
			$comment = $this->request->post('comment');

			if ($resource_type == null ||
				$resource_id == null ||
				$robot_id == null ||
				$robot_comment == null ||
				$comment == null) {
				throw new Exception("Faltan datos", 1001);
			}

			// Buscamos la ubicación
			$location = Model::factory('atlas')->getLocationPageById($resource_id);

			// Revisamos que el lugar exista
			if (count($location) != 1) {
				throw new Exception("No se ha encontrado la ubicación o bien la ubicación está repetida", 1002);
			}

			$discussion_id = Model::factory('discuss')->create_discussion('',
				$robot_comment,
				'location',
				$resource_id,
				$robot_id,
				true);

			$userfront_id = null;
			if ($this->user) {
				$userfront_id = $this->user['id'];
			}
			

			$comment_id = Model::factory('discuss')->comment_discussion($comment, $discussion_id, $userfront_id);

			$discussion = Model::factory('discuss')->get_discussion_by_id($discussion_id);
			if ($this->user) 
			{
				$discussion['like_txt'] = "Me gusta";
			}
			else
			{
				$discussion['like_txt'] = "";
			}

			$result['status'] = 'SUCCESS';
			$result['discussions'] = array($discussion);
		} catch (Exception $e) {
			$result['error']['code'] = $e->getCode();
			$result['error']['message'] = $e->getMessage();
		}
		print json_encode($result, true);
		exit;
	}

	/**
	 * Crear la discusión de una persona
	 */
	public function action_ajax_create_discussion() {
		$result = array();
		$result['status'] = 'ERROR';
		$result['error'] = array('code' => null, 'message' => null);
		try {
			// Obtenenemos el ID de la ubicación
			$resource_type = $this->request->post('resource_type');
			$resource_id = $this->request->post('resource_id');
			$comment = $this->request->post('comment');

			if ($resource_type == null ||
				$resource_id == null ||
				$comment == null) 
			{
				throw new Exception("Faltan datos", 1001);
			}

			// Buscamos la ubicación
			$location = Model::factory('atlas')->getLocationPageById($resource_id);

			// Revisamos que el lugar exista
			if (count($location) != 1) {
				throw new Exception("No se ha encontrado la ubicación o bien la ubicación está repetida", 1002);
			}

			$userfront_id = null;
			if ($this->user) {
				$userfront_id = $this->user['id'];
			}

			$discussion_id = Model::factory('discuss')->create_discussion('',
				$comment,
				'location',
				$resource_id,
				$userfront_id);

			$discussion = Model::factory('discuss')->get_discussion_by_id($discussion_id);
			if ($this->user) 
			{
				$discussion['like_txt'] = "Me gusta";
			}
			else
			{
				$discussion['like_txt'] = "";
			}

			$result['status'] = 'SUCCESS';
			$result['discussions'] = array($discussion);
		} catch (Exception $e) {
			$result['error']['code'] = $e->getCode();
			$result['error']['message'] = $e->getMessage();
		}
		print json_encode($result, true);
		exit;
	}

	/**
	 * Comentar una discusión
	 */
	public function action_ajax_comment_discussion() {
		$result = array();
		$result['status'] = 'ERROR';
		$result['error'] = array('code' => null, 'message' => null);
		try {
			// Obtenenemos el ID de la ubicación
			$discussion_id = $this->request->post('discussion_id');
			$comment = $this->request->post('comment');
			

			if ($discussion_id == null ||
				$comment == null) 
			{
				throw new Exception("Faltan datos", 1002);
			}

			// Buscamos la discusión
			$discussion = Model::factory('discuss')->get_discussion_by_id($discussion_id);

			if ($discussion == null) {
				throw new Exception("La discusión no existe", 1003);
			}

			$userfront_id = null;
			if ($this->user) {
				$userfront_id = $this->user['id'];
			}

			$comment_id = Model::factory('discuss')->comment_discussion($comment, $discussion_id, $userfront_id);
			$comment = Model::factory('discuss')->get_comment_by_id($comment_id);
			if ($this->user) 
			{
				$comment['like_txt'] = "Me gusta";
			}
			else
			{
				$comment['like_txt'] = "";
			}
			$result['status'] = 'SUCCESS';
			$result['comments'] = array($comment);
		} catch (Exception $e) {
			$result['error']['code'] = $e->getCode();
			$result['error']['message'] = $e->getMessage();
		}
		print json_encode($result, true);
		exit;
	}

	/**
	 * Comentar comentario
	 */
	public function action_ajax_comment_comment() {
		$result = array();
		$result['status'] = 'ERROR';
		$result['error'] = array('code' => null, 'message' => null);
		try {
			// Obtenenemos el ID de la ubicación
			$comment_id = $this->request->post('comment_id');
			$message = $this->request->post('comment');

			if ($comment_id == null ||
				$message == null) 
			{
				throw new Exception("Faltan datos", 1002);
			}

			// Buscamos la discusión
			$comment = Model::factory('discuss')->get_comment_by_id($comment_id);
			if ($comment == null) {
				throw new Exception("La discusión no existe", 1003);
			}

			$userfront_id = null;
			if ($this->user) {
				$userfront_id = $this->user['id'];
			}

			$comment_id = Model::factory('discuss')->comment_comment($message, $comment_id, $userfront_id);
			$comment = Model::factory('discuss')->get_comment_by_id($comment_id);
			if ($this->user) 
			{
				$comment['like_txt'] = "Me gusta";
			}
			else
			{
				$comment['like_txt'] = "";
			}
			$result['status'] = 'SUCCESS';
			$result['comments_comments'] = array($comment);
		} catch (Exception $e) {
			$result['error']['code'] = $e->getCode();
			$result['error']['message'] = $e->getMessage();
		}
		print json_encode($result, true);
		exit;
	}

	/*
	** Consulta de lugares que ya han sido evaluados
	*/
	public function action_show_location_evaluated()
	{
		$list = Model::factory('atlas')->getLocationsAlreadyEvaluated();
		//var_dump($list);
		$this->template->content = View::factory('frontend/atlas/show_location_evaluated')
			->bind('lista',$list);
	}
}
