<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Contento Frontend Categories controller.
 *
 * @package    Contento/Frontend
 * @category   Controllers
 * @author     Abargon
 * @copyright  (c) 2013 Abargon
 * @license    http://abargon.com
 */
class Controller_Frontend_Categories extends Controller_Frontend_Template {

	public function action_dewey(){ //Trae todas las categorias del sistem Dewey 

		$this->category = Model::factory('Categories');
		$data['categories'] = $this->category->fetch_categories();
		$this->template->content->data = $data;
	}

	public function action_search(){
		
		$category = $this->request->param('category');
		$this->category = Model::factory('Book');
		$data['books'] = $this->category->get_by_categories($category);			
		$this->template->content->data = $data;			
	}


}