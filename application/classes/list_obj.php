<?php defined('SYSPATH') or die('No direct script access.');
  /**
   * 
   * Class List Obj
   *
   * @param int $id List Id
   * @param int $name List Name
   * @param int $user User Id
   * @param string $description List Description
   * @param bool $locked List Lock Setting 
   * @param enum $privacy  public or private
   * @param enum $type Classic or Dynamic
   * @param int $numbooks Number of books
   * @param array $keywords Keywords of list
   * @param array $books Array of books
   * @return List Obj
   */
class list_obj {
	
	public function __construct ($id,$name,$slug,$user,$description,$locked,$privacy,$type,$num_books,$keywords,$books){
		$this->id = $id;
		$this->name = $name;
		$this->slug = $slug;
		$this->user = $user;
		$query= DB::select('fullname')->from('userfront')->where('id','=',$user)->execute();
		$this->username = $query[0]['fullname'];
		$this->num_books   = $num_books;
		$this->description = $description;
		$this->privacy = $privacy;
		$this->locked = $locked;
		$this->type = $type;
		$this->keywords = $keywords;		
		$this->books = $books;
		$this->likes = DB::select()->from('pi_like')->where('resource','=',"list")->and_where("resource_id","=",$id)->execute()->count();
		$this->created_on = strtotime(DB::select('created_on')->from('pi_list')->where('id','=',$id)->execute()->as_array()[0]['created_on']);
	}

	public function getId(){
		return $this->id;
	}

	public function getName(){
		return $this->name;
	}

	public function getUser(){
		return $this->user;
	}

	public function getUserName(){
		return $this->username;
	}

	public function getLocked(){
		return $this->locked;
	}

	public function getPrivacy(){
		return $this->privacy;
	}

	public function getType(){
		return $this->type;
	}

	public function getDescription(){
		return $this->description;
	}

	public function getNumBooks(){
		return $this->num_books;
	}

	public function getKeyWords(){
		return $this->keywords;
	}

	public function getBooks(){
		return $this->books;
	}
}
