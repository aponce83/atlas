<?php 
/**
 * Todas las rutas llevan como prefijo admin!
 */
define(PI_BACKEND_DIR, 'xbackend');

// Iniciar sesión
Route::set('admin_login', 'admin(/login)')
  ->defaults(array(
    'directory'  => PI_BACKEND_DIR,
    'controller' => 'admin',
    'action'     => 'login'
  ));

// Cerrar sesión
Route::set('admin_logout', 'admin/logout')
  ->defaults(array(
    'directory'  => PI_BACKEND_DIR,
    'controller' => 'admin',
    'action'     => 'logout'
  ));

// Realiza el procesamiento del inicio de sesión
Route::set('admin_do_login', 'admin/do_login')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'admin',
    'action'  => 'do_login'
  ));

// Homepage del admin
Route::set('admin_home', 'admin/home')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'admin',
    'action'  => 'home'
  ));

// Ajax del admin
Route::set('admin_ajax_get_amount_of_users_by_date', 'admin/ajax/get_amount_of_users_by_date')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'ajax',
    'action'  => 'get_amount_of_users_by_date'
  ));

Route::set('admin_ajax_get_book_tracking_by_date', 'admin/ajax/get_book_tracking_by_date')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'ajax',
    'action' => 'get_book_tracking_by_date'
  ));

Route::set('admin_ajax_get_platform_metrics', 'admin/ajax/get_platform_metrics')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'ajax',
    'action' => 'get_platform_metrics'
  ));

Route::set('admin_ajax_get_search_info_in_groups', 'admin/ajax/get_search_info_in_groups')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'ajax',
    'action' => 'get_search_info_in_groups'
    ));


// Indice del menú
Route::set('admin_menus', 'admin/menus')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'menu',
    'action'  => 'index'
  ));

// Actualización del menu
Route::set('admin_menus_update', 'admin/menus/update')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'menu',
    'action'  => 'update'
  ));

// Indice de los usuarios
Route::set('admin_users', 'admin/users')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'user',
    'action'  => 'index'
  ));

// Actualización de los usuarios
Route::set('admin_users_update', 'admin/users/update')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'user',
    'action'  => 'update'
  ));

// Trivias
Route::set('admin_trivias', 'admin/trivias')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'trivia',
    'action' => 'index'
  ));

Route::set('admin_trivias_update', 'admin/trivias/update')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'trivia',
    'action' => 'update'
  ));

Route::set('admin_trivias_answers', 'admin/trivias/answers')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'trivia',
    'action' => 'answer'
  ));

Route::set('admin_trivias_answers_update', 'admin/trivias/answers/update')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'trivia',
    'action' => 'answer_update'
  ));

Route::set('admin_trivias_questions', 'admin/trivias/questions')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'trivia',
    'action' => 'question'
  ));

Route::set('admin_trivias_questions_update', 'admin/trivias/questions/update')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'trivia',
    'action' => 'question_update'
  ));

/**
 * Por aprobar
 */
Route::set('admin_trivias_waiting', 'admin/trivias/waiting')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'trivia',
    'action' => 'waiting'
  ));

/**
 * Aprobación de la trivia
 */
Route::set('admin_trivias_approval', 'admin/trivia/approval')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'trivia',
    'action' => 'approval'
  ));

// Juegos
Route::set('admin_games_awards', 'admin/games/awards')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'games',
    'action' => 'awards'
  ));

Route::set('admin_games_awards_update', 'admin/games/awards/update')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'games',
    'action' => 'awards_update'
  ));

// Super administradores
Route::set('admin_supers', 'admin/supers')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'super',
    'action' => 'index'
  ));

Route::set('admin_supers_add', 'admin/supers/add')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'super',
    'action' => 'add'
  ));

Route::set('admin_supers_update', 'admin/supers/update')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'super',
    'action' => 'update'
  ));

// Administración de roles
Route::set('admin_roles', 'admin/roles')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'super',
    'action' => 'role'
    ));

Route::set('admin_roles_add', 'admin/roles/add')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'super',
    'action' => 'role_add'
    ));

Route::set('admin_roles_update', 'admin/roles/update')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'super',
    'action' => 'role_update'
    ));

Route::set('admin_discussion', 'admin/discussion') 
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'discussion',
    'action' => 'index'
  ));

Route::set('admin_discussion_delete', 'admin/discussion/<discussion_id>/delete', array('discussion_id' => '[0-9\-]+')) 
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'discussion',
    'action' => 'delete_discussion'
  ));

Route::set('admin_comment_delete', 'admin/comment/<comment_id>/delete', array('comment_id' => '[0-9\-]+')) 
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'discussion',
    'action' => 'delete_comment'
  ));

/*
Route::set('admin_users_give_trophies', 'admin/users/give_trophies')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'user',
    'action' => 'give_trophies'
  ));
*/

// Hace la creación de todos los lugares
/*Route::set('admin_locations_import_all', 'admin/locations/import_all')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'location',
    'action' => 'import_all'
    ));
*/

// Migra la tabla de amigos a seguidos
/*
Route::set('admin_migrate_friends', 'admin/migrate/friends')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'user',
    'action' => 'migrate_friends'
    ));
*/


/****************************** API *********************************/
// Listado de usuarios en espera
Route::set('admin_api_waiting', 'admin/api/waiting')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'api',
    'action' => 'waiting'
    ));

// Listado de todos los usuarios
Route::set('admin_api_index', 'admin/api(/index)')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'api',
    'action' => 'index'
    ));

// Realización de la aprobación
Route::set('admin_api_approval', 'admin/api/approval')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'api',
    'action' => 'approval'
    ));

/****************************** LM API Log *********************************/
// Información de las peticiones realizadas al sistema
Route::set('admin_lmapi_log', 'admin/lmapi_log')
  ->defaults(array(
    'directory' => PI_BACKEND_DIR,
    'controller' => 'lmapi',
    'action' => 'index'
    ));

