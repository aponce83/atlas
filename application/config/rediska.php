<?php
return array(
    Rediska::DEFAULT_NAME => array (
        'name'         => Rediska::DEFAULT_NAME,
        'namespace'    => 'app::',
        'servers'      => array(
                /*array('host' => 'pub-redis-18666.us-east-1-3.6.ec2.redislabs.com',
                    'port' => 18666,
                    'password' => 'RxJtTO9SX5GE1lG')*/
                array('host' => '127.0.0.1',
                    'port' => 6379)
            )
    ),
    'cache'   => array (
        'name'         => 'cache',
        'namespace'    => 'cache::',
    ),
    'session'  => array (
        'name'         => 'session',
        'namespace'    => 'session::',
    )
);

?>
