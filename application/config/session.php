<?php defined('SYSPATH') or die('No direct access allowed.');

return array(
	'database' => array(
		'name' => 'session',
		'lifetime' => 1800,
		'group' => 'default',
		'table' => 'sys_session',
		'columns' => array(
			'session_id'  => 'id',
			'last_active' => 'last_active',
			'contents'    => 'contents'
		),
		'gc' => 500,
	),
);