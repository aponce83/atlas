<?php defined('SYSPATH') or die('No direct access allowed.');
$db = array();
if (Kohana::$environment == Kohana::PRODUCTION) {
	$db['default'] = array
		(
			'type'       => 'mysql',
			'connection' => array(
				'hostname'   => 'localhost',
				'database'   => 'librosmexico_front',
				'username'   => 'lmx_front',
				'password'   => 'b38r74m5U0X75fF',
				'persistent' => FALSE,
			),
			'table_prefix' => '',
			'charset'      => 'utf8',
			'caching'      => FALSE,
			'profiling'    => FALSE,
		);
} 
elseif (Kohana::$environment == Kohana::STAGING)
{
	$db['default'] =  array
		(
			'type'       => 'mysql',
			'connection' => array(
				'hostname'   => 'localhost',
				'database'   => 'conaculta2',
				'username'   => 'root',
				'password'   => '',
				'persistent' => FALSE,
			),
			'table_prefix' => '',
			'charset'      => 'utf8',
			'caching'      => FALSE,
			'profiling'    => TRUE,
		);
}
elseif (Kohana::$environment == Kohana::DEVELOPMENT)
{
	$db['default'] =  array
		(
			'type'       => 'mysql',
			'connection' => array(
				'hostname'   => 'localhost',
				'database'   => 'librosmexico_front',
				'username'   => 'lmx_front',
				'password'   => 'b38r74m5U0X75fF',
				'persistent' => FALSE,
			),
			'table_prefix' => '',
			'charset'      => 'utf8',
			'caching'      => FALSE,
			'profiling'    => TRUE,
		);
}
$db['conaculta'] = array
(
	'type' => 'mysql',
	'connection' => array(
		'hostname' => '192.163.213.53',
		'database' => 'conacultaX',
		'username' => 'conaculta',
		'password' => 'mtWGpbPoAgzQW0L',
		'persistent' => FALSE,
	),
	'table_prefix' => '',
	'charset' => 'utf8',
	'caching' => FALSE,
	'profiling' => TRUE,
);
return $db;
