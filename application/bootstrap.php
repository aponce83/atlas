<?php defined('SYSPATH') or die('No direct script access.');
 
// -- Environment setup --------------------------------------------------------

// Load the core Kohana class
require SYSPATH.'classes/kohana/core'.EXT;

if (is_file(APPPATH.'classes/kohana'.EXT))
{
	// Application extends the core
	require APPPATH.'classes/kohana'.EXT;
}
else
{
	// Load empty core extension
	require SYSPATH.'classes/kohana'.EXT;
}

/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set('America/Mexico_City');

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
setlocale(LC_ALL, 'es_MX.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://kohanaframework.org/guide/using.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('es-mx');

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV']))
{
	Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
}

if($_SERVER['SERVER_NAME'] == 'librosmexico.mx' || 
	$_SERVER['SERVER_NAME'] == 'www.librosmexico.mx') {
	Kohana::$environment = Kohana::PRODUCTION;
} else if ($_SERVER['SERVER_NAME'] == 'dev.librosmexico.mx') {
	Kohana::$environment = Kohana::STAGING;
} else if ($_SERVER['SERVER_NAME']=='localhost') {
	Kohana::$environment = Kohana::DEVELOPMENT;
} else {
	Kohana::$environment = Kohana::DEVELOPMENT;
}

/**
 * Initialize Kohana, setting the default options..
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
if (Kohana::$environment == Kohana::PRODUCTION) {
	Kohana::init(array(
	'base_url'   => '/',
	'errors' => false
	));
} else if (Kohana::$environment == Kohana::STAGING) {
	Kohana::init(array(
	'base_url'   => '/',
	'errors' => true
	));
} else if (Kohana::$environment == Kohana::DEVELOPMENT) {
	Kohana::init(array(
	'base_url' => '/www.librosmexico.mx/',
	'index_file' => false,
	'errors' => true
	));
}

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH.'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);
Kohana::$config->attach(new Kohana_Config_File('images_data'));

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
	'auth'       => MODPATH.'auth',       // Basic authentication
	'cache'      => MODPATH.'cache',      // Caching with multiple backends
	'database'   => MODPATH.'database',   // Database access
	'image'      => MODPATH.'image',      // Image manipulation
	'userguide'  => MODPATH.'userguide',  // User guide and API documentation
	'contento'   => MODPATH.'contento',   // Contento modules: Auth, Pagination, Model, etc
	'email'      => MODPATH.'email',      // Shadowhand email wrapper
	'librosmexicoapi' => MODPATH.'librosmexicoapi',	// Libros Mexico API
	'rediska' => MODPATH.'rediska',
	'simplehtmldom' => MODPATH.'simplehtmldom',
	'password' => MODPATH.'password'
));

/**
 * Set the cookie salt. This is required for enabling database sessions
 */
Cookie::$httponly = TRUE;
/**
 * Set cookie salt
 */
Cookie::$salt = '%dvNVD!q2nLp^egO32aEZA@0XV5CC3AATJf#E#n!yqzg!g0LuwPq%l0yYlF0et&U';

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
/**
 * Backend
 * Backend module comes first in order to avoid 'pages' route handle it
 */
// route::set('backend', array('Controller_Backend_Template', 'route'));

/**
 * Frontend
 */
require_once('routes.php');

/**
 * Backend
 */
require_once('routes_backend.php');

/**
 * Api
 */
require_once('routes_api.php');


?>
