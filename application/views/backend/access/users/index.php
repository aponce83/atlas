		<?php if($action_add): ?><button type="button" class="button add" data-action="<?=$action_add?>">Agregar nuevo</button><?php endif; ?>
		<div class="box">
			<div class="box-header clearfix">
				<h2>Listado</h2>
				<p class="filters">
					<span>Ver:</span>
					<a href="#" data-status="-1"<?php if($status==-1): ?> class="selected"<?php endif; ?>>Todos</a>
					<a href="#" data-status="1"<?php if($status==1): ?> class="selected"<?php endif; ?>>Activo</a>
					<a href="#" data-status="0"<?php if($status==0): ?> class="selected"<?php endif; ?>>Inactivo</a>
				</p>
			</div>
			<div class="box-body">
				<form name="list" action="" method="get" class="clearfix" data-token="<?=Security::token()?>">
					<input type="hidden" name="id" value="" />
					<input type="hidden" name="csrf_token" value="" />
					<input type="hidden" name="page" value="<?=$page?>" />
					<input type="hidden" name="order_by" value="<?=$order_by?>" />
					<input type="hidden" name="sort" value="<?=$sort?>" />
					<input type="hidden" name="status" value="<?=$status?>" />
					<ul class="box-search clearfix">
						<li>
							<h3>Filtros</h3>
							<p>Utilice los filtros para refinar los resultados mostrados en la tabla</p>
						</li>
						<li>
							<label>Texto <span>(Nombre, correo electr&oacute;nico)</span></label>
							<input type="text" name="text" value="<?=$text?>" />
						</li>
						<li>
							<button type="submit" class="button small">Filtrar resultados</button>
						</li>
					</ul>
					<div class="box-table search clearfix">
						<?php if( ! count($data)): ?>
						<div class="message info">No existen registros.</div>
						<?php else: ?>
						<table>
							<tbody>
								<tr>
									<th width="5"><input type="checkbox" name="select-all" value="1" data-tooltip="Seleccionar" /></th>
									<th class="sortable asc" abbr="last_name" title="Ordenar por apellidos">Apellidos</th>
									<th class="sortable asc" abbr="first_name" title="Ordenar por nombre">Nombre</th>
									<th class="sortable" abbr="date_created" title="Ordenar por fecha de registro">Fecha registro</th>
									<th class="sortable" abbr="last_login" title="Ordenar por &uacute;ltima sesi&oacute;n">&Uacute;ltima sesi&oacute;n</th>
									<th class="sortable" abbr="status" title="Ordenar por estatus">Estatus</th>
									<th width="5">Acciones</th>
								</tr>
								<?php foreach($data as $item): ?>
								<tr class="<?=$item['mode']?>">
									<td data-itemname="<?=$item['name']?>"><input type="checkbox" name="id[]" value="<?=$item['id']?>" class="select" /></td>
									<td><?=$item['last_name']?></td>
									<td><?=$item['first_name']?></td>
									<td nowrap="nowrap"><?=Timestamp::format($item['date_created'], '%d-%b-%y %H:%M')?></td>
									<td nowrap="nowrap"><?=Timestamp::format($item['last_login'], '%d-%b-%y %H:%M')?></td>
									<td class="status"><?=$item['status']?></td>
									<td class="actions">
										<?php if($action_edit): ?><a href="<?=$action_edit?>?id=<?=$item['id']?>" class="edit" data-tooltip="Editar">Editar</a><?php endif; ?>
										<?php if($action_delete): ?><a href="<?=$action_delete?>?id=<?=$item['id']?>" class="delete" data-tooltip="Borrar" data-id="<?=$item['id']?>">Borrar</a><?php endif; ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<div class="bulk">
							<?php if($action_delete OR $action_status): ?>
							<select name="command">
								<option value="">Seleccione acci&oacute;n</option>
								<?php if($action_delete): ?><option value="delete">Borrar</option><?php endif; ?>
								<?php if($action_status): ?><option value="1">Activar</option><?php endif; ?>
								<?php if($action_status): ?><option value="0">Desactivar</option><?php endif; ?>
							</select>
							<button type="button" class="button small bulk disabled" data-action="<?=$action_status?>">Aplicar a seleccionados</button>
							<?php endif; ?>
						</div>
						<?=$page_links?>
						<?php endif;?>
					</div>
				</form>
			</div>
		</div>