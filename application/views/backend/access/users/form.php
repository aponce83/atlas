		<form name="save" action="" method="post" class="form validate">
			<input type="hidden" name="id" value="<?=$data['id']?>" />
			<div class="sidebar">
				<?php if($action_status): ?>
				<div class="sidebox">
					<h2>Estatus</h2>
					<div class="field">
						<label class="option"><input type="radio" name="status" value="1"<?php if($data['status']==1): ?> checked="checked"<?php endif; ?> /> Activo</label>
						<label class="option"><input type="radio" name="status" value="0"<?php if($data['status']==0): ?> checked="checked"<?php endif; ?> /> Inactivo</label>
					</div>
				</div>
				<?php endif; ?>
				<?php if($data['id']): ?>
				<div class="sidebox">
					<h2>&Uacute;ltima modificaci&oacute;n</h2>
					<p class="last-modified"><strong><?=$data['log_user']?></strong><br /> <?=Timestamp::format($data['log_time'], '%d de %B del %Y a las %H:%M')?></p>
				</div>
				<?php endif; ?>
			</div>
			<div class="fieldset">
				<h2>Datos generales</h2>
				<div class="field">
					<label>Nombre <span class="req">*</span></label>
					<input type="text" name="first_name" value="<?=$data['first_name']?>" class="required" title="Escriba el nombre" />
				</div>
				<div class="field">
					<label>Apellidos  <span class="req">*</span></label>
					<input type="text" name="last_name" value="<?=$data['last_name']?>" class="required" title="Escriba los apellidos" />
				</div>
				<div class="field">
					<label>Correo electr&oacute;nico <span class="req">*</span></label>
					<input type="text" name="email" value="<?=$data['email']?>" class="required email" title="Escriba el correo electr&oacute;nico" />
				</div>
				<br />
				<h2>Datos de acceso</h2>
				<?php if($data['id']): ?>
				<div class="message info">Llene los siguientes campos solamente si desea cambiar la contrase&ntilde;a.</div>
				<?php endif; ?>
				<div class="field">
					<label>Contrase&ntilde;a<?php if(!$data['id']): ?> <span class="req">*</span><?php endif; ?></label>
					<input type="password" name="password" class="<?php if(!$data['id']): ?>required <?php endif; ?>password" title="Escriba la contrase&ntilde;a" />
				</div>
				<div class="field third">
					<label>Confirmaci&oacute;n de contrase&ntilde;a<?php if(!$data['id']): ?> <span class="req">*</span><?php endif; ?></label>
					<input type="password" name="confirm_password" class="<?php if(!$data['id']): ?>required <?php endif; ?>confirm password" title="Escriba la confirmaci&oacute;n de la contrase&ntilde;a" />
				</div>
				<br />
				<!--
				<h2>Permisos adicionales</h2>
				<div class="field">
					<label>&Aacute;reas de desarrollo</label>
					<div class="list">
						<label class="option"><input type="checkbox" name="opciones[]" value="1" /> Opci�n 1</label>
						<label class="option"><input type="checkbox" name="opciones[]" value="2" /> Opci�n 2</label>
						<label class="option"><input type="checkbox" name="opciones[]" value="3" /> Opci�n 3</label>
						<label class="option"><input type="checkbox" name="opciones[]" value="4" /> Opci�n 4</label>
						<label class="option"><input type="checkbox" name="opciones[]" value="5" /> Opci�n 5</label>
						<label class="option"><input type="checkbox" name="opciones[]" value="6" /> Opci�n 6</label>
						<label class="option"><input type="checkbox" name="opciones[]" value="7" /> Opci�n 7</label>
						<label class="option"><input type="checkbox" name="opciones[]" value="8" /> Opci�n 8</label>
						<label class="option"><input type="checkbox" name="opciones[]" value="9" /> Opci�n 9</label>
						<label class="option"><input type="checkbox" name="opciones[]" value="10" /> Opci�n 10</label>
					</div>
				</div>
				<br />
				-->
				<div class="buttons">
					<button type="submit" class="button">Guardar</button>
					<button type="button" class="button cancel">Cancelar</button>
					<?php if($data['id'] AND $action_delete): ?><button type="button" class="button delete">Eliminar</button><?php endif; ?>
				</div>
			</div>
		</form>
