		<form name="save" action="" method="post" class="form validate">
			<input type="hidden" name="id" value="<?=$data['id']?>" />
			<div class="sidebar">
				<?php if($action_status): ?>
				<div class="sidebox">
					<h2>Estatus</h2>
					<div class="field">
						<label class="option"><input type="radio" name="status" value="1"<?php if($data['status']==1): ?> checked="checked"<?php endif; ?> /> Activo</label>
						<label class="option"><input type="radio" name="status" value="0"<?php if($data['status']==0): ?> checked="checked"<?php endif; ?> /> Inactivo</label>
					</div>
				</div>
				<?php endif; ?>
				<div class="sidebox">
					<h2>Tutor</h2>
					<div class="field">
						<label class="option"><input type="radio" name="tutor" value="1"<?php if($data['tutor']==1): ?> checked="checked"<?php endif; ?> /> S&iacute;</label>
						<label class="option"><input type="radio" name="tutor" value="0"<?php if($data['tutor']==0): ?> checked="checked"<?php endif; ?> /> No</label>
					</div>
				</div>
				<?php if($data['id']): ?>
				<div class="sidebox">
					<h2>&Uacute;ltima modificaci&oacute;n</h2>
					<p class="last-modified"><strong><?=$data['log_user']?></strong><br /> <?=Timestamp::format($data['log_time'], '%d de %B del %Y a las %H:%M')?></p>
				</div>
				<?php endif; ?>
			</div>
			<div class="fieldset">
				<h2>Datos generales</h2>
				<div class="field">
					<label>Nombre <span class="req">*</span></label>
					<input type="text" name="name" value="<?=$data['name']?>" class="required" title="Escriba el nombre del perfil" />
				</div>
				<br />
				<h2>Permisos</h2>
				<table class="permissions" width="500">
					<?php foreach($modules as $level1): ?>
						<tr>
							<th style="background:#f3f3f3;"><?=$level1['name']?></th>
							<th style="background:#f3f3f3;">Ver</th>
							<th style="background:#f3f3f3;">Agregar</th>
							<th style="background:#f3f3f3;">Editar</th>
							<th style="background:#f3f3f3;">Borrar</th>
							<th style="background:#f3f3f3;">Estatus</th>
							<th style="background:#f3f3f3;">Ordenar</th>
						</tr>
						<?php foreach($level1['modules'] as $level2): ?>
						<tr>
							<td class="level2"><?=$level2['name']?></td>
							<td align="center"><?php if(in_array(1, $level2['permissions'])): ?><input type="checkbox" name="access_control[]" value="<?=$level2['id']?>.1"<?php if(in_array($level2['id'].'.1', (array)$data['access_control'])): ?> checked="checked"<?php endif; ?> /><?php endif; ?></td>
							<td align="center"><?php if(in_array(2, $level2['permissions'])): ?><input type="checkbox" name="access_control[]" value="<?=$level2['id']?>.2"<?php if(in_array($level2['id'].'.2', (array)$data['access_control'])): ?> checked="checked"<?php endif; ?> /><?php endif; ?></td>
							<td align="center"><?php if(in_array(3, $level2['permissions'])): ?><input type="checkbox" name="access_control[]" value="<?=$level2['id']?>.3"<?php if(in_array($level2['id'].'.3', (array)$data['access_control'])): ?> checked="checked"<?php endif; ?> /><?php endif; ?></td>
							<td align="center"><?php if(in_array(4, $level2['permissions'])): ?><input type="checkbox" name="access_control[]" value="<?=$level2['id']?>.4"<?php if(in_array($level2['id'].'.4', (array)$data['access_control'])): ?> checked="checked"<?php endif; ?> /><?php endif; ?></td>
							<td align="center"><?php if(in_array(5, $level2['permissions'])): ?><input type="checkbox" name="access_control[]" value="<?=$level2['id']?>.5"<?php if(in_array($level2['id'].'.5', (array)$data['access_control'])): ?> checked="checked"<?php endif; ?> /><?php endif; ?></td>
							<td align="center"><?php if(in_array(6, $level2['permissions'])): ?><input type="checkbox" name="access_control[]" value="<?=$level2['id']?>.6"<?php if(in_array($level2['id'].'.6', (array)$data['access_control'])): ?> checked="checked"<?php endif; ?> /><?php endif; ?></td>
						
						</tr>
						<?php endforeach; ?>
					<?php endforeach; ?>
				</table>
				<br />
				<div class="buttons">
					<button type="submit" class="button">Guardar</button>
					<button type="button" class="button cancel">Cancelar</button>
					<?php if($data['id'] AND $action_delete): ?><button type="button" class="button delete">Eliminar</button><?php endif; ?>
				</div>
			</div>
		</form>
