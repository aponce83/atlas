	<form id="login" action="" method="post" class="login validate">
		<div class="box">
			<div class="box-header clearfix">
				<h2>Recupera Contrase&ntilde;a</h2>
			</div>
			<div class="box-body">
				<div class="box-login">
					<div class="field">
						<label>Correo electr&oacute;nico</label>
						<input type="text" name="email" value="" class="required email" title="Escriba su correo electr&oacute;nico" />
					</div>
					<button type="submit" class="button submit">Continuar</button>
				</div>
			</div>
		</div>
		<?php if ($success && $sent): ?>
			<div class="message"><div class="success"><?=__('Your new password has been sent to you email address')?></div></div>
		<?php endif; ?>
		<?php if ( ! $success): ?>
			<div class="message"><div class="warning"><?=__('Username does not exist')?></div></div>
		<?php endif; ?>
	</form>