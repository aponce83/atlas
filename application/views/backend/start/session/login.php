	<form id="login" action="" method="post" class="login validate">
		<div class="box">
			<div class="box-header clearfix">
				<h2>Login</h2>
			</div>
			<div class="box-body">
				<?php if($error): ?><div class="message error">El correo electr&oacute;nico y/o la contrase&ntilde;a son inv&aacute;lidos.</div><?php endif; ?>
				<div class="box-login">
					<div class="field">
						<label>Correo electr&oacute;nico</label>
						<input type="text" name="username" value="<?=$username?>" class="required email" title="Escriba su correo electr&oacute;nico" />
					</div>
					<div class="field">
						<label>Contrase&ntilde;a</label>
						<input type="password" name="password" class="required" title="Escriba su contrase&ntilde;a" />
					</div>
					<button type="submit" class="button submit">Continuar</button>
					<label class="option"><input type="checkbox" name="remember" value="1" /> Recordarme en este equipo</label>
				</div>
			</div>
		</div>
		<p><a href="start/session/password">Olvid&eacute; mi contrase&ntilde;a</a></p>
	</form>
<!--

<form id="login" action="" method="post" class="validate">
	<fieldset class="login">
		<label>
			<span><?=__('Username / Email')?></span>
			<input type="text" name="username" value="<?=$username?>" class="required email" />
		</label>
		<label>
			<span><?=__('Password')?></span>
			<input type="password" name="password" class="required" />
		</label>
		<label class="option">
			<input type="checkbox" name="remember" value="1" /> <?=__('Remember me')?>
		</label>
		<button type="submit" class="submit"><span><?=__('Continue')?></span></button>
	</fieldset>
	<p><a href="start/session/password"><?=__('Forgot your password?')?></a></p>
</form>
-->