
<div class="overview">
    <p>
	    Bienvenido al Administrador del sitio Libros MX.<br /> 
    	Puede obtener ayuda enviado un correo a <a href="soporte@intellego.com.mx">soporte@intellego.com.mx</a>    </p>
    <br /><br />
    <div class="left">
        <h3>Actividad Reciente</h3>
        <table class="zebra">
            <tr>
                <th>Módulo</th>
                <th>Elemento</th>
                <th>Fecha</th>
                <th>Acción</th>
            </tr>
            <?php foreach($transactions as $transaction): ?>
            <tr>
                <td><?=$transaction['module_name']?></td>
                <td><?=$transaction['item_name']?></td>
                <td><?=date('d.m.y H:i', $transaction['timestamp'])?></td>
                <td><?=Lookup::name('action', $transaction['action'])?></td>
            </tr>
            <?php endforeach; ?>
        </table>
        <br /><br />
        <h3>Historial de sesiones</h3>
        <table class="zebra">
            <tr>
                <th>Navegador</th>
                <th>IP</th>
                <th>Fecha</th>
                <th>Acción</th>
            </tr>
            <?php foreach($sessions as $session): ?>
            <tr>
                <td width="300"><?=$session['user_agent']?></td>
                <td><?=$session['remote_address']?></td>
                <td><?=date('d.m.y H:i', $session['timestamp'])?></td>
                <td><?=Lookup::name('session', $session['action'])?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>