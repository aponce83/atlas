
		<form name="save" action="" method="post" class="form validate" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?=$data['id']?>" />
            <input type="hidden" name="parent_id" value="<?=$data['parent_id']?>" />
            <input type="hidden" name="site_id" value="<?=$data['site_id']?>" /> <!-- Id Site CMM -->
			<input type="hidden" name="type_id" value="2" />
			<div class="sidebar">
				<div class="sidebox">
                    <h2>Fecha <span class="req">*</span></h2>
					<div class="field clearfix">
						<input type="text" name="date_content" value="<?=Timestamp::format($data['date_content'], '%Y-%m-%d')?>" class="date" title="Seleccione la fecha" />
						<span class="note">aaaa-mm-dd</span>
					</div>                    
					<h2>Imagen <span class="req">*</span> <span class="info">Imagen JPEG, PNG, GIF<br /> tamaño sugerido(518px width)</span></h2>
					<div class="field">
						<div class="file">
							<input type="text" name="image" value="<?=$data['image']?>" class="file" title="Selecccione el archivo" readonly="readonly" />
							<input type="file" id="picture_upload" name="picture_upload" class="uploadify" />
							<div class="preview"><img src="../assets/files/news/<?=$data['image']?>" alt="" /></div>
						</div>
					</div>                   
                                                     
					<?php if($action_status): ?>
					<h2>Estatus</h2>
					<div class="field">
						<label class="option"><input type="radio" name="status" value="1"<?php if($data['status']==1): ?> checked="checked"<?php endif; ?> /> Activo</label>
						<label class="option"><input type="radio" name="status" value="0"<?php if($data['status']==0): ?> checked="checked"<?php endif; ?> /> Inactivo</label>
					</div>
					<?php endif; ?> 
				</div>
				<?php if($data['id']): ?>
				<div class="sidebox">
					<h2>&Uacute;ltima modificaci&oacute;n</h2>
					<p class="last-modified"><strong><?=$data['log_user']?></strong><br /> <?=Timestamp::format($data['last_modified'], '%d de %B del %Y a las %H:%M')?></p>
				</div>
				<?php endif; ?>
			</div>
			
			
			<div class="fieldset">
				<h2>Información general</h2>               
				<div class="field full">
					<label>Título <span class="req">*</span></label>
					<input type="text" name="title" value="<?=$data['title']?>" class="required" title="Escriba el t&iacute;tulo" />
				</div>              
				<div class="field full">
					<label>Resumen <span class="req">*</span></label>
					<textarea name="summary" cols="50" rows="2" title="Escriba el resumen"><?=$data['summary']?></textarea>
				</div>	
				<div class="field full">
					<label>Contenido <span class="req">*</span></label>
					<textarea name="content" cols="50" rows="8" class="richtext required"  title="Escriba el contenido"><?=$data['content']?></textarea>
				</div>
                <div style="height: 400px; width: 600px;">
                </div>			             
                <br />
				
				
				
				<div class="buttons">
					<button type="submit" class="button">Guardar</button>
					<button type="button" class="button cancel">Cancelar</button>
					<?php if($data['id'] AND $action_delete): ?><button type="button" class="button delete">Eliminar</button><?php endif; ?>
				</div>                
			</div>
		</form>
<script>