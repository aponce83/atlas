
		<form name="save" action="" method="post" class="form validate" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?=$data['id']?>" />
            <input type="hidden" name="parent_id" value="<?=$data['parent_id']?>" />
            <input type="hidden" name="site_id" value="<?=$data['site_id']?>" /> <!-- Id Site CMM -->
			<div class="sidebar">
				<div class="sidebox">
					<?php if($data['parent_id']==111): ?>
                    <h2>Fecha</h2>
					<div class="field clearfix">
						<input type="text" name="date_content" value="<?=Timestamp::format($data['date_content'], '%Y-%m-%d')?>" class="date" title="Seleccione la fecha" />
						<span class="note">aaaa-mm-dd</span>
					</div>
                    <?php else: ?>
                    	<input type="hidden" name="date_content" value="<?=$data['date_content']?>" />
                    <?php endif; ?>
					
                    <?php if($data['parent_id']==7777): ?>
                    
                    <h2>Imagen <span class="req">*</span> <span class="info">Imagen JPEG, PNG, GIF<br /> <?php if($data['id']!=4): ?>tamaño sugerido(<?=$data['lb_size']?>)<?php endif; ?></span></h2>
					<div class="field">
						<div class="file">
							<input type="text" name="picture" value="<?=$data['picture']?>" class="file" title="Selecccione el archivo" readonly="readonly" />
							<input type="file" id="picture_upload" name="picture_upload" class="uploadify" />
							<div class="preview"><img src="<?=$data['src_picture']?>" alt="" /></div>
						</div>
					</div>
                    <?php else: ?>
                    	<input type="hidden" name="picture" value="<?=$data['picture']?>" />
                    <?php endif; ?>                   
                                                      
					<?php if($action_status): ?>
					<h2>Home</h2>
					<div class="field">
						<label class="option"><input type="radio" name="in_home" value="1"<?php if($data['in_home']==1): ?> checked="checked"<?php endif; ?> /> Activo</label>
						<label class="option"><input type="radio" name="in_home" value="0"<?php if($data['in_home']==0): ?> checked="checked"<?php endif; ?> /> Inactivo</label>
					</div>
                    <br />
                    <input type="hidden" name="main_article" value="0" />
					<h2>Estatus</h2>
					<div class="field">
						<label class="option"><input type="radio" name="status" value="1"<?php if($data['status']==1): ?> checked="checked"<?php endif; ?> /> Activo</label>
						<label class="option"><input type="radio" name="status" value="0"<?php if($data['status']==0): ?> checked="checked"<?php endif; ?> /> Inactivo</label>
					</div>
					<?php endif; ?>   
					<h2>Menú</h2>
					<div class="field">
						<label class="option"><input type="checkbox" name="menu_header" value="1"<?php if($data['menu_header']==1): ?> checked="checked"<?php endif; ?> /> Header</label>
						<label class="option"><input type="checkbox" name="menu_main" value="1"<?php if($data['menu_main']==1): ?> checked="checked"<?php endif; ?> /> Main</label>
						<label class="option"><input type="checkbox" name="menu_footer" value="1"<?php if($data['menu_footer']==1): ?> checked="checked"<?php endif; ?> /> Footer</label>
					</div>					               
				</div>
				<?php if($data['id']): ?>
				<div class="sidebox">
					<h2>&Uacute;ltima modificaci&oacute;n</h2>
					<p class="last-modified"><strong><?=$data['log_user']?></strong><br /> <?=Timestamp::format($data['log_time'], '%d de %B del %Y a las %H:%M')?></p>
				</div>
				<?php endif; ?>
			</div>
			<div class="fieldset">
				<h2>Datos generales</h2>
				<div class="field full">
					<label>T&iacute;tulo <span class="req">*</span></label>
					<input type="text" name="title" value="<?=$data['title']?>" class="required" title="Escriba el t&iacute;tulo" />
				</div>                
				<div class="field full">
					<label>Resumen</label>
					<textarea name="summary" cols="50" rows="2"><?=$data['summary']?></textarea>
				</div>
				<div class="field full">
					<label>Metatags</label>
					<textarea name="metatags" cols="50" rows="2"><?=$data['metatags']?></textarea>
				</div>
				<div class="field full">
					<label>Contenido</label>
					<textarea name="content" cols="50" rows="8" class="richtext"><?=$data['content']?></textarea>
				</div>
				<div class="field full">
					<label>Contenido Extra</label>
					<textarea name="content_extra" cols="50" rows="8" class="richtext"><?=$data['content_extra']?></textarea>
				</div>                
				<div class="field full">
					<label>Redireccionar <span>Utilice este campo para ir a una URL en lugar de mostrar el contenido</span></label>
					<input type="text" name="link" value="<?=$data['link']?>" />
				</div>
                <div class="field full">
                	<label>Nueva Ventana</label>
                    <input type="checkbox" name="newwindow" value="1"  <?php if($data['newwindow']) { echo "checked"; } ?> />
                </div>             
                <br />
				<div class="buttons">
					<button type="submit" class="button">Guardar</button>
					<button type="button" class="button cancel">Cancelar</button>
					<?php if($data['id'] AND $action_delete): ?><button type="button" class="button delete">Eliminar</button><?php endif; ?>
				</div>                
			</div>
		</form>
