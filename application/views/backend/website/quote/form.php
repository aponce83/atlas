
		<form name="save" action="" method="post" class="form validate" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?=$data['id']?>" />
            <input type="hidden" name="parent_id" value="<?=$data['parent_id']?>" />
            <input type="hidden" name="site_id" value="<?=$data['site_id']?>" /> <!-- Id Site CMM -->
			<input type="hidden" name="type_id" value="2" />
			<div class="sidebar">
				<div class="sidebox">                 
                                                     
					<?php if($action_status): ?>
					<h2>Estatus</h2>
					<div class="field">
						<label class="option"><input type="radio" name="status" value="1"<?php if($data['status']==1): ?> checked="checked"<?php endif; ?> /> Activo</label>
						<label class="option"><input type="radio" name="status" value="0"<?php if($data['status']==0): ?> checked="checked"<?php endif; ?> /> Inactivo</label>
					</div>
					<?php endif; ?> 
				</div>
				<?php if($data['id']): ?>
				<div class="sidebox">
					<h2>&Uacute;ltima modificaci&oacute;n</h2>
					<p class="last-modified"><strong><?=$data['log_user']?></strong><br /> <?=Timestamp::format($data['last_modified'], '%d de %B del %Y a las %H:%M')?></p>
				</div>
				<?php endif; ?>
			</div>
			
			
			<div class="fieldset">
				<h2>Información general</h2>               
				<div class="field full">
					<label>Autor <span class="req">*</span></label>
					<select name="author_id">
					<?php if($authors && is_array($authors)){?>
						<?php foreach($authors as $author){ ?>
							<option value="<?=$author['id']?>" <?=($author['id'] == $data['author_id']) ? 'selected' : ''?>><?=$author['fullname']?></option>
						<?php } ?>
					<?php } ?>
					</select>
				</div>   
				<div class="field full">
					<label>Cita <span class="req">*</span></label>
					<textarea rows="8" name="quote" class="required" title="Escriba la cita"><?=$data['quote']?></textarea>
				</div>   
                <div style="height: 400px; width: 600px;">
                </div>			             
                <br />
				
				
				
				<div class="buttons">
					<button type="submit" class="button">Guardar</button>
					<button type="button" class="button cancel">Cancelar</button>
					<?php if($data['id'] AND $action_delete): ?><button type="button" class="button delete">Eliminar</button><?php endif; ?>
				</div>                
			</div>
		</form>
<script>