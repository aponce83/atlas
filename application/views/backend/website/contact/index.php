		<div class="box">
			<div class="box-header clearfix">
				<h2>Listado</h2>
			</div>
			<div class="box-body">
				<form name="list" action="" method="get" class="clearfix" data-token="<?=Security::token()?>">
					<input type="hidden" name="id" value="" />
					<input type="hidden" name="csrf_token" value="" />
					<input type="hidden" name="status" value="" />
					<input type="hidden" name="serialized" value="" />
					<div class="box-table clearfix">
						<?php if( ! count($data)): ?>
						<div class="message info">No existen registros.</div>
						<?php else: ?>
						<div class="sortable-head">
							<strong>T&iacute;tulo</strong>
							<button type="button" class="button small cancel">Cancelar</button>
							<button type="button" class="button small serialize">Guardar</button>
						</div>
						<ol class="nested-sortable">
							<?php foreach($data as $item): ?>
							<li id="item-<?=$item['id']?>"><div><?=$item['name']?></div></li>
							<?php endforeach; ?>
						</ol>
						<table>
							<tbody>
								<tr>
									<th width="570">Nombre</th>
									<th>Correo Electr&oacute;nico</th>
                                   	<th width="5">Acciones</th>
								</tr>
								<?php foreach($data as $item): ?>
								<tr class="<?=$item['mode']?>">
									<td><?=$item['name']?></td>
									<td><?=$item['email']?></td>
									<td class="actions">
										<?php if($action_edit): ?><a href="<?=$action_edit?>?id=<?=$item['id']?>" class="edit" data-tooltip="Ver detalle">Ver detalle</a><?php endif; ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<?php endif; ?>
					</div>
				</form>
			</div>
		</div>