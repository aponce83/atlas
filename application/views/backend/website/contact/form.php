		<form name="save" action="" method="post" class="form validate">
			<input type="hidden" name="id" value="<?=$data['id']?>" />
            <input type="hidden" name="parent_id" value="<?=$data['parent_id']?>" />
            <input type="hidden" name="thumbnail" value="" />
			<div class="sidebar">
				<div class="sidebox">
				
					<h2>Fecha de envío</h2>
					<div class="field">
						<div class="file">
							<input type="text" value="<?=Timestamp::format($data['date_created'], '%d de %B del %Y')?>" disabled="disabled" />
					</div>
					</div>
				<br />
				</div>
			</div>
			<div class="fieldset" style="min-height: 1900px;">
				<h2>Informaci&oacute;n del Contacto</h2>
				<div class="field full">
					<label>Nombre</label>
					<input type="text" name="nombre" value="<?=$data['name']?>"  title="Escriba el nombre" disabled="disabled" />
				</div>
				<div class="field full">
					<label>E-mail</label>
					<input type="text" name="correo" value="<?=$data['email']?>"  title="Escriba el correo" disabled="disabled" />
				</div>
				<div class="field full">
					<label>Asunto</label>
					<input type="text" name="asunto" value="<?=$dataarea['title']?>"  title="Escriba el asunto" disabled="disabled" />
				</div>
				<!--div class="field full">
					<label>&Aacute;rea</label>
					<input type="text" name="area" value="<?=$dataarea['title']?>"  title="Escriba el &aacute;rea" disabled="disabled" />
				</div-->
				<div class="field full">
					<label>Mensaje</label>
					<textarea name="comentario" cols="50" rows="10" disabled="disabled"><?=$data['message']?></textarea>
				</div>
				<br />
				<div class="buttons">
					<button type="button" class="button cancel">Regresar</button>
				</div>                
			</div>
		</form>
