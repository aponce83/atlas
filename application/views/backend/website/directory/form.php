
		<form name="save" action="" method="post" class="form validate" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?=$data['id']?>" />
            <input type="hidden" name="parent_id" value="<?=$data['parent_id']?>" />
            <input type="hidden" name="site_id" value="<?=$data['site_id']?>" /> <!-- Id Site CMM -->
			<div class="sidebar">
				<div class="sidebox">
					<?php if($data['parent_id']==111): ?>
                    <h2>Fecha</h2>
					<div class="field clearfix">
						<input type="text" name="date_content" value="<?=Timestamp::format($data['date_content'], '%Y-%m-%d')?>" class="date" title="Seleccione la fecha" />
						<span class="note">aaaa-mm-dd</span>
					</div>
                    <?php else: ?>
                    	<input type="hidden" name="date_content" value="<?=$data['date_content']?>" />
                    <?php endif; ?>
                    <h2>Departamento</h2>
                    <select name="department_id">
                    	<option value="0"> - Seleccione un departamento - </option>
                        <?php foreach($deps as $dep){ ?>
                        	<option value="<?=$dep['id']?>" <?=$data['department_id']==$dep['id']?'selected':''?>><?=$dep['title']?></option>
                        <?php } ?>
                    </select><br /><br />
					<h2>Foto <span class="req">*</span> <span class="info">Imagen JPEG, PNG, GIF<br /> <?php if($data['id']!=4): ?>tamaño sugerido(<?=$data['lb_size']?>)<?php endif; ?></span></h2>
					<div class="field">
						<div class="file">
							<input type="text" name="picture" value="<?=$data['picture']?>" class="file" title="Selecccione el archivo" readonly="readonly" />
							<input type="file" id="picture_upload" name="picture_upload" class="uploadify" />
							<div class="preview"><img src="<?=$data['src_picture']?>" alt="" /></div>
						</div>
					</div>                   
                    
					<?php if($action_status): ?>
					<h2>Estatus</h2>
					<div class="field">
						<label class="option"><input type="radio" name="status" value="1"<?php if($data['status']==1): ?> checked="checked"<?php endif; ?> /> Activo</label>
						<label class="option"><input type="radio" name="status" value="0"<?php if($data['status']==0): ?> checked="checked"<?php endif; ?> /> Inactivo</label>
					</div>
					<?php endif; ?> 
				</div>
				<?php if($data['id']): ?>
				<div class="sidebox">
					<h2>&Uacute;ltima modificaci&oacute;n</h2>
					<p class="last-modified"><strong><?=$data['log_user']?></strong><br /> <?=Timestamp::format($data['last_modified'], '%d de %B del %Y a las %H:%M')?></p>
				</div>
				<?php endif; ?>
			</div>
			<div class="fieldset">
				<h2>Datos generales</h2>
				<div class="field full">
					<label>Nombre <span class="req">*</span></label>
					<input type="text" name="name" value="<?=$data['name']?>" class="required" title="Escriba el nombre" />
				</div>
                <div class="field full">
					<label>Apellidos <span class="req">*</span></label>
					<input type="text" name="lastname" value="<?=$data['lastname']?>" class="required" title="Escriba los paellidos" />
				</div>
                <div class="field full">
					<label>Correo Electrónico <span class="req">*</span></label>
					<input type="text" name="email" value="<?=$data['email']?>" class="required" title="Escriba el correo electrónico" />
				</div>
                <div class="field full">
					<label>Fecha de nacimiento <span class="req">*</span></label>
					<input type="text" name="birthday" value="<?=$data['birthday']?>" class="required" title="Ingrese la fecha de nacimiento" />
				</div>
                <div class="field full">
					<label>Teléfono <span class="req">*</span></label>
					<input type="text" name="phone" value="<?=$data['phone']?>" class="required" title="Escriba el número telefonico" />
				</div>
                <div class="field full">
					<label>Extensión <span class="req">*</span></label>
					<input type="text" name="extension" value="<?=$data['extension']?>" class="required" title="Escriba la extension" />
				</div>  				
                <div style="height: 524px;">
                </div>			             
                <br />
				<div class="buttons">
					<button type="submit" class="button">Guardar</button>
					<button type="button" class="button cancel">Cancelar</button>
					<?php if($data['id'] AND $action_delete): ?><button type="button" class="button delete">Eliminar</button><?php endif; ?>
				</div>                
			</div>
		</form>
