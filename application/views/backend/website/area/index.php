		<?php if($action_add): ?><button type="button" class="button add" data-action="<?=$action_add?>">Agregar nuevo</button><?php endif; ?>
		<div class="box">
			<div class="box-header clearfix">
				<h2>Listado</h2>
			</div>
			<div class="box-body">
				<form name="list" action="" method="get" class="clearfix" data-token="<?=Security::token()?>">
					<input type="hidden" name="id" value="" />
					<input type="hidden" name="csrf_token" value="" />
					<input type="hidden" name="status" value="" />
					<input type="hidden" name="serialized" value="" />
					<div class="box-table clearfix">
						<?php if( ! count($data)): ?>
						<div class="message info">No existen registros.</div>
						<?php else: ?>
						<div class="sortable-head">
							<strong>T&iacute;tulo</strong>
							<button type="button" class="button small cancel">Cancelar</button>
							<button type="button" class="button small serialize">Guardar</button>
						</div>
						<ol class="nested-sortable">
							<?php foreach($data as $item): ?>
							<li id="item-<?=$item['id']?>"><div><?=$item['name']?></div></li>
							<?php endforeach; ?>
						</ol>
						<table>
							<tbody>
								<tr>
									<th width="5"><input type="checkbox" name="select-all" value="1" data-tooltip="Seleccionar" /></th>
									<th width="570">T&iacute;tulo</th>
                                    <th>Estatus</th>
									<th width="5">Acciones</th>
								</tr>
								<?php foreach($data as $item): ?>
								<tr class="<?=$item['mode']?>">
									<td data-itemname="<?=$item['title']?>"><input type="checkbox" name="id[]" value="<?=$item['id']?>" class="select" /></td>
									<td><?=$item['title']?></td>
                                    <td class="status"><?php if($item['status']==1){?>Activo<?php }else{?>Inactivo<?php }?></td>
                                    <td class="actions">
										<?php if($action_add && $item['sublevels']): ?><a href="<?=$action_add?>?parent_id=<?=$item['id']?>" class="add" data-tooltip="Agregar">Agregar</a><?php else: ?><a href="#" class="blank">Blank</a><?php endif; ?>
										<?php if($action_edit): ?><a href="<?=$action_edit?>?id=<?=$item['id']?>" class="edit" data-tooltip="Editar">Editar</a><?php endif; ?>
										<?php if($action_delete): ?><a href="<?=$action_delete?>?id=<?=$item['id']?>" class="delete" data-tooltip="Borrar" data-id="<?=$item['id']?>">Borrar</a><?php endif; ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<div class="bulk">
							<?php if($action_delete OR $action_status OR $action_sort): ?>
							<select name="command">
								<option value="">Seleccione acci&oacute;n</option>
								<?php if($action_delete): ?><option value="delete">Borrar</option><?php endif; ?>
								<?php if($action_status): ?><option value="1">Activar</option><?php endif; ?>
								<?php if($action_status): ?><option value="0">Desactivar</option><?php endif; ?>
							</select>
							<button type="button" class="button small bulk disabled" data-action="<?=$action_status?>">Aplicar a seleccionados</button>
							<?php endif; ?>
						</div>
						<?php endif; ?>
					</div>
				</form>
			</div>
		</div>