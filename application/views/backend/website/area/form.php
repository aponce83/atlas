		<form name="save" action="" method="post" class="form validate">
			<input type="hidden" name="id" value="<?=$data['id']?>" />
			<div class="sidebar">
				<div class="sidebox">
					<?php if($action_status): ?>
					
					
					<h2>Estatus</h2>
					<div class="field">
						<label class="option"><input type="radio" name="status" value="1" <?php if($data['status']==1): ?> checked="checked"<?php endif; ?>/> Activo</label>
						<label class="option"><input type="radio" name="status" value="0" <?php if($data['status']==0): ?> checked="checked"<?php endif; ?>/> Inactivo</label>
					</div>
					<!--<h2>Home</h2>
					<div class="field">
						<label class="option"><input type="radio" name="in_home" value="1"<?php if($data['in_home']==1): ?> checked="checked"<?php endif; ?> /> Activo</label>
						<label class="option"><input type="radio" name="in_home" value="0"<?php if($data['in_home']==0): ?> checked="checked"<?php endif; ?> /> Inactivo</label>
					</div> -->                   
					<?php endif; ?>
				    <!--<h2>Imagen <span class="req">*</span> <span class="info">Imagen JPEG, PNG, GIF<br />(202px x 460px / 420px 460px)</span></h2>
					<div class="field">
						<div class="file">
							<input type="text" name="picture" value="<?=$data['picture']?>" class="file" title="Selecccione el archivo" readonly="readonly" />
							<input type="file" id="picture_upload" name="picture_upload" class="uploadify" />
							<div class="preview"><img src="<?=$data['image']?>" alt="" /></div>
						</div>
					</div>
                    <h2>Imagen over <span class="req">*</span> <span class="info">Imagen JPEG, PNG, GIF<br />(202px x 460px / 420px 460px)</span></h2>
					<div class="field">
						<div class="file">
							<input type="text" name="picture_over" value="<?=$data['picture_over']?>" class="file" title="Selecccione el archivo" readonly="readonly" />
							<input type="file" id="picture_over_upload" name="picture_over_upload" class="uploadify" />
							<div class="preview"><img src="<?=$data['src_picture_over']?>" alt="" /></div>
						</div>
					</div>-->
                    <!--<h2>Logo <span class="req">*</span> <span class="info">Imagen JPEG, PNG, GIF<br />(232px x 92px)</span></h2>
					<div class="field">
						<div class="file">
							<input type="text" name="logo" value="<?=$data['logo']?>" class="file" title="Selecccione el archivo" readonly="readonly" />
							<input type="file" id="logo_upload" name="logo_upload" class="uploadify"  />
							<div class="preview"><img src="<?=$data['src_logo']?>" alt="" /></div>
						</div>
					</div>   
					<h2>Thumbnail<span class="req">*</span> <span class="info">Imagen JPEG, PNG, GIF<br />(202px x 193px)</span></h2>
					<div class="field">
						<div class="file">
							<input type="text" name="thumbnail" value="<?=$data['thumbnail']?>" class="file" title="Selecccione el archivo" readonly="readonly" />
							<input type="file" id="thumbnail_upload" name="thumbnail_upload" class="uploadify" />
							<div class="preview"><img src="<?=$data['src_thumb']?>" alt="" /></div>
						</div>
					</div>-->
				</div>
				<?php if($data['id']): ?>
				<div class="sidebox">
					<h2>&Uacute;ltima modificaci&oacute;n</h2>
					<p class="last-modified"><strong><?=$data['log_user']?></strong><br /> <?=Timestamp::format($data['log_time'], '%d de %B del %Y a las %H:%M')?></p>
				</div>
				<?php endif; ?>
			</div>
			<div class="fieldset" style="min-height: 1900px;">
				<h2>Datos generales</h2>
				<div class="field full">
					<label>T&iacute;tulo <span class="req">*</span></label>
					<input type="text" name="title" value="<?=$data['title']?>" class="required" title="Escriba el t&iacute;tulo" />
				</div>
				<div class="field full">
					<label>Correo</label>
					<input type="text" name="email" value="<?=$data['email']?>" class="mail required"  title="Escriba el correo" />
				</div>
				<br />
				<div class="buttons">
					<button type="submit" class="button">Guardar</button>
					<button type="button" class="button cancel">Cancelar</button>
				</div>                
			</div>
		</form>
