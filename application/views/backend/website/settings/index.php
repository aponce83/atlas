		<?php if($action_add&&false): ?><button type="button" class="button add" data-action="<?=$action_add?>">Agregar nuevo</button><?php endif; ?>
        <div class="box">
			<div class="box-header clearfix">
				<h2>Listado</h2>              
                <select class="language" data-action="<?=str_replace('form', 'language', $action_add)?>">
                	<?php foreach($language as $row): ?>
                	<option value="<?=$row['id']?>" <?php if($row['id']==Session::instance()->get('language_id')) { echo "selected"; } ?>><?=$row['title']?></option>
                    <?php endforeach; ?>
                </select>
			</div>
			<div class="box-body">
				<form name="list" action="" method="get" class="clearfix" data-token="<?=Security::token()?>">
					<input type="hidden" name="id" value="" />
					<input type="hidden" name="csrf_token" value="" />
					<input type="hidden" name="status" value="" />
					<input type="hidden" name="serialized" value="" />
					<div class="box-table clearfix">
						<?php if( ! count($data)): ?>
						<div class="message info">No existen registros.</div>
						<?php else: ?>
						<div class="sortable-head">
							<strong>T&iacute;tulo</strong>
							<button type="button" class="button small cancel">Cancelar</button>
							<button type="button" class="button small serialize">Guardar</button>
						</div>
						<ol class="nested-sortable">
							<?php foreach($data as $item): ?>
							<li id="item-<?=$item['id']?>"><div><?=$item['title']?></div>
								<?php if(count($item['slide'])): ?>
								<ol>
									<?php foreach($item['slide'] as $sub): ?>
									<li id="item-<?=$sub['id']?>"><div><?=$sub['title']?></div>
										<?php if(count($sub['subpages'])): ?>
                                        <ol>
                                            <?php foreach($sub['subpages'] as $art): ?>
                                            <li id="item-<?=$art['id']?>"><div><?=$art['title']?></div></li>
                                            <?php endforeach; ?>
                                        </ol>
                                        <?php endif; ?>                                    
                                    </li>
									<?php endforeach; ?>
								</ol>
								<?php endif; ?>
							</li>
							<?php endforeach; ?>
						</ol>
						<table>
							<tbody>
								<tr>
									<th width="5"><input type="checkbox" name="select-all" value="1" data-tooltip="Seleccionar" /></th>
									<th width="130">Nombre</th>
									<th width="130">Nombre de sistema</th>
									<th width="90">Valor</th>
									<th width="40">Tipo</th>
									<th width="5">Acciones</th>
								</tr>
                                
								<?php  foreach($data as $item): ?>
								<tr class="<?=$item['mode']?>">
									<td data-itemname="<?=$item['slug']?>"><input type="checkbox" name="id[]" value="<?=$item['id']?>" class="select" /></td>
									<td><?=$item['title']?></td>
									<td><?=$item['sys_title']?></td>
									<td><?=$item['type']=='on_off'?($item['value']==1?'Activo':'Inactivo'):Text::limit_chars($item['value'], 200, '...')?></td>
									<td><?=$item['type']?></td>
									<td class="actions">
										<?php if($action_edit): ?><a href="<?=$action_edit?>?id=<?=$item['id']?>" class="edit" data-tooltip="Editar">Editar</a><?php endif; ?>
										<?php if($action_delete): ?><a href="<?=$action_delete?>?id=<?=$item['id']?>" class="delete" data-tooltip="Borrar" data-id="<?=$item['id']?>">Borrar</a><?php endif; ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<div class="bulk">
							<?php if($action_delete OR $action_status OR $action_sort): ?>
							<select name="command">
								<option value="">Seleccione acci&oacute;n</option>
								<?php if($action_delete): ?><option value="delete">Borrar</option><?php endif; ?>
							</select>
							<button type="button" class="button small bulk disabled" data-action="<?=$action_status?>">Aplicar a seleccionados</button>
							<?php if($action_sort AND count($data) > 1): ?>
							<span class="sep">&nbsp;</span>
							<button type="button" class="button small sort" data-action="<?=$action_sort?>">Ordenar</button>
							<?php endif; ?>
							<?php endif; ?>
						</div>
						<?php endif; ?>
					</div>
				</form>
			</div>
		</div>