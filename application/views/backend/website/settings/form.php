
		<form name="save" action="" method="post" class="form validate" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?=$data['id']?>" />
            <input type="hidden" name="parent_id" value="<?=$data['parent_id']?>" />
            <input type="hidden" name="site_id" value="<?=$data['site_id']?>" /> 
			 <input type="hidden" name="title" value="<?='var_name'?>" /> 
			<div class="sidebar">
				<div class="sidebox">
					
					
					<!-- booleano-->
					<?php if($data['type']=='on_off'){ ?>
					<div id="boolean">
						<h2>Valor</h2>
						<div class="field">
							<label class="option"><input type="radio" id="valor" name="value" value="1"<?php if($data['value']==1): ?> checked="checked"<?php endif; ?> /> Activo</label>
							<label class="option"><input type="radio" id="valor" name="value" value="0"<?php if($data['value']==0): ?> checked="checked"<?php endif; ?> /> Inactivo</label>
						</div>
					</div>
					<?php } ?>
					
					
					<!-- imagen-->
					<?php if($data['type']=='image'){ ?>
					<div id="img">
						<input type="hidden" name="type" value="image" />
						<h2>Imagen <span class="req">*</span> <span class="info">Imagen JPEG, PNG, GIF<br /> tamaño sugerido(518px width)</span></h2>						
							<div class="file">
								<input type="text" name="value" value="<?=$data['value']?>" class="file required" title="Selecccione el archivo" readonly="readonly" />
								<input type="file" id="picture_upload" name="picture_upload" class="uploadify" />
								<div class="preview"><img src="../assets/files/settings/<?=$data['value']?>" alt="" /></div>
							</div>				
					</div>
					<?php } ?>
					
					
				 
					<h2>&Uacute;ltima modificaci&oacute;n</h2>
					<p class="last-modified"><strong><?=$data['log_user']?></strong><br /> <?=Timestamp::format($data['log_time'], '%d de %B del %Y a las %H:%M')?></p>				               
				</div>		
			</div>
			<div class="fieldset">
				<h2>Datos Generales</h2>
				<div class="field full">
					<label>Nombre</label>
					<input type="text" name="title" readonly value="<?=$data['title']?>"  title="Escriba Nombre" />
				</div> 
				<div class="field full">
					<label>Descripcion </label>
					<textarea name="description" readonly rows="8" ><?=$data['description']?></textarea>
				</div>   
				
				
				<!--Texto corto-->
				<?php if($data['type']=='short_text'){ ?>
				<div id="shortext">
					<div class="field full">
						<label>Texto </label>
						<input type="text" name="value" class="required" value="<?=$data['value']?>"  title="Escriba Texto"/>
					</div> 
				</div> 
				<?php } ?>
				
				<!--text largo-->
				<?php if($data['type']=='long_text'){ ?>
				<div id="longtext"> 
					<div class="field full">
						<label>Texto </label>
						<textarea type="text" name="value" class="required"  rows="10"  title="Escriba Texto" ><?=$data['value']?></textarea>
					</div> 
				</div> 
				<?php } ?>
				
				<!--text enriquecido-->
				<?php if($data['type']=='rich_text'){ ?>
				<div id="longtext"> 
					<div class="field full">
						<label>Valor </label>
						<textarea type="text" name="value"  class="richtext required" rows="10"  title="Escriba Texto" ><?=$data['value']?></textarea>
					</div> 
				</div> 
				<?php } ?>
				
				
                <br />
				<div class="buttons">
					<button type="submit" class="button">Guardar</button>
					<button type="button" class="button cancel">Cancelar</button>
				</div>                
			</div>
		</form>
