 
		<div class="box">
			<div class="box-header clearfix">
				<h2>Listado</h2>
			</div>
			<div class="box-body">
				<form name="list" action="" method="get" class="clearfix" data-token="<?=Security::token()?>">
					<input type="hidden" name="id" value="" />
					<input type="hidden" name="csrf_token" value="" />
					<input type="hidden" name="page" value="<?=$page?>" />
					<input type="hidden" name="order_by" value="<?=$order_by?>" />
					<input type="hidden" name="sort" value="<?=$sort?>" />
					<input type="hidden" name="status" value="<?=$status?>" />
					<div class="box-table search editor clearfix">
						<?php if( ! count($data)): ?>
						<div class="message info">No existen registros.</div>
						<?php else: ?>
						<table>
							<tbody>
								<tr>
									<th>Id</th>
									<th>Nombre</th>
									<th>Correo electrónico</th>
									<th>Tel&eacute;fono</th>
									<th>Fecha de creación</th>
									<th width="5">Acciones</th>
								</tr>
								<?php $i=0; foreach($data['all_users'] as $item): ?>
								<tr class="<?=$item['mode']?>" id="userfront_row-<?php echo $item['id']; ?>">
									<form action="userfront/userfront/update" method="POST">
										<td><?php echo $item['id'];?></td>
										<td>
											<input 
												type="hidden" 
												name="id" 
												value="<?php echo $item['id']; ?>">
											<input 
												type="text" 
												name="fullname" 
												value="<?php echo $item['fullname']; ?>" 
												style="width:240px;"
												id="fullname-<?php echo $item['id']; ?>">
										</td>
										<td><?php echo $item['email']; ?></td>
										<td><?php echo $item['telefono']; ?></td>
										<td>
											<?php
											$mil = $item['date_created'];
											$seconds = $mil /*/ 1000*/;
											echo date("d/m/Y H:i:s", $seconds);
											?>
										</td>
										<td class="actions">
											<input 
												type="button" 
												value="Guardar" 
												class="button"
												onclick="updateUser(<?php echo $item['id']; ?>)">
										</td>
									</form>
								</tr>
								<?php endforeach; $i++;?>
							</tbody>
						</table>
						<!--div class="bulk">
							<?php if($action_delete OR $action_status): ?>
							<select name="command">
								<option value="">Seleccione acci&oacute;n</option>
								<?php if($action_delete): ?><option value="delete">Borrar</option><?php endif; ?>
								<?php if($action_status): ?><option value="1">Activar</option><?php endif; ?>
								<?php if($action_status): ?><option value="0">Desactivar</option><?php endif; ?>
							</select>
							<button type="button" class="button small bulk disabled" data-action="<?=$action_status?>">Aplicar a seleccionados</button>
							<?php endif; ?>
						</div-->
						<?=$pagination?>
						<?php endif;?>
					</div>
				</form>
			</div>
		</div>
<script>
	function updateUser(userfront_id) {
		var name_id = "#fullname-"+userfront_id;
		var new_name = $(name_id).val();
		var row_id = "#userfront_row-"+userfront_id;
		$.ajax({
	    url: "userfront/userfront/form",
	    type: "post",
	    data: { userfront_id: userfront_id,
	    fullname: new_name },
	    dataType: "json",
	    success: function (data) {
	      if (data.status == 'success') {
	      	$(row_id).css('background-color', 'green');
	      } else {
	      	$(row_id).css('background-color', 'red');
	      	console.log(data);
	      	console.log('Hubo un error');
	      }
	    }
	  });
	}
</script>