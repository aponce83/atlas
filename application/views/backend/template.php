<!doctype html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<base href="<?=URL::base(TRUE)?>admin/" />
	<script type="text/javascript" src="../assets/scripts/jquery-1.7.1.min.js"></script>
	<link rel="stylesheet" type="text/css" media="screen, projection" href="../assets/styles/backend.css" />
    <link rel="stylesheet" type="text/css" href="../assets/chosen/chosen.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	
	<script type="text/javascript" src="../assets/scripts/jquery-ui-1.8.18.custom.min.js"></script>
	<script type="text/javascript" src="../assets/scripts/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript" src="../assets/scripts/jquery.ui.nestedSortable.js"></script>
	<script type="text/javascript" src="../assets/uploadify/jquery.uploadify-3.1.min.js"></script>
	<script type="text/javascript" src="../assets/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="../assets/ckeditor/adapters/jquery.js"></script>
	<script type="text/javascript" src="../assets/scripts/backend.js"></script>
    <script src="../assets/chosen/chosen.jquery.js" type="text/javascript"></script>
	<title><?=$title?> - Consola de administración - Abarcontent</title>
	
</head>
<body>
<div id="wrapper" class="navbar">
	<div id="header" class="clearfix">
		<h2>Consola de administración para <strong>Abarcontent</strong> <span>(<a href="<?=URL::base(TRUE)?>" target="_blank">ver sitio</a>)</span></h2>
		<?php if($user): ?>
		<div class="identity">
			<a href="start/myaccount/form"><?=$user['email']?></a>
			<ul>
				<li><strong><?=$user['name']?></strong><br /><?=$user['role']?></li>
				<li><a href="start/myaccount/form">Mi cuenta</a></li>
				<li><a href="start/session/logout" class="logout">Salir</a></li>
			</ul>
		</div>
		<?php endif; ?>
	</div>
	<?php if($user AND $menu): ?>
	<ul id="menu" class="clearfix">
		<li><a href="start/overview/index">Inicio</a></li>
		<?php foreach($menu as $level1): ?>
		<?php if(count($level1['submenu'])): ?>
		<li><a href="<?=$level1['directory']?>/<?=$level1['submenu'][0]['directory']?>/index" class="<?=$level1['selected']?>"><?=$level1['name']?><?php if(count($level1['submenu'])): ?> <span></span><?php endif; ?></a>
			<ul>
				<?php foreach($level1['submenu'] as $level2): ?>
				<li><a href="<?=$level1['directory']?>/<?=$level2['directory']?>/index"><?=$level2['name']?></a><span><?=$level2['description']?></span></li>
				<?php endforeach; ?>
			</ul>
		</li>
		<?php endif; ?>
		<?php endforeach; ?>
	</ul>
	<div id="content" class="clearfix">
		<div id="content-title" class="clearfix">
			<h1><?=$title?></h1>
		</div>
		<?php if($errors): ?>
		<div class="message error">
			Ocurrieron los siguientes errores:
			<ul>
				<?php foreach($errors as $error): ?>
				<li><?=$error?></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<?php endif; ?>
	<?php endif; ?>
		<?=$content?>
	<?php if($user AND $menu): ?>
	</div>
	<?php endif; ?>
</div>

<div id="modal-overlay"></div>
<div id="modal-confirm" data-action="" data-itemid="" data-status="">
	<div class="box">
		<div class="box-header clearfix">
			<h2>Atención</h2>
			<a href="#" class="close">Cerrar</a>
		</div>
		<div class="box-body">
			<p>
				<span class="ellipsis">Está a punto de borrar <strong>"Item name"</strong></span>
				¿Está seguro que desea continuar?
			</p>
			<div class="buttons">
				<button type="button" class="button small accept">Aceptar</button>
				<button type="button" class="button small cancel">Cancelar</button>
			</div>
			<label class="option"><input type="checkbox" name="delete" value="1" checked="checked" />Seguir mostrando esta alerta</label>
		</div>
	</div>
</div>
<div id="tooltip"></div>

</body>
</html>