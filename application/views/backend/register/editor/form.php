
		<form name="save" action="" method="post" class="form validate" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?=$data['id']?>" />
            <input type="hidden" name="parent_id" value="<?=$data['parent_id']?>" />
            <input type="hidden" name="site_id" value="<?=$data['site_id']?>" /> <!-- Id Site CMM -->
			<input type="hidden" name="type_id" value="2" />
			<div class="sidebar">
				<div class="sidebox">
				<!--h2>Estatus</h2>
					<div class="field">
						<label class="option"><input type="radio" name="status" value="N"<?php if($data['borrado']=='N'): ?> checked="checked"<?php endif; ?> /> Activo</label>
						<label class="option"><input type="radio" name="status" value="S"<?php if($data['borrado']=='S'): ?> checked="checked"<?php endif; ?> /> Inactivo</label>
					</div-->     
				<?php if(!$exist_admin):?>
					<h2>Subir de rol</h2>
					<div class="field">
						<label class="option"><input type="checkbox" name="id_nivel" id="change_rol" value="4"<?php if($data['id_nivel']=='4'): ?> checked="checked"<?php endif; ?> />Administrador</label>
					</div>               				
				<?php endif;?>
					<h2>Documentos</h2>
					<div class="field">
						<?php if($docs){ $i=0; ?>
							<?php foreach($docs as $doc){ ?>
									<div class="file">
										<?php if($doc['file_type']==1){ ?>
											Identificación oficial<br />
										<?php }else{ ?>
											RFC<br />
										<?php } ?>
										<?php
											$x = explode('-', $doc['file']);
											$s = array_shift($x);
											$f = implode('-', $x);
										?>
										<input type="text" name="bussiness_id_<?=$i?>" id="pdf_upload_<?=$i?>" value="<?=$f?>" class="" title="Selecccione el archivo" readonly="readonly" />
										<!--input type="file" id="picture_upload_<?=$i?>" name="picture_upload" class="uploadify_pdf" /-->
										<!--input type="file" name="picture_upload[]" id="picture_upload_<?=$i?>" style="text-indent: -9999px; height: 27px; line-height: 27px; width: 101px; background-image: url(../assets/images/backend/browse.png); float: right;"/-->
										<div><a target="_blank" href="../assets/files/editor_docs/<?=$data['id']?>/<?=$doc['file']?>">Ver archivo</a></div>
									</div><br /><br />
							<?php $i++; } ?>
						<?php }else{ ?>
							<div class="file">
								Identificación oficial<br />
								<input type="text" name="bussiness_id" id="pdf_upload" value="" class="" title="Selecccione el archivo" readonly="readonly" />
								<!--input type="file" id="picture_upload_1" name="picture_upload" class="uploadify_pdf" /-->
								<!--input type="file" name="picture_upload" id="picture_upload_1" style="text-indent: -9999px; height: 27px; line-height: 27px; width: 101px; background-image: url(../assets/images/backend/browse.png); float: right;"/-->
							</div><br /><br />
						<?php } ?>
					</div>  
				<div class="field">
					<label>Tel&eacute;fono</label>
					<input type="text" name="telefono" value="<?=$data['telefono']?>" readonly title="Escriba el telefono" />
				</div>   					
				</div>
			</div>
			
			
			<div class="fieldset">
				<h2>Información general</h2>               
				<div class="field full">
					<label>Nombre completo</label>
					<input type="text" name="fullname" value="<?=$data['nombre']?>" readonly title="Escriba el nombre completo" />
				</div>   
				<div class="field full">
					<label>Correo electrónico </label>
					<input type="text" name="correo" value="<?=$data['correo']?>" readonly title="Escriba el correo" />
				</div>   
				<div class="field full">
					<label>Rol</label>
					<input type="text" name="nivel" value="<?=$data['nivel']?>" readonly title="Escriba el nivel" />
				</div>   
				<div class="field full">
					<label>Proveedor</label>
					<input type="text" name="proveedor" value="<?=$data['proveedor']?>" readonly title="Escriba el proveedor" />
				</div>   
                <div style="height: 400px; width: 600px;">
                </div>			             
                <br />
				
				
				
				<div class="buttons">
					<button type="submit" class="button">Guardar</button>
					<button type="button" class="button cancel">Cancelar</button>
					<?php if($data['id'] AND $action_delete): ?><button type="button" class="button delete">Eliminar</button><?php endif; ?>
				</div>                
			</div>
		</form>
<script>

	$(document).ready(function(){

		$("#change_rol").click( function(){
			if(!$("#change_rol").is(":checked"))
				$("input[name=nivel]").val("Editor");
			else if($("#change_rol").is(":checked"))
				$("input[name=nivel]").val("Administrador");
		});
		
		$('#picture_upload').change(function(){
				var str=$("#picture_upload").val();
				var datos = str.slice(12);
				$("#pdf_upload").val(datos);
			});
		
		$('#picture_upload_0').change(function(){
				var str=$("#picture_upload_0").val();
				var datos = str.slice(12);
				$("#pdf_upload_0").val(datos);
			});
			
		$('#picture_upload_1').change(function(){
				var str=$("#picture_upload_1").val();
				var datos = str.slice(12);
				$("#pdf_upload_1").val(datos);
			});
	});
</script>