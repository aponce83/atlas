<div class="box">
	<div class="box-header clearfix">
		<h2>Estadísticas de LIBROSMÉXICO.MX</h2>
	</div>
	<div class="box-body">
		<h1>Todos los paneles se actualizan cada 24 horas automáticamente.</h1>
			<div class="bloque_grafica">
				<h2>Usuarios</h2>
				<p>Cantidad de usuarios nuevos registrados en el sitio</p>
				<div id="chart_amount_of_users_by_date" style="min-width: 310px; height: 400px; margin: 0 auto">
				</div>
			</div>
      <div class="bloque_grafica">
        <h2>Libros</h2>
        <p>Los libros más populares en el sitio por visitas</p>
        <table class="table">
          <tr>
            <th>Id</th>
            <th>Total</th>
          </tr>
          <?php
          $i = 0;
          foreach($data['last_popular_books'] as $book) {
          ?>
          <tr>
            <td><?php echo ++$i; ?></td>
            <td><a href="<?php echo URL::base().'libros/'.$book['book_id']; ?>" target="_blank"><?php echo $book['book_id']; ?></a></td>
            <td><?php echo $book['total']; ?></td>
          </tr>
          <?php
          }
          ?>
        </table>
      </div>
			<div class="bloque_grafica">
				
					<h2>Usuarios LIBROSMÉXICO.MX por Mes</h2>
					<p>Este tablero compara el número de usuarios únicos por mes que entran en LIBROSMÉXICO.MX.</p>
					<p>Se contabilizan los últimos 28 días naturales ya que es un número múltiplo de 7 y así podemos comparar cada día de la semana con su par del mes pasado.</p>
				<iframe width="736" height="491" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1bg_kLjaTQsySM-gjzGpHsO4kYxU--Reg33ga0t7_ICc/pubchart?oid=1075431834&amp;format=interactive"></iframe>
			</div>
			<div class="bloque_grafica">	
					<h2>Páginas vistas en LIBROSMÉXICO.MX por Mes</h2>
					<p>Se refiere a cuántas páginas ven los usuarios en un día. Si hay 1 usuario que ve una página, y otro que ve 10, el total de páginas vistas serían 11.</p>
				<iframe width="731" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1bg_kLjaTQsySM-gjzGpHsO4kYxU--Reg33ga0t7_ICc/pubchart?oid=1546971358&amp;format=interactive"></iframe>
			</div>
			<div class="bloque_grafica">
					<h2>Top 10 palabras claves de los usuarios en el buscador interno de LIBROSMÉXICO.MX</h2>
					<p>Son los términos que escriben los usuarios en el buscador. Si encuentran números, posiblemente sean ISBN. Se cuentan los últimos 30 días.</p>
				<iframe width="800" height="415" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1bg_kLjaTQsySM-gjzGpHsO4kYxU--Reg33ga0t7_ICc/pubchart?oid=100453223&amp;format=interactive"></iframe>
			</div>
			<div class="bloque_grafica">	
				<h2>Libros más consultados en LIBROSMÉXICO.MX</h2>
					<p>Son los libros que más han visitado los usuarios. Son usuarios únicos. Se cuentan los últimos 30 días.</p>
				<iframe width="791" height="423" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1bg_kLjaTQsySM-gjzGpHsO4kYxU--Reg33ga0t7_ICc/pubchart?oid=1028127907&amp;format=interactive"></iframe>
				</div>
			<div class="bloque_grafica">
				<h2>Sesiones de usuarios registrados en LIBROSMÉXICO.MX</h2>
				<p>Cuenta las veces que se loguean los usuarios. Da igual si es por Facebook o por otro método. Si un usuario se loguea dos veces, se cuentan ambas.</p>
				<iframe width="783" height="370" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1bg_kLjaTQsySM-gjzGpHsO4kYxU--Reg33ga0t7_ICc/pubchart?oid=1385307323&amp;format=interactive"></iframe>
			</div>
			<div class="bloque_grafica">
				<h2>Sesiones login vs no login en LIBROSMÉXICO.MX</h2>
					<p>Se comparan las sesiones en los que los usuarios hacen login contra los que no se loguean. Se cuentan los últimos 30 días.</p>
				<iframe width="600" height="371" seamless frameborder="0" scrolling="no" src="https://docs.google.com/spreadsheets/d/1bg_kLjaTQsySM-gjzGpHsO4kYxU--Reg33ga0t7_ICc/pubchart?oid=986018538&amp;format=interactive"></iframe>
			</div>
		
	</div>
</div>

<script src="//code.highcharts.com/highcharts.js"></script>
<script src="//code.highcharts.com/modules/exporting.js"></script>
<script>
	var amount_of_users_by_date = <?php echo(json_encode($data['amount_of_users_by_date'],true)); ?>;
	$(function () {
		var yData = [];
		var xData = [];
		$.each(amount_of_users_by_date, function(i, e) {
			xData[i] = e.dc;
			yData[i] = parseInt(e.c);
		});
    $('#chart_amount_of_users_by_date').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Nuevos usuarios en los últimos 30 días'
        },
        xAxis: {
            categories: xData,
            crosshair: true,
            title: {
            	text: 'Días'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Cantidad'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
          data: yData
        }]
    });
});
</script>