<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Usuarios en espera de aprobación</h4>
      </div>
      <div class="panel-body">
        <?php
        if (count($data['api_users']) > 0 ) {
        ?>
        <p>A continuación se presentan los usuarios pendientes por aprobar</p>
        <?php
        }
        ?>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <?php
          foreach ($data['api_users'] as $api_user) {
            $heading_id = 'heading_'.$api_user['id'];
            $collapse_id = 'collapse_'.$api_user['id'];
          ?>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="<?php echo $heading_id; ?>">
              <h4 class="panel-title">
                <a data-toggle="collapse" 
                  data-parent="#accordion" 
                  href="#<?php echo $collapse_id; ?>" 
                  aria-expanded="true" 
                  aria-controls="<?php echo $collapse_id; ?>">
                  <?php echo $api_user['fullname']; ?>
                </a>
              </h4>
            </div>
            <div 
              id="<?php echo $collapse_id; ?>" 
              class="panel-collapse collapse in" 
              role="tabpanel" 
              aria-labelledby="<?php echo $heading_id; ?>">
              <div class="panel-body">
                <h5>Datos del usuario</h5>
                <table class="table table-striped">
                  <tr>
                    <th>Mail</th>
                    <td><?php echo $api_user['email']; ?></td>
                  </tr>
                  <tr>
                    <th>Aplicaciones a desarrollar</th>
                    <td>
                      <?php echo $api_user['description']; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Registrado el</th>
                    <td><?php echo $api_user['created_on']; ?></td>
                  </tr>
                </table>
                <hr>
                <h5>Operaciones</h5>
                <ul class="list-unstyled">
                  <li>
                    <button 
                      type="button" 
                      class="btn btn-warning"
                      onclick="show_modal_api_user(<?php echo $api_user['id']; ?>, '<?php echo $api_user['fullname']; ?>');">
                      Aprobar
                    </button>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <?php
          }
          if (count($data['api_users']) == 0) {
          ?>
          <p class="text-muted">Por el momento no hay usuarios en espera de aprobación</p>
          <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <form action="admin/api/approval" method="post">
      <input 
        type="hidden" 
        id="approval-api_user_id" 
        name="api_user_id" 
        value="">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title" id="myModalLabel">Aprobar al usuario</h4>
        </div>
        <div class="modal-body">
          ¿Deseas aprobar al usuario: <strong><span id="approval-api_user_fullname"></span></strong>?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          <button type="submit" class="btn btn-success">Si</button>
        </div>
      </div>
    </form>
  </div>
</div>
<script>
  /**
   * Muestra la modal
   */
  function show_modal_api_user(api_user_id, api_user_fullname) {
    $('#approval-api_user_id').val(api_user_id);
    $('#approval-api_user_fullname').text(api_user_fullname);
    $('#myModal').modal('show');
  }
</script>

