<!-- 
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Cantidad de usuarios registrados en los últimos días</h4>
      </div>
      <div class="panel-body">
        <div id="chart_amount_of_users_by_date" style="min-width: 310px; height: 400px; margin: 0 auto">
        </div>
      </div>
    </div>
  </div>
</div>
 -->
<link href="assets/modernui-framework/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/>
<script src="assets/modernui-framework/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<?php
  $today = new DateTime();
  $to_string = $today->format('Y-m-d');
  $interval = new DateInterval('P1M');
  $from_string = $today->sub($interval)->format('Y-m-d');

  $today = new DateTime();
  $interval_1w = new DateInterval('P1W');
  $from_1w = $today->sub($interval_1w)->format('Y-m-d');
?>
<!-- Glances -->
<div class="row">
  <div class="col-lg-3 col-md-6">
    <div class="panel info-box panel-white">
      <div class="panel-body">
        <div class="info-box-stats">
          <p class="counter" id="users_get_platform_metrics"></p>
          <span class="info-box-title">Total de usuarios registrados</span>
        </div>
        <div class="info-box-icon">
          <i class="icon-users"></i>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6">
    <div class="panel info-box panel-white">
      <div class="panel-body">
        <div class="info-box-stats">
          <p class="counter" id="discussions_get_platform_metrics"></p>
          <span class="info-box-title">Total de discusiones creadas</span>
        </div>
        <div class="info-box-icon">
          <i class="icon-bubbles"></i>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6">
    <div class="panel info-box panel-white">
      <div class="panel-body">
        <div class="info-box-stats">
          <p class="counter" id="trivias_get_platform_metrics"></p>
          <span class="info-box-title">Total de trivias contestadas</span>
        </div>
        <div class="info-box-icon">
          <i class="icon-question"></i>
          </div>
        </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-6">
    <div class="panel info-box panel-white">
      <div class="panel-body">
        <div class="info-box-stats">
          <p class="counter" id="lists_get_platform_metrics"></p>
          <span class="info-box-title">Listas creadas por los usuarios</span>
        </div>
        <div class="info-box-icon">
          <i class="icon-list"></i>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Cantidad de usuarios registrados -->
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Usuarios registrados en un intervalo de tiempo</h4>
      </div>
      <div class="panel-body">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-1 control-label">De</label>
            <div class="col-sm-2">
              <input 
                type="text" 
                class="form-control date-picker" 
                id="from_amount_of_users_by_date"
                value="<?php echo $from_string; ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1 control-label">Al</label>
            <div class="col-sm-2">
              <input 
                type="text" 
                class="form-control date-picker" 
                id="to_amount_of_users_by_date"
                value="<?php echo $to_string; ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1 control-label"></label>
            <div class="col-sm-2">
              <button 
                type="button" 
                class="btn btn-primary" 
                onclick="update_amount_of_users_by_date()">
                Consultar
              </button>
            </div>
          </div>
        </div>
        <div id="chart_amount_of_users_by_date" style="min-width: 310px; height: 400px; margin: 0 auto">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Búsquedas realizadas -->
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Búsquedas realizadas</h4>
      </div>
      <div class="panel-body">
        <div class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-1 control-label">De</label>
            <div class="col-sm-2">
              <input 
                type="text" 
                class="form-control date-picker" 
                id="from_search_tracking_by_date"
                value="<?php echo $from_string; ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1 control-label">Al</label>
            <div class="col-sm-2">
              <input 
                type="text" 
                class="form-control date-picker" 
                id="to_search_tracking_by_date"
                value="<?php echo $to_string; ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1 control-label">Tipo</label>
            <div class="col-sm-2">
              <select class="form-control" id="par_search_tracking">
                <option value="">Sin filtro</option>
                <option value="cadena">Todo</option>
                <option value="autores">Autores</option>
                <option value="titulo">Titulo</option>
                <option value="isbn">Isbn</option>
                <option value="keywords">Palabras clave</option>
                <option value="editorial">Editorial</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1 control-label">Ubicación</label>
            <div class="col-sm-2">
              <select class="form-control" id="location_search_tracking">
                <option value="">Sin filtro</option>
                <option value="header">Encabezado</option>
                <option value="search">Campo de búsqueda</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1 control-label"></label>
            <div class="col-sm-2">
              <button 
                type="button" 
                class="btn btn-primary" 
                onclick="update_search_tracking()">
                Consultar
              </button>
            </div>
          </div>
        </div>
        <div id="table_search_tracking"></div>
      </div>
    </div>
  </div>
</div>

<script src="assets/scripts/highcharts/highcharts.js"></script>
<script src="assets/scripts/highcharts/modules/exporting.js"></script>
<script src="assets/scripts/highcharts/modules/canvas-tools.js"></script>
<script src="assets/scripts/export-csv/export-csv.js"></script>
<!-- <script src="assets/scripts/jspdf/jspdf.min.js"></script> -->

<script src="assets/scripts/highcharts/highcharts-export-clientside.js"></script>
<script>
  
  function create_table_rows(temp_table_id, columns, data) {
    for (var i = 0 ; i < data.length ; i++) {
      var row$ = $('<tr/>');
      for (var colIndex = 0 ; colIndex < columns.length ; colIndex++) {
        var cellValue = data[i][columns[colIndex]];
        if (cellValue == null) { 
          cellValue = ""; 
        }
        row$.append($('<td/>').html(cellValue));
      }
      $(temp_table_id).append(row$);
    }
  }

  function create_table_headers(temp_table_id, data) {
    var columnSet = [];
    var thead$ = $('<thead/>');
    var headerTr$ = $('<tr/>');
 
    for (var i = 0 ; i < data.length ; i++) {
      var rowHash = data[i];
      for (var key in rowHash) {
        if ($.inArray(key, columnSet) == -1){
          columnSet.push(key);
          headerTr$.append($('<th/>').html(key));
        }
      }
    }
    thead$.append(headerTr$);
    $(temp_table_id).append(thead$);
    return columnSet;
  }

  function create_table_given_data(id, data) {

    $(id).empty();
    if (data.length == 0) {
      $(id).html('<p>No encontré resultados :(</p>');
      return;
    }
    $(temp_table_id).DataTable().destroy();

    var temp_table_id = id.replace("#", "") + '_table_temp';
    $(id).append('<table id="'+ temp_table_id +'" class="display table"></table>');
    temp_table_id = "#" + temp_table_id;
    var columns = create_table_headers(temp_table_id, data);
    create_table_rows(temp_table_id, columns, data);
    $(temp_table_id).DataTable({
      buttons: [
        {
          extend: 'csv',
          text: 'Copy all data',
          exportOptions: {
            modifier: {
              search: 'none'
            }
          }
        }
      ]
    });
  }

  function numberWithCommas(n) {
    var parts=n.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
  }

  function update_amount_of_users_by_date() {
    var from = $('#from_amount_of_users_by_date').val();
    var to = $('#to_amount_of_users_by_date').val();
    $.ajax({
      url: "<?php echo URL::base(); ?>admin/ajax/get_amount_of_users_by_date",
      type: "get",
      data: { from: from,
      to: to },
      dataType: "json",
      success: function (data) {
        $('#chart_amount_of_users_by_date').empty();
        var yData = [];
        var xData = [];
        $.each(data.res, function(i, e) {
          xData[i] = e.dc;
          yData[i] = parseInt(e.c);
        });
        $('#chart_amount_of_users_by_date').highcharts({
          chart: {
              type: 'column'
          },
          title: {
              text: 'Usuarios registrados ('+from + ' al ' + to +')'
          },
          xAxis: {
              categories: xData,
              crosshair: true,
              title: {
                text: 'Días'
              }
          },
          yAxis: {
              min: 0,
              title: {
                  text: 'Cantidad'
              }
          },
          tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y}</b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              column: {
                  pointPadding: 0.2,
                  borderWidth: 0
              }
          },
          series: [{
            data: yData,
            name: 'Usuarios registrados'
          }]
        });
      }
    });
  }

  function update_platform_metrics() {
    $.ajax({
      url: "<?php echo URL::base(); ?>admin/ajax/get_platform_metrics",
      type: "get",
      data: {},
      dataType: "json",
      success: function (data) {
        // usuarios registrados
        var amount_of_users = data.res.amount_of_users;
        var f_amount_of_users = numberWithCommas(amount_of_users);
        $('#users_get_platform_metrics').html(f_amount_of_users);

        // discusiones almacenadas
        var amount_of_discussions = data.res.amount_of_discussions;
        var f_amount_of_discussions = numberWithCommas(amount_of_discussions);
        $('#discussions_get_platform_metrics').html(f_amount_of_discussions);

        // trivias contestadas
        var amount_of_trivias = data.res.amount_of_trivias;
        var f_amount_of_trivias = numberWithCommas(amount_of_trivias);
        $('#trivias_get_platform_metrics').html(f_amount_of_trivias);

        // listas creadas por los usuarios
        var amount_of_lists = data.res.amount_of_lists;
        var f_amount_of_lists = numberWithCommas(amount_of_lists);
        $('#lists_get_platform_metrics').html(f_amount_of_lists);
      }
    });
  }

  function update_search_tracking() {
    var from = $('#from_search_tracking_by_date').val();
    var to = $('#to_search_tracking_by_date').val();
    var par = $('#par_search_tracking').val();
    var location = $('#location_search_tracking').val();
    var search_params = { from: from,
      to: to};
    if (par) {
      search_params.par = par;
    }
    if (location) {
      search_params.location = location;
    }
    $.ajax({
      url: "<?php echo URL::base(); ?>admin/ajax/get_search_info_in_groups",
      type: "get",
      data: search_params,
      dataType: "json",
      success: function (data) {
        $('#table_search_tracking').empty();
        create_table_given_data('#table_search_tracking', data.res);
      }
    });
  }

  $(function () {
    $.each($('.date-picker'), function(k, v) {
      var current = $(v).val();
      $(v).datepicker({
        orientation: "top auto",
        autoclose: true,
        format: 'yyyy-mm-dd'
      });
    });
    update_amount_of_users_by_date();
    update_platform_metrics();
    update_search_tracking();
    // setInterval(update_platform_metrics, 10000);
  });
</script>
