<!DOCTYPE html>
<html>
  <head>
    <title>Libros México | Inicio</title>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta charset="UTF-8">
    <meta name="description" content="Admin Dashboard Template" />
    <meta name="keywords" content="admin,dashboard" />
    <meta name="author" content="Steelcoders" />

    <base href="<?php echo URL::base(); ?>">
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href="assets/modernui-framework/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
    <link href="assets/modernui-framework/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
    <link href="assets/modernui-framework/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/modernui-framework/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="assets/modernui-framework/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/> 
    <link href="assets/modernui-framework/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>  
    <link href="assets/modernui-framework/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>  
    <link href="assets/modernui-framework/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/modernui-framework/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/> 
    
    <!-- Theme Styles -->
    <link href="assets/modernui-framework/css/modern.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/modernui-framework/css/themes/green.css" class="theme-color" rel="stylesheet" type="text/css"/>
    <link href="assets/modernui-framework/css/custom.css" rel="stylesheet" type="text/css"/>
    
    <script src="assets/modernui-framework/plugins/3d-bold-navigation/js/modernizr.js"></script>
    <script src="assets/modernui-framework/plugins/offcanvasmenueffects/js/snap.svg-min.js"></script>
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="page-login">
    <main class="page-content">
      <div class="page-inner">
        <div id="main-wrapper">
          <div class="row">
            <div class="col-md-3 center">
              <div class="login-box">
                <a href="index.html" class="logo-name text-lg text-center">Libros México</a>
                <p class="text-center m-t-md">Por favor coloca tu cuenta</p>
                <?php 
                if ($data['error']) {
                ?>
                <p class="text-center m-t-md">Hubo un error con tu cuenta</p>
                <?php
                }
                ?>
                <form class="m-t-md" action="admin/do_login" method="POST">
                  <div class="form-group">
                    <input type="email" name="username" class="form-control" placeholder="Correo electrónico" required>
                  </div>
                  <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Contraseña" required>
                  </div>
                  <button type="submit" class="btn btn-success btn-block">Iniciar sesión</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    <!-- Javascripts -->
    <script src="assets/modernui-framework/plugins/jquery/jquery-2.1.4.min.js"></script>
    <script src="assets/modernui-framework/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="assets/modernui-framework/plugins/pace-master/pace.min.js"></script>
    <script src="assets/modernui-framework/plugins/jquery-blockui/jquery.blockui.js"></script>
    <script src="assets/modernui-framework/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/modernui-framework/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="assets/modernui-framework/plugins/switchery/switchery.min.js"></script>
    <script src="assets/modernui-framework/plugins/uniform/jquery.uniform.min.js"></script>
    <script src="assets/modernui-framework/plugins/offcanvasmenueffects/js/classie.js"></script>
    <script src="assets/modernui-framework/plugins/waves/waves.min.js"></script>
    <script src="assets/modernui-framework/js/modern.min.js"></script>
  </body>
</html>