<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Trofeos que se pueden obtener</h4>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table id="games-awards-editable" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
              <?php
              $dont_show = $data['dont_show'];
              $dont_edit = $data['dont_edit'];
              foreach ($data['awards'][0] as $key => $value) {
                $edit_title = $key;
                $edit_title = str_replace('_', ' ', $edit_title);
                $edit_title = ucfirst($edit_title);
                $can_edit = !in_array($key, $dont_edit);
                if (in_array($key, $dont_show)) {
                  continue;
                }
              ?>
              <th>
                <?php echo $edit_title ?>
              </th>
              <?php
              }
              ?>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <?php
              $dont_show = $data['dont_show'];
              $dont_edit = $data['dont_edit'];
              foreach ($data['awards'][0] as $key => $value) {
                $edit_title = $key;
                $edit_title = str_replace('_', ' ', $edit_title);
                $edit_title = ucfirst($edit_title);
                $can_edit = !in_array($key, $dont_edit);
                if (in_array($key, $dont_show)) {
                  continue;
                }
              ?>
              <th>
                <?php echo $edit_title ?>
              </th>
              <?php
              }
              ?>
            </tr>
          </tfoot>
          <tbody>
            <?php
              foreach ($data['awards'] as $answer) {
                $pk = $answer['id'];
            ?>
              <tr>
                <?php
                foreach ($answer as $key => $value) {
                  $edit_title = $key;
                  $edit_title = str_replace('_', ' ', $edit_title);
                  $edit_title = ucfirst($edit_title);
                  $can_edit = !in_array($key, $dont_edit);
                  if (in_array($key, $dont_show)) {
                    continue;
                  }
                ?>
                <td>
                  <?php 
                  if ($can_edit) {
                  ?>
                  <a 
                    href="javascript:void(0);"
                    data-type="text" 
                    data-pk="<?php echo $pk ?>" 
                    data-url="<?php echo URL::base(); ?>admin/games/awards/update" 
                    data-title="<?php echo $edit_title ?>"
                    data-name="<?php echo $key; ?>">
                  <?php
                  }
                  ?>
                    <?php echo $value ?>
                  <?php 
                  if ($can_edit) {
                  ?>
                  </a>
                  <?php
                  }
                  ?>
                </td>
                <?php
                }
                ?>
              </tr>
            <?php
              }
            ?>
          </tbody>
          </table>  
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function() {
  //editables 
  $('#games-awards-editable td a').editable();
  $('#games-awards-editable').DataTable();
});
</script>