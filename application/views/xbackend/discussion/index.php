<div class="profile-timeline">
  <ul class="list-unstyled">
    <?php
      foreach ($data['discussions'] as $discussion) {
    ?>
    <li class="timeline-item" id="#<?php echo $discussion['id'];?>">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-white">
            <div class="panel-body">
              <div class="timeline-item-header">
                <img src="<?php echo $discussion['image']; ?>" alt="">
                <p><?php echo $discussion['fullname']; ?></p>
                <small><?php echo $discussion['time_string']; ?></small>
                <small>
                  <a href="<?php echo $discussion['discussion_url']; ?>" target="_blank"><?php echo $discussion['discussion_url']; ?></a>
                </small>
                <small>
                  (<?php echo $discussion['resource_type'];?>)
                </small>
              </div>
              <div class="timeline-item-post">
                <p>
                  <?php echo $discussion['description']; ?>
                </p>
                <div class="timeline-options">
                  <?php
                  if ($discussion['deleted'] == 0) {
                  ?>
                  <a href="admin/discussion/<?php echo $discussion['id']?>/delete"><i class="icon-trash"></i>Borrar</a>
                  <?php
                  } else {
                  ?>
                  Mensaje borrado
                  <?php
                  }
                  ?>
                </div>
                <?php
                foreach ($discussion['comments'] as $comment) {
                ?>
                <div class="timeline-comment">
                  <div class="timeline-comment-header">
                      <img src="<?php echo $comment['image']; ?>" alt="">
                      <p><?php echo $comment['fullname']; ?> <small><?php echo $comment['time_string']; ?></small></p>
                  </div>
                  <p class="timeline-comment-text"><?php echo $comment['comment']; ?></p>
                  <?php
                  if ($comment['deleted'] == 0) {
                  ?>
                  <a href="admin/comment/<?php echo $comment['id']?>/delete"><i class="icon-trash"></i>Borrar</a>
                  <?php
                  } else {
                  ?>
                  Mensaje borrado
                  <?php
                  }
                  ?>
                </div>
                <?php
                  foreach ($comment['comments_comments'] as $c) {
                ?>
                <div class="timeline-comment" style="padding-left: 50px;">
                  <div class="timeline-comment-header">
                      <img src="<?php echo $c['image']; ?>" alt="">
                      <p><?php echo $c['fullname']; ?> <small><?php echo $c['time_string']; ?></small></p>
                  </div>
                  <p class="timeline-comment-text"><?php echo $c['comment']; ?></p>
                  <?php
                  if ($c['deleted'] == 0) {
                  ?>
                  <a href="admin/comment/<?php echo $c['id']?>/delete"><i class="icon-trash"></i>Borrar</a>
                  <?php
                  } else {
                  ?>
                  Mensaje borrado
                  <?php
                  }
                  ?>
                </div>
                <?php
                  }
                  ?>
                <?php
                }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </li>
    <?php
      }
    ?>
  </ul>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Páginas en el menu</h4>
      </div>
      <div class="panel-body">
        <?php var_dump($data['discussions']); ?>
      </div>
    </div>
  </div>
</div>
