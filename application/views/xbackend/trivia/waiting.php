<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Trivias por aprobar</h4>
      </div>
      <div class="panel-body">
        <?php
        if (count($data['trivias']) > 0 ) {
        ?>
        <p>A continuación se presentan las trivias pendientes por aprobar</p>
        <?php
        }
        ?>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <?php
          foreach ($data['trivias'] as $trivia) {
            $heading_id = 'heading_'.$trivia['trivia_id'];
            $collapse_id = 'collapse_'.$trivia['trivia_id'];
          ?>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="<?php echo $heading_id; ?>">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $collapse_id; ?>" aria-expanded="true" aria-controls="<?php echo $collapse_id; ?>">
                  <?php echo $trivia['trivia_name']; ?>
                </a>
              </h4>
            </div>
            <div id="<?php echo $collapse_id; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="<?php echo $heading_id; ?>">
              <div class="panel-body">
                <h5>Datos de la trivia</h5>
                <table class="table table-striped">
                  <tr>
                    <th>Creada el</th>
                    <td><?php echo $trivia['created_on']; ?></td>
                  </tr>
                  <tr>
                    <th>Autor</th>
                    <td>
                      <a href="<?php echo $trivia['user']['url']; ?>" target="_blank">
                        <?php echo $trivia['user']['fullname']; ?>
                      </a> 
                      (<a href="mailto:<?php echo $trivia['user']['email']; ?>"><?php echo $trivia['user']['email']; ?></a>)
                    </td>
                  </tr>
                  <tr>
                    <th>Puntos</th>
                    <td>
                      <i class="fa fa-book text-danger"></i> <?php echo $trivia['trivia_score']; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Descripción</th>
                    <td>
                      <?php echo $trivia['trivia_description']; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Palabras clave</th>
                    <td>
                      <?php echo $trivia['keywords']; ?>
                    </td>
                  </tr>
                  <tr>
                    <th>Libros asociados</th>
                    <td>
                    <?php 
                      foreach ($trivia['books'] as $book) {
                        ?>
                        <a 
                          href="<?php echo URL::base(true).'libros/'.$book['id_libro'];?>" 
                          target="_blank"
                          style="margin-right: 10px;">
                          <img src="<?php echo $book['imagen']; ?>" width="100px">
                        </a>
                        <?php
                      }
                    ?>
                    </td>
                  </tr>
                </table>
                <hr>
                <h5>Preguntas</h5>
                <table class="table table-striped">
                  <?php
                  foreach($trivia['questions'] as $question) {

                  ?>
                  <tr>
                    <td>
                      <p><?php echo $question['question_label'];?></p>
                      <ul class="list-unstyled">
                        <?php 
                        foreach ($question['answers'] as $answer) {
                          $class_text = 'text-danger';
                          $icon = 'fa-close';
                          if ($answer['is_correct']) {
                            $class_text = 'text-success';
                            $icon = 'fa-check';
                          }
                        ?>
                        <li class="<?php echo $class_text; ?>">
                          <i class="fa <?php echo $icon; ?>"></i> <?php echo $answer['answer_label']; ?>
                        </li>
                        <?php
                        }
                        ?>
                      </ul>
                    </td>
                  </tr>
                  <?php
                  }
                  ?>
                </table>
                <hr>
                <h5>Operaciones</h5>
                <p class="text-danger lead">Una vez que apruebes la trivia, se mostrará en la página de trivias y no podrá ser modificada.</p>
                <ul class="list-unstyled">
                  <li>
                    <button 
                      type="button" 
                      class="btn btn-warning"
                      onclick="show_modal_trivia(<?php echo $trivia['trivia_id']; ?>, '<?php echo $trivia['trivia_name']; ?>');">
                      Aprobar
                    </button>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <?php
          }
          if (count($data['trivias']) == 0) {
          ?>
          <p class="text-muted">Por el momento no hay trivias en espera de aprobación</p>
          <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <form action="admin/trivia/approval" method="post">
      <input 
        type="hidden" 
        id="approval-trivia-id" 
        name="trivia_id" 
        value="">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title" id="myModalLabel">Aprobar trivia</h4>
        </div>
        <div class="modal-body">
          ¿Deseas aprobar la trivia: <strong><span id="approval-trivia-name"></span></strong>?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          <button type="submit" class="btn btn-success">Si</button>
        </div>
      </div>
    </form>
  </div>
</div>
<script>
  /**
   * Muestra la trivia
   * @param  {int} trivia_id id de la trivia
   */
  function show_modal_trivia(trivia_id, trivia_name) {
    $('#approval-trivia-id').val(trivia_id);
    $('#approval-trivia-name').text(trivia_name);
    $('#myModal').modal('show');
  }
</script>

