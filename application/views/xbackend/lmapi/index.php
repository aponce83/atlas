<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">LM API Log</h4>
      </div>
      <div class="panel-body">
        <!--/ Modal -->
        <div class="table-responsive">
          <table id="lmapis-editable" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
              <?php
              $dont_show = $data['dont_show'];
              $dont_edit = $data['dont_edit'];
              foreach ($data['lmapis'][0] as $key => $value) {
                $edit_title = $key;
                $edit_title = str_replace('_', ' ', $edit_title);
                $edit_title = ucfirst($edit_title);
                $can_edit = !in_array($key, $dont_edit) && $data['is_super'];
                if (in_array($key, $dont_show)) {
                  continue;
                }
              ?>
              <th>
                <?php echo $edit_title ?>
              </th>
              <?php
              }
              ?>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <?php
              $dont_show = $data['dont_show'];
              $dont_edit = $data['dont_edit'];
              foreach ($data['lmapis'][0] as $key => $value) {
                $edit_title = $key;
                $edit_title = str_replace('_', ' ', $edit_title);
                $edit_title = ucfirst($edit_title);
                $can_edit = !in_array($key, $dont_edit) && $data['is_super'];
                if (in_array($key, $dont_show)) {
                  continue;
                }
              ?>
              <th>
                <?php echo $edit_title ?>
              </th>
              <?php
              }
              ?>
            </tr>
          </tfoot>
          <tbody>
            <?php
              foreach ($data['lmapis'] as $log) {
                $pk = $log['id'];
            ?>
              <tr>
                <?php
                foreach ($log as $key => $value) {
                  $edit_title = $key;
                  $edit_title = str_replace('_', ' ', $edit_title);
                  $edit_title = ucfirst($edit_title);
                  $can_edit = !in_array($key, $dont_edit) && $data['is_super'];
                  if (in_array($key, $dont_show)) {
                    continue;
                  }
                  
                  $type = 'text';
                ?>
                <td>
                  <?php 
                  if ($can_edit) {
                  ?>
                  <a 
                    href="javascript:void(0);"
                    class="update_<?php echo $key; ?> update_<?php echo $type;?>"
                    data-type="<?php echo $type; ?>" 
                    data-pk="<?php echo $pk ?>" 
                    data-url="<?php echo URL::base(); ?>admin/lmapis/update" 
                    data-title="<?php echo $edit_title ?>"
                    data-name="<?php echo $key; ?>"
                    data-value="<?php echo $value; ?>">
                  <?php
                  }
                  ?>
                  <?php
                    echo $value;
                  ?>
                  <?php 
                  if ($can_edit) {
                  ?>
                  </a>
                  <?php
                  }
                  ?>
                </td>
                <?php
                }
                ?>
              </tr>
            <?php
              }
            ?>
          </tbody>
          </table>  
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function() {
  //editables 
  $('#lmapis-editable td a.update_text').editable();
  $('#lmapis-editable').DataTable({
    "order": [[ 0, "desc" ]],
  });

});
</script>