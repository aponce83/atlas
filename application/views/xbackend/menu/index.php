<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Páginas en el menu</h4>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table id="menu-editable" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Href</th>
              <th>Placement</th>
              <th>Target</th>
              <th>Icon</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Href</th>
              <th>Placement</th>
              <th>Target</th>
              <th>Icon</th>
            </tr>
          </tfoot>
          <tbody>
            <?php
              foreach ($data['menu'] as $menu) {
                $can_edit = $data['is_super'];
            ?>
              <tr>
                <td><?php echo $menu['id']; ?></td>
                <td>
                  <?php
                  if ($can_edit) {
                  ?>
                  <a 
                    href="javascript:void(0);"
                    data-type="text" 
                    data-pk="<?php echo $menu['id']; ?>" 
                    data-url="<?php echo URL::base(); ?>admin/menus/update" 
                    data-title="Nombre"
                    data-name="nombre">
                  <?php
                  }
                  ?>
                    <?php echo $menu['nombre']; ?>
                  <?php
                  if ($can_edit) {
                  ?>
                  </a>
                  <?php
                  }
                  ?>
                </td>
                <td>
                  <?php
                  if ($can_edit) {
                  ?>
                  <a 
                    href="javascript:void(0);"
                    href="javascript:void(0);"
                    data-type="text" 
                    data-pk="<?php echo $menu['id']; ?>" 
                    data-url="<?php echo URL::base(); ?>admin/menus/update" 
                    data-title="Href"
                    data-name="href">
                  <?php
                  }
                  ?>
                    <?php echo $menu['href']; ?>
                  <?php
                  if ($can_edit) {
                  ?>
                  </a>
                  <?php
                  }
                  ?>
                </td>
                <td>
                  <?php
                  if ($can_edit) {
                  ?>
                  <a 
                    href="javascript:void(0);"
                    href="javascript:void(0);"
                    data-type="text" 
                    data-pk="<?php echo $menu['id']; ?>" 
                    data-url="<?php echo URL::base(); ?>admin/menus/update" 
                    data-title="Placement"
                    data-name="placement">
                  <?php
                  }
                  ?>
                    <?php echo $menu['placement']; ?>
                  <?php
                  if ($can_edit) {
                  ?>
                  </a>
                  <?php
                  }
                  ?>
                </td>
                <td>
                  <?php
                  if ($can_edit) {
                  ?>
                  <a 
                    href="javascript:void(0);"
                    href="javascript:void(0);"
                    data-type="text" 
                    data-pk="<?php echo $menu['id']; ?>" 
                    data-url="<?php echo URL::base(); ?>admin/menus/update" 
                    data-title="Target"
                    data-name="target">
                  <?php
                  }
                  ?>
                    <?php echo $menu['target']; ?>
                  <?php
                  if ($can_edit) {
                  ?>
                  </a>
                  <?php
                  }
                  ?>
                </td>
                <td>
                  <?php
                  if ($can_edit) {
                  ?>
                  <a 
                    href="javascript:void(0);"
                    href="javascript:void(0);"
                    data-type="text" 
                    data-pk="<?php echo $menu['id']; ?>" 
                    data-url="<?php echo URL::base(); ?>admin/menus/update" 
                    data-title="Icon"
                    data-name="icon">
                  <?php
                  }
                  ?>
                    <?php echo $menu['icon']; ?>
                  <?php
                  if ($can_edit) {
                  ?>
                  </a>
                  <?php
                  }
                  ?>
                </td>
              </tr>
            <?php
              }
            ?>
          </tbody>
          </table>  
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function() {
  //editables 
  $('#menu-editable td a').editable();
  $('#menu-editable').DataTable();
});
</script>