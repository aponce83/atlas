<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Roles</h4>
      </div>
      <div class="panel-body">
        <?php
        if ($data['is_super']) {
        ?>
        <button type="button" class="btn btn-success m-b-sm" data-toggle="modal" data-target="#myModal">Agregar un nuevo rol</button>
        <!-- Modal -->
        <form 
          id="add-row-form" 
          action="admin/roles/add"
          method="post">
          <div 
            class="modal fade" 
            id="myModal" 
            tabindex="-1" 
            role="dialog" 
            aria-labelledby="myModalLabel" 
            aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button 
                    type="button" 
                    class="close" 
                    data-dismiss="modal" 
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel">Nuevo rol</h4>
                </div>
                <div class="modal-body">
                  <?php
                  $role_model = $data['role_model'];
                  foreach ($role_model as $attr) {
                  ?>
                  <div class="form-group">
                    <?php
                    if ($attr['type'] == 'text') {
                    ?>
                    <input 
                      type="text" 
                      id="<?php echo $attr['id'];?>-input"
                      name="<?php echo $attr['name'];?>"
                      class="form-control" 
                      placeholder="<?php echo $attr['placeholder'];?>"
                      <?php
                      if ($attr['required']) {
                      ?>
                      required
                      <?php
                      }
                      ?>>
                    <?php 
                    } 
                    ?>
                  </div>
                  <?php
                  }
                  ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" id="add-row" class="btn btn-success">Agregar</button>
                </div>
              </div>
            </div>
          </div>
        </form>
        <!--/ Modal -->
        <?php
        }
        ?>
        <div class="table-responsive">
          <table id="roles-fixed" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
              <?php
              $dont_show = $data['dont_show'];
              $dont_edit = $data['dont_edit'];
              foreach ($data['roles'][0] as $key => $value) {
                $edit_title = $key;
                $edit_title = str_replace('_', ' ', $edit_title);
                $edit_title = ucfirst($edit_title);
                $can_edit = !in_array($key, $dont_edit) && $data['is_super'];
                if (in_array($key, $dont_show)) {
                  continue;
                }
              ?>
              <th>
                <?php echo $edit_title ?>
              </th>
              <?php
              }
              ?>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <?php
              $dont_show = $data['dont_show'];
              $dont_edit = $data['dont_edit'];
              foreach ($data['roles'][0] as $key => $value) {
                $edit_title = $key;
                $edit_title = str_replace('_', ' ', $edit_title);
                $edit_title = ucfirst($edit_title);
                $can_edit = !in_array($key, $dont_edit) && $data['is_super'];
                if (in_array($key, $dont_show)) {
                  continue;
                }
              ?>
              <th>
                <?php echo $edit_title ?>
              </th>
              <?php
              }
              ?>
            </tr>
          </tfoot>
          <tbody>
            <?php
              foreach ($data['roles'] as $role) {
                $pk = $role['id'];
            ?>
              <tr>
                <?php
                foreach ($role as $key => $value) {
                  $edit_title = $key;
                  $edit_title = str_replace('_', ' ', $edit_title);
                  $edit_title = ucfirst($edit_title);
                  $can_edit = !in_array($key, $dont_edit) && $data['is_super'];
                  if (in_array($key, $dont_show)) {
                    continue;
                  }
                ?>
                <td>
                  <?php 
                  if ($can_edit) {
                  ?>
                  <a 
                    href="javascript:void(0);"
                    data-type="text" 
                    data-pk="<?php echo $pk ?>" 
                    data-url="<?php echo URL::base(); ?>admin/roles/update" 
                    data-title="<?php echo $edit_title ?>"
                    data-name="<?php echo $key; ?>">
                  <?php
                  }
                  ?>
                    <?php echo $value ?>
                  <?php 
                  if ($can_edit) {
                  ?>
                  </a>
                  <?php
                  }
                  ?>
                </td>
                <?php
                }
                ?>
              </tr>
            <?php
              }
            ?>
          </tbody>
          </table>  
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function() {
  //editables 
  $('#roles-fixed td a').editable();
  $('#roles-fixed').DataTable({
    "order": [[ 0, "asc" ]],
  });
});
</script>