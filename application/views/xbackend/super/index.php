<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Administradores</h4>
      </div>
      <div class="panel-body">
        <?php
        if ($data['is_super']) {
        ?>
        <button type="button" class="btn btn-success m-b-sm" data-toggle="modal" data-target="#myModal">Agregar un nuevo admin</button>
        <!-- Modal -->
        <form 
          id="add-row-form" 
          action="admin/supers/add"
          method="post">
          <div 
            class="modal fade" 
            id="myModal" 
            tabindex="-1" 
            role="dialog" 
            aria-labelledby="myModalLabel" 
            aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button 
                    type="button" 
                    class="close" 
                    data-dismiss="modal" 
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel">Nuevo admin</h4>
                </div>
                <div class="modal-body">
                  <?php
                  $admin_model = $data['admin_model'];
                  foreach ($admin_model as $attr) {
                  ?>
                  <div class="form-group">
                    <?php
                    if ($attr['type'] == 'text' || 
                      $attr['type'] == 'email' ||
                      $attr['type'] == 'password') {
                    ?>
                    <input 
                      type="<?php echo $attr['type']; ?>" 
                      id="<?php echo $attr['id'];?>-input"
                      name="<?php echo $attr['name'];?>"
                      class="form-control" 
                      placeholder="<?php echo $attr['placeholder'];?>"
                      <?php
                      if ($attr['required']) {
                      ?>
                      required
                      <?php
                      }
                      ?>>
                    <?php 
                    } else if ($attr['type'] == 'select') {
                    ?>
                    <select
                      id="<?php echo $attr['id'];?>-input"
                      name="<?php echo $attr['name'];?>"
                      class="form-control"
                      <?php
                      if ($attr['required']) {
                      ?>
                      required
                      <?php
                      }
                      ?>
                      >
                      <option value=""><?php echo $attr['prepend'];?>  </option>
                      <?php 
                      foreach ($attr['options'] as $option) {
                      ?>
                      <option
                        value="<?php echo $option['value']; ?>">
                        <?php echo $option['text'];?>
                      </option>
                      <?php
                      }
                      ?>
                    </select>
                    <?php
                    }
                    ?>
                  </div>
                  <?php
                  }
                  ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" id="add-row" class="btn btn-success">Agregar</button>
                </div>
              </div>
            </div>
          </div>
        </form>
        <?php
        }
        ?>
        <!--/ Modal -->
        <div class="table-responsive">
          <table id="supers-editable" class="display table" style="width: 100%; cellspacing: 0;">
          <thead>
            <tr>
              <?php
              $dont_show = $data['dont_show'];
              $dont_edit = $data['dont_edit'];
              foreach ($data['supers'][0] as $key => $value) {
                $edit_title = $key;
                $edit_title = str_replace('_', ' ', $edit_title);
                $edit_title = ucfirst($edit_title);
                $can_edit = !in_array($key, $dont_edit) && $data['is_super'];
                if (in_array($key, $dont_show)) {
                  continue;
                }
              ?>
              <th>
                <?php echo $edit_title ?>
              </th>
              <?php
              }
              ?>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <?php
              $dont_show = $data['dont_show'];
              $dont_edit = $data['dont_edit'];
              foreach ($data['supers'][0] as $key => $value) {
                $edit_title = $key;
                $edit_title = str_replace('_', ' ', $edit_title);
                $edit_title = ucfirst($edit_title);
                $can_edit = !in_array($key, $dont_edit) && $data['is_super'];
                if (in_array($key, $dont_show)) {
                  continue;
                }
              ?>
              <th>
                <?php echo $edit_title ?>
              </th>
              <?php
              }
              ?>
            </tr>
          </tfoot>
          <tbody>
            <?php
              foreach ($data['supers'] as $user) {
                $pk = $user['id'];
            ?>
              <tr>
                <?php
                foreach ($user as $key => $value) {
                  $edit_title = $key;
                  $edit_title = str_replace('_', ' ', $edit_title);
                  $edit_title = ucfirst($edit_title);
                  $can_edit = !in_array($key, $dont_edit) && $data['is_super'];
                  if (in_array($key, $dont_show)) {
                    continue;
                  }
                  
                  $type = 'text';
                  if ($key == 'date_created') {
                    $value = LM::unix_timestamp_to_readable($value);
                  }
                  if ($key == 'role_name') {
                    $type = 'select';
                    $read_value = $value;
                    foreach ($data['roles'] as $role) {
                      if ($role['text'] == $read_value) {
                        $value = $role['value'];
                      }
                    }
                  }
                ?>
                <td>
                  <?php 
                  if ($can_edit) {
                  ?>
                  <a 
                    href="javascript:void(0);"
                    class="update_<?php echo $key; ?> update_<?php echo $type;?>"
                    data-type="<?php echo $type; ?>" 
                    data-pk="<?php echo $pk ?>" 
                    data-url="<?php echo URL::base(); ?>admin/supers/update" 
                    data-title="<?php echo $edit_title ?>"
                    data-name="<?php echo $key; ?>"
                    data-value="<?php echo $value; ?>">
                  <?php
                  }
                  ?>
                  <?php
                  if ($key != 'role_name') {
                    echo $value;  
                  } else {
                    echo $read_value;
                  }
                  ?>
                  <?php 
                  if ($can_edit) {
                  ?>
                  </a>
                  <?php
                  }
                  ?>
                </td>
                <?php
                }
                ?>
              </tr>
            <?php
              }
            ?>
          </tbody>
          </table>  
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(function() {
  //editables 
  $('#supers-editable td a.update_text').editable();
  $('.update_role_name').editable({
    source: <?php echo (json_encode($data['roles'])); ?>,
  });
  $('#supers-editable').DataTable({
    "order": [[ 0, "desc" ]],
  });

});
</script>