<div style="min-height: 650px;">
  <div class="static-wraper">
    <div class="dos active">
      <h2>AYUDA</h2>
      <p>Con la finalidad de hacer más fluida la navegación para nuestros usuarios, brindamos las siguientes secciones de ayuda.</p>
      <div class="block_1 type_A" >
        <h3 class="open_article">
          <a name="pf">Preguntas frecuentes</a>
        </h3>
        <article class="hide_module">
          <p>
            <b>¿Qué es LIBROSMÉXICO.MX?</b>
          </p>
          <p>LIBROSMEXICO.MX es una plataforma digital para la difusión del libro y la lectura.</p><br>
          <p><b>¿Puedo leer libros en LIBROSMÉXICO.MX?</b></p>
          <p>En LIBROSMÉXICO.MX contamos con algunos títulos que podrás consultar en línea, así como la información para que puedas adquirir el libro.</p><br>
          <p><b>¿Para registrarme tengo que pagar?</b></p>
          <p>LIBROSMÉXICO.MX es una plataforma gratuita para todos los interesados.</p><br>
          <p><b>¿Puedo descargar documentos y libros patrimoniales de la historia de México sin ningún costo?</b></p>
          <p>Los documentos y libros patrimoniales de la historia de México no tienen ningún costo, por lo que son de libre lectura y descarga.</p><br>
          <p><b>¿Qué tipos de libros puedo encontrar?</b></p>
          <p>En LIBROSMÉXICO.MX podrás encontrar los siguientes tipos de libros: libro, audiolibro, libro electrónico, libro con CD, libro con CD-ROM, revistas, DVD, CD y CD-ROM.</p><br>
          <p><b>¿Puedo modificar información del catálogo?</b></p>
          <p>Si hay un error, puedes <a href="<?=Url::base().'contacto'?>">contactarnos</a>.</p><br>
          <p><b>¿Cómo puedo agregar libros al catálogo?</b></p>
          <p>Los profesionales de la lectura pueden agregar libros al catálogo en la página <a href="https://pro.librosmexico.mx/">pro.librosmexico.mx</a>.</p><br>
        </article>
      </div>
      <div class="block_1 type_A">
        <h3 class="open_article"><a name="tr">Tutoriales</a></h3>
        <article class="hide_module">
        <?php foreach($tutorials as $tutoriales): ?>
        <p><?php echo $tutoriales['title']?></p>
          <?php echo $tutoriales['content']?>
        <?php endforeach; ?>
        </article>
      </div>
    </div>    
  </div>        
</section>
