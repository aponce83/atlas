<link rel="stylesheet" type="text/css" href="assets/styles/custom_f.css"/>

<div class="top-users-wrp">
	<h1>
		tabla de posiciones
		<a class="ranking-list" href="mis-logros">Trofeos</a>
	</h1>
	<div class="users-select-row">
		<a class="users-select" href="tabla-posiciones/amigos">
			<div class="col-xs-6 <?php if($data['table_type']=='amigos') echo 'users-selected'; ?>">Mis amigos</div>
		</a>
		<a class="users-select" href="tabla-posiciones/general">
			<div class="col-xs-6 <?php if($data['table_type']=='general') echo 'users-selected'; ?>">General</div>
		</a>
	</div>
	
	<div class="col-xs-12 top-labels">
		<div class="lbl-1">Posición</div>
		<div class="lbl-2">Usuario</div>
		<div class="lbl-3">Biblos</div>	
	</div>




	<?php
	$enum_row = 1;
	foreach( $data['users_section1'] as $user_info )
	{
	?>

	<div class="col-xs-12 row-item <?php if($data['current_user_id']==$user_info['id']) echo 'active-top-row'; ?> " data-position="<?=$enum_row?>">
		<div class="medal">
			<?php 
			switch( $enum_row )
			{
				case 1: ?>
					<img src="assets/images/medalla-oro.svg">
					<?php 
					break;
				case 2: ?>
					<img src="assets/images/medalla-plata.svg">
					<?php 
					break;
				case 3: ?>
					<img src="assets/images/medalla-bronce.svg">
					<?php 
					break;
				default: ?>
					<img src="assets/images/medalla-normal.svg">
					<?php 
					break;
			}
			?>
			<label <?php if( $enum_row==1 || $enum_row==2 ||$enum_row==3 ) echo 'class="lbl-black"' ?> ><?=$enum_row?></label>
		</div>
		<?php
		if($data['current_user_id']==$user_info['id'])
		{
		?>
			<div class="thumb">
				<img src="<?=$user_info['image']?>">
			</div>
			<div class="usr-name">
				<?=$user_info['fullname']?>
			</div>
		<?php
		}
		else
		{
		?>
			<div class="thumb">
				<a href="lector/<?=base64_encode($user_info['id'])?>/circulo">
					<img src="<?=$user_info['image']?>">
				</a>
			</div>
			<div class="usr-name">
				<a href="lector/<?=base64_encode($user_info['id'])?>/circulo">
					<?=$user_info['fullname']?>
				</a>
			</div>
		<?php
		}
		?>
		

		<div class="score">
			<img src="assets/images/biblos.svg">
			<span><?=number_format($user_info['score'])?></span>
		</div>
	</div>
	
	<?php
	$enum_row++;
	}
	?>


	<?php 
	//si existe la sección 2 0 no existe la seccion 2 pero  hay más de 10 registros, entonces se muestran los puntos
	if( (isset($data['users_section2'])) || ( !isset($data['users_section2']) && $data['total_rows'] > 10 ) )
	{
		if( $data['start_section2'] > 11 )
		{
		?>
			<div class="col-xs-12 row-item middle-points">
				<img src="assets/images/puntossuspensivos.svg">
			</div>
		<?php
		}
	}
	?>




	<?php
	if( isset($data['users_section2']) )
	{
		$enum_row = $data['start_section2'];
		foreach( $data['users_section2'] as $user_info )
		{
		?>

		<div class="col-xs-12 row-item <?php if($data['current_user_id']==$user_info['id']) echo 'active-top-row'; ?> " data-position="<?=$enum_row?>">
			<div class="medal">
				<?php 
				switch( $enum_row )
				{
					case 1: ?>
						<img src="assets/images/medalla-oro.svg">
						<?php 
						break;
					case 2: ?>
						<img src="assets/images/medalla-plata.svg">
						<?php 
						break;
					case 3: ?>
						<img src="assets/images/medalla-bronce.svg">
						<?php 
						break;
					default: ?>
						<img src="assets/images/medalla-normal.svg">
						<?php 
						break;
				}
				?>
				<label><?=$enum_row?></label>
			</div>

			<?php
			if($data['current_user_id']==$user_info['id'])
			{
			?>
				<div class="thumb">
					<img src="<?=$user_info['image']?>">
				</div>
				<div class="usr-name">
					<?=$user_info['fullname']?>
				</div>
			<?php
			}
			else
			{
			?>
				<div class="thumb">
					<a href="lector/<?=base64_encode($user_info['id'])?>/circulo">
						<img src="<?=$user_info['image']?>">
					</a>
				</div>
				<div class="usr-name">
					<a href="lector/<?=base64_encode($user_info['id'])?>/circulo">
						<?=$user_info['fullname']?>
					</a>
				</div>
			<?php
			}
			?>
			
			<div class="score">
				<img src="assets/images/biblos.svg">
				<span><?=number_format($user_info['score'])?></span>
			</div>
		</div>
		
		<?php
		$enum_row++;
		}
	}
	?>
	

	<!-- <div class="col-xs-12 row-item">
		<div class="medal">
			<img src="assets/images/medalla-oro.svg">
			<label>1</label>
		</div>
		<div class="thumb">
			<img src="assets/images/profile.jpg">
		</div>
		<div class="usr-name">Luis Roberto Galván Guerra</div>
		<div class="score">
			<img src="assets/images/biblos.svg">
			<span>15,372</span>
		</div>
	</div> -->





	<div class="row clearfix"></div>
</div>





<!-- pantalla de notificacion de posicion -->
    <div id="notifPosWrp" class="notif-pos-wrp">
      <div class="notif-content">
        <div class="blue-bar top-bar">
          <a href="javascript: void(0);" class="not-close-lnk">X</a>
        </div>
        <p class="title-row1">¡felicidades!</p>
        <div class="main-img-container">
          <img id="medalImage" class="main-img" src="assets/images/medalla-normal.svg">
          <p id="posNumber" class="pos-number">5</p>
        </div>
        <p id="awardDesc" class="award-desc">
          <!-- award description -->Alcanzaste la <span id="textPos"></span>a posición en la tabla 
          <?php
          if( $data['current_table'] == 'amigos' )
          	echo 'de amigos';
          if( $data['current_table'] == 'general' )
          	echo 'general';
          ?>
        </p>
          <div class="col-xs-12 social-row">
            <div class="social-links">
              <p class="share-lbl">Comparte</p>
              <a id="adwardSharePosFb" class="sharePosFb" href="javascript: void(0);" data-name="" data-description="" data-image="">
                <img src="assets/images/facebook-notif.svg">
              </a>
              <a id="adwardSharePosTw" class="sharePosTw" href="javascript: void(0);" data-name="" data-description="">
                <img class="tw-link" src="assets/images/twitter-notif.svg">
              </a>
            </div>
          </div>
        <div class="blue-bar bottom-bar"></div>
      </div>
    </div>
    <!-- pantalla de notificacion de posicion -->



<script>
	$(document).ready(function(){

		$('.not-close-lnk').click(function(){
			$('#notifPosWrp').fadeOut(400, function(){
				$('#notifPosWrp').css('opacity', 0);
			});
			//se habilita scroll
			var $window = $(window);
			$window.disablescroll("undo");
		});


		//notificacion de mi posición en la tabla
		$('.active-top-row').click(function(){

			var userPosition = $(this).data('position');
			var imagePath = $(this).children('.medal').children('img').attr('src');
			console.log(imagePath);

			//colocar informacion necesaria con datas en los links para compartir en fb y tw
			$('#adwardSharePosFb').attr('data-name', '¡FELICIDADES!');

			<?php
          	if( $data['current_table'] == 'amigos' )
          		$aux_string = 'de amigos';
          	if( $data['current_table'] == 'general' )
          		$aux_string = 'general';
          	?>
			$('#adwardSharePosFb').attr('data-description', 'Alcanzaste la '+userPosition+'a posición en la tabla <?=$aux_string?>');
			$('#adwardSharePosFb').attr('data-image', $(this).children('.medal').children('img')[0].src );

			$('#adwardSharePosTw').attr('data-name', '¡FELICIDADES!, ');
			$('#adwardSharePosTw').attr('data-description', 'alcanzaste la '+userPosition+'a posición en la tabla <?=$aux_string?>');
			
			
			showNotifPosition(userPosition, imagePath);
		});




		$('#adwardSharePosTw').click(function(){
			var name = $(this).attr('data-name');
			var description = $(this).attr('data-description');
			var url = 'http://www.librosmexico.mx/';

			shareTW(url, name+description, description);
		});

		$('#adwardSharePosFb').click(function(){
			var name = $(this).attr('data-name');
			var description = $(this).attr('data-description');
			var image = $(this).attr('data-image');
				image = image.replace('.svg', '.png');
			var url = 'http://www.librosmexico.mx/';

			shareFB(name, description, image, url );
		});





	});

	function shareTW(url, titulo, autor){
		titulo += ' en LIBROSMEXICO.MX';
	    var params = {
	        access_token: "65efccf36ae26915b2362f75c2b09b8856732202",
	        longUrl: url,
	        format: 'json'
	    };
	    $.getJSON('https://api-ssl.bitly.com/v3/shorten', params, function (response, status_txt) {
	        var urlbit =response.data.url;
	        var len = 140 - (urlbit.length)+7;  

			if (titulo.length>len) {
				titulo = titulo.substring(0,len);
				titulo = '"'+titulo+'..."';
			} else {
				titulo = '"'+titulo+'"';
			}

			var device = navigator.userAgent;
			var shareURL = 'http://twitter.com/intent/tweet?text='+titulo+' '+urlbit;

			if (device.match(/Iphone/i)|| device.match(/Ipod/i)|| device.match(/Android/i)|| device.match(/J2ME/i)|| device.match(/BlackBerry/i)|| device.match(/iPhone|iPad|iPod/i)|| device.match(/Opera Mini/i)|| device.match(/IEMobile/i)|| device.match(/Mobile/i)|| device.match(/Windows Phone/i)|| device.match(/windows mobile/i)|| device.match(/windows ce/i)|| device.match(/webOS/i)|| device.match(/palm/i)|| device.match(/bada/i)|| device.match(/series60/i)|| device.match(/nokia/i)|| device.match(/symbian/i)|| device.match(/HTC/i)){
				location.target="_newtab";
				location.href = shareURL; 
			}	else {
				
				window.open('http://twitter.com/intent/tweet?text='+titulo+' '+urlbit,"_blank","top=200, left=500, width=400, height=400");

			}
				
	    });
	}

	function shareFB(title, desc, img, url){
	    var share_caption  =    'librosmexico.mx';
	    
	    FB.ui({
	        method: 'feed',
	        appId: '394615860726938',   
	        name: title,
	        link: url,
	        picture: img,
	        caption : share_caption,
	        description: desc
	    }, function(response) {
	        if(response && response.post_id){
	            
	        }
	        else{}
	    }); 
	}

	function showNotifPosition(userPosition, imagePath)
	{
		//se insertan los textos obtenidos en la pantalla de notificacion
		$('#posNumber').text( userPosition );
		$('#textPos').text(userPosition);
		$('#medalImage').attr('src', imagePath);

		//se obtiene valor del scroll actual
		var scrollHeigh = $(window).scrollTop();

		//se establece el top de la ventana de notificacion
		$('#notifPosWrp').css('top', scrollHeigh-100);
		$('#notifPosWrp').show();
		$("#notifPosWrp").animate({top: scrollHeigh, opacity: 1}, 300);

		//se bloquea el scroll
		var $window = $(window);
		$window.disablescroll({
		    handleScrollbar: true
		});

	}


</script>























