<script>
function loginFB(){
  FB.getLoginStatus(checkLoginStatusLoginFB);
}

function checkLoginStatusLoginFB(response) {
  if(response && response.status == 'connected') {
       FB.api('/me',{ fields: 'name, email, id' },function(response) {
          var fid = response.id;
          var name = response.name;
          var email = response.email;
    loadSpinner();
    $.ajax({
        type: "POST",
        url: "login-fb-ajax",
        data:{
              fbid: fid,
              name: name,
              email: email
        },
      async: false,
      dataType: 'json'              
    }).done(function(data) {
      console.log(data.status);
      if (data.status=="doesnt_exists") {
        hideSpinner();
        setNotify('No tenemos tu cuenta registrada en librosmexico.mx', error);
      } else if (data.status == "no") {
        setNotify('Hubo un error al iniciar con facebook.', error);
      } else {
        location.reload();
      }
    }).fail(function() {
        setNotify('Hubo un error al iniciar con facebook.', error);
      });
    });
  } else {
    FB.login(function(response) {
      FB.api('/me',{ fields: 'name, email, id' },function(response) {
        var fid = response.id;
        var name = response.name;
        var email = response.email;
        console.log(response);
        loadSpinner();
        $.ajax({
          type: "POST",
          url: "login-fb-ajax",
          data:{
              fbid: fid,
              name: name,
              email: email
          },
          async: false,
          dataType: 'json'              
        }).done(function(data){

          if (data.status=="no") {
            successFB(fid,name,email);
          } else {
            location.reload();
          }
        }).fail(function(){
            setNotify('Hubo un error al iniciar con facebook.', error);                   
          });
        });
      });            
  }
}
function successFB(fbid,name,email){
      loadSpinner();  
      $.ajax({
          type: "POST",
          url: "login-fb-ajax",
          data:{
              fbid: fbid,
              name: name,
              email: email
          },
        async: false,
        dataType: 'json'              
      }).done(function(data){
         if(data.status=="yes")
          location.reload();
        else{
          hideSpinner();
          setNotify('Hubo un error al iniciar con facebook',error);
        }
      });
}
</script>
<div class="ajax-loader-login">
  
</div>
<div class="inner-block home-signup view-login">
  <div class="dos">
    <div class="row block_1">
      <a href="registro-usuario" class="btn btn-rounded btn-con-cuenta">¿No tienes cuenta?</a>
    </div>
    <div class="block_1 type_A">
      <div class="inscripcion form-group">
        <div class="header-form">
          <div class="img-form-login-background" style="">
            <h3>Inicia sesión aquí</h3>
            <p>Bienvenido(a) a LIBROSMÉXICO.MX la comunidad más grande de lectores en México.</p>
            <p>¿Quieres compartir esta experiencia?</p>
            <div class="register-facebook">
              <a 
                href="javascript:void(0);" 
                onclick='loginFB()' 
                class="btn btn-rounded btn-fb" 
                name="btnRegFb">
                <i class="fa fa-facebook" aria-hidden="true"></i>
                Iniciar sesión con Facebook
              </a>
            </div>
            <div class="row-img">
              <div class="position-conten">
                <figure class="content-imagen">
                    <img class="circle-img" src="assets/images/icon_login.svg">
                </figure>
              </div>
              <p>Inicia sesión</p>
            </div>
          </div>
				</div>
        <form id="inscripcion" method="post">
          <div class="form-group">
            <fieldset class="row">
              <div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
              <?php echo $data['error_datos']; ?>
              </div>
            </fieldset>
            <fieldset class="row">
              <div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
                <label for="inputEmail">Correo electrónico <span class="text-danger">*</span></label>
                <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="" required>
                <label id="error_email" class="text-danger hide">Escribe una dirección de correo válida</label>
                <label id="error_email_formato" class="text-danger hide">Formato de correo no válido</label>
              </div>
            </fieldset>
            <fieldset class="row">
              <div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
                <label for="inputPassword">Contraseña <span class="text-danger">*</span></label>
                <input type="password" class="form-control" name="inputPassword" id="inputPassword" placeholder="" required>
                <label id="error_password" class="text-danger hide">Escribe tu contraseña</label>
                <span class="help-block"><a class="text-green" href="recuperar-password">¿Olvidaste tu contraseña?</a></span>
              </div>
            </fieldset>
            <fieldset class="row">
              <div class="col col-xs-12 col-sm-12 col-lg-8 form-group remember-me">
                <input type="checkbox" name="remember_me" value="1" checked="checked"> Recordar inicio de sesión
              </div>
            </fieldset>
          </div>
          <input id="login" name="login" type="submit" value="Iniciar sesión" class="btn btn-rounded btn-rounded-green">
          <a href="registro-usuario" class="btn btn-rounded bg-color-blue btn-visible">¿No tienes cuenta?</a>
        </form>
      </div>
		</div>
  </div>
</div>
</section>
<script type="text/javascript">
$('#login').click(function(){
	var	datos_error = 0;
	if($('#inputEmail').val() == ""){
		$('#error_email').removeClass("hide");
		$('#error_email_formato').addClass("hide");
		$('#inputEmail').addClass("inputs-error"); 
		datos_error += 1;
	}else{
		$('#error_email').addClass("hide");
		expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    if (!expr.test($('#inputEmail').val())){
	    	$('#error_email_formato').removeClass("hide");
	    	datos_error += 1;
	    	$('#inputEmail').addClass("inputs-error");
	    }else{
	    	$('#error_email_formato').addClass("hide");
	    	$('#inputEmail').removeClass("inputs-error");
	    }
	}

	if($('#inputPassword').val() == ""){
		$('#error_password').removeClass("hide");
		$('#inputPassword').addClass("inputs-error");
		datos_error += 1;
	}else{
		$('#error_password').addClass("hide");
		$('#inputPassword').removeClass("inputs-error");
	}
	if (datos_error == 0) {
		return true;
	}else{
		return false;
	};
});
</script>
<script type="text/javascript">

/**
 * parses and returns URI query parameters 
 * 
 * @param {string} param parm
 * @param {bool?} asArray if true, returns an array instead of a scalar 
 * @returns {Object|Array} 
 */
 $(document).ready(function(){
  var URLactual = window.location;
  URLactual = URLactual.toString();
  var found = URLactual.indexOf('code');
  

  if( found != -1 ){
    $(".ajax-loader-login").html('<img src="assets/images/ajax-loader-t.gif" width="100">');
     $(".ajax-loader-login").show();
  }
});

function loadSpinner(){
  $(".ajax-loader-login").html('<img src="assets/images/ajax-loader-t.gif" width="100">');
  $(".ajax-loader-login").show();
}
function hideSpinner(){
  $(".ajax-loader-login").hide();
}

var estado_pag = 0;
var redirecionamiento = 0;

var var_interval = setInterval(function(){
	estado_pag = $('#var-login').text();
	if (parseInt(estado_pag) == 1) {
	  window.location = "<?php echo $_SESSION['base_url']; ?>";
	  clearInterval(var_interval);
	}
}, 3000);


</script>