<link rel="stylesheet" type="text/css" href="assets/styles/styles_l.css"/>
<div class="inner-block home-signup view-recover">
	
    <div class="dos">
    	<div class="row block_1">
			<a href="inicio-sesion" class="btn btn-rounded btn-con-cuenta">¿Ya tienes una cuenta?</a>
		</div>
       	<div class="block_1 type_A">
			<div class="inscripcion form-group">
				<div class="header-form">
					<div class="img-form-login-background" style="">
					<h3>¿Olvidaste tu contraseña?</h3>
                    <p>Proporciona los siguientes datos para recuperar tu contraseña..</p>
                    <div class="row-img">
	                    <div class="position-conten">
	                        <figure class="content-imagen">
	                            <img class="circle-img" src="assets/images/icon_contrasena.svg">
	                        </figure>
	                    </div>
	                    <p>Recuperar contraseña</p>
	                </div>
                </div>
                </div>
              	<form id="inscripcion" method="post">
              		<div class="form-group">
              			<fieldset class="row">
              				<div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
		                        <?php echo $data['error_datos']; ?>
		                    </div>
              			</fieldset>
              			<fieldset class="row">
              				<div class="col col-xs-12 col-sm-12 col-lg-8 form-group text-light">
		                        <label>Ingresa la dirección de correo electrónico de tu cuenta y te enviaremos un correo con una nueva contraseña.</label>
		                    </div>
              			</fieldset>
              			<fieldset class="row">
              				<div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
		                        <label for="inputEmail">Correo electrónico <span class="text-danger">*</span></label>
		                        <input type="email" class="form-control" id="inputEmail" name="email" placeholder="">
		                        <label id="error_email" class="text-danger hide">Escribe una dirección de correo válida</label>
		                        <label id="error_email_formato" class="text-danger hide">Escribe una dirección de correo válida</label>
		                    </div>
              			</fieldset>
              		</div>
                    <input name="recover_password" id="recover_button" type="submit" class="btn btn-rounded btn-rounded-green" value="Recuperar contraseña">
                    
                    <a href="inicio-sesion" class="btn btn-rounded bg-color-blue btn-visible">¿Ya tienes una cuenta?</a>
				</form>
            </div>
		</div>
    </div>
</div>
</section>
<script type="text/javascript">
$('#recover_button').click(function(){
	if($('#inputEmail').val() == ""){
		$('#error_email').removeClass("hide");
		$('#error_email_formato').addClass("hide");
		$('#inputEmail').addClass("inputs-error");
		return false;
	}else{
		$('#error_email').addClass("hide");
		expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    if (!expr.test($('#inputEmail').val())){
	    	$('#error_email_formato').removeClass("hide");
	    	$('#inputEmail').addClass("inputs-error");
	    	return false;
	    }else{
	    	$('#error_email_formato').addClass("hide");
	    	$('#inputEmail').removeClass("inputs-error");
	    	return true;
	    }
	}
});


$(document).ready(function(){
		<?php
			foreach($data["notificaciones"] as $n) {
		?>
			setTimeout(function(){
				setNotify("<?php echo $n["m"]; ?>", <?php echo $n["l"]; ?>);
			}, 300);
		<?php
			}
		?>
	});
</script>