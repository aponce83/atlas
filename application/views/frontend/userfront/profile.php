<?php
	if($user_front['fullname'])
		$name=$user_front['fullname'];
	else 
		$name=$user_front['namefb'];

	if($user_front['fbid'])
		$disabled = 'disabled';
	else 
		$disabled = '';

	$privacy = array(0=>'',1=>'',2=>'',3=>'');
	$privacy[$user_front['privacy_id']-1] = 'checked';


?>
<input type='hidden' id='actual-email' value='<?=$user_front["email"]?>'>
<input type='hidden' id='actual-set' value='<?=$user_front["pw_set"]?>'>
<input type='hidden' id='actual-handle' value='<?=$user_front["handle"]?>'>
<section class='section-book-detail'>
<div class="container-fluid ">
	<div class='row '>
		<div class="col col-xs-12 home-activity">
			<div class="row no-margin profile-info-row">

				<div class="col-xs-12 no-padding content-img">
					<img src="<?=$user_front['image']?>" onMouseOver='showDiv()' onMouseOut='hideDiv()'  class="profile-img">
					<?php if(!$user_front['fbid']){?>
					<div class="profile-img" id="profile-img" onMouseOver='showDiv()' onMouseOut='hideDiv()'>
						<img src="assets/images/detail/hover_foto.png" class="pointer" >
						<form method="post" enctype="multipart/form-data">
							<input id="uploadedfile" type="file" name="uploadedfile" class="input-file-profile" onchange='updatePicture()'>
							<input type="submit" id="submit-file" class="btn-text" name="archivo" style="display: none !important;" value="Cambiar foto de perfil">
						</form>
					</div>
					<?php }?>
				</div>

				<div class="col-xs-12 no-padding username-row">
					<?=$name?>
				</div>

				<div class="col-xs-12 no-padding">
					<div class="col-xs-12 col-sm-6 col-sm-offset-3 no-padding">
						<hr class="dotted-underline">							
					</div>						
				</div>

				<div class="col-xs-12 no-padding">
					<div class="row no-margin">
						<div class="hidden-xs col-sm-2 no-padding"></div>
						<div class="col-xs-12 col-sm-8 no-padding">
							<div class="row no-margin" style='text-align:center'>
								<a href="<?=Url::base().'biblioteca_general'?>">
									<div class="col-xs-6 no-padding profile-info-col">
										<?=$data->user_list_count? $data->user_list_count: $data['user_list_count'] ?> listas
									</div>
								</a>
								<a href="<?=Url::base().'lista/'.$data['id'].'/ya-lo-lei'?>">
									<div class="col-xs-6 no-padding profile-info-col">
										<?=$data->user_book_count? $data->user_book_count: $data['user_book_count']  ?> <?=$data->user_book_count ?> <?=($data->user_book_count!=1)? "libros leídos" : "libro leído" ?>
									</div>
								</a>
							</div>
						</div>
						<div class="hidden-xs col-sm-2 no-padding"></div>							
					</div>
				</div>

			</div>
		</div>
<!--Data-profile-->
		<div class='col-xs-12 update-info-block desk-profile'>
			<div class='col-xs-4 lateral-info no-padding'>
				<div class='col-xs-12 lateral-item active-item' id='item-data-profile' onclick='displayMenu("data-profile")'>
					Mis datos 
				</div>
				<div class='col-xs-12 lateral-item' id='item-data-privacy' onclick='displayMenu("data-privacy")'>
					Privacidad
				</div>
				<div class='col-xs-12 lateral-item' id='item-data-password' onclick='displayMenu("data-password")'>
					Cambiar contraseña 
				</div>
				<div class='col-xs-12 lateral-item' id='item-data-delete' onclick='displayMenu("data-delete")'>
					Eliminar cuenta
				</div>
			</div>

			<div class='update-data col-xs-8' id='data-profile'>
				<div class='col-xs-10 update-title'>
					Mis datos
				</div>

				<div class='col-xs-10 update-rows'>
					<p>Nombre de usuario</p>
					<input type='text' id='handle' class='col-xs-12' placeholder='@' value="<?=$user_front['handle']?>">
					<p class='error-profile' id='error-handle'>Soló caracteres alfanuméricos</p>
					<p class='error-profile' id='error-handle-exist'>Este nombre de usuario ya está ligado a otra cuenta</p>
				</div>

				<div class='col-xs-10 update-rows'>
					<p>Nombre</p>
					<input type='text' id='fullname' name='name' class='col-xs-12' value='<?=$name?>' placeholder='Escribe tu nombre...' <?=$disabled?>>
					<p class='error-profile' id='error-name'>El nombre es obligatorio</p>
				</div>

				<div class='col-xs-10 update-rows'>
					<p>Correo electrónico</p>
					<input type='text' id='email' class='col-xs-12' value="<?=$user_front['email']?>" placeholder='Escribe tu correo electrónico...' <?=$disabled?>>
					<p class='error-profile' id='error-email'>Escribe un correo válido</p>
					<p class='error-profile' id='error-email-exist'>Este correo ya está ligado a otra cuenta</p>
				</div>

				<div class='col-xs-10 update-rows'>
					<p>Descripción personal</p>
					<textarea id='description' placeholder='Incluye una descripción personal...' class='col-xs-12' ><?=$user_front['bio']?></textarea>
				</div>

				<!--<div class='col-xs-10 update-rows'>
		            <p class='form-group'>
		            	<input  class='tick' id="notificaction" name="notificaction" type="checkbox"/> 
                    	<label id='notlabel' class='tick' for='notificaction'></label> Habilitar notificaciones
                    </p>
				</div>-->

				<div class='col-xs-12 no-padding btn-submit'>
					<div class='col-xs-6 btn-centered'>
						<input type="button" name="guardar-misdatos" onclick="saveData()"  id="guardar-misdatos" value="Guardar cambios" class="input-centered guardar-cambios hover-guardar-cambios"/>
					</div>	
					<?php if(!$user_front['fbid']){?>
					<div class='col-xs-5 btn-centered'>
						<input type="button" onclick='linkFB()' name="ligar-fb" id="ligar-fb" value="Conectar con facebook" class="input-centered guardar-cambios hover-guardar-cambios"/>
					</div>	
					<?php }?>
				</div>
			</div>
<!--Data-Profile-Ends-->
<!--Data-Privacy-Start-->
			<div class='update-data col-xs-8 hide' id='data-privacy' >
				<div class='col-xs-10 update-title'>
					Privacidad
				</div>
				<div class='col-xs-10 update-rows'>
					<p>¿Quién puede ver mi información?</p>
				</div>

				<form class='privacy-panel'>
					<div class='col-xs-10 update-rows'>
						<p><input type='radio' name='privacy'class='privacy-tick privacy' value='1' id='todos' <?=$privacy[0]?>><label for='todos' id='l-1' class='privacy-tick'> Todos</p>						
					</div>
					<div class='col-xs-10 update-rows'>
						<p><input type='radio' name='privacy'class='privacy-tick privacy' value='2' id='amigos' <?=$privacy[1]?>><label for='amigos' id='l-2' class='privacy-tick'> Solo mis seguidores</p>							
					</div>
				</form>

				<div class='col-xs-12 no-padding btn-submit'>
					<div class='col-xs-5 btn-centered'>
						<input type="submit" onclick='savePrivacy()' id="save-privacy" value="Guardar cambios" class="input-centered guardar-cambios hover-guardar-cambios"/>
					</div>	
				</div>
			</div>	
<!--Data-Privacy-End-->
<!--Data-Paswword-Start-->
			<div class='update-data col-xs-8 hide' id='data-password' >
				<div class='col-xs-10 update-title'>
					Cambiar contraseña
				</div>
				<?php if($user_front['pw_set']){?>
				<div class='col-xs-10 update-rows'>
					<p>Contraseña actual</p>
					<input type='password' id='actual-pass' class='col-xs-12' placeholder='Escribe tu contraseña actual...'>
					<p class='error-profile' id='error-pass-actual'>La contraseña es incorrecta</p>
				</div>
				<?php }?>

				<div class='col-xs-10 update-rows'>
					<p>Nueva contraseña</p>
					<input type='password' id='new-pass' class='col-xs-12' placeholder='Escribe una nueva contraseña...'>
					<p class='advice-mini'>La contraseña debe ser difícil de adivinar pero fácil de recordar</p>
				</div>

				<div class='col-xs-10 update-rows'>
					<p>Confirmar contraseña</p>
					<input type='password' id='confirm-pass' class='col-xs-12' placeholder='Confirma tu nueva contraseña...'>
					<p class='error-profile' id='error-pass-confirm'>Las contraseñas no coinciden</p>					
				</div>

				<div class='col-xs-12 no-padding btn-submit'>
					<div class='col-xs-5 btn-centered'>
						<input type="submit" onclick='savePassword()' id="guardar-password" value="Guardar cambios" class="input-centered guardar-cambios hover-guardar-cambios"/>
					</div>	
				</div>
			</div>	
<!--Data-Password-End-->
<!--Data-Delete-Start-->
			<div class='update-data col-xs-8 hide' id='data-delete' >
				<div class='col-xs-10 update-title'>
					Eliminar mi cuenta
				</div>

				<div class='col-xs-10 update-rows'>
					<p>Escribe tu correo electrónico</p>
					<input type='email' id='email-delete' class='col-xs-12' placeholder='Escribe tu correo electrónico...'>
					<p class='error-profile' id='error-delete-email'>El correo es incorrecto</p>					
				</div>
				<?php if($user_front['pw_set']){?>
				<div class='col-xs-10 update-rows'>
					<p>Escribe tu contraseña actual</p>
					<input type='password' id='actual-pass-delete' class='col-xs-12' placeholder='Escribe tu contraseña actual...'>
					<p class='error-profile' id='error-delete-password'>La contraseña es incorrecta</p>					

				</div>

				<div class='col-xs-10 update-rows'>
					<p>Confirmar contraseña</p>
					<input type='password' id='confirm-pass-delete' class='col-xs-12' placeholder='Confirma tu contraseña...'>
					<p class='error-profile' id='error-delete-confirm'>Las contraseñas no coinciden</p>										
				</div>
				<?php }?>

				<div class='col-xs-12 no-padding btn-submit'>
					<div class='col-xs-5 btn-centered'>
						<input onclick='validateDelete()' type="submit" name="guardar-misdatos" id="guardar-misdatos" value="Eliminar mi cuenta" class="input-centered eliminar-cuenta  hover-eliminar-cuenta"/>
					</div>	
				</div>

			</div>	
<!--Data-Delete-Ends-->
		</div>
<!--Data-Mobile-Start-->		
		<div class='col-xs-12 mb-profile' >
			<div class='col-xs-12 no-padding row-menu-data' onclick='showMobile("data-mb")'>
				<div class='mobile-menu-data col-xs-11'>
					Mis datos
				</div>
				<div class='mobile-menu-data col-xs-1'>
					<img src='assets/images/desplegable-b.png'>			
				</div>
			</div>
			<div class='col-xs-12 menu-data-hidden no-padding' id='data-mb'>
				<div class='col-xs-12 no-padding row-input-mb'>
					<p>Nombre</p>
					<input type='text' id='fullname-mb' name='name' class='col-xs-12' value='<?=$name?>' placeholder='Escribe tu nombre...' <?=$disabled?>>
				</div>			
				<div class='col-xs-12 no-padding row-input-mb'>
					<p>Correo electrónico</p>
					<input type='text' id='email-mb' class='col-xs-12' value="<?=$user_front['email']?>" placeholder='Escribe tu correo electrónico...' <?=$disabled?>>
					<p class='error-profile' id='error-email-mb'>Escribe un correo válido</p>
					<p class='error-profile' id='error-email-exist-mb'>Este correo ya está ligado a otra cuenta</p>
				</div>

				<div class='col-xs-12 no-padding row-input-mb'>
					<p>Descripción personal</p>
					<textarea id='description-mb' placeholder='Incluye una descripción personal...' class='col-xs-12' ><?=$user_front['bio']?></textarea>
				</div>	

				<div class='col-xs-12 no-padding btn-centered row-input-mb'>
					<input type="button"  onclick="saveData('-mb')"  value="Guardar cambios" class="input-mb guardar-cambios hover-guardar-cambios"/>
				</div>

				<?php if(!$user_front['fbid']){?>
				<div class='col-xs-12 no-padding btn-centered row-input-mb'>
					<input type="button" onclick='linkFB()' name="ligar-fb" id="ligar-fb" value="Conectar con facebook" class="input-mb guardar-cambios hover-guardar-cambios"/>
				</div>	
				<?php }?>
			</div>			
			<div class='col-xs-12 no-padding row-menu-data' onclick='showMobile("privacy-mb")'>
				<div class='mobile-menu-data col-xs-11 '>
					Privacidad
				</div>
				<div class='mobile-menu-data col-xs-1'>
					<img src='assets/images/desplegable-b.png'>			
				</div>
			</div>
			<div class='col-xs-12 menu-data-hidden no-padding' id='privacy-mb'>
				<div class='col-xs-12 no-padding row-input-mb'>
					<p>¿Quién puede ver mi información?</p>
				</div>

				<form class='privacy-panel'>
					<div class='col-xs-12 no-padding row-input-mb'>
						<p><input type='radio' name='privacy'class='privacy-tick privacy-mob' value='1' id='todos-mb' <?=$privacy[0]?>><label for='todos-mb' id='lmb-1' class='privacy-tick'> Todos</p>						
					</div>
					<div class='col-xs-12 no-padding row-input-mb'>
						<p><input type='radio' name='privacy'class='privacy-tick privacy-mob' value='2' id='amigos-mb' <?=$privacy[1]?>><label for='amigos-mb' id='lmb-2' class='privacy-tick'> Solo mis seguidores</p>							
					</div>
				</form>

				<div class='col-xs-12 no-padding row-input-mb'>
					<input type="submit" onclick='savePrivacy("-mb")' value="Guardar cambios" class="input-mb guardar-cambios hover-guardar-cambios"/>
				</div>
			</div>				

			<div class='col-xs-12 no-padding row-menu-data' onclick='showMobile("pass-mb")'>
				<div class='mobile-menu-data col-xs-11 '>
					Cambiar contraseña
				</div>
				<div class='mobile-menu-data col-xs-1'>
					<img src='assets/images/desplegable-b.png'>			
				</div>
			</div>

			<div class='col-xs-12 menu-data-hidden no-padding' id='pass-mb'>
				<?php if($user_front['pw_set']){?>
				<div class='col-xs-12 no-padding row-input-mb'>
					<p>Contraseña actual</p>
					<input type='password' id='actual-pass-mb' class='col-xs-12' placeholder='Escribe tu contraseña actual...'>
					<p class='error-profile' id='error-pass-actual-mb'>La contraseña es incorrecta</p>
				</div>
				<?php }?>

				<div class='col-xs-12 no-padding row-input-mb'>
					<p>Nueva contraseña</p>
					<input type='password' id='new-pass-mb' class='col-xs-12' placeholder='Escribe una nueva contraseña...'>
					<p class='advice-mini'>La contraseña debe ser difícil de adivinar pero fácil de recordar</p>
				</div>

				<div class='col-xs-12 no-padding row-input-mb'>
					<p>Confirmar contraseña</p>
					<input type='password' id='confirm-pass-mb' class='col-xs-12' placeholder='Confirma tu nueva contraseña...'>
					<p class='error-profile' id='error-pass-confirm-mb'>Las contraseñas no coinciden</p>					
				</div>

				<div class='col-xs-12 no-padding row-input-mb'>
					<input type="submit" onclick='savePassword("-mb")' value="Guardar cambios" class="input-mb guardar-cambios hover-guardar-cambios"/>
				</div>
			</div>


			<div class='col-xs-12 no-padding row-menu-data' onclick='showMobile("delete-mb")' style='margin-bottom:30px;'>
				<div class='mobile-menu-data col-xs-11 '>
					Eliminar cuenta
				</div>
				<div class='mobile-menu-data col-xs-1'>
					<img src='assets/images/desplegable-b.png'>			
				</div>
			</div>

			<div class='col-xs-12 menu-data-hidden no-padding' id='delete-mb'>
				<div class='col-xs-12 no-padding row-input-mb'>
					<p>Escribe tu correo electrónico</p>
					<input type='email' id='email-delete-mb' class='col-xs-12' placeholder='Escribe tu correo electrónico...'>
					<p class='error-profile' id='error-delete-email-mb'>El correo es incorrecto</p>					
				</div>

				<?php if($user_front['pw_set']){?>
				<div class='col-xs-12 no-padding row-input-mb'>
					<p>Escribe tu contraseña actual</p>
					<input type='password' id='actual-pass-delete-mb' class='col-xs-12' placeholder='Escribe tu contraseña actual...'>
					<p class='error-profile' id='error-delete-password-mb'>La contraseña es incorrecta</p>					

				</div>

				<div class='col-xs-12 no-padding row-input-mb'>
					<p>Confirmar contraseña</p>
					<input type='password' id='confirm-pass-delete-mb' class='col-xs-12' placeholder='Confirma tu contraseña...'>
					<p class='error-profile' id='error-delete-confirm-mb'>Las contraseñas no coinciden</p>										
				</div>
				<?php }?>

				<div class='col-xs-12 no-padding row-input-mb'  style='margin-bottom:30px;'>
					<input onclick='validateDelete("-mb")' type="submit" value="Eliminar mi cuenta" class="input-mb eliminar-cuenta hover-eliminar-cuenta"/>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<script>
$( document ).ready(function() {
$('.profile-img-home-dsk').attr('src','<?=$user_front["image"]?>');
});
function showMobile(id){
	$("#"+id).toggle();
}
function updatePicture(){
		 var file = $("#uploadedfile").val();
		 console.log(file);
		 if(!file == ""){		 	
			document.getElementById("submit-file").click();
		}
}
function displayMenu(id){

	$(".lateral-item").each(function(){
		$(this).removeClass('active-item');
	});
	$(".update-data").each(function(){
		$(this).addClass('hide');
	});
	$("#"+id).removeClass('hide');
	$("#item-"+id).addClass('active-item');
}
function savePrivacy(id){
	var radio = 1;
	if(id===undefined){
		$('.privacy').each(function(){
			if($(this).is(":checked"))
				radio = $(this).val();
		});
	}else{ 
		$('.privacy-mob').each(function(){
			if($(this).is(":checked"))
			radio = $(this).val();
		});
	}

		$.ajax({
	        type: "POST",
	        url: "ajax-save-data",
	        data:{
	        	privacy_id:radio
	        },
	        dataType: 'json'
		}).done(function(data){
	       if(data.status=="yes"){
				setNotify('Tus datos se actualizaron correctamente',exito);
	      }else{
	       		setNotify('Ocurrió un error al actualizar tus datos',error);
	      }
	});	
}

function saveData(id){
	var ban = 0;
	if(id===undefined)
		id='';
	var email  = $('#email'+id).val();
	var actual = $('#actual-email').val();
	var actualhandle = $('#actual-handle').val();
	var name   = $('#fullname'+id).val();
	var description = $('#description'+id).val();
	var set = $('#actual-set').val();
	var handle = $('#handle').val();

    $('#error-email-exist'+id).hide();
    $('#error-name').hide();
    $('#error-handle-exist'+id).hide();

    if(set){
			if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(email)){
				$('#error-email'+id).hide();
				if(email != actual){
					$.ajax({
						        type: "POST",
						        url: "verify-email-register",
						        data:{
						        		email: email,
						        },
						        async: false,
						        dataType: 'json'
					}).done(function(data){
					       if(data.status=="yes"){
					       		$('#error-email-exist'+id).hide();
					      }else{
					       		$('#error-email-exist'+id).show();
					       		ban++;
					      }
					});
				}
			}
			else{
				$('#error-email'+id).show();
				ban++;
			}

			if ( name == '') {
				$('#error-name'+id).show();
				ban++;
			}

			var pattern = new RegExp(/[^A-Za-z0-9]/)
			if( !(pattern.test(handle)) && (handle != '') ){
				$('#error-handle'+id).hide();

				if( handle != actualhandle ){
					$.ajax({
							type: "POST",
							url: "verify-handle-register",
							data:{
									handle: handle,
							},
							async: false,
							dataType: 'json'
					}).done(function(data){
							if(data.status=="yes"){
									$('#error-handle-exist'+id).hide();
							}else{
									$('#error-handle-exist'+id).show();
									ban++;
							}
					});
				}

			}else{
				$('#error-handle'+id).show();
				ban++;
			}

		}

	if(ban==0){
		$.ajax({
						type: "POST",
						url: "ajax-save-data",
						data:{
								email: email,
								fullname: name,
								bio: description,
								handle: handle
						},
						dataType: 'json'
			}).done(function(data){
					if(data.status=="yes"){
						setNotify('Tus datos se actualizaron correctamente',exito);
						$('.usr-top-name').text(name);
						$('.username-row').text(name);
					}else{
						setNotify('Ocurrió un error al actualizar tus datos',error);
					}
		});
	}
}
function savePassword(id){
	$('#error-pass-confirm'+id).hide();	
	var set = $('#actual-set').val();	
	if(id===undefined)
		id='';

	var ban = 0;
	if(!validateNewPass(id)){
		$('#error-pass-confirm'+id).show();
		ban++;
	}else
		$('#error-pass-confirm'+id).hide();

	if(set){
		if(!validateCurrentPass(id)){
		   $('#error-pass-actual'+id).show();
		   ban++;
		}else
			$('#error-pass-actual'+id).hide();
	}

	if (ban==0) {
		$('#error-pass-confirm'+id).hide();		
	  $('#error-pass-actual'+id).hide();

		var pass = $("#new-pass"+id).val();  	
		var old = $('#actual-pass'+id).val();
		var set    = $('#actual-set').val();	

		$.ajax({
		        type: "POST",
		        url: "ajax-save-data",
		        data:{
		        		password: pass,
		        		old: old,
		        		type: 'password',
		        		set: set
		        },
		        dataType: 'json'
			}).done(function(data){
	      if (data.status=="yes") {
					setNotify('Tus datos se actualizaron correctamente',exito);
					$("#new-pass"+id).val("");	
					$('#actual-pass'+id).val("");
					$("#confirm-pass"+id).val("");
	      } else {
	       	setNotify('Ocurrió un error al actualizar tus datos',error);
	      }
		});	
	}
}
function validateDelete(id){

	if(id===undefined)
		id='';

	var email  = $('#email-delete'+id).val();
	var set    = $('#actual-set').val();	
	var actual = $('#actual-email').val();

	var confirm= $('#confirm-pass-delete').val();
	var pass   = $('#actual-pass-delete').val();
	var ban = 0;

	if(email != actual){
		$('#error-delete-email'+id).show();
		ban++;
	}else
		$('#error-delete-email'+id).hide();

	if(set){
		if(!validateCurrentPass('-delete'+id)){
		   $('#error-delete-password'+id).show();
		   ban++;
		}else{
			$('#error-delete-password'+id).hide();

			if(pass!=confirm){
				$('#error-delete-confirm'+id).show();
				ban++;
			}
			else {
				$('#error-delete-confirm'+id).hide();				
			}
		}
	}
	if(ban==0){
		$('#error-delete-confirm'+id).hide();		
	   	$('#error-delete-password'+id).hide();
		$('#error-delete-email'+id).hide();

		$.ajax({
		        type: "POST",
		        url: "delete-acount",
		        data:{
		        		password: pass,
		        		email: email,
		        		set: set
		        },
		        dataType: 'json',
		        async: false
			}).done(function(data){
		      if (data.status=="yes") {
		      	location.reload();
		      } else {
		       	setNotify('Ocurrió un error al eliminar tu cuenta',error);
		      }
			});	
	}	
}

function validateNewPass(id){

	var newer   = $("#new-pass"+id).val();
	var confirm = $("#confirm-pass"+id).val();

	if(newer!=confirm || newer.length==0)
		return false;
	else 
		return true;
}

function validateCurrentPass(id){

  var pass = $('#actual-pass'+id).val();
  var ret=false;
	    $.ajax({
	        type: "POST",
	        url: "verify-pass",
	        data:{
	        		password: pass
	        },
	        async: false,
		    dataType: 'json'	        
	    }).done(function(data){
		       if(data.status=="yes"){
		       ret = true;
		   }
	       	else 
	       		ret = false;
	    });
	return ret;
}

function linkFB(){	
	FB.getLoginStatus(checkLoginStatus);
}

function checkLoginStatus(response) {
    if(response && response.status == 'connected') {
         FB.api('/me',{ fields: 'name, email, id' },function(response) {
            var fid = response.id;
			$.ajax({
	            type: "POST",
	            url: "link-facebook",
	            data:{
	            	  fbid: fid
	            }
	        }).done(function(){
  				setNotify('Tu cuenta se ligó correctamente a facebook.', exito);
  				location.reload();					       		
	        }).fail(function(){
  				setNotify('Hubo un error al conectar las cuentas.', error);					       		
	        });
        });
    } else {
         FB.login(function(response) {
            FB.api('/me', function(response) {
            console.log(JSON.stringify(response));})
         });            
    }
}
function showDiv(){

	$('#profile-img').show();
}
function hideDiv(){
	var mq = window.matchMedia( "(max-width: 767px)" );
	if (mq.matches) 
		$('#profile-img').val();
	else		
		$('#profile-img').hide();
}
</script>
<link rel="stylesheet" type="text/css" href="assets/styles/update.css"/>
