<link rel="stylesheet" type="text/css" href="assets/styles/styles_l.css">
  <?php if(isset($data['error'])){ ?>
			<script>
				console.log("<?=$data['error']?>");
			</script>
	<?php } ?>
<div class="inner-block home-signup wcm-cont">
    <div id="bienvenidos" class="dos <?php echo $enable; ?> ">
       	<div class="block_1 type_A">
			<div class="inscripcion form-group">
				<div class="header-form">
					<div class="row" style="position:relative;">
						<a href="" class="right btn-closed_welcome" style="position: absolute; top: 7px; right: 25px;"><img src="assets/images/icon_close.svg"></a>
					</div>
					<hgroup class="form-group">
	                    <h2 style="font-size: 40px;"><?php echo $data['name_user_log_reg']; ?></h2>
	                    <h3 style="font-family: 'Open Sans',sans-serif!important; font-weight: 400; margin-bottom: 40px; color: #0B7F6B; font-size: 30px;">bienvenido(a) a LIBROSMÉXICO.</h3>
	                </hgroup>
	                <div class="row-img-bienbenidos form-group">
                        <div class="position-conten">
                            <figure class="content-imagen">
                                <img class="circle-img" src="assets/images/icono_bienvenida.svg" style="width:174px height:174px; margin-bott6om:40px;">
                            </figure>
                        </div>
                        <p style=" font-family: 'Open Sans',sans-serif!important; font-weight: 300;">Podrás crear listas de tus libros favoritos,
                          <span>compartir información con tus amigos</span>, encontrar lugares para comprar, <span>conocer otros lectores</span>, escribir reseñas sobre tus textos preferidos, <span>encontrar nuevos libros</span> y más.</p>
                    </div>
                </div>
              	<form class="row no-margin" id="inscripcion" method="post">
					          <?php /*
                    <div class="row" id="rowInfWelcm">
                        <p>¿<span>Quieres conocer</span> todo lo que <span>LIBROSMÉXICO.MX</span> tiene para tí?</p>
                    </div>

                    <div class="col col-xs-12 col-sm-12 col-lg-8 form-group group-btns welcome-btns">

                    	<a href="<?php echo URL::base(); ?>" id="btn-ver-img" class="btn btn-rounded btn-rounded-green" style="font-family: 'Montserrat',serif; padding-top: 10px; padding-bottom: 10px; font-size: 20px; width: 45%;">Quiero verlo <img id="img-btn-ver" src="assets/images/icono_flecha-bienvenida.svg"></a>

                     <input id="save_user" class="btn btn-rounded right btn-cansel-wcm" name="btn_cansel" value="Cancelar" type="submit" style="font-family: 'Montserrat',serif; padding-top: 10px; padding-bottom: 10px; font-size: 20px;"/>
                    </div>
                    */ ?>
				        </form>
            </div>
		</div>
    </div>
</div>
</section>

<script>
  $(document).ready(function(){
    //determinar ancho del navegador
    var docWidth = $(window).width();
    if( docWidth <= 480 )
    {
      $('#rowInfWelcm').remove();
      $('.btn-cansel-wcm').remove();

      var newHtmlBtn = 'Continuar <img id="img-btn-ver" src="assets/images/icono_flecha-bienvenida.svg">';
      $('#btn-ver-img').html( newHtmlBtn );
    }
  });
</script>

<script type="text/javascript">
$( "a#btn-ver-img" ).mouseover(function() {

    $("#img-btn-ver").attr("src","assets/images/icono_flecha-bienvenida-white.svg");
  }).mouseout(function() {
    $("#img-btn-ver").attr("src","assets/images/icono_flecha-bienvenida.svg");
  });
 /*
var n = 0;
$( "div.enterleave" )
  .mouseenter(function() {
    n += 1;
    $( this ).find( "span" ).text( "mouse enter x " + n );
  })
  .mouseleave(function() {
    $( this ).find( "span" ).text( "mouse leave" );
  });*/
</script>