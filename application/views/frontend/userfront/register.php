<link rel="stylesheet" type="text/css" href="assets/styles/styles_l.css"/>
<script>
  function loadSpinner(){
    $(".ajax-loader-login").html('<img src="assets/images/ajax-loader-t.gif" width="100">');
    $(".ajax-loader-login").show();
  }
  function hideSpinner(){
    $(".ajax-loader-login").hide();
  }

  function register_facebook() {
    loadSpinner();
    FB.getLoginStatus(login_status_cb);
  }

  function login_status_cb(response) {
      FB.login(function(rs) {
        FB.api('/me', { fields: 'name, email, id' }, function(data) {
          register_fb_ajax(data);
        });
      },{scope:'email'});
  }

  function register_fb_ajax(data) {
    if (data.error) {
      hideSpinner();
      return;
    }
    var fbid = data.id;
    var email = data.email;
    var name = data.name;
    var lmxid = "<?php echo $data['lmxid'];?>";
    $.ajax({
        type: "POST",
        url: "register-fb-ajax",
        data:{
          fbid: fbid,
          name: name,
          email: email,
          lmxid: lmxid
        },
      async: true,
      dataType: 'json'
    }).done(function(data){
      if (data.status == "yes") {
        window.location = '<?php echo URL::base(true)."user_welcome"; ?>';
      }
    });
  }
</script>
<div class="ajax-loader-login">

</div>
<div class="inner-block home-signup">
  <div id="form-registro" class="dos">
    <div class="row block_1">
      <a href="inicio-sesion" class="btn btn-rounded btn-con-cuenta">¿Ya tienes una cuenta?</a>
    </div>
   	<div class="block_1 type_A">
			<div class="inscripcion form-group">
				<div class="header-form">
					<div class="img-form-login-background" style="">
				    <hgroup class="form-group">
              <h2>Regístrate aquí</h2>
              <p>Y sé parte de la comunidad más grande de lectores en México.</p>
              <p>Comparte la experiencia con tus amigos.</p>
            </hgroup>
            <div class="register-facebook">
              <a
                href="javascript:void(0);"
                onclick='register_facebook()'
                class="btn btn-rounded btn-fb"
                name="btnRegFb">
                <i class="fa fa-facebook" aria-hidden="true"></i>
                Registrarse con Facebook
              </a>
            </div>
            <div class="row-img">
              <div class="position-conten">
                <figure class="content-imagen">
                  <img class="circle-img" src="assets/images/icon_registrarse.svg">
                </figure>
              </div>
              <p>Regístrate</p>
            </div>
          </div>
        </div>
        <form id="inscripcion" method="post" action='userfront/save'>
          <input type="hidden" name="lmxid" value="<?php echo $data['lmxid']; ?>">
          <div class="form-group">
          	<fieldset class="row">
      				<div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
	              <?php echo $data['error_datos']; ?>
                <?php
                  $errors = $data['errors'];
                  $error_duplicate_email = false;
                  $error_duplicate_username = false;
                  foreach ($errors as $error) {
                    // revisa si el correo ya está duplicado
                    if ($error['error_no'] == 1001) {
                      $error_duplicate_email = true;
                    }
                    if ($error['error_no'] == 1002){
                      $error_duplicate_username = true;
                    }
                  }
                ?>
              </div>
      			</fieldset>
            <fieldset class="row">
              <div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
                <span>Nombre de usuario<span class="text-danger">*</span></span>
              </div>
              <div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
                <label>
                  <input type="text" name="username" id="username_reg" class="required" value="<?php echo $data['user']['username']; ?>">
                </label>
                <label id="error_username_repeated" class="text-danger <?php if (!$error_duplicate_username) { echo 'hide'; } ?>">El nombre de usuario ya está registrado</label>
                <label id="error_username" class="text-danger hide">Nombre de usuario no válido (caracteres alfanuméricos)</label>
              </div>
            </fieldset>
            <fieldset class="row">
              <div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
                <span>Nombre <span class="text-danger">*</span></span>
              </div>
              <div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
                <label>
                  <input type="text" name="name" id="name_reg" class="required" value="<?php echo $data['user']['fullname']; ?>">
                </label>
                <label id="error_name" class="text-danger hide">Escribe tu nombre completo</label>
              </div>
            </fieldset>
            <fieldset class="row">
              <div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
                <span>Correo electrónico <span class="text-danger">*</span></span>
              </div>
              <div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
                <label>
                  <input
                    type="text"
                    name="email"
                    id="email_reg"
                    class="required <?php if ($error_duplicate_email) { echo 'inputs-error'; } ?>"
                    value="<?php echo $data['user']['email']; ?>">
                </label>
                <label id="error_email_repeated" class="text-danger <?php if (!$error_duplicate_email) { echo 'hide'; } ?>">El correo ya está registrado</label>
                <label id="error_email" class="text-danger hide">Escribe una dirección de correo válida</label>
                <label id="error_email_formato" class="text-danger hide">Formato de correo no válido</label>
              </div>
            </fieldset>
           	<fieldset class="row">
              <div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
             		<span> Contraseña <span class="text-danger">*</span></span>
              </div>
           		<div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
                <label> <input type="password" id="pass_reg" name="password" class="required"></label>
                <label class="text-gray">La contraseña tiene que ser difícil de adivinar pero fácil de recordar.</label>
                <label id="error_password" class="text-danger hide">Contraseña Obligatoria</label>
                <label id="error_password_count" class="text-danger hide">Tu contraseña debe contener mínimo 8 caracteres</label>
				   		</div>
            </fieldset>
            <fieldset class="row">
           		<div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
                <span> Confirmar contraseña <span class="text-danger">*</span></span>
              </div>
              <div class="col col-xs-12 col-sm-12 col-lg-8 form-group">
                <label> <input type="password" id="pass_reg2" name="password" class="required"></label>
                <label id="error_passw_coincidencia" class="text-danger hide">Las contraseñas no coinciden</label>
				   		</div>
           	</fieldset>
          </div>
          <li class="col col-xs-12 col-sm-12 col-lg-8 form-group">
            <div>
              <ol class="checkboxw">
                <li class="text-gray">
                  <input type="checkbox" id="terminos" name="terminos" value="1"/>
                  <label class="span-css terminos">Acepto las <a class="text-gray" href="esp/terminos-y-condiciones" target="_self">políticas de privacidad</a>
                    y <a class="text-gray" href="esp/terminos-y-condiciones" target="_self">los términos y condiciones.</a>
                  </label>
               	</li>
               	<li>
               		<label id="error_check" class="text-danger hide">Para registrarte debes de aceptar las politicas de privacidad</label>
               	</li>
                <?php /*
                <li class="text-gray" style="margin-top: 10px">
                  <input type="checkbox" id="receive_notifications" name="receive_notifications" value="1"/>
                  <label class="span-css">
                    Recibir notificaciones de nuevas publicaciones de <a href="https://unade.librosmexico.mx" target="_blank">Una De Libros</a>
                  </label>
                </li>
                */ ?>
              </ol>
            </div>
          </li>

          <input class="btn right btn-rounded-green" id="save_user" name="user_register" value="Regístrate" type="submit"/>
          <a href="inicio-sesion" class="btn btn-rounded bg-color-blue btn-visible">¿Ya tienes una cuenta?</a>
        </form>
      </div>
    </div>
    </div>
</div>
</section>
<div id="var-login" class="hide">
	<?php echo $data['login-facebook']; ?>
</div>
<div id="var-dir" class="hide">
	<?php echo $data['login-facebook']; ?>
</div>

<script>
$('#save_user').click(function(){
	var	datos_error = 0;
  var pattern = new RegExp(/[^A-Za-z0-9]/)
  var username = $('#username_reg').val();
  if( pattern.test(username) || username == '' ){
		$('#error_username').removeClass("hide");
		$('#username_reg').addClass("inputs-error");
		datos_error += 1;
	}else{
		$('#error_username').addClass("hide");
		$('#username_reg').removeClass("inputs-error");
	}
	if($('#name_reg').val() == ""){
    $('#error_name').removeClass("hide");
    $('#name_reg').addClass("inputs-error");
    datos_error += 1;
  }else{
    $('#error_name').addClass("hide");
    $('#name_reg').removeClass("inputs-error");
  }
  if($('#email_reg').val() == ""){
		$('#error_email').removeClass("hide");
		$('#error_email_formato').addClass("hide");
		$('#email_reg').addClass("inputs-error");
		datos_error += 1;
	}else{
		$('#error_email').addClass("hide");
		expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    if (!expr.test($('#email_reg').val())){
	    	$('#error_email_formato').removeClass("hide");
	    	$('#email_reg').addClass("inputs-error");
			datos_error += 1;
	    }else{
	    	$('#email_reg').removeClass("inputs-error");
	    	$('#error_email_formato').addClass("hide");
	    }
	}

	if($('#pass_reg').val() == ""){
		$('#error_password').removeClass("hide");
		$('#pass_reg').addClass("inputs-error");
		datos_error += 1;
	}else{
		$('#error_password').addClass("hide");
		if($('#pass_reg').val().length <= 7){
			$('#error_password_count').removeClass("hide");
			$('#pass_reg').addClass("inputs-error");
			datos_error += 1;
		}else{
			$('#error_password_count').addClass("hide");
			$('#pass_reg').removeClass("inputs-error");
		}
	}
	if($('#pass_reg').val() == $('#pass_reg2').val()){
		$('#error_passw_coincidencia').addClass("hide");
		$('#pass_reg2').removeClass("inputs-error");
	}else{
		$('#error_passw_coincidencia').removeClass("hide");
		$('#pass_reg2').addClass("inputs-error");
		datos_error += 1;
	}
	if($('#terminos').is(':checked')){
		$('#error_check').addClass("hide");
		$('#terminos').removeClass("inputs-error");
	}else{
		$('#error_check').removeClass("hide");
		$('#terminos').addClass("inputs-error");
		datos_error += 1;
	}
	if (datos_error == 0) {
		return true;
	}else{
		$('#pass_reg').val("");
		$('#pass_reg2').val("");
		return false;
	};
});

$('#form-registro label.span-css').click(function(){
  if($(this).parent().find('input').is(":checked")) {
    $(this).parent().find('input').prop('checked', '');
    $(this).css("background", "url(assets/images/check_box.svg) left no-repeat");

  }else{
    $(this).parent().find('input').prop('checked', 'checked');
    $(this).css("background", "url(assets/images/check_box_select.svg) left no-repeat");
  };
});

 </script>
 <div id="fb-root"></div>

<script type="text/javascript">

/**
 * parses and returns URI query parameters
 *
 * @param {string} param parm
 * @param {bool?} asArray if true, returns an array instead of a scalar
 * @returns {Object|Array}
 */
function getURIParameter(param, asArray) {
  return document.location.search.substring(1).split('&').reduce(function(p,c) {
    var parts = c.split('=', 2).map(function(param) { return decodeURIComponent(param); });
    if(parts.length == 0 || parts[0] != param) return (p instanceof Array) && !asArray ? null : p;
    return asArray ? p.concat(parts.concat(true)[1]) : parts.concat(true)[1];
  }, []);
}

var par = getURIParameter("code");

if( par != null ){
 //$(".ajax-loader-login").show();
}

var estado_pag = 0;
var redirecionamiento = 0;

var var_interval = setInterval(function(){

  estado_pag = $('#var-login').text();
  if (parseInt(estado_pag) == 1) {
    window.location = "<?php echo $_SESSION['base_url']; ?>"+"/user_welcome";
    clearInterval(var_interval);
  }
	if (parseInt(estado_pag) == 2) {
    window.location = "<?php echo $_SESSION['base_url']; ?>"+"/inicio-sesion";
    clearInterval(var_interval);
  }
  if (parseInt(estado_pag) == 3) {
    $(".ajax-loader-login").html('<img src="assets/images/ajax-loader-t.gif" width="100">');
    $(".ajax-loader-login").show();
    window.location = "<?php echo $_SESSION['base_url']; ?>"+"/";
    clearInterval(var_interval);
  }
}, 300);

</script>