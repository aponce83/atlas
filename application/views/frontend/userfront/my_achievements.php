<link rel="stylesheet" type="text/css" href="assets/styles/custom_f.css"/>

<div class="achiev-wrp">
	<h1>TROFEOS
		<a class="ranking-list" href="tabla-posiciones/amigos">Tabla de posiciones</a>
	</h1>
	<div class="info-achievments-row">
		<div class="sec-inf">
			<img src="assets/images/biblos.svg">
			<div class="inf-text">
				<p class="qty"><?php echo $data['user_score']; ?></p>
				<p class="name">biblos</p>
			</div>
		</div>
		<div class="sec-inf midd-sec-inf">
			<img src="assets/images/trofeos.svg">
			<div class="inf-text">
				<p class="qty"><?php echo $data['total_adwards']; ?></p>
				<p class="name">trofeos</p>
			</div>
		</div>
		<div class="sec-inf last-sec-inf">
			<img src="assets/images/<?php echo $data['medal_image']; ?>.svg">
			<div class="inf-text">
				<p class="qty"><?php echo $data['user_position']; ?><span class="super-ind"><?php echo $data['user_positionAux']; ?></span></p>
				<p class="name">posición general</p>
			</div>
		</div>
	</div>
	<div class="achiev-content row">

		<?php 
		//se muestran todos los trofeos registrados en el sistema
		foreach ( $data['adwards_info'] as $award ) 
		{
			if( $award['id']!=31 && $award['id']!=32 )
			{
			?>

			<div class="col-md-3 col-sm-6">
			<div class="achiev-item" data-name="<?php echo $award['name']; ?>" data-description="<?php echo $award['description']; ?>" data-image="<?php echo $award['image_path']; ?>" >
				<div class="img-wrp">
					<?php
					if( array_search( $award['id'], $data['user_adwards'] )  !== false )
					{ 
					?>
						<img class="lnk-award-img" src="assets/images/<?php echo $award['image_path']; ?>">
					<?php
					}
					else
					{
					?>
						<img src="assets/images/awards/placeholder.svg">
					<?php
					}
					
						
					?>
				</div>
				<div class="inf-wrp">
					<hr class="gray-line">
					<p class="name">
						<span><?php echo $award['name']; ?></span>
					</p>
					<p class="description">
						<?php echo $award['description']; ?>
					</p>
					<hr class="red-line">
					<p class="date">
						<?php
						if( array_search( $award['id'], $data['user_adwards'] )  !== false )
						{
							$array_pos = array_search( $award['id'], $data['user_adwards'] );
						?>
							Ganado el: <?php echo $data['user_adwards_dates'][$array_pos]; ?>
						<?php
						}
						else
						{
						?>
							Aún no tienes éste trofeo
						<?php
						}
						?>
						
						
					</p>
				</div>
				
			</div>
		</div>

			<?php
			}
		

		



			
		}
		?>


	</div>
</div>

<script>
	$(document).ready(function(){

		//se muestra informacion del trofeo seleccionado
		$('.lnk-award-img').click(function(){

			var item = $(this).closest('.achiev-item');


			//se obtiene información del trofeo seleccionado
			var adwardParams = [];
    		adwardParams['name'] = item.data('name');
    		adwardParams['description'] = item.data('description');
    		adwardParams['image'] = item.data('image');

    		showNotification( adwardParams );

		});

	});
</script>

