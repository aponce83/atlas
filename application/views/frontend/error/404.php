<!--h2>Lo sentimos la pagina solicitada no existe</h2-->
<section>
                <!-- ============================= 
                            Comienza contenido
                     ============================= -->
                <div class="error">
                    <article>
                        <img alt="notfound" class="left" src="assets/images/error-404.png" />
                        <div class="right">
                            <h3>Lo sentimos la página a la que intentaste entrar no existe o existe un error.</h3>
                            <h4>Si deseas puedes intentar una búsqueda</h4>
                            <form id="search_error"  action="esp/busqueda" method="get" >
                                <input class="bserror" type="text"  name="texto" placeholder="Busca por autor, libro, ISBN o editorial...">
								<!--input  value="" id="search_button" type="submit" /-->   
                            </form>
                            <!--h4 class="center">&oacute;</h4>
                            <nav class="opciones">
                               <a class="inicio" href="<?php echo URL::base();?>">Regresar al inicio</a>
							   <?php// if($user_front){ ?>
                               <a href="esp/mi-biblioteca">Ir a la biblioteca</a>
							   <?php //} ?>
                            </nav-->
							
                        </div>
                    </article>
                </div>
            </section>