<?php
  
  $lang = $data['lang'];
  $userid_list = $data['user_id'];
  $slug = $data['list_slug'];
  $list_id = $data['list_id'];
  $current_user = Session::instance()->get('user_front');
  $current_user_id = $current_user['id'];

  //Tipos de Producto
  $tipos = array(
    '10' => 'Libro',
    '11' => 'Colección',
    '20' => 'CD',
    '30' => 'Libros electrónicos',
    '40' => 'Cd-ROM',
    '50' => 'DVD',
    '60' => 'Libro + CD',
    '70' => 'Audio Libro',
    '80' => 'Revista',
  );

  //notificación de libro borrado
  if( isset($_SESSION['deleted_book']) )
  {
    if( $_SESSION['deleted_book']==true )
    {
      ?>
        <script> setNotify("El libro se ha borrado correctamente de tu lista.", exito); </script>
      <?php
    }
    else
    {
      ?>
        <script> setNotify("Ocurrió un problema por favor intenta más tarde.", error); </script>
      <?php
    }
    //se destruye la variable de sesión 'sent_mail'
    unset( $_SESSION['deleted_book'] );
  }
    

  //---- Verifica la existencia de la lista
  if( $list_id === null )
  {
    echo '<div class="list-msg">La lista no existe</div>'; 
    die();
  }
  
  //---- Configurar (Editar) lista (Formulario autoprocesado)
  if( isset($_POST['submit-update-list']))
  {
    if( $current_user_id === $userid_list )
    {
      $list_name = $_POST['list-name'];
      $list_desc = $_POST['list-description'];
      $list_keyw = $_POST['list-keywords'];
      $list_privacy = $_POST['list-privacy'];

      $data = array(
        'name'        => $list_name,
        'description' => $list_desc,
        'key_words'   => $list_keyw,
        'privacy'     =>$list_privacy,
        'userfront_id'=> $userid_list
      );
      Model::factory('List')->update_list( $list_id, $data );
    }
    else
    {
      echo '<div class="list-msg">No puede editar listas de otros usuarios</div>';
    }
  }

  //----Busca nuevos libros en listas dinamicas y los agrega--
  if( Model::factory('List')->is_dynamic_list( $list_id ) )
  {
    $new_dynamic_books = Model::factory('List')->auto_populate_dynimic_list( $list_id );
  }

  $list = Model::factory('List')->getList($list_id);
  $list_name = $list->getName();
  $list_descrip = $list->getDescription();
  $list_books = $list->getBooks();
  $list_type = $list->getType();
  $list_privacy = $list->getPrivacy();
  $list_keywords = $list->getKeyWords();

  $has_keywords = (strlen(trim($list_keywords)) > 0);
  $key= explode(",",$list_keywords);


  //---- Verifica Privacidad de la lista
  if( $current_user_id !== $userid_list AND $list_privacy == 'private'  )
  {
    echo '<div class="list-msg">Esta lista es privada</div>';
    die();
  }

  foreach ($data['history_likes'] as $likes){
      $like[]=$likes['list_like'];
  }

  if($data['likes'][0]['likes']!=NULL){
    if(isset($current_user))
       $likes = "<span class='like-count-".$list_id."' data-count='".$data['likes'][0]['likes']."'> | ".$data['likes'][0]['likes']." me gusta</span>";
    else
       $likes = "<span class='like-count-".$list_id."' like-count-".$list_id."' data-count='".$data['likes'][0]['likes']."'> ".$data['likes'][0]['likes']." me gusta</span>";      
  }
  else
    $likes = "<span class='like-count-".$list_id."' data-count='0'> </span>";

  if($data['history_likes']){
    if(in_array($list_id,$like))
      $like_label = "<span class='pointer btn-like list-like-".$list_id."' onClick='likeItList(".$list_id.")' data-like='1'>Ya no me gusta </span>";
    else
      $like_label = "<span class='pointer btn-like list-like-".$list_id."' onClick='likeItList(".$list_id.")' data-like='0'>Me gusta </span>";
  }

  //----Colores de la lista---
  if( $slug == 'mi-biblioteca' OR 
    $slug == 'lo-estoy-leyendo' OR
    $slug == 'ya-lo-lei' OR 
    $slug == 'lo-quiero-leer')
  {
    $color = 'green';
  } else if( $list_type == 'classic' ) {
    $color = 'orange';
    $mis_listas = 'mis-listas';
  } else {
    $color = 'blue';
  }

?>
<script type="text/javascript">
<?php
foreach ($data['noty_message'] as $noty_message) {
?>
  setNotify("<?php echo $noty_message['message']; ?>", <?php echo $noty_message['type']; ?>);
<?php
}?>
</script>
<link rel="stylesheet" type="text/css" href="assets/styles/book_detail.min.css">

<div class="list_details_section">
    <input type='hidden' value='<?=$list_id?>' id='actual-list'>
    <!-- Título -->
    <div class="row  <?php echo $color ?>-btm-border list-title">
      <div class="col-sm-12  <?php echo $mis_listas ?>">
        <h2 class=""><?php echo $list_name ." (". count($list_books) . ") "  ?></h2>
        <?php if ($data['owner'] && !$data['is_owner_and_guest_the_same']) { ?>
        <p>Lista creada por: 
          <a href="<?php echo $data['owner']['url']; ?>">
            <?php echo $data['owner']['fullname']; ?>
          </a>
        </p>
        <?php } ?>
        <?php 
        if( $list_type == 'dynamic' ):

          echo '<p class="sub-title">Lista Inteligente</p>';
          $params = Model::factory('List')->get_parms_for_dynamic_list( $list_id );

          echo '<p class="search-params">CRITERIOS DE BÚSQUEDA: ';
          foreach ($params as $key_par => $value) {
            if( $key_par != 'min' && $key_par != 'pagina' && $key_par != 'registros_por_pagina' && $key_par != 'attr' && $key_par != 'orden' && $key_par != 'orden_dir' && $key_par != 'origen' ){
              ?><span class="param_key"><?php echo ucfirst(LM::paramToSpanish($key_par)) ?>: </span> <span class="param_val">"<?php echo ($key_par == 'tipo_producto') ? ucwords( $tipos[$value] ) : ucwords($value) ?>"</span>&nbsp;&nbsp;<?php 
            }
          }
          echo '</p>';
        
        endif; 
        ?>
        <p class="list-description"><?php echo $list_descrip ?></p>
      </div>

        <div class="col-lg-12 col-xs-12 col-md-3 info" style='margin-bottom:10px;'>      
            <div class="pull-right social-share-row">
              <span class="share-list-link">Compartir 
              <img onclick='shareFB("<?=($data['list']->name)?>","<?=($data['list']->description)?>","http://librosmexico.mx/assets/images/libros_logo100x.png","http://librosmexico.mx/lista/<?=$data['list']->user?>/<?=$data['list']->slug?>")' src='assets/images/detail/facebook_cita.svg' class='social-medios-mini social-fb pointer' style="cursor:pointer!important;">
              <!--<img src='assets/images/detail/twitter_cita.svg' class='social-medios-mini social-tw pointer' onclick="shareTW('http://librosmexico.mx/lista/<?=$data["list"]->user?>/<?=$data["list"]->slug?>','<?=utf8_decode($data['list']->name)?>')" style="cursor:pointer!important;">-->
              <?php
                $titletw = 'Visita mi lista: "'.$data['list']->name.'" en @LibrosMexicoMX';
                echo '<a class="a2a_button_twitter" target="_blank" onclick="window.open(&#34http://www.addtoany.com/add_to/twitter?linkurl='.urlencode(URL::base(true)."lista/".$data['list']->user."/".$data['list']->slug).'&amp;linkname='.urlencode($titletw).'&amp;linknote=&#34,&#34_blank&#34,&#34top=200, left=200, width=450, height=500&#34)" rel="nofollow" aria-label="Twitter"><span class="a2a_svg a2a_s__default a2a_s_twitter" style="width: 18px; line-height: 18px; height: 18px; border-radius: 2px; background-size: 18px; background-image: url(\'assets/images/twitternew.svg\'); display: inline-block; cursor:pointer!important; vertical-align:middle;"></span></a>';
              ?>
            </span>
            </div>
            <div class="pull-right clear-sides">

              <?php if(isset($current_user))echo $like_label; echo $likes;?>
            </div>
          </div>
      
      <?php if( $current_user_id === $userid_list ): //Si no es una lista del usuario actual, no se puede configurar?>
      <div class="add-book  hidden-xs">
        <?php if( $list_type != 'dynamic' ): ?>
        <button class="<?php echo $color ?>-button" data-toggle="modal" data-target="#addBook">
          Agregar un libro
        </button>
        <?php endif; ?>
        <br>
        <?php if( !($slug == 'mi-biblioteca' OR 
        $slug == 'lo-estoy-leyendo' OR 
        $slug == 'ya-lo-lei' OR 
        $slug == 'lo-quiero-leer') ): ?>
        <button class="blue-button" data-toggle="modal" data-target="#configlist">
          Configurar lista
        </button>
        <?php endif; ?>
      </div>

      <?php endif; ?>
      <div class="col-xs-12 no-padding" style='margin-bottom:10px; margin-left:15px;'>      
          <div class="col-xs-6 no-padding">      
                <?php if(!$empty_ban && $has_keywords){?>
                <span class='tag-word'>Palabras Clave:
                  <?php foreach ($key as $keys){
                    if(strlen($keys)>2){?>
                      <a href='buscar/libro?keywords=<?=$keys?>' class='tags'><?=$keys?></a>
                    <?php } }?>
                   </span>
                <?php } ?>    
          </div> 
          <div class="col-xs-6 no-padding text-right">      
            
          </div>        
      </div>  
      <?php 
          if( $current_user )
          {
          ?>

          <div class='col-xs-12 no-padding book-information' style="margin-left:15px;">
            <div class="col-xs-12 col-md-3 rating no-padding block-com-opt" id='div-library-1'  >         
              <div class='btn-add-library add-library-desk text-center' id='btn-add-1'onMouseOver='addLibrary(1);' onMouseOut='addLibraryOut(1);'>
                <span class='add' id='button-title-1'>Comparar lista</span>
                <form class='add-list' id='list-mini-1' > 
                  <p class="comp-item-normal">¿Con cuál de tus listas quieres comparla?</p>
                  <?php 
                  $index=1;
                  foreach ($data['menu_lists'] as $list) {
                    if ($list->id == $list_id) {
                      continue;
                    }
                    ?>

                   <p id='p<?=$index?>'>
                    <input class='tick comp-item' id="check-<?=$index?>" type="checkbox"/> 
                      <label onclick='setChecked(<?=$index?>);clickIt("listas/comparar/<?=$list->id?>/<?=$list_id?>/en-comun/1/25")' id='l<?=$index?>'class='tick comp-item' for="check-<?=$index?>"></label><span class="comp-item" style='word-wrap: break-word;'><?=$list->name?></span>
                   </p> 
                 <?php $index++; }?>                          
              </form>             
              </div>
            </div>              

            <div class="col-xs-12 col-md-3 rating no-padding block-com-opt" id='div-library-2' >         
              <div class='btn-add-library add-library-desk text-center' id='btn-add-2' onMouseOver='addLibrary(2);' onMouseOut='addLibraryOut(2);'>
                <span class='add' id='button-title-2'>Agregar libros a otra lista</span>
                <form class='add-list' id='list-mini-2' >
                  <p class="comp-item-normal">¿A cuál de tus listas quieres agregar estos libros?</p>
                  <?php 
                  foreach ($data['menu_lists'] as $list) {
                    if ($list->id == $list_id) {
                      continue;
                    }
                    ?>
                   <p id='p<?=$index?>'>
                    <input  class='tick comp-item' id="check-<?=$index?>" type="checkbox"/> 
                      <label onclick='setChecked(<?=$index?>);appendList(<?=$list->id?>,<?=$list_id?>)' id='l<?=$index?>'class='tick comp-item' for="check-<?=$index?>"></label><span class="comp-item" style='word-wrap: break-word;'><?=$list->name?></span>
                   </p> 
                 <?php $index++; }?>   
              </form>             
              </div>
            </div> 

            <div class="col-xs-12 col-md-3 rating no-padding block-com-opt" id='div-library-3' onMouseOver='addLibrary(3);' onMouseOut='addLibraryOut(3);' >         
              <div class='btn-add-library add-library-desk text-center' id='btn-add-3'>
                <span class='add' id='button-title-3'>Fusionar listas</span>
                <form class='add-list' id='list-mini-3' >
                  <p class="comp-item-normal">¿Con cuál de tus listas quieres fusionarla?</p>
                  <?php 
                  foreach ($data['menu_lists'] as $list) {
                    if ($list->id == $list_id) {
                      continue;
                    }
                    ?>
                   <p id='p<?=$index?>'>
                    <input  class='tick comp-item' id="check-<?=$index?>" type="checkbox"/> 
                      <label onclick='setChecked(<?=$index?>);copyModal(<?=$list->id?>,"fuze")' id='l<?=$index?>'class='tick comp-item' for="check-<?=$index?>"></label><span class="comp-item" style='word-wrap: break-word;'><?=$list->name?></span>
                   </p> 
                 <?php $index++; }?>     
              </form>             
              </div>
            </div> 

            <div class="col-xs-12 col-md-3 rating no-padding pointer block-com-opt" style='text-align:right;' id='div-library-4' >         
              <div class='btn-add-library add-library-desk text-center' id='btn-add-4' onclick='copyModal(<?=$list_id?>,"copy")'>
                <span class='add' >Copiar lista</span>           
              </div>
            </div>   

          </div>

          <?php
          }
          ?>
    </div><!-- end row list-title-->
    

    <?php
    if( count($list_books) === 0 ):
      echo '<div class="list-msg">Aún no has agregado libros a esta lista</div>';
    else: 
    ?>
      <div class="row  list-search">
        <div class="col-sm-12">
          <form class="form-inline">
            <input type="text" class="form-control" id="search_book" name="search_book" placeholder="Busca un libro en la lista...">
            <button type="submit" class="btn"></button>
          </form>
        </div>
      </div><!-- end row -->
    
      <!-- Ordenar Libros MD -->
      <div class="row  list-order hidden-xs">
        <div class="col-sm-10">
          <form class="form-inline">
            <div class="form-group">
              <label for="list_order">Ordenar por:</label>
              <div class="select-list-wrapper">
                <select class="form-control" name="list_order" id="list_order">
                  <option value="num">Seleccione</option>
                  <option value="popular">Más populares</option>
                  <option value="recent">Más recientes</option>
                  <option value="title">Título (A-Z)</option>
                  <option value="autor">Autor (A-Z)</option>
                  <option value="editorial">Editorial (A-Z)</option>
                </select>
              </div>
              
            </div>
            <div class="form-group">
              <label for="list_show">Mostrar:</label>
              <div class="select-list-wrapper">
                <select class="form-control" name="list_show" id="list_show">
                  <option value="5">5 Libros</option>
                  <?php if( count($list_books) > 10 ): ?>
                    <option value="10">10 Libros</option>
                  <?php endif; ?>
                  <?php if( count($list_books) > 20 ): ?>
                    <option value="20">20 Libros</option>
                  <?php endif; ?>
                  <?php if( count($list_books) > 30 ): ?>
                    <option value="30">30 Libros</option>
                  <?php endif; ?>
                  
                  <option value="-1">Todos</option>
                </select>
              </div>
              
            </div>
          </form>
        </div>
        <div class="col-sm-2  aling-r">
          <span id="info_pag">1-10 de 130</span>
        </div>
      </div><!-- end row  list-order-->

      <!-- Ordenar Libros XS -->
      <div class="row list-order-xs visible-xs">
        <div class="col-xs-12">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Ordenar por <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                  </a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <form>
                    <div class="form-group">
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-order" id="book-order-radio1" value="popular" checked>
                          Más populares
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-order" id="book-order-radio2" value="recent">
                          Más recientes
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-order" id="book-order-radio3" value="title">
                          Título (A-Z)
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-order" id="book-order-radio4" value="autor">
                          Autor (A-Z)
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-order" id="book-order-radio4" value="editorial">
                          Editorial (A-Z)
                        </label>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Mostrar <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                  <form>
                    <div class="form-group">
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-show" id="book-show-radio1" value="5" checked>
                          5 Libros
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-show" id="book-show-radio2" value="10">
                          10 Libros
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-show" id="book-show-radio3" value="20">
                          20 Libros
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-show" id="book-show-radio3" value="30">
                          30 Libros
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-show" id="book-show-radio3" value="-1">
                          Todos
                        </label>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12">
          <span id="info_pag_xs">1-10 de 130</span>
        </div>
      </div><!-- end row  list-order list-order-xs-->
    
      <table id="table_list" class="table_list">
        <thead>
          <tr>
            <th>num</th>
            <th>img</th>
            <th>title</th>
            <th>editorial</th>
            <th>rating</th>
            <th>del</th>
            <th>autor</th>
            <th>fecha agregado</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($list_books as $key => $book) 
          {  
            if($book['imagen']==NULL)
              $img = 'assets/images/generic_book.jpg';
            else
              $img = $book['imagen'];

            //---Rating

            //$rating = rand(0, 5);
            $rating = Model::factory('List')->get_book_rating( $book['id_libro'] );
            $amarilla = '<img src="assets/images/detail/estrella_amarilla_xm.svg">';
            $gris = '<img src="assets/images/detail/estrella_gris_xm.svg">';
            
            $stars = '';
            $stars .=  ( $rating >= 1 ) ? $amarilla : $gris;
            $stars .= ( $rating >= 2 ) ? $amarilla : $gris;
            $stars .= ( $rating >= 3 ) ? $amarilla : $gris;
            $stars .= ( $rating >= 4 ) ? $amarilla : $gris;
            $stars .= ( $rating >= 5 ) ? $amarilla : $gris;
            $stars .= '<br><span class="hide">'.$rating.'</span>';

            $is_new_book = Model::factory('List')->is_new_book_in_list( $book['id_libro'], $list_id );

          ?>
          <tr class="each_list">
            <td class="td_num  hide">
              <span class="num_lis  hidden-xs"><?php echo  $key+1 ?></span>
            </td>
            <td class="td_img">
              <a href="<?php echo url::base(). 'libros/' . $book['id_libro'] ?>">
                <img src="<?php echo $img ?>">
              </a>
            </td>
            <td class="td_title">
              <p class="book_title"><a href="<?php echo url::base(). 'libros/' . $book['id_libro'] ?>"><?php echo $book['titulo'] ?></a></p>
              <p class="book_autor"><?php echo $book['autores'][1][0]['nombre'] ?></p>
              <p class="visible-xs"><?php echo $book['editorial'][0]['nombre'] ?></p>
              <p class="visible-xs" id="rating_xs"><?php echo $stars; ?></p>
              <p class="book_new"><?php echo ($is_new_book) ? 'Nuevo en esta lista' : '' ?></p>
            </td>
            <td class="td_editorial">
              <?php foreach ($book['editorial'] as $editorial): ?>
              <p class="book_editorial">
                <?php echo $editorial['nombre']; ?>
              </p>
              <?php endforeach ?>
            </td>
            <td class="td_rating">
              <div class="list-rating" id="rating_sm">
                <?php echo $stars; ?>
              </div>
            </td>
            <td class="td_close">
              <?php if( $current_user_id === $userid_list && $list_type != 'dynamic'): ?>
              <div class="list-del">
                <button type="button" data-toggle="modal" data-target="#delBook" data-bookid="<?php echo $book['id_libro'] ?>" data-userid="<?php echo $current_user_id ?>"  data-listslug="<?php echo $slug ?>" data-book-name="<?php echo $book['titulo'] ?>"></button>
              </div>
              <?php endif; ?>
            </td>
            <td class="hide">
              <span class="hide"><?php echo $book['autores'][1][0]['nombre'] ?></span>
            </td>
            <td class="hide">
              <span class="hide"><?php echo Model::factory('List')->get_added_date_book( $list_id, $book['id_libro'] ); ?></span>
            </td>
          </tr>
            <?php Model::factory('List')->its_no_longer_a_new_book( $book['id_libro'], $list_id ); ?>
          <?php } //end foreach ?>
        </tbody>
      </table>
    
    <?php endif; ?>

</div>

<div class="div-add-book-xs  visible-xs">
  <button type="button" class="btn add-book-xs" data-toggle="modal" data-target="#addBook">Agregar un libro</button>
  <?php if( !($slug == 'mi-biblioteca' OR 
    $slug == 'lo-estoy-leyendo' OR 
    $slug == 'ya-lo-lei' OR
    $slug == 'lo-quiero-leer') ): ?>
    <button type="button" class="btn config-list-xs" data-toggle="modal" data-target="#configlist">Configurar lista</button>
  <?php endif; ?>
</div>

<!-- Modal Agregar Libros -->
<div class="modal fade" id="addBook" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<!-- div class="row" -->
  <div class="modal-dialog" role="document"> <!-- col-xs-11 col-md-8 center-modal  -->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar un libro</h4>
      </div>
      <div class="modal-body no-padding-sides">
        <div class="ajax-loader">
          <img src="assets/images/ajax-loader.gif">
        </div>
        <div class="row">
          <div class="col-xs-12">
            <form class="form-inline">
              <div class="form-group  div_add_book_search">
                <label class="sr-only" for="add_book_search">Busca un libro...</label>
                <input type="text" class="form-control" id="add_book_search" name="add_book_search" placeholder="Busca un libro...">
              </div>
              <div class="form-group  div_add_book_select">
                <select class="form-control" id="search_param_add_book">
                  <option value="cadena">Todo</option>
                  <option value="titulo">Título</option>
                  <option value="autores">Autor</option>
                  <option value="editorial">Editorial</option>
                  <option value="isbn">ISBN</option>
                  <option value="keywords">Palabras Clave</option>
                </select>
              </div>
              <div class="btn btn-search-book" id="btn_add_book_search"></div>
            </form>
          </div>
        </div>
        <form action="" method="POST" id="form_search_result" onsubmit="return validateFormAddBook();">
          <div id="search_result">
            <!-- Ajax -->
          </div>
        </form>
      </div>
      
      <div class="modal-footer no-padding-sides">
        <div id="total-results" style="text-align: left; font-size: 14px;"></div>
        <span class="float-left"><a href="#" id="see_more_results">Ver más resultados</a></span>
        <span class="float-left  list-msg" id="no-results" style="font-size: 14px;">No hay resultados para el criterio de búsqueda</span>
        <button type="submit" class="add_book_list btn-blue" id="add_book_list" form="form_search_result" name="add_books">Agregar</button>
      </div>
    </div>
  </div>
<!-- /div -->
</div>

<!-- Modal Eliminar Libros -->
<div class="modal fade" id="delBook">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Eliminar libro</h4>
      </div>
      <div class="modal-body">
        <p>¿Estás seguro de que quieres eliminar el libro "<span class="del-book-title">book-title</span>" de la lista "<?php echo $list_name ?>"?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn del-book-ok">Borrar</button>
        <button type="button" class="btn del-book-cancel" data-dismiss="modal">Cancelar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal Configurar Lista -->
<div class="modal fade" id="configlist">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Configurar lista</h4>
      </div>
      <div class="modal-body">
        <form action="" method="POST" id="form-update-list">
          <div class="form-group">
            <label for="list-name">Nombre</label>
            <input type="text" class="form-control modal-input" id="list-name" name="list-name" placeholder="Agrega un nombre a tu lista..." value="<?php echo $list_name ?>">
          </div>
          <div class="form-group">
            <label for="list-description">Descripción</label>
            <textarea class="form-control modal-input" rows="3" id="list-description" name="list-description" placeholder="Agrega una breve descripción..."><?php echo $list_descrip ?></textarea>
          </div>
          <div class="form-group">
            <label for="list-keywords">Palabras clave</label>
            <textarea class="form-control modal-input" rows="3" id="list-keywords" name="list-keywords" placeholder="Escribe una o más palabras clave..."><?php echo $list_keywords ?></textarea>
          </div>
          <div class="form-group">
            <label for="list-privacy">¿Quién puede ver mi lista?</label>
            <select class="form-control modal-input" id="list-privacy" name="list-privacy">
              <option value="public" <?php echo ($list_privacy == 'public') ? 'selected="selected"' : ''; ?> >Todos</option>
              <option value="private" <?php echo ($list_privacy == 'private') ? 'selected="selected"' : ''; ?>>Sólo yo</option>
            </select>
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn" name="submit-update-list" form="form-update-list">Actualizar lista</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal Copy List-->
<div class="modal fade" id="modal-copy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div id='create-list-dialog' class="modal-dialog" role="document">
    <div id="create-list-content" class="modal-content">
      <div class="modal-header" id='create-list-header'>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Copiar en una nueva lista</h4>
      </div>
      <div class="modal-body">
          <p class='text-label'>Nombre</p>
          <input type='text' id='name-list-c' class='modal-input-text no-margin-bottom' placeholder='Agrega un nombre a tu lista...'>
          <p class='list-error' id='name-list-error'>Escribe un nombre válido mayor a 3 caratéres</p>
          <div class='margin-butt'></div>         
          <p class='text-label'>Descripción</p>
          <textarea class='modal-input-textarea' id='description-list' placeholder='Agrega una breve descripción...'></textarea> 
          <p class='text-label no-margin-bottom'>Palabras clave</p>                   
          <p class='text-mini-label' >Separa cada palabra con una coma.</p>
          <textarea id='keywords-list' class='modal-input-textarea no-margin-bottom' placeholder='Escribe una o más palabras clave...'></textarea>
          <p class='text-mini-label' >Las palabras clave deben tener 5 o más caracteres.</p>           
          <p class='list-error'id='keywords-list-error'>Escribe una palabra clave</p>
          <div class='margin-butt'></div>  
          <p class='text-label'>¿Quién puede ver mi lista?</p>
          <select id='privacy-list' class='modal-input-select'>
            <option value='public'>Todos</option>
            <option value='private'>Sólo yo</option>
          </select>
        <div id='create-button' class='modal-buttons-create'>
          <button class='btn btn-modal-create' onclick='copyList()'>Crear Lista</button>
        </div>  
      </div>
    </div>
  </div>
</div>

<!-- Modal Fuze List-->
<div class="modal fade" id="modal-fuze" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div id='create-list-dialog' class="modal-dialog" role="document">
    <div id="create-list-content" class="modal-content">
      <div class="modal-header" id='create-list-header'>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Fusionar en una nueva lista</h4>
      </div>
      <div class="modal-body">
          <p class='text-label'>Nombre</p>
          <input type='text' id='name-list-f' class='modal-input-text no-margin-bottom' placeholder='Agrega un nombre a tu lista...'>
          <p class='list-error' id='name-list-error-f'>Escribe un nombre válido mayor a 3 caractéres</p>
          <div class='margin-butt'></div>         
          <p class='text-label'>Descripción</p>
          <textarea class='modal-input-textarea' id='description-list-f' placeholder='Agrega una breve descripción...'></textarea> 
          <p class='text-label no-margin-bottom'>Palabras clave</p>                   
          <p class='text-mini-label' >Separa cada palabra con una coma.</p>
          <textarea id='keywords-list-f' class='modal-input-textarea no-margin-bottom' placeholder='Escribe una o más palabras clave...'></textarea>
          <p class='text-mini-label' >Las palabras clave deben tener 5 o más caracteres.</p> 
          <div class='margin-butt'></div>  
          <p class='text-label'>¿Quién puede ver mi lista?</p>
          <select id='privacy-list-f' class='modal-input-select'>
            <option value='public'>Todos</option>
            <option value='private'>Sólo yo</option>
          </select>
        <div id='create-button' class='modal-buttons-create'>
          <button class='btn btn-modal-create' onclick='fuzeList("-f")'>Crear Lista</button>
        </div>  
      </div>
    </div>
  </div>
</div>

<script>

function listValidation(end){
  var ban=0;
  var field = $('#name-list'+end).val();
  if(field.length<3){
    $('#name-list'+end).css("border","1px solid #cc4242");
    $('#name-list-error'+end).show();
    ban++;
  }
  else{
    $('#name-list'+end).css("border","1px solid #354f5e");    
    $('#name-list-error'+end).hide(); 
  }
  if(ban!=0)
    return false;
  else
    return true;
}

$(document).ready(function(){
  
  $('span.comp-item').click(function(){
    $(this).closest('p').children('label').trigger('click');
  });


  $('#btn-add-1').click(function(){
    var windowWidth = $(window).width();
    if( windowWidth <= 480 )
    {
      //alert();
      //se da clic al botón de comparar lista en versión móvil
      //addLibrary(1);
    }
  });

  



});

function addLibrary(id){
  $('.block-com-opt').children('.btn-add-library').attr('style', '');
  $( "#btn-add-"+id ).animate({
    borderRadius: "10px",
    backgroundColor:"#354f5e",
    color: "#3db29c",
    zIndex: "99",
    position: "absolute"
    }, 0);
  $( "#btn-add-"+id ).css('position','absolute');
  $("#button-title-"+id).hide();
  $("#list-mini-"+id).show();
}

function addLibraryOut(id){
  $( "#btn-add-"+id ).animate({
    padding:"8px 10px 8px 10px",
    borderRadius: "30px",
    backgroundColor: "#FFF",
    borderColor: "#354f5e",
    border:"1px solid #354f5e",   
    color: "#354f5e",
    width: "175px"
    }, 0);

  $("#button-title-"+id).show();
  $("#list-mini-"+id).hide();
}
function setChecked(id){
  if ($("#check-"+id).is(":checked"))   
    $("#p"+id).removeClass("check-it");
  else 
    $("#p"+id).addClass("check-it");
}
function clickIt(url){
  location.href=url;
}
function appendList(mine,their){
  $.ajax({
      url: "appendList/"+mine+"/"+their,
      method: "POST"
      }).done(function(){
      setNotify('Se agregaron los libros a tu lista.', exito);
    }).fail(function() { setNotify('Hubo un problema al agregar los libros, intenta más tarde.', error);
  }); 
}
function copyModal(id,modal){
  $("#modal-"+modal).modal("show");
  $("#modal-"+modal).data("lista",id);
}
function fuzeList(id){
  if(listValidation(id)){
  var myId = $("#modal-fuze").data('lista');
  var theirId = $("#actual-list").val();
  $(".ajax-loader-login").html('<img src="assets/images/ajax-loader-t.gif" width="100">');
  $('.ajax-loader-login').show();
      $.ajax({
              type: "POST",
              url: "appendNew/"+myId+"/"+theirId,
              data:{
                    name:$('#name-list-f').val(),
                    description: $('#description-list-f').val(),
                    keywords:$('#keywords-list-f').val(),
                    privacy: $('#privacy-list-f option:selected').val()
              }
                                 
          }).done(function(){
              setNotify('Se fusionaron correctamente tus listas.', exito);
              $('.ajax-loader-login').hide();                       
          }).fail(function(){
              setNotify('Ocurrió un error al fusionar las listas', error); 
              $('.ajax-loader-login').hide();                      
          });
          $("#modal-fuze").modal('hide'); 
  } 
}
function copyList(id){
  if(listValidation('-c')){
  var lisToCopy = $("#modal-copy").data('lista');
      $.ajax({
              type: "POST",
              url: "copyList/"+lisToCopy,
              data:{
                    list: lisToCopy,
                    name:$('#name-list-c').val(),
                    description: $('#description-list').val(),
                    keywords:$('#keywords-list').val(),
                    privacy: $('#privacy-list option:selected').val()
              }
                            
          }).done(function(){
              setNotify('Se copió correctamente la lista', exito);                            
          }).fail(function(){
              setNotify('Ocurrió un error al copiar la listas', error);                       
          });
        $("#modal-copy").modal('hide');
  } 
}
</script>
