<!-- Importar my_friends.css para obtener la opción de agregar a amigos -->
<link rel="stylesheet" href="assets/styles/list_details.css">
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/custom_f.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/custom_r.css"/>
<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/scripts/custom_r.js"></script>
<div class="library-content">
	<div class="lists-panel col-md-4  hidden-sm hidden-xs">
		<?php
		include  DOCROOT . 'application/views/frontend/library/library_menu_unsession.php';
		?>
	</div>
	<div class="list-panel col-md-8">
		<?php 
		include 'soloyotengo_books_detail.php';
		?>
	</div>
	<div class="clear"></div>
</div>