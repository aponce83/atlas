
<div class="dos">
<section>
	<div class="busqueda">
	<article>
	 <h2>Catálogo Gastronómico</h2>		
	    <div class="wrapp-list clear book" id="first-container">
	        <?php foreach($data['books'] as $books){?>
	                <div class="elementos book">
	                    <div class="left">
	                        <figure><img src="<?=filter_var($books['imagen'], FILTER_VALIDATE_URL) ? $books['imagen'] : 'assets/images/generic_book.jpg'?>"></figure>
	                    </div>
	                    <div class="titulosel">
	                        <h4><?=Text::limit_chars($books['titulo'], 90, '...')?></h4>
	                        <p><?=$books['autores'][0]['nombre']?></p>
	                        <?php if($books['tipo_producto']==10||$books['tipo_producto']==30){ ?><img class="bookicon right" src="assets/images/<?=$books['tipo'] == 10?'e-icon.png':'i-icon.png'?>" /><?php } ?>
	                    </div>
	                    <div class="enlacesbr">
	                        <a href="libros/<?=$books['id']?>">Ver ficha</a>
	                        <a class="want_to_read <?=$books['library'] && $books['library']['want_to_read']==1?'active':''?>" data-id="<?=$books['id']?>" href="#top">Agregar a mi biblioteca</a>
	                    </div>
	                </div>
	        <?php } ?>
	    </div>
			<div class="counter book">
			</div>    
	   </article> 
	</div>    
</section>
</div>