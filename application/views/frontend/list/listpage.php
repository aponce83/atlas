
<style>

.pag-block-actv {
	color:#3db29c!important;
}

@media screen and (max-width: 479px) {

    #lists-pagination {
		display: block !important;
		float: none;
		text-align: center;
	}

	.page-book-last {
		display: none;
	}

	.page-book-first {
		display: none;
	}

	.list-pagination-item {
		font-size: 18px !important;
	}

	.no-pag-respons {
		display: none;
	}

	.block-pag-mbl {
		border: 1px solid #ccc;
	}

	.pag-block-actv {
		background-color: #3db29c;
		color:#fff!important;
	}

	.pag-block-norm {
		background-color: #fff;
		color: #888 !important;
	}

  }
	
</style>


<section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cont">
	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cont2" style="margin-top: 50px;">
			<h1>LISTAS</h1>
			<section class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padd" style="margin-bottom:20px;">
				<div class="buscar">
					<input 
						type="text" 
						class="buscador" 
						name="search" 
						id="list-search" 
						placeholder="Busca el nombre de la lista"></input>
					<div class="img-responsive lupa" id="search-list-btn">
						<img src="assets/images/icono_buscar.svg" height="30px" width="30px">
					</div>
        </div>
			</section>
			<section class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div>
	      <form class="form-inline" style="margin:0!important;">
	        <div class="form-group">
	          <label for="list_order">Ordenar por:</label>
	          <select class="form-control" name="list_order" id="list_order" style="background-image: url('assets/images/backend/bullet_arrow_down.png'); background-repeat: no-repeat; background-position: 96% center;">
	            <option selected disabled>Seleccione</option>
	            <option value="likes DESC">Más populares</option>
	            <option value="created_on DESC">Más recientes</option>
	            <option value="name ASC">Nombre (A-Z)</option>
	            <option value="name DESC">Nombre (Z-A)</option>
	          </select>
	        </div>
	        <div class="form-group">
	          <label for="list_show" style="padding-left: 20px!important;">Mostrar:</label>
	          <select class="form-control" name="list_show" id="list_show" style="background-image: url('assets/images/backend/bullet_arrow_down.png'); background-repeat: no-repeat; background-position: 96% center;">
	          	<option selected disabled>Seleccione</option>
	            <option value="10">10 Listas</option>
	            <option value="20">20 Listas</option>
	            <option value="50">50 Listas</option>
	          </select>
	        </div>
      </form>
    </div>


	</section>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padd vis-ind">
			<div class="col-md-12 col-sm-12 col-xs-12 indicador2 no-padd">
     			<span class="info_pag"> <?=$data['list_min']?>-<?=$data['list_max']?> de <?=number_format($data['list_count'],0,'',',')?> | </span>
     			<span class="info_pag2">página <?=$data['page']?> de <?=$data['max_pages']?></span>
    		</div>
		</div>
		</div>
</section>

<section>
	<div class="col-xs-12 no-padd vis-ind2">
		<div class="col-xs-12 indicador2 no-padd">
			<span class="info_pag"><?=$data['list_min']?>-<?=$data['list_max']?> de <?=number_format($data['list_count'],0,'',',')?> |</span>
			<span class="info_pag2">página <?=$data['pages']?> de <?=$data['max_pages']?></span>
		</div>		
	</div>
</section>


<!-- END Ordenar por y Mostrar xs -->




<div class="row no-margin" id="main-lists-container">

	<?php 
		$class[0]='';
		$class[1]='no-margin-right';

		$class[2]='classic-list';
		$class[3]='dynamic-list';
		$class[4]='genius-list';

		$class[5]='flecha_left_verde.svg';
		$class[6]='flecha_naranja.svg';
		$class[7]='flecha_azul.svg';

		$class[8] ='default';
		$class[9] ='myList';
		$class[10]='intelligent';	

		$class[11]='hidden-list';
		$classic_counter=0;
		$dynamic_counter=0;

		$counter = 1;

		$delete = "<img src='assets/images/detail/cerrar.svg' class='delete-list' onClick='deleteListModal(";

	  foreach ($data['history_likes'] as $likes){
	      $like[]=$likes['list_like'];
	  }
		foreach ($data['list'] as $list){
				   if($list->type=='classic')
					{
						$addClass=$class[4];
						$im=$class[7];
						$rowClass=$class[10];
						
					}
					else{
							$hidden = $class[11];	
							$addClass=$class[0];
							$im=$class[0];
							$rowClass=$class[0];	
							}										

			  	$list_id=$list->id;
				$user = Session::instance()->get('user_front');
  
				$user_profile='lector/'.base64_encode($list->user).'/circulo';
			
			if($list->likes!=0){
			    if(isset($user['id']))
			       $likes = "<span class='like-count-".$list_id." gris' data-count='".$list->likes."'> | ".$list->likes." me gusta</span>";
			    else
			       $likes = "<span class='like-count-".$list_id." gris' like-count-".$list->likes."' data-count='".$list->likes."'> ".$list->likes." me gusta</span>";      
			}
  			else
   				$likes = "<span class='like-count-".$list_id." gris' data-count='0'> </span>";	

			  if($data['history_likes']){
			    if(in_array($list_id,$like))
			      $like_label = "<span class='pointer btn-like list-like-".$list_id."' onClick='likeItList(".$list_id.")' data-like='1'>Ya no me gusta </span>";
			    else
			      $like_label = "<span class='pointer btn-like list-like-".$list_id."' onClick='likeItList(".$list_id.")' data-like='0'>Me gusta </span>";
			  }
			?>
			<?php if ($list->num_books>0) { ?>

			<div class="col-xs-12 cont" data-rating="<?=$list->likes.''?>" data-date="<?=$list->created_on.''?>" data-name="<?=$list->name?>" style="">
				<div class='cont2 list-info-container no-padding col-xs-12 <?=$rowClass?> <?=$list->id?> <?=$hidden?>' data-listname='<?=$list->name?>'>
					<section class='section-list-title col-md-4 col-xs-12'>
						<div class="col-xs-12 no-padding">						
							<a href="lista/<?=$list->user?>/<?=$list->slug?>" class="list-section-title"><?=ucwords($list->name)?> (<?=$list->num_books?>)</a>
						</div>
						<div class="col-xs-12 no-padding author-list">						
							por: <a href='<?=$user_profile?>' class='green-link'><?=$list->username?></a>
						</div>						
						<div class="col-lg-12 col-md-12 desc">
							<p class="description no-padd"><?php echo $list->description; ?></p>
						</div>
					</section>	

					<section class='col-lg-8 col-md-8 book-container-list'>
						
							<div class="row">
								<div class="col-lg-8 col-md-8 fix-5px-sides list-wrapper">
									<?php 
										$counter = 1;	     	
										$aux_break = 0;
								     	foreach($list->books as $item)      
								     	{
								     		if(!$item['imagen'])
								     			$ruta="assets/images/generic_book.jpg";
								     		else 
								     			$ruta=$item['imagen'];

								     		if($counter > 4 ){ ?>
								     			<div class="item-list-wrp remove-mobile">
								     				<a href="libros/<?php echo $item['id_libro']; ?>">
								     					<img src="<?=$ruta?>" class='mini-list-book'>
								     				</a>
								     			</div>	     			
								     		<?php  }
								     		else{ ?>
								     			<div class="item-list-wrp">
								     				<a href="libros/<?php echo $item['id_libro']; ?>">
								     					<img src="<?=$ruta?>" class='mini-list-book'>
								     				</a>
									     		</div>	 
									     		<?php $counter++;
									     	}

									    if (++$aux_break == 7) break;

									    }
								     	?>
								</div>
								
							</div>
						
				     		


					</section>	

					<?php echo "<div class='section-see-list genius-list col-xs-12'>";?>
					<div class="col-xs-6 text-left no-padding"><?php
					if(isset($user['id'])) 
						echo $like_label; ?><?=$likes?></div>
					<div class="col-xs-6 text-right no-padding">
					<a class='text-right' href="lista/<?=$list->user?>/<?=$list->slug?>">ver lista <img src= 'assets/images/flecha-azul-d.svg' class='arrow_list'></a>
					</div>	
				</div>	
			</div>	



			</div>

		<?php };?>

	<?php };?>

</div>

<?php

	$paginationLinks = "";
	$nPages = $data['max_pages'];

	if ($data['page']>1) {
		$paginationLinks .= "<li class='list-pagination-item'><a class='page-book-first' href='javascript:void(0)' ><i class='fa fa-angle-left fa-lg'></i><i class='fa fa-angle-left fa-lg'></i></a></li>";
		$paginationLinks .= "<li class='list-pagination-item'><a class='page-book-prev' href='javascript:void(0)' ><i class='fa fa-angle-left'></i> <span class='no-pag-respons'>anterior</span></a><span class='no-pag-respons'>|</span></li>";
	}

	for ($i = $data['page']-2; $i<$data['page']+2; $i++) {
		if ($i>=1 && $i<=$nPages) {
			if ($data['page']==$i)
				$paginationLinks .= "<li class='list-pagination-item'><a class='page-book pag-block-actv block-pag-mbl pag-number-".$i."' href='javascript:void(0)' data-id=''>".$i."</a><span class='no-pag-respons'>|</span></li>";
			else
				$paginationLinks .= "<li class='list-pagination-item'><a class='page-book pag-block-norm block-pag-mbl pag-number-".$i."' href='javascript:void(0)' data-id=''>".$i."</a><span class='no-pag-respons'>|</span></li>";		
		}
	}
		
	if ($data['page']<$nPages) {
		$paginationLinks .= "<li class='list-pagination-item'><a class='page-book-next' href='javascript:void(0)' ><span class='no-pag-respons'>siguiente</span> <i class='fa fa-angle-right'></i></a></li>";
		$paginationLinks .= "<li class='list-pagination-item'><a class='page-book-last' href='javascript:void(0)' ><i class='fa fa-angle-right fa-lg'></i><i class='fa fa-angle-right fa-lg'></i></a></li>";
	}

?>

<?php if ($data['max_pages']>1) { ?>
	
	<section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cont">
		<!-- Paginación -->
			<div class="col-lg-12 col-md-12 col-sm-12 cont2">
				<div class="col-md-12 col-sm-12 col-xs-12 margen-arriba">
					<div class="dataTables_paginate" id="lists-pagination">
						<?=$paginationLinks?>	
					</div>
				</div>
			</div>		
	</section>

	<section class="col-xs-12 pag-marg">
							

	</section>
<?php } ?>
<div id="spinner2"></div> 
<div id="spinner"></div> 