<link rel="stylesheet" type="text/css" href="assets/styles/comparing_styles.css">
<div class="row search_data">
	<div class="col-sm-12 col-xs-11">
		<h2>Resultados de la comparación de listas:</h2>
		<h3 class="common-subtitle"><span class="list-name"> <a href="lista/<?php echo $data['owner']['id']; ?>/<?php echo $data['other_slug']; ?>"><?php echo $data['other_list']; ?></a> </span> de <span class="owner-name"> <a href="lector/<?php echo base64_encode($data['owner']['id']); ?>/circulo"><?php echo $data['owner']['fullname']; ?></a></span> con tu lista <span class="list-name"> <a href="lista/<?php echo $data['myId']; ?>/<?php echo $data['mine_slug']; ?>"><?php echo $data['mine_list']; ?></a></span></h3>
	</div>
</div>

<div class="top-comparing-links">
	<div class="comparing-block f-block">¿Qué deseas ver?</div>
	<a href="listas/comparar/<?php echo $data['id_list_mine']; ?>/<?php echo $data['id_list_other']; ?>/en-comun/1/25">
		<div class="comparing-block block-link b-left active">Libros en común</div>
	</a>
	<a href="listas/comparar/<?php echo $data['id_list_mine']; ?>/<?php echo $data['id_list_other']; ?>/no-tengo/1/25">
		<div class="comparing-block block-link b-center">Libros que sólo tiene la otra lista</div>
	</a>
	<a href="listas/comparar/<?php echo $data['id_list_mine']; ?>/<?php echo $data['id_list_other']; ?>/solo-yo-tengo/1/25">
		<div class="comparing-block block-link b-right">Libros que sólo tiene mi lista</div>
	</a>
</div>

<h4 class="common-b">Libros en común
	<!-- <a class="result-to-list-btn" href="#">Convertir en lista</a> -->
</h4>


<?php
if($data['total_records']!=0)
{
?>

<div class="row adj-flt">
	<div class="col-xs-8 col-sm-10">
		<form class="form-inline">
			<div class="form-group">
				<label class="show-lbl" for="list_show">Mostrar:</label>
				<div class="select-list-wrapper">
					<select class="form-control list-show" name="list_show" id="list_show" onchange="location = this.options[this.selectedIndex].value;">
	                	<option <?php if($data['res_per_page']==25) echo 'selected'; ?> value="listas/comparar/<?php echo $data['id_list_mine']; ?>/<?php echo $data['id_list_other']; ?>/en-comun/1/25">25 Libros</option>  
	                	<option <?php if($data['res_per_page']==50) echo 'selected'; ?> value="listas/comparar/<?php echo $data['id_list_mine']; ?>/<?php echo $data['id_list_other']; ?>/en-comun/1/50">50 Libros</option>  
	                	<option <?php if($data['res_per_page']==75) echo 'selected'; ?> value="listas/comparar/<?php echo $data['id_list_mine']; ?>/<?php echo $data['id_list_other']; ?>/en-comun/1/75">75 Libros</option>
	                	<option <?php if($data['res_per_page']==100) echo 'selected'; ?> value="listas/comparar/<?php echo $data['id_list_mine']; ?>/<?php echo $data['id_list_other']; ?>/en-comun/1/100">100 Libros</option>   
                	</select>
				</div>
			</div>
		</form>
	</div>
	<div class="col-xs-4 col-sm-2 align-r inf-pag">
		<?php
		$start  =  0;
		if($data['current_pag']==1)
			$start = 1;
		else
			$start = (($data['current_pag'] - 1) * $data['res_per_page']) + 1 ;


		$end 	=  0;
		if($data['current_pag'] * $data['res_per_page'] > $data['total_records'] )
			$end = $data['total_records'];
		else
			$end = $data['current_pag'] * $data['res_per_page'];
		?>
		<span id="inf-pag"><?php echo $start; ?> - <?php echo $end; ?> de <?php echo $data['total_records']; ?></span>
	</div>
</div>

<?php
}
else
{
?>

	<div class="row adj-flt">
		<div class="col-xs-12">
			<label class="no-results-lbl">No hay resultados</label>
		</div>
	</div>

<?php
}
?>






<br>

<div class="adj-flt">

	<?php
	foreach( $data['book_info'] as $book )
	{
	?>

	<div class="item-book">
		<div class="col-sm-6">
			<div class="img-book">
				<a href="libros/<?php echo $book['id_libro']; ?>">
					<img src="<?php echo $book['imagen']; ?>">
				</a>
			</div>
			<div class="inf-book">
				<a href="libros/<?php echo $book['id_libro']; ?>">
					<p class="title"><?php echo $book['titulo']; ?></p>
				</a>
				<p class="author"><?php echo $book['autores'][1][0]['nombre']; ?></p>
			</div>
		</div>
		<div class="col-sm-6 hidden-xs">
			<?php foreach($book['editorial'] as $editorial  )
			{ ?>
				<p class="editor"><?php echo $editorial['nombre']; ?></p>
			<?php } ?>
			
		</div>
	</div>

	<?php
	}	
	?>

	

</div>

	<div class="col-xs-12 no-padding-sides pagination-links">
		<?php 
		$total_pages = $data['total_pages'];
		$current_pag = $data['current_pag'];
		if( $total_pages>1 )
		{
			for( $i=1; $i<=$total_pages; $i++ )
			{ 
				//se revisa que no sea el link actual
				if($current_pag==$i)
				{
					echo $i . '<span>|</span> ';
				}
				else
				{
				?>
					<a href="listas/comparar/<?php echo $data['id_list_mine']; ?>/<?php echo $data['id_list_other']; ?>/en-comun/<?php echo $i; ?>/<?php echo $data['res_per_page']; ?>"><?php echo $i; ?></a> <span>|</span>
				<?php
				}
			}
			if($current_pag==$total_pages)
			{
				echo '<span>Siguiente > </span>';
			}
			else
			{
			?>
				<a href="listas/comparar/<?php echo $data['id_list_mine']; ?>/<?php echo $data['id_list_other']; ?>/en-comun/<?php echo $current_pag+1; ?>/<?php echo $data['res_per_page']; ?>">Siguiente ></a>
			<?php
			}
		}
		?>
	</div>






