<style>
	#spinner {
	    position: absolute;
	    bottom: 150px;
	    right: 50%;
	}

	#spinner2 {
	    position: absolute;
	    top: 600px;
	    right: 50%;
	}
</style>

<div class="row no-margin" id="list-wrapper" style="display: block; margin-bottom:30px;">		
 <?=$data['listpage']?>
</div>

<script>
  var spinner;
  var target;
	var page = 1;
	var page_size = 10;
	var order = 'created_on DESC';
	var name = '';	
	var pages = <?=$data['max_pages']?>;

	$(document).ready(function(){
		Setup();
		SetupSpinner();		
	});

	function searchListAction()
	{
		var target = document.getElementById('spinner2');
		spinner.spin(target);
		var txt = $("#list-search").val();
		if((jQuery.trim( txt )).length==0)
			name = '';
		else
			name=txt;
		$.ajax({
			type:"POST",
			url:'api/listas/retrieve',
			data:{search_term:name, search_order:order, page_size:page_size, page:1 },
			error: function(){
					console.log('error');
					spinner.stop();
			},
			success: function(response) {
				var resp = $.parseJSON( response );
				if (resp.status) {
					$("#list-wrapper").html(resp.html);
					pages = resp.maxPages;
					Setup();
					spinner.stop();
	      }
			}
		});
	}

	function SetupSpinner () {
	var opts = {
		  lines: 9 // The number of lines to draw
		, length: 25 // The length of each line
		, width: 12 // The line thickness
		, radius: 40 // The radius of the inner circle
		, scale: 1 // Scales overall size of the spinner
		, corners: 1 // Corner roundness (0..1)
		, color: '#000' // #rgb or #rrggbb or array of colors
		, opacity: 0.25 // Opacity of the lines
		, rotate: 0 // The rotation offset
		, direction: 1 // 1: clockwise, -1: counterclockwise
		, speed: 1 // Rounds per second
		, trail: 60 // Afterglow percentage
		, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
		, zIndex: 2e9 // The z-index (defaults to 2000000000)
		, className: 'spinner' // The CSS class to assign to the spinner
		, top: '-200px' // Top position relative to parent
		, left: '50%' // Left position relative to parent
		, shadow: false // Whether to render a shadow
		, hwaccel: false // Whether to use hardware acceleration
		, position: 'absolute' // Element positioning
		}
		spinner = new Spinner(opts);
}

	function Setup () {
		$("#list-search").val(name);
		
		$(".page-book").click(function() {
			var target = document.getElementById('spinner');
			var nPage = parseInt($(this).text());
			spinner.spin(target);
			$.ajax({
				type:"POST",
				url:'api/listas/retrieve',
				data:{search_term:name, search_order:order, page_size:page_size, page:nPage },
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					var resp = $.parseJSON( response );
					if (resp.status) {
						$("#list-wrapper").html(resp.html);
						page=nPage;
						pages = resp.maxPages;
						Setup();
						spinner.stop();
		      }
				}
			});
		});

		$(".page-book-first").click(function() {
			var target = document.getElementById('spinner');
			var nPage = 1;
			spinner.spin(target);
			$.ajax({
				type:"POST",
				url:'api/listas/retrieve',
				data:{search_term:name, search_order:order, page_size:page_size, page:nPage },
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					var resp = $.parseJSON( response );
					if (resp.status) {
						$("#list-wrapper").html(resp.html);
						page=nPage;
						pages = resp.maxPages;
						Setup();
						spinner.stop();
		      }
				}
			});
		});

		$(".page-book-prev").click(function() {
			var target = document.getElementById('spinner');
			spinner.spin(target);
			var nPage = page-1;
			if (nPage<1)
				nPage=1;
			$.ajax({
				type:"POST",
				url:'api/listas/retrieve',
				data:{search_term:name, search_order:order, page_size:page_size, page:nPage },
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					var resp = $.parseJSON( response );
					if (resp.status) {
						$("#list-wrapper").html(resp.html);
						page=nPage;
						pages = resp.maxPages;
						Setup();
						spinner.stop();
		      }
				}
			});
		});

		$(".page-book-next").click(function() {
			var target = document.getElementById('spinner');
			spinner.spin(target);
			var nPage = page + 1;
			if (nPage>pages)
				nPage=pages;
			$.ajax({
				type:"POST",
				url:'api/listas/retrieve',
				data:{search_term:name, search_order:order, page_size:page_size, page:nPage },
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					var resp = $.parseJSON( response );
					if (resp.status) {
						$("#list-wrapper").html(resp.html);
						page=nPage;
						pages = resp.maxPages;
						Setup();
						spinner.stop();
		      }
				}
			});
		});

		$(".page-book-last").click(function() {
			var target = document.getElementById('spinner');
			spinner.spin(target);
			var nPage = pages;
			$.ajax({
				type:"POST",
				url:'api/listas/retrieve',
				data:{search_term:name, search_order:order, page_size:page_size, page:nPage },
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					var resp = $.parseJSON( response );
					if (resp.status) {
						$("#list-wrapper").html(resp.html);
						page=nPage;
						pages = resp.maxPages;
						Setup();
						spinner.stop();
		      }
				}
			});
		});


		$("#list_show").change(function() {
			var target = document.getElementById('spinner2');
			spinner.spin(target);
			page_size = $(this).val();
			$.ajax({
				type:"POST",
				url:'api/listas/retrieve',
				data:{search_term:name, search_order:order, page_size:page_size, page:1 },
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					var resp = $.parseJSON( response );
					if (resp.status) {
						$("#list-wrapper").html(resp.html);
						pages = resp.maxPages;
						Setup();
						spinner.stop();
		      }
				}
			});
		});


		$("#list_order").change(function() {
			var target = document.getElementById('spinner2');
			spinner.spin(target);
			order = $(this).val();
			$.ajax({
				type:"POST",
				url:'api/listas/retrieve',
				data:{search_term:name, search_order:order, page_size:page_size, page:1 },
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					var resp = $.parseJSON( response );
					if (resp.status) {
						$("#list-wrapper").html(resp.html);
						pages = resp.maxPages;
						Setup();
						spinner.stop();
		      }
				}
			});
		});

		$('#list-search').keyup(function(e){
			if (e.which == 13) {
		        searchListAction();
		    }
		});

		$("#search-list-btn").click(function() {

			searchListAction();
		});

		
	}
	
function likeItList(id){
	var ban;
	var count;
	ban = $(".list-like-"+id).data('like');
	count = parseInt($('.like-count-'+id).data('count'));

	$.ajax({
	  url: "like-list",
	  method: "POST",
	  data:{list: id,
	  		option: ban}
	}).done(function(){
		if(ban=="0"){
			count = count +1; 
			$('.list-like-'+id).text('Ya no me gusta');
			$('.list-like-'+id).data('like','1');	

			$('.like-count-'+id).data('count',count);	
			$('.like-count-'+id).text(' | '+count+' me gusta');	
		}
		else{ 
			count = count -1;		
			$('.list-like-'+id).text('Me gusta');
			$('.list-like-'+id).data('like','0');	
			$('.like-count-'+id).data('count',count);		
			if(count<=0)
				$('.like-count-'+id).text('');
			else	
				$('.like-count-'+id).text(' | '+count+' me gusta');				
		}
	});
}
</script>
<style>
.pointer{
	cursor: pointer;
}
.list-pagination-item {
	font-family: 'Open Sans', sans-serif;
	font-size: 14px;
}

.list-pagination-item a {
    text-decoration: none!important;
    padding: 0 6px!important;
}

@media screen and (max-width: 479px) {
	.list-pagination-item a {
	    
	    padding: 4px 15px 4px 15px !important;
	}
}

</style>

<script src="assets/scripts/jquery.paging.js"></script>
<script src="assets/scripts/tinysort.js"></script>

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="assets/styles/right_side.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/lists.min.css"/>
<script type="text/javascript" src="assets/scripts/spin.min.js"></script>


