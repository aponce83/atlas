<div class="list_details_section">
    <!-- Título -->
    <div class="row  list-title">
      <div class="col-sm-12">
        <h2 class="">Todos</h2>
       
      <?php if( $current_user_id === $userid_list ): //Si no es una lista del usuario actual, no se puede configurar?>
      <div class="add-book  hidden-xs">
        <?php if( $list_type != 'dynamic' ): ?>
        <button class="<?php echo $color ?>-button" data-toggle="modal" data-target="#addBook">
          Agregar un Libro
        </button>
        <?php endif; ?>
        <br>
        <?php 
        if( !($slug == 'mi-biblioteca' OR 
          $slug == 'lo-estoy-leyendo' OR 
          $slug == 'ya-lo-lei' OR
          $slug == 'lo-quiero-leer') ): ?>
        <button class="blue-button" data-toggle="modal" data-target="#configlist">
          Configurar Lista
        </button>
        <?php endif; ?>
      </div>
      <?php endif; ?>
    </div><!-- end row list-title-->

    <?php
    if( count($list_books) === 0 ):
      echo '<div class="list-msg">Aún no has agregado libros a esta lista</div>';
    else: 
    ?>
      <div class="row  list-search">
        <div class="col-sm-12">
          <form class="form-inline">
            <input type="text" class="form-control" id="search_book" name="search_book" placeholder="Busca tus libros...">
            <button type="submit" class="btn"></button>
          </form>
        </div>
      </div><!-- end row -->
    
      <!-- Ordenar Libros MD -->
      <div class="row  list-order hidden-xs">
        <div class="col-sm-10">
          <form class="form-inline">
            <div class="form-group">
              <label for="list_order">Ordenar por:</label>
              <div class="select-list-wrapper">
                <select class="form-control" name="list_order" id="list_order">
                  <option value="num">Seleccione</option>
                  <option value="popular">Más populares</option>
                  <option value="recent">Más recientes</option>
                  <option value="title">Título (A-Z)</option>
                  <option value="autor">Autor (A-Z)</option>
                  <option value="editorial">Editorial (A-Z)</option>
                </select>
              </div>
              
            </div>
            <div class="form-group">
              <label for="list_show">Mostrar:</label>
              <div class="select-list-wrapper">
                <select class="form-control" name="list_show" id="list_show">
                  <option value="5">5 Libros</option>
                  <?php if( count($list_books) > 10 ): ?>
                    <option value="10">10 Libros</option>
                  <?php endif; ?>
                  <?php if( count($list_books) > 20 ): ?>
                    <option value="20">20 Libros</option>
                  <?php endif; ?>
                  <?php if( count($list_books) > 30 ): ?>
                    <option value="30">30 Libros</option>
                  <?php endif; ?>
                  
                  <option value="-1">Todos</option>
                </select>
              </div>
              
            </div>
          </form>
        </div>
        <div class="col-sm-2  aling-r">
          <span id="info_pag">1-10 de 130</span>
        </div>
      </div><!-- end row  list-order-->

      <!-- Ordenar Libros XS -->
      <div class="row list-order-xs visible-xs">
        <div class="col-xs-12">
          
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Ordenar por <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                  </a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  
                  <form>
                    <div class="form-group">
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-order" id="book-order-radio1" value="popular" checked>
                          Más populares
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-order" id="book-order-radio2" value="recent">
                          Más recientes
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-order" id="book-order-radio3" value="title">
                          Título (A-Z)
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-order" id="book-order-radio4" value="autor">
                          Autor (A-Z)
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-order" id="book-order-radio4" value="editorial">
                          Editorial (A-Z)
                        </label>
                      </div>
                    </div>
                  </form>

                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Mostrar <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                
                  <form>
                    <div class="form-group">
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-show" id="book-show-radio1" value="5" checked>
                          5 Libros
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-show" id="book-show-radio2" value="10">
                          10 Libros
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-show" id="book-show-radio3" value="20">
                          20 Libros
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-show" id="book-show-radio3" value="30">
                          30 Libros
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-show" id="book-show-radio3" value="-1">
                          Todos
                        </label>
                      </div>
                    </div>
                  </form>

                </div>
              </div>
            </div>
          </div>

        </div>
      </div><!-- end row  list-order list-order-xs-->
    
      <table id="table_list" class="table_list">
        <thead>
          <tr>
            <th>num</th>
            <th>img</th>
            <th>title</th>
            <th>editorial</th>
            <th>rating</th>
            <th>del</th>
            <th>autor</th>
            <th>fecha agregado</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($list_books as $key => $book) 
          {  
            if($book['imagen']==NULL)
              $img = 'assets/images/generic_book.jpg';
            else
              $img = $book['imagen'];

            //---Rating

            //$rating = rand(0, 5);
            $rating = Model::factory('List')->get_book_rating( $book['id_libro'] );
            $amarilla = '<img src="assets/images/detail/estrella_amarilla_xm.svg">';
            $gris = '<img src="assets/images/detail/estrella_gris_xm.svg">';
            
            $stars = '';
            $stars .=  ( $rating >= 1 ) ? $amarilla : $gris;
            $stars .= ( $rating >= 2 ) ? $amarilla : $gris;
            $stars .= ( $rating >= 3 ) ? $amarilla : $gris;
            $stars .= ( $rating >= 4 ) ? $amarilla : $gris;
            $stars .= ( $rating >= 5 ) ? $amarilla : $gris;
            $stars .= '<br><span class="hide">'.$rating.'</span>';

            $is_new_book = Model::factory('List')->is_new_book_in_list( $book['id_libro'], $list_id );

          ?>
          <tr class="each_list">
            <td class="td_num">
              <span class="num_lis  hidden-xs"><?php echo  $key+1 ?></span>
            </td>
            <td class="td_img">
              <a href="<?php echo url::base(). 'libros/' . $book['id_libro'] ?>">
                <img src="<?php echo $img ?>">
              </a>
            </td>
            <td class="td_title">
              <p class="book_title"><a href="<?php echo url::base(). 'libros/' . $book['id_libro'] ?>"><?php echo $book['titulo'] ?></a></p>
              <p class="book_autor"><?php echo $book['autores'][1][0]['nombre'] ?></p>
              <p class="visible-xs"><?php echo $book['editorial'] ?></p>
              <p class="visible-xs" id="rating_xs"><?php echo $stars; ?></p>
              <p class="book_new"><?php echo ($is_new_book) ? 'Nuevo en esta lista' : '' ?></p>
            </td>
            <td class="td_editorial">
              <?php foreach ($book['editorial'] as $editorial): ?>
              <p class="book_editorial">
                <?php echo $editorial['nombre']; ?>
              </p>
              <?php endforeach ?>
            </td>
            <td class="td_rating">
              <div class="list-rating" id="rating_sm">
                <?php echo $stars; ?>
              </div>
            </td>
            <td class="td_close">
              <?php if( $current_user_id === $userid_list && $list_type != 'dynamic'): ?>
              <div class="list-del">
                <button type="button" data-toggle="modal" data-target="#delBook" data-bookid="<?php echo $book['id_libro'] ?>" data-userid="<?php echo $current_user_id ?>"  data-listslug="<?php echo $slug ?>" data-book-name="<?php echo $book['titulo'] ?>"></button>
              </div>
              <?php endif; ?>
            </td>
            <td class="hide">
              <span class="hide"><?php echo $book['autores'][1][0]['nombre'] ?></span>
            </td>
            <td class="hide">
              <span class="hide"><?php echo Model::factory('List')->get_added_date_book( $list_id, $book['id_libro'] ); ?></span>
            </td>
          </tr>
            <?php Model::factory('List')->its_no_longer_a_new_book( $book['id_libro'], $list_id ); ?>
          <?php } //end foreach ?>
        </tbody>
      </table>
    
    <?php endif; ?>

</div>

<div class="div-add-book-xs  visible-xs">
  <button type="button" class="btn add-book-xs" data-toggle="modal" data-target="#addBook">Agregar un libro</button>
  <?php 
  if( !($slug == 'mi-biblioteca' OR 
    $slug == 'lo-estoy-leyendo' OR 
    $slug == 'ya-lo-lei' OR
    $slug == 'lo-quiero-leer') ): ?>
    <button type="button" class="btn config-list-xs" data-toggle="modal" data-target="#configlist">Configurar lista</button>
  <?php endif; ?>
</div>

<!-- Modal Agregar Libros -->
<div class="modal fade" id="addBook" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  
<div class="row">
  <div class="col-xs-11 col-md-8 center-modal" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar un Libro</h4>
      </div>
      <div class="modal-body no-padding-sides">
        <div class="ajax-loader">
          <img src="assets/images/ajax-loader.gif">
        </div>
        <div class="row">
          <div class="col-xs-12">
            <form class="form-inline">
              <div class="form-group  div_add_book_search">
                <label class="sr-only" for="add_book_search">Busca un libro...</label>
                <input type="text" class="form-control" id="add_book_search" name="add_book_search" placeholder="Busca un libro...">
              </div>
              <div class="form-group">
                <select class="form-control" id="search_param">
                  <!-- option>Todo</option -->
                  <option value="titulo">Título</option>
                  <option value="autores">Autor</option>
                  <option value="editorial">Editorial</option>
                  <option value="temas">Tema</option>
                  <option value="keywords">Palabras Clave</option>
                </select>
              </div>
              <div class="btn btn-search-book" id="btn_add_book_search"></div>
            </form>
          </div>
        </div>
        <form action="" method="POST" id="form_search_result" onsubmit="return validateFormAddBook();">
          <div id="search_result">
            <!-- Ajax -->
          </div>
        </form>
      </div>
      
      <div class="modal-footer no-padding-sides">
        <span class="float-left"><a href="#" id="see_more_results">Ver más resultados</a></span>
        <button type="submit" class="add_book_list btn-blue" id="add_book_list" form="form_search_result" name="add_books">Agregar</button>
      </div>
    </div>
  </div>
</div>
  

</div>

<!-- Modal Eliminar Libros -->
<div class="modal fade" id="delBook">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Eliminar libro</h4>
      </div>
      <div class="modal-body">
        <p>¿Estás seguro de que quieres eliminar el libro "<span class="del-book-title">book-title</span>" de la lista "<?php echo $list_name ?>"?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn del-book-ok">Borrar</button>
        <button type="button" class="btn del-book-cancel" data-dismiss="modal">Cancelar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal Configurar Lista -->
<div class="modal fade" id="configlist">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Configurar lista</h4>
      </div>
      <div class="modal-body">
        <form action="" method="POST" id="form-update-list">
          <div class="form-group">
            <label for="list-name">Nombre</label>
            <input type="text" class="form-control modal-input" id="list-name" name="list-name" placeholder="Agrega un nombre a tu lista..." value="<?php echo $list_name ?>">
          </div>
          <div class="form-group">
            <label for="list-description">Descripción</label>
            <textarea class="form-control modal-input" rows="3" id="list-description" name="list-description" placeholder="Agrega una breve descripción..."><?php echo $list_descrip ?></textarea>
          </div>
          <div class="form-group">
            <label for="list-privacy">¿Quién puede ver mi lista?</label>
            <select class="form-control modal-input" id="list-privacy" name="list-privacy">
              <option value="public" <?php echo ($list_privacy == 'public') ? 'selected="selected"' : ''; ?> >Todos</option>
              <option value="private" <?php echo ($list_privacy == 'private') ? 'selected="selected"' : ''; ?>>Sólo yo</option>
            </select>
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn" name="submit-update-list" form="form-update-list">Actualizar lista</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
