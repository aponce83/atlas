 <script src="assets/scripts/list_all.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/custom_f.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/custom_r.css"/>
<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/scripts/custom_r.js"></script>
<!-- Importa los componentes para hacer mostrar correctamente el sidebar -->
<link rel="stylesheet" href="assets/styles/list_details.css">
<div class="library-content">
	<div class="lists-panel col-md-4  hidden-sm hidden-xs">
		<?php
		if ($data['is_owner_and_guest_the_same']) {
			include DOCROOT . 'application/views/frontend/library/library_menu.php';
		} else {
			include DOCROOT . 'application/views/frontend/library/library_menu_unsession.php';
		}
		?>
	</div>
	<div class="list-panel col-md-8">
		<div class="list_details_section">
	    <div class="row list-title">
	      <div class="col-sm-12 green-btm-border">
	        <h2>Todos</h2>
          <?php if ($data['owner'] && !$data['is_owner_and_guest_the_same']) { ?>
          <p>Libros en las listas de: 
            <a href="<?php echo $data['owner']['url']; ?>">
              <?php echo $data['owner']['fullname']; ?>
            </a>
          </p>
          <?php } ?>
	      </div>
	    </div>
	  <?php
    if( count($data['list_data']) === 0 ):
      echo '<div class="list-msg">Aún no se han agregado libros a las listas</div>';
    else: 
    	$pagesURL = URL::base(true)."lista/".base64_encode($data['owner_of_this_page_id']);
    ?>
			<input type="hidden" id="page-url" value="<?php echo $pagesURL; ?>">
      <div class="row list-search hidden">
        <div class="col-sm-12">
          <form class="form-inline">
            <input type="text" class="form-control" id="search_book" name="search_book" placeholder="Busca tus libros...">
            <button type="submit" class="btn"></button>
          </form>
        </div>
      </div><!-- end row -->
    
      <!-- Ordenar Libros MD -->
      <div class="row  list-order hidden-xs">
        <div class="col-sm-10">
          <form class="form-inline">
            <div class="form-group">
              <label for="list_order">Ordenar por:</label>
              <div class="select-list-wrapper">
                <select class="form-control" name="list_order" id="list_order" onchange="reloadPage('desktop')">
                  <option value="recent">Más recientes</option>
                </select>
              </div>
              
            </div>
            <div class="form-group">
              <label for="list_show">Mostrar:</label>
              <div class="select-list-wrapper">
                <select class="form-control" name="list_show" id="list_show" onchange="reloadPage('desktop')">
                	<option value="10" <?php if($data["how_many_books_per_page"] == 10){echo("selected");}?>>10 Libros</option>
                  <option value="20" <?php if($data["how_many_books_per_page"] == 20){echo("selected");}?>>20 Libros</option>
                  <option value="50" <?php if($data["how_many_books_per_page"] == 50){echo("selected");}?>>50 Libros</option>
                </select>
              </div>
            </div>
          </form>
        </div>
        <div class="col-sm-2  aling-r">
          <span id="info_pag">
						<?php 
              if ($data['showing_to'] > $data['how_many_books']) {
                $data['showing_to'] = $data['how_many_books'];
              }
              echo "{$data['showing_from']} - {$data['showing_to']} de {$data['how_many_books']}"; 
            ?>
          </span>
        </div>
      </div><!-- end row  list-order-->

      <!-- Ordenar Libros XS -->
      <div class="row list-order-xs visible-xs">
        <div class="col-xs-12">
          
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Ordenar por <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                  </a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <form>
                    <div class="form-group">
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-order" id="book-order-radio2" value="recent" onchange="reloadPage('mobile')">
                          Más recientes
                        </label>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Mostrar <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                
                  <form>
                    <div class="form-group">
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-show" id="book-show-radio1" value="10" onchange="reloadPage('mobile')" <?php if($data["how_many_books_per_page"] == 10){echo("checked");}?>>
                          10 Libros
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-show" id="book-show-radio2" value="20" onchange="reloadPage('mobile')" <?php if($data["how_many_books_per_page"] == 20){echo("checked");}?>>
                          20 Libros
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="book-show" id="book-show-radio3" value="50" onchange="reloadPage('mobile')" <?php if($data["how_many_books_per_page"] == 50){echo("checked");}?>>
                          50 Libros
                        </label>
                      </div>
                    </div>
                  </form>

                </div>
              </div>
            </div>
          </div>

        </div>
      </div><!-- end row  list-order list-order-xs-->
    
      <table id="" class="table_list">
        <thead>
          <tr>
            <th>img</th>
            <th>title</th>
            <th>editorial</th>
            <th>rating</th>
          </tr>
        </thead>
        <tbody>
          <?php
          //foreach ($list_books as $key => $book) 
          //$data['list_data']
          foreach ($data['list_data'] as $k => $item) {  
          	$book = $item['book'];
          	$lists = $item['lists'];

            if ($book['imagen']==NULL) {
              $img = 'assets/images/generic_book.jpg';
            } else {
              $img = $book['imagen'];
            }

            //---Rating

            //$rating = rand(0, 5);
            $rating = Model::factory('List')->get_book_rating( $book['id_libro'] );
            $amarilla = '<img src="assets/images/detail/estrella_amarilla_xm.svg">';
            $gris = '<img src="assets/images/detail/estrella_gris_xm.svg">';
            
            $stars = '';
            $stars .=  ( $rating >= 1 ) ? $amarilla : $gris;
            $stars .= ( $rating >= 2 ) ? $amarilla : $gris;
            $stars .= ( $rating >= 3 ) ? $amarilla : $gris;
            $stars .= ( $rating >= 4 ) ? $amarilla : $gris;
            $stars .= ( $rating >= 5 ) ? $amarilla : $gris;
            $stars .= '<br><span class="hide">'.$rating.'</span>';
          ?>
          <tr class="each_list">
            <td class="td_img">
              <a href="<?php echo URL::base(). 'libros/' . $book['id_libro'] ?>">
                <img src="<?php echo $img ?>">
              </a>
            </td>
            <td class="td_title">
              <p class="book_title"><a href="<?php echo url::base(). 'libros/' . $book['id_libro'] ?>"><?php echo $book['titulo'] ?></a></p>
              <p class="book_autor"><?php echo $book['autores'][1][0]['nombre'] ?></p>
              <p class="visible-xs"><?php echo $book['editorial'] ?></p>
              <p class="visible-xs" id="rating_xs"><?php echo $stars; ?></p>
              <p class="book_new">
              	<ul>
              	<?php foreach ($lists as $l): ?>
              		<li>
              			<a href="lista/<?php echo $item['userfront_id']; ?>/<?php echo $l['slug']; ?>">
              				<?php echo $l['name']; ?>
              			</a>
              		</li>
              	<?php endforeach ?>
              	</ul>
              </p>
            </td>
            <td class="td_editorial">
              <?php foreach ($book['editorial'] as $editorial): ?>
              <p class="book_editorial">
                <?php echo $editorial['nombre']; ?>
              </p>
              <?php endforeach ?>
            </td>
            <td class="td_rating">
              <div class="list-rating" id="rating_sm">
                <?php echo $stars; ?>
              </div>
            </td>
          </tr>
          <?php } //end foreach ?>
        </tbody>
      </table>
    	<div class="pull-right pagination">
    		<?php 
					$nextPage = $data["current_page"] + 1;
					if ($nextPage > $data["how_many_pages"]) {
						$nextPage = $data["how_many_pages"];
					}
					$prevPage = $data["current_page"] - 1;
					if ($prevPage < 1) {
						$prevPage = 1;
					}
					if ($data["current_page"] != 1) {
				?>
						<a href="<?php 
							echo LM::getURLWithParams($pagesURL, 
								array("page" => $prevPage,
									"expression" => "",
									"order_by" => "",
									"limit" => $data['how_many_books_per_page']));
						 ?>">
							<span class='imagen1'>anterior</span> 
						</a> |
				<?php
					}
   			?>
					<span>
						<?php
							for ($i = 0; $i < $data["how_many_pages"]; $i++) {
								$pindex = $i + 1;
								if ($i != 0) {
									echo "|";
							}
						?>
						<a 
							href="
						<?php 
							echo LM::getURLWithParams($pagesURL, 
								array("page" => $pindex,
									"expression" => "",
									"order_by" => "",
									"limit" => $data['how_many_books_per_page']));
						 ?>" 
							class="paginate_button <?php if ($pindex == $data["current_page"]) { echo "current"; } ?>">
							<?php echo ($pindex); ?>
						</a>
						<?php
						}
						?>
					</span>
				<?php
						if ($data["current_page"] != $data["how_many_pages"]) {
				?>
						| <a href="<?php 
							echo LM::getURLWithParams($pagesURL, 
								array("page" => $nextPage,
									"expression" => "",
									"order_by" => "",
									"limit" => $data['how_many_books_per_page']));
						 ?>"><span class='imagen2'>siguiente</span></a>
				<?php
						}
   			?>
    	</div>

    <?php endif; ?>
	  </div>
	</div>
	<div class="clear"></div>
</div>