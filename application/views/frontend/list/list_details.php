<!-- Importar my_friends.css para obtener la opción de agregar a amigos -->
<link rel="stylesheet" href="assets/styles/list_details.css">
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/custom_f.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/custom_r.css"/>
<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/scripts/custom_r.js"></script>
<div class="library-content">

	<div class="lists-panel col-md-4  hidden-sm hidden-xs">
		<?php
		if($data['is_owner_and_guest_the_same']) {
			include  DOCROOT . 'application/views/frontend/library/library_menu.php';
		} else {
			include  DOCROOT . 'application/views/frontend/library/library_menu_unsession.php';
		}
		?>
	</div>
	<div class="list-panel col-md-8">
		<?php 
		if($data['is_owner_and_guest_the_same']) {
			include 'list_data.php';
		} else {
		  include 'list_data_slug.php';
		}?>
	</div>
	<div class="clear"></div>
</div>
<script>
function shareFB(title, desc, img, url){
	var product_name   = 	title;
	var description	   =	desc;
	var share_image	   =	img;
	var share_url	   =	url;
	var share_caption  = 	'librosmexico.mx';
	
    FB.ui({
        method: 'feed',
      	appId: '394615860726938',   
        name: title,
        link: share_url,
        picture: share_image,
        caption : share_caption,
        description: description

    }, function(response) {
        if(response && response.post_id){}
        else{}
    });	
}
function likeItList(id){
	var ban;
	var count;
	ban = $(".list-like-"+id).data('like');
	count = parseInt($('.like-count-'+id).data('count'));

	$.ajax({
	  url: "like-list",
	  method: "POST",
	  data:{list: id,
	  		option: ban}
	}).done(function(){
		if(ban=="0"){
			count = count +1; 
			$('.list-like-'+id).text('Ya no me gusta');
			$('.list-like-'+id).data('like','1');	

			$('.like-count-'+id).data('count',count);	
			$('.like-count-'+id).text(' | '+count+' me gusta');	
		}
		else{ 
			count = count -1;		
			$('.list-like-'+id).text('Me gusta');
			$('.list-like-'+id).data('like','0');	
			$('.like-count-'+id).data('count',count);		
			if(count<=0)
				$('.like-count-'+id).text('');
			else	
				$('.like-count-'+id).text(' | '+count+' me gusta');				
		}
	});
}
function shareTW(url,titulo){
    var params = {
        access_token: "65efccf36ae26915b2362f75c2b09b8856732202",
        longUrl: url,
        format: 'json'
    };
    $.getJSON('https://api-ssl.bitly.com/v3/shorten', params, function (response, status_txt) {
        var urlbit =response.data.url;
        var len = 140 - (urlbit.length)+7;  

		if (titulo.length>len){
			titulo = titulo.substring(0,len);
			titulo = '"'+titulo+'..."';
		}else
			titulo = '"'+titulo+'"';

		var device = navigator.userAgent

		if (device.match(/Iphone/i)|| device.match(/Ipod/i)|| device.match(/Android/i)|| device.match(/J2ME/i)|| device.match(/BlackBerry/i)|| device.match(/iPhone|iPad|iPod/i)|| device.match(/Opera Mini/i)|| device.match(/IEMobile/i)|| device.match(/Mobile/i)|| device.match(/Windows Phone/i)|| device.match(/windows mobile/i)|| device.match(/windows ce/i)|| device.match(/webOS/i)|| device.match(/palm/i)|| device.match(/bada/i)|| device.match(/series60/i)|| device.match(/nokia/i)|| device.match(/symbian/i)|| device.match(/HTC/i)){
			location.target="_newtab";
			location.href='http://twitter.com/intent/tweet?text='+titulo+' '+urlbit;
		}	
		else 
			window.open('http://twitter.com/intent/tweet?text='+titulo+' '+urlbit,"_blank","top=200, left=500, width=400, height=400");
    });
}
</script>


<script>
	

			

			function shareTW(url,titulo){
	    var params = {
	        access_token: "65efccf36ae26915b2362f75c2b09b8856732202",
	        longUrl: url,
	        format: 'json'
	    };
	    $.getJSON('https://api-ssl.bitly.com/v3/shorten', params, function (response, status_txt) {
	        var urlbit =response.data.url;
	        var len = 140 - (urlbit.length)+7;  

			if (titulo.length>len){
				titulo = titulo.substring(0,len);
				titulo = '"'+titulo+'..."';
			}else
				titulo = '"'+titulo+'"';

			var device = navigator.userAgent

			if (device.match(/Iphone/i)|| device.match(/Ipod/i)|| device.match(/Android/i)|| device.match(/J2ME/i)|| device.match(/BlackBerry/i)|| device.match(/iPhone|iPad|iPod/i)|| device.match(/Opera Mini/i)|| device.match(/IEMobile/i)|| device.match(/Mobile/i)|| device.match(/Windows Phone/i)|| device.match(/windows mobile/i)|| device.match(/windows ce/i)|| device.match(/webOS/i)|| device.match(/palm/i)|| device.match(/bada/i)|| device.match(/series60/i)|| device.match(/nokia/i)|| device.match(/symbian/i)|| device.match(/HTC/i)){
				location.target="_newtab";
				location.href='http://twitter.com/intent/tweet?text='+titulo+' '+urlbit;
			}	
			else 
				window.open('http://twitter.com/intent/tweet?text='+titulo+' '+urlbit,"_blank","top=200, left=500, width=400, height=400");
	    });
	}
</script>