<div class="row no-margin atlas-searchbar">
	<div class="row no-margin atlas-bararea">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 atlas-searchar-padding">
			<div class="row no-margin">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 atlas-searchbar-title">
					¿Qué estás buscando?
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 atlas-searchbox-padding">
					<div class="row no-margin">
						<?php $sBarString=""; $count=0; foreach ($tags as $tag) { 
								if ($count==0)
									$sBarString.=$tag;
								else
									$sBarString.=(",".$tag);
								$count++;
							} ?>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 no-padding" id="tag-searchbar-select" data-fullsearch="<?=$sBarString?>">							
							<select class="tag-select-filters" multiple>
								  <?php $i=0; foreach ($tags as $tag) { ?>
								    <option value="<?=$tag?>" class="tag-searchbar-option" selected><?=$tag?></option>

							    <?php $i++; } ?>
							</select>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 no-padding" style="height:20px;">
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-padding" style="position:relative;">
							<img src="assets/images/markers/location.svg" class="location-pointer">
							<input type="text" class="atlas-search-input" id="search-location" placeholder="Busca la dirección"><?php /*onkeyup="doSearch(event, this)*/?>
							<section class="buscador-anadir">
								
							</section>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 no-padding" style="height:40px;margin-bottom:20px;">
							<div class="search_btn">
							</div>
							<div class="search_btn-mbl">								
								<i class="fa fa-search icon-search-mbl"></i> Buscar
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>