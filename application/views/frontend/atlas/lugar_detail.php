<link rel="stylesheet" type="text/css" href="assets/styles/atlaslugarstyle.css"/>
<script async src="//static.addtoany.com/menu/page.js"></script>

<div class="lugar-detalle">
	<div class="foto-lugar-main">
	<?php
		//var_dump($location);
		$imageurl = 'assets/images/'.$location->image; 
		//echo $imageurl;
		if ($imageurl !== 'assets/images/') 
		{
			echo '<img class="foto-lugar-grande" src="assets/images/'.$location->image.'">';
		}

		function url()
                    {
                        
                        return URL::base(true).'atlas-de-lectura/detalle/';
                    }

	?>
	</div>
	<div class="sup-div">
		<div class="sup-izq-div">
			<div class="col-xs-8 lugar-title">
				<?php
				 
				 echo '<p>'.$location->name.'</p>';

				?>
			</div>
			<div class="col-xs-4 sup-der-div">
			<!--book-rating-start--> 
            <?php
            /*
            <div class="col-xs-12 rating">
                <div class="col-xs-4 rate" id="rate-0"><?=isset($location->rating)?$location->rating:0?></div>
                    <div class="col-xs-8 no-padding texto-rate"> 
                        <span>Calificación general</span>
                        <div id='rank-stars'class="stars-gral">
                            <form>   
                            <?php for($i=5; $i>0;$i--){ 
                            if (true){ 
                                    if($i<=$location->rating){?>
                                         <input class="star-gral-ranked star-gral star-0-<?=$i?>" id="star-0-<?=$i?>" type="radio" name="star" />
                                         <label class="star-gral-ranked star-gral star-0-<?=$i?>" for="star-0-<?=$i?>" onclick='setCalificacion(<?=$i?>,0)')></label>
                                     <?php }
                                     else{?>
                                         <input class="star-gral star-0-<?=$i?>" id="star-0-<?=$i?>" type="radio" name="star" />
                                         <label class="star-gral star-0-<?=$i?>" for="star-0-<?=$i?>" onclick='setCalificacion(<?=$i?>, 0)'></label>                                         
                                     <?php }
                            }
                            else{?>
                                     <label class="star-gral"  style='content: url("assets/images/detail/estrella_gris_xm.svg")'></label>                                     
                                <?php 
                                }
                            }?>                             
                            </form>
                    </div>
                </div>
            </div>                  
            */
            ?>
            <!--book-rating-end-->
		</div>
		<?php
		/*
			<div class="col-xs-8 like-div">
				<?php
				//echo '<div id="textbox" style="display:inline">';
				if(is_null($data->user))
		        {
		             if ($location->likes > 0)
		            {
		                echo '<p class="likes" id="like"><span class="like-link" >Me gusta</span><bold style="color:#989898"> | '.$location->likes.' me gusta</bold></p>';
		            }
		            else
		            {
		                echo '<p class="likes" id="like"><span class="like-link" >Me gusta</span></p>';
		            }
		        }
		        else
		        {
		            if ($location->userLikesThis)
		            {
		                echo '<p class="likes" id="like"><span class="like-link" onclick="setLike(this)">Ya no me gusta</span><bold style="color:#989898"> | <span id="like-count">'.$location->likes.' me gusta</span></bold></p>';
		            }
		            else
		            {
		                if ($location->likes > 0)
		                {
		                    echo '<p class="likes" id="like"><span class="like-link" onclick="setLike(this)">Me gusta</span><bold style="color:#989898"> | <span id="like-count">'.$location->likes.' me gusta</span></bold></p>';
		                }
		                else
		                {
		                    echo '<p class="likes" id="like"><span class="like-link" onclick="setLike(this)">Me gusta</span><bold style="color:#989898"> | <span id="like-count">Se el primero en decir que te gusta esto</span></bold></p>';
		                }
		                
		            }
		        }
		        ?>
			</div>
		*/
		?>
			<div class="col-xs-8 tags">
			
				<?php
					foreach($location->tags as $tag)
					{
						echo '<span class="tag-'.$tag->slug.'">'.$tag->name.'</span>';
					}
				?>
			</div>
		</div>

		
		
	</div>
	<div style="clear: both;"></div>
	<hr>
	<?php
	if ($location->description != "" && !is_null($location->description))
	{
		echo '<div class="desc-div">';
		echo '<p>'.$location->description.'</p>';
		echo '</div>';
		echo '<hr>';
	}
	?>
	
	<div class="lugar-data">
	<?php
	 $direccion = "";
	 if ($location->street != "" && !is_null($location->street))
	 {
	 	$direccion = $direccion.$location->street;
	 }
	 if (!is_null($location->ext_number) && $location->ext_number != "")
     {
     	$direccion = $direccion." ".$location->ext_number;
     }
     if (!is_null($location->int_number) || $location->int_number != "")
     {
     	$direccion = $direccion.", interior ".$location->int_number;
     }
      if ($location->town != "" && !is_null($location->town))
	 {
	 	$direccion = $direccion.", ".$location->town;
	 }
	  if ($location->municipal_office != "" && !is_null($location->municipal_office))
	 {
	 	$direccion = $direccion.", ".$location->municipal_office;
	 }
	 if (!is_null($location->city) && $location->city != "")
     {
     	$direccion = $direccion.",  ".$location->city;
     }
      if ($location->state != "" && !is_null($location->state))
	 {
	 	$direccion = $direccion.", ".$location->state;
	 }
	  if ($location->zip != "" && !is_null($location->zip))
	 {
	 	$direccion = $direccion.". CP ".$location->zip.".";
	 }


    if ($direccion != "")
     {
     	echo '<p><span class="lugar-data-bold">Dirección: </span><span class="lugar-data-normal">'.$direccion.'</span></p>';	
     }
    

   $days = ['1'=>'Lunes','2'=>'Martes','3'=>'Miércoles','4'=>'Jueves','5'=>'Viernes','6'=>'Sábado','7'=>'Domingo'];
   $scheduleOpener = '<p><span class="lugar-data-bold">Horario(s): </span><span class="lugar-data-normal"><br>';
   $scheduleCloser = '';
   $finalScheduleOutput = '';
   foreach ($days as $dayId => $dayName) {
   		$timeString = ''; 
   		foreach ($location->schedule as $schedule) {
   		 if ($schedule->day==$dayId) {
   		 	 foreach($schedule->times as $time) {
	   	 	 		$timeString .= ('('.date('G:i',strtotime($time->begin_time)).' - '.date('G:i',strtotime($time->end_time)).') ');
	   	 	 }
	   	 }
	   }
	   if ($timeString!=='') {
	   	$finalScheduleOutput.=($dayName.': '.$timeString.'<br>');
	   }
   }
   if ($finalScheduleOutput != "")
    {
    	echo $scheduleOpener.$finalScheduleOutput.$scheduleCloser;
	}
	if ($location->phone != "" && !is_null($location->phone)) 
	{
		echo '<p><span class="lugar-data-bold">Teléfono: </span><span class="lugar-data-normal">'.$location->phone.'</span></p>';
	}
	if ($location->mail != "" && !is_null($location->mail)) 
	{
		echo '<p><span class="lugar-data-bold">Contacto: </span><a class="lugar-data-normal" href="mailto:'.$location->email.'">'.$location->email.'</a></p>';
	}
                            
	 echo '<p class="lugar-data-share"><span class="lugar-data-bold">Compartir en: </span><span class="lugar-data-img"><img src="assets/images/detail/facebook.svg" class="social-medios-mini pointer fb-pointer" width=32 height=32>&nbsp;&nbsp;';
	 $twfrase='Estoy viendo "'.$location->name;
	 echo '<a class="a2a_button_twitter" target="_blank" onclick="window.open(&#34http://www.addtoany.com/add_to/twitter?linkurl='.urlencode(url().$location->id).'&amp;linkname='.urlencode($twfrase.'" vía @LibrosMexicoMX').'&amp;linknote=&#34,&#34_blank&#34,&#34top=200, left=200, width=450, height=500&#34)" rel="nofollow" aria-label="Twitter"><span class="a2a_svg a2a_s__default a2a_s_twitter" style="width: 32px; line-height: 32px; height: 27px; border-radius: 2px; background-size: 34px; display: inline-block;cursor:pointer!important;background-image:url(&#34assets/images/detail/twitter.svg&#34)"></span></a></p>';
	?>
		<p> <a href="javascript:void(0);" data-toggle="modal" data-target="#new-location-modal">
		  	¿Quiéres agregar un nuevo lugar? Haz click aquí
		  </a></p>
	</div>
	<div class="calificaciones">
	<!--
    <div class="title-calif">
		<p>TUS CALIFICACIONES</p>
	</div>
	-->

	<?php
	$index = 1;
	foreach ($location->rating_rubro as $categoria) {
		if ($location->rating_rubro_label[$index] == "") {
			continue;
		}
	/*
	?>

	<div class="col-xs-12 "> 
	<!--rating-start-->
		<div class="col-xs-6 "> 
		    <div class="col-xs-12 rating">
		        <div class="col-xs-12 rate" id="rate-<?=$index?>"><?=isset($location->user_rating_rubro[$index])?$location->user_rating_rubro[$index]:0?></div>
		            <div class="col-xs-8 no-padding texto-rate"> 
		                <span><?=$location->rating_rubro_label[$index]?></span>
		                <div id='rank-stars-subject-<?=$index?>'class="stars">
		                    <form>   
		                    <?php for($i=5; $i>0;$i--){ 
		                    if (isset($data->user)){
		                            if($i<=$location->user_rating_rubro[$index]){?>
		                                 <input class="star-ranked star star-<?=$index?>-<?=$i?>" id="star-<?=$index?>-<?=$i?>" type="radio" name="star" />
		                                 <label class="star-ranked star star-<?=$index?>-<?=$i?>" for="star-<?=$index?>-<?=$i?>" onclick='setCalificacion(<?=$i?>,<?=$index?>)')></label>
		                             <?php }
		                             else{?>
		                                 <input class="star star-<?=$index?>-<?=$i?>" id="star-<?=$index?>-<?=$i?>" type="radio" name="star" />
		                                 <label class="star star-<?=$index?>-<?=$i?>" for="star-<?=$index?>-<?=$i?>" onclick='setCalificacion(<?=$i?>,<?=$index?>)'></label>                                         
		                             <?php }
		                    }
		                    else{?>
		                             <input class="star star-<?=$index?>-<?=$i?>" id="star-<?=$index?>-<?=$i?>" type="radio" name="star" />
		                             <label class="star star-<?=$index?>-<?=$i?>" for="star-<?=$index?>-<?=$i?>" onclick='setCalificacion(<?=$i?>,<?=$index?>)'></label>                                      
		                        <?php 
		                        }
		                    }?>                             
		                    </form>
		            </div>
		        </div>
		    </div>   
		</div>
    <!--rating-end-->
    </div>
	<?php
	*/
	$index++;
	}
	?>
	
    

    </div>
</div>

<script type="text/javascript">
$(document).ready(function () {
	<?php
		$simageurl='assets/images/'.$location->image; 
		if ($simageurl!=='assets/images/') 
			$shareimg = 'assets/images/places/'.$location->image;
		else
			$shareimg = 'assets/images/logos/libros_logo.png';
	?>	
	$('html').delegate(".fb-pointer", "click", function(e) {
		var imagen = "https://librosmexico.mx/<?=$shareimg?>?>";
		
		shareFB("<?=$location->name?>","<?=$location->description?>","https://librosmexico.mx/<?=$shareimg?>","https://librosmexico.mx/atlas-de-lectura/detalle/<?=$location->id?>");
	});
	
});

function setLike (e) {
	  var usr = -1;
	  <?php if (isset($data->user)) { ?>
	  	usr = <?=$data->user?>;
	  <?php } ?>
	  var l_id = <?=$location->id?>;
	  var likeState = "";
	  if ($(e).text()=="Me gusta")
	  	likeState="like";
	  else
	  	likeState="dislike";

	  $.ajax({
			type:"POST",
			url:'api/atlas/like_location',
			data:{user:usr, 
			    	like_state:likeState, 
			    	location:l_id },
			error: function(){
					console.log('error');
			},
			success: function(response) {
				var resp = $.parseJSON( response );
				$(e).text(resp.like_state);
				if (resp.total_likes>0)
					$("#like-count").text(resp.total_likes+" me gusta");
				else
					$("#like-count").text("Se el primero en decir que te gusta esto");
			}
		});
}


function shareFB(title, desc, img, url){
			var product_name   = 	title;
			var description	   =	desc;
			var share_image	   =	img;
			var share_url	   =	url;
			var share_caption  = 	'librosmexico.mx';
			
		    FB.ui({
		        method: 'feed',
		      	appId: '394615860726938',   
		        name: title,
		        link: share_url,
		        picture: share_image,
		        caption : share_caption,
		        description: description
		    }, function(response) {
		        if(response && response.post_id){}
		        else{}
		    });	
		}

		function shareTW(url,titulo){
    var params = {
        access_token: "65efccf36ae26915b2362f75c2b09b8856732202",
        longUrl: url,
        format: 'json'
    };
    $.getJSON('https://api-ssl.bitly.com/v3/shorten', params, function (response, status_txt) {
        var urlbit =response.data.url;
        var len = 140 - (urlbit.length)+7;  

		if (titulo.length>len){
			titulo = titulo.substring(0,len);
			titulo = '"'+titulo+'..."';
		}else
			titulo = '"'+titulo+'"';

		var device = navigator.userAgent

		if (device.match(/Iphone/i)|| device.match(/Ipod/i)|| device.match(/Android/i)|| device.match(/J2ME/i)|| device.match(/BlackBerry/i)|| device.match(/iPhone|iPad|iPod/i)|| device.match(/Opera Mini/i)|| device.match(/IEMobile/i)|| device.match(/Mobile/i)|| device.match(/Windows Phone/i)|| device.match(/windows mobile/i)|| device.match(/windows ce/i)|| device.match(/webOS/i)|| device.match(/palm/i)|| device.match(/bada/i)|| device.match(/series60/i)|| device.match(/nokia/i)|| device.match(/symbian/i)|| device.match(/HTC/i)){
			location.target="_newtab";
			location.href='http://twitter.com/intent/tweet?text='+titulo+' '+urlbit;
		}	
		else 
			window.open('http://twitter.com/intent/tweet?text='+titulo+' '+urlbit,"_blank","top=200, left=500, width=400, height=400");
    });
}
var rankInit = <?=$location->rating?>;

function setCalificacion(rank,elemento){
	var lugar= "<?=$location->name?>";
	var lugarID= <?=$location->id?>;
	var clase = "<?=$location->tags[0]->slug?>";
	if (elemento == 0)
	{
		return 0;
	}
	else
	{
		var ratings = <?php echo json_encode($location->rating_rubro);?>;
		rankInit = $('#rate-'+elemento).text();
		

	}
	console.log(rankInit);
	console.log(rank);
	

	if (rankInit == rank )
	{
		rank = 0 ;
		rankInit = 0;
	}
	else
	{
		rankInit = rank;
	}
	var i = 0;
	for (i =0; i<=rank; i++)
	{
		$(".star-"+elemento+"-"+i).addClass("star-ranked");

	}
	for(i=rank+1; i<=5; i++)
	{
		$(".star-"+elemento+"-"+i).removeClass("star-ranked");
	}
	//$(".rate").html(rank);
	if ( elemento == 0)
	{
		$.ajax({
		  url: "rank_location",
		  method: "POST",
		  data:{ranking: rank,
		  		location:lugarID},
		  error: function(){
			},
		  success: function(response) {
		  },
		}).done(function(data){
			var new_avg_rank = JSON.stringify(data);
			new_avg_rank = new_avg_rank.replace(/"/g, "");
			new_avg_rank = replaceAll(new_avg_rank,"\\", "");
			$('#rate-'+elemento).text(new_avg_rank);
			//$('#rate-'+elemento).text("Checa la base");
			// Marca todas las estrellas que aparecen en la vista
			
			//loadLastRatings();	
	});
	}
	else
	{
		$.ajax({
		  url: "rank_location_subject/"+elemento,
		  method: "POST",
		  data:{ranking: rank,
		  		location: lugarID,
		  		loc_class: clase},
		  error: function(response){
			},
		  success: function(response) {
		  },
		}).done(function(data){
			var new_avg_rank = JSON.stringify(data);
			new_avg_rank = new_avg_rank.replace(/"/g, "");
			//new_avg_rank = replaceAll(new_avg_rank,"\\", "");
			$('#rate-'+elemento).text(rank);

			var i = 1;
			var valores = [];
			while ($('#rate-'+i).length)
			{
				console.log ("Hay #rate-"+i+"...");
				valores.push(parseInt($('#rate-'+i).text()))
				i++;
			}
			var suma = 0;
			for	(i = 0; i < valores.length; i++) 
			{
    			suma+=valores[i];
			}

			var promedio = 0;
			if (valores.length)
			{
				promedio = Math.round(suma/valores.length);
			}


			console.log ("hay "+i+" elementos");
			
			console.log("suma = "+suma);
			console.log("promedio = "+promedio);
			$.ajax({
		  url: "rank_location",
		  method: "POST",
		  data:{ranking: promedio,
		  		location:lugarID},
		  error: function(){
			},
		  success: function(response) {
		  },
		}).done(function(data){
			var new_avg_rank = JSON.stringify(data);
			new_avg_rank = new_avg_rank.replace(/"/g, "");
			new_avg_rank = replaceAll(new_avg_rank,"\\", "");
			setCalificacion(promedio, 0);
			for(i=1; i<=5; i++)
			{
				$(".star-0-"+i).removeClass("star-gral-ranked");
			}
			$('#rate-0').text(new_avg_rank);
			for (i =1; i<=new_avg_rank; i++)
			{
				$(".star-0-"+i).addClass("star-gral-ranked");

			}
			
			// Marca todas las estrellas que aparecen en la vista
			
			//loadLastRatings();	
	}); 

	});
	}

	
}




</script>
