<link rel="stylesheet" type="text/css" href="assets/styles/atlaslugarstyle.css"/>

<div class="title only-mbl">
  <p>MAPA</p>
</div>
<hr class="hr-map-mbl only-mbl">

<div class="lugar-map">
	<div id="map2">
    
  </div>
  <hr>
  <div class="foot-map">
    <a href="http://www.google.com.mx/maps/?q=<?=str_replace(",", ".", $location->latitude)?>,<?=str_replace(",", ".", $location->longitude)?>" target="_blank"><p>Ver en Google Maps <img src="assets/images/detail/arrow.svg"></p></a>
  </div>
</div>  
<script type="text/javascript">

var map2;
<?php 
  /* 
    No todos los lugares tienen latitud y longitud, si no tienen la posición
    entonces no mostrar el mapa
  */ 
  $lat = str_replace(",", ".", $location->latitude);
  $lgn = str_replace(",", ".", $location->longitude);
  if ($lat == NULL) {
    $lat = 'null';
  }
  if ($lgn == NULL) {
    $lgn = 'null';
  }
?>
var miPosicion = { lat: <?php echo $lat; ?>, lng: <?php echo $lgn; ?> }
var nombreLugar=<?php echo '"'.$location->name.'"';?>;
var direccionLugar=<?php echo '"'.$location->street.' '.$location->ext_number.', '.$location->town.'"';?>;
//var tipoPin = <?php echo '"'.$location->tags[0]->slug.'"';?>;
var tipoPin = <?php echo '"'.$location->slug.'"';?>;
var boxText = '<h4>'+nombreLugar+'</h4><p>'+direccionLugar+'</p>';


$(document).ready(function()
{
  initMap2();
});

function initMap2() 
{
  map2 = new google.maps.Map(document.getElementById('map2'), {
    center: { lat: <?php echo $lat; ?>, lng: <?php echo $lgn; ?> },
    zoom: 16,
    disableDefaultUI: true
  });

  var icono = "";
  switch (tipoPin)
  {
    case "libreria":
      icono = 'assets/images/detail/pin_libreria.svg';
      break;
    case "biblioteca":
      icono = 'assets/images/detail/pin_biblioteca.svg';
      break;
    case "sala-de-lectura":
      icono = 'assets/images/detail/pin_salas.svg';
      break;
    case "centro-de-lectura":
      icono = 'assets/images/detail/pin_centro.svg';
      break;
    default:
      icono = 'assets/images/detail/pin_otros.svg';
      break;
  }

  var infowindow = new google.maps.InfoWindow({
    content: boxText
  });

  var boxOptions = {
            alignBottom: true
            ,content: boxText
            ,disableAutoPan: false
            ,maxWidth: 0
            ,pixelOffset: new google.maps.Size(-125, -70)
            ,zIndex: null
            ,boxClass: "infobox-location"
            ,closeBoxMargin: "15px 12px 2px 2px"
            ,closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif"
            ,infoBoxClearance: new google.maps.Size(5, 5)
            ,isHidden: false
            ,pane: "floatPane"
            ,enableEventPropagation: false
    };


  var marker = new google.maps.Marker({
    position: miPosicion,
    map: map2,
    animation: google.maps.Animation.DROP,
    icon: icono,

    title: nombreLugar
  });

  marker.addListener('click', function() {
    //infowindow.open(map, marker);
    var ib = new InfoBox();
    ib.setOptions(boxOptions);
    ib.open(map2, marker);

  });
  var ib = new InfoBox();
  ib.setOptions(boxOptions);
  ib.open(map2, marker);

  //iwBackground.children(':nth-child(3)').find('div').children().css({'color':'#000000','box-shadow': 'rgba(0, 0, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});
  
}

</script>



