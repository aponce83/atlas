<link rel="stylesheet" type="text/css" href="assets/styles/custom_f.css"/>

<div class="search-bar-wrp">
<?php
	echo View::factory('frontend/atlas/searchbar')->set('tags', $data->tags);
?>
</div>
<div class="detail-content">
	<div class="home-atlas-wrapper">
		<div class="col-xs-12 colum-wrappers">
			<div class="home-title-section">
				<h1><a href="atlas-de-lectura">Atlas de lectura</a></h1>
			</div>
			<div class="col-xs-8 primer-columna">
				<!-- Contenido del panel izquierdo -->
        <?php include 'lugar_detail.php';?>
        <!-- Comentarios -->
        <?php // require_once 'comment_detail.php'; ?>
			</div>
			<div class="col-xs-4 segunda-columna" data-setmobile="0"

				<?php
				if (!is_null($location->latitude) && $location->latitude != 0)
				{
					include 'detail_map.php';
				}
				?>	
				<?php // include 'popular_list_detail.php';?>	
				<?php // include 'commented_list_detail.php';?>	
				<?php include 'near_list_detail.php';?>	
			</div>
		</div>
	</div>

</div>

<div id="map" class="hide"></div>
<?php
	echo View::factory('frontend/atlas/suggestion_modal_snippet');
?>

<?php /* Inicio de coodigo para barra de busqueda */ ?>
<script 
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBH23el0xVqCyH5LE3NIMyJgAqtoo9wMdE&libraries=places&language=es">
</script>
<script  src="assets/scripts/infobox.js" type="text/javascript"></script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="assets/scripts/select-widget-min.js"></script>
<script type="text/javascript" src="assets/scripts/book-detail.js"></script>
<script src="assets/scripts/jquery.sumoselect.min.js"></script>
<script src="assets/scripts/icheck.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/styles/drop-down.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/sumoselect.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/atlas.css"/>

<script async defer type="text/javascript">
	var selectedLat = 19.390519;
	var selectedLng = -99.4238064;

	var localLat = 19.4326068;
	var localLng = -99.1353989;
	var geocoder;
	var localRef="Plaza de la Constitución, Centro, Ciudad de México, México";
	var personMarker=null;

	var tags = [<?php $t=0; foreach ($tags as $filter) { ?>
								<?php if ($t==0) { ?>
									"<?=$filter?>"
								<?php } else { ?>
									,"<?=$filter?>"
								<?php } ?>
							<?php $t++; } ?>];



	$(document).ready(function(){
		initMap();
		SetupService();
		

		$(".search_btn-mbl").click(function () {
			$("#search-location").val(localRef);
    	if ($(".tag-select-filters").val()!=null) {
				var filters = $(".tag-select-filters").val();
				var tag_filters="";
				for (var k=0; k<filters.length; k++) {
					var sample = filters[k];
					var trimmed = sample.replace(/\s/gi, "+");
					if (k==0)
						tag_filters+=trimmed;
					else
						tag_filters+=(","+trimmed);
				}
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
			}
			else {
				var full_filters = $("#tag-searchbar-select").data("fullsearch");
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
			}
		});

		$(".tag-select-filters").SumoSelect({ 
						 placeholder:"Selecciona",
			       triggerChangeCombined: true,
			       forceCustomRendering: true,
			       captionFormat: "{0} Seleccionados"
		});

		$(".tag-select-filters").change(function () {
			console.log($(this).val());
		});		
		
		$('html').delegate(".location-item", "click", function(e) {
			  $(".atlas-search-input").val($(this).data("info"));
			  selectedLat = $(this).data("lat");
			  selectedLng = $(this).data("lng");
			  console.log(selectedLat+','+selectedLng);
			  $(".buscador-anadir").removeClass("show");
	  		$(".buscador-anadir").empty();
	  		e.stopPropagation();
		});

		$('html').click(function() {
			$(".buscador-anadir").removeClass("show");
	  	$(".buscador-anadir").empty();
		});

		$(".search_btn").click(function () {
			$("#search-location").val(localRef);
    	if ($(".tag-select-filters").val()!=null) {
				var filters = $(".tag-select-filters").val();
				var tag_filters="";
				for (var k=0; k<filters.length; k++) {
					var sample = filters[k];
					var trimmed = sample.replace(/\s/gi, "+");
					if (k==0)
						tag_filters+=trimmed;
					else
						tag_filters+=(","+trimmed);
				}
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
			}
			else {
				var full_filters = $("#tag-searchbar-select").data("fullsearch");
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
			}
		});

		$(".atlas-search-input").val("<?=$data->search_reference?>");
		//$(".tag-select-filters")

		/*parte nueva de infra*/
		getLocation();
		getLocationList();

    $(".location-pointer").click(function() {
    	$("#search-location").val(localRef);
    	if ($(".tag-select-filters").val()!=null) {
				var filters = $(".tag-select-filters").val();
				var tag_filters="";
				for (var k=0; k<filters.length; k++) {
					var sample = filters[k];
					var trimmed = sample.replace(/\s/gi, "+");
					if (k==0)
						tag_filters+=trimmed;
					else
						tag_filters+=(","+trimmed);
				}
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
			}
			else {
				var full_filters = $("#tag-searchbar-select").data("fullsearch");
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
			}
    });
	}); // Fin de document.ready


// searchFriends abort
	var search_user_xhr;
	

	function searchFriends(o) {
		var searchTerms = $(o).val();
		searchTerms = searchTerms.trim();

		if ($(".buscador-anadir").hasClass("show")) {
			$(".buscador-anadir").removeClass("show");
		}

		if (searchTerms == "") {
			return;
		}

		 SendPlaceRequest (searchTerms);
	}

	
	function doSearch(e, o) {
		var str = $(o).val();
		if (e.keyCode === 13 || 
			str.length >= 3) {
			searchFriends(o);
		} else if (str.length <= 3) {
			$(".buscador-anadir").empty();
			$(".buscador-anadir").removeClass("show");
		}
	}

	function SetupService () {
			service = new google.maps.places.PlacesService(map);		
			
			var input = document.getElementById('search-location');
			var options = {
			  componentRestrictions: {country: 'mx'}
			};
			autocomplete = new google.maps.places.Autocomplete(input, options);
			autocomplete.addListener('place_changed',function(){
				var place = autocomplete.getPlace();
				selectedLat = place.geometry.location.lat();
				selectedLng = place.geometry.location.lng();
				if ($(".tag-select-filters").val()!=null) {
					var filters = $(".tag-select-filters").val();
					var tag_filters="";
					for (var k=0; k<filters.length; k++) {
						var sample = filters[k];
						var trimmed = sample.replace(/\s/gi, "+");
						if (k==0)
							tag_filters+=trimmed;
						else
							tag_filters+=(","+trimmed);
					}
					window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+selectedLat+"&lng="+selectedLng+"&reference="+$(".atlas-search-input").val();
				}
				else {
					var full_filters = $("#tag-searchbar-select").data("fullsearch");
					window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+selectedLat+"&lng="+selectedLng+"&reference="+$(".atlas-search-input").val();
				}
			});	
	}

	function SendPlaceRequest (text) {
		var request = {
			query: text,
			language: "es"
	  };

	  service.textSearch(request, searchCallback);
	}

	function searchCallback(results, status) {
		$(".buscador-anadir").empty();
	  if (status == google.maps.places.PlacesServiceStatus.OK) {
	  	if (results.length>0) {
	  		if (!$(".buscador-anadir").hasClass("show"))
	  			$(".buscador-anadir").addClass("show");
		    for (var i = 0; i < results.length; i++) {
		    	var appnd = "<div data-lat='"+results[i].geometry.location.lat()+"' data-lng='"+results[i].geometry.location.lng()+"' data-info='"+results[i].formatted_address+"; "+results[i].name+"' class='location-item'>"+results[i].formatted_address+"<br>"+results[i].name+"</div>";
		      $(".buscador-anadir").append(appnd);
		      
		    }
		  }
		  else {
		  	$(".buscador-anadir").removeClass("show");
		  	$(".buscador-anadir").empty();
		  }
	  }
	  else {
	  	$(".buscador-anadir").removeClass("show");
	  	$(".buscador-anadir").empty();
	  }
	}

	function initMap() {
		mapnomap = new google.maps.Map(document.getElementById('map'), {
	      center: {lat: 19.4326068, lng: -99.1353989},
	      zoom: 14,
	      scrollwheel: false
	  });
	}

	function getLocation() 
	{
	    if (navigator.geolocation) 
	    {
	        navigator.geolocation.getCurrentPosition(changePosition, showError);
	    } 
	    else
	    {
	    	alert("Su navegador no soporta la caracteristica de localizacion");
	    }


	}

	function changePosition (position) 
	{
		localLat = position.coords.latitude;
		localLng = position.coords.longitude;
		buildNearListPage();
		CreateGeocoder();
		

	}

	function getLocationList() 
	{
	    if (navigator.geolocation) 
	    {
	    	//console.log("Si hay geolocation para mi");
	        navigator.geolocation.getCurrentPosition(changePositionList, showError);
	    } 
	    else
	    {
	    	alert("Su navegador no soporta la caracteristica de localizacion");
	    }


	}

	function changePositionList (position) 
	{
		localLat = position.coords.latitude;
		localLng = position.coords.longitude;
		buildNearListPage();
	}


	/*funciones nuevas de infra*/
	function showError (error) {
		switch(error.code) {
        case error.PERMISSION_DENIED:
            console.log("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            console.log("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            console.log("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            console.log("An unknown error occurred.");
            break;
    }
	}

	function CreateGeocoder () {
		geocoder = new google.maps.Geocoder;
		geocodeLatLng(geocoder);
	}

	function geocodeLatLng(geocoder) {
	  var latlng = {lat: localLat, lng: localLng};
	  geocoder.geocode({'location': latlng}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {
	      if (results[0]) {	        
	        localRef=results[0].formatted_address;
	      } else {
	        console.log('No results found');
	      }
	    } else {
	      console.log('Geocoder failed due to: ' + status);
	    }
	  });
	}


	function buildNearListPage()
	{
		// document.getElementById("near-list-main").innerHTML="Coordenadas: "+localLat+","+localLng;
		//getLocation();
		//console.log("");
		var cPage = 1;
		var target = document.getElementById('near-list-main');
		//spinner.spin(target);
		var tagstring = GetTagString ();
		$.ajax({
				type:"POST",
				url:'api/atlas/retrieve_near_list',
				data:{tags:tagstring, 
					    latitude: localLat-0.5, 
					    longitude: localLng-0.5, 
					    latitudeM: localLat+0.5, 
					    longitudeM: localLng+0.5, 
					    reference:localRef, 
					    page:(cPage-1) },
				error: function(){
						console.log('error');
						//spinner.stop();
				},
				success: function(response) {
					//spinner.stop();
					var resp = $.parseJSON( response );
					$('#near-list-main').html(resp.html);
					//Setups();
					//BuildMapMarkers (resp.total);
					pages = resp.pages;
					page=1;
				}
		});
	}
	
	function GetTagString () 
	{
		var retVal="";
		for (var i=0; i<tags.length; i++) {
			if (i==0)
				retVal+=tags[i];
			else
				retVal+=(","+tags[i]);
		}
		return retVal;
	}


</script>




<?php /* Fin de coodigo para barra de busqueda */ ?>
