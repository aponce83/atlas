<?=$data->atlas_searchbar?>
<div class="row no-margin atlas-title">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 atlas-title-col">
		<a href="atlas-de-lectura">Atlas de lectura</a>
	</div>
</div>

<div class="row no-margin home-row" id="sec-spinner-container">
	<div class="col col-xs-12 col-sm-5 col-md-6 col-lg-6 atlas-map-section">
			<div class-'row no-margin'>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 atlas-results-title-col no-padding">
					<span class="atlas-results-title" style="display: inline-block!important;">
					</span>
					<span class="atlas-results-amount"><?=$data->search_reference!==""?("En ".$data->search_reference):""?> <a href="javascript:void(0);" data-toggle="modal" data-target="#new-location-modal">¿Conoces algún lugar en esta área? Compártelo aquí</a></span>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 atlas-results-filter-col-mbl">
					<span class="atlas-filter-title-mbl">
						Filtros <i class="fa fa-angle-down arrow-link-down"></i>
					</span>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 atlas-results-filter-items-mbl">
					<fieldset class="fieldset-mbl hide">
						<?php $i=0; foreach ($data->tags as $tag) { ?>
						  <input type="checkbox" name="tag_filters" value="<?=$tag?>" id="<?='tag_checkbox_'.$tag?>" <?php if (strpos($data->tag_string,$tag) !== false) { ?>checked<?php } ?>/>
						  <label for="<?='tag_checkbox_'.$tag?>" class="tag_checkbox"><?=$tag?></label><br>
						<?php $i++; } ?>
						<input type="checkbox" name="tag_filters" value="all" id="tag_checkbox_all"/><label for="tag_checkbox_all" class="tag_checkbox">Todos</abel>
			    </fieldset>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
					<div id="map" class="atlas-results-map"></div>
				</div>
			</div>
	</div>
	<div class="col col-xs-12 col-sm-7 col-md-6 col-lg-6 atlas-results-column">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 atlas-results-filter-col">
			<span class="atlas-filter-title">
				Filtros
			</span>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 atlas-results-filter-col">
			<fieldset class="fieldset-dsk">
				<?php $i=0; foreach ($data->tags as $tag) { ?>
					<span style="display:inline-block" class="fieldset-dsk-item">
					  <input type="checkbox" name="tag_filters" value="<?=$tag?>" id="<?='tag_checkbox_'.$tag?>" <?php if (strpos($data->tag_string,$tag) !== false) { ?>checked<?php } ?>/>
					  <label for="<?='tag_checkbox_'.$tag?>" class="tag_checkbox"><?=$tag?></label>
					</span>
				<?php $i++; } ?>
				<span style="display:inline-block">
					<input type="checkbox" name="tag_filters" value="all" id="tag_checkbox_all"/><label for="tag_checkbox_all" class="tag_checkbox">Todos</abel>
				</span>
	    </fieldset>
		</div>
		<div class="page-wrapper-container">
			<div class-'row no-margin atlas-results-row' id="content-page-wrapper">
			</div>		
		</div>		
	</div>
</div>
<?php
	echo View::factory('frontend/atlas/suggestion_modal_snippet');
?>

<?php /* Inicio de coodigo para barra de busqueda */ ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxe9PzkkqPpM0hiMBf3UxzZkuOCLbPOD8&libraries=places&language=es"></script>

<script type="text/javascript">
	var autocomplete;
	
	$(document).ready(function(){
		initMap();
		SetupService();

		$(".tag-select-filters").SumoSelect({ 
						 placeholder:"Estoy buscando...",
			       triggerChangeCombined: true,
			       forceCustomRendering: true,
			       captionFormat: "{0} Seleccionados"
		    });

		$(".tag-select-filters").change(function () {
			console.log($(this).val());
		});		
		
		$('html').delegate(".location-item", "click", function(e) {
			  $(".atlas-search-input").val($(this).data("info"));
			  lat = $(this).data("lat");
			  lng = $(this).data("lng");
			  console.log(selectedLat+','+selectedLng);
			  $(".buscador-anadir").removeClass("show");
	  		$(".buscador-anadir").empty();
	  		e.stopPropagation();
		});

		$('html').click(function() {
			$(".buscador-anadir").removeClass("show");
	  	$(".buscador-anadir").empty();
		});

		$(".search_btn").click(function () {
			$("#search-location").val(localRef);
    	if ($(".tag-select-filters").val()!=null) {
				var filters = $(".tag-select-filters").val();
				var tag_filters="";
				for (var k=0; k<filters.length; k++) {
					var sample = filters[k];
					var trimmed = sample.replace(/\s/gi, "+");
					if (k==0)
						tag_filters+=trimmed;
					else
						tag_filters+=(","+trimmed);
				}
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
			}
			else {
				var full_filters = $("#tag-searchbar-select").data("fullsearch");
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
			}
		});

		$(".search_btn-mbl").click(function () {
			$("#search-location").val(localRef);
    	if ($(".tag-select-filters").val()!=null) {
				var filters = $(".tag-select-filters").val();
				var tag_filters="";
				for (var k=0; k<filters.length; k++) {
					var sample = filters[k];
					var trimmed = sample.replace(/\s/gi, "+");
					if (k==0)
						tag_filters+=trimmed;
					else
						tag_filters+=(","+trimmed);
				}
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
			}
			else {
				var full_filters = $("#tag-searchbar-select").data("fullsearch");
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
			}
		});

		$(".atlas-search-input").val("<?=$data->search_reference?>");
		//$(".tag-select-filters")
});


// searchFriends abort
	var search_user_xhr;
	

	function searchFriends(o) {
		var searchTerms = $(o).val();
		searchTerms = searchTerms.trim();

		if ($(".buscador-anadir").hasClass("show")) {
			$(".buscador-anadir").removeClass("show");
		}

		if (searchTerms == "") {
			return;
		}

		 SendPlaceRequest (searchTerms);
	}

	
	function doSearch(e, o) {
		var str = $(o).val();
		if (e.keyCode === 13 || 
			str.length >= 3) {
			searchFriends(o);
		} else if (str.length <= 3) {
			$(".buscador-anadir").empty();
			$(".buscador-anadir").removeClass("show");
		}
	}

	function SetupService () {
		service = new google.maps.places.PlacesService(map);		
		
		var input = document.getElementById('search-location');
		var options = {
		  componentRestrictions: {country: 'mx'}
		};
		autocomplete = new google.maps.places.Autocomplete(input, options);
		autocomplete.addListener('place_changed',function(){
			var place = autocomplete.getPlace();
			selectedLat = place.geometry.location.lat();
			selectedLng = place.geometry.location.lng();
			if ($(".tag-select-filters").val()!=null) {
				var filters = $(".tag-select-filters").val();
				var tag_filters="";
				for (var k=0; k<filters.length; k++) {
					var sample = filters[k];
					var trimmed = sample.replace(/\s/gi, "+");
					if (k==0)
						tag_filters+=trimmed;
					else
						tag_filters+=(","+trimmed);
				}
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+selectedLat+"&lng="+selectedLng+"&reference="+$(".atlas-search-input").val();
			}
			else {
				var full_filters = $("#tag-searchbar-select").data("fullsearch");
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+selectedLat+"&lng="+selectedLng+"&reference="+$(".atlas-search-input").val();
			}
		});		
	}

	function SendPlaceRequest (text) {
		var request = {
			query: text,
			language: "es"
	  };

	  service.textSearch(request, searchCallback);
	}

	function searchCallback(results, status) {
		$(".buscador-anadir").empty();
	  if (status == google.maps.places.PlacesServiceStatus.OK) {
	  	if (results.length>0) {
	  		if (!$(".buscador-anadir").hasClass("show"))
	  			$(".buscador-anadir").addClass("show");
		    for (var i = 0; i < results.length; i++) {
		    	var appnd = "<div data-lat='"+results[i].geometry.location.lat()+"' data-lng='"+results[i].geometry.location.lng()+"' data-info='"+results[i].formatted_address+"; "+results[i].name+"' class='location-item'>"+results[i].formatted_address+"<br>"+results[i].name+"</div>";
		      $(".buscador-anadir").append(appnd);
		      
		    }
		  }
		  else {
		  	$(".buscador-anadir").removeClass("show");
		  	$(".buscador-anadir").empty();
		  }
	  }
	  else {
	  	$(".buscador-anadir").removeClass("show");
	  	$(".buscador-anadir").empty();
	  }
	}

</script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="assets/scripts/select-widget-min.js"></script>
<script src="assets/scripts/jquery.sumoselect.min.js"></script>
<script src="assets/scripts/icheck.min.js"></script>
<script src="assets/scripts/markerwithlabel.js"></script>
<link rel="stylesheet" type="text/css" href="assets/styles/drop-down.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/sumoselect.css"/>

<?php /* Fin de coodigo para barra de busqueda */ ?>

<script type="text/javascript" src="assets/scripts/spin.min.js"></script>
<script type="text/javascript" src="assets/scripts/infobox.js"></script>

<script type="text/javascript">
	var map;
	var service;
	var spinner;
	var spinner2;
	var page = 1;
	var markers = [];
	var usr = "<?=$data->user?>";
	var usemyPos = <?php if ($data->use_my_pos) { ?>true<?php } else { ?>false<?php } ?>;

	var searchCircle=null;
	var selectedRadius = 0.18195845206498;
	var personMarker=null;

	var loaded=false;

	var lat = <?php echo str_replace(",", ".", $data->search_lat); ?>;
	var lng = <?php echo str_replace(",", ".", $data->search_lng); ?>;
	var localLat =  19.4326018;
	var localLng = -99.13320490000001;
	var geocoder;
	var localRef="Zócalo, Plaza de la Constitución, Centro, Cuauhtémoc, Ciudad de México";
	var tags = [<?php $t=0; foreach ($data->tag_filters as $filter) { ?>
								<?php if ($t==0) { ?>
									"<?=$filter?>"
								<?php } else { ?>
									,"<?=$filter?>"
								<?php } ?>
							<?php $t++; } ?>];
	var ref = "<?=$data->search_reference?>";

	$(document).ready(function () {
		$('html, body').stop().animate({
        scrollTop: $(".atlas-title-col").offset().top
    }, 1500, 'easeInOutExpo');
		//initMap();
		$("input[name='tag_filters']").iCheck({
	    checkboxClass: 'icheckbox_minimal-green',
	    radioClass: 'iradio_minimal',
	    increaseArea: '20%' // optional
	  });

	  $(".atlas-results-filter-col-mbl").click(function () {
	  	if ($(".fieldset-mbl").hasClass("hide")) {
	  		$(".fieldset-mbl").removeClass("hide");
	  		$(".arrow-link-down").removeClass("fa-angle-down");
	  		$(".arrow-link-down").addClass("fa-angle-up");
	  		$(this).addClass("atlas-results-filter-col-mbl-green");
	  		$(".arrow-link-down").addClass("arrow-link-down-green");
	  		$(".fieldset-mbl").addClass("fieldset-mbl-green");
	  	}
	  	else {
	  		$(".fieldset-mbl").addClass("hide");
	  		$(".arrow-link-down").removeClass("fa-angle-up");
	  		$(".arrow-link-down").addClass("fa-angle-down");
	  		$(this).removeClass("atlas-results-filter-col-mbl-green");
	  		$(".arrow-link-down").removeClass("arrow-link-down-green");
	  		$(".fieldset-mbl").removeClass("fieldset-mbl-green");
	  	}
	  });

	  $("input[name='tag_filters']").on('ifToggled', function(event){
	  	setTimeout(BuildTagArray, 200)
		});

		$("input[name='tag_filters']").on('ifChecked', function(event){
	  	if ($(this).is("#tag_checkbox_all")) {
	  		$("input[name='tag_filters']").iCheck('check'); 
	  		setTimeout(BuildTagArray, 200);
	  	}
		});

		$("input[name='tag_filters']").on('ifUnchecked', function() {
			$("#tag_checkbox_all").iCheck('uncheck');
		});

		google.maps.event.addListener(map, 'bounds_changed', function() {
			if (!loaded) {
       	PageQuery();
       	var image = 'assets/images/markers/people.png';
			  personMarker = new google.maps.Marker({
			    position: {lat: map.getCenter().lat(), lng: map.getCenter().lng()},
			    map: map,
			    icon: image,
			    optimized: false
			  }); 
			  personMarker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
       	loaded=true;
      }      
    });

    google.maps.event.addListener(map, 'center_changed', function() {
    	if (loaded) {
	    	personMarker.setMap(null);
	    	personMarker=null;
	    	var image = 'assets/images/markers/people.png';
			  personMarker = new google.maps.Marker({
			    position: {lat: map.getCenter().lat(), lng: map.getCenter().lng()},
			    map: map,
			    icon: image,
			    optimized: false
			  });
			  personMarker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
			}
    });

		$('html').delegate(".location-result-likes-link", "click", function(e) {
			  var l_id = $(this).data("locationid");
			  var likeState = "";
			  if ($(this).text()=="Me gusta")
			  	likeState="like";
			  else
			  	likeState="dislike";

			  $.ajax({
					type:"POST",
					url:'api/atlas/like_location',
					data:{user:usr, 
					    	like_state:likeState, 
					    	location:l_id },
					error: function(){
							console.log('error');
					},
					success: function(response) {
						var resp = $.parseJSON( response );
						$(".location-result-likes-link-"+resp.l_id).text(resp.like_state);
						$(".location-result-likecount-"+resp.l_id).text(resp.total_likes+" me gusta");
					}
				});
		});

    SetupSpinner ();

    var target = document.getElementById('sec-spinner-container');
		spinner2.spin(target);

    getLocation();
    
    $(".location-pointer").click(function() {
    	$("#search-location").val(localRef);
    	if ($(".tag-select-filters").val()!=null) {
				var filters = $(".tag-select-filters").val();
				var tag_filters="";
				for (var k=0; k<filters.length; k++) {
					var sample = filters[k];
					var trimmed = sample.replace(/\s/gi, "+");
					if (k==0)
						tag_filters+=trimmed;
					else
						tag_filters+=(","+trimmed);
				}
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
			}
			else {
				var full_filters = $("#tag-searchbar-select").data("fullsearch");
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
			}
    });

    $('html').delegate(".atlas-location-number", "click", function(e) {
    	var id = parseInt($(this).text());
    	map.panTo(markers[id-1].getPosition());
    	google.maps.event.trigger(markers[id-1], 'click');
    });
	});

	function CreateGeocoder () {
		geocoder = new google.maps.Geocoder;
		geocodeLatLng(geocoder);
	}

	function geocodeLatLng(geocoder) {
	  var latlng = {lat: localLat, lng: localLng};
	  geocoder.geocode({'location': latlng}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {
	      if (results[0]) {	        
	        localRef=results[0].formatted_address;
	      } else {
	        console.log('No results found');
	      }
	    } else {
	      console.log('Geocoder failed due to: ' + status);
	    }
	  });
	}

	function getLocation() {
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(changePosition, showError);
	    } else { 
	        
	    }
	}

	function changePosition (position) {
		localLat = position.coords.latitude;
		localLng = position.coords.longitude;
		spinner2.stop();
		CreateGeocoder();
		if (usemyPos) {
			lat = localLat;
			lng = localLng;
			map.setCenter(new google.maps.LatLng(lat,lng));
			setTimeout(PageQuery, 200);
		}
	}

	function showError (error) {
		spinner2.stop();
		if (usemyPos) {
			lat=localLat;
			lng=localLng;
			map.setCenter(new google.maps.LatLng(lat,lng));
			setTimeout(PageQuery, 200);	
		}
		switch(error.code) {
        case error.PERMISSION_DENIED:
            console.log("User denied the request for Geolocation.");
            break;
        case error.POSITION_UNAVAILABLE:
            console.log("Location information is unavailable.");
            break;
        case error.TIMEOUT:
            console.log("The request to get user location timed out.");
            break;
        case error.UNKNOWN_ERROR:
            console.log("An unknown error occurred.");
            break;
    }
	}

	function BuildTagArray () {
		tags=[];
		if ($(window).width()>599) {
			var tmpArr=[];
		  $(".fieldset-dsk > .fieldset-dsk-item > .icheckbox_minimal-green").each(function() {
		  	if ($(this).hasClass("checked")) {
		  		if ($(this).find('input:checkbox:first').val()!="all")
		  			tmpArr.push($(this).find('input:checkbox:first').val());
		  	}
		  });
		  if (tmpArr.length>0) {
		  	tags = tmpArr;
		  	PageQuery();
		  }
		  else {
		  	tags.push("nofilters");
		  	PageQuery();
		  }
		}
		else {
			var tmpArr=[];
		  $(".fieldset-mbl > .icheckbox_minimal-green").each(function() {
		  	if ($(this).hasClass("checked")) {
		  		if ($(this).find('input:checkbox:first').val()!="all")
		  			tmpArr.push($(this).find('input:checkbox:first').val());
		  	}
		  });
		  if (tmpArr.length>0) {
		  	tags = tmpArr;
		  	PageQuery();
		  }
		  else {
		  	tags.push("nofilters");
		  	PageQuery();
		  }
		}
	}

	function initMap() {
		var _lat = <?php echo str_replace(",", ".", $data->search_lat); ?>;
		var _lng = <?php echo str_replace(",", ".", $data->search_lng); ?>;

		map = new google.maps.Map(document.getElementById('map'), {
	      center: {lat: _lat, lng: _lng},
	      zoom: 15,
	      scrollwheel: false
	  });

	  google.maps.event.addListener(map, 'dragend', function() { 
	  	lat=map.getCenter().lat();
	  	lng=map.getCenter().lng();
	  	PageQuery();	  	
	  });

		google.maps.event.addListener(map, 'zoom_changed', function() { 
			lat=map.getCenter().lat();
	  	lng=map.getCenter().lng();
	  	PageQuery(); 
	  	console.log(map.getZoom());
		});
	}

	function buildCircle () {
		searchCircle = new google.maps.Circle({
      strokeColor: '#0000FF',
      strokeOpacity: 0.4,
      strokeWeight: 2,
      fillColor: '#0000FF',
      fillOpacity: 0.1,
      map: map,
      center: {lat: map.getCenter().lat(), lng: map.getCenter().lng()},
      radius: getZoomRadius()
    });
	}

	function getZoomRadius () {
		/*
		if (map.getZoom()>15) {
			selectedRadius = -1.0;
			return 0;
		}
		*/
		switch (map.getZoom()) {
			default:
				selectedRadius = 0.72783380825992;
				return 1200;
			case 17:
				selectedRadius = 0.18195845206498;
				return 300;
			case 16:
				selectedRadius = 0.36391690412996;
				return 600;
			case 15:
				selectedRadius = 0.72783380825992;
				return 1200;
			case 14:
				selectedRadius = 1.45566761651984;
				return 2400;
			case 13:
				selectedRadius = 2.91133523303968;
				return 4800;
			case 12:
				selectedRadius = 5.82267046607936;
				return 9600;
			case 11:
				selectedRadius = 11.64534093215872;
				return 19200;
			case 10:
				selectedRadius = 23.29068186431744;
				return 38400;
			case 9:
				selectedRadius = 46.58136372863488;
				return 76800;
			case 8:
				selectedRadius = 93.16272745726976;
				return 153600;
			case 7:
				selectedRadius = 186.3254549145395;
				return 307200;
			case 6:
				selectedRadius = 372.650909829079;
				return 614400;
			case 5:
				selectedRadius = 745.3018196581581;
				return 1228800;
			case 4:
				selectedRadius = 1490.603639316316;
				return 2457600;
			case 3:
				selectedRadius = -1.0;
				return 0;
			case 2:
				selectedRadius = -1.0;
				return 0;
			case 1:
				selectedRadius = -1.0;
				return 0;
		}
	}

	function lerp(a, b, f)
	{
	    return a + f * (b - a);
	}

	/* Pagination Setup **********************************************************************************/

	function Setups () {
		setupPaginationLinks ();
	}

	function SetupSpinner () {
	var opts = {
		  lines: 9 // The number of lines to draw
		, length: 25 // The length of each line
		, width: 12 // The line thickness
		, radius: 40 // The radius of the inner circle
		, scale: 1 // Scales overall size of the spinner
		, corners: 1 // Corner roundness (0..1)
		, color: '#000' // #rgb or #rrggbb or array of colors
		, opacity: 0.25 // Opacity of the lines
		, rotate: 0 // The rotation offset
		, direction: 1 // 1: clockwise, -1: counterclockwise
		, speed: 1 // Rounds per second
		, trail: 60 // Afterglow percentage
		, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
		, zIndex: 2e9 // The z-index (defaults to 2000000000)
		, className: 'spinner' // The CSS class to assign to the spinner
		, top: '150px' // Top position relative to parent (150)
		, left: '50%' // Left position relative to parent 
		, shadow: false // Whether to render a shadow
		, hwaccel: false // Whether to use hardware acceleration
		, position: 'absolute' // Element positioning
		}

		var opts2 = {
		  lines: 9 // The number of lines to draw
		, length: 25 // The length of each line
		, width: 12 // The line thickness
		, radius: 40 // The radius of the inner circle
		, scale: 1 // Scales overall size of the spinner
		, corners: 1 // Corner roundness (0..1)
		, color: '#000' // #rgb or #rrggbb or array of colors
		, opacity: 0.25 // Opacity of the lines
		, rotate: 0 // The rotation offset
		, direction: 1 // 1: clockwise, -1: counterclockwise
		, speed: 1 // Rounds per second
		, trail: 60 // Afterglow percentage
		, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
		, zIndex: 2e9 // The z-index (defaults to 2000000000)
		, className: 'spinner' // The CSS class to assign to the spinner
		, top: '550px' // Top position relative to parent (150)
		, left: '50%' // Left position relative to parent 
		, shadow: false // Whether to render a shadow
		, hwaccel: false // Whether to use hardware acceleration
		, position: 'absolute' // Element positioning
		}

		spinner = new Spinner(opts);
		spinner2 = new Spinner(opts2);
}

	function setupPaginationLinks () {
		$(".next-page-btn").click(function () {
			if ((page+1)<=pages) {
				var target = document.getElementById('content-page-wrapper');
				spinner.spin(target);
				
				var tagstring = GetTagString ();

				var cPage = page + 1;
				try {
				searchCircle.setMap(null);
			}
			catch(e) {

			}
			buildCircle();
				$.ajax({
					type:"POST",
					url:'api/atlas/retrieve_page',
					data:{tags:tagstring, 
					    latitude: map.getBounds().getSouthWest().lat(), 
					    longitude: map.getBounds().getSouthWest().lng(), 
					    latitudeM: map.getBounds().getNorthEast().lat(), 
					    longitudeM: map.getBounds().getNorthEast().lng(), 
					    reference:ref, 
					    page:(cPage-1),
					    rad: selectedRadius },
					error: function(){
							console.log('error');
							spinner.stop();
					},
					success: function(response) {
						spinner.stop();
						var resp = $.parseJSON( response );
						$('#content-page-wrapper').html(resp.html);
						Setups();
						BuildMapMarkers (resp.total);
						page+=1;
					}
				});
			}
		});

		$(".prev-page-btn").click(function () {
			if ((page-1)>=1) {
				var target = document.getElementById('content-page-wrapper');
				spinner.spin(target);

				var tagstring = GetTagString ();

				var cPage = page-1;
				try {
				searchCircle.setMap(null);
			}
			catch(e) {

			}
			buildCircle();
				$.ajax({
					type:"POST",
					url:'api/atlas/retrieve_page',
					data:{tags:tagstring, 
					    latitude: map.getBounds().getSouthWest().lat(), 
					    longitude: map.getBounds().getSouthWest().lng(), 
					    latitudeM: map.getBounds().getNorthEast().lat(), 
					    longitudeM: map.getBounds().getNorthEast().lng(), 
					    reference:ref, 
					    page:(cPage-1),
					    rad: selectedRadius },
					error: function(){
							console.log('error');
							spinner.stop();
					},
					success: function(response) {
						spinner.stop();
						var resp = $.parseJSON( response );
						$('#content-page-wrapper').html(resp.html);
						Setups();
						BuildMapMarkers (resp.total);
						page-=1;
					}
				});
			}
		});


		$(".first-page-btn").click(function () {
			var target = document.getElementById('content-page-wrapper');
			spinner.spin(target);

			var tagstring = GetTagString ();

			var cPage = 1;
			try {
				searchCircle.setMap(null);
			}
			catch(e) {

			}
			buildCircle();

			$.ajax({
				type:"POST",
				url:'api/atlas/retrieve_page',
				data:{tags:tagstring, 
					    latitude: map.getBounds().getSouthWest().lat(), 
					    longitude: map.getBounds().getSouthWest().lng(), 
					    latitudeM: map.getBounds().getNorthEast().lat(), 
					    longitudeM: map.getBounds().getNorthEast().lng(), 
					    reference:ref, 
					    page:(cPage-1),
					    rad: selectedRadius },
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					spinner.stop();
					var resp = $.parseJSON( response );
					$('#content-page-wrapper').html(resp.html);
					Setups();
					BuildMapMarkers (resp.total);
					page=1;
				}
			});
		});

		$(".last-page-btn").click(function () {
			var target = document.getElementById('content-page-wrapper');
			spinner.spin(target);

			var tagstring = GetTagString ();

			var cPage = pages;
			try {
				searchCircle.setMap(null);
			}
			catch(e) {

			}
			buildCircle();
			$.ajax({
				type:"POST",
				url:'api/atlas/retrieve_page',
				data:{tags:tagstring, 
					    latitude: map.getBounds().getSouthWest().lat(), 
					    longitude: map.getBounds().getSouthWest().lng(), 
					    latitudeM: map.getBounds().getNorthEast().lat(), 
					    longitudeM: map.getBounds().getNorthEast().lng(), 
					    reference:ref, 
					    page:(cPage-1),
					    rad: selectedRadius },
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					spinner.stop();
					var resp = $.parseJSON( response );
					$('#content-page-wrapper').html(resp.html);
					Setups();
					BuildMapMarkers (resp.total);
					page=pages;
				}
			});
		});

		$(".number-link").click(function () {
			var target = document.getElementById('content-page-wrapper');
			spinner.spin(target);

			var tagstring = GetTagString ();

			var cPage = parseInt($(this).text());

			try {
				searchCircle.setMap(null);
			}
			catch(e) {

			}
			buildCircle();
			$.ajax({
				type:"POST",
				url:'api/atlas/retrieve_page',
				data:{tags:tagstring, 
					    latitude: map.getBounds().getSouthWest().lat(), 
					    longitude: map.getBounds().getSouthWest().lng(), 
					    latitudeM: map.getBounds().getNorthEast().lat(), 
					    longitudeM: map.getBounds().getNorthEast().lng(), 
					    reference:ref, 
					    page:(cPage-1),
					    rad: selectedRadius },
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					spinner.stop();
					var resp = $.parseJSON( response );
					$('#content-page-wrapper').html(resp.html);
					Setups();
					BuildMapMarkers (resp.total);
					page = cPage;
				}
			});
		});
	}

	function PageQuery () {
		var cPage = 1;
		var target = document.getElementById('content-page-wrapper');
		spinner.spin(target);
		var tagstring = GetTagString ();
		
		try {
				searchCircle.setMap(null);
			}
			catch(e) {

			}
			buildCircle();

			$.ajax({
				type:"POST",
				url:'api/atlas/retrieve_page',
				data:{tags:tagstring, 
					    latitude: map.getBounds().getSouthWest().lat(), 
					    longitude: map.getBounds().getSouthWest().lng(), 
					    latitudeM: map.getBounds().getNorthEast().lat(), 
					    longitudeM: map.getBounds().getNorthEast().lng(), 
					    reference:ref, 
					    page:(cPage-1),
					    rad: selectedRadius },
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					spinner.stop();
					var resp = $.parseJSON( response );
					$('#content-page-wrapper').html(resp.html);
					Setups();
					BuildMapMarkers (resp.total);
					pages = resp.pages;
					page=1;
				}
		});
	}

	function BuildMapMarkers (total)	{
		var results =  $(".location-result");
		switch (total) {
			case 0: 
				$(".atlas-results-title").text("No se encontraron resultados");
			break;
			case 1:
				$(".atlas-results-title").text("1 resultado");
			break;
			default:
				if (total>100) {
					$(".atlas-results-title").text("Más de 100 resultados");
				} else if (total == null) {
					$(".atlas-results-title").text("No se encontraron resultados");
				} else {
					$(".atlas-results-title").text(total+" resultados");
				}
			break;
		}

		if (markers.length>0) {
			for (var m=0; m<markers.length; m++) {
				markers[m].setMap(null);
				markers[m].infobox.close();
			}
			markers = [];	
		}

		windows = [];

		for (var i=0; i<results.length; i++) {
			var _name = $(results[i]).data("name");
			var _lat = $(results[i]).data("lat").replace(",", ".");
			var _lng = $(results[i]).data("lng").replace(",", ".");
			var _rat = $(results[i]).data("rating");
			var _addr = $(results[i]).data("street");
			var _number = $(results[i]).data("itemid");
			var _tag = $(results[i]).data("tag");
			var _slug = $(results[i]).data("slug");
			var myLatLng = {lat: parseFloat(_lat), lng: parseFloat(_lng)};


			var image = 'assets/images/markers/pin-point.png';

			switch (_slug) {
				/*
				case  "Biblioteca":
						image='assets/images/markers/pin_bilbioteca.svg';
				break;
				case  "Otros":
						image='assets/images/markers/pin_otros.svg';
				break;
				case  "Centro de lectura":
						image='assets/images/markers/pin_centro.svg';
				break;
				case  "Sala de lectura":
						image='assets/images/markers/pin_salas.svg';
				break;
				case  "Librería":
						image='assets/images/markers/pin_libreria.svg';
				break;
				case  "Paralibros":
						image='assets/images/markers/pin_paralibro.svg';
				break;
				case  "Papelería":
						image='assets/images/markers/pin_papeleria.svg';
				break;
				case  "Tienda departamental":
						image='assets/images/markers/pin_tiendadep.svg';
				break;
				*/
				case  "biblioteca":
						image='assets/images/markers/pin_bilbioteca.svg';
				break;
				case  "otros":
						image='assets/images/markers/pin_otros.svg';
				break;
				case  "centro-de-lectura":
						image='assets/images/markers/pin_centro.svg';
				break;
				case  "sala-de-lectura":
						image='assets/images/markers/pin_salas.svg';
				break;
				case  "libreria":
						image='assets/images/markers/pin_libreria.svg';
				break;
				case  "paralibros":
						image='assets/images/markers/pin_paralibro.svg';
				break;
				case  "papelería":
						image='assets/images/markers/pin_papeleria.svg';
				break;
				case  "tienda-departamental":
						image='assets/images/markers/pin_tiendadep.svg';
				break;
			}

			/*var _marker = new google.maps.Marker({
				    position: myLatLng,
				    map: map,
				    label: ""+_number,
				    title: _name,
				    icon: image
				});*/

			var marker1 = new MarkerWithLabel({
       position: myLatLng,
       draggable: false,
       raiseOnDrag: false,
       map: map,
       labelContent: ""+_number,
       labelAnchor: new google.maps.Point(20, 42),
       labelClass: "marker-labels", // the CSS class for the label
       labelStyle: {opacity: 0.7},
       icon:image,
       zIndex: i,
       labelZIndex:0,
       labelInBackground: true
     });
			
			var cString = GetMarkerWindow(_name,_addr,_rat,_tag,$(results[i]).data("url"));

			AttachMessage(marker1, cString);

			markers.push(marker1);//_marker);
		}
	}

	function AttachMessage(marker, message) {
		marker.opened = false;

	  /*var infowindow = new google.maps.InfoWindow({
	    content: message,
	    disableAutoPan:true
	  });

	  marker.addListener('click', function() {
	  	if (!marker.opened)
	    	infowindow.open(marker.get('map'), marker);
	    else
	    	infowindow.close();
	    marker.opened=!marker.opened;
	  });*/
		var boxText = document.createElement("div");
		boxText.style.cssText = "color: black;"+
		                        "padding: 5px 15px 15px 15px;"+
		                        "border-radius:20px;";
		 
		var boxOptions = {
						alignBottom: true
		        ,content: boxText
		        ,disableAutoPan: false
		        ,maxWidth: 0
		        ,pixelOffset: new google.maps.Size(-125, -55)
		        ,zIndex: null
		        ,boxClass: "infobox-location"
		        ,closeBoxMargin: "15px 12px 2px 2px"
		        ,closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif"
		        ,infoBoxClearance: new google.maps.Size(5, 5)
		        ,isHidden: false
		        ,pane: "floatPane"
		        ,enableEventPropagation: false
		};
		var ib = new InfoBox();

		marker.infobox=ib;
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
		 
		    return function() {
		 						
		 						for (var i=0; i<markers.length; i++) {
		 							markers[i].infobox.close();
		 						}

		            ib.setOptions(boxOptions);
		 
				        boxText.innerHTML = message;
		 						
		 						//if (!marker.opened)
						    	ib.open(map, marker);
						    //else
						    	//ib.close();
						    //marker.opened=!marker.opened;
		    }
		 
		})(marker, i)); 
	}

	function GetMarkerWindow (lName,lAddr,lRating,lTag,lUrl) {
		var contentString = '<div class="gm-style-iw">'+
		      '<div id="siteNotice">'+
		      '</div><a href="'+lUrl+'" style="color:black; text-decoration:none;">'+
		      '<h2 id="firstHeading" class="location-infobox-name">'+lName+'</h2></a>'+
		      '<div id="bodyContent">'+
		      '<p class="location-infobox-address">'+lAddr+
		      '</p>';
		switch (lTag) {
			case  "Biblioteca":
					contentString+=('<span class="infobox-biblioteca">');
			break;
			case  "Otros":
					contentString+=('<span class="infobox-otros">');
			break;
			case  "Centro de lectura":
					contentString+=('<span class="infobox-centro">');
			break;
			case  "Sala de lectura":
					contentString+=('<span class="infobox-sala">');
			break;
			case  "Librería":
					contentString+=('<span class="infobox-libreria">');
			break;
		}
		contentString+=(lTag+'</span>');
		contentString+=('</div>'+
		      					'</div>');
		return contentString;
	}

	function GetTagString () {
		var retVal="";
		for (var i=0; i<tags.length; i++) {
			if (i==0)
				retVal+=tags[i];
			else
				retVal+=(","+tags[i]);
		}
		return retVal;
	}
</script>

<link rel="stylesheet" type="text/css" href="assets/styles/atlas.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/skins/minimal/green.css"/>
