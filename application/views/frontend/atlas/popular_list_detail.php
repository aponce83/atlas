<link rel="stylesheet" type="text/css" href="assets/styles/atlaslugarstyle.css"/>
<script async src="//static.addtoany.com/menu/page.js"></script>

<div class="lugar-lista-pop">
	
	<div class="title">
		<p>LUGARES MÁS POPULARES</p>
	</div>
	<hr>
	<div class="list-pop">
		<?php
		//var_dump($lista_pop);
		foreach ($lista_pop as $lugar) 
		{
			//var_dump($lugar);
			echo '<div class="col-xs-12 element">';
			echo '<div class= col-xs-4 element-img>';
			$imageurl='assets/images/'.$lugar->image; 
			if ($imageurl!=='assets/images/') 
			{
				echo '<a href="atlas-de-lectura/detalle/'.$lugar->id.'"><img class="foto-lugar-grande" src="assets/images/'.$lugar->image.'"  width=110 height=110></a>';
			}
			else
			{
				$index = count($lugar->tags);
				$index = $index -1;
				switch ($lugar->tags[$index]->slug)
				{
					case "biblioteca":
						echo '<a href="atlas-de-lectura/detalle/'.$lugar->id.'"><img src="assets/images/places/nodisp_detalle-biblioteca100x100.svg" class="header-icon" width=110 height=110></a>';
						break;
					case "libreria":
						echo '<a href="atlas-de-lectura/detalle/'.$lugar->id.'"><img src="assets/images/places/nodisp_detalle-libreria100x100.svg" class="header-icon" width=110 height=110></a>';
						break;
					case "centro-de-lectura":
						echo '<a href="atlas-de-lectura/detalle/'.$lugar->id.'"><img src="assets/images/places/nodisp_detalle-centro100x100.svg" class="header-icon" width=110 height=110></a>';
						break;
					case "sala-de-lectura":
						echo '<a href="atlas-de-lectura/detalle/'.$lugar->id.'"><img src="assets/images/places/nodisp_detalle-sala100x100.svg" class="header-icon" width=110 height=110></a>';
						break;
					case "paralibros":
						echo '<a href="atlas-de-lectura/detalle/'.$lugar->id.'"><img src="assets/images/places/nodisp_detalle-paralibros100x100.svg" class="header-icon" width=110 height=110></a>';
						break;
					case "otros":
						echo '<a href="atlas-de-lectura/detalle/'.$lugar->id.'"><img src="assets/images/places/nodisp_detalle-otros100x100.svg" class="header-icon" width=110 height=110></a>';
						break;
					case "tienda-departamental":
						echo '<a href="atlas-de-lectura/detalle/'.$lugar->id.'"><img src="assets/images/places/nodisp_detalle-tiendadep100x100.svg" class="header-icon" width=110 height=110></a>';
						break;
					case "papeleria":
						echo '<a href="atlas-de-lectura/detalle/'.$lugar->id.'"><img src="assets/images/places/nodisp_detalle-papeleria100x100.svg" class="header-icon" width=110 height=110></a>';
						break;
					default:
						echo '<a href="atlas-de-lectura/detalle/'.$lugar->id.'"><img src="assets/images/places/location_placeholder2.svg" class="header-icon" width=110 height=110></a>';
						break;
				}
				
			}
			//echo '<img src="'.$lugar->image.'" height=110 width=110>';
			echo '</div>';
			echo '<div class="col-xs-8 element-txt">';
			echo '<div class="col-xs-10 element-title"><a href="atlas-de-lectura/detalle/'.$lugar->id.'" class="element-title-link"><p>'.$lugar->name.'</p></a></div>';
			if ($lugar->rating != "" && !is_null($lugar->rating))
			{
				echo '<div class="col-xs-1 element-rate-side">'.$lugar->rating.'</div>';	
			}
			$direccion = "";
			 if ($lugar->street != "" && !is_null($lugar->street))
			 {
			 	$direccion = $direccion.$lugar->street;
			 }
			 if (!is_null($lugar->ext_number) && $lugar->ext_number != "")
		     {
		     	$direccion = $direccion." ".$lugar->ext_number;
		     }
		     if (!is_null($lugar->int_number) || $lugar->int_number != "")
		     {
		     	$direccion = $direccion.", interior ".$lugar->int_number;
		     }
		      if ($lugar->town != "" && !is_null($lugar->town))
			 {
			 	$direccion = $direccion.", ".$lugar->town;
			 }
			if ($direccion != "")
			{
				echo '<div class="col-xs-12 element-dir"><p>'.$direccion.'.</p></div>';	
			}
			
			echo '<div class="col-xs-12 element-tags">';
					foreach($lugar->tags as $tag)
					{
						
						echo '<span class="tag-'.$tag->slug.'">'.$tag->name.'</span>';
					}
			echo '</div>';
			echo '</div>';
			echo '<div style="clear: both;"></div>';
			echo '<div class="element-foot">';
			echo '<a href="atlas-de-lectura/detalle/'.$lugar->id.'"><p>ver más  <img src="assets/images/detail/arrow.svg"></p></a>';
			echo '</div>';
			echo '<hr>';
			echo '</div>';
			
		}
		?>
	</div>
	<div class="foot-pop">
		<a href="atlas-de-lectura/lugares-populares/1/24"><p>Conoce más lugares  <img src="assets/images/detail/arrow_red.svg"></p></a>
	</div>
</div>
