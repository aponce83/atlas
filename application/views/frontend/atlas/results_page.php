<?php 
	$paginationLinks = "";
	$nPages = $total_pages;
	$page = $current_page;

	if ($page>1) {
		$paginationLinks .= "<span class='first-page-btn' style='cursor: pointer;'><span class='last-btns'><i class='fa fa-angle-left arrow-link prev-page arrow-first'></i><i class='fa fa-angle-left arrow-link prev-page arrow-first'></i></span></span>";
		$paginationLinks .= "<span class='prev-page-btn' style='cursor: pointer;'><i class='fa fa-angle-left arrow-link prev-page arrow-prev'></i><span class='pagination-word prev-word'>anterior</span></span>";
	}

	for ($i = $page-3; $i<$page+2; $i++) {
		if ($i>=1 && $i<=$nPages) {
			if ($page==$i)
				$paginationLinks .= "<span class='number-bar'>|</span><span class='number-link' style='color:#3db29c!important; font-weight:600!important;'>".$i."</span>";		
			else
				$paginationLinks .= "<span class='number-bar'>|</span><span class='number-link'>".$i."</span>";		
		}
	}
		
	if ($page<$nPages) {
		$paginationLinks .= "<span class='next-page-btn' style='cursor: pointer;'><span class='number-bar'>|</span><span class='pagination-word'>siguiente</span><i class='fa fa-angle-right arrow-link next-page arrow-next'></i></span>";
		$paginationLinks .= "<span class='last-page-btn' style='cursor: pointer;'><span class='last-btns'><i class='fa fa-angle-right arrow-link prev-page arrow-last'></i><i class='fa fa-angle-right arrow-link prev-page arrow-last'></i></span></span>";
	}

?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
	<div class="row no-margin atlas-result-page-items">
		<!--Seccion para el filtro-->
		<?php if (count($locations)==0) { ?>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding atlas-no-results">
				No se encontraron resultados en el área.
			</div>
		<?php } else { $l=(($current_page*24)-24); 

											
			foreach ($locations as $location) { ?>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-padding">
				<div class="location-result" data-name="<?=$location->name?>" 
																		 data-lat="<?=$location->latitude?>" 
					                           data-lng="<?=$location->longitude?>" 
					                           data-rating="<?=$location->rating?>"
					                           data-eval="<?=$location->evaluations?>"
					                           data-street="<?=$location->street.$location->ext_number?>"
					                           data-tag="<?=$location->tags[0]->name?>" 
					                           data-slug="<?=$location->slug?>" 
					                           data-url="<?=URL::base()?>atlas-de-lectura/detalle/<?=$location->id?>"
					                           data-itemid="<?=($l+1)?>">
						<div class='row no-margin'>	
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding atlas-item-header">
									<div class="atlas-location-number yellow-number" style="cursor:pointer;">
										<?=($l+1)?>
									</div>
									<a href="<?=URL::base().'atlas-de-lectura/detalle/'.$location->id?>">
										<?php $imageurl='assets/images/'.$location->image; 
										if ($imageurl!=='assets/images/') 
											{
											?>
												<img src="assets/images/<?=$location->image?>" class="header-icon">									
											<?php 
											} 
											else 
											{ 
												//no hay imagen asignada, determinar el placeholder correspondiente
												$none_image = '';
												/*
												foreach($location->tags as $tag)
												{
													$none_image = $tag->slug;
												}
												*/
												$none_image = $location->slug;
												switch($none_image)
												{
													case 'libreria':
														$none_image = 'nodisp_busqueda-libreria100x100.svg';
														break;
													case 'biblioteca':
														$none_image = 'nodisp_busqueda-biblioteca100x100.svg';
														break;
													case 'centro-de-lectura':
														$none_image = 'nodisp_busqueda-centro100x100.svg';
														break;
													case 'otros-espacios':
														$none_image = 'nodisp_busqueda-otros100x100.svg';
														break;
													case 'paralibros':
														$none_image = 'nodisp_busqueda-paralibros100x100.svg';
														break;
													case 'sala-de-lectura':
														$none_image = 'nodisp_busqueda-sala100x100.svg';
														break;
													case 'tienda-departamental':
														$none_image = 'nodisp_busqueda-tiendadep100x100.svg';
														break;
													case 'papeleria':
														$none_image = 'nodisp_busqueda-papeleria100x100.svg';
														break;
													default: 
														$none_image = 'location_placeholder.svg';
												}
												?>
												<img src="assets/images/places/<?=$none_image?>" class="header-icon">
											<?php 
											}
											?>
									</a>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding atlas-data-col">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
											<span class="location-result-title"><a href="<?=URL::base().'atlas-de-lectura/detalle/'.$location->id?>"><?=LM::text_to_ellipsis($location->name, 70);?></a></span>
										</div>
										<!--
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
												<span class="location-result-likes">
												<?php if ($user) { ?>
													<span class="location-result-likes-link location-result-likes-link-<?=$location->id?>" data-locationid="<?=$location->id?>">
														<?=($location->userLikesThis($user['id'])?"Ya no me gusta":"Me gusta")?>
													</span> | 
												<?php } ?>	
												<span class="location-result-likecount location-result-likecount-<?=$location->id?>"><?=$location->likes?> me gusta</span>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
											<span class="location-result-address">
												<div class="atlas-location-rating">
													<?php for ($r=0; $r<5; $r++) { 
														if ($r<$location->rating) { ?>
															<img src="assets/images/detail/estrella_amarilla_sm.svg" class="location-rating-img">
														<?php } else { ?>
												    	<img src="assets/images/detail/estrella_gris_sm.svg" class="location-rating-img">
											    <?php } } ?>
												</div>
											</span>
										</div>
										-->
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 location-tags-col">
											<?php foreach ($location->tags as $tag) { ?>
													<span class="span-<?=$tag->slug?>"><?=$tag->name?></span>
											<?php } ?>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 location-more">
											<span><a href="<?=URL::base().'atlas-de-lectura/detalle/'.$location->id?>">ver más <i class="fa fa-chevron-right" style="color: black; font-size: 12px;"></i></a></span>
										</div>
								</div>
						</div>
				</div>		
			</div>
			<?php if (($l+1)%2==0) { ?>
		  	<div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
		 	<?php } ?>
		<?php $l++; } } ?>
	</div>

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding" style="text-align:right; padding-right: 35px!important; padding-left: 35px!important; margin-top:20px;">
		<?php if ($total_pages>1) { ?>
			<?=$paginationLinks?>
		<?php } ?>
	</div>
</div>