<link rel="stylesheet" type="text/css" href="assets/styles/atlas_comment_detail.css">
<script src='assets/scripts/spin.min.js'></script>
<script src='assets/scripts/jquery.spin.js'></script>
<script src='assets/scripts/mustache.min.js'></script>
<script async src="//static.addtoany.com/menu/page.js"></script>

<!--Commment-starts-->
<input type="hidden" name="location_id" id="location_id" value="<?php echo $location->id; ?>">
<div class="atlas_location_comments">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h2>COMENTARIOS</h2>
    </div>
  </div>
  <div class="row discussion-wrapper" id="load-here-discuss">
    <div class="discussion-spinner center">
      <span></span>
    </div>
  </div>
  <div class="row start-a-discussion-wrapper">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="start-a-discussion">
        <h3>¿Tienes algún comentario?</h3>
        <textarea 
          name="comment" 
          id="start-a-discussion" 
          rows="10"
          placeholder="Déjanos aquí una reseña del lugar"></textarea>
        <div class="start-a-discussion-action pull-right">
          <input
            type="button"
            class="lm-btn lm-btn-green btn-large"
            value="Comentar"
            onclick="create_discussion(null, this);">
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Template general para llamar una discusión -->
<script id="mst-discussion" type="text/html">
  {{#discussions}}
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 discussion">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topic main-topic">
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 topic-profile-photo">
          <a href="{{userfront_url}}">
            <img src="{{image}}" alt="{{fullname}}" class="profile-photo-normal img-circle">
          </a>
        </div>
        <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 topic-data">
          <div class="topic-header clear">
            <div class="topic-header-profile"><a href="{{userfront_url}}">{{fullname}}</a> hizo un comentario</div>
            <div class="topic-header-time">{{time_string}}</div>
          </div>
          <div class="topic-comment">
            {{description}}
          </div>
          <div class="topic-social">
            <div class="pull-left">
              <a href="javascript:void(0);" onclick="toggle_like({{id}}, this);">{{like_txt}}</a><span>{{like_count}}</span>
            </div>
            <div class="pull-right">
              <a href="javascript:void(0);"><img onclick="shareFB(&quot;Compartí la discusión {{description}} de {{fullname}} en LIBROSMEXICO.MX&quot;,&quot;{{image}}&quot;,&quot;www.google.com&quot;)" src="assets/images/detail/facebook_cita.svg" class="social-medios-mini pointer" style="cursor:pointer!important;"></a>
              <a class="a2a_button_twitter" target="_blank" onclick="window.open(&#34http://www.addtoany.com/add_to/twitter?linkurl=librosmexico.mx&amp;linkname=Comparto la discusión {{description}} de {{fullname}} vía @LibrosMexicoMX&amp;linknote=&#34,&#34_blank&#34,&#34top=200, left=200, width=450, height=500&#34)" rel="nofollow" aria-label="Twitter"><span class="a2a_svg a2a_s__default a2a_s_twitter" style="width: 16px; line-height: 13px; height: 12px; border-radius: 2px; background: url(&#34assets/images/detail/twitter_cita.svg&#34);background-size: 16px; display: inline-block;vertical-align: middle;cursor:pointer!important;"></span></a>
            </div>
          </div>
          <div class="topic-action">
            <textarea 
              placeholder="Responder"
              rows="1"
              onkeyup="comment_discussion(event, this);"
              data-discussion-id="{{discussion_id}}"></textarea>
          </div>
        </div>
      </div>
    </div>
    {{#comments}}
    <div class="row discussion-comments discussion-comments-{{comment_id}}">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topic direct-main-topic-child">
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 topic-profile-photo">
          <a href="{{userfront_url}}">
            <img src="{{image}}" alt="{{fullname}}" class="profile-photo-normal img-circle">
          </a>
        </div>
        <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 topic-data">
          <div class="topic-header clear">
            <div class="topic-header-profile"><a href="{{userfront_url}}">{{fullname}}</a> hizó un comentario</div>
            <div class="topic-header-time">{{time_string}}</div>
          </div>
          <div class="topic-comment">
            {{comment}}
          </div>
          <div class="topic-actions">
            <a href="javascript:void(0);" onclick="show_response(this);">Responder</a> | <a href="javascript:void(0);" onclick="toggle_like2({{id}}, this);">{{like_txt}}</a><span>{{like_count}}</span>
          </div>
          <div class="topic-action hide">
            <textarea 
              placeholder="Escribe un comentario" 
              rows="1"
              onkeyup="comment_comment(event, this);"
              data-comment-id="{{comment_id}}"></textarea>
          </div>
        </div>
      </div>
    </div>
      {{#comments_comments}}
    <div class="row discussion-comments-comments">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topic topic-child-child">
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 topic-profile-photo">
          <a href="{{userfront_url}}">
            <img src="{{image}}" alt="{{fullname}}" class="profile-photo-normal img-circle">
          </a>
        </div>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 topic-data">
          <div class="topic-header clear">
            <div class="topic-header-profile"><a href="{{userfront_url}}">{{fullname}}</a> hizó un comentario</div>
            <div class="topic-header-time">{{time_string}}</div>
          </div>
          <div class="topic-comment">
            {{comment}}
          </div>
          <div class="topic-actions">
            <a href="javascript:void(0);" onclick="show_response(this);">Responder</a> | <a href="javascript:void(0);" onclick="toggle_like2({{id}}, this);">{{like_txt}}</a><span>{{like_count}}</span>
          </div>
          <div class="topic-action hide">
            <textarea 
              placeholder="Escribe un comentario" 
              rows="1"
              onkeyup="comment_comment(event, this);"
              data-comment-id="{{comment_id}}"></textarea>
          </div>
        </div>
      </div>
    </div>
      {{/comments_comments}}
    {{/comments}}
  </div>
  {{/discussions}}
</script>

<script id="mst-comment" type="text/html">
{{#comments}}
<div class="row discussion-comments discussion-comments-{{comment_id}}">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topic direct-main-topic-child">
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 topic-profile-photo">
      <a href="{{userfront_url}}">
        <img src="{{image}}" alt="{{fullname}}" class="profile-photo-normal img-circle">
      </a>
    </div>
    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 topic-data">
      <div class="topic-header clear">
        <div class="topic-header-profile"><a href="{{userfront_url}}">{{fullname}}</a> hizó un comentario</div>
        <div class="topic-header-time">{{time_string}}</div>
      </div>
      <div class="topic-comment">
        {{comment}}
      </div>
      <div class="topic-actions">
        <a href="javascript:void(0);" onclick="show_response(this);">Responder</a> | <a href="javascript:void(0);" onclick="toggle_like2({{id}}, this);">{{like_txt}}</a><span>{{like_count}}</span>
      </div>
      <div class="topic-action hide">
        <textarea 
          placeholder="Escribe un comentario" 
          rows="1"
          onkeyup="comment_comment(event, this);"
          data-comment-id="{{comment_id}}"></textarea>
      </div>
    </div>
  </div>
</div>
{{/comments}}
</script>

<script id="mst-comment-comment" type="text/html">
{{#comments_comments}}
<div class="row discussion-comments-comments">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topic topic-child-child">
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 topic-profile-photo">
      <a href="{{userfront_url}}">
        <img src="{{image}}" alt="{{fullname}}" class="profile-photo-normal img-circle">
      </a>
    </div>
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 topic-data">
      <div class="topic-header clear">
        <div class="topic-header-profile"><a href="{{userfront_url}}">{{fullname}}</a> hizó un comentario</div>
        <div class="topic-header-time">{{time_string}}</div>
      </div>
      <div class="topic-comment">
        {{comment}}
      </div>
      <div class="topic-actions">
        <a href="javascript:void(0);" onclick="show_response(this);">Responder</a> | <a href="javascript:void(0);" onclick="toggle_like2({{id}}, this);">{{like_txt}}</a><span>{{like_count}}</span>
      </div>
      <div class="topic-action hide">
        <textarea 
          placeholder="Escribe un comentario" 
          rows="1"
          onkeyup="comment_comment(event, this);"
          data-comment-id="{{resource_id}}"></textarea>
      </div>
    </div>
  </div>
</div>
{{/comments_comments}}
</script>

<!-- Template general para llamar una discusión de robot-->
<script id="mst-robot-discussion" type="text/html">
  {{#robot_discussions}}
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 discussion">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topic main-topic">
        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 topic-profile-photo">
          <a href="javascript:void(0);">
            <img src="{{user.image}}" alt="{{user.fullname}}" class="profile-photo-normal img-circle">
          </a>
        </div>
        <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 topic-data">
          <div class="topic-header clear">
            <div class="topic-header-profile"><a href="javascript:void(0);">{{user.fullname}}</a> hizo un comentario</div>
          </div>
          <div class="topic-comment">
            {{description}}
          </div>
          <div class="topic-action">
            <textarea 
                placeholder="Escribe un comentario" 
                rows="1"
                onkeyup="comment_robot_discussion(event, this);"
                data-resource-type="{{resource_type}}"
                data-resource-id="{{resource_id}}"
                data-robot-id="{{user.id}}"
                data-robot-comment="{{description}}"
                data-is-a-robot="1"
                ></textarea>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{/robot_discussions}}
</script>

<script>
  /**
   * Opciones predeterminadas para el spinner
   * @type {Object}
   */
  var spinnerOpts = {
    lines: 7, 
    length: 0, 
    width: 3, 
    radius: 4,
    scale: 1,
    corners: 1,
    color: '#FFF',
    opacity: 0.25,
    rotate: 0,
    direction: 1,
    speed: 1,
    trail: 60,
    fps: 20,
    zIndex: 2e9,
    top: '50%',
    left: '50%',
    shadow: false,
    hwaccel: false,
    position: 'absolute'
  };

  /**
   * Realiza las funciones de like
   */
  function toggle_like(id, element) {
    console.log(id);
    console.log(element);
    console.log($(element).parent());
    console.log($(element).html());
    var locationId = <?=$location->id?>;
    var usr = -1;
    <?php if (isset($data->user)) { ?>
      usr = <?=$data->user?>;
    <?php } ?>
    var d_id = id;
    var likeState = "";
    if ($(element).text()=="Me gusta")
    {
      likeState="like";
    }
    else
    {
      likeState="dislike";
    }
    
    $.ajax({
      type:"POST",
      url:'atlas-de-lectura/discusion/me-gusta',
      data:{user:usr, 
            like_state:likeState, 
            discuss:id,
            location:locationId},
      error: function(){
      },
      success: function(response) {
        var resp = $.parseJSON( response );
        $(element).text(resp.like_state);
        if (resp.total_likes>0)
          $(element).next().text(" | "+resp.total_likes+" me gusta");
        else
          $(element).next().text("");
      }
    });
  }

  function toggle_like2(id, element) {
    console.log(id);
    console.log(element);
    console.log($(element).parent());
    console.log($(element).html());
    var locationId = <?=$location->id?>;
    var usr = -1;
    <?php if (isset($data->user)) { ?>
      usr = <?=$data->user?>;
    <?php } ?>
    var d_id = id;
    var likeState = "";
    if ($(element).text()=="Me gusta")
    {
      likeState="like";
    }
    else
    {
      likeState="dislike";
    }
    
    $.ajax({
      type:"POST",
      url:'atlas-de-lectura/comment/me-gusta',
      data:{user:usr, 
            like_state:likeState, 
            discuss:id,
            location:locationId},
      error: function(){
      },
      success: function(response) {
        var resp = $.parseJSON( response );
        $(element).text(resp.like_state);
        if (resp.total_likes>0)
          $(element).next().text(" | "+resp.total_likes+" me gusta");
        else
          $(element).next().text("");
      }
    });
  }

  /**
   * Agrega las discusiones de los robots a la vista a detalle, estás discusiones
   * no tienen comentarios
   * @param {object} data
   */
  function add_robot_discussions(data) {
    var template = $('#mst-robot-discussion').html();
    Mustache.parse(template);
    var render = Mustache.render(template, data);
    $('#load-here-discuss').append(render);
  }

  /**
   * Agrega las discusiones de los usuarios a la vista a detalle, estás discusiones
   * pueden contener comentarios
   * @param {object} data
   */
  function add_user_discussions(data) {
    var template = $('#mst-discussion').html();
    Mustache.parse(template);
    var render = Mustache.render(template, data);
    $('#load-here-discuss').append(render);
  }

  /**
   * Carga las discusiones
   */
  function load_discussions() {
    $('.discussion-spinner span').spin(spinnerOpts);
    var location_id = $('#location_id').val();
    var data = {location_id: location_id};
    $.ajax({
      url: "atlas-de-lectura/discusiones",
      type: "post",
      data: data,
      dataType: "json",
      success: function (data) {
        $('.discussion-spinner span').spin(false);
        //$('#test-data').text(JSON.stringify(data, null, 4));
        if (data.status == 'SUCCESS') {
          if (data.robot_discussions) {
            add_robot_discussions(data);
          }
          if (data.discussions) {
            add_user_discussions(data);
          }
        }
      }
    });
  }

  $(function() {
    // Manda a llamar el proceso de carga de las discusiones de la página
    load_discussions();
  });

  /**
   * Función que se manda a llamar cada que alguien le da enter en el comentario
   * de una discusión creada por un robot
   * @param  {event} event tipo de evento
   * @param  {element} o     elemento al que se le dió click en el enter
   */
  function comment_robot_discussion(event, o) {
    var keyCode = ('which' in event) ? event.which : event.keyCode;
    var comment = $(o).val();
    if (keyCode == 13 && comment.length > 5) {
      var resource_type = $(o).data('resource-type');
      var resource_id = $(o).data('resource-id');
      var robot_id = $(o).data('robot-id');
      var robot_comment = $(o).data('robot-comment');
      var data = {
        resource_type: resource_type,
        resource_id: resource_id,
        robot_id: robot_id,
        robot_comment: robot_comment,
        comment: comment
      };
      $.ajax({
        url: "atlas-de-lectura/discusiones/crear-robot",
        type: "post",
        data: data,
        dataType: "json",
        success: function (data) {
          $(o).parent().parent().parent().parent().parent().remove();
          var template = $('#mst-discussion').html();
          Mustache.parse(template);
          console.log(data);
          console.log(JSON.stringify(data));
          var render = Mustache.render(template, data);
          $('#load-here-discuss').prepend(render);
        }
      });
    }
  }

  /**
   * Crea la discusión
   */
  function create_discussion(event, o) {
    var comment = $('#start-a-discussion').val();
    if (comment.length > 5) {
      var resource_type = 'location';
      var resource_id = $('#location_id').val();
      var data = {
        resource_type: resource_type,
        resource_id: resource_id,
        comment: comment
      };
      $.ajax({
        url: "atlas-de-lectura/discusiones/crear",
        type: "post",
        data: data,
        dataType: "json",
        success: function (data) {
          //$('#test-data').text(JSON.stringify(data, null, 4));
          $('#start-a-discussion').val('');
          var template = $('#mst-discussion').html();
          Mustache.parse(template);
          var render = Mustache.render(template, data);
          $('#load-here-discuss').prepend(render);
        }
      });
    }
  }

  /**
   * Comentario en una discusión
   */
  function comment_discussion(event, o) {
    var comment = $(o).val();
    var discussion_id = $(o).data('discussion-id');
    var keyCode = ('which' in event) ? event.which : event.keyCode;
    if (keyCode == 13 && comment.length > 5) {
      var data = {
        discussion_id: discussion_id,
        comment: comment
      };
      $.ajax({
        url: "atlas-de-lectura/discusiones/comentar-discusion",
        type: "post",
        data: data,
        dataType: "json",
        success: function (data) {
          var parent = $(o).parent().parent().parent().parent().parent();
          var insert = $(parent).find('.row:eq(0)');
          var template = $('#mst-comment').html();
          Mustache.parse(template);
          var render = Mustache.render(template, data);
          $(insert).after(render);
          $(o).val('');
        }
      });
    }
  }

  /**
   * Comentario en un comentario
   */
  function comment_comment(event, o) {
    var comment = $(o).val();
    var comment_id = $(o).data('comment-id');
    var keyCode = ('which' in event) ? event.which : event.keyCode;
    if (keyCode == 13 && comment.length > 5) {
      var data = {
        comment_id: comment_id,
        comment: comment
      };
      $.ajax({
        url: "atlas-de-lectura/discusiones/comentar-comentario",
        type: "post",
        data: data,
        dataType: "json",
        success: function (data) {
          var insert = $(o).closest('.discussion-comments');
          if (insert.length == 0) {
            var resource_id = data.comments_comments[0].resource_id;
            var id = '.discussion-comments-'+resource_id;
            var insert = $(id);
          }
          var template = $('#mst-comment-comment').html();
          Mustache.parse(template);
          var render = Mustache.render(template, data);
          $(insert).after(render);
          $(o).val('');
        }
      });
    }
  }

  /**
   * Muestra la respuesta
   */
  function show_response(o) {
    $(o).parent().parent().find('.topic-action').removeClass('hide');
  }
</script>
<!--Comment-ends-->