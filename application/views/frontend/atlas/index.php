<link rel="stylesheet" type="text/css" href="assets/styles/custom_f.css"/>
<script type="text/javascript" src="assets/scripts/spin.min.js"></script>

<div class="search-bar-wrp">

	<?=$data->atlas_searchbar?>
	
</div>

<div class="home-atlas-wrapper">
	<div class="col-xs-12 home-title-section">
		<h1>Atlas de lectura</h1>
		<h2>Te ayudamos a encontrar tu sitio ideal de lectura</h2>
	</div>
</div>

<div class="home-atlas-wrapper">
	<div class="row">
		<div class="col-xs-12 images-sections-lnks">

			<!-- librerias -->
				<div class="col-xs-12 col-sm-6 col-md-4 image-section">
					<a href="<?=URL::base()?>atlas-de-lectura/busqueda?tag=Librería&lat=19.4326077&lng=-99.13320799999997&reference=Ciudad%20de%20México,%20D.F.,%20México;%20Ciudad%20de%20México">
						<div class="lnk-wrp atl-btn-orange">
							<div class="pic-info">
								<img src="assets/images/icon_libreria.svg">
								<span class="section-name">Librerías</span>
							</div>
						</div>	
					</a>

				</div>
			
			

			<!-- bibliotecas -->
			<div class="col-xs-12 col-sm-6 col-md-4 image-section">
				<a href="<?=URL::base()?>atlas-de-lectura/busqueda?tag=Biblioteca,Biblioteca+privada,Biblioteca+pública&lat=19.4326077&lng=-99.13320799999997&reference=Ciudad%20de%20México,%20D.F.,%20México;%20Ciudad%20de%20México">
					<div class="lnk-wrp atl-btn-green">
						<div class="pic-info">
							<img src="assets/images/icon_bilbiotecas.svg">
							<span class="section-name">Bibliotecas</span>
						</div>
					</div>
				</a>
			</div>

			<!-- centro de lectura -->
			<div class="col-xs-12 col-sm-6 col-md-4 image-section">
				<a href="<?=URL::base()?>atlas-de-lectura/busqueda?tag=Centro+de+lectura&lat=19.4326077&lng=-99.13320799999997&reference=Ciudad%20de%20México,%20D.F.,%20México;%20Ciudad%20de%20México">
					<div class="lnk-wrp atl-btn-blue">
						<div class="pic-info">
							<img src="assets/images/icon_centro-lectura.svg">
							<span class="section-name">Centro de lectura</span>
						</div>
					</div>
				</a>
			</div>

			<!-- sala de lectura -->
			<div class="col-xs-12 col-sm-6 col-md-4 image-section">
				<a href="<?=URL::base()?>atlas-de-lectura/busqueda?tag=Sala+de+lectura&lat=19.4326077&lng=-99.13320799999997&reference=Ciudad%20de%20México,%20D.F.,%20México;%20Ciudad%20de%20México">
					<div class="lnk-wrp atl-btn-red">
						<div class="pic-info">
							<img src="assets/images/icon_sala-lectura.svg">
							<span class="section-name">Sala de lectura</span>
						</div>
					</div>
				</a>
			</div>

			<!-- otros -->
			<div class="col-xs-12 col-sm-6 col-md-4 image-section">
				<a href="<?=URL::base()?>atlas-de-lectura/busqueda?tag=Paralibros,Libro+Club,Libros+para+llevar+y+traer,Tienda+departamental,Otros+espacios&lat=19.4326077&lng=-99.13320799999997&reference=Ciudad%20de%20México,%20D.F.,%20México;%20Ciudad%20de%20México">

					<div class="lnk-wrp atl-btn-yellow">
						<div class="pic-info">
							<img src="assets/images/icon_otros.svg">
							<span class="section-name">Otros</span>
						</div>
					</div>
				</a>
			</div>


			<!-- otros -->
			<div class="col-xs-12 col-sm-6 col-md-4 image-section">
				<a href="<?=URL::base()?>atlas-de-lectura/busqueda?tag=Paralibros&lat=19.4326077&lng=-99.13320799999997&reference=Ciudad%20de%20México,%20D.F.,%20México;%20Ciudad%20de%20México">

					<div class="lnk-wrp atl-btn-yellow2">
						<div class="pic-info">
							<img src="assets/images/icono_paralibros.svg">
							<span class="section-name">Paralibro</span>
						</div>
					</div>
				</a>
			</div>


		</div>
	</div>
</div>

<div class="home-atlas-wrapper">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center">
		  <a href="javascript:void(0);" data-toggle="modal" data-target="#new-location-modal">
		  	¿Quiéres agregar un nuevo lugar? Haz click aquí
		  </a>
		</div>
	</div>
</div>

<div class="home-atlas-wrapper">
	
	<div class="col-xs-12">
		<br><br>
		<h3 class="sub-section sub-orange">LUGARES CERCA DE TI</h3>
	</div>

	<div class="row">
		<div class="col-xs-12 home-section-wrapper" id="closest_places_container">
			

			<div class="row">
				<div class="col-xs-12 main-more-btn">
					<a class="main-more-btn-orange fix-padd-90" href="<?=URL::base()?>atlas-de-lectura/busqueda">Ver lugares</a>
				</div>
			</div>

			
		</div>
	</div>
	
	
	
</div>



<div class="more-commented-section">
	<div class="home-atlas-wrapper">
		<div class="col-xs-12">
			<h3 class="sub-section">LOS LUGARES MÁS COMENTADOS</h3>     
		</div>

		<div class="row">
			<div class="col-xs-12 home-section-wrapper">
				<?php foreach ($data->more_comment_places as $place) { 
					$address = '';
					if( $place->street!='' && $place->street!=NULL )
					{
						$address.=$place->street;
					}
					if( $place->ext_number!='' && $place->ext_number!=NULL )
					{
						$address.=' '.$place->ext_number;
					}
					if( $place->int_number!='' && $place->int_number!=NULL )
					{
						$address.=' '.$place->int_number;
					}
					if( $place->town!='' && $place->town!=NULL )
					{
						$address.=' '.$place->town;
					}
					if (strlen($address) > 80)
   						$address = LM::text_to_ellipsis($aux_name, 77);
				?>
				<div class="col-sm-6 col-md-4 block-info-section">
					<div class="section-container">
						<div class="panel-image">
							<?php if ($place->image == NULL || $place->image == '')
							{
								$none_image = '';
								/*
								foreach($place->tags as $tag)
								{
									$none_image = $tag->slug;
								}
								*/
								$none_image = $place->slug;
								switch($none_image)
								{
									case 'libreria':
										$none_image = 'nodisp_libreria_inicio.svg';
										break;
									case 'biblioteca':
										$none_image = 'nodisp_biblioteca_inicio.svg';
										break;
									case 'centro-de-lectura':
										$none_image = 'nodisp_centro_inicio.svg';
										break;
									case 'otros':
										$none_image = 'nodisp_otros_inicio.svg';
										break;
									case 'paralibros':
										$none_image = 'nodisp_paralibro_inicio.svg';
										break;
									case 'sala-de-lectura':
										$none_image = 'nodisp_sala_inicio.svg';
										break;
									case 'tienda-departamental':
										$none_image = 'nodisp_tiendadep_inicio.svg';
										break;
									case 'papeleria':
										$none_image = 'nodisp_papeleria_inicio.svg';
										break;
									default: 
										$none_image = 'nodisp_inicio.svg';
								}
							?>
								<a href="atlas-de-lectura/detalle/<?=$place->id?>">
									<img src="assets/images/places/<?=$none_image?>">
								</a>
							<?php }
							else
							{ ?>
								<a href="atlas-de-lectura/detalle/<?=$place->id?>">
									<img src="assets/images/<?=$place->image?>">
								</a>
							<?php } ?>
						</div>
						<div class="panel-inf white-panel-inf">
							<p class="inf-title">
								<a href="atlas-de-lectura/detalle/<?=$place->id?>">
									<?php
									$aux_name = $place->name;
									if (strlen($aux_name) > 50)
									{
										$aux_name = LM::text_to_ellipsis($aux_name, 47);
									}
	   								echo $aux_name;
									?>
								</a>
							</p>
							<p class="inf-address">
								<?=$address?>
							</p>
							
							<div class="fix-btm-panel">
								<!--
								<div class="stars-row">
									<?php for( $aux=0; $aux<$place->rating; $aux++ ) { ?>
										<img src="assets/images/detail/estrella_amarilla_xm.svg">
									<?php } ?>
									<?php for( $aux=$place->rating; $aux<5; $aux++ ) { ?>
										<img src="assets/images/detail/estrella_gris_xm.svg">
									<?php } ?>
								</div>
								<div class="likes-row">
									<span class="like-btn" data-locid="<?=$place->id?>" data-id="s2_id<?=$place->id?>" onclick="setLike(this)">
										<?php 
										if(isset($data->user))
										{
											if($place->userLikesThis($data->user))
											{
												echo 'Ya no me gusta';
											}
											else
											{
												echo 'Me gusta';
											} 
										}
										?>
									</span>
									<span class="likes-counter" id="s2_id<?=$place->id?>">
										<?php
										if(isset($data->user))
										{
											echo ' | ';
										}
										?>
									<?=$place->likes?> me gusta
									</span>
								</div>
								-->
								<div class="bottom-panel">
									<a href="atlas-de-lectura/detalle/<?=$place->id?>">
										ver más
										<img src="assets/images/icon_arrow.svg">
									</a>
									
								</div>
							</div>

						</div>
					</div>
				</div>
				<?php } ?>

				<div class="row">
					<div class="col-xs-12 main-more-btn">
						<a class="main-more-btn-blue fix-padd-100" href="atlas-de-lectura/sitios-mas-comentados/1/24">Ver lugares</a>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>

<div class="home-atlas-wrapper">

	
	<div class="col-xs-12">
		<h3 class="sub-section sub-red">VER LUGARES POPULARES</h3>
	</div>

	<div class="row">
		<div class="col-xs-12 home-section-wrapper">
			<?php foreach ($data->popular_places as $place) { 
				$address = '';
					if( $place->street!='' && $place->street!=NULL )
					{
						$address.=$place->street;
					}
					if( $place->ext_number!='' && $place->ext_number!=NULL )
					{
						$address.=' '.$place->ext_number;
					}
					if( $place->int_number!='' && $place->int_number!=NULL )
					{
						$address.=' '.$place->int_number;
					}
					if( $place->town!='' && $place->town!=NULL )
					{
						$address.=' '.$place->town;
					}
					if (strlen($address) > 80)
   						$address = LM::text_to_ellipsis($aux_name, 77);
				?>
			<div class="col-sm-6 col-md-4 block-info-section">
				<div class="section-container">
					<div class="panel-image">
						<?php if ($place->image == NULL || $place->image == '')
							{
								$none_image = '';
								foreach($place->tags as $tag)
								{
									$none_image = $tag->slug;
								}
								$none_image = $place->slug;
								switch($none_image)
								{
									case 'libreria':
										$none_image = 'nodisp_libreria_inicio.svg';
										break;
									case 'biblioteca':
										$none_image = 'nodisp_biblioteca_inicio.svg';
										break;
									case 'centro-de-lectura':
										$none_image = 'nodisp_centro_inicio.svg';
										break;
									case 'otros':
										$none_image = 'nodisp_otros_inicio.svg';
										break;
									case 'paralibros':
										$none_image = 'nodisp_paralibro_inicio.svg';
										break;
									case 'sala-de-lectura':
										$none_image = 'nodisp_sala_inicio.svg';
										break;
									case 'tienda-departamental':
										$none_image = 'nodisp_tiendadep_inicio.svg';
										break;
									case 'papeleria':
										$none_image = 'nodisp_papeleria_inicio.svg';
										break;
									default: 
										$none_image = 'nodisp_inicio.svg';
								}
							?>
							<a href="atlas-de-lectura/detalle/<?=$place->id?>">
								<img src="assets/images/places/<?=$none_image?>">
							</a>
						<?php }
						else
						{ ?>
							<a href="atlas-de-lectura/detalle/<?=$place->id?>">
								<img src="assets/images/<?=$place->image?>">
							</a>
						<?php } ?>
					</div>
					<div class="panel-inf">
						<p class="inf-title">
							<a href="atlas-de-lectura/detalle/<?=$place->id?>">
								<?php
								$aux_name = $place->name;
								if (strlen($aux_name) > 50)
								{
									$aux_name = LM::text_to_ellipsis($aux_name, 47);
								}
   								echo $aux_name;
								?>
							</a>
						</p>
						<p class="inf-address">
							<?=$address?>
						</p>

						<div class="fix-btm-panel">
							<!--
							<div class="stars-row">
								<?php for( $aux=0; $aux<$place->rating; $aux++ ) { ?>

									<img src="assets/images/detail/estrella_amarilla_xm.svg">
								<?php } ?>
								<?php for( $aux=$place->rating; $aux<5; $aux++ ) { ?>
									<img src="assets/images/detail/estrella_gris_xm.svg">
								<?php } ?>
							</div>
							<div class="likes-row">
								<span class="like-btn" data-locid="<?=$place->id?>" data-id="s1_id<?=$place->id?>" onclick="setLike(this)">
									<?php 
									if(isset($data->user))
									{
										if($place->userLikesThis($data->user))
										{
											echo 'Ya no me gusta';
										}
										else
										{
											echo 'Me gusta';
										} 
									}
									?>
								</span>
								<span class="likes-counter" id="s1_id<?=$place->id?>">
									<?php
										if(isset($data->user))
										{
											echo ' | ';
										}
										?>
									<?=$place->likes?> me gusta
								</span>
							</div>
							-->
							<div class="bottom-panel">
								<a href="atlas-de-lectura/detalle/<?=$place->id?>">
									ver más
									<img src="assets/images/icon_arrow.svg">
								</a>
								
							</div>
						</div>


					</div>
				</div>
			</div>
		<?php } ?>
			<div class="row">
				<div class="col-xs-12 main-more-btn">
					<a class="main-more-btn-red" href="atlas-de-lectura/lugares-populares/1/24">Conoce más lugares</a>
				</div>
			</div>

			
		</div>
	</div>
</div>
<div id="map" class="atlas-results-map"></div>
<?php
	echo View::factory('frontend/atlas/suggestion_modal_snippet');
?>











<?php /* Inicio de coodigo para barra de busqueda */ ?>
<script
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxe9PzkkqPpM0hiMBf3UxzZkuOCLbPOD8&libraries=places&language=es">
</script>

<script type="text/javascript">

	<?php 

	if( isset($data->user) )
	{
	?>
		console.log('usuario logueado');
	<?php }
	else
	{
	?>
		console.log('usuario NO logueado');
	<?php }

	?>

	var spinner;

	var localLat = 19.4326068;
    var localLng = -99.1353989;
    var geocoder;
    var localRef="Plaza de la Constitución, Centro, Ciudad de México, México";


	var selectedLat = 19.390519;
	var selectedLng = -99.4238064;

	$(document).ready(function(){
		initMap();
		SetupService();
		getLocation();

		SetupSpinner ();

		var target = document.getElementById('closest_places_container');
		spinner.spin(target);

		$("#map").hide();


		var currentWidth = 0;
		var currentHeight = 0;
		$('.panel-image').children('a').children('img').each(function(){
			
			currentWidth = $(this).width();

			currentHeight = currentWidth / 1.64;

			$(this).css('height', currentHeight);
		});

		$(window).resize(function(){
	        var currentWidth = 0;
			var currentHeight = 0;
			$('.panel-image').children('a').children('img').each(function(){
				
				currentWidth = $(this).width();

				currentHeight = currentWidth / 1.64;

				$(this).css('height', currentHeight);
			});
	    });



		$(".location-pointer").click(function() {

	    	$("#search-location").val(localRef);
	    	if ($(".tag-select-filters").val()!=null) {
					var filters = $(".tag-select-filters").val();
					var tag_filters="";
					for (var k=0; k<filters.length; k++) {
						var sample = filters[k];
						var trimmed = sample.replace(/\s/gi, "+");
						if (k==0)
							tag_filters+=trimmed;
						else
							tag_filters+=(","+trimmed);
					}
					window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
				}
				else {
					var full_filters = $("#tag-searchbar-select").data("fullsearch");
					window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
				}
	    });

		$(".search_btn-mbl").click(function () {
			$("#search-location").val(localRef);
	    	if ($(".tag-select-filters").val()!=null) {
					var filters = $(".tag-select-filters").val();
					var tag_filters="";
					for (var k=0; k<filters.length; k++) {
						var sample = filters[k];
						var trimmed = sample.replace(/\s/gi, "+");
						if (k==0)
							tag_filters+=trimmed;
						else
							tag_filters+=(","+trimmed);
					}
					window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
				}
				else {
					var full_filters = $("#tag-searchbar-select").data("fullsearch");
					window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
				}
		});

		$(".tag-select-filters").SumoSelect({ 
                placeholder:"Estoy buscando...",
                triggerChangeCombined: true,
                forceCustomRendering: true,
                captionFormat: "{0} Seleccionados"
        });

		$(".tag-select-filters").change(function () {
			//console.log($(this).val());
		});		
		
		$('html').delegate(".location-item", "click", function(e) {
			  $(".atlas-search-input").val($(this).data("info"));
			  selectedLat = $(this).data("lat");
			  selectedLng = $(this).data("lng");
			  //console.log(selectedLat+','+selectedLng);
			  $(".buscador-anadir").removeClass("show");
	  		$(".buscador-anadir").empty();
	  		e.stopPropagation();
		});

		$('html').click(function() {
			$(".buscador-anadir").removeClass("show");
	  	$(".buscador-anadir").empty();
		});

		$(".search_btn").click(function () {
			$("#search-location").val(localRef);
	    	if ($(".tag-select-filters").val()!=null) {
					var filters = $(".tag-select-filters").val();
					var tag_filters="";
					for (var k=0; k<filters.length; k++) {
						var sample = filters[k];
						var trimmed = sample.replace(/\s/gi, "+");
						if (k==0)
							tag_filters+=trimmed;
						else
							tag_filters+=(","+trimmed);
					}
					window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
				}
				else {
					var full_filters = $("#tag-searchbar-select").data("fullsearch");
					window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
				}
		});

		$(".atlas-search-input").val("<?=$data->search_reference?>");
		//$(".tag-select-filters")
});

function SetupSpinner () {
	var opts = {
		  lines: 7 // The number of lines to draw
		, length: 0 // The length of each line
		, width: 3 // The line thickness
		, radius: 4 // The radius of the inner circle
		, scale: 1 // Scales overall size of the spinner
		, corners: 1 // Corner roundness (0..1)
		, color: '#000' // #rgb or #rrggbb or array of colors
		, opacity: 0.25 // Opacity of the lines
		, rotate: 0 // The rotation offset
		, direction: 1 // 1: clockwise, -1: counterclockwise
		, speed: 0.9 // Rounds per second
		, trail: 59 // Afterglow percentage
		, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
		, zIndex: 2e9 // The z-index (defaults to 2000000000)
		, className: 'spinner' // The CSS class to assign to the spinner
		, top: '0px' // Top position relative to parent (150)
		, left: '50%' // Left position relative to parent 
		, shadow: false // Whether to render a shadow
		, hwaccel: false // Whether to use hardware acceleration
		, position: 'absolute' // Element positioning
		}

		spinner = new Spinner(opts);
		
}


function CreateGeocoder () {
		geocoder = new google.maps.Geocoder;
		geocodeLatLng(geocoder);
	}

	function geocodeLatLng(geocoder) {
	  var latlng = {lat: localLat, lng: localLng};
	  geocoder.geocode({'location': latlng}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {
	      if (results[0]) {	        
	        localRef=results[0].formatted_address;
	      } else {
	        console.log('No results found');
	      }
	    } else {
	      console.log('Geocoder failed due to: ' + status);
	    }
	  });
	}

	function getLocation() {
	    if (navigator.geolocation) {
	    	navigator.geolocation.getCurrentPosition(showPosition);
	        navigator.geolocation.getCurrentPosition(changePosition, showError);
	    } else { 
	    	
	    }
	}

	function showError (error) {
		//spinner2.stop();
		switch(error.code) {
	        case error.PERMISSION_DENIED:
	            console.log("User denied the request for Geolocation.");
	            showPosition(-1);
	            break;
	        case error.POSITION_UNAVAILABLE:
	            console.log("Location information is unavailable.");
	            showPosition(-1);
	            break;
	        case error.TIMEOUT:
	            console.log("The request to get user location timed out.");
	            showPosition(-1);
	            break;
	        case error.UNKNOWN_ERROR:
	            console.log("An unknown error occurred.");
	            showPosition(-1);
	            break;
	    }
	}

	function changePosition (position) {
		localLat = position.coords.latitude;
		localLng = position.coords.longitude;
		CreateGeocoder();
		//if (usemyPos) {
			lat = localLat;
			lng = localLng;
			map.setCenter(new google.maps.LatLng(lat,lng));
			//setTimeout(PageQuery, 200);
		//}
	}


// searchFriends abort
	var search_user_xhr;
	

	function searchFriends(o) {
		var searchTerms = $(o).val();
		searchTerms = searchTerms.trim();

		if ($(".buscador-anadir").hasClass("show")) {
			$(".buscador-anadir").removeClass("show");
		}

		if (searchTerms == "") {
			return;
		}

		 SendPlaceRequest (searchTerms);
	}

	
	function doSearch(e, o) {
		var str = $(o).val();
		if (e.keyCode === 13 || 
			str.length >= 3) {
			searchFriends(o);
		} else if (str.length <= 3) {
			$(".buscador-anadir").empty();
			$(".buscador-anadir").removeClass("show");
		}
	}

	function SetupService () {
		service = new google.maps.places.PlacesService(map);		
		
		var input = document.getElementById('search-location');
		var options = {
		  componentRestrictions: {country: 'mx'}
		};
		autocomplete = new google.maps.places.Autocomplete(input, options);
		autocomplete.addListener('place_changed',function(){
			var place = autocomplete.getPlace();
			selectedLat = place.geometry.location.lat();
			selectedLng = place.geometry.location.lng();
			if ($(".tag-select-filters").val()!=null) {
				var filters = $(".tag-select-filters").val();
				var tag_filters="";
				for (var k=0; k<filters.length; k++) {
					var sample = filters[k];
					var trimmed = sample.replace(/\s/gi, "+");
					if (k==0)
						tag_filters+=trimmed;
					else
						tag_filters+=(","+trimmed);
				}
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+selectedLat+"&lng="+selectedLng+"&reference="+$(".atlas-search-input").val();
			}
			else {
				var full_filters = $("#tag-searchbar-select").data("fullsearch");
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+selectedLat+"&lng="+selectedLng+"&reference="+$(".atlas-search-input").val();
			}
		});		
	}

	function SendPlaceRequest (text) {
		var request = {
			query: text,
			language: "es"
	  };

	  service.textSearch(request, searchCallback);
	}

	function searchCallback(results, status) {
		$(".buscador-anadir").empty();
	  if (status == google.maps.places.PlacesServiceStatus.OK) {
	  	if (results.length>0) {
	  		if (!$(".buscador-anadir").hasClass("show"))
	  			$(".buscador-anadir").addClass("show");
		    for (var i = 0; i < results.length; i++) {
		    	var appnd = "<div data-lat='"+results[i].geometry.location.lat()+"' data-lng='"+results[i].geometry.location.lng()+"' data-info='"+results[i].formatted_address+"; "+results[i].name+"' class='location-item'>"+results[i].formatted_address+"<br>"+results[i].name+"</div>";
		      $(".buscador-anadir").append(appnd);
		      
		    }
		  }
		  else {
		  	$(".buscador-anadir").removeClass("show");
		  	$(".buscador-anadir").empty();
		  }
	  }
	  else {
	  	$(".buscador-anadir").removeClass("show");
	  	$(".buscador-anadir").empty();
	  }
	}

	function initMap() {
	  map = new google.maps.Map(document.getElementById('map'), {
	      center: {lat: 19.390519, lng: -99.4238064},
	      zoom: 14,
	      scrollwheel: false
	  });
	}

</script>

<script>

function showPosition(position) {
	var aux_latitude = 0;
	var aux_longitude = 0;
	if( position == -1 )
	{
		// no se recibió posición, entonces posición será la del zócalo
		aux_latitude = 19.4326068;
        aux_longitude = -99.1353989;

	}
	else
	{
		//se recibió posición, entonces tomar las que se reciben como parámetro
		aux_latitude = position.coords.latitude;
        aux_longitude = position.coords.longitude;

	}
	//console.log(position);
    $.ajax({
        url: "near_places",
        type: "post",
        data: {
                latitude: aux_latitude,
                longitude: aux_longitude
            },
        dataType: "json"
        }).done(function(data){
        	renderNearPlaces(data);
        	spinner.stop();
        });
}

function renderNearPlaces(data)
{
	if (data.near_places == null) {
		return;
	}
	for(var aux=0; aux<data.near_places.length; aux++)
	{
		var address = '';
		if( data.near_places[aux]['street'] != '' && data.near_places[aux]['street']!= null )
			address += data.near_places[aux]['street'];
		if( data.near_places[aux]['ext_number'] != '' && data.near_places[aux]['ext_number']!= null )
			address += ' '+data.near_places[aux]['ext_number'];
		if( data.near_places[aux]['int_number'] != '' && data.near_places[aux]['int_number']!= null )
			address += ' '+data.near_places[aux]['int_number'];
		if( data.near_places[aux]['town'] != '' && data.near_places[aux]['town']!= null )
			address += ' '+data.near_places[aux]['town'];

		if( address.length > 80 )
			address = address.substring(0, 77) + '...';


		var htmlSection = '';
		htmlSection += '<div class="col-sm-6 col-md-4 block-info-section">';
		htmlSection += '<div class="section-container">';
			htmlSection += '<div class="panel-image">';
				if(data.near_places[aux]['image']==null || data.near_places[aux]['image']=='')
				{
					var noneImage = '';
					/*
					if (data.near_places[aux]['tags']) {
						if( data.near_places[aux]['tags'].length > 0 ) {
							for(var aux2=0; aux2<data.near_places[aux]['tags'].length; aux2++) {
								noneImage = data.near_places[aux]['tags'][aux2]['slug'];
							}
						}
					}
					*/
					noneImage = data.near_places[aux]['slug'];
					
					switch(noneImage)
					{
						case 'libreria':
							noneImage = 'nodisp_libreria_inicio.svg';
							break;
						case 'biblioteca':
							noneImage = 'nodisp_biblioteca_inicio.svg';
							break;
						case 'centro-de-lectura':
							noneImage = 'nodisp_centro_inicio.svg';
							break;
						case 'otros':
							noneImage = 'nodisp_otros_inicio.svg';
							break;
						case 'paralibros':
							noneImage = 'nodisp_paralibro_inicio.svg';
							break;
						case 'sala-de-lectura':
							noneImage = 'nodisp_sala_inicio.svg';
							break;
						case 'tienda-departamental':
							noneImage = 'nodisp_tiendadep_inicio.svg';
							break;
						case 'papeleria':
							noneImage = 'nodisp_papeleria_inicio.svg';
							break;
						default: 
							noneImage = 'nodisp_inicio.svg';
					}
					htmlSection += '<a href="atlas-de-lectura/detalle/'+ data.near_places[aux]['id'] +'">';
					htmlSection += '<img src="assets/images/places/'+ noneImage +'">';
					htmlSection += '</a>';
				}
					
				else
				{
					htmlSection += '<a href="atlas-de-lectura/detalle/'+ data.near_places[aux]['id'] +'">';
					htmlSection += '<img src="assets/images/'+ data.near_places[aux]['image'] +'">';
					htmlSection += '</a>';
				}
					
			htmlSection += '</div>';
			htmlSection += '<div class="panel-inf">';
				htmlSection += '<p class="inf-title">';
					htmlSection += '<a href="atlas-de-lectura/detalle/'+data.near_places[aux]['id']+'">';
						
						var aux_name = data.near_places[aux]['name'];
						if( data.near_places[aux]['name'].length > 50 )
							aux_name = aux_name.substr(0, 47) + '...';

						htmlSection += aux_name;
					htmlSection += '</a>';
				htmlSection += '</p>';
				htmlSection += '<p class="inf-address">';
					htmlSection += address;
				htmlSection += '</p>';

				
				htmlSection += '<div class="fix-btm-panel">';
				/*
				htmlSection += '<div class="stars-row">';
					for( var star=0; star<parseInt(data.near_places[aux]['rating']); star++ )
					{
						
						htmlSection += '<img src="assets/images/detail/estrella_amarilla_xm.svg">';
					}
					for( var star=parseInt(data.near_places[aux]['rating']); star<5; star++ )
					{
						htmlSection += '<img src="assets/images/detail/estrella_gris_xm.svg">';
					}
				htmlSection += '</div>';
				htmlSection += '<div class="likes-row">';
				var aux_text = '';
				var aux_pipe = '';
				
				if(data.near_places[aux]['evaluated_by_user'] != null )
				{
					if(data.near_places[aux]['evaluated_by_user'])
					{
						aux_text = 'Ya no me gusta ';
						aux_pipe = ' | ';
					}
					else
					{
						aux_text = 'Me gusta';
						aux_pipe = ' | ';
					}
						
				}
				
					htmlSection += '<span class="like-btn" data-locid="'+data.near_places[aux]['id']+'" data-id="s3_id'+data.near_places[aux]['id']+'" onclick="setLike(this)">'+ aux_text +' </span>';
					htmlSection += '<span class="likes-counter" id="s3_id'+data.near_places[aux]['id']+'">'+aux_pipe+data.near_places[aux]['likes']+' me gusta</span>';
				htmlSection += '</div>';
				*/
				htmlSection += '<div class="bottom-panel">';
					htmlSection += '<a href="atlas-de-lectura/detalle/'+data.near_places[aux]['id']+'">';
						htmlSection += 'ver más ';
						htmlSection += '<img src="assets/images/icon_arrow.svg">';
					htmlSection += '</a>';
				htmlSection += '</div>';
				htmlSection += '</div>';

			htmlSection += '</div>';
		htmlSection += '</div>';
	htmlSection += '</div>';

	$('#closest_places_container').append(htmlSection);
	}

	var rowHTML = $('#closest_places_container').children('.row');
	$('#closest_places_container').children('.row').remove();
	$('#closest_places_container').append(rowHTML);

}


function setLike (e) {
	  var usr = -1;
	  <?php if (isset($data->user)) { ?>
	  	usr = <?=$data->user?>;
	  <?php } ?>
	  var item_id = $(e).attr('data-id');
	  var l_id = $(e).attr('data-locid');
	  var likeState = "";

		if ($(e).text().search("Ya no me gusta") >= 0 )
			likeState="dislike";
		else
			likeState="like";

		$.ajax({
			type:"POST",
			url:'api/atlas/like_location',
			data:{user:usr, 
			    	like_state:likeState, 
			    	location:l_id },
			error: function(){
					console.log('error');
			},
			success: function(response) {
				var resp = $.parseJSON( response );
				$(e).text(resp.like_state);
				if (resp.total_likes>0)
					$("#"+item_id).text( ' | '+ resp.total_likes+" me gusta");
				else
					$("#"+item_id).text(" | 0 me gusta");
			}
		});
}


</script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="assets/scripts/select-widget-min.js"></script>
<script src="assets/scripts/jquery.sumoselect.min.js"></script>
<script src="assets/scripts/icheck.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/styles/drop-down.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/sumoselect.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/atlas.css"/>

<?php /* Fin de coodigo para barra de busqueda */ ?>


