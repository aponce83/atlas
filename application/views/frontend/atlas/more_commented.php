<link rel="stylesheet" type="text/css" href="assets/styles/custom_f.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/comparing_styles.css"/>
<script type="text/javascript" src="assets/scripts/spin.min.js"></script>

<div class="search-bar-wrp">

	<?=$data->atlas_searchbar?>
	
</div>

<div class="home-atlas-wrapper no-height">
	<div class="col-xs-12 home-title-section no-margin-bottom">
		<h1><a href="atlas-de-lectura">Atlas de lectura</a></h1>
	</div>
</div>


<div class="home-atlas-wrapper">

	
	<div class="col-xs-12">
		<h3 class="sub-section sub-blue">Lugares más comentados</h3>
	</div>




	<div class="col-xs-12 fix-25-margin-btm">
		<div class="col-xs-12 col-sm-8">
			<form class="form-inline">
				<div class="form-group">
					<label class="show-lbl" for="list_show">Mostrar:</label>
					<div class="select-list-wrapper">
						<select class="form-control list-show" name="list_show" id="list_show" onchange="location = this.options[this.selectedIndex].value;">
		                	<option <?php if($data->res_per_page==24) echo 'selected'; ?> value="atlas-de-lectura/sitios-mas-comentados/1/24">24 sitios</option>  
		                	<option <?php if($data->res_per_page==48) echo 'selected'; ?> value="atlas-de-lectura/sitios-mas-comentados/1/48">48 sitios</option>  
		                	<option <?php if($data->res_per_page==72) echo 'selected'; ?> value="atlas-de-lectura/sitios-mas-comentados/1/72">72 sitios</option>
		                	<option <?php if($data->res_per_page==96) echo 'selected'; ?> value="atlas-de-lectura/sitios-mas-comentados/1/96">96 sitios</option>   
	                	</select>
					</div>
				</div>
			</form>
		</div>
		<div class="col-xs-12 col-sm-4 align-r inf-pag">
		<?php
		$start  =  0;
		if($data->current_page==1)
			$start = 1;
		else
			$start = (($data->current_page - 1) * $data->res_per_page) + 1 ;
		$end 	=  0;
		if($data->current_page * $data->res_per_page > $data->total_locations )
			$end = $data->total_locations;
		else
			$end = $data->current_page * $data->res_per_page;
		?>
			<span id="inf-pag">Mostrar: <?php echo $start; ?> - <?php echo $end; ?> de <?=number_format($data->total_locations, 0, '', ',')?> lugares | pág. <?=$data->current_page?> de <?=$data->total_pages?></span>
		</div>
	</div>


	<div class="row">
		<div class="col-xs-12 home-section-wrapper">
			<?php foreach ($data->more_comment_places as $place) { 
				$address = '';
				if( $place->street!=='' )
					$address.=$place->street;
				if( $place->ext_number!=='' )
					$address.=' '.$place->ext_number;
				if( $place->int_number!=='' )
					$address.=' '.$place->int_number;
				if( $place->town!=='' )
					$address.=' '.$place->town;
				if (strlen($address) > 80)
   					$address = LM::text_to_ellipsis($aux_name, 77);
				?>
			<div class="col-sm-6 col-md-4 block-info-section fix-25-margin-btm">
				<div class="section-container">
					<div class="panel-image">
						<?php if ($place->image == NULL || $place->image == '')
							{
								$none_image = '';
								/*
								foreach($place->tags as $tag)
								{
									$none_image = $tag->slug;
								}
								*/
								$none_image = $place->slug;
								switch($none_image)
								{
									case 'libreria':
										$none_image = 'nodisp_libreria_inicio.svg';
										break;
									case 'biblioteca':
										$none_image = 'nodisp_biblioteca_inicio.svg';
										break;
									case 'centro-de-lectura':
										$none_image = 'nodisp_centro_inicio.svg';
										break;
									case 'otros':
										$none_image = 'nodisp_otros_inicio.svg';
										break;
									case 'paralibros':
										$none_image = 'nodisp_paralibro_inicio.svg';
										break;
									case 'sala-de-lectura':
										$none_image = 'nodisp_sala_inicio.svg';
										break;
									case 'tienda-departamental':
										$none_image = 'nodisp_tiendadep_inicio.svg';
										break;
									case 'papeleria':
										$none_image = 'nodisp_papeleria_inicio.svg';
										break;
									default: 
										$none_image = 'nodisp_inicio.svg';
								}
							?>
						<a href="atlas-de-lectura/detalle/<?=$place->id?>">
							<img src="assets/images/places/<?=$none_image?>">
						</a>
						<?php }
						else
						{ ?>
						<a href="atlas-de-lectura/detalle/<?=$place->id?>">
							<img src="assets/images/<?=$place->image?>">
						</a>
						<?php } ?>
					</div>
					<div class="panel-inf">
						<p class="inf-title">
							<a href="atlas-de-lectura/detalle/<?=$place->id?>">
								<?php
								$aux_name = $place->name;
								if (strlen($aux_name) > 50)
								{
									$aux_name = LM::text_to_ellipsis($aux_name, 47);
								}
   								echo $aux_name;
								?>
							</a>
						</p>
						<p class="inf-address">
							<?=$address?>
						</p>


						<div class="fix-btm-panel">
							<!--
							<div class="stars-row">
								<?php for( $aux=0; $aux<$place->rating; $aux++ ) { ?>
									<img src="assets/images/detail/estrella_amarilla_xm.svg">
								<?php } ?>
								<?php for( $aux=$place->rating; $aux<5; $aux++ ) { ?>
									<img src="assets/images/detail/estrella_gris_xm.svg">
								<?php } ?>
							</div>
							<div class="likes-row">
								<span class="like-btn" data-locid="<?=$place->id?>" data-id="s1_id<?=$place->id?>" onclick="setLike(this)">
									<?php 
									if(isset($data->user))
									{
										if($place->userLikesThis($data->user))
										{
											echo 'Ya no me gusta';
										}
										else
										{
											echo 'Me gusta';
										} 
									}
									?>
								</span>
								<span class="likes-counter" id="s1_id<?=$place->id?>">
									<?php
										if(isset($data->user))
										{
											echo ' | ';
										}
										?>
									<?=$place->likes?> me gusta
								</span>
							</div>
							-->
							<div class="bottom-panel">
								<a href="atlas-de-lectura/detalle/<?=$place->id?>">
									ver más
									<img src="assets/images/icon_arrow.svg">
								</a>
								
							</div>
						</div>
						




					</div>
				</div>
			</div>
		<?php } ?>



			<!-- <div class="row">
				<div class="col-xs-12 main-more-btn">
					<a class="main-more-btn-red" href="#">Conoce más lugares</a>
				</div>
			</div> -->

				<?php 
				$total_pages = $data->total_pages;
				$current_pag = $data->current_page;
				if($total_pages>1)
				{ 
				?>
				
				<div class="col-xs-12">
					<div class="col-xs-12 no-padding-sides pagination-links fix-25-margin-btm fix-top-border-pag">
						<?php 
						if($current_pag>1)
						{ 
						?>
							<a href="atlas-de-lectura/sitios-mas-comentados/<?php echo '1'; ?>/<?php echo $data->res_per_page; ?>"><<</a>
							<a href="atlas-de-lectura/sitios-mas-comentados/<?=$current_pag-1?>/<?php echo $data->res_per_page; ?>">anterior</a>
							<span>|</span>
						<?php 
						}
						
						if($current_pag>2)
						{ 
						?>
							<a href="atlas-de-lectura/sitios-mas-comentados/<?=$current_pag-2?>/<?php echo $data->res_per_page; ?>"><?=$current_pag-2?></a>
							<span>|</span>

						<?php 
						}
						if($current_pag>1)
						{ 
						?>
							<a href="atlas-de-lectura/sitios-mas-comentados/<?=$current_pag-1?>/<?php echo $data->res_per_page; ?>"><?=$current_pag-1?></a>
							<span>|</span>

						<?php 
						}

						echo $current_pag;
						echo '<span> |</span>';

						if($current_pag<$total_pages)
						{ 
						?>
							<a href="atlas-de-lectura/sitios-mas-comentados/<?=$current_pag+1?>/<?php echo $data->res_per_page; ?>"><?=$current_pag+1?></a>
							<span>|</span>
						<?php 
						}

						if($current_pag<$total_pages-1)
						{ 
						?>
							<a href="atlas-de-lectura/sitios-mas-comentados/<?=$current_pag+2?>/<?php echo $data->res_per_page; ?>"><?=$current_pag+2?></a>
							<span>|</span>
						<?php 
						}

						if($current_pag<$total_pages)
						{ 
						?>
							<a href="atlas-de-lectura/sitios-mas-comentados/<?=$current_pag+1?>/<?php echo $data->res_per_page; ?>">siguiente</a>
						<?php 
						}

						if($current_pag<$total_pages)
						{ 
						?>
							<a href="atlas-de-lectura/sitios-mas-comentados/<?=$total_pages?>/<?php echo $data->res_per_page; ?>">>></a>
						<?php 
						}

						
						?>
					</div>
				</div>

				<?php
				}
				?>
				
			

			
		</div>
	</div>
	
	
	
</div>





<div id="map" class="atlas-results-map"></div>












<?php /* Inicio de coodigo para barra de busqueda */ ?>
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBH23el0xVqCyH5LE3NIMyJgAqtoo9wMdE&libraries=places&language=es">
</script>

<script type="text/javascript">

	<?php 

	if( isset($data->user) )
	{
	?>
		console.log('usuario logueado');
	<?php }
	else
	{
	?>
		console.log('usuario NO logueado');
	<?php }

	?>

	var spinner;

	var localLat = 19.4326068;
    var localLng = -99.1353989;
    var geocoder;
    var localRef="Plaza de la Constitución, Centro, Ciudad de México, México";


	var selectedLat = 19.390519;
	var selectedLng = -99.4238064;

	$(document).ready(function(){
		initMap();
		SetupService();
		getLocation();

		SetupSpinner ();

		$("#map").hide();

		var target = document.getElementById('closest_places_container');
		spinner.spin(target);

		$("#map").css('height', '0px !important');
		$("#map").css('width', '0px');

		var currentWidth = 0;
		var currentHeight = 0;
		$('.panel-image').children('a').children('img').each(function(){
			
			currentWidth = $(this).width();

			currentHeight = currentWidth / 1.64;

			$(this).css('height', currentHeight);
		});

		$(window).resize(function(){
	        var currentWidth = 0;
			var currentHeight = 0;
			$('.panel-image').children('a').children('img').each(function(){
				
				currentWidth = $(this).width();

				currentHeight = currentWidth / 1.64;

				$(this).css('height', currentHeight);
			});
	    });

		



		$(".location-pointer").click(function() {
	    	$("#search-location").val(localRef);
	    	if ($(".tag-select-filters").val()!=null) {
					var filters = $(".tag-select-filters").val();
					var tag_filters="";
					for (var k=0; k<filters.length; k++) {
						var sample = filters[k];
						var trimmed = sample.replace(/\s/gi, "+");
						if (k==0)
							tag_filters+=trimmed;
						else
							tag_filters+=(","+trimmed);
					}
					window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
				}
				else {
					var full_filters = $("#tag-searchbar-select").data("fullsearch");
					window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+localLat+"&lng="+localLng+"&reference="+$(".atlas-search-input").val();
				}
	    });

		$(".search_btn-mbl").click(function () {
			if ($(".tag-select-filters").val()!=null) {
				var filters = $(".tag-select-filters").val();
				var tag_filters="";
				for (var k=0; k<filters.length; k++) {
					var sample = filters[k];
					var trimmed = sample.replace(/\s/gi, "+");
					if (k==0)
						tag_filters+=trimmed;
					else
						tag_filters+=(","+trimmed);
				}
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+selectedLat+"&lng="+selectedLng+"&reference="+$(".atlas-search-input").val();
			}
			else {
				var full_filters = $("#tag-searchbar-select").data("fullsearch");
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+selectedLat+"&lng="+selectedLng+"&reference="+$(".atlas-search-input").val();	
			}
		});

		$(".tag-select-filters").SumoSelect({ 
                placeholder:"Estoy buscando...",
                triggerChangeCombined: true,
                forceCustomRendering: true,
                captionFormat: "{0} Seleccionados"
        });

		$(".tag-select-filters").change(function () {
			console.log($(this).val());
		});		
		
		$('html').delegate(".location-item", "click", function(e) {
			  $(".atlas-search-input").val($(this).data("info"));
			  selectedLat = $(this).data("lat");
			  selectedLng = $(this).data("lng");
			  console.log(selectedLat+','+selectedLng);
			  $(".buscador-anadir").removeClass("show");
	  		$(".buscador-anadir").empty();
	  		e.stopPropagation();
		});

		$('html').click(function() {
			$(".buscador-anadir").removeClass("show");
	  	$(".buscador-anadir").empty();
		});

		$(".search_btn").click(function () {
			if ($(".tag-select-filters").val()!=null) {
				var filters = $(".tag-select-filters").val();
				var tag_filters="";
				for (var k=0; k<filters.length; k++) {
					var sample = filters[k];
					var trimmed = sample.replace(/\s/gi, "+");
					if (k==0)
						tag_filters+=trimmed;
					else
						tag_filters+=(","+trimmed);
				}
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+selectedLat+"&lng="+selectedLng+"&reference="+$(".atlas-search-input").val();
			}
			else {
				var full_filters = $("#tag-searchbar-select").data("fullsearch");
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+selectedLat+"&lng="+selectedLng+"&reference="+$(".atlas-search-input").val();
			}
		});

		$(".atlas-search-input").val("<?=$data->search_reference?>");
		//$(".tag-select-filters")
});

function SetupSpinner () {
	var opts = {
		  lines: 7 // The number of lines to draw
		, length: 0 // The length of each line
		, width: 3 // The line thickness
		, radius: 4 // The radius of the inner circle
		, scale: 1 // Scales overall size of the spinner
		, corners: 1 // Corner roundness (0..1)
		, color: '#000' // #rgb or #rrggbb or array of colors
		, opacity: 0.25 // Opacity of the lines
		, rotate: 0 // The rotation offset
		, direction: 1 // 1: clockwise, -1: counterclockwise
		, speed: 0.9 // Rounds per second
		, trail: 59 // Afterglow percentage
		, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
		, zIndex: 2e9 // The z-index (defaults to 2000000000)
		, className: 'spinner' // The CSS class to assign to the spinner
		, top: '0px' // Top position relative to parent (150)
		, left: '50%' // Left position relative to parent 
		, shadow: false // Whether to render a shadow
		, hwaccel: false // Whether to use hardware acceleration
		, position: 'absolute' // Element positioning
		}

		spinner = new Spinner(opts);
		
}


function CreateGeocoder () {
		geocoder = new google.maps.Geocoder;
		geocodeLatLng(geocoder);
	}

	function geocodeLatLng(geocoder) {
	  var latlng = {lat: localLat, lng: localLng};
	  geocoder.geocode({'location': latlng}, function(results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {
	      if (results[0]) {	        
	        localRef=results[0].formatted_address;
	      } else {
	        console.log('No results found');
	      }
	    } else {
	      console.log('Geocoder failed due to: ' + status);
	    }
	  });
	}

	function getLocation() {
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(changePosition, showError);
	    } else { 
	    	
	    }
	}

	function showError (error) {
		//spinner2.stop();
		switch(error.code) {
	        case error.PERMISSION_DENIED:
	            console.log("User denied the request for Geolocation.");
	            break;
	        case error.POSITION_UNAVAILABLE:
	            console.log("Location information is unavailable.");
	            break;
	        case error.TIMEOUT:
	            console.log("The request to get user location timed out.");
	            break;
	        case error.UNKNOWN_ERROR:
	            console.log("An unknown error occurred.");
	            break;
	    }
	}

	function changePosition (position) {
		localLat = position.coords.latitude;
		localLng = position.coords.longitude;
		CreateGeocoder();
		//if (usemyPos) {
			lat = localLat;
			lng = localLng;
			map.setCenter(new google.maps.LatLng(lat,lng));
			//setTimeout(PageQuery, 200);
		//}
	}


// searchFriends abort
	var search_user_xhr;
	

	function searchFriends(o) {
		var searchTerms = $(o).val();
		searchTerms = searchTerms.trim();

		if ($(".buscador-anadir").hasClass("show")) {
			$(".buscador-anadir").removeClass("show");
		}

		if (searchTerms == "") {
			return;
		}

		 SendPlaceRequest (searchTerms);
	}

	
	function doSearch(e, o) {
		var str = $(o).val();
		if (e.keyCode === 13 || 
			str.length >= 3) {
			searchFriends(o);
		} else if (str.length <= 3) {
			$(".buscador-anadir").empty();
			$(".buscador-anadir").removeClass("show");
		}
	}

	function SetupService () {
		service = new google.maps.places.PlacesService(map);		
		
		var input = document.getElementById('search-location');
		var options = {
		  componentRestrictions: {country: 'mx'}
		};
		autocomplete = new google.maps.places.Autocomplete(input, options);
		autocomplete.addListener('place_changed',function(){
			var place = autocomplete.getPlace();
			selectedLat = place.geometry.location.lat();
			selectedLng = place.geometry.location.lng();
			if ($(".tag-select-filters").val()!=null) {
				var filters = $(".tag-select-filters").val();
				var tag_filters="";
				for (var k=0; k<filters.length; k++) {
					var sample = filters[k];
					var trimmed = sample.replace(/\s/gi, "+");
					if (k==0)
						tag_filters+=trimmed;
					else
						tag_filters+=(","+trimmed);
				}
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+tag_filters+"&lat="+selectedLat+"&lng="+selectedLng+"&reference="+$(".atlas-search-input").val();
			}
			else {
				var full_filters = $("#tag-searchbar-select").data("fullsearch");
				window.location="<?=URL::base()?>atlas-de-lectura/busqueda?tag="+full_filters+"&lat="+selectedLat+"&lng="+selectedLng+"&reference="+$(".atlas-search-input").val();
			}
		});		
	}

	function SendPlaceRequest (text) {
		var request = {
			query: text,
			language: "es"
	  };

	  service.textSearch(request, searchCallback);
	}

	function searchCallback(results, status) {
		$(".buscador-anadir").empty();
	  if (status == google.maps.places.PlacesServiceStatus.OK) {
	  	if (results.length>0) {
	  		if (!$(".buscador-anadir").hasClass("show"))
	  			$(".buscador-anadir").addClass("show");
		    for (var i = 0; i < results.length; i++) {
		    	var appnd = "<div data-lat='"+results[i].geometry.location.lat()+"' data-lng='"+results[i].geometry.location.lng()+"' data-info='"+results[i].formatted_address+"; "+results[i].name+"' class='location-item'>"+results[i].formatted_address+"<br>"+results[i].name+"</div>";
		      $(".buscador-anadir").append(appnd);
		      
		    }
		  }
		  else {
		  	$(".buscador-anadir").removeClass("show");
		  	$(".buscador-anadir").empty();
		  }
	  }
	  else {
	  	$(".buscador-anadir").removeClass("show");
	  	$(".buscador-anadir").empty();
	  }
	}

	function initMap() {
	  map = new google.maps.Map(document.getElementById('map'), {
	      center: {lat: 19.390519, lng: -99.4238064},
	      zoom: 14,
	      scrollwheel: false
	  });
	}

</script>

<script>


function setLike (e) {
	  var usr = -1;
	  <?php if (isset($data->user)) { ?>
	  	usr = <?=$data->user?>;
	  <?php } ?>
	  var item_id = $(e).attr('data-id');
	  var l_id = $(e).attr('data-locid');
	  var likeState = "";
	  if ($(e).text()=="Me gusta")
	  	likeState="like";
	  else
	  	likeState="dislike";

	  $.ajax({
			type:"POST",
			url:'api/atlas/like_location',
			data:{user:usr, 
			    	like_state:likeState, 
			    	location:l_id },
			error: function(){
					console.log('error');
			},
			success: function(response) {
				var resp = $.parseJSON( response );
				$(e).text(resp.like_state);
				if (resp.total_likes>0)
					$("#"+item_id).text( ' | '+ resp.total_likes+" me gusta");
				else
					$("#"+item_id).text(" | 0 me gusta");
			}
		});
}


</script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="assets/scripts/select-widget-min.js"></script>
<script src="assets/scripts/jquery.sumoselect.min.js"></script>
<script src="assets/scripts/icheck.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/styles/drop-down.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/sumoselect.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/atlas.css"/>

<?php /* Fin de coodigo para barra de busqueda */ ?>


