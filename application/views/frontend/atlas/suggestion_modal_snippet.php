<link rel="stylesheet" type="text/css" href="assets/styles/atlas_suggestion_modal_snippet.css"/>
<div class="modal fade" tabindex="-1" role="dialog" id="new-location-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Nueva ubicación</h4>
      </div>
      <form method="post" action="api/atlas/submit" class="lm_modal_ajax_sender">
      <input type="hidden" name="contact[title]" value="Nueva ubicación">
      <div class="modal-body">
        <p>¿Conoces algún lugar qué no se encuentra en nuestra base de datos? Llena el siguiente formulario y pronto lo verás en LIBROSMÉXICO.MX</p>
        <div class="form-input">
          <p class='text-label'>¿Cuál es el nombre de la ubicación?</p>
          <input type='text' name='contact[name]' placeholder='Escribe el nombre de la ubicación'>
        </div>
        <div class="form-input">
          <p class='text-label'>¿En dónde se encuentra?</p>
          <input type='text' name='contact[address]' placeholder='Escribe la dirección del lugar'>
        </div>
        <div class="form-input">
          <p class='text-label'>¿Qué tipo de lugar es?</p>
          <div class="select-list-wrapper">
            <select name=''>
              <option value="-">-</option>
              <option value="Biblioteca">Biblioteca</option>
              <option value="Librería">Librería</option>
              <option value="Centro de Lectura">Centro de Lectura</option>
              <option value="Sala de Lectura">Sala de Lectura</option>
              <option value="Paralibro">Paralibro</option>
              <option value="Otro">Otro</option>
            </select>
          </div>
        </div>
        <div class="form-input">
          <p class='text-label'>¿Cuál es tu correo electrónico?</p>
          <input type='text' name='contact[his_email]' placeholder='Solo lo usaremos por si tenemos dudas de la ubicación :)'>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="lm-btn-grey" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="lm-btn lm-btn-blue">Guardar</button>
        <button type="button" class="lm-btn lm-btn-blue lm-btn-close hide" data-dismiss="modal">Cerrar</button>
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
  function ajax_response_modal(element) {
    return function (data) {
      $(element).find('.modal-footer .lm-btn-grey').addClass('hide');
      $(element).find('.modal-footer .lm-btn-blue').addClass('hide');
      $(element).find('.modal-footer .lm-btn-close').removeClass('hide');
      $(element).find('.modal-body').html(data.message);
    };
  }
  $(function() {
    $('.lm_modal_ajax_sender').submit(function(e) {
      e.preventDefault();
      console.log('se manda a llamar');
      var method = $(this).attr('method');
      var action = $(this).attr('action');

      $.ajax({
        url: action,
        type: method,
        data: $(this).serialize(),
        dataType: "json",
        success: ajax_response_modal($(this))
      });
    });
  });
</script>