<div class="row no-margin" id="page-content-id">
	<div class="row no-margin">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-5 no-padding top-item">
			<div class="form-group">
				<div class="col-xs-4 col-sm-4 col-md-3 col-lg-4 no-padding">
		      <label for="list_order" class="show-label">Ordenar por:</label>
		    </div>
	      <div class="col-xs-8 col-sm-6 col-md-7 col-lg-7 no-padding">
		      <select class="form-control" name="page_order" id="page_order" style="background-image: url('assets/images/backend/bullet_arrow_down.png'); background-repeat: no-repeat; background-position: 96% center;">
		        <option value="num">Seleccione</option>
		        <option value="fecha_colofon">Más recientes</option>
		        <option value="titulo">Título (A-Z)</option>
		        <option value="editorial">Editorial (A-Z)</option>
		      </select>
		    </div>
		    <div class="hidden-xs col-sm-2 col-md-2 col-lg-1 no-padding"></div>
	    </div>
		</div>
		<div class="col-xs-12 col-sm-5 col-md-5 col-lg-3 no-padding top-item">
			<div class="form-group">
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-5 no-padding">
	      	<label for="list_show" class="show-label">Mostrar:</label>
	      </div>
	      <div class="col-xs-8 col-sm-8 col-md-8 col-lg-7 no-padding">
		      <select class="form-control" name="page_show" id="page_show" style="background-image: url('assets/images/backend/bullet_arrow_down.png'); background-repeat: no-repeat; background-position: 96% center;">
		      	<option selected disabled>Seleccione</option>
		        <option value="16">16 Libros</option>
		        <option value="32">32 Libros</option>
		        <option value="48">48 Libros</option>
		        <option value="64">64 Libros</option>
		      </select>
	      </div>
	    </div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 no-padding top-item pages-item">
			<span><?=$min_items.' - '.$max_items.' de '.number_format($main_content['item_count'],0,'',',')?></span><span class="page-divider">|</span><span><?='página '.$page.' de '.$main_content['pages']?></span>
		</div>
	</div>
	<div class="dsk-destacados" id="main-content-dsk">
		<?php $i=1; foreach($main_content['books'] as $nov) { ?>
			<a href="<?=URL::base().'libros/'.$nov['id_libro']?>">
				<div class="col col-xs-6 col-sm-6 col-md-4 col-lg-3 no-padding" style="overflow:hidden;">
					<div class="book-wrapper">
						<img alt="<?=$nov['titulo']?>" src="<?=!$nov['imagen']?'assets/images/generic_book.jpg':$nov['imagen']?>" />
						<?php if (isset($nov['tipo_producto']) && $nov['tipo_producto']!=="Libro" && $nov['tipo_producto']!=="") { ?>
							<?php if ($nov['tipo_producto'] === 'Libros electrónicos') { ?>
								<p class="home-type">Libro electrónico</p>
							<?php } else { ?>
								<p class="home-type"><?=$nov['tipo_producto']?></p>
							<?php } ?>
						<?php } ?>
							<div class="home-book-info-mask hidden">
								<p class="home-title"><?=$nov['titulo']?></p>
	              <?php $a=0; foreach($nov['autores'] as $aut) { if ($a==0) { ?>
									<p class="home-autor"><?=$aut[0]['nombre']?></p>
								<?php } $a++; } ?>
	              <?php $ed =0; foreach ($nov['editorial'] as $edit) { 
									if ($ed<2) { ?>
	              	<p class="home-editorial"><?=$edit['nombre'] ?></p>
	              <?php } $ed++; } ?>
	              <?php if (count($nov['editorial'])>2) { ?>
	              	<p class="home-editorial">...</p>
	              <?php } ?>
	              <?php if (count($nov['keywords'])>0) { ?>
	              	<?php if ( !(count($nov['keywords'])==1 && $nov[0]=='') ) { ?>
		              	<div class="key-words-home">
			              	<p>Palabras clave:</p>
			              	<?php $w=0; foreach($nov['keywords'] as $keyword) { 
			              		if ($w<5) { ?>
			                  	<span class="sp-keyword-home"><?=$keyword?></span>
			                <?php } $w++; } ?>
			              </div>
			            <?php } ?>  
		            <?php } ?>
	            </div>										
					</div>
	      </div>
	    </a>
	    <?php if ($i%2==0) { ?>
	    	<div class="clearfix visible-xs-block"></div>
				<div class="clearfix visible-sm-block"></div>
			<?php } ?>
			<?php if ($i%3==0) { ?>
				<div class="clearfix visible-md-block"></div>
			<?php } ?>
			<?php if ($i%4==0) { ?>
				<div class="clearfix visible-lg-block"></div>
			<?php } ?>
	  <?php } ?>
	</div>
	<div class="mbl-destacados" id="main-content-mal">
		<?php foreach($main_content['books'] as $nov) { ?>
			<a href="<?=URL::base().'libros/'.$nov['id_libro']?>" class="novedades-mbl-item">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding book-mbl-container">
					<div class="row no-margin">
						<div class="col-xs-3 no-padding">
							<img alt="<?=$nov['titulo']?>" src="<?=!$nov['imagen']?'assets/images/generic_book.jpg':$nov['imagen']?>" class="mbl-book-portrait" />
						</div>
						<div class="col-xs-9 no-padding">
							<span class="mbl-book-name">
								<?=$nov['titulo']?>
							</span>
							<span class="mbl-book-author">
								<?php $a=0; foreach($nov['autores'] as $aut) { if ($a==0) { ?>
									<?=$aut[0]['nombre']?>
								<?php } $a++; } ?>												
							</span>
							<span class="mbl-book-author">
								<?php $ed =0; foreach ($nov['editorial'] as $edit) { 
									if ($ed<2) { ?>
	              	<p class="home-editorial"><?=$edit['nombre'] ?></p>
	              <?php } $ed++; } ?>
	              <?php if (count($nov['editorial'])>2) { ?>
	              	<p class="home-editorial">...</p>
	              <?php } ?>
							</span>
						</div>
					</div>
		    </div>
	    </a>					    
	  <?php } ?>
	</div>
</div>

<?php
	$paginationLinks = "";
	$nPages = $main_content['pages'];

	if ($page>1) {
		$paginationLinks .= "<span class='first-page-btn' style='cursor: pointer;'><span class='last-btns'><i class='fa fa-angle-left arrow-link prev-page arrow-first'></i><i class='fa fa-angle-left arrow-link prev-page arrow-first'></i></span></span>";
		$paginationLinks .= "<span class='prev-page-btn' style='cursor: pointer;'><i class='fa fa-angle-left arrow-link prev-page arrow-prev'></i><span class='pagination-word prev-word'>anterior</span></span>";
	}

	for ($i = $page-2; $i<$page+2; $i++) {
		if ($i>=1 && $i<=$nPages) {
			if ($page==$i)
				$paginationLinks .= "<span class='number-bar'>|</span><span class='number-link' style='color:#3db29c!important; font-weight:600!important;'>".$i."</span>";		
			else
				$paginationLinks .= "<span class='number-bar'>|</span><span class='number-link'>".$i."</span>";		
		}
	}
		
	if ($page<$nPages) {
		$paginationLinks .= "<span class='next-page-btn' style='cursor: pointer;'><span class='number-bar'>|</span><span class='pagination-word'>siguiente</span><i class='fa fa-angle-right arrow-link next-page arrow-next'></i></span>";
		$paginationLinks .= "<span class='last-page-btn' style='cursor: pointer;'><span class='last-btns'><i class='fa fa-angle-right arrow-link prev-page arrow-last'></i><i class='fa fa-angle-right arrow-link prev-page arrow-last'></i></span></span>";
	}

?>

<?php if ($main_content['pages']>1) { ?>
	<div class="row no-margin" style="padding-right: 35px; margin-bottom:30px;">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding pagination-col">
			<?=$paginationLinks?>
		</div>
	</div>
<?php } ?>

