<div class="hm_row hm_search_row visible-xs-block">
	<div class="hm_container">
		<form method="GET" action="buscar/libro" id="hm_form_search_mobile">
			<input type="hidden" name="search_param" value="cadena">
			<input type="hidden" name="normal_search" value="1">
			<div>
				<div class="hm_search_input">
					<input
						id="txt_book_search_mobile"
						name="txt_book_search"
						placeholder="Busca los libros que necesitas"
						onfocus="this.placeholder = ''"
						onblur="this.placeholder = 'Busca los libros que necesitas'"
						type="text">
					<img class="img-search" src="assets/images/icon-red-search.png" onclick="hm_submit_search_mobile();">
				</div> <br>
				<span> descubre <a href="buscar/libro?catalogo=novedades">nuevos títulos.</a><!-- crea y comparte </span>
				<a href="listas">listas</a> de tus obras favoritas
				en <a href="actualizaciones">tu muro</a>-->.
			</div>
		</form>
	</div>
</div>
<div class="row hm_row hm_search_row hidden-xs" style="margin-left: 0; background-color: #fff;">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 hm_container">
		<form method="GET" action="buscar/libro" id="hm_form_search">
			<input type="hidden" name="search_param" value="cadena">
			<input type="hidden" name="normal_search" value="1">
			<div>
				<div class="hm_search_input">
					<input
						id="txt_book_search_desktop"
						name="txt_book_search"
						placeholder="Busca los libros que necesitas"
						onfocus="this.placeholder = ''"
						onblur="this.placeholder = 'Busca los libros que necesitas'"
						type="text">
					<img class="img-search" src="assets/images/icon-red-search.png" onclick="hm_submit_search();">
				</div>
				<span>, descubre <a href="buscar/libro?catalogo=novedades">nuevos títulos.</a><!--<br> crea y comparte </span>
				<a href="listas">listas</a> de tus obras favoritas
				en <a href="actualizaciones">tu muro</a>-->
			</div>
		</form>
	</div>
</div>
<div class="hm_row hm_text_header">
	<div class="hm_container container">
		<div class="hm_content">
			<p class="hm_text">
				Usa nuestros catálogos para encontrar el libro que buscas y dónde comprarlo. Explora el Atlas para encontrar espacios de lectura cerca de ti. Consulta el Observatorio para conocer las labores de fomento a la lectura en México.
			</p>
		</div>
	</div>
</div>
<div class="hm_row hm_options">
	<div class="hm_container container">
		<div class="hm_content">
			<article class="col-xs-12 col-sm-6 col-md-4 article catalogos">
				<div class="hm_background">
					<div class="hm_background_image">
					</div>
				</div>
				<div class="hm_content_article">
					<header class="hm_header">
						<h2 class="hm_title">Catálogos</h2>
					</header>
					<hr class="hm_sepador">
					<footer>
						<p class="text">La más extensa selección de títulos comercializados en México y dónde puedes encontrarlos.</p>
						<a class="hm_btn hm_btn_hover" href="buscar/libro?catalogo=todo">
							LIBROS MÉXICO
							<span class="line"></span>
							<img class="ico ico-hover" src="assets/images/flecha-blanca-d.svg">
						</a>
						<a class="hm_btn hm_btn_hover" href="buscar/libro?precio_vigente=1">
							PRECIO ÚNICO
							<span class="line"></span>
							<img class="ico ico-hover" src="assets/images/flecha-blanca-d.svg">
						</a>
						<a class="hm_btn" href="buscar/libro?editorial=conaculta">
							CATÁLOGO HISTÓRICO
							<span class="line"></span>
							<img class="ico" src="assets/images/flecha-roja-d.svg">
							<img class="ico ico-hover" src="assets/images/flecha-blanca-d.svg">
						</a>
						<a class="hm_btn" href="http://clasicos.librosmexico.mx">
							BIBLIOTECA DÍGITAL <span class="line"></span>
							<img class="ico" src="assets/images/flecha-roja-d.svg">
							<img class="ico ico-hover" src="assets/images/flecha-blanca-d.svg">
						</a>
					</footer>
				</div>
			</article>
			<article class="col-xs-12 col-sm-6 col-md-4 article atlas">
				<div class="hm_background">
					<div class="hm_background_image">
					</div>
				</div>
				<div class="hm_content_article">
					<header class="hm_header">
						<h2 class="hm_title">Módulos</h2>
					</header>
					<hr class="hm_sepador">
					<footer>
						<?php /*
						<p class="text">Una probadita de los clásicos, para jóvenes de todas las edades.</p>
						<a class="hm_btn" href="//unade.librosmexico.mx/">
							UNA DE LIBROS
							<span class="line"></span>
							<img class="ico" src="assets/images/flecha-gris-d.svg">
							<img class="ico ico-hover" src="assets/images/flecha-blanca-d.svg">
						</a><br>
						*/ ?>
						<p class="text">Encuentra nuevos lugares para disfrutar de la lectura.</p>
						<a class="hm_btn" href="atlas-de-lectura">
							ATLAS
							<span class="line"></span>
							<img class="ico" src="assets/images/flecha-gris-d.svg">
							<img class="ico ico-hover" src="assets/images/flecha-blanca-d.svg">
						</a>
					</footer>
				</div>
			</article>
			<article class="col-xs-12 col-sm-6 col-md-4 article profecionales">
				<div class="hm_background">
					<div class="hm_background_image">
					</div>
				</div>
				<div class="hm_content_article">
					<header class="hm_header">
						<h2 class="hm_title">Profesionales</h2>
					</header>
					<hr class="hm_sepador">
					<footer>
						<p class="text">Acceso a la plataforma oficial dirigida a los profesionales del mundo del libro.</p>
						<a class="hm_btn hm_btn_hover" href="//pro.librosmexico.mx">
							ENTRADA A PROFESIONALES
							<span class="line"></span>
							<img class="ico" src="assets/images/flecha-blanca-d.svg">
						</a><br>
						<a class="hm_btn hm_btn_gray" href="//observatorio.librosmexico.mx">
							OBSERVATORIO
							<span class="line"></span>
							<img class="ico" src="assets/images/flecha-gris-d.svg">
							<img class="ico ico-hover" src="assets/images/flecha-blanca-d.svg">
						</a>
					</footer>
				</div>
			</article>
		</div>
	</div>
</div>

<?php /* ?>
<div class="hm_row hm_banner">
	<div class="hm_container container">
		<a href="//unade.librosmexico.mx/">
			<div class="col-md-8 col-md-offset-2  banner_wrapper">
				<div class="probadita">
					<span>UNA PROBADITA DE LOS CLÁSICOS EN</span>
				</div>
				<div class="una_de_libros">
					<div class="una_de">
						<span>UNA DE LIBROS</span>
					</div>
					<div class="conoce">
						<span style="font-weight: 600">CONOCE</span><br>
						<span>MÁS</span><i class="fa fa-long-arrow-right" aria-hidden="true" style="float: right"></i>
					</div>
					<div style="clear: both"></div>
				</div>
			</div>
		</a>
	</div>
</div>
<?php */ ?>

<div class="hm_row hm_categories">
	<hr class="container line-separation">
	<div class="hm_container container">
		<div class="hm_content clearfix">
				<div class="col-xs-2 hm_content-cat">
						<header class="hm_header">
							<h2 class="hm_title">TRIVIAS</h2>
							<span class="hm_line"></span>
						</header>
						<p class="hm_description">Demuestra todo lo que sabes sobre tus libros favoritos y gana puntos.</p>
						<a class="hm_link-cat" href="trivias">VER MÁS <span class="line"></span> <span class="angle-right  fa fa-angle-right fa-2x"></span></a>
				</div>
				<div class="load-trivias">
					<!-- Carga el contenido usando ajax load_trivias(); -->
				</div>
		</div>
		<div class="hm_content clearfix">
				<div class="col-xs-2 hm_content-cat">
						<header class="hm_header">
							<h2 class="hm_title">NOVEDADES</h2>
							<span class="hm_line"></span>
						</header>
						<p class="hm_description">
							Descubre los últimos títulos que las editoriales están comercializando en México.
						</p>
						<a class="hm_link-cat" href="buscar/libro?catalogo=novedades">
							VER MÁS <span class="line"></span> <span class="angle-right  fa fa-angle-right fa-2x"></span>
						</a>
				</div>
				<div class="load-new-releases">
					<!-- Carga el contenido usando ajax load_new_releases(); -->
				</div>
		</div>
		<div class="hm_content clearfix">
				<div class="col-xs-2 hm_content-cat">
						<header class="hm_header">
							<h2 class="hm_title">LISTAS</h2>
							<span class="hm_line"></span>
						</header>
						<p class="hm_description">
							Clasifica y organiza tus libros en una estantería digital.
							<br>
							<span class="load-popular-list-info">

							</span>
						</p>
						<a class="hm_link-cat" href="listas">
							VER MÁS <span class="line"></span>
							<span class="angle-right  fa fa-angle-right fa-2x"></span>
						</a>
				</div>
				<div class="load-popular-list">
					<!-- Carga el contenido usando ajax load_popular_list(); -->
				</div>
		</div>
	</div>
</div>
<div class="hm_row hm_tickes_tabs">
	<div class="container">
		<div class="hm_content">
			<a href="http://clasicos.librosmexico.mx/">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<div class="tooltip_arrow_brown hm_tooltip_arrow">
						<p>biblioteca digital</p>
					</div>
					<div class="hm_description">
						<p class="hm_text">El acervo de los clásicos ahora tiene vanguardia.</p>
						<span class="line brown"></span>
					</div>
				</div>
			</a>
			<a href="buscar/libro?editorial=conaculta">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<div class="tooltip_arrow_brown hm_tooltip_arrow">
						<p>Secretaría de Cultura</p>
					</div>
					<div class="hm_description">
						<p class="hm_text">95 años de cultura reflejados en una sección.</p>
						<span class="line brown"></span>
					</div>
				</div>
			</a>
			<a href="buscar/libro?precio_vigente=1">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<div class="tooltip_arrow_brown hm_tooltip_arrow">
						<p>PRECIO ÚNICO</p>
					</div>
					<div class="hm_description">
						<p class="hm_text">Consulta el listado oficial del Registro del Precio Único del Libro fijado por los editores.</p>
						<span class="line brown"></span>
					</div>
				</div>
			</a>
		</div>
		<!--
		<div class="hm_content">
			<a href="actualizaciones">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<div class="tooltip_arrow_red hm_tooltip_arrow">
						<p>MURO</p>
					</div>
					<div class="hm_description">
						<p class="hm_text">Comparte con otros lectores los títulos que leíste, estás leyendo y planeas leer.</p>
						<span class="line red"></span>
					</div>
				</div>
			</a>
			<a href="listas">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<div class="tooltip_arrow_red hm_tooltip_arrow">
						<p>LISTAS</p>
					</div>
					<div class="hm_description">
						<p class="hm_text">Clasifica y organiza tus libros en una estantería digital.</p>
						<span class="line red"></span>
					</div>
				</div>
			</a>
			<a href="trivias">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<div class="tooltip_arrow_red hm_tooltip_arrow">
						<p>TRIVIAS</p>
					</div>
					<div class="hm_description">
						<p class="hm_text">Demuestra todo lo que sabes sobre tus libros favoritos y gana puntos.</p>
						<span class="line red"></span>
					</div>
				</div>
			</a>
		</div>
		-->
		<div class="hm_content">
			<a href="atlas-de-lectura">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<div class="tooltip_arrow_black hm_tooltip_arrow">
						<p>ATLAS</p>
					</div>
					<div class="hm_description">
						<p class="hm_text">Conoce agradables lugares para hacer lo que tanto amas: leer.</p>
						<span class="line black"></span>
					</div>
				</div>
			</a>
			<a href="//pro.librosmexico.mx">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<div class="tooltip_arrow_black hm_tooltip_arrow">
						<p>PRO</p>
					</div>
					<div class="hm_description">
						<p class="hm_text">
							Los profesionales del libro cuentan con un espacio para gestionar datos de sus libros y
							trámites relativos a su actividad editorial.
						</p>
						<span class="line black"></span>
					</div>
				</div>
			</a>
			<a href="//observatorio.librosmexico.mx">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<div class="tooltip_arrow_black hm_tooltip_arrow">
						<p>OBSERVATORIO</p>
					</div>
					<div class="hm_description">
						<p class="hm_text">
							Promueve la elaboración de estudios e investigaciones sobre la lectura y el libro en México.
						</p>
						<span class="line black"></span>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>
<!-- script src='assets/scripts/spin.min.js'></script>
<script src='assets/scripts/jquery.spin.js'></script -->
<!-- Juntar y minificar ambos archivos en uno solo -->
<script>

	var spinnerOpts = {
	 lines: 7 // The number of lines to draw
		, length: 0 // The length of each line
		, width: 3 // The line thickness
		, radius: 4 // The radius of the inner circle
		, scale: 1 // Scales overall size of the spinner
		, corners: 1 // Corner roundness (0..1)
		, color: '#000' // #rgb or #rrggbb or array of colors
		, opacity: 0.25 // Opacity of the lines
		, rotate: 0 // The rotation offset
		, direction: 1 // 1: clockwise, -1: counterclockwise
		, speed: 1 // Rounds per second
		, trail: 60 // Afterglow percentage
		, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
		, zIndex: 2e9 // The z-index (defaults to 2000000000)
		, top: '50%' // Top position relative to parent
		, left: '50%' // Left position relative to parent
		, shadow: false // Whether to render a shadow
		, hwaccel: false // Whether to use hardware acceleration
		, position: 'relative' // Element positioning
	};

	/**
	 * Identifica si un elemento se está mostrando
	 * @param  {element}  el elemento a mostrar
	 * @return {Boolean}    falso o verdadero
	 */
	function isElementInViewport (el) {
    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
      el = el[0];
    }
    var rect = el.getBoundingClientRect();
    return (
      rect.top > 0 &&
      rect.left > 0 &&
      rect.bottom < (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right < (window.innerWidth || document.documentElement.clientWidth)
    );
	}

	/**
	 * Indica si un elemento se ve en la pantalla
	 * @param  {identificador}   el       elemento a buscar
	 * @param  {Function} callback [description]
	 */
	function onVisibilityChange(el, callback) {
	    var old_visible = false;
	    return function () {
        var visible = isElementInViewport(el);
        if (visible != old_visible) {
          old_visible = visible;
          if (typeof callback == 'function') {
            callback();
          }
        }
	    }
	}

	/**
	 * Genera el código HTML para mostrar un libro en la lista de libros
	 * recientes
	 * @param  {object} book libro
	 * @param  {int} i    iterador
	 */
	function generate_html_code_for_book(book) {
		var img_src = book.imagen;
		if (book.imagen == null) {
			img_src = 'assets/images/generic_book.jpg';
		}
		var href = 'libros/' + book.id_libro;
		var inner_html = '';
		inner_html += '<div class="col-xs-2 hm_book">';
		inner_html += '<a href="' + href + '">';
		inner_html += '<img class="image" src="' + img_src + '">';
		inner_html += '</a>';
		inner_html += '</div>';

		return inner_html;
	}

	/**
	 * Carga los nuevos lanzamientos
	 */
	var g_new_releases_loaded = false;
	function load_new_releases() {
		if (g_new_releases_loaded) {
			return;
		}
		g_new_releases_loaded = true;
		var spinner = $('.load-new-releases');
		// $(spinner).spin(spinnerOpts);
		$.ajax({
	    url: "ajax/homepage/new_releases",
	    type: "post",
	    data: {
	    	amount_of_books: 5,
				slice_size_of_books: 5
	    },
	    dataType: "json",
	    success: function (data) {
	      if (data.status == "OK") {
	        if (data.new_releases != null) {
	        	var inner_html = '';
	        	for (var i in data.new_releases) {
	        		var book = data.new_releases[i];
	            inner_html += generate_html_code_for_book(book);
	          }
						// $(spinner).spin(false);
						$('.load-new-releases').html(inner_html);
	        }
	      }
	    }
	  });
	}

	/**
	 * Carga las listas populares
	 */
	var g_popular_list_loaded = false;
	function load_popular_list() {
		if (g_popular_list_loaded) {
			return;
		}
		g_popular_list_loaded = true;
		var spinner = $('.load-popular-list');
		// $(spinner).spin(spinnerOpts);
		$.ajax({
	    url: "ajax/listas_destacadas/1",
	    type: "post",
	    data: {},
	    dataType: "json",
	    success: function (data) {
	      if (data.status == "OK") {
	        if (data.featured_lists != null) {
	        	var inner_html = '';
	        	for (var i in data.featured_lists) {
	        		var list = data.featured_lists[i];
	            for (var j in list.books) {
	            	if (j == 5) {
	            		break;
	            	}
	            	var book = list.books[j];
	            	inner_html += generate_html_code_for_book(book);
	            }
	            if (i == 0) {
	            	var info_html = '';
	            	list_url = 'lista/' + list.user + '/' + list.slug;
	            	list_name = truncate(list.name, 22);
	            	list_name = capitalizeFirstLetter(list_name);
	            	info_html = '<br><a href="' + list_url + '">' + list_name + '</a>';
	            	$('.load-popular-list-info').html(info_html);
	            }
	        	}
						// $(spinner).spin(false);
						$('.load-popular-list').html(inner_html);
	        }
	      }
	    }
	  });
	}

	/**
	 * Carga la sección de las trivias utilizando ajax
	 */
	var g_load_trivias = false;
	function load_trivias() {
		if (g_load_trivias) {
			return;
		}
		g_load_trivias = true;
		var spinner = $('.load-trivias');
		// $(spinner).spin(spinnerOpts);
		$.ajax({
		  url: "ajax/homepage/trivias",
		  type: "get",
		  data:  null,
		  dataType: "text",
		  success: function (data) {
		  	// $(spinner).spin(false);
		    $('.load-trivias').html(data);
		  }
		});
	}

	/**
	 * Carga las imágenes solo cuando tienen que mostrarse
	 */
	var g_load_lazy_images = false;
	function load_lazy_images() {
		if (g_load_lazy_images) {
			return;
		}
		g_load_lazy_images = true;
		$('.lazy-load-image').each(function(i, e) {
			var src = $(e).data('src');
			$(e).attr("src", src);
		});
	}

	function truncate(s, l){
		if (s.length > l) {
			return s.substring(0, l)+'...';
		} else {
			return s;
		}
	}

	function capitalizeFirstLetter(s) {
    return s.charAt(0).toUpperCase() + s.slice(1);
	}

	/**
	 * Realiza la búsqueda cuando se le da click a la imagen
	 */
	function hm_submit_search(){
		$('#txt_book_search_desktop').attr('placeholder', '');
		$('#hm_form_search').submit();
	}
	/**
	 * Realiza la búsqueda cuando se le da click a la imagen
	 * pra la versión móvil
	 */
	function hm_submit_search_mobile(){
		$('#txt_book_search_mobile').attr('placeholder', '');
		$('#hm_form_search_mobile').submit();
	}

</script>
