<?php 
session_start();
$tipos = LM::tipo_clave_producto();
?>
<script type="text/javascript" src="assets/scripts/home.js"></script>
<script async src="//static.addtoany.com/menu/page.js"></script>
<link rel="stylesheet" type="text/css" href="assets/styles/activity.css">
<link rel="stylesheet" type="text/css" href="assets/styles/homepage_index.css">

<?php if (!$user_front) { ?>
	<div class="row home-header-row">
		<div class="home-header-container">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 home-header-info">
			  <h1>LIBROSMÉXICO.MX</h1>
			  <h2>Todos los libros en una página</h2>
			  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-margin">
			    <h3>¿Qué puedes hacer?</h3>
			    <p>Encuentra tus libros favoritos y nuevos, crea listas de tus títulos
			    preferidos, conoce a otros lectores y comparte la 
			    experiencia de leer.</p>
			  </div>
			  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			    <h3>¿Eres editor, librero o bibliotecario?</h3>
			    <p>Descubre las ventajas que LIBROSMÉXICO le ofrece a <a href="https://pro.librosmexico.mx" target="_blank">profesionales del libro</a>.</p>
			  </div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 home-header-login">
			  <h2>Crea una cuenta</h2>
			  <form id="home-header-form">
			  	<label id="home-header-form-error">
			  	</label>
			  	<label>
			  		<input 
			  			type="text" 
			  			name="name"
			  			class="required"
			  			value=""
			  			placeholder="Nombre"
			  			required="required">
			  	</label>
			  	<label>
			  		<input 
			  			type="email" 
			  			name="email"
			  			class="required email"
			  			value=""
			  			placeholder="Correo electrónico"
			  			required="required">
			  	</label>
			  	<label>
			  		<input 
			  			type="password" 
			  			name="password"
			  			class="required"
			  			value=""
			  			placeholder="Contraseña"
			  			required="required">
			  	</label>
			  	<label>
           	<p class="home-header-terms">
           		<input
				  			type="checkbox" 
				  			name="terminos" 
				  			value=""
				  			id="home-header-terms-checkbox"
				  			required="required">
				  			Acepto las <a href="politicas-de-privacidad" target="_blank">políticas de privacidad</a> y los <a href="terminos-y-condiciones" target="_blank">términos y condiciones</a>.
           	</p>
			  	</label>
			  	<div class="home-header-actions">
			  		<input 
			  			type="submit"
			  			value="Registrate"
			  			class="lm-btn lm-btn-full-green" />
			  		<a 
              href="javascript:void(0);" 
              onclick='registerFB()' 
              class="btn btn-rounded btn-fb" 
              name="btnRegFb">
              <img class="social-icon" src="assets/images/icono_facebook-login.svg">
              Registrarse con Facebook
            </a>
			  	</div>
			  </form>
			  <script>
			  $(function() {
					$('#home-header-form').on('submit', function(e) {
						e.preventDefault();
						var checked = $('#home-header-terms-checkbox').is(':checked');
						if (checked) {
							$.ajax({
								url: "register-ajax",
								type: "post",
								data: $('#home-header-form').serialize(),
								dataType: "json",
								success: function (data) {
									if (data.status == 'yes') {
										//location.reload();
										window.location = window.location.protocol+"//"+window.location.host+window.location.pathname+"user_welcome";
									} else if (data.status == 'no') {
										if (data.error.code == 1001) {
											$('#home-header-form .email').css('border', '1px solid red');
											$('#home-header-form-error').html(data.error.message);
										}
									}
								}
							});
						} else {
							$('.home-header-terms').css('color', 'red');
						}
					});
			  });
			  </script>
			</div>
		</div>
	</div>
<?php } ?>

	<div class="row no-margin home-row">
		<!-- Libros -->
		<div class="col col-xs-12 col-sm-8 col-md-8 col-lg-8 home-books">

			<!-- Novedades -->
			<h2>NOVEDADES</h2>			
			<hr class="red-hr">
			<div class="row no-margin">
				<div class="dsk-destacados load-new-releases dsk-new-releases">
				<?php // el contenido se carga con ajax: load_new_releases(); ?>
				</div>
			  <div class="mbl-destacados load-new-releases mbl-new-releases">
			  <?php // el contenido se carga con ajax: load_new_releases(); ?>
			  </div>
		  </div>

			<!-- Destacados -->
			<h2>DESTACADOS</h2>			
			<hr>
			<div class="row no-margin">
				<div class="dsk-destacados load-highlights dsk-highlights">
				<?php // el contenido se carga con ajax: load_highlights(); ?>
				</div>
				<!-- Destacados movil -->
				<div class="mbl-destacados load-highlights mbl-highlights">
				<?php // el contenido se carga con ajax: load_highlights(); ?>
				</div>
			</div>
		</div>

		<!-- Informativos -->

		<div class="col col-xs-12 col-sm-4 col-md-4 col-lg-4 home-information">
			<?php
			if (strlen($data['banner'][0]['icon']) > 5) {
			?>
			<div class="home-banner">
				<a href="<?=$data['banner'][0]['href']?>" target="<?=$data['banner'][0]['target']?>">
					<img src='<?=$data['banner'][0]['icon']?>'>
				</a>
			</div>
			<?php
			}
			?>
			
			<!-- Trivias -->
			<div class="sidebar-trivias">
				<?php include 'trivia_side.php';?>
			</div>
			<!-- /Trivias -->
			
			<!-- Listas destacadas -->
			<div class="listas-destacadas">
				<h2>LISTAS DESTACADAS</h2>
				<hr class="blue-hr">
				<div class="listas-destacadas-body">
					<?php // el contenido se carga con ajax: load_featured_lists(); ?>
				</div>
			</div>
			<!-- /Listas destacadas -->

			<div class="cita">
				<h2>CITA DEL DÍA</h2>
				<hr>
				<?php if($data['random_quote']) { ?>
					<h4>"<?=utf8_decode($data['random_quote']['quote'])?>"</h4>
					<span>- <?=utf8_decode($data['random_quote']['author'])?></span>
					<a href="javascript:void(0);" style="cursor:default!important;">Compartir</a>
          <li class="fa fa-facebook-square fa-md  pointer" onclick='shareFB("Cita del Día", "<?=utf8_decode($data['random_quote']['quote'])?>","https://librosmexico.mx/assets/images/libros_logo100x.png","https://librosmexico.mx")' style="color: #4b66a0"></li>
					<!-- img onclick='shareFB("Cita del Día", "<?php //utf8_decode($data['random_quote']['quote'])?>","https://librosmexico.mx/assets/images/libros_logo100x.png","https://librosmexico.mx")' src='assets/images/detail/facebook_cita.svg' class='social-medios-mini pointer' style="cursor:pointer!important;" -->
					<?php
					$random_quote = utf8_decode($data['random_quote']['quote']);
					$url_base = URL::base(true);
					$id_text =  ' vía @LibrosMexicoMX ';
					$total_text = $random_quote.$url_base.$id_text;

					//longitu total de la cadena
					$text_length = strlen($total_text);

					//si la cadena supera los 140 caracteres, entonces se hace un substring
					if( $text_length > 140 )
					{
						$extra_char = $text_length - 140;
						$random_quote = LM::text_to_ellipsis($random_quote, $random_quote-$extra_char );
					}
					echo '<a class="a2a_button_twitter" target="_blank" onclick="window.open(&#34http://www.addtoany.com/add_to/twitter?linkurl='.urlencode(URL::base(true)).'&amp;linkname='.urlencode($random_quote." vía @LibrosMexicoMX").'&amp;linknote=&#34,&#34_blank&#34,&#34top=200, left=200, width=450, height=500&#34)" rel="nofollow" aria-label="Twitter">';
          //echo '<span class="a2a_svg a2a_s__default a2a_s_twitter" style="width: 13px; line-height: 13px; height: 13px; border-radius: 2px; background-size: 13px; display: inline-block;vertical-align: sub;cursor:pointer!important;"></span>';
          echo '<li class="fa fa-twitter-square fa-md  pointer" style="color: #3b94d9"></li>';
          echo '</a>';
        } ?>
			</div>
		</div>
	</div>	

	<!-- Tooltip 1 -->	
<form id="formToolTip1" method="post" action="">
	<div id="tooltip-1">
		<div class="contenedor">
			<p class="indicador">Tip 1 de 4</p>
			<h5 class="tip">Tip 1</h5>
			<h6 class="titulo-tip">Menú de usuario</h6>
			<p class="small"> <span> Configuración:</span>  Puedes cambiar tu información personal, seleccionar una foto para tu perfil y añadir una breve descripción.</p>
			<hr/ class="hr">
			<div class="contenedor-botones">
				<input type="hidden" name="omitir">
				<a class="boton-omitir omitir-hover" role="button">omitir</a>
				<a href="libros/<?=$data['random']?>" class="boton boton-rounded boton-siguiente no-hover-tooltip" id="link-siguiente" >siguiente </a>
			</div>
		</div>
		<img src="assets/images/background_tooltip.svg">
	</div>
</form>
	
	<script src='assets/scripts/spin.min.js'></script>
	<script src='assets/scripts/jquery.spin.js'></script>

	<script>
<?php 
$tipos = LM::tipo_clave_producto();
echo 'var global_tipos = '.json_encode($tipos, true).';';
?>
var spinnerOpts = {
	 lines: 7 // The number of lines to draw
	, length: 0 // The length of each line
	, width: 3 // The line thickness
	, radius: 4 // The radius of the inner circle
	, scale: 1 // Scales overall size of the spinner
	, corners: 1 // Corner roundness (0..1)
	, color: '#000' // #rgb or #rrggbb or array of colors
	, opacity: 0.25 // Opacity of the lines
	, rotate: 0 // The rotation offset
	, direction: 1 // 1: clockwise, -1: counterclockwise
	, speed: 1 // Rounds per second
	, trail: 60 // Afterglow percentage
	, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
	, zIndex: 2e9 // The z-index (defaults to 2000000000)
	, top: '50%' // Top position relative to parent
	, left: '50%' // Left position relative to parent
	, shadow: false // Whether to render a shadow
	, hwaccel: false // Whether to use hardware acceleration
	, position: 'relative' // Element positioning
}

/**
 * Carga las listas destacadas en el home
 */
function load_featured_lists () {
	var spinner = $('.listas-destacadas-body');
	$(spinner).spin(spinnerOpts);
	$.ajax({
    url: "ajax/listas_destacadas/5",
    type: "post",
    data: {},
    dataType: "json",
    success: function (data) {
      if (data.status == "OK") {
        if (data.featured_lists != null) {
        	var html = '';
        	for (var i in data.featured_lists) {
        		var list = data.featured_lists[i];
            html += '<div class="row no-margin section-list-title section-list-title-own">';
            html += 	'<a href="lista/' + list.user + '/' + list.slug +'">' + list.name + ' (' + list.num_books +')</a>';
            html += '</div>';
            html += '<div class="row no-margin">';
            for (var j in list.books) {
            	if ( j < 5 ) {
            		var book = list.books[j];
            		var img_src = 'assets/images/generic_book.jpg';
            		if (book.imagen != null) {
            			img_src = book.imagen;
            		}
            		var desk = '';
            		if (j == 4) {
            			desk = 'desk';
            		}
            		html += '<a href="libros/' + book.id_libro + '">';
            		html += 	'<img src="' + img_src + '" class="home-list-book list-push-right-own ' + desk + '">';
            		html += '</a>';
            	}
            }
            html += '</div>';
            html += '<div class="row section-see-list-own no-margin own-list">';
            html += 	'<a href="lista/' + list.user + '/' + list.slug +'"> ver más <img src="assets/images/detail/flecha_azul.svg" class="arrow_list"></a>';
            html += '</div>';
        	}
        	html += '<div class="row section-see-list-more no-margin see-more-list">';
					html += 	'<a href="listas"><span class="pointer">ver más listas <img src="assets/images/detail/flecha_azul.svg" class="arrow_list"></span></a>';
					html += '</div>';
					$(spinner).spin(false);
					$('.listas-destacadas-body').html(html);
        }
      }
    }
  });
}

/**
 * Carga los nuevos lanzamientos
 */
function load_new_releases() {
	var spinner = $('.load-new-releases');
	$(spinner).spin(spinnerOpts);
	$.ajax({
    url: "ajax/homepage/new_releases",
    type: "post",
    data: {},
    dataType: "json",
    success: function (data) {
      if (data.status == "OK") {
        if (data.new_releases != null) {
        	var desktop_html = '';
        	var mobile_html = '';
        	for (var i in data.new_releases) {
        		var book = data.new_releases[i];
            desktop_html += generate_desktop_html_for_new_release(book, i + 1);
            // solo se van a cargar cuatro libros para móviles
            if (i < 4) {
            	mobile_html += generate_mobile_html_for_new_release(book);
            }
          }
          desktop_html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">';
          desktop_html += 	'<a href="buscar/libro?catalogo=novedades">';
          desktop_html += 		'<span class="mbl-view-more more-renowned more-tag" style="color: #CC4242!important;">';
          desktop_html += 			'Ver más novedades <i class="fa fa-angle-right more-arrow" style="color: #CC4242!important;"></i>';
          desktop_html += 		'</span>';
          desktop_html += 	'</a>';
          desktop_html += '<div>';

          mobile_html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding book-mbl-container">';
          mobile_html += 	'<hr class="section-line">';
          mobile_html += 	'<a href="buscar/libro?catalogo=novedades">';
          mobile_html += 		'<span class="mbl-view-more more-news" style="color: #CC4242!important;">';
          mobile_html += 			'Ver más novedades';
          mobile_html += 		'</span>';
          mobile_html += 	'</a>';
          mobile_html += '<div>';

					$(spinner).spin(false);
					$('.load-new-releases.dsk-new-releases').html(desktop_html);
					$('.load-new-releases.mbl-new-releases').html(mobile_html);
					add_book_wrapper_events();
					add_keywords_events();
        }
      }
    }
  });
}

/**
 * Genera el código HTML para mostrar un libro en la lista de libros
 * recientes
 * @param  {object} book libro
 * @param  {int} i    iterador
 */
function generate_desktop_html_for_new_release(book, i) {

	var img_src = book.imagen;
	if (book.imagen == null) {
		img_src = 'assets/images/generic_book.jpg';
	}
	
	var book_type = '';
	if (book.tipo != null) {
  	if (book.tipo != '10') {
  		book_type = global_tipos[book.tipo];
  		if (book_type == 'Libros electrónicos') {
  			book_type = 'Libro electrónico';
  		}
  		book_type = '<p class="home-type">' + book_type + '</p>';
  	}
  }

  var author = '';
  if (book.autores.length > 0) {
  	author = book.autores[0].nombre;
  }

  var editorial = '';
  if (book.editorial.length > 0) {
  	editorial = book.editorial[0].nombre;
  	if (book.editorial.length > 1) {
  		editorial += ', <span>...</span>';
  	}
  }

  var keywords = '';
  if (book.keywords.length > 0) {
  	keywords += '<p>Palabras clave:</p>';
  	var limit = book.keywords.length > 5 ? 5 : book.keywords.length;
  	for (var j = 0; j < limit; j++) {
  		var keyword = book.keywords[j];
  		keywords += '<span class="sp-keyword-home">' + keyword + '</span>';
  	}
  	if (book.keywords.length > 5) {
  		keywords += '<span>...</span>';
  	}
  }

	var html = "";
	html += '<a href="libros/' + book.id_libro + '">';
  html += 	'<div class="col col-xs-6 col-sm-6 col-md-4 col-lg-3 no-padding" style="overflow:hidden;">';
  html +=			'<div class="book-wrapper">';
  html += 			'<img alt="' + book.titulo + '" src="' + img_src + '">';
  html +=				book_type;
  html +=				'<div class="home-book-info-mask hidden">';
  html +=					'<p class="home-title">' + book.titulo + '</p>';
  html +=					'<p class="home-autor">' + author + '</p>';
  html += 				'<p class="home-editorial">' + editorial + '</p>';
  html +=					'<div class="key-words-home">';
  html +=						keywords;
  html +=					'</div>';
  html +=				'</div>';
  html +=			'</div>';
  html +=		'</div>';
  html +=	'</a>';

  if (i % 2 == 0) {
  	html += '<div class="clearfix visible-xs-block"></div>';
  	html += '<div class="clearfix visible-sm-block"></div>';
  }
  if (i % 3 == 0) {
  	html += '<div class="clearfix visible-md-block"></div>';
  }
  if (i % 4 == 0) {
  	html += '<div class="clearfix visible-lg-block"></div>';
  }

  return html;
}

/**
 * Genera el código HTML mobile para mostrar un libro como destacao
 * recientes
 * @param  {object} book libro
 */
function generate_mobile_html_for_new_release(book) {

	var img_src = book.imagen;
	if (book.imagen == null) {
		img_src = 'assets/images/generic_book.jpg';
	}
	
	var book_type = '';
	if (book.tipo != null) {
  	if (book.tipo != '10') {
  		book_type = global_tipos[book.tipo];
  		if (book_type == 'Libros electrónicos') {
  			book_type = 'Libro electrónico';
  		}
  		book_type = '<p class="home-type">' + book_type + '</p>';
  	}
  }

  var author = '';
  if (book.autores.length > 0) {
  	author = book.autores[0].nombre;
  }

  var editorial = '';
  if (book.editorial.length > 0) {
  	editorial = book.editorial[0].nombre;
  	if (book.editorial.length > 1) {
  		editorial += ', <span>...</span>';
  	}
  }

  var keywords = '';
  if (book.keywords.length > 0) {
  	keywords += '<p>Palabras clave:</p>';
  	var limit = book.keywords.length > 5 ? 5 : book.keywords.length;
  	for (var j = 0; j < limit; j++) {
  		var keyword = book.keywords[j];
  		keywords += '<span class="sp-keyword-home">' + keyword + '</span>';
  	}
  	if (book.keywords.length > 5) {
  		keywords += '<span>...</span>';
  	}
  }

  var html = "";
	html += '<a href="libros/' + book.id_libro + '" class="novedades-mbl-item">';
  html += 	'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding book-mbl-container">';
  html +=			'<div class="row no-margin">';
  html +=				'<div class="col-xs-3 no-padding">';
  html += 				'<img alt="' + book.titulo + '" src="' + img_src + '" class="mbl-book-portrait">';
  html +=				'</div>';
  html +=				'<div class="col-xs-9 no-padding">';
  html +=					'<span class="mbl-book-name">' + book.titulo + '</span>';
  html +=					'<span class="mbl-book-author">' + author + '</span>';
  html +=					'<span class="mbl-book-author">' + editorial + '</span>';
  html +=				'</div>';
  html +=			'</div>';
  html +=		'</div>';
  html +=	'</a>';

  return html;
}

/**
 * Carga los libros destacados
 */
function load_highlights() {
	var spinner = $('.load-highlights');
	$(spinner).spin(spinnerOpts);
	$.ajax({
    url: "ajax/homepage/highlights",
    type: "post",
    data: {},
    dataType: "json",
    success: function (data) {
      if (data.status == "OK") {
        if (data.highlights != null) {
        	var desktop_html = '';
        	var mobile_html = '';
        	for (var i in data.highlights) {
        		var book = data.highlights[i];
            desktop_html += generate_desktop_html_for_new_release(book, i + 1);
            // solo se van a cargar cuatro libros para móviles
            if (i < 4) {
            	mobile_html += generate_mobile_html_for_new_release(book);
            }
          }
          desktop_html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">';
          desktop_html += 	'<a href="contenido/destacados">';
          desktop_html += 		'<span class="mbl-view-more more-renowned more-tag">';
          desktop_html += 			'Ver más destacados <i class="fa fa-angle-right more-arrow" style="color: #ef6c15 !important;"></i>';
          desktop_html += 		'</span>';
          desktop_html += 	'</a>';
          desktop_html += '<div>';

          mobile_html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding book-mbl-container">';
          mobile_html += 	'<hr class="section-line">';
          mobile_html += 	'<a href="contenido/destacados">';
          mobile_html += 		'<span class="mbl-view-more more-news" style="color: #ef6c15 !important;">';
          mobile_html += 			'Ver más destacados';
          mobile_html += 		'</span>';
          mobile_html += 	'</a>';
          mobile_html += '<div>';

					$(spinner).spin(false);
					$('.load-highlights.dsk-highlights').html(desktop_html);
					$('.load-highlights.mbl-highlights').html(mobile_html);
					add_book_wrapper_events();
					add_keywords_events();
        }
      }
    }
  });
}

/**
 * Agrega los eventos del book wrapper
 */
function add_book_wrapper_events() {
	$(".book-wrapper").hover(function() {
		$(this).children(".home-book-info-mask").removeClass("hidden");
	}, function() {
		$(this).children(".home-book-info-mask").addClass("hidden");
	});
}

/**
 * Agrega los eventos de las palabras clave
 */
function add_keywords_events() {
	$( '.key-words-home' ).unbind( 'mousewheel DOMMouseScroll' ).bind( 'mousewheel DOMMouseScroll', function ( e ) {
	  var e0 = e.originalEvent;
	  var delta = e0.wheelDelta || -e0.detail;
    this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
    e.preventDefault();
	});
}

$(document).ready(function () {

	$(".sp-keyword-home").each(function () {
		var txt = $(this).text();
		$(this).html("<a href='buscar/libro?keywords="+txt+"'>"+txt+"</a>");
	});

	$("#tooltip-1").hide();

	add_book_wrapper_events();
	add_keywords_events();

	<?php
	//se obtiene id de usuario logueado
	if( isset($_SESSION['user_id']) )
	{
		$session_user_id = $_SESSION['user_id'];
		$tooltip_status = Model::factory('userfront')->getTootltipStatus($session_user_id);
		if( (int)$tooltip_status == 1 )
		{
			Model::factory('userfront')->updateTooltips( $_SESSION['user_id'], 2 );
		}
	}
	else
	{
		$tooltip_status = 0;
	}
	?>

	var tooltip_status =<?php echo isset($tooltip_status) ? $tooltip_status : -1; ?>;
	tooltip_status = parseInt(tooltip_status);

	if ( tooltip_status == 1 ) {
		//mostrar tooltips
		setTimeout(function(){ 
			$('.usr-submenu-wrapper').removeClass('hidden');
			var position = $(".Configuración").offset();
			var tooltip_width= $("#tooltip-1").width();	
			$("#tooltip-1") .offset ({top: (position.top- 10), left: (position.left-tooltip_width-5)});
			$("#tooltip-1").show();
		}, 800);
		$(".boton-omitir").click(function(){
			$("#tooltip-1").fadeOut(500);
			$('.usr-submenu-wrapper').addClass('hidden');
			var dataString = 'omitir=1';
			$.ajax({
                type: "POST",
                url: "ajax_tooltip",
                data: dataString,
                cache: false,
                success: function(result) {
                    
                }
            });
		});
	} else {
		$("#tooltip-1").hide();
	}

	// tooltip
	$(window).resize(function(){
		if ( tooltip_status ==1 ) {
			var ventana_ancho = $(window).width();
			var position = $(".Actualizaciones").offset();
			var tooltip_width= $("#tooltip-1").width();	
			$("#tooltip-1").offset ({top: (position.top- 10), left: (position.left-tooltip_width-5)});
			if (ventana_ancho <= 767) {
				$("#tooltip-1").hide();
			} else {
			 	$("#tooltip-1").show();
			}
		} else {
			$("#tooltip-1").hide();
		}
  });

	$('#link-siguiente').click(function(){
		$("#tooltip-1").fadeOut(800);
		var dataString = 'update-tooltip=2';
		$.ajax({
            type: "POST",
            url: "ajax_tooltip",
            data: dataString,
            cache: false,
            success: function(result) {}
        });
	});

	// Carga los nuevos lanzamientos
	load_new_releases();
	// Carga los destacados
	load_highlights();
	// Carga las listas destacadas
	load_featured_lists();

});

function shareFB(title, desc, img, url){
	var product_name   = 	title;
	var description	   =	desc;
	var share_image	   =	img;
	var share_url	   =	url;
	var share_caption  = 	'librosmexico.mx';
	
  FB.ui({
      method: 'feed',
    	appId: '394615860726938',   
      name: title,
      link: share_url,
      picture: share_image,
      caption : share_caption,
      description: description
  }, function(response) {
      if(response && response.post_id){}
      else{}
  });
}

function shareTW(url,titulo){
	var params = {
    access_token: "65efccf36ae26915b2362f75c2b09b8856732202",
    longUrl: url,
    format: 'json'
	};
	$.getJSON('https://api-ssl.bitly.com/v3/shorten', params, function (response, status_txt) {
		var urlbit =response.data.url;
		var len = 140 - (urlbit.length)+7;  
		if (titulo.length>len) {
			titulo = titulo.substring(0,len);
			titulo = '"'+titulo+'..."';
		} else {
			titulo = '"'+titulo+'"';
		}
		var device = navigator.userAgent
		if (device.match(/Iphone/i)|| device.match(/Ipod/i)|| device.match(/Android/i)|| device.match(/J2ME/i)|| device.match(/BlackBerry/i)|| device.match(/iPhone|iPad|iPod/i)|| device.match(/Opera Mini/i)|| device.match(/IEMobile/i)|| device.match(/Mobile/i)|| device.match(/Windows Phone/i)|| device.match(/windows mobile/i)|| device.match(/windows ce/i)|| device.match(/webOS/i)|| device.match(/palm/i)|| device.match(/bada/i)|| device.match(/series60/i)|| device.match(/nokia/i)|| device.match(/symbian/i)|| device.match(/HTC/i)){
			location.target="_newtab";
			location.href='http://twitter.com/intent/tweet?text='+titulo+' '+urlbit;
		}	else  {
			window.open('http://twitter.com/intent/tweet?text='+titulo+' '+urlbit,"_blank","top=200, left=500, width=400, height=400");
		}
	});
}
</script>


