<script async src="//static.addtoany.com/menu/page.js"></script>
<?php
function url()
{                    
    return URL::base(true).'trivias/findid/';
}

if ($data["sidebar_trivias"]) {
    $user_id = $data["user_id"];
	$trivias = $data["sidebar_trivias"];
    //var_dump($trivias);
	$preguntas = $data["sidebar_preguntas"];
	$respuestas = $data["sidebar_respuestas"]
?>

<link rel="stylesheet" type="text/css" href="assets/styles/sidebar_trivia.css">
<link rel="stylesheet" type="text/css" href="assets/styles/triviastyle.css"/>
<script async src="//static.addtoany.com/menu/page.js"></script>

<div class="sidebar">
	<div class="sidebar-header">
		<h2>TRIVIAS</h2><input type="hidden" value = "" id="TriviaElegida"></input>
	</div>
	<div class="sidebar-body">
		<?php
		$i = 1;
		foreach ($trivias as $trivia) {
			$trivia_first_book = $trivia["books"][0];
		?>
        <div class="sidebar-trivia">
            
            <?php
            //var_dump($trivia);
            if (!$trivia['solved'])
            {
                echo '<div class="sidebar-trivia-header" style="background-image:linear-gradient(rgba(0, 0, 0, 0.50), rgba(0, 0, 0, 0.50)),  url('.$trivia_first_book['imagen'].')">';
                echo '<img src="'.$trivia_first_book['imagen'].'" class="trivia-book" style="cursor: pointer;" data-toggle="modal" data-target="#trivia'.$i.'" onclick = "select_trivia('.$i.')" value="'.$i.'">';
                echo '</div>';
            }
            else
            {
                echo '<div class="sidebar-trivia-header" style="background-image:linear-gradient(rgba(0, 0, 0, 0.50), rgba(0, 0, 0, 0.50)),  url('.$trivia_first_book['imagen'].')">';
                echo '<img src="'.$trivia_first_book['imagen'].'" class="trivia-book" style="cursor: pointer;" data-toggle="modal" data-target="#trivia'.$i.'" onclick = "ver_resultados('.$i.')" value="'.$i.'">';
                echo '</div>';
            }
            if ((int)$trivia['trivia_score'] != 0)
                    {
                        //echo '<button type="button" class="btn btn-default btn-round btn-sm-green " style="align-content:center">';
                        //echo '</button>';
                        echo '<div class="btn-sm-green-label-num">';
                        echo $trivia['trivia_score'];
                        echo '</div>';
                        echo '<div class="btn-sm-green-label">';
                        echo '<span>biblos</span>';
                        echo '</div>';
                    }
            ?>       
            <div class="sidebar-trivia-body">
                
                    <?php 
                        if (!$trivia['solved']) 
                        {
                            echo '<div class="trivia-title" style="cursor: pointer;" data-toggle="modal" data-target="#trivia'.$i.'" onclick = "ver_resultados('.$i.')" value="'.$i.'">';
                            echo $trivia['trivia_name'];
                            echo '</div>';                        
                        }
                        else 
                        {
                            echo '<div class="trivia-title" style="cursor: pointer;" data-toggle="modal" data-target="#trivia'.$i.'" onclick = "select_trivia('.$i.')" value="'.$i.'">';
                            echo $trivia['trivia_name'];
                            echo '</div>';                        
                        }
                    ?>
                
                <div class="trivia-data-action-wrapper clear">
                    <div class="trivia-data">
                        <?php
                        if ($trivia["author_fullname"]) {
                        ?>
                        <p>Por <?php echo $trivia["author_fullname"]; ?></p>
                        <?php
                        }
                        ?>
                        <?php
                        if ($trivia["created_on"]) {
                            $created_on = explode(' ',$trivia['created_on'])[0];
                            $created_on = date("d/m/Y", strtotime($created_on));
                        ?>
                        <p>Creada el <?php echo $created_on; ?></p>
                        <?php
                        }
                        ?>
                    </div>
                    <?php

                    echo '<div class="trivia-action" id="trivia-action'.$i.'">';
                        
                        $button_attributes = '';
                        $button_label = '';
                        if (!$trivia['solved']) {
                            $button_attributes = 'class="lm-btn" data-toggle="modal" data-target="#trivia'.$i.'" onclick="select_trivia('.$i.')" value="'.$i.'"';
                            $button_label = 'Contestar trivia';
                        } else {
                            $button_attributes = 'class="lm-btn" data-toggle="modal" data-target="#trivia'.$i.'" onclick="ver_resultados('.$i.')" value="'.$i.'"';
                            $button_label = 'Ver resultados';
                        }
                        ?>
                        <button <?php echo $button_attributes; ?>>
                            <?php echo $button_label; ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
		<?php
		$i++;
		}?>
        <div id="boton-descubre" class="descubre-btn" style="display: block;"><button id="fin1" type="button" class="btn-rounded-green-sidebar" onclick="location.href = &quot;trivias&quot;;">ver más trivias&nbsp;<img src="assets/images/flecha_verde_copia.svg"</button></div>
	</div>
</div>
<?php
}
?>

<!-- Las modal windows de las trivias -->
<?php
//var_dump(count($preguntas_list[0]));
$i = 1;
//Esta parte arma cada uno de las ventanas modales con cada encuesta y su contenido
foreach ($trivias as $trivia) {
    echo '<div class="modal fade" id ="trivia'.$i.'" tabindex="-1" role="dialog" aria-labelledby="EtiquetaModal'.$i.'" aria-hidden="true">';
    echo '<div class="modal-dialog">';
    
    echo '<div class="modal-content ">';
    echo '<div class="modal-header">';
    echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
    echo '<p id= "modal-trivia-titulo'.$i.'" class="trivia-modal-title">Trivia</p>';
    echo '<p id= "modal-trivia-titulo-alt'.$i.'" class="trivia-modal-title2">'.$trivia['trivia_name'].'</p>';
    echo '</div>';
    echo '<div class="modal-body" style="padding-bottom: 20px">';
    echo '<div id="ctn'.$i.'0">';
    setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish'); 
    $fechax = explode(' ',$trivia['created_on'])[0];
    $fechax = explode(' ',$trivia['created_on'])[0];
    setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish'); 
                                $mes = explode("-",$fechax)[1];
                                $mesletra = "";
                                switch ($mes)
                                {
                                    case 1:
                                        $mesletra = "enero";
                                        break;
                                    case 2:
                                        $mesletra = "febrero";
                                        break;
                                    case 3:
                                        $mesletra = "marzo";
                                        break;
                                    case 4:
                                        $mesletra = "abril";
                                        break;
                                    case 5:
                                        $mesletra = "mayo";
                                        break;
                                    case 6:
                                        $mesletra = "junio";
                                        break;
                                    case 7:
                                        $mesletra = "julio";
                                        break;
                                    case 8:
                                        $mesletra = "agosto";
                                        break;
                                    case 9:
                                        $mesletra = "septiembre";
                                        break;
                                    case 10:
                                        $mesletra = "octubre";
                                        break;
                                    case 11:
                                        $mesletra = "noviembre";
                                        break;
                                    case 12:
                                        $mesletra = "diciembre";
                                        break;

                                }
                                echo '<p class="trivia-modal-created">Creada el '.strftime('%d de '.$mesletra.' de %Y',strtotime($fechax)).'</p>';
    echo '<div style="clear: both;"></div>';
    
    echo '<div id="portada-data">';
    echo '<div class="portada-img">';
    echo '<img src="'.$trivia['books'][0]['imagen'].'" class="img-portada-grande"></img>';
    echo '</div>';//end of div portada-img
    echo '<div class="portada-txt">';
    echo '<p class="trivia-modal-title3">'.$trivia['trivia_name'].'</p>';
    echo '<p class="trivia-modal-creator">por '.$trivia['author_fullname'].'</p>';
    echo '<p class="trivia-modal-description">'.$trivia['trivia_description'].'</p>'; 
    echo '<div class="portada-button">';
    echo '<button id="next'.$i.'" type="button" class="btn btn-rounded btn-rounded-green-modal" onclick="changeContent()">Comenzar trivia</button>'; 
    echo '</div>';//end of div portada-button
    echo '</div>';//end of div portada-txt
     echo '</div>';  //end of div portada data          
    echo '<div style="clear: both;"></div>';
    echo '<div class="lista-trivia">';
    echo '<center style="padding-top:5px">';
    if (count($trivia['books']) == 0 || count($trivia['books']) == 1)
    {
        echo '<p class="trivia-modal-libros">Libro en esta trivia:</p>';
    }
    else
    {
        echo '<p class="trivia-modal-libros">Libros en esta trivia:</p>';
    }
    foreach ($trivia['books'] as $book) {
        echo '<a href="libros/'.$book['id_libro'].'" target = "_blank">';
        echo '<img src="'.$book['imagen'].'" class="home-list-book list-books-trivia"></a>';
    }
    echo '</center>'; 
    echo '</div>';  //end of div lista-trivia    
    echo '<div id="textbox"  style="display:inline">';
    if(is_null($user_id))
    {
         if ($trivia['trivia_total_likes'] > 0)
        {
            echo '<p class="likes" id="likeportada'.$i.'"><span class="like-link" >Me gusta</span><bold style="color:#989898"> | '.$trivia['trivia_total_likes'].' me gusta</bold></p>';
        }
        else
        {
            echo '<p class="likes" id="likeportada'.$i.'"><span class="like-link" >Me gusta</span></p>';
        }
    }
    else
    {
        if ($trivia['liked'])
        {
            echo '<p class="likes" id="likeportada'.$i.'"><span class="like-link" onclick="dislike_trivia()">Ya no me gusta</span><bold style="color:#989898"> | '.$trivia['trivia_total_likes'].' me gusta</bold></p>';
        }
        else
        {
            if ($trivia['trivia_total_likes'] > 0)
            {
                echo '<p class="likes" id="likeportada'.$i.'"><span class="like-link" onclick="like_trivia()">Me gusta</span><bold style="color:#989898"> | '.$trivia['trivia_total_likes'].' me gusta</bold></p>';
            }
            else
            {
                echo '<p class="likes" id="likeportada'.$i.'"><span class="like-link" onclick="like_trivia()">Me gusta</span><bold style="color:#989898"> | Se el primero en decir que te gusta esto</bold></p>';
            }
            
        }
    }
    echo '<span style="float:right;"><span class="keyw-p">Compartir trivia&nbsp&nbsp</span>';
    echo '&nbsp';
    echo '<img onclick="shareFBtrivia(&quot;'.$trivia['trivia_name'].'&quot;,&quot;Te invito a contestar la trivia '.$trivia['trivia_name'].' en LIBROSMEXICO.MX&quot;,&quot;'.$trivia['books'][0]['imagen'].'&quot;,&quot;'.url().$trivia['trivia_id'].'&quot;)"src="assets/images/detail/facebook_cita.svg" class="social-medios-mini pointer" style="cursor:pointer!important;">';
    
    echo '&nbsp';
    $twmsg = 'Te invito a contestar la trivia &#34'.$trivia['trivia_name'].'&#34 en LIBROSMEXICO.MX via @LibrosMexicoMX';
    ?>
    <div class="a2a_kit a2a_kit_size_13 a2a_default_style" 
        data-a2a-url=<?php echo '"'.url().$trivia['trivia_id'].'"';?>  
        data-a2a-title=<?php echo '"'.$twmsg.'"';?>
        data-a2a-via= "LibrosMexicoMX" style="display: inline;float: right;margin-top: 5px;cursor:pointer!important;">
        <a class="a2a_button_twitter"></a>
        </a>
    </div>
    <?php
    echo '</span></p>';
    echo '<div style="clear: both;"></div>';
    echo '</div>';
    echo '</div>';

    $numero_preguntas = count($preguntas[$i-1]);
    //echo '<input type="text" id="numpreguntas'.$i.'", value="'.$numero_preguntas.'">';
    echo '<input type="hidden" id="numpreguntas'.$i.'", value="'.$numero_preguntas.'">';


    $j = 1;
    //var_dump($numero_preguntas);
    foreach ($preguntas[$i-1] as $pregunta) {
        echo '<div id="ctn'.$i.''.$j.'"  style="display:none;">';
        echo '<center>';
        //echo "<p>Pregunta ".$j."</p>";
        echo '<p class="trivia-pregunta-label">'.$pregunta['question_label'].'</p>';
        echo '<div class="btn-group-vertical" data-toggle="buttons" id="Respuesta'.$i.$j.'">';
        $k = 1;
        $resp_correct = "";
        //echo '<input type="text" value="" id="respuestaOK'.$i.$j.'" name="respuestaOK'.$i.$j.'" ></input>';

        foreach ($respuestas[$i-1][$j-1] as $respuesta) {
            if((int)$respuesta['is_correct']==1) {
                $resp_correct=$respuesta['answer_label'];
            }
        }

        foreach ($respuestas[$i-1][$j-1] as $respuesta) {
            echo '<label class="btn btn-success2">'; 
            $val = $respuesta['answer_id'].'|'.(int)$respuesta['is_correct'].'|'.$pregunta['question_score'].'|'.$j.'. '.$respuesta['answer_label'].'|'.$resp_correct;
            echo '<input type="radio" name="Respuesta'.$i.$j.'" id="opcion'.$i.$j.$k.'" autocomplete="off" value="'.$val.'" onchange="display(this)">'.$respuesta['answer_label'].'</input>';    
            
            echo '</label>';
            //echo '<br>';
            $k++;
        }                              
        //echo '<input type="text" value="" id="respuestaSelect'.$i.$j.'" name="respuestaSelect'.$i.$j.'" ></input>';
        echo '<input type="hidden" value="" id="respuestaSelect'.$i.$j.'" name="respuestaSelect'.$i.$j.'" ></input>';
        if ($numero_preguntas == $j)
        {
            //echo "<br>Esta es la ultima";
            //echo '<button id="fin'.$i.'" type="button" class="btn btn-rounded btn-rounded-green" data-dismiss="modal" onclick="onClickFin()">Finalizar</button>';
            //echo '<button id="next'.$i.'" type="button" class="btn btn-rounded btn-rounded-green" onclick="viewResults()">Finalizar</button>';
        }
        echo '</center>';
        echo '</div>';
    
        $j++;                               
    }
    echo '<div id="ctnfinal'.$i.'"  style="display:none;">';
    echo '<center>';
        echo '<div id="resultado'.$i.'"></div>';
        echo '<button id="fin'.$i.'" type="button" class="btn btn-rounded btn-rounded-green-modal" data-dismiss="modal" >Cerrar</button>';
    ?>
        <div class="load-related-trivia" data-bookid="<?php echo $trivia['books'][0]['id_libro']; ?>">
            
        </div>
    <?php
    echo '</center>';
    echo '<div id="sharebox'.$i.'">';
    
    echo '</div>';
    //echo '<p style="float:left;font-size:75%"><bold>Me Gusta | </bold>'.$trivia['trivia_total_likes'].' Me Gusta</p>';
    echo '<div id="likesfinal">';
    if ($trivia['liked'])
    {
        echo '<p class="likes" id="likefin'.$i.'"><span class="like-link" onclick="dislike_trivia()">Ya no me gusta</span><bold style="color:#989898"> | '.$trivia['trivia_total_likes'].' me gusta</bold></p>';
    }
    else
    {
        if ($trivia['trivia_total_likes'] > 0)
        {
            echo '<p class="likes" id="likefin'.$i.'"><span class="like-link" onclick="like_trivia()">Me gusta</span><bold style="color:#989898"> | '.$trivia['trivia_total_likes'].' me gusta</bold></p>';
        }
        else
        {
            echo '<p class="likes" id="likefin'.$i.'"><span class="like-link" onclick="like_trivia()">Me gusta</span><bold style="color:#989898"> | Se el primero en decir que te gusta esto</bold></p>';
        }
        
    }
    echo '<div style="clear: both;"></div>';
    echo '</div>';
    echo '</center>';
    echo '</div>';

    echo '</div>';
    echo '<div class="modal-footer" style="align-content: left;text-align: left;">';
    echo '<div id="respuestas-fin'.$i.'"></div>';
    echo '<div id="keywords'.$i.'"><p class="keyw-p">Palabras clave: ';
    $claves = explode(", ", $trivia['keywords']);
    $count = 0;
    foreach ($claves as $clave)
    {
        if ($count != 0)
        {
            echo '<span class="keywords">'." ".$clave." ".'</span>&nbsp&nbsp';
        }
        else
        {
            echo '<span class="keywords">'." &nbsp".$clave." ".'</span>&nbsp&nbsp';
        }
        $count++;
        
    }
    //echo "<p>".$trivia['keywords']."</p>";
    echo "</p></div>";
    echo "<center>";
    //echo '<button id="next'.$i.'" type="button" class="btn btn-primary" onclick="changeContent()">Siguiente</button>';

    echo '<div style="display:none;text-align:center;" id="Paginasalt'.$i.'">';
    $j=1;
    foreach ($preguntas[$i-1] as $pregunta) 
    {
        echo '<button id="PaginaLabel'.$i.$j.'" type="button" class="btn btn-paginator" disabled="disabled" value="'.$j.'" onclick="changecontentbynumpage('.$j.')">'.$j.'</button>';
        
        $j++;
    }
    echo '</div>';

    echo '<div  class="btn-group" data-toggle="buttons" style="display:none;text-align:center;" id="Paginas'.$i.'">';
    
    /*foreach ($preguntas_list[$i-1] as $pregunta) 
    {
        echo '<label class="btn btn-success" id="PaginaLabel'.$i.$j.'">'; 
        echo '<input type="radio" name="Pagina'.$i.'" id="Pagina'.$i.$j.'" autocomplete="off" value="'.$j.'" onchange="changecontentbynumpage('.$j.')">'.$j.'</input>';    
        echo '</label>';
        $j++;
    }*/
    echo '</div>';
    echo "</center>";
    echo '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    $i++;
}

?>
	
<!-- Motor de las trivias -->
<script type="text/javascript">
StopWatch = function()
{
    this.StartMilliseconds = 0;
    this.ElapsedMilliseconds = 0;
    this.TimeStr = "";
}  

StopWatch.prototype.Start = function()
{
    this.StartMilliseconds = new Date().getTime();
}

StopWatch.prototype.Stop = function()
{
    this.ElapsedMilliseconds = new Date().getTime() - this.StartMilliseconds;
    // strip the miliseconds
    this.ElapsedMilliseconds /= 1000;

    // get seconds
    var seconds = Math.round(this.ElapsedMilliseconds % 60);
    var strsec;
    if (seconds<10)
    {
        strsec = "0"+seconds;
    }
    else
    {
        strsec = ""+seconds;
    }

    // remove seconds from the date
    this.ElapsedMilliseconds = Math.floor(this.ElapsedMilliseconds / 60);

    // get minutes
    var minutes = Math.round(this.ElapsedMilliseconds % 60);
    var strmin;
    if (minutes<10)
    {
        strmin = "0"+minutes;
    }
    else
    {
        strmin = ""+minutes;
    }

    // remove minutes from the date
    this.ElapsedMilliseconds = Math.floor(this.ElapsedMilliseconds / 60);

    // get hours
    var hours = Math.round(this.ElapsedMilliseconds);
    var strhr;
    if (hours<10)
    {
        strhr = "0"+hours;
    }
    else
    {
        strhr = ""+hours;
    }

    this.TimeStr = strhr + ":" + strmin + ":" + strsec;
    setTimeout(display, 1000);
}

var preguntaActual = 0;
var respuestasTrivia = [];
var total_preguntas = 0;
var triviaelegida = 0;
var reloj = new StopWatch();

function reset()
{
    preguntaActual = 0;
    respuestasTrivia = [];
    total_preguntas = 0;
    triviaelegida = 0;
}


function changeContent()
{
    changecontentbynumpage(parseInt(preguntaActual)+1);
}

function changecontentbynumpage(paginaclickeada)
{
    var trivia =  document.getElementById('TriviaElegida').value;
    //La variable global PreguntaActual tiene la pregunta donde estamos, 0 es la portada
    //PreguntaActual es donde estoy
    total_preguntas = parseInt(document.getElementById('numpreguntas'+trivia).value);
    //console.log ("Numero de preguntas: "+total_preguntas);
    var preguntaSiguiente = paginaclickeada;
    var Respuesta = $('#respuestaSelect'+trivia+''+(preguntaActual)).val();
    respuestasTrivia[preguntaActual-1]= Respuesta;
    //console.log("el ctn actual es"+"#ctn"+trivia+''+preguntaActual+". El que sigue es: "+"#ctn"+trivia+''+preguntaSiguiente);
    //oculto todo
    var i = 0;
    for (i = 0; i<total_preguntas; i++)
    {
        $("#ctn"+trivia+''+i).hide();//lo oculto
        document.getElementById('ctn'+trivia+''+i).hidden = true;
    }

    $("#ctn"+trivia+''+preguntaActual).hide();//lo oculto
    document.getElementById('ctn'+trivia+''+preguntaActual).hidden = true;
    $("#ctn"+trivia+''+preguntaSiguiente).show();//muestro el siguiente (Que es pregunta actual mas uno)
    document.getElementById('ctn'+trivia+''+preguntaSiguiente).hidden = false;
    $("#Paginas"+trivia).show();//seguimos mostrando el paginador de la trivia
    document.getElementById('Paginas'+trivia).hidden = false;
     $("#Paginasalt"+trivia).show();//seguimos mostrando el paginador de la trivia
    document.getElementById('Paginasalt'+trivia).hidden = false;
    $("#keywords"+trivia).hide();//ocultamos las palabras clave
    //document.getElementById('keywords'+trivia).hidden = true;
    $(document.getElementById("PaginaLabel"+trivia+""+preguntaActual)).removeClass('active'); //cambiamos el boton activo 
    $(document.getElementById("PaginaLabel"+trivia+""+preguntaActual)).addClass('passed ');
    $(document.getElementById("PaginaLabel"+trivia+""+preguntaSiguiente)).addClass('active');
    $(document.getElementById("PaginaLabel"+trivia+""+preguntaSiguiente)).removeAttr("disabled");
    $(document.getElementById("PaginaLabel"+trivia+""+preguntaSiguiente)).prop('enabled', 'no-matter-what-you-write-here');

    //cambiamos el title
    $('#modal-trivia-titulo'+trivia).hide();
    document.getElementById('modal-trivia-titulo-alt'+trivia).hidden = false;
    $("#modal-trivia-titulo-alt"+trivia).show();
    if (preguntaActual == 0)
    {
        //alert("aqui iniciaría el timer");
        reloj.Start();

    }
    preguntaActual = preguntaSiguiente; //decimos que pregunta actual sera la que sigue
}

function display(obj) {

    if(typeof obj === 'undefined'){
    return;
    }

    var valor = obj.value;
    var trivia =  document.getElementById('TriviaElegida').value;

    document.getElementById('respuestaSelect'+trivia+''+preguntaActual).value = valor;
    //document.getElementById('respuestaSelectTxt'+trivia+''+preguntaActual).value=ansLabel;
    var trivia =  document.getElementById('TriviaElegida').value;
    
    var Respuesta = $('#respuestaSelect'+trivia+''+(preguntaActual)).val();
        respuestasTrivia[preguntaActual-1]= Respuesta;
    if (preguntaActual != total_preguntas)
    {
        changeContent();
    }
    else
    {
        viewResults();
    }
    
}

function viewResults()
{
    var trivia =  document.getElementById('TriviaElegida').value;
    reloj.Stop();
    if (respuestasTrivia.length > 0)
    {
        
        var triviaId = <?php echo json_encode($trivia['trivia_id']);?>;
        var puntaje = 0;
        var total_preguntas = respuestasTrivia.length;
        var aciertos = 0;
        var triviaIMG = <?php echo json_encode($trivias_list['trivia']['trivia_name']);?>;
        //console.log("triviaId: "+triviaId);

        for (var i = 0; i<respuestasTrivia.length; i++)//para cada respuesta
        {
            if (respuestasTrivia[i] == undefined)
                alert ('No has respondido a todas las preguntas');
            if (respuestasTrivia[i].split("|")[1] == 1)//checamos que sea correcta
            {
                puntaje = puntaje + parseInt(respuestasTrivia[i].split("|")[2]);//acumulamos los valores
                aciertos++;
            }
        }
        var porcentaje_calif = (aciertos*100)/total_preguntas;
        //console.log("Si respondo, el score es: "+aciertos+ " de "+total_preguntas+" un "+porcentaje_calif+"%correcto.");
       

        var data = {triviaid: triviaId, score: puntaje, like: 0, time: '00:03:15',name: 'Johnny Bravo'};
        if (porcentaje_calif >= 0 && porcentaje_calif < 25)
        {
            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">¡Lástima! Parece ser que te equivocaste de libro.</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
        }
        if (porcentaje_calif >= 25 && porcentaje_calif < 50)
        {
            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">¡Cuidado! Al parecer no prestaste suficiente atención a tu lectura.</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
        }
        if (porcentaje_calif >= 50 && porcentaje_calif < 75)
        {
            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">¡Buen intento! Te recomendamos que repitas tu lectura para mejorar.</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
        }
        if (porcentaje_calif >= 75 && porcentaje_calif < 100)
        {
            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">¡Bien hecho! Un poco más y te podrás considerar un experto.</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
        }
        if (porcentaje_calif == 100)
        {
            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia"> ¡Excelente trabajo! Eres un experto en el tema.</p><p class="result-gral-trivia">Tuviste todas las respuestas correctas.</p>';
        }

        var labelAns=[];
        var respuestasContent = '<div class= "tabla-result"><center><table >';
        for (var i = 0; i<respuestasTrivia.length; i++)//para cada respuesta
        {
            if (respuestasTrivia[i].split("|")[1] == 1)
            {
                labelAns[i] = respuestasTrivia[i].split("|")[3];//pintamos los enunciados
                respuestasContent = respuestasContent + '<tr valign="middle"><td><span class="trivia-results-ok">'+labelAns[i]+'</span></td><td><img src="assets/images/backend/trivias-palomita.svg"></td><td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td></tr>';
            }
            else
            {
                labelAns[i] = respuestasTrivia[i].split("|")[3];//pintamos los enunciados
                respuestasContent = respuestasContent + '<tr valign="middle"><td><span class="trivia-results-mal">'+labelAns[i]+'</span></td><td><img src="assets/images/backend/trivias-tache.svg"></td><td><span class="trivia-results-neutro">Respuesta correcta: '+respuestasTrivia[i].split("|")[4]+'</span></td></tr>';
            }
            
        }
        
        respuestasContent = respuestasContent + '</table></center></div>'

        var titulo = 'Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.';
        document.getElementById('respuestas-fin'+trivia).innerHTML = respuestasContent;
        
        trivia = trivia -1;
        //console.log(trivia);
        var triviasList = <?php echo json_encode($trivias);?>;
        
        triviaId = triviasList[trivia]['trivia_id'];
        var titulofb = triviasList[trivia]['trivia_name'];
        var descfb = 'Obtuve '+aciertos+' aciertos de '+total_preguntas+' al contestar la trivia &#92&#34'+triviasList[trivia]['trivia_name']+'&#92&#34';
        var imglibrofb = "";
        var share_url = window.location.origin + '/trivias/findid/'+triviaId;
        
        if (triviasList[trivia]['books'][0] != null)
        {
            //console.log(triviasList[trivia]['books'][0]['imagen']);
            imglibrofb = triviasList[trivia]['books'][0]['imagen'];
        }
        else
        {
            //console.log("No existe");
            imglibrofb="http://cdn.librosmexico.mx/assets/images/generic_book.jpg";
        }

        var titulotw = 'Obtuve '+aciertos+' aciertos de '+total_preguntas+' al contestar la trivia "'+triviasList[trivia]['trivia_name']+'" vía @LibrosMexicoMX';

        var sharecontent= ' <center>'+
                          '<p><p class="share-results-trivia">Comparte tus resultados</p>'+
                          '<img onclick="shareFB(&quot;'+titulofb+'&quot;,&quot;'+descfb+'&quot;,&quot;'+imglibrofb+'&quot;,&quot;'+share_url+'&quot;)" src="assets/images/detail/facebook.svg" class="social-medios-mini pointer" style="margin-bottom: 32px;cursor:pointer!important;">'+
                          '&nbsp;&nbsp;&nbsp;'+
                          '<a class="a2a_button_twitter" target="_blank" onclick="window.open(&#34http://www.addtoany.com/add_to/twitter?linkurl='+encodeURIComponent(share_url)+'&amp;linkname='+encodeURIComponent(titulotw)+'&amp;linknote=&#34,&#34_blank&#34,&#34top=200, left=200, width=450, height=500&#34)" rel="nofollow" aria-label="Twitter"><span class="a2a_svg a2a_s__default a2a_s_twitter" style="width: 40px; line-height: 40px; height: 40px; border-radius: 2px; background-size: 40px; display: inline-block;;"></span></a>'+
                          '</p>'+
                          '</center>';

        document.getElementById('sharebox'+(trivia+1)).innerHTML = sharecontent;

        var content_boton_afuera = '<button class="lm-btn" data-toggle="modal" data-target="#trivia'+(trivia+1)+'" onclick="ver_resultados('+(trivia+1)+')" value="'+(trivia+1)+'">Ver resultados</button>'
        document.getElementById('trivia-action'+(trivia+1)).innerHTML = content_boton_afuera;

        
    }
    else
    {
        //console.log("Si respondo, <br>No hay respuestas porque no hay pregutnas");
        var triviaId = <?php echo json_encode($trivias_list);?>;

        var data = {triviaid: triviaId, score: puntaje, like: 0, time: '00:00:00',name: 'Johnny Bravo'};
        document.getElementById('resultado'+(trivia+1)).innerHTML = '<ol><li>No hubo preguntas</li></ol>';
        preguntaActual = 0;
    }
    trivia = trivia +1;

    $("#ctn"+trivia+''+preguntaActual).hide();
    document.getElementById('ctn'+trivia+''+preguntaActual).hidden = true;
    $("#ctnfinal"+trivia).show();
    document.getElementById('ctnfinal'+trivia).hidden = false;
    $("#fin"+trivia).show();
    document.getElementById('fin'+trivia).hidden = false;
    $("#next"+trivia).hide();
    document.getElementById('next'+trivia).hidden = true;
    document.getElementById('Paginasalt'+trivia).hidden = true;
    $("#Paginasalt"+trivia).hide();
    onClickFin();
    reset();
}


function ver_resultados(triviapos)
{
    reset();
    document.getElementById('TriviaElegida').value=parseInt(triviapos);
    //obtengo el valor de la trivia elegida
    var trivia =  document.getElementById('TriviaElegida').value;
    //oculto todo
    total_preguntas = parseInt(document.getElementById('numpreguntas'+trivia).value);
    var i = 0;
    var respuestas = "";
    for (i = 0; i<total_preguntas; i++)
    {
        $("#ctn"+trivia+''+i).hide();//lo oculto
         document.getElementById('ctn'+trivia+''+i).hidden = true;
    }
    //ocultamos las palabras clave
    $("#keywords"+trivia).hide();
    //cambiamos el title
    $('#modal-trivia-titulo'+trivia).hide();
    document.getElementById('modal-trivia-titulo-alt'+trivia).hidden = false;
    $("#modal-trivia-titulo-alt"+trivia).show();

    //construímos la tabla de aciertos
    triviasList = <?php echo json_encode($trivias);?>;
    //triviasList = <?php echo json_encode($trivias);?>;
    /*esta parte hay que cambiarla por un ajax que me traiga estos mismos resultados*/
    console.log(JSON.stringify(triviasList[trivia-1]));
    console.log("Trivia id = "+triviasList[trivia-1]['trivia_id']);
    var triviaID = triviasList[trivia-1]['trivia_id'];//de aqui saco el id de la trivia
    var userID = <?php echo $user_id?$user_id:'0';?>;//de esta saco el id del usuario
    //ahora si, el ajax
    $.ajax({
              url: "trivias/verresults",
              method: "POST",
              data:{user_id: userID,
                    trivia_id:triviaID},
              error: function()
                {
                    console.log("Error trayendo los resultados de la tabla.");
                },
              success: function(response) 
              {
                console.log("Todo OK al parecer");
              },
            }).done(function(data){
                console.log("Ponemos que data trajimos en el ajax:");
                var result = data;
                //aqui debo armar y mostrar lo que traje si no se queda undefined
                respuestas = JSON.parse(data);
                console.log(respuestas);

                console.log("esto es lo que trae el ajax (espero):")
    console.log(JSON.stringify(respuestas));
    //respuestas = 
    var aciertos=0;
    total_preguntas=respuestas.length;
    var respuestasContent = '<div class= "tabla-result"><center><table >';
    for (var index = 0; index < respuestas.length; index++)
    {
        if (respuestas[index]['is_correct'] == 1)
            {
                //pintamos los enunciados
                respuestasContent = respuestasContent + '<tr valign="middle"><td><span class="trivia-results-ok">'+respuestas[index]['answer_label']+'</span></td><td><img src="assets/images/backend/trivias-palomita.svg"></td><td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td></tr>';
                aciertos++;
            }
            else
            {
                //pintamos los enunciados
                respuestasContent = respuestasContent + '<tr valign="middle"><td><span class="trivia-results-mal">'+respuestas[index]['answer_label']+'</span></td><td><img src="assets/images/backend/trivias-tache.svg"></td><td><span class="trivia-results-neutro">Respuesta correcta: '+respuestas[index]['correct_label']+'</span></td></tr>';
            }

    }
    respuestasContent = respuestasContent + '</table></center></div>'
    var porcentaje_calif = (aciertos*100)/total_preguntas;

    if (porcentaje_calif >= 0 && porcentaje_calif < 25)
        {
            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">¡Lástima! Parece ser que te equivocaste de libro.</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
        }
        if (porcentaje_calif >= 25 && porcentaje_calif < 50)
        {
            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">¡Cuidado! Al parecer no prestaste suficiente atención a tu lectura.</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
        }
        if (porcentaje_calif >= 50 && porcentaje_calif < 75)
        {
            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">¡Buen intento! Te recomendamos que repitas tu lectura para mejorar.</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
        }
        if (porcentaje_calif >= 75 && porcentaje_calif < 100)
        {
            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">¡Bien hecho! Un poco más y te podrás considerar un experto.</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
        }
        if (porcentaje_calif == 100)
        {
            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia"> ¡Excelente trabajo! Eres un experto en el tema.</p><p class="result-gral-trivia">Tuviste todas las respuestas correctas.</p>';
        }

    var titulo = 'Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.';
    document.getElementById('respuestas-fin'+trivia).innerHTML = respuestasContent;
    console.log(respuestasContent);



    //muestro el final
    $("#ctnfinal"+''+trivia).show();//lo oculto
    document.getElementById('ctnfinal'+''+trivia).hidden = false;

     trivia = trivia -1;
        //console.log(trivia);
        var triviasList = <?php echo json_encode($trivias);?>;
        
        triviaId = triviasList[trivia]['trivia_id'];
        var titulofb = triviasList[trivia]['trivia_name'];
        var descfb = 'Obtuve '+aciertos+' aciertos de '+total_preguntas+' al contestar la trivia &#92&#34'+triviasList[trivia]['trivia_name']+'&#92&#34';
        var imglibrofb = "";
        var share_url = window.location.origin + '/trivias/findid/'+triviaId;
        //var share_url = 'http://librosmexico.mx';
        if (triviasList[trivia]['books'][0] != null)
        {
            //console.log(triviasList[trivia]['books'][0]['imagen']);
            imglibrofb = triviasList[trivia]['books'][0]['imagen'];
        }
        else
        {
            //console.log("No existe");
            imglibrofb="http://cdn.librosmexico.mx/assets/images/generic_book.jpg";
        }

        var titulotw = 'Obtuve '+aciertos+' aciertos de '+total_preguntas+' al contestar la trivia "'+triviasList[trivia]['trivia_name']+'" vía @LibrosMexicoMX';


        var sharecontent= ' <center>'+
                          '<p><p class="share-results-trivia">Comparte tus resultados</p>'+
                          '<img onclick="shareFB(&quot;'+titulofb+'&quot;,&quot;'+descfb+'&quot;,&quot;'+imglibrofb+'&quot;,&quot;'+share_url+'&quot;)" src="assets/images/detail/facebook.svg" class="social-medios-mini pointer" style="margin-bottom: 32px;cursor:pointer!important;">'+
                          '&nbsp;&nbsp;&nbsp;'+
                          '<a class="a2a_button_twitter" target="_blank" onclick="window.open(&#34http://www.addtoany.com/add_to/twitter?linkurl='+encodeURIComponent(share_url)+'&amp;linkname='+encodeURIComponent(titulotw)+'&amp;linknote=&#34,&#34_blank&#34,&#34top=200, left=200, width=450, height=500&#34)" rel="nofollow" aria-label="Twitter"><span class="a2a_svg a2a_s__default a2a_s_twitter" style="width: 40px; line-height: 40px; height: 40px; border-radius: 2px; background-size: 40px; display: inline-block;;"></span></a>'+
                          '</p>'+
                          '</center>';

        document.getElementById('sharebox'+(trivia+1)).innerHTML = sharecontent;

        var content_boton_afuera = '<button class = "btn btn-rounded btn-rounded-green"  data-toggle="modal" data-target="#trivia'+(trivia+1)+'" onclick = "ver_resultados('+(trivia+1)+')" value="'+(trivia+1)+'">Ver resultados</button>';
        document.getElementById('trivia_action'+(trivia+1)).innerHTML = content_boton_afuera;
        //console.log(trivia+1);


            });
    

        
}

function select_trivia(triviapos)
{
    reset();
    document.getElementById('TriviaElegida').value = parseInt(triviapos);
    
    load_related_books_to_trivia(triviapos);
}

/**
 * Carga los libros relacionados
 * @param  {string} id del elemento
 */
function load_related_books_to_trivia(trivia_pos) {
    var e = parseInt(trivia_pos);
    var trivia_id = '#trivia' + e;
    var final_target = trivia_id + ' .load-related-trivia';
    var book_id = $(final_target).data('bookid');
    $.ajax({
        url: "libros/" + book_id + "/relacionados",
        type: "post",
        data: {},
        dataType: "json",
        success: function (data) {
            if (data.status == "OK") {
                if (data.related_books != null) {
                    var html = '<p class="recomendacion-trivia">Te recomendamos leer: </p>';
                    for (var i in data.related_books) { 
                        var book = data.related_books[i];
                        html += '<a href="libros/' + book.id + '" target="_blank">';
                            html += '<img src="' + book.img_mediana + '" class="home-list-book list-books-trivia">';
                        html += '</a>';
                    }
                    $(final_target).html(html);
                }
            }
        }
    });
}

function onClickFin()
{
    if (respuestasTrivia.length > 0)
    {
        
        var trivia =  parseInt(document.getElementById('TriviaElegida').value);
        trivia = trivia -1;
        var triviasList = <?php echo json_encode($trivias);?>;
        triviaId = triviasList[trivia]['trivia_id'];

        var puntaje = 0;
        var aciertos = 0;
        for (var i = 0; i<respuestasTrivia.length; i++)//para cada respuesta
        {
            if (respuestasTrivia[i].split("|")[1] == 1)//checamos que sea correcta
            {
                puntaje = puntaje + parseInt(respuestasTrivia[i].split("|")[2]);//acumulamos los valores
                aciertos++;
            }
        }
        calificacion = (aciertos*100)/respuestasTrivia.length;
        //console.log("Si respondo, <br>Las respuestas son: "+respuestasTrivia);

         var data = {triviaid: triviaId, score: puntaje, porcentaje: calificacion, like: 0, time: reloj.TimeStr,resptrivia: JSON.stringify(respuestasTrivia)};
        postajax('trivias/contestar_commit', data);
    }
    
    
}

function post(path, params, method) 
{
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) 
    {
        if(params.hasOwnProperty(key)) 
        {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

function postajax(path, params)
{
    event.preventDefault();
    var request = $.ajax({
            'url': path,
            'type':'POST',
            'data': params,
            'async': true,
            'dataType': "json"
        });
    return false;
}

function shareFBtrivia(title, desc, img, url)
{
    var product_name   =    title;
    var description    =    desc;
    var share_image    =    img;
    var share_url      =    url;
    var share_caption  =    'librosmexico.mx';
    FB.ui({
        method: 'feed',
        appId: '394615860726938',   
        name: title,
        link: share_url,
        picture: share_image,
        caption : share_caption,
        description: description
        }, function(response) {
            if(response && response.post_id){

            }
            else{}
        }); 
}

function shareTWtrivia(url, titulo)
{
    //titulo = titulo + ' en LIBROSMEXICO.MX';
    var params = {
                    access_token: "65efccf36ae26915b2362f75c2b09b8856732202",
                    longUrl: url,
                    format: 'json'
                 };
    $.getJSON('https://api-ssl.bitly.com/v3/shorten', params, function (response, status_txt) {
    var urlbit =response.data.url;
    var len = 140 - ((urlbit.length)+7+14);  

    if (titulo.length>len) 
    {
        titulo = titulo.substring(0,len);
        titulo = '"'+titulo+'..."';
    } 
    else 
    {
        titulo = '"'+titulo+'"';
    }
    var device = navigator.userAgent;
    var shareURL = 'http://twitter.com/intent/tweet?text='+titulo+' @LibrosMexicoMX '+urlbit;

    if (device.match(/Iphone/i)|| device.match(/Ipod/i)|| device.match(/Android/i)|| device.match(/J2ME/i)|| device.match(/BlackBerry/i)|| device.match(/iPhone|iPad|iPod/i)|| device.match(/Opera Mini/i)|| device.match(/IEMobile/i)|| device.match(/Mobile/i)|| device.match(/Windows Phone/i)|| device.match(/windows mobile/i)|| device.match(/windows ce/i)|| device.match(/webOS/i)|| device.match(/palm/i)|| device.match(/bada/i)|| device.match(/series60/i)|| device.match(/nokia/i)|| device.match(/symbian/i)|| device.match(/HTC/i)){
        location.target="_newtab";
        location.href = shareURL; 
    }   
    else 
    {

        window.open('http://twitter.com/intent/tweet?text='+titulo+' @LibrosMexicoMX '+urlbit ,"_blank","top=200, left=500, width=400, height=400");
    }

    });
}

function like_trivia()
{
    var trivia =  document.getElementById('TriviaElegida').value;
    trivia = trivia-1;
    var triviasList = <?php echo json_encode($trivias);?>;
    var trivialikes = parseInt(triviasList[trivia]['trivia_total_likes']);
    var triviaId = parseInt(triviasList[trivia]['trivia_id']);
    trivialikes++;
    var contenido = '<span class="like-link" onclick="dislike_trivia()">Ya no me gusta</span><bold style="color:#989898"> | '+trivialikes+' me gusta</bold>';
    trivia = trivia+1;
    document.getElementById('likeportada'+trivia).innerHTML = contenido;
    document.getElementById('likefin'+trivia).innerHTML = contenido;
    var data = {triviaid: triviaId, likes: trivialikes};
    likepostajax('trivias/like', data);
    //likepost('trivias/like', data);
}

function dislike_trivia()
{
    var trivia =  document.getElementById('TriviaElegida').value;
    trivia = trivia-1;
    var triviasList = <?php echo json_encode($trivias);?>;
    var trivialikes = parseInt(triviasList[trivia]['trivia_total_likes']);
    var triviaId = parseInt(triviasList[trivia]['trivia_id']);
    trivialikes--;
    var contenido = '<span class="like-link" onclick="like_trivia()">Me gusta</span><bold style="color:#989898"> | '+trivialikes+' me gusta</bold>';
    trivia = trivia+1;
    document.getElementById('likeportada'+trivia).innerHTML = contenido;
    document.getElementById('likefin'+trivia).innerHTML = contenido;
    var data = {triviaid: triviaId, likes: trivialikes};
    likepostajax('trivias/dislike', data);
    //likepost('trivias/dislike', data);
}

function likepostajax(path, params)
{
    event.preventDefault();
    var request = $.ajax({
            'url': path,
            'type':'POST',
            'data': params,
            'async': true,
            'dataType': "json"
        });
    return false;
}

function likepost(path, params, method) 
{
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) 
    {
        if(params.hasOwnProperty(key)) 
        {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

</script>
