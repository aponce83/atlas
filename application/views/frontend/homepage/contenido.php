<?php 
session_start();
?>
<script type="text/javascript" src="assets/scripts/home.js"></script>
<script type="text/javascript">
function displayMore(){	
	$(".renowned_list").toggle();
}
</script>

<style type="text/css">
.own-list a{
	font-family: 'open_sanssemibold';
    font-size: 16px;
    text-decoration: none;
    text-align: right;
    color: #354f5e;    
}

.own-list {
	border-bottom: 1px solid #ddd;
	margin-bottom: 10px !important;
}
.renowned_list{
	display: none;
}
.see-more-list span{
	font-family: 'Montserrat';
    font-size: 18px;
    text-decoration: none;
    text-align: center;
    color: #354f5e;    
}
.see-more-list{
    text-align: center;   
}
.list-push-right-own{
	margin-right: 3px;
}
@media screen and (max-width: 691px) {
	.list-push-right-own{	
	    margin: 0 auto;
	    float: none;
	    text-align: center;
	    margin-right: -4px;
	}
}
.section-see-list-own a{
	    margin-bottom: 10px !important;
		font-family: 'Montserrat';
	    font-size: 18px;
	    text-decoration: none;
	    color: #354f5e;    	    
	    
}
.section-see-list-own{
		font-family: 'Montserrat';
    	font-size: 18px;
	    border-bottom: 1px solid #ddd;
	    margin-bottom: 10px !important; 
	    text-align: right;	    
}
.section-see-list-more a{
	    margin-bottom: 10px !important;
		font-family: 'Montserrat';
	    font-size: 18px;
	    text-decoration: none;
	    text-align: center !important;
	    color: #354f5e;    	    
}
.section-see-list-more{	
	    margin-bottom: 10px !important;
		font-family: 'Montserrat';
	    font-size: 18px;
	    text-decoration: none;
	    text-align: center !important;
	    color: #354f5e;    	    
}
.section-list-title-own{
	font-family: 'Montserrat';
	font-size: 18px;	
    margin-bottom: 10px !important;
}
.section-list-title-own a{
	color:#000;
	text-decoration: none;
}
@media screen and (max-width: 691px) {
	.home-list-book{	
		width: 68px !important;
	    height: 104px !important;
	}
}
.contenedor {
	width: 260px;
	height: 170px;
	margin: 10px 13px auto; 
	position: absolute;
	
}

.contenedor-left{
	width: 260px;
	height: 170px;
	margin: 10px 30px;
	position: absolute;
	
}


.tip {
	color: white;
	font-size: 16px;
	text-align: left;
	font-family: 'Montserrat', sans-serif; font-weight: 400;
	margin-top: -8px;


}

.titulo-tip {
	color: #3db29c;
	font-size: 14px;
	text-align: left;
	font-family: 'Montserrat', sans-serif; font-weight: 400;
	margin-top: 5px;

}

.small {
	color: white;
	font-size: 14px;
	text-align: left;
	font-family: 'Montserrat', sans-serif; font-weight: 100;
	line-height: 1.5em;
	margin-top: 3px;
}

.indicador {
	color: white;
	font-size: 12px;
	text-align: right;
	font-family: 'Montserrat', sans-serif; font-weight: 100;
	line-height: 1.5em;
}

.hr {
	height: 1px;
    border: 0;
    background-color: white;
    margin: -5px auto;

}
.contenedor-botones {
	float: right;
	margin-top: 9px;
}
.boton-omitir {
	color: #78919f;
	font-size: 12px;
	text-align: right;
	font-family: 'Montserrat', sans-serif; font-weight: 200;
	text-decoration: none;

}

.boton {
	
	display:inline-block;
	padding: 5px 0px;
	margin-bottom: 0px;
	width: 100px;
	background-image: none;
	color: white;
	font-size: 12px;
	font-family: 'Montserrat', sans-serif; font-weight: 200;
	margin:3px;
	text-align: center;
	white-space: nowrap;
    vertical-align: middle;
	text-decoration: none;
	-webkit-user-select: none;
	cursor: pointer;
	
}

.boton-rounded {
	border-radius: 15px;
	
}

.boton-siguiente {
	margin-left: 10px;
    color: #fff;
    border: 1px solid #fff;
}

.no-hover-tooltip:hover {
	color: white !important;

}

.omitir-hover:hover {
	color: #78919f !important;
}

 p span {
 	color: #cfb047;
 }
 .margen { 
 	margin-top: 60px;
 }

 #tooltip-1 { 
 	position: absolute;
 	z-index: 10;
 }

#link-siguiente{

}

.signup-title {
	font-size: 36px!important;
  padding-top: 26px!important;
  padding-bottom: 0px!important;
}

.home-signup-span {
		color: #fff;
    font-size: 30px;
    margin-top: -2px;
    text-align: center;
    text-transform: none;
    display: block;
 }

 .home-signup-span-small {
		color: #fff;
    font-size: 18px;
    margin-top: 6px;
    padding-bottom: 20px;
    text-align: center;
    text-transform: none;
    display: block;
    font-family: 'Open Sans',sans-serif;
    font-weight: 300;
 }

 .home-signup {
    background-color: #354f5e!important;
    min-height: 253px!important;
    width: 100%!important;
}

.signup-btn {
    border: 1px solid #3db29c!important;
    color: #3db29c!important;
    background: none!important;
    border-radius: 30px;
    margin: 0 auto;
    margin-top: 10px!important;
    display: block;
    width: 290px;
}

.signup-btn:hover {
    border: 1px solid #3db29c!important;
    color: white!important;
    background-color: #3db29c!important;
    border-radius: 30px;
    margin: 0 auto;
    display: block;
    width: 290px;
}

.home-portrait {
	width: 48px!important;
	height: 48px!important;
	border: 1px solid #ddd;
  padding: 1px;
  margin-bottom: 10px!important;
}

.activity-who {
    color: #3db29c;
    font-family: 'Montserrat', sans-serif;
    font-weight: 400;
    font-size: 14px;
    padding-right: 8px;
}

.activity-who:hover {
	color: #3db29c;
	text-decoration: none;
}

.activity-desc-col {
    padding: 0 4px 0 23px!important;
}

.activity-hr {
	margin-bottom:20px!important;
}

.activity-who-list {
    color: black;
    font-family: 'Montserrat', sans-serif;
    font-weight: 400;
    font-size: 14px;
    padding-right: 8px;
}

.activity-who-list:hover {
	text-decoration: none;
}

.more-tag {
	font-size: 18px;
  text-align: right;
  display: block;
  position: relative;
  padding-right: 15px;
  text-decoration: none;
}

.more-arrow {
	font-size: 26px;
	position: absolute;	
	right: 0;
	top:0;
	font-size: 26px!important;
  position: absolute;
  color: #ef6c15!important;
}

.paging-hr {
    margin-top: 12px;
    margin-bottom: 20px!important;
    border: 0;
 }

 /**************/
 .top-item {
		 margin-bottom:20px;
	}
	.show-label {
		line-height: 33px;
		font-family: 'Open Sans',sans-serif;
		color: #888;
    font-weight: 400;
	}
	.pages-item {
		line-height: 33px; 
		text-align: right;
		font-family: 'Open Sans',sans-serif;
	}
	.page-divider {
		padding-left: 10px;
		padding-right: 10px;
	}

	.arrow-prev {
		position: absolute;
		top: 0px;
	}

	.arrow-next {
		position: absolute;
		right: -10px;
    top: -2px;
	}

	.arrow-link {
		font-size: 19px!important;
		cursor: pointer;
		color: #898989;
	}
	.number-link {
		font-size: 14px;
		font-family: 'Open Sans',sans-serif;
		padding: 0 7px;
		cursor: pointer;
		color: #898989;
		font-weight: 400;
	}
	.number-link:hover {
		text-decoration: underline!important;
	}
	.number-bar {
		font-size: 14px;
		font-family: 'Open Sans',sans-serif;
		color: #898989;
		font-weight: 400;
	}
	.pagination-word {
		font-size: 14px;
		font-family: 'Open Sans',sans-serif;
		color: #898989;
		font-weight: 400;
		padding-left: 5px;
		padding-right: 5px;
	}
	.pagination-col {
		float: right;
		text-align: right;
		position:relative;
	}
	.prev-word {
		padding-left: 17px;
	}

	.first-page-btn {
		cursor: pointer;
		position: relative;
    width: 20px;
    padding-right: 24px;
  }

  .last-page-btn {
		cursor: pointer;
		position: relative;
    width: 20px;
    padding-left: 20px;
  }

  .next-page-btn {
  	position:relative;
  }

  .arrow-first {
  	top: -2px!important;
  }
  .arrow-last {
  	top: -2px!important;
  }
  .last-btns {
  	position: absolute;
    display: inline-table;
    top: -2px;
  }
</style>
<link rel="stylesheet" type="text/css" href="assets/styles/activity.css"/>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>

<?php /* 
	foreach ($data['recent_books'] as $key => $recent) {
		foreach ($data['recent_books'] as $inKey => $inRecent) {
			if (($data['recent_books'][$inKey]['titulo']===$data['recent_books'][$key]['titulo']) && ($key!==$inKey)) {
			unset($data['recent_books'][$inKey]);
			}
		}	
	}

	foreach ($data['destacado'] as $key => $recent) {
		foreach ($data['destacado'] as $inKey => $inRecent) {
			if (($data['destacado'][$inKey]['titulo']===$data['destacado'][$key]['titulo']) && ($key!==$inKey)) {
			unset($data['destacado'][$inKey]);
			}
		}	
	} */
?>

	<div class="row no-margin home-row">
		<!-- Libros -->
		<div class="col col-xs-12 col-sm-8 col-md-8 col-lg-8 home-books">

			
			<!-- Novedades -->
			<?php if(isset($data['main_content']) && is_array($data['main_content'])) { ?>
				<h2><?=$data['main_title']?></h2>			
				<hr class="paging-hr orange-hr">
				<div class="row no-margin" id="content-page-wrapper">
					<?=$data['content_page']?>
			  </div>
			<?php } ?>

		</div>

		<!-- Informativos -->

		<div class="col col-xs-12 col-sm-4 col-md-4 col-lg-4 home-information">
			<div class="lectores-conectados">
				<h2 style="margin:0!important;"><?=$data['secondary_title']?></h2>		
				<hr class="red-hr">
			</div>	
			<div class="activity-renowned">
				<?php $i=0; foreach ($data['secondary_content']['books'] as $rBook) { 
					if ($i<4) { ?>
						<div class="row renowned-book-item">
							<div class="col col-xs-2 col-sm-3 col-md-3 col-lg-2 no-padding">
								<a target="_blank"  href="<?=Url::base()?><?='libros/'.$rBook['id']?>"><img src="<?=!$rBook['imagen']?'assets/images/generic_book.jpg':$rBook['imagen']?>" alt="<?=$rBook['titulo']?>" class="activity-extra-book" /></a>
							</div>
							<div class="col col-xs-10 col-sm-9 col-md-9 col-lg-10 renowned-act-info-col">
								<div class="row renowned-book-act-info">
									<a target="_blank"  href="<?=Url::base()?><?='libros/'.$rBook['id']?>" class="act-book-title"><?=$rBook['titulo']?></a>
									<?php $a=0; foreach ($rBook['autores'] as $autor) { 
										if ($a<1) { ?>
										<span class="act-book-author"><?=$autor[0]['nombre'] ?></span>
									<?php } $a++; } ?>
								</div>
								<div class="row no-margin">
									<?php for ($k=0; $k<5; $k++) { 
										if ($k<$rBook['rating']) { ?>
											<img src="assets/images/detail/estrella_amarilla_sm.svg" class="act-extra-book-star <?=($k>0)? 'act-extra-right':''?>">
										<?php } else { ?>	
											<img src="assets/images/detail/estrella_gris_sm.svg" class="act-extra-book-star <?=($k>0)? 'act-extra-right':''?>">
										<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>
				 <?php }
				$i++; } ?>
			</div>
			<br>	
			<div class="listas-destacadas">
				<h2>LISTAS DESTACADAS</h2>
				<hr class="blue-hr">
				<?php 
				$list_counter=0;
				$class='';
				if($data['renowned_lists']) { 
				?>
					<?php  foreach ($data['renowned_lists'] as $list ) { 
					  		if($list_counter>1)
								$class='renowned_list';						
					?>
					<div class='row no-margin  section-list-title section-list-title-own <?=$class?>'>
						<a href="lista/<?php echo $list->getUser(); ?>/<?=$list->slug?>"><?=$list->name?> (<?=$list->num_books?>)</a>
					</div>	
					  <div class="row no-margin <?=$class?>">
				      <?php $i=0; foreach ($list->books as $book ) { ?>
				      	<?php if ($i<5) { ?>
				        	<img src="<?=!$book['imagen']?'assets/images/generic_book.jpg':$book['imagen']?>" alt="<?=$book['titulo']?>" class="home-list-book list-push-right-own <?php if ($i==4) { ?> desk <?php } ?>" />
				        <?php } ?>
				      <?php $i++; } ?>
				    </div>
				    <div class="row section-see-list-own no-margin own-list <?=$class?>">
						<a href="lista/<?php echo $list->getUser(); ?>/<?=$list->slug?>">ver más <img src= 'assets/images/detail/flecha_azul.svg' class='arrow_list'></a>
				    </div>
			    <?php $list_counter++; }		    
			   } 
			  if($list_counter>1){
			  ?> 	<div class="row section-see-list-more no-margin see-more-list">
						<a href="<?=URL::base()?>listas"><span class='pointer'>ver más listas <img src= 'assets/images/detail/flecha_azul.svg' class='arrow_list'></span></a>
				    </div>
			   <?php }?>
			</div>
			<div class="cita">
				<h2>CITA DEL DÍA</h2>
				<hr>
				<?php if($data['random_quote']) { ?>
					<h4>"<?=utf8_decode($data['random_quote']['quote'])?>"</h4>
					<span>- <?=utf8_decode($data['random_quote']['author'])?></span>
					<a href="javascript:void(0);" style="cursor:default!important;">Compartir</a>
					<img onclick='shareFB("Cita del Día", "<?=utf8_decode($data['random_quote']['quote'])?>","https://librosmexico.mx/assets/images/libros_logo100x.png","http://librosmexico.mx")' src='assets/images/detail/facebook_cita.svg' class='social-medios-mini pointer' style="cursor:pointer!important;">
					<img src='assets/images/detail/twitter_cita.svg' class='social-medios-mini pointer' onclick="shareTW('https://librosmexico.mx','<?=utf8_decode($data['random_quote']['quote'])?>')" style="cursor:pointer!important;"></span>
			 <?php } ?>
			</div>

		</div>

	</div>	

	<!-- Tooltip 1 -->	
<!--form id="formToolTip1" method="post" action="">
	<div id="tooltip-1">
		<div class="contenedor">
			<p class="indicador">Tip 1 de 4</p>
			<h5 class="tip">Tip 1</h5>
			<h6 class="titulo-tip">Menú de usuario</h6>
			<p class="small"> <span> Actualizaciones:</span> Puedes ver todas las actividades que has realizado dentro de LIBROSMÉXICO y comentar las actividades de tus amigos.</p>
			<hr/ class="hr">
			<div class="contenedor-botones">
				<input type="hidden" name="omitir">
				<a class="boton-omitir omitir-hover" role="button">omitir</a>
				<a href="libros/<?=$data['random']?>" class="boton boton-rounded boton-siguiente no-hover-tooltip" id="link-siguiente" >siguiente </a>
			</div>
		</div>
		<img src="assets/images/background_tooltip.svg">
	</div>
</form-->
	

	<script>
	$(document).ready(function () {
		$(".sp-keyword-home").each(function () {
			var txt = $(this).text();
			$(this).html("<a href='buscar/libro?keywords="+txt+"'>"+txt+"</a>");
		});

		$("#tooltip-1").hide();

		$(".book-wrapper").hover(function() {
			$(this).children(".home-book-info-mask").removeClass("hidden");
		}, function() {
			$(this).children(".home-book-info-mask").addClass("hidden");
		});


		
		<?php
		//se obtiene id de usuario logueado
		if( isset($_SESSION['user_id']) )
		{
			$session_user_id = $_SESSION['user_id'];
			$tooltip_status = Model::factory('userfront')->getTootltipStatus($session_user_id);
			if( (int)$tooltip_status == 1 )
			{
				Model::factory('userfront')->updateTooltips( $_SESSION['user_id'], 2 );
			}
		}
		else
		{
			$tooltip_status = 0;
		}
		//se obtiene su tooltip_status
		?>


		var tooltip_status = <?php echo isset($tooltip_status) ? $tooltip_status : -1; ?>;
		tooltip_status = parseInt(tooltip_status);

		if( tooltip_status == 1 )
		{
			
			//mostrar tooltips
			setTimeout(function(){ 
				$('.usr-submenu-wrapper').removeClass('hidden');
				var position = $(".Actualizaciones").offset();
				var tooltip_width= $("#tooltip-1").width();	
				$("#tooltip-1") .offset ({top: (position.top- 10), left: (position.left-tooltip_width-5)});
				$("#tooltip-1").show();
			}, 800);
			

			$(".boton-omitir").click(function(){
				$("#tooltip-1").fadeOut(500);
				$('.usr-submenu-wrapper').addClass('hidden');
				var dataString = 'omitir=1';
				$.ajax({
	                type: "POST",
	                url: "ajax_tooltip",
	                data: dataString,
	                cache: false,
	                success: function(result) {
	                    
	                }
	            });
			});
		    
		}
		else
		{
			$("#tooltip-1").hide();
		}
		$( '.key-words-home' ).unbind( 'mousewheel DOMMouseScroll' ).bind( 'mousewheel DOMMouseScroll', function ( e ) {
		  var e0 = e.originalEvent;
		  var delta = e0.wheelDelta || -e0.detail;

		    this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
		    e.preventDefault();		    
		});
		// tooltip
		$(window).resize(function(){

			if( tooltip_status ==1 ) 
			{
				var ventana_ancho = $(window).width();
				var position = $(".Actualizaciones").offset();
				var tooltip_width= $("#tooltip-1").width();	
				$("#tooltip-1").offset ({top: (position.top- 10), left: (position.left-tooltip_width-5)});
	        	if (ventana_ancho <= 767)
	        	{
	        		$("#tooltip-1").hide();
	        	}
	        	 else {
	        	 	$("#tooltip-1").show();
	        	}
			}
			else
			{
				$("#tooltip-1").hide();
			}
	   	});

		
		$('#link-siguiente').click(function(){
			$("#tooltip-1").fadeOut(800);
			var dataString = 'update-tooltip=2';
			$.ajax({
	            type: "POST",
	            url: "ajax_tooltip",
	            data: dataString,
	            cache: false,
	            success: function(result) {}
	        });
		});	
	});

	</script>

	<script>
		function shareFB(title, desc, img, url){
			var product_name   = 	title;
			var description	   =	desc;
			var share_image	   =	img;
			var share_url	   =	url;
			var share_caption  = 	'librosmexico.mx';
			
		    FB.ui({
		        method: 'feed',
		      	appId: '394615860726938',   
		        name: title,
		        link: share_url,
		        picture: share_image,
		        caption : share_caption,
		        description: description
		    }, function(response) {
		        if(response && response.post_id){}
		        else{}
		    });	
		}

		function shareTW(url,titulo){
    var params = {
        access_token: "65efccf36ae26915b2362f75c2b09b8856732202",
        longUrl: url,
        format: 'json'
    };
    $.getJSON('https://api-ssl.bitly.com/v3/shorten', params, function (response, status_txt) {
        var urlbit =response.data.url;
        var len = 140 - (urlbit.length)+7;  

		if (titulo.length>len){
			titulo = titulo.substring(0,len);
			titulo = '"'+titulo+'..."';
		}else
			titulo = '"'+titulo+'"';

		var device = navigator.userAgent

		if (device.match(/Iphone/i)|| device.match(/Ipod/i)|| device.match(/Android/i)|| device.match(/J2ME/i)|| device.match(/BlackBerry/i)|| device.match(/iPhone|iPad|iPod/i)|| device.match(/Opera Mini/i)|| device.match(/IEMobile/i)|| device.match(/Mobile/i)|| device.match(/Windows Phone/i)|| device.match(/windows mobile/i)|| device.match(/windows ce/i)|| device.match(/webOS/i)|| device.match(/palm/i)|| device.match(/bada/i)|| device.match(/series60/i)|| device.match(/nokia/i)|| device.match(/symbian/i)|| device.match(/HTC/i)){
			location.target="_newtab";
			location.href='http://twitter.com/intent/tweet?text='+titulo+' '+urlbit;
		}	
		else 
			window.open('http://twitter.com/intent/tweet?text='+titulo+' '+urlbit,"_blank","top=200, left=500, width=400, height=400");
    });
}
</script>

<script type="text/javascript" src="assets/scripts/spin.min.js"></script>


<script>
	var spinner;
	var page = 1;
	var items = 16;
	var page_order = "fecha_colofon";
	var pages = <?=$data['main_content']['pages']?>;
	var originSource = "<?=$data['main_title']?>";

	$(document).ready(function () {
		SetupSpinner();
		setupPaginationLinks();
		SetupPageSize();
		SetupPageOrder ();
	});

	function Setups () {
		setupPaginationLinks ();
		SetupHovers();
		SetupPageSize();
		SetupPageOrder ();
	}

	function SetupSpinner () {
	var opts = {
		  lines: 9 // The number of lines to draw
		, length: 25 // The length of each line
		, width: 12 // The line thickness
		, radius: 40 // The radius of the inner circle
		, scale: 1 // Scales overall size of the spinner
		, corners: 1 // Corner roundness (0..1)
		, color: '#000' // #rgb or #rrggbb or array of colors
		, opacity: 0.25 // Opacity of the lines
		, rotate: 0 // The rotation offset
		, direction: 1 // 1: clockwise, -1: counterclockwise
		, speed: 1 // Rounds per second
		, trail: 60 // Afterglow percentage
		, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
		, zIndex: 2e9 // The z-index (defaults to 2000000000)
		, className: 'spinner' // The CSS class to assign to the spinner
		, top: '150px' // Top position relative to parent
		, left: '50%' // Left position relative to parent 
		, shadow: false // Whether to render a shadow
		, hwaccel: false // Whether to use hardware acceleration
		, position: 'absolute' // Element positioning
		}
		spinner = new Spinner(opts);
}

	function setupPaginationLinks () {
		$(".next-page-btn").click(function () {
			if ((page+1)<=pages) {
				var target = document.getElementById('content-page-wrapper');
				spinner.spin(target);
				$.ajax({
					type:"POST",
					url:'api/content/retrieve_content',
					data:{origin:originSource, page:(page+1), item_count:items, order_field:page_order,order_direction:'asc'},
					error: function(){
							console.log('error');
							spinner.stop();
					},
					success: function(response) {
						spinner.stop();
						var resp = $.parseJSON( response );
						$('#content-page-wrapper').html(resp.html);
						Setups();
						page+=1;
					}
				});
			}
		});

		$(".prev-page-btn").click(function () {
			if ((page-1)>=1) {
				var target = document.getElementById('content-page-wrapper');
				spinner.spin(target);
				$.ajax({
					type:"POST",
					url:'api/content/retrieve_content',
					data:{origin:originSource, page:(page-1), item_count:items, order_field:page_order,order_direction:'asc'},
					error: function(){
							console.log('error');
							spinner.stop();
					},
					success: function(response) {
						spinner.stop();
						var resp = $.parseJSON( response );
						$('#content-page-wrapper').html(resp.html);
						Setups();
						page-=1;
					}
				});
			}
		});


		$(".first-page-btn").click(function () {
			var target = document.getElementById('content-page-wrapper');
			spinner.spin(target);
			$.ajax({
				type:"POST",
				url:'api/content/retrieve_content',
				data:{origin:originSource, page:1, item_count:items, order_field:page_order,order_direction:'asc'},
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					spinner.stop();
					var resp = $.parseJSON( response );
					$('#content-page-wrapper').html(resp.html);
					Setups();
					page=1;
				}
			});
		});

		$(".last-page-btn").click(function () {
			var target = document.getElementById('content-page-wrapper');
			spinner.spin(target);
			$.ajax({
				type:"POST",
				url:'api/content/retrieve_content',
				data:{origin:originSource, page:pages, item_count:items, order_field:page_order,order_direction:'asc'},
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					spinner.stop();
					var resp = $.parseJSON( response );
					$('#content-page-wrapper').html(resp.html);
					Setups();
					page=pages;
				}
			});
		});



		$(".number-link").click(function () {
			var target = document.getElementById('content-page-wrapper');
			spinner.spin(target);
			var cPage = parseInt($(this).text());
			$.ajax({
				type:"POST",
				url:'api/content/retrieve_content',
				data:{origin:originSource, page:cPage, item_count:items, order_field:page_order,order_direction:'asc'},
				error: function(){
						console.log('error');
						spinner.stop();
				},
				success: function(response) {
					spinner.stop();
					var resp = $.parseJSON( response );
					$('#content-page-wrapper').html(resp.html);
					Setups();
					page = cPage;
				}
			});
		});
	}

	function SetupHovers () {
		$(".sp-keyword-home").each(function () {
			var txt = $(this).text();
			$(this).html("<a href='buscar/libro?keywords="+txt+"'>"+txt+"</a>");
		});
	
		$(".book-wrapper").hover(function() {
			$(this).children(".home-book-info-mask").removeClass("hidden");
		}, function() {
			$(this).children(".home-book-info-mask").addClass("hidden");
		});
	}

	function SetupPageSize () {
		$("#page_show").change(function() {
			items = $(this).val();
			var cPage = 1;
			var target = document.getElementById('content-page-wrapper');
			spinner.spin(target);
				$.ajax({
					type:"POST",
					url:'api/content/retrieve_content',
					data:{origin:originSource, page:cPage, item_count:items, order_field:page_order,order_direction:'asc'},
					error: function(){
							console.log('error');
							spinner.stop();
					},
					success: function(response) {
						spinner.stop();
						var resp = $.parseJSON( response );
						$('#content-page-wrapper').html(resp.html);
						Setups();
						pages = resp.pages;
					}
			});
		});
	}

	function SetupPageOrder () {
		$("#page_order").change(function() {
			page_order = $(this).val();
			var cPage = 1;
			var target = document.getElementById('content-page-wrapper');
			spinner.spin(target);
				$.ajax({
					type:"POST",
					url:'api/content/retrieve_content',
					data:{origin:originSource, page:cPage, item_count:items, order_field:page_order,order_direction:'asc'},
					error: function(){
							console.log('error');
							spinner.stop();
					},
					success: function(response) {
						spinner.stop();
						var resp = $.parseJSON( response );
						$('#content-page-wrapper').html(resp.html);
						Setups();
						pages = resp.pages;
					}
			});
		});
	}
</script>


