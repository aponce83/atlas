<!DOCTYPE html>
<html class="no-js" xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
    <?= $head ?>   
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '394615860726938',
          status     : true,
          xfbml      : true,
          cookie     : true,
          version    : 'v2.4'
        });
      };
    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/all.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
    </script>
    <?php
    LMView::inject_cssjs_files($head_css_injection, 'css');
    LMView::inject_cssjs_files($head_js_injection, 'js');
    ?>
  </head>
  <body>
    <div id="wrapper">
        <header>
          <?= $header ?>
        </header>
        <section class="main-content" style="margin-top: -5px;">    
          <?= $content?>
        </section>
        <footer>
          <?= $footer ?>     
        </footer>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script>
      function setNotify(e,n){$.notify(e,{className:n,clickToHide:!1,autoHide:!0,autoHideDelay:3e3,hideDuration:500,globalPosition:"left bottom"})}$(document).ready(function(){$(".main-menu > .submenu").hover(function(){$(".submenu-wrapper").removeClass("hidden")},function(){$(".submenu-wrapper").addClass("hidden")}),$(".main-menu > .usr-low-submenu").hover(function(){$(".submenu-wrapper").removeClass("hidden")},function(){$(".submenu-wrapper").addClass("hidden")}),$(".mobile-menu > .item.main").click(function(){$(".mobile-main-submenu").hasClass("hidden")?($(".mobile-submenu").addClass("hidden"),$(".mobile-main-submenu").removeClass("hidden")):$(".mobile-main-submenu").addClass("hidden")}),$(".mobile-menu > .item.user").click(function(){$(".mobile-user-submenu").hasClass("hidden")?($(".mobile-submenu").addClass("hidden"),$(".mobile-user-submenu").removeClass("hidden")):$(".mobile-user-submenu").addClass("hidden")}),$(".mobile-menu > .item.search").click(function(){$(".mobile-search-submenu").hasClass("hidden")?($(".mobile-submenu").addClass("hidden"),$(".mobile-search-submenu").removeClass("hidden"),$(".input-mbl-search").focus()):$(".mobile-search-submenu").addClass("hidden")}),$(window).resize(function(){$(".mobile-main-submenu").addClass("hidden"),$(".mobile-user-submenu").addClass("hidden")}),$(".usr-submenu").hover(function(){$(".usr-submenu-wrapper").removeClass("hidden")},function(){$(".usr-submenu-wrapper").addClass("hidden")})});var exito="success",error="error",warning="warn";
    </script>
    <?php
    LMView::inject_cssjs_files($footer_css_injection, 'css');
    LMView::inject_cssjs_files($footer_js_injection, 'js');
    ?>
  <?php
  // Agrega el código de seguimiento solo si nos encontramos en el servidor
  // de producción
  if (Kohana::$environment == Kohana::PRODUCTION) {
?>
<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 ga('create', 'UA-57105412-1', 'auto');
 ga('send', 'pageview');
</script>
<?php
    }
?>
    <?php
    LMView::inject_jscode($jscode_injection);
    ?>
  </body>
</html>