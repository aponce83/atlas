<?php $editoriales = $data['editoriales']['cuerpo']; ?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"/>
<h1>Editoriales</h1>

<div class="row  editorial-search">
  <div class="col-sm-12">
    <form class="form-inline">
      <input type="text" class="form-control" id="search_editorial" name="search_editorial" placeholder="Busca una editorial...">
      <button type="button" class="btn"></button>
    </form>
  </div>
</div><!-- end row -->

<div class="row  editorial-abc">
  <div class="col-sm-12">
    <ul>
      <li> <a href="editoriales">Ver todos</a> </li>
      <li> <a href="#" id="abc-1">0-9</a> </li>
      <?php 
        for ($i=65; $i <= 90 ; $i++) { 
          echo '<li> <a href="#" id="abc-'.strtolower(chr($i)).'">'.chr($i).'</a> </li>';
        }
      ?>
    </ul>
  </div>
  
</div>

<!-- Mostrar # de editoriales -->
<div class="row  editorial-order">
  <div class="col-sm-8">
    <form class="form-inline">
      <div class="form-group">
        <label for="list_show">Mostrar:</label>
      </div>
      <div class="form-group">
        <div class="select-list-wrapper">
          <select class="form-control" name="list_show" id="list_show">
            <option value="50">50 Editoriales</option>
            <?php if( count($editoriales) > 50 ): ?>
              <option value="75">75 Editoriales</option>
            <?php endif; ?>
            <?php if( count($editoriales) > 75 ): ?>
              <option value="100">100 Editoriales</option>
            <?php endif; ?>
            <?php if( count($editoriales) > 100 ): ?>
              <option value="200">200 Editoriales</option>
            <?php endif; ?>
            <!-- option value="-1">Todos</option-->
          </select>
        </div>
      </div>
    </form>
  </div>
  <div class="col-sm-4  align-r">
    <span id="info_pag">1-50 de 3,269</span>
  </div>
</div><!-- end row  list-order-->

<table class="table table-striped" id="table_editoriales">
  <thead>
    <tr>
      <!-- <th>Inicial</th> -->
      <th>Lista de editoriales</th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach ($editoriales as $key => $editorial) { 
      if( strlen($editorial['nombre']) > 2 && $editorial['nombre'] != '...' && $editorial['nombre'] != '[s n]' ):
        $name_clean = mb_strtolower( mb_substr( trim($editorial['nombre']) , 0, 1) );
        if ( $name_clean == '2' || 
          $name_clean == '3' || 
          $name_clean == '4' || 
          $name_clean == '5' || 
          $name_clean == '5' || 
          $name_clean == '7' || 
          $name_clean == '8' || 
          $name_clean == '9' || 
          $name_clean == '0' ) {
          $name_clean = '1';
        }
      ?>
      <tr data-abc="<?php echo $name_clean; ?>" class="row-edit">
        <td><a href="buscar/libro?editorial=<?php echo $editorial['nombre'] ?>"> <?= $editorial['nombre'] ?></a></td>
      </tr>
      <?php
      endif;
    } ?>
  </tbody>
  <tfoot>
  </tfoot>
</table>

<script type="text/javascript" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="http://jquery-datatables-column-filter.googlecode.com/svn/trunk/media/js/jquery.dataTables.columnFilter.js"></script>
<script type="text/javascript" src="assets/scripts/editoriales.js"></script>
