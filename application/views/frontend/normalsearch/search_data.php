<div class="search_data">
  
  <!-- Buscar Libros -->
  <div class="row  search-search">
    <div class="col-sm-12 col-xs-12">
      <h2>
        <?php //echo ( is_catalogo() ) ? 'Catálogo' : (is_novedades() ? 'Novedades' : 'Resultados de la búsqueda para:'); 
          if( is_catalogo() ){
            echo 'Catálogo';
          }else if( is_novedades() )
            echo 'Novedades';
          else if( is_precio_unico() )
            echo 'Consulta pública del precio único de venta al público de los libros';
          else if( is_conaculta() ){
            echo '';
          }else{
            echo 'Resultados de la búsqueda para:';
          }
        ?>
      </h2>

      <form id="form_main_search" class="form-inline form-book-search"  method="GET" action="<?php echo url::base()?>buscar/libro">
        <div class="form-group" style="width: 55%">
          <label class="sr-only" for="txt_book_search">Busca un libro...</label>
          <input type="text" class="form-control  txt_book_search" id="txt_book_search" name="txt_book_search" placeholder="Busca un libro..." value="<?php echo $val; ?>">
        </div>
        <div class="form-group" style="width: 30%">
          <select class="form-control" id="search_param" name="search_param">
            <option value="cadena">Todo</option>
            <option value="titulo" <?php echo ($par == 'titulo') ? 'selected' : ''; ?> >Título</option>
            <option value="autores" <?php echo ($par == 'autores') ? 'selected' : ''; ?> >Autor</option>
            <option value="editorial" <?php echo ($par == 'editorial') ? 'selected' : ''; ?> >Editorial</option>
            <option value="isbn" <?php echo ($par == 'isbn') ? 'selected' : ''; ?> >ISBN</option>
            <option value="keywords" <?php echo ($par == 'keywords') ? 'selected' : ''; ?> >Palabras Clave</option>
          </select>
        </div>
        <?php 
        if( is_conaculta() ){
          echo '<input type="hidden" name="editorial" value="conaculta">';
        }else if( is_novedades() ){
          echo '<input type="hidden" name="catalogo" value="novedades">';
        }else if( is_precio_unico() ){
          echo '<input type="hidden" name="precio_vigente" value="1">';
        }
        ?>
        <input type="hidden" name="tracking_search" value="search">
        <button type="submit" class="btn btn-search-book" id="btn_book_search" name="normal_search"></button>
        <div class="form-group hidden-xs"></div>
      </form>

    </div>
  </div>

  <!-- Ordenar Libros MD -->
  <div class="row  search-order hidden-xs">
    <?php if( true/*$response !== null*/ ): ?>
    <div class="col-sm-12 col-lg-7">
      <form class="form-inline">
        <div class="form-group" style="margin-bottom: 5px;">
          <label for="list_order">Ordenar por:</label>
          <select class="form-control" name="list_order" id="list_order">
            <option value="num">Seleccione</option>
            <!-- option value="popular">Más populares</option -->
            <option value="fecha_colofon">Más recientes</option>
            <option value="titulo">Título (A-Z)</option>
            <!-- option value="autores">Autor (A-Z)</option -->
            <option value="editorial">Editorial (A-Z)</option>
          </select>
        </div>
        <div class="form-group">
          <label for="list_show">Mostrar:</label>
          <select class="form-control" name="list_show" id="list_show">
            <option value="16">16 Libros</option>
            <?php if( true/*$count_response > 16*/ ): ?>
              <option value="32" <?php echo ($reg_x_pag == 32) ? 'selected' : ''; ?> >32 Libros</option>
            <?php endif; ?>
            <?php if( true/*$count_response > 32*/ ): ?>
              <option value="48" <?php echo ($reg_x_pag == 48) ? 'selected' : ''; ?> >48 Libros</option>
            <?php endif; ?>
            <?php if( true/*$count_response > 48*/ ): ?>
              <option value="64" <?php echo ($reg_x_pag == 64) ? 'selected' : ''; ?> >64 Libros</option>
            <?php endif; ?>
          </select>
        </div>
      </form>
    </div>
    <div class="col-sm-12 col-lg-5  aling-r">
      <span id="info_pag"><?php echo number_format($inicio) . ' - ' . number_format($fin) ?> de <?php echo number_format($count_response) . ' resultados  |  pág. ' . number_format($pagina) . ' de ' . number_format($total_paginas) ?></span>
    </div>
    <?php endif; ?>
  </div><!-- end row  list-order-->
  
  <!-- Total de resultados XS -->
  <div class="row  search-order visible-xs">
    <div class="col-xs-12">
      <span id="info_pag_xs"><?php echo number_format($count_response) . ' resultados' ?></span>
    </div>
    
  </div>

  <!-- Filtros para precio único -->
  <?php if( false ):  //is_precio_unico() ?>
  <div class="row  precio-unico">
    <?php if( false ): //$response !== null?>
    <div class="col-sm-7">
      <form class="form-inline">
        <div class="form-group" style="margin-bottom: 5px;">
          <label for="list_order">Filtrar por precio único:</label>
          <select class="form-control" name="list_precio" id="list_precio">
            <option value="1">Sí</option>
            <option value="0">No</option>
            <option value="todos">Mostrar todos</option>
          </select>
        </div>
      </form>
    </div>
    <?php endif; ?>
  </div><!-- end row  precio único-->
  <?php endif; ?>
  
  <!-- Etiquetas de Filtrado -->
  <div class="row search-filter">
    <div class="col-sm-12">
      <?php echo ( is_catalogo() || is_novedades() ) ? '<span class="txt-filter" style="display: none;">Filtrado por:</span>' : '<span class="txt-filter">Filtrado por:</span>'; ?>
      <?php 
      $p = 0;
      foreach ($params as $param => $value): 
        if( !($value == '' OR $param == 'registros_por_pagina' OR $param == 'pagina' OR $param == 'origen' OR $param == 'min' OR $param == 'attr' OR $param == 'orden' OR $param == 'orden_dir' OR  $param == 'todos' OR ( $param == 'titulo' && $value == 'e') OR is_novedades() ) ):
          
          switch ($param) {
            case 'tipo_producto':
              $name = 'type';
              break;
            case 'fecha_colofon':
              $name = 'colofon';
              break;
            case 'keywords':
              $name = 'keyword';
              break;
            case 'temas':
              $name = 'theme';
              break;
            default:
              $name = $param;
              break;
          }
        ?>
          <?php if( $param == 'editorial' ): 
            $editoriales = explode("|", $value);

            foreach ($editoriales as $edit) {
              ?>
              <span class="label-filter" <?php echo 'data-span'.$name.'="'.$edit.'" data-filter="'.$name.'" data-value="'.$edit.'"' ?> >
                <?php if( !(is_conaculta() ) ): ?>
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                <?php endif; ?>
                <?php echo ucfirst(LM::paramToSpanish($param))  . ': ' . ucwords($edit); ?>
              </span>
              <?php
            }

            ?>
              
          <?php else: ?>
            <span class="label-filter" <?php echo 'data-span'.$name.'="'.$value.'" data-filter="'.$name.'" data-value="'.$value.'"' ?> >
              <?php if( !(is_conaculta() || is_precio_unico() ) ): ?>
              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
              <?php endif; ?>
              <?php 
                if( $param == 'tipo_producto')
                  echo ucfirst(LM::paramToSpanish($param))  . ': ' . ucwords( $tipos[$value] );
                else if( $param == 'precio_vigente')
                  echo ucfirst(LM::paramToSpanish($param))  . ': ' . ucwords( $si_no[$value] );
                else
                  echo ucfirst(LM::paramToSpanish($param))  . ': ' . ucwords($value);
              ?>
            </span>
          <?php endif; ?>
        <?php
        $p++;
        endif;
      endforeach; 
      if( $p == 0) echo '<script>$(".txt-filter").hide();</script>';
      ?>
    </div>
  </div>
  
  <!-- pre><?php //echo var_dump($_SESSION['search']); ?></pre -->

  <!-- Resultados de Búsqueda -->
  <div class="row  search-book-data">
    <div class="ajax-loader  loader-book-data">
      <img src="assets/images/ajax-loader-t.gif" width="100">
    </div>
    
    <?php if( $response == null ) echo '<div class="list-msg">Cargando resultados...</div>'; //No hay resultados para el criterio de búsqueda ?>

    <?php foreach( $response['cuerpo'] as $book ): ?>
    <?php
      if($book['imagen']==NULL){
        $img = 'assets/images/generic_book.jpg';
        $book_cover = 'book_without_cover';
      }else{
        $img = $book['imagen'];
        $book_cover = 'book_with_cover';
      }
    ?>
    <div class="col-lg-3 col-sm-4 col-xs-12  search-book-data-container">
      <div class="search-book-img">
        <?php if( $book['tipo'] != '10' ): ?>
        <div class="label-tipo"><span><?php echo  $tipos[$book['tipo']]  ?></span></div>
        <?php endif; ?>
        <a href="<?php echo url::base(). 'libros/' . $book['id_libro'] ?>"><img src="<?php echo $img ?>"></a>
        
          <div class="search-book-info  hidden-xs <?php echo $book_cover ?>" onclick="var bookUrl = '<?php echo url::base().'libros/'.$book['id_libro'] ?>'; window.location = bookUrl;">
          <a href="<?php echo url::base(). 'libros/' . $book['id_libro'] ?>">
            <div class="search-book-info-mask">
              <p class="title"><?php echo $book['titulo'] ?></p>
              <p class="autor"><?php echo $book['autores'][1][0]['nombre'] ?></p>
              <?php echo ($book['autores'][1][1]['nombre'] != null ) ? '<p class="autor">...</p>' : '' ?>
              <p class="editorial"><?php echo $book['editorial'][0]['nombre'] ?></p>
              <?php echo ($book['editorial'][1]['nombre'] != null ) ? '<p class="editorial">...</p>' : '' ?>
              <div class="key-words">
                <?php
                if( count($book['keywords']) > 0 )
                {
                  if( $book['keywords'][0] != '' )
                  {
                    echo '<p>Palabras clave:</p>';
                    $k = 1;
                    foreach( $book['keywords'] as $keyw )
                    {
                      if( $k <= 6 )
                      {
                        if( $keyw != '' ){
                          ?><a href='buscar/libro?keywords=<?=$keyw?>'><span class="sp-keyword"><?php echo $keyw ?></span></a>&nbsp; <?php
                        }
                      }
                      else
                      {
                        ?><span>...</span><?php
                        break;
                      }
                      $k++;
                    }
                  }
                }
                ?>
              </div>
            </div>
          </a>
        </div>

        <div class="search-book-info-xs  visible-xs">
          <p class="title"><a href="<?php echo url::base(). 'libros/' . $book['id_libro'] ?>"><?php echo $book['titulo'] ?></a></p>
          <p class="autor"><?php echo $book['autores'][1][0]['nombre'] ?></p>
          <p class="editorial"><?php echo $book['editorial'][0]['nombre'] ?></p>
        </div>
        <div class="visible-xs" style="clear: both"></div>
      </div>
      
    </div>
    <?php endforeach; ?>

  </div>
  
  <!-- Paginación -->
  <?php if( true/*$response !== null && $total_paginas > 1*/ ): ?>
  <div class="row  search-pagination">
    <div class="col-sm-8 col-sm-offset-4">
      <?php

      $url = url::base() . 'buscar/libro/';

      $urlcatalogo = '';
      if( is_conaculta() )
        $urlcatalogo = '?editorial=conaculta';
      else if( is_novedades() )
        $urlcatalogo = '?catalogo=novedades';
      else if( is_precio_unico() )
        $urlcatalogo = '?precio_vigente=1';


      if( $pagina > 1 ){ ?>
        <a href="<?php echo $url . '1' . $urlcatalogo ?>"><div class="flechadoble-i"></div></a>
        <span>
          <a href="<?php echo $url . ($pagina-1) . $urlcatalogo ?>">
            <div class="flechasencilla-i"></div>anterior
          </a>
        </span><?php
      }

      if( $pagina > 2 ){
        ?> <span>| <a href="<?php echo $url . ($pagina-2) . $urlcatalogo ?>"><?php echo ($pagina-2) ?></a></span> <?php
      }

      if( $pagina > 1 ){
        ?> <span>| <a href="<?php echo $url . ($pagina-1) . $urlcatalogo  ?>"><?php echo ($pagina-1) ?></a></span> <?php
      }

      ?> <span class="current-page">| <?php echo ($pagina) ?></span> <?php

      if( $pagina < $total_paginas ){
        ?> <span>| <a href="<?php echo $url . ($pagina+1)  . $urlcatalogo  ?>"><?php echo ($pagina+1) ?></a></span> <?php
      }

      if( $pagina < $total_paginas-1 ){
        ?> <span>| <a href="<?php echo $url . ($pagina+2) . $urlcatalogo  ?>"><?php echo ($pagina+2) ?>  | </a></span> <?php
      }
      
      if( $pagina < $total_paginas ){ ?>
        <span>
          <a href="<?php echo $url . ($pagina+1) . $urlcatalogo ?>">
            siguiente<div class="flechasencilla-d"></div>
          </a>
        </span><?php
      }

      if( $pagina < $total_paginas ){
        ?><a href="<?php echo $url . $total_paginas . $urlcatalogo ?>"><div class="flechadoble-d"></div></a><?php
      }
      ?>
      
    </div>
  </div>
  <?php endif; ?>
  <!-- end Paginación -->
  
</div>

<!-- JQuery-UI -->
<script type="text/javascript" src="assets/scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="assets/scripts/jquery.ui.touch-punch.min.js"></script>

<!-- Chosen -->
<script type="text/javascript" src="assets/scripts/chosen_v1.4.2/chosen.jquery.min.js"></script>

<script type="text/javascript" src="assets/scripts/catalogos.js"></script>
<script type="text/javascript" src="assets/scripts/search_filters_f.js"></script>
<script type="text/javascript" src="assets/scripts/list.js"></script>

<!-- iCheck -->
<script type="text/javascript" src="assets/icheck/icheck.min.js"></script>
