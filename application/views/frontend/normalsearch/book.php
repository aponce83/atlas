<!-- JQuery-UI -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
<!-- iCheck -->
<link rel="stylesheet" type="text/css" href="assets/icheck/skins/minimal/green.css"/>

<link rel="stylesheet" type="text/css" href="assets/styles/modal.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/custom_f.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/custom_r.css"/>
<link rel="stylesheet" type="text/css" href="assets/chosen/chosen.css" />
<?php

$tipos = LM::tipo_clave_producto();
$si_no = LM::precio_unico();

$session = Session::instance();
$sess = $session->as_array();


$reg_x_pag = (isset($sess['search']['registros_por_pagina'])) ? $sess['search']['registros_por_pagina'] : '16' ;

$params = array(
  'registros_por_pagina'  => $reg_x_pag,
  'attr' => 'id_libro, titulo, tipo, tipo_producto, temas, editorial, autores, keywords, fecha_colofon, imagen, isbn',
  //'min'                   => '1'
);

if( isset($_GET['pagination']) || isset($_GET['tobook']) ){
  //Si es paginación, no borra parámetros de búsqueda
  $par = $_GET['search_param'];
  $val = $_GET['txt_book_search'];
}
elseif( isset($_GET['normal_search']) ){
  $par = $_GET['search_param'];
  $val = $_GET['txt_book_search'];

  if( is_conaculta() ){
    $params['editorial'] = 'conaculta';
  }

  if( is_precio_unico() )
    $params['precio_vigente'] = '1';

  $params[$par] = $val;

  $session->set('search', $params);

  //-----Tracking de las búsquedas----
  $tracking_search = array($par => $val);
  $query = json_encode($tracking_search, true);
  $source = $_GET['tracking_search'];
  $user = Session::instance()->get('user_front');
  $userfront_id = $user['id'];
  $cd = new CrawlerDetect;
  if(!$cd->isCrawler()){
    $user_agent = $cd->getUserAgent();
    Model::factory('search')->set_search_tracking($query,
      $source,
      $userfront_id,
      $user_agent,
      $par,
      $val);
  }
  //-----------------------------------
  sessionStorage_removeItem();
  if ( is_conaculta() ){
    $editoriales_conaculta = 'conaculta|SEP / Dirección General de Publicaciones y Bibliotecas|SEP / Dirección General de Publicaciones y Medios|Sep / Dirección General De Divulgación|Secretaría de Cultura / Dirección General de Publicaciones';
    echo '<script type="text/javascript">sessionStorage.setItem("editorial", "'.$editoriales_conaculta.'");</script>';
  }elseif(is_precio_unico()){
    echo '<script type="text/javascript">sessionStorage.setItem("precio_vigente", "1");</script>';
  }elseif(is_novedades()){
    $fecha_colofon_1 = date("Y-m-d",strtotime('-500 year'));
    $fecha_colofon_2 = date("Y-m-d");
    echo '<script type="text/javascript">sessionStorage.setItem("fecha_colofon_1", "'.$fecha_colofon_1.'");</script>';
    echo '<script type="text/javascript">sessionStorage.setItem("fecha_colofon_2", "'.$fecha_colofon_2.'");</script>';
    echo '<script type="text/javascript">sessionStorage.setItem("orden", "fecha_colofon");</script>';
    echo '<script type="text/javascript">sessionStorage.setItem("orden_dir", "desc");</script>';
  }
  echo '<script type="text/javascript">sessionStorage.setItem("search_param", "'.$par.'");</script>';
  echo '<script type="text/javascript">sessionStorage.setItem("search_val", "'.$val.'");</script>';
  echo '<script type="text/javascript">sessionStorage.setItem("'.$par.'", "'.$val.'");</script>';

}
elseif( isset($_POST['advanced_search']) ){
  sessionStorage_removeItem();

  foreach ($session->get('search') as $key => $value) {
    $params[$key] = $value;
  }

  for ( $i = 0 ; $i < count($_POST['param']) ; $i++ ) {
    $params[$_POST['param'][$i]] = $_POST['val'][$i];
  }

  foreach ($params as $key => $value) {
    echo '<script type="text/javascript">sessionStorage.setItem("'.$key.'", "'.$value.'");</script>';
  }

  $session->set('search', $params);

} elseif ( is_conaculta() ) {
  $editoriales_conaculta = 'conaculta|SEP / Dirección General de Publicaciones y Bibliotecas|SEP / Dirección General de Publicaciones y Medios|Sep / Dirección General De Divulgación|Secretaría de Cultura / Dirección General de Publicaciones';
  $params['editorial'] = $editoriales_conaculta;
  $session->set('search', $params);

  sessionStorage_removeItem();
  echo '<script type="text/javascript">sessionStorage.setItem("search_param", "editorial");</script>';
  echo '<script type="text/javascript">sessionStorage.setItem("search_val", "'.$editoriales_conaculta.'");</script>';
  echo '<script type="text/javascript">sessionStorage.setItem("editorial", "'.$editoriales_conaculta.'");</script>';
}
elseif ( is_catalogo() )
{
  $params['titulo'] = 'e';

  $session->set('search', $params);

  sessionStorage_removeItem();
  echo '<script type="text/javascript">sessionStorage.setItem("search_param", "catalogo");</script>';
  echo '<script type="text/javascript">sessionStorage.setItem("search_val", "todo");</script>';
  //echo '<script type="text/javascript">sessionStorage.setItem("tipo_producto", "10");</script>';
  echo '<script type="text/javascript">sessionStorage.setItem("titulo", "e");</script>';
}
elseif ( is_novedades() )
{
  $params['fecha_colofon_1'] = date("Y-m-d",strtotime('-500 year'));
  $params['fecha_colofon_2'] = date("Y-m-d");
  $params['orden'] = "fecha_colofon";
  $params['orden_dir'] = "desc";

  $session->set('search', $params);

  sessionStorage_removeItem();
  echo '<script type="text/javascript">sessionStorage.setItem("search_param", "catalogo");</script>';
  echo '<script type="text/javascript">sessionStorage.setItem("search_val", "novedades");</script>';
  echo '<script type="text/javascript">sessionStorage.setItem("fecha_colofon_1", "'.$params['fecha_colofon_1'].'");</script>';
  echo '<script type="text/javascript">sessionStorage.setItem("fecha_colofon_2", "'.$params['fecha_colofon_2'].'");</script>';
  echo '<script type="text/javascript">sessionStorage.setItem("orden", "fecha_colofon");</script>';
  echo '<script type="text/javascript">sessionStorage.setItem("orden_dir", "desc");</script>';
}
elseif( is_precio_unico() ){
  $params['precio_vigente'] = '1';
  //$params['titulo'] = 'e';

  $session->set('search', $params);

  sessionStorage_removeItem();
  echo '<script type="text/javascript">sessionStorage.setItem("search_param", "precio_vigente");</script>';
  echo '<script type="text/javascript">sessionStorage.setItem("search_val", "1");</script>';
  echo '<script type="text/javascript">sessionStorage.setItem("precio_vigente", "1");</script>';
}
elseif( count($_GET) > 0 )
{
  sessionStorage_removeItem();

  foreach ($_GET as $key => $value) {
    $params[$key] = $value;
    echo '<script type="text/javascript">sessionStorage.setItem("'.$key.'", "'.$value.'");</script>';
  }

  $session->set('search', $params);

}
else
{
  $params = $sess["search"];
}


$registros_por_pagina = $params['registros_por_pagina'];
$pagina = $data['pag'];
$params['pagina'] = $pagina;
echo '<script type="text/javascript">sessionStorage.setItem("pagina", "'.$pagina.'");</script>';

$params['todos'] = 1;
$params['origen'] = "L|E";

//=================== Peticion al servicio web ======================
//$response = Model::factory('List')->advanced_search( $params );
//================================================================

$count_response = $response['encabezado']['registros'];

$total_paginas = ceil( $count_response / $registros_por_pagina );

$fin = $pagina * $registros_por_pagina;
$inicio = $fin - $registros_por_pagina + 1;

if( $pagina == $total_paginas)
  $fin = $count_response;


echo '<script type="text/javascript">sessionStorage.setItem("registros_por_pagina", "'.$params['registros_por_pagina'].'");</script>';
echo '<script type="text/javascript">sessionStorage.setItem("total_registros", "'.$count_response.'");</script>';


function is_conaculta()
{
  if( isset($_GET['editorial']) AND $_GET['editorial'] == 'conaculta' )
    return true;
  else
    return false;
}

function is_catalogo()
{
  if( isset($_GET['catalogo']) AND $_GET['catalogo'] == 'todo' )
    return true;
  else
    return false;
}

function is_novedades()
{
  if( isset($_GET['catalogo']) AND $_GET['catalogo'] == 'novedades' )
    return true;
  else
    return false;
}

function is_precio_unico()
{
  if( isset($_GET['precio_vigente']) AND $_GET['precio_vigente'] == '1' )
    return true;
  else
    return false;
}

function sessionStorage_removeItem()
{
  echo '<script type="text/javascript">sessionStorage.removeItem("titulo");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("autores");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("editorial");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("isbn");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("tipo_producto");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("keywords");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("fecha_colofon_1");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("fecha_colofon_2");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("cadena");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("temas");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("coleccion");</script>';
  //-----Busqueda Avanzada
  echo '<script type="text/javascript">sessionStorage.removeItem("subtitulo");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("sinopsis");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("edicion");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("encuadernacion");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("no_volumen");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("nacional");</script>';

  echo '<script type="text/javascript">sessionStorage.removeItem("ilustraciones_1");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("ilustraciones_2");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("peso_1");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("peso_2");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("alto_1");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("alto_2");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("ancho_1");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("ancho_2");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("grosor_1");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("grosor_2");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("paginas_1");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("paginas_2");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("fecha_publicacion_1");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("fecha_publicacion_2");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("fecha_actualizacion_1");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("fecha_actualizacion_2");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("iva");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("disponibilidad");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("audiencia");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("nivel_lectura");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("nivel_academico");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("idioma_original");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("pais");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("precio_vigente");</script>';

  echo '<script type="text/javascript">sessionStorage.removeItem("reimpresion");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("curso");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("estado");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("asignatura");</script>';

  //-----paginación
  echo '<script type="text/javascript">sessionStorage.removeItem("total_registros");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("pagina");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("total_paginas");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("fin");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("inicio");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("pagina_autores");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("pagina_editoriales");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("pagina_keywords");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("pagina_coleccion");</script>';
  //-----Orden-----
  echo '<script type="text/javascript">sessionStorage.removeItem("orden");</script>';
  echo '<script type="text/javascript">sessionStorage.removeItem("orden_dir");</script>';
  //----Otros------
  echo '<script type="text/javascript">sessionStorage.removeItem("");</script>';
}

?>

<?php if( is_conaculta() ): ?>
<div class="catalogo-conaculta">
  <div class="catalogo-conaculta-title">
    <p><span>Catálogo histórico de la Secretaría de Cultura</span></p>
  </div>
  <div class="catalogo-conaculta-description">
    <p style="text-align: justify;">
      El catálogo histórico de la Secretaría de Cultura reúne las obras publicadas  desde 1921 por el Gobierno Federal, año en que se crea la Secretaría de Educación Pública, a la fecha. Recoge colecciones fundamentales
      para la memoria histórica y cultural de México así como obras clásicas de la literatura nacional
      y universal. Se lleva a cabo gracias a una ardua investigación y búsqueda de las colecciones editadas
      por la Dirección General de Publicaciones y Medios, y la Dirección General de Publicaciones y Bibliotecas,
      con apoyo de diferentes instituciones que nos proporcionan acceso a su acervo histórico.<br/><br>
      El trabajo finalizará en 2017 y reunirá la mayoría de las colecciones que fueron editadas por la Secretaría de Educación Pública, después por Conaculta y ahora por la Secretaría de Cultura.
    </p>
  </div>
</div>
<script type="text/javascript">

</script>
<style type="text/css">
  .main-content{
    background-color: #1d1d1d;
    color: #fff;
  }
  .library-content .lists-panel .filter-section .filter-wrapper .container-resizable .add-list span.item-filter{
    color: #fff;
  }
  .library-content .lists-panel .filter-section .filter-wrapper,
  .library-content .lists-panel .filter-section h2.filter-main{
    border-bottom: 1px solid #d90000;
  }
  .library-content .lists-panel .filter-section h2.filter-header,
  .library-content .lists-panel .filter-section .topics-wrapper .topics-container section .container-level p a,
  .library-content .lists-panel .filter-section .topics-wrapper .topics-container section .container-level .all-topics-lnk a,
  .library-content .lists-panel .filter-section .filter-wrapper .container-resizable a.show-options{
    color: #fff;
  }
  .search_data .form-book-search .btn-search-book{
    background-image: url("assets/images/icono_buscar_blanco.svg");
  }
  .library-content .lists-panel .filter-section button.smart-list-btn{
    border: 1px solid #fff;
    color: #fff;
    font-size: 16px;
    font-family: 'montserrat';
  }
  .library-content .lists-panel .filter-section button.smart-list-btn:hover{
    background-color: #fff;
    color: #000;
  }
  .search_data .search-pagination .current-page,
  .search_data .search-pagination a:hover{
    color: #d90000;
  }
  .library-content .lists-panel .filter-section .filter-wrapper h3.filter-title{
    background-image: url(assets/images/desplegable.png);
  }
  .modal .modal-title{
    color: #000;
  }
  .icheckbox_minimal-green, .iradio_minimal-green{
    background: url(assets/icheck/skins/minimal/red.png) no-repeat;
  }
  .icheckbox_minimal-green.checked{
    background-position: -40px 0;
  }
  .iradio_minimal-green{
    background-position: -100px 0;
  }
  @media screen and (max-width: 991px){
    .library-content .books-panel .search_data .search-book-data .search-book-data-container .search-book-img .search-book-info-xs p a{
      color: #fff;
    }
  }
</style>
<?php endif; ?>

<div class="library-content">
  <!-- Nav tabs -->
  <ul class="nav nav-tabs  visible-xs visible-sm" role="tablist">
    <li role="presentation" id="tab-results" class="active"><a href="#results" aria-controls="home" role="tab" data-toggle="tab">Resultados</a></li>
    <li role="presentation" id="tab-filters"><a href="#filters"  aria-controls="profile"  role="tab" data-toggle="tab">Filtrar por</a></li>
  </ul>
  <!-- Tab panes -->
  <div class="tab-content">
    <div class="lists-panel tab-pane col-md-4" id="filters">
      <!-- contenido del panel de filtros -->
      <?php include 'search_filters.php';?>
    </div>
    <div class="books-panel tab-pane active col-md-8" data-setmobile="0" id="results">
      <!-- contenido de los resultados -->
      <?php include 'search_data.php';?>
    </div>
  </div>

</div>