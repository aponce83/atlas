<?php
//se llenan dos arrays con los valores del data categories
$dewey = array();
$names = array();
foreach( $data['categories'] as $section )
{
	array_push( $dewey, $section['dewey'] );
	array_push( $names, $section['nombre'] );
}
$styles = ( $_GET['editorial'] == 'conaculta' ) ? 'style="color: #fff" onmouseover="this.style.color=\'#cc4242 \'" onmouseout="this.style.color=\'#fff \'"' : '';
?>
<br>
<h2 class="filter-header red-bottom">temas</h2>

<div class="topics-wrapper" data-width="0">
	<div class="topics-container" data-panel="1">

		<!-- nivel 1 -->
		<section>
			<div class="container-level level-arrow">
				<?php
				foreach ($data['categories'] as $section)
					if($section['dewey']%100==0)
					{
					?>
						<p data-target="<?php echo $section['dewey']; ?>" class="next-level">
							<a>
								<?php echo $section['nombre']; ?>
							</a>
						</p>
					<?php
					}
				?>
			</div>
		</section>
		<!-- nivel 1 -->

		<!-- nivel 2 -->
		<section>
			<?php
			$for_counter = 0;
			foreach ($data['categories'] as $section)
			{
				if($section['dewey']%100==0)
				{
				 ?>
			      	<div id="<?php echo $section['dewey']; ?>" class=" column-mode container-level level-arrow">
						<p class="level-title previous-level">
							<a><?php echo $section['nombre']; ?></a>
						</p>
						<a class="dewey-link" data-dewey="<?php echo $dewey[$for_counter]; ?>" data-name="<?php echo $names[$for_counter]; ?>" href="buscar/libro?temas=<?php echo $dewey[$for_counter]; ?>&tema-nombre=<?php echo $names[$for_counter]; ?>">
							<p class="all-topics-lnk" <?php echo $styles ?>>todos</p>
						</a>
				<?php
				}
				else
				{
					if($section['id']%10==0)
					{
					?>
						<!-- secciones del segundo nivel -->
						<p data-target="<?php echo $section['dewey']; ?>" class="next-level">
							<a><?php echo $section['nombre']; ?></a>
						</p>

					<?php
					}
					//revisar que el siguiente no sea de nivel 1
					$current_id = (int)$section['id'];

					if( (($current_id+10)%100) == 0 )
					{ ?>
						<!-- se cierra el div -->
						</div>
					<?php }
				}
				$for_counter++;
			} ?>
		</section>
		<!-- nivel 2 -->


		<!-- nivel 3 -->
		<section>
			<?php
			$for_counter = 0;
			foreach ($data['categories'] as $section)
			{

				if( ($dewey[$for_counter]%10==0) || ($dewey[$for_counter]=='000') )
				{
				?>
					<div id="<?php echo $dewey[$for_counter]; ?>" class=" column-mode container-level">
						<p class="level-title previous-level">
							<a <?php echo $styles ?>><?php echo $names[$for_counter]; ?></a>
						</p>
						<a class="dewey-link" data-dewey="<?php echo $dewey[$for_counter]; ?>" data-name="<?php echo $names[$for_counter]; ?>" href="buscar/libro?temas=<?php echo $dewey[$for_counter]; ?>&tema-nombre=<?php echo $names[$for_counter]; ?>">
							<p class="all-topics-lnk" <?php echo $styles ?>>todos</p>
						</a>
				<?php
				}
				else {
					if( $dewey[$for_counter]%10!=0 ) { ?>
						<a class="dewey-link" data-dewey="<?php echo $dewey[$for_counter]; ?>" data-name="<?php echo $names[$for_counter]; ?>" href="buscar/libro?temas=<?php echo $dewey[$for_counter]; ?>&tema-nombre=<?php echo $names[$for_counter]; ?>">
							<p><span <?php echo $styles ?>><?php echo $names[$for_counter]; ?></span></p>
						</a> <?php
					}
				}
				if( (( $dewey[$for_counter+1] )%10) == 0 ) { ?>
					</div> <?php
				}
				$for_counter++;
			}
			?>
		</section>
		<!-- nivel 3 -->


	</div>
</div>