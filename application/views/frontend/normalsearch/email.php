<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
    <table style="margin:20px auto 0 ;" width="600" align="center"  border="0" cellspacing="0" cellpadding="0">
  <tr>
  <td>  
  
    <a style="display:block; clear:both; margin:0 0 30px;" href="#"><img src="assets/images/libros_logo.png" /></a>
      <h2 style="font-weight:normal;"><font style="font-family:Arial, Helvetica, sans-serif; font-size:18px;">¡Bienvenido a LIBROSMÉXICO.MX, <strong>'.$data['fullname'].'!</strong></font></h2>
    <p><font style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">¡Gracias por registrarte! Tu cuenta ha sido Activada, ingresa a ella para comenzar a disfrutar de los beneficios que te da 
LIBROSMÉXICO.MX.</font></p>
   
  <a href="<?=URL::site();?>" target="_blank" style='color:#3db29c;text-decoration:none'><?php echo URL::site(); ?></a>      
    <p><font style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">Atentamente <br />
LIBROSMÉXICO.MX</font></p>
   <div style="margin:40px 0 0 0; border-top:solid 2px #354f5e; padding:10px 0 0;">              
     <p><font style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#606366;">Este correo electrónico se generó automáticamente. Por favor, no lo respondas.</font></p>
   </div>
   </td>
  </tr>
</table>
</body>
</html>