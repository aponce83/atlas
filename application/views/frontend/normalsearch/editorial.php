<link rel="stylesheet" type="text/css" href="assets/styles/editoriales.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/right_side.css"/>

<div class="editorial-content">

  <div class="data-panel col-sm-8 col-xs-12" data-setmobile="0" id="data">
    <!-- contenido de los resultados -->
    <?php include 'editorial_data.php';?>
  </div>
  <div class="aside-panel col-sm-4 col-xs-12" id="aside">
    <!-- contenido del panel de filtros -->
    
    <!-- Listas destacadas -->
    <div class="listas-destacadas">
      <h2>LISTAS DESTACADAS</h2>
      <hr class="blue-hr">
      <?php 
      $list_counter=0;

      if(count($data['renowned_lists']) > 0) {
        foreach ($data['renowned_lists'] as $list ) {
        ?>
        <div class='row no-margin  section-list-title section-list-title-own <?=$class?>'>
          <a href="lista/<?php echo $list->getUser(); ?>/<?=$list->slug?>"><?=$list->name?> (<?=$list->num_books?>)</a>
        </div>  
          <div class="row no-margin <?=$class?>">
            <?php $i=0; foreach ($list->books as $book ) { ?>
              <?php if ($i<5) { ?>
                <img src="<?=!$book['imagen']?'assets/images/generic_book.jpg':$book['imagen']?>" alt="<?=$book['titulo']?>" class="home-list-book list-push-right-own <?php if ($i==4) { ?> desk <?php } ?>" />
              <?php } ?>
            <?php $i++; } ?>
          </div>
          <div class="row section-see-list-own no-margin own-list <?=$class?>">
          <a href="lista/<?php echo $list->getUser(); ?>/<?=$list->slug?>">ver más <img src= 'assets/images/detail/flecha_azul.svg' class='arrow_list'></a>
          </div>
        <?php
        }       
      }
      ?>  
      <div class="row section-see-list-more no-margin see-more-list">
        <a href="<?=URL::base()?>listas"><span class='pointer'>ver más listas <img src= 'assets/images/detail/flecha_azul.svg' class='arrow_list'></span></a>
      </div>
    </div>
    <!-- //Listas destacadas -->

    <!-- Trivias -->
    <div class="sidebar-trivias">
      <?php include '../homepage/trivia_side.php';?>
    </div>
    <!-- /Trivias -->
  </div>
</div>