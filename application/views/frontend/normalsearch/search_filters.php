<?php
//array con la respuesta de la petición al servicio de búsqueda avanzada de libros méxico
/*function unique_filters($response)
{
	$unique_authors = array(); //array para guardar los autores sin repetir
	$authors_count_books = array(); //array para contar los libros de cada autor

	$unique_editorials = array(); //array para guardar las editoriales sin repetir
	$editorials_count_books = array(); //array para contar los libros de cada editorial

	$unique_keywords = array(); //array para guardar las palabras clave sin repetir
	$keywords_count_books = array(); //array para contar los libros de cada palabra clave

	$i = 1;
	foreach ($response as $book) { //recorre todos los libros del resultado
		//Autores
		foreach ($book['autores'][1] as $autor)  //recorre todos los autores de cada libro
		{
			if( !in_array( $autor['nombre'], $unique_authors) ) //si el autor no existe en el array
			{
				$unique_authors[$autor['id']] = $autor['nombre']; //se agrega
				$authors_count_books[$autor['id']] = 1;	//y se inicia su conteo de libros a uno
			}
			else //si ya existe el autor en el array
				$authors_count_books[$autor['id']] += 1; //sólo se agrega un libro más a la cuenta
		}
		
		//Editoriales
		if( isset( $book['editorial'] ) ) //Resultado de busqueda con información mínima (min=1)
		{
			if( !in_array($book['editorial'][0]['nombre'], $unique_editorials))
			{
				$unique_editorials[$book['editorial'][0]['id']] = $book['editorial'][0]['nombre'];
				$editorials_count_books[$book['editorial'][0]['id']] = 1;
			}
			else
				$editorials_count_books[$book['editorial'][0]['id']] += 1;
		}

		//Keywords
		foreach ($book['keywords'] as $keyword) //recorre todos las palabras clave de cada libro
		{
			if( !in_array( $keyword, $unique_keywords) ) //si la palabra clave no existe en el array
			{
				$unique_keywords[$keyword] = $keyword; //se agrega
				$keywords_count_books[$keyword] = 1;	//y se inicia su conteo de libros a uno
			}
			else //si ya existe la palabra clave en el array
				$keywords_count_books[$keyword] += 1; //sólo se agrega un libro más a la cuenta
			
			//$i++;
		}

	} //end foreach book

	$authors = array();
	foreach ($unique_authors as $id => $name) {
		$authors[] = array('id' => $id, 'name' => $name, 'total_books' => $authors_count_books[$id]);
	}

	$editorials = array();
	foreach ($unique_editorials as $id => $name) {
		$editorials[] = array('id' => $id, 'name' => $name, 'total_books' => $editorials_count_books[$id]);
	}

	$keywords = array();
	foreach ($unique_keywords as $id => $name) {
		$keywords[] = array('id' => $id, 'name' => $name, 'total_books' => $keywords_count_books[$id]);
	}

	$unique_filters = array(
		'authors'			=> $authors,
		'editorials'	=> $editorials,
		'keywords'		=> $keywords,
	);

	return $unique_filters;

}*/
//$filters = unique_filters($response['cuerpo']);
?>

<div class="filter-section">
	<h2 class="filter-main">Filtrar por:</h2>
	<!-- filtro Precio único-->
	<div class="filter-wrapper" data-status="closed">
		<h3 class="filter-title" id="filter-price-arrow">Precio único</h3>
		<div class="container-resizable">
			<div class="ajax-loader  loader-price">
				<img src="assets/images/ajax-loader-t.gif" width="50">
			</div>
			<form class='add-list' data-height="0" data-status="closed"  id="filter-price">
				<p>
					<input type="radio" name="precio_vigente" data-precio="1" <?php echo (is_precio_unico()) ? 'checked="checked"' : '' ?>  />
					<span class="item-filter" data-precio="1">Vigente</span>
				</p>
				<p>
					<input type="radio" name="precio_vigente" data-precio="0" />
					<span class="item-filter" data-precio="0">Sin vigencia</span>
				</p>
				<p>
					<input type="radio" name="precio_vigente" data-precio="" />
					<span class="item-filter" data-precio="todos">Todos</span>
				</p>
			</form>
			<div class="lnk-wrp">
				
			</div>
		</div>
	</div>
	<!-- filtro -->
	<!-- filtro  Título-->
	<div class="filter-wrapper" data-status="closed">
		<h3 class="filter-title">Título</h3>
		<div class="container-resizable">
			<div class="ajax-loader  loader-title">
				<img src="assets/images/ajax-loader-t.gif" width="50">
			</div>
			<div class="form-group">
				<input type="text" class="form-control search-filter" placeholder="Filtra por título..." id="title-predictive-search">
			</div>
			<div class="lnk-wrp"></div>
		</div>
	</div>
	<!-- filtro -->
	<!-- filtro  Autor-->
	<div class="filter-wrapper" data-status="closed">
		<h3 class="filter-title">Autor</h3>
		<div class="container-resizable">
			<div class="ajax-loader  loader-authors">
				<img src="assets/images/ajax-loader-t.gif" width="50">
			</div>
			<div class="form-group">
				<input type="text" class="form-control search-filter" placeholder="Filtra por autor..." id="author-predictive-search">
			</div>
			<form class='add-list' data-height="0" data-status="closed" id="filter-authors">
				<?php
				$i = 1;
				foreach ($filters['authors'] as $autor):
				?>
				<p>
					<input type="checkbox" name="autores" data-autores="<?php echo $autor['name'] ?>" />
					<span class="item-filter" data-name="<?php echo $autor['name'] ?>"><?php echo LM::text_to_ellipsis($autor['name'], 40) /*substr($autor['name'], 0, 40)*/; ?></span>
				</p>
				<?php
				$i++;
				endforeach; ?>
			</form>
			<div class="lnk-wrp">
				<a class="show-options" id="show_more_author">Ver más</a>
			</div>
		</div>
	</div>
	<!-- filtro -->
	<!-- filtro Editorial -->
	<div class="filter-wrapper" data-status="closed">
		<h3 class="filter-title">Editorial</h3>
		<div class="container-resizable">
			<div class="ajax-loader  loader-editorials">
				<img src="assets/images/ajax-loader-t.gif" width="50">
			</div>
			<div class="form-group">
				<input type="text" class="form-control search-filter" placeholder="Filtra por editorial..." id="editorial-predictive-search">
			</div>
			<form class='add-list' data-height="0" data-status="closed" id="filter-editorials">
				<?php
				$i = 1;
				foreach ($filters['editorials'] as $editorial):
				?>
				<p class="p-item">
					<input type="checkbox" name="editorial" data-editorial="<?php echo $editorial['name'] ?>" />
					<span class="item-filter" data-name="<?php echo $editorial['name'] ?>"><?php echo LM::text_to_ellipsis($editorial['name'], 80) /*substr($editorial['name'], 0, 80)*/; ?></span>
					<span class="clear"></span>
				</p>
				<?php
				$i++;
				endforeach; ?>
			</form>
			<div class="lnk-wrp">
				<a class="show-options" id="show_more_editorials">Ver más</a>
			</div>
		</div>
	</div>
	<!-- filtro -->
	<!-- filtro Fecha de colofón -->
	<div class="filter-wrapper" data-status="closed">
		<h3 class="filter-title" id="filter-colofon-arrow">Fecha de colofón</h3>
		<div class="container-resizable">
			<div class="ajax-loader  loader-colofon">
				<img src="assets/images/ajax-loader-t.gif" width="50">
			</div>
			<p>
			  <label for="amount">Año colofón:</label>
			  <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
			</p>
			<div id="slider-colofon"></div>
			<div class="lnk-wrp"></div>
		</div>
	</div>
	<!-- filtro -->
	<!-- filtro Tipo de libro -->
	<div class="filter-wrapper" data-status="closed">
		<h3 class="filter-title" id="filter-type-arrow">Tipo de libro</h3>
		<div class="container-resizable">
			<div class="ajax-loader  loader-type">
				<img src="assets/images/ajax-loader-t.gif" width="50">
			</div>
			<form class='add-list' data-height="0" data-status="closed"  id="filter-type">
				<p>
					<input type="radio" name="type" data-type="10" data-name="Libro" />
					<span class="item-filter" data-name="Libro">Libro</span>
				</p>
				<p>
					<input type="radio" name="type" data-type="11" data-name="Colección" />
					<span class="item-filter" data-name="Colección">Colección</span>
				</p>
				<p>
					<input type="radio" name="type" data-type="20" data-name="CD" />
					<span class="item-filter" data-name="CD">CD</span>
				</p>
				<p>
					<input type="radio" name="type" data-type="30" data-name="Libros electrónicos" />
					<span class="item-filter" data-name="Libros electrónicos">Libros electrónicos</span>
				</p>
				<p>
					<input type="radio" name="type" data-type="40" data-name="Cd-ROM" />
					<span class="item-filter" data-name="Cd-ROM">Cd-ROM</span>
				</p>
				<p>
					<input type="radio" name="type" data-type="50" data-name="DVD" />
					<span class="item-filter" data-name="DVD">DVD</span>
				</p>
				<p>
					<input type="radio" name="type" data-type="60" data-name="Libro + CD" />
					<span class="item-filter" data-name="Libro + CD">Libro + CD</span>
				</p>
				<p>
					<input type="radio" name="type" data-type="70" data-name="Audio Libro" />
					<span class="item-filter" data-name="Audio Libro">Audio Libro</span>
				</p>
				<p>
					<input type="radio" name="type" data-type="80" data-name="Revista" />
					<span class="item-filter" data-name="Revista">Revista</span>
				</p>
			</form>
			<div class="lnk-wrp">
				<?php if( count($filters['product_type']) > 3): ?>
				<a class="show-options-link">Ver todas las opciones</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<!-- filtro -->
	<!-- filtro Palabras clave-->
	<div class="filter-wrapper" data-status="closed">
		<h3 class="filter-title" id="filter-keywords-arrow">Palabras clave</h3>
		<div class="container-resizable">
			<div class="ajax-loader  loader-keywords">
        <img src="assets/images/ajax-loader-t.gif" width="50">
      </div>
			<div class="form-group">
				<input type="text" class="form-control search-filter" placeholder="Filtra por palabras clave..." id="keyword-predictive-search">
			</div>
			<form class='add-list' data-height="0" data-status="closed" id="filter-keywords">	
				<?php
				$i = 1;
				foreach ($filters['keywords'] as $keyword):
				?>
				<p>
					<input type="checkbox" name="keyword" data-keyword="<?php echo $keyword['name'] ?>" />
					<span class="item-filter" data-name="<?php echo $keyword['name'] ?>"><?php echo LM::text_to_ellipsis($keyword['name'], 40) /*substr($keyword['name'], 0, 40)*/; ?></span>
				</p>
				<?php
				$i++;
				endforeach; ?>
			</form>
			<div class="lnk-wrp">
				<a class="show-options" id="show_more_keywords">Ver más</a>
			</div>
		</div>
	</div>
	<!-- filtro -->
	<!-- filtro Colecciones -->
	<div class="filter-wrapper" data-status="closed">
		<h3 class="filter-title" id="filter-coleccion-arrow">Colecciones</h3>
		<div class="container-resizable">
			<div class="ajax-loader  loader-coleccion">
				<img src="assets/images/ajax-loader-t.gif" width="50">
			</div>
			<div class="form-group">
				<input type="text" class="form-control search-filter" placeholder="Filtra por colección..." id="coleccion-predictive-search">
			</div>
			<form class='add-list' data-height="0" data-status="closed" id="filter-coleccion">	
				<?php
				$i = 1;
				foreach ($filters['coleccion'] as $coleccion) {
					if ($coleccion['name'] == "") {
						continue;
					}
				?>
				<p>
					<input type="checkbox" name="coleccion" data-coleccion="<?php echo $coleccion['name'] ?>" />
					<span class="item-filter" data-name="<?php echo $coleccion['name'] ?>"><?php echo LM::text_to_ellipsis($coleccion['name'], 40) /*substr($coleccion['name'], 0, 40)*/; ?></span>
				</p>
				<?php
					$i++;
				}
				?>
			</form>
			<div class="lnk-wrp">
				<?php if( count($filters['coleccion']) > 3): ?>
				<a class="show-options" id="show_more_colecciones">Ver más</a>
			<?php endif; ?>
			</div>
		</div>
	</div>
	<!-- filtro -->

	<button class="smart-list-btn" data-toggle="modal" data-target="#advanced-search">Búsqueda avanzada</button>

	<?php 
		if( $user_front ):
			?><button class="smart-list-btn" data-toggle="modal" data-target="#create-dynamic-list">Guardar búsqueda en mis listas</button><?php
		else:
			?><button class="smart-list-btn" onclick="showModalRegister()"  href='javascript:void(0);'>Guardar búsqueda en mis listas</button><?php
		endif;
	?>

	<?php include 'temas.php';?>

</div>

<!-- Modal Crear Lista Dinámica-->
<div class="modal fade" id="create-dynamic-list" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div id='create-list-dialog' class="modal-dialog" role="document">
    <div id="create-list-content" class="modal-content">
      <div class="modal-header" id='create-list-header'>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Crear una nueva lista</h4>
      </div>
      <div class="modal-body">
				<div class="ajax-loader  loader-dynamics">
					<img src="assets/images/ajax-loader.gif">
				</div>
				<p class='text-label'>Nombre</p>
				<input type='text' id='name-list' class='modal-input-text no-margin-bottom' placeholder='Agrega un nombre a tu lista...'>
				<p class='list-error' id='name-list-error'>Escribe un nombre válido</p>
				<div class='margin-butt'></div>      		
				<p class='text-label'>Descripción</p>
				<textarea class='modal-input-textarea' id='description-list' placeholder='Agrega una breve descripción...'></textarea> 
				<p class='text-label no-margin-bottom'>Palabras clave</p>	      		     		
				<p class='text-mini-label' >Separa cada palabra con una coma.</p>
				<textarea id='keywords-list' class='modal-input-textarea no-margin-bottom' placeholder='Escribe una o más palabras clave...'></textarea>
				<div class='margin-butt'></div>  
				<p class='text-label'>¿Quién puede ver mi lista?</p>
				<select id='privacy-list' class='modal-input-select'>
					<option value='public'>Todos</option>
					<option value='private'>Sólo yo</option>
				</select>
				<div id='create-button' class='modal-buttons-create'>
					<button class='btn btn-modal-create' onclick='createDynamicList()'>Crear lista</button>
				</div>	
      </div>
    </div>
  </div>
</div>
<!-- /.modal -->

<!-- Modal NO se puede Crear Lista Dinámica sin estar logeado-->
<div class="modal fade" id="no-create-dynamic-list">
  <div class="modal-dialog">
    <div class="modal-content no-padding-sides">
      <div class="modal-header ">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">No es posible crear la lista</h4>
      </div>
      <div class="modal-body">
        <p class='text-label'>Debes registrarte o iniciar sesión para guardar tu búsqueda</p>
      </div>
      <div class="modal-footer">
        <!-- button type="button" class="btn btn-modal-create" data-dismiss="modal">Cerrar</button -->
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal Búsqueda Avanzada-->
<div class="modal fade" id="advanced-search">
  <div class="modal-dialog modal-lg">
    <div class="modal-content no-padding-sides">
      <div class="modal-header ">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Búsqueda Avanzada</h3>
      </div>
      <div class="modal-body">
				<section class="advance-search-content">
					<form method="POST" action="<?php echo url::base() ?>buscar/libro">
						<div class="input-filtro">
							<div class="col-xs-12  col-sm-4">
								<button type="button" class="filter-remove"></button>
								<select id='select-1' name="param[]" class="form-control  chosen-select" data-placeholder="Seleccionar filtro..."> <!-- onChange='refreshList()' -->
									<option value="0">Seleccionar filtro...</option>
									<option value="alto_1">Alto</option>
									<option value="ancho_1">Ancho</option>
									<option value="audiencia">Audiencia</option>
									<option value="autores">Autores</option>
									<option value="coleccion">Colección</option>
									<option value="disponibilidad">Disponibilidad</option>
									<option value="edicion">Edición</option>
									<option value="editorial">Editorial</option>
									<option value="encuadernacion">Encuadernación</option>
									<option value="fecha_actualizacion_1">Fecha de actualización</option>
									<option value="fecha_colofon_1">Fecha de colofón</option>
									<option value="fecha_publicacion_1">Fecha de publicación</option>
									<option value="grosor_1">Grosor</option>
									<option value="idioma_original">Idioma original</option>
									<option value="ilustraciones_1">Ilustraciones</option>
									<option value="iva">IVA</option>
									<option value="nacional">Nacional</option>
									<option value="nivel_academico">Nivel académico</option>
									<option value="nivel_lectura">Nivel de lectura</option>
									<option value="paginas_1">Número de páginas</option>
									<option value="no_volumen">Número de volumen</option>
									<option value="pais">País</option>
									<option value="keywords">Palabras clave</option>
									<option value="peso_1">Peso</option>
									<option value="con_portada">Portada</option>
									<option value="precio_vigente">Precio vigente</option>
									<option value="sinopsis">Sinopsis</option>
									<option value="subtitulo">Subtítulo</option>
									<option value="temas">Temas</option>
									<option value="tipo_producto">Tipo de libro</option>
									<option value="titulo">Título</option>
									<option value="cadena">Todo</option>
								</select>
							</div>
							<div class="input-type  col-xs-12 col-sm-8">
								<input type="text" name="val[]" class="form-control  input-single" placeholder="Escribe una palabra o frase...">
							</div>
							<div class="clear"></div>
						</div>

						<div id="conten"></div>

						<div class="clear"></div>

						<div class="advance-search-btns">
							<button type="button" class="big blue-button" onclick='addFilter()' >Agregar campo de búsqueda</button><br>
							<button type="button" name="advanced_search" class="small green-button">Buscar</button>
							<button type="button" class="small blue-button" id="clear_content">Limpiar</button>
						</div>
					</form>
				</section>
			</div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->


