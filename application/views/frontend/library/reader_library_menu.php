

<div class="list-section green-list-hov">
		<div class="green-list-hov owner">
			<div class="col-xs-12 book-review-title no-padding" style="border-bottom: 2px solid #3db29c;">				
				<h2 class="list-title">LECTOR</h2>
			</div>
			<div class="col-xs-12 no-padding owner-profile">
				<div class="owner-image" style="float: left; margin-bottom: 25px; margin-top: 15px;">
					<a href="lector/<?php echo base64_encode($data['owner_id']);?>/circulo">
						<?php if ($data['user']['image']!=='') { ?>
							<img src="<?php echo $data['user']['image']; ?>" alt="<?php echo $data['user']['fullname']; ?>"	class="portrait">
						<?php } else { ?>
							<img src="<?php echo 'assets/users/images/user.png' ?>" alt="<?php echo $data['user']['fullname']; ?>"	class="portrait">
						<?php }?>
					</a>
				</div>
				<div class="owner-name" style="float: left; padding-top: 40px; padding-left: 15px;">
					<a href="lector/<?php echo base64_encode($data['owner_id']);?>/circulo">
						<?php echo $data['user']['fullname']; ?>
					</a>
				</div>
			</div>
      <div class="row clear" style="margin-bottom: 10px;">
      <?php 
        echo View::factory('frontend/relationship/follow_button_snippet')->set('userfront_id', $data['owner_id']);
      ?>
      </div>
		</div>

			<h2 class="list-title green-btm-border">Libros de <?=$data['user']['fullname']?></h2>
			<div class="list-item hide-mobile">
				<a href="lista/<?php echo base64_encode($data['owner_id']);?>">Todos (<?=$data['total_books']?>)</a>
			</div>
			<div class="list-item hide-mobile">

				<a href="lista/<?=$data['owner_list_library']['userfront_id']?>/mi-biblioteca">Mi biblioteca (<?php echo $data['owner_list_library']['size']; ?>)</a>
			</div>
			<div class="list-item hide-mobile">
				<a href="lista/<?=$data['owner_list_reading']['userfront_id']?>/lo-estoy-leyendo">Lo estoy leyendo (<?php echo $data['owner_list_reading']['size']; ?>)</a>
			</div>
			<div class="list-item hide-mobile">
				<a href="lista/<?=$data['owner_list_read']['userfront_id']?>/ya-lo-lei">Ya lo leí (<?php echo $data['owner_list_read']['size']; ?>)</a>
			</div>
      <?php 
      if ($data['owner_list_to_be_read']) {
      ?>
      <div class="list-item hide-mobile">
        <a href="lista/<?php echo $data['owner_list_to_be_read']['userfront_id']?>/lo-quiero-leer">Lo quiero leer (<?php echo $data['owner_list_to_be_read']['size']; ?>)</a>
      </div>
      <?php 
      }
      ?>
		</div>
		
		<div class="list-section orange-list-hov" data-status="closed">
			<h2 class="list-title orange-btm-border">Listas de <?=$data['user']['fullname']?>
			</h2>
			<div class="form-group no-margin-bottom">
				<input type="text" class="form-control search-list" placeholder="Busca una lista...">
			</div>
			<div class="lists-wrp me-list-wrp" data-height="0">
			<?php 
			$counter_c = 0;
			foreach ($data['owner_lists_classic'] as $myList) {
				  	if($counter_c<2){?>
						<div class="list-item hide-mobile <?=$myList['list_id']?> me-list" data-listid="<?=$myList['list_id']?>" data-listname='<?=$myList['name']?>'>
							<a href="lista/<?=$myList['userfront_id']?>/<?=$myList['slug']?>"><?=$myList['name']?> <span>(<?=$myList['size']?>)</span> </a>
						</div>
			<?php }
					else{?>
						<div class="list-item list-hidden hide-mobile <?=$myList['list_id']?>  me-list" data-listid="<?=$myList['list_id']?>"  data-listname='<?=$myList['name']?>'>
							<a href="lista/<?=$myList['userfront_id']?>/<?=$myList['slug']?>"><?=$myList['name']?> <span>(<?=$myList['size']?>)</span> </a>
						</div>			
			<?php 	}
					$counter_c++;
				}?>
			</div>
			
			<?php if (count($data['owner_lists_classic'])>2) {?>
			<div class="col-xs-12 all-lists-link me-list-more">
				<a class="all-list-orange" data-text="Ver todas mis listas">
					Ver todas mis listas
				</a>
			</div>
			<?php }?>
		</div>

		<div class="list-section dark-blue-list-hov" data-status="closed">
			<h2 class="list-title dark-blue-btm-border">Listas inteligentes de <?=$data['user']['fullname']?></h2>
			<div class="form-group no-margin-bottom">
				<input type="text" class="form-control search-list" placeholder="Busca una lista...">
			</div>
			<div class="lists-wrp genius-wrp" data-height="0">
			<?php $counter_d = 0; 
			foreach ($data['owner_lists_dynamic'] as $genius) {
						$new_books = Model::factory('List')->how_many_new_books_are_there($genius['list_id']);
						if($counter_d<2){?>
						<div class="list-item hide-mobile <?=$genius['list_id']?> genius"  data-listname='<?=$genius['name']?>'>
							<a href="lista/<?=$genius['userfront_id']?>/<?=$genius['slug']?>"><?=$genius['name']?> (<?=$genius['size']?>)</a>
							<?php echo ( count($new_books) >= 1 ) ? '<p class="new-books-lbl">Hay '.count($new_books).' nuevo(s) libro(s) en esta lista</p>' : ''; ?>
						</div>
			<?php }
					else{?>
						<div class="list-item list-hidden hide-mobile <?=$genius['list_id']?> genius"  data-listname='<?=$genius->name?>'>
							<a href="lista/<?=$genius['userfront_id']?>/<?=$genius['slug']?>"><?=$genius['name']?> (<?=$genius['size']?>)</a>
							<?php echo ( count($new_books) >= 1 ) ? '<p class="new-books-lbl">Hay '.count($new_books).' nuevo(s) libro(s) en esta lista</p>' : ''; ?>
						</div>			
			<?php 	}
					$counter_d++;
				}?>
			</div>
			
			<!-- renderear ésta sección solo si al más de dos listas  -->
			<?php if(count($data['owner_lists_dynamic'])>2){?>
			<div class="col-xs-12 all-lists-link genius-more">
				<a class="all-list-dark-blue" data-text="Ver todas mis listas">
					Ver todas mis listas
				</a>
			</div>
			<?php }?>


		</div>

<!-- Modal Create List-->
<div class="modal fade" id="create-list" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div id='create-list-dialog' class="modal-dialog" role="document">
    <div id="create-list-content" class="modal-content">
      <div class="modal-header" id='create-list-header'>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Crear una nueva lista</h4>
      </div>
      <div class="modal-body">
      		<p class='text-label'>Nombre</p>
      		<input type='text' id='name-list' class='modal-input-text no-margin-bottom' placeholder='Agrega un nombre a tu lista...'>
      		<p class='list-error' id='name-list-error'>Escribe un nombre válido</p>
      		<div class='margin-butt'></div>      		
      		<p class='text-label'>Descripción</p>
      		<textarea class='modal-input-textarea' id='description-list' placeholder='Agrega una breve descripción...'></textarea> 
      		<p class='text-label no-margin-bottom'>Palabras clave</p>	      		     		
      		<p class='text-mini-label' >Separa cada palabra con una coma.</p>
      		<textarea id='keywords-list' class='modal-input-textarea no-margin-bottom' placeholder='Escribe una o más palabras clave...'></textarea>
      		<div class='margin-butt'></div>  
      		<p class='text-label'>¿Quién puede ver mi lista?</p>
      		<select id='privacy-list' class='modal-input-select'>
      			<option value='public'>Todos</option>
      			<option value='private'>Sólo yo</option>
      		</select>
      	<div id='create-button' class='modal-buttons-create'>
      		<button class='btn btn-modal-create' onclick='createList()'>Crear Lista</button>
      	</div>	
      </div>
    </div>
  </div>
</div>

<style>
a{
	text-decoration: none;
}
a:hover{
	text-decoration: none;
}
</style>

<script src="assets/scripts/library_f.js"></script>
<script src="assets/scripts/list.js"></script>
<link rel="stylesheet" type="text/css" href="assets/styles/modal.css"/>