<div class='list-info-container my-library no-padding col-lg-12'>
	<div class='section-list-title col-lg-12'>
		<a href=<?php echo "'".$data['list']->id."'";?>>Mi Biblioteca (<?php echo $data['list']->num_books;?>)</a>
	</div>	

	<div class='book-container-list col-lg-12'>
     	<?php 
     	foreach($data['list']->books as $item)      
     		echo "<img src='".$item['imagen']."' class='mini-list-book'>";
     	?>		
	</div>	

	<div class='section-see-list classic-list col-lg-12'>
		<a href=<?php echo "'".$data['list']->id."'";?>>ver lista <img src= 'assets/images/detail/flecha_left_verde.svg' class='arrow_list'></a>
	</div>	
</div>	

<div class='list-info-container my-library no-padding col-lg-12'>
	<div class='section-list-title col-lg-12'>
		<a href=<?php echo "'".$data['list']->id."'";?>>Lo que estoy leyendo (<?php echo $data['list']->num_books;?>)</a>
	</div>	

	<div class='book-container-list col-lg-12'>
     	<?php 
     	foreach($data['list']->books as $item)      
     		echo "<img src='".$item['imagen']."' class='mini-list-book'>";
     	?>		
	</div>	

	<div class='section-see-list classic-list col-lg-12'>
		<a href=<?php echo "'".$data['list']->id."'";?>>ver lista <img src= 'assets/images/detail/flecha_left_verde.svg' class='arrow_list'></a>
	</div>	
</div>			

<div class='list-info-container no-padding col-lg-12'>
	<div class='section-list-title col-lg-12'>
		<a>Ya lo leí (10)</a>
	</div>	

	<div class='book-container-list col-lg-12'>
		<img src='assets/images/destacado_2.png' class='mini-list-book'>
		<img src='assets/images/destacado_1.png' class='mini-list-book'>
		<img src='assets/images/destacado_4.png' class='mini-list-book'>		
		<img src='assets/images/destacado_3.png' class='mini-list-book'>
		<img src='assets/images/destacado_5.png' class='mini-list-book'>
		<img src='assets/images/destacado_6.png' class='mini-list-book'>
		<img src='assets/images/destacado_8.png' class='mini-list-book no-margin-right'>
	</div>	

	<div class='section-see-list classic-list col-lg-12'>
		<a>ver lista <img src= 'assets/images/detail/flecha_left_verde.svg' class='arrow_list'></a>
	</div>	
</div>	

<div class='list-info-container no-padding col-lg-12'>
	<div class='section-list-title col-lg-12'>
		<a>Top 10 de terror (50)</a>
		<img src= 'assets/images/detail/cerrar.svg' class='delete-list'>
	</div>	

	<div class='book-container-list col-lg-12'>
		<img src='assets/images/destacado_2.png' class='mini-list-book'>
		<img src='assets/images/destacado_1.png' class='mini-list-book'>
		<img src='assets/images/destacado_4.png' class='mini-list-book'>		
		<img src='assets/images/destacado_3.png' class='mini-list-book'>
		<img src='assets/images/destacado_5.png' class='mini-list-book'>
		<img src='assets/images/destacado_6.png' class='mini-list-book'>
		<img src='assets/images/destacado_8.png' class='mini-list-book no-margin-right'>
	</div>	

	<div class='section-see-list dynamic-list col-lg-12'>
		<a>ver lista <img src= 'assets/images/detail/flecha_naranja.svg' class='arrow_list'></a>
	</div>	
</div>	

<div class='list-info-container no-padding col-lg-12'>
	<div class='section-list-title col-lg-12'>
		<a>Top 10 de Ciencia Ficción (30)</a>
		<img src= 'assets/images/detail/cerrar.svg' class='delete-list'>
	</div>	

	<div class='book-container-list col-lg-12'>
		<img src='assets/images/destacado_2.png' class='mini-list-book'>
		<img src='assets/images/destacado_1.png' class='mini-list-book'>
		<img src='assets/images/destacado_4.png' class='mini-list-book'>		
		<img src='assets/images/destacado_3.png' class='mini-list-book'>
		<img src='assets/images/destacado_5.png' class='mini-list-book'>
		<img src='assets/images/destacado_6.png' class='mini-list-book'>
		<img src='assets/images/destacado_8.png' class='mini-list-book no-margin-right'>
	</div>	

	<div class='section-see-list dynamic-list col-lg-12'>
		<a>ver lista <img src= 'assets/images/detail/flecha_naranja.svg' class='arrow_list'></a>
	</div>	
</div>	

<div class='list-info-container no-padding col-lg-12'>
	<div class='section-list-title col-lg-12'>
		<a>De Bolsillo (8)</a>
		<img src= 'assets/images/detail/cerrar.svg' class='delete-list'>

	</div>	

	<div class='book-container-list col-lg-12'>
		<img src='assets/images/destacado_2.png' class='mini-list-book'>
		<img src='assets/images/destacado_1.png' class='mini-list-book'>
		<img src='assets/images/destacado_4.png' class='mini-list-book'>		
		<img src='assets/images/destacado_3.png' class='mini-list-book'>
		<img src='assets/images/destacado_5.png' class='mini-list-book'>
		<img src='assets/images/destacado_6.png' class='mini-list-book'>
		<img src='assets/images/destacado_8.png' class='mini-list-book no-margin-right'>
	</div>	

	<div class='section-see-list genius-list col-lg-12'>
		<a>ver lista <img src= 'assets/images/detail/flecha_azul.svg' class='arrow_list'></a>
	</div>	
</div>	