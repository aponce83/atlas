<script src="assets/scripts/library_f.js"></script>
<script src="assets/scripts/list.js"></script>
<link rel="stylesheet" type="text/css" href="assets/styles/modal.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/library_menu.css"/>

<div class="list-section green-list-hov">
			<h2 class="list-title green-btm-border">mis libros</h2>
			<div class="list-item hide-mobile">
				<a href="lista/<?php echo base64_encode($data['owner_id']);?>">Todos (<?=$data['total_books']?>)</a>
			</div>
			<div class="list-item hide-mobile">

				<a href="lista/<?=$data['owner_list_library']['userfront_id']?>/mi-biblioteca">Mi biblioteca (<?php echo $data['owner_list_library']['size']; ?>)</a>
			</div>
			<div class="list-item hide-mobile">
				<a href="lista/<?=$data['owner_list_reading']['userfront_id']?>/lo-estoy-leyendo">Lo estoy leyendo (<?php echo $data['owner_list_reading']['size']; ?>)</a>
			</div>
			<div class="list-item hide-mobile">
				<a href="lista/<?=$data['owner_list_read']['userfront_id']?>/ya-lo-lei">Ya lo leí (<?php echo $data['owner_list_read']['size']; ?>)</a>
			</div>
			<?php 
			if ($data['owner_list_to_be_read']) {
			?>
			<div class="list-item hide-mobile">
				<a href="lista/<?php echo $data['owner_list_to_be_read']['userfront_id']?>/lo-quiero-leer">Lo quiero leer (<?php echo $data['owner_list_to_be_read']['size']; ?>)</a>
			</div>
			<?php 
			}
			?>
		</div>
		
		<div class="list-section orange-list-hov" data-status="closed">
			<h2 class="list-title orange-btm-border">mis listas 
				<button data-toggle="modal" data-target="#create-list">Crear lista</button>
			</h2>
			<div class="form-group no-margin-bottom">
				<?php if ($data['owner_lists_classic']) { ?>
				<input type="text" class="form-control search-list" placeholder="Busca una lista...">
				<?php } ?>
			</div>
			<div class="lists-wrp me-list-wrp" data-height="0">
			<?php 
			$counter_c = 0;
			foreach ($data['owner_lists_classic'] as $myList) {
				  	if($counter_c<2){?>
						<div class="list-item hide-mobile <?=$myList['list_id']?> me-list" data-listid="<?=$myList['list_id']?>" data-listname='<?=$myList['name']?>'>
							<a href="lista/<?=$myList['userfront_id']?>/<?=$myList['slug']?>"><?=$myList['name']?> <span>(<?=$myList['size']?>)</span> </a>
						</div>
			<?php }
					else{?>
						<div class="list-item list-hidden hide-mobile <?=$myList['list_id']?>  me-list" data-listid="<?=$myList['list_id']?>"  data-listname='<?=$myList['name']?>'>
							<a href="lista/<?=$myList['userfront_id']?>/<?=$myList['slug']?>"><?=$myList['name']?> <span>(<?=$myList['size']?>)</span> </a>
						</div>			
			<?php 	}
					$counter_c++;
				}?>
				<?php if (!$data['owner_lists_classic']) { ?>
				<p class="menu-empty-list-message">No tienes listas creadas, puedes crear tu primera lista <a href="javascript:void(0);" data-toggle="modal" data-target="#create-list">aquí</a></p>
				<?php } ?>
			</div>
			
			<?php if (count($data['owner_lists_classic'])>2) {?>
			<div class="col-xs-12 all-lists-link me-list-more">
				<a class="all-list-orange" data-text="Ver todas mis listas">
					Ver todas mis listas
				</a>
			</div>
			<?php }?>
		</div>

		<div class="list-section dark-blue-list-hov" data-status="closed">
			<h2 class="list-title dark-blue-btm-border">mis búsquedas</h2>
			<div class="form-group no-margin-bottom">
				<?php if ($data['owner_lists_dynamic']) { ?>
				<input type="text" class="form-control search-list" placeholder="Busca una lista...">
				<?php } ?>
			</div>
			<div class="lists-wrp genius-wrp" data-height="0">
			<?php $counter_d = 0; 
			foreach ($data['owner_lists_dynamic'] as $genius) {
						$new_books = Model::factory('List')->how_many_new_books_are_there($genius['list_id']);
						if($counter_d<2){?>
						<div class="list-item hide-mobile <?=$genius['list_id']?> genius"  data-listname='<?=$genius['name']?>'>
							<a href="lista/<?=$genius['userfront_id']?>/<?=$genius['slug']?>"><?=$genius['name']?> (<?=$genius['size']?>)</a>
							<?php echo ( count($new_books) >= 1 ) ? '<p class="new-books-lbl">Hay '.count($new_books).' nuevo(s) libro(s) en esta lista</p>' : ''; ?>
						</div>
			<?php }
					else{?>
						<div class="list-item list-hidden hide-mobile <?=$genius['list_id']?> genius"  data-listname='<?=$genius->name?>'>
							<a href="lista/<?=$genius['userfront_id']?>/<?=$genius['slug']?>"><?=$genius['name']?> (<?=$genius['size']?>)</a>
							<?php echo ( count($new_books) >= 1 ) ? '<p class="new-books-lbl">Hay '.count($new_books).' nuevo(s) libro(s) en esta lista</p>' : ''; ?>
						</div>			
			<?php 	}
					$counter_d++;
				}?>
				<?php if (!$data['owner_lists_dynamic']) { ?>
				<p class="menu-empty-list-message">No tienes búsquedas guardadas.</p>
				<?php } ?>
			</div>
			
			<!-- renderear ésta sección solo si al más de dos listas  -->
			<?php if(count($data['owner_lists_dynamic'])>2){?>
			<div class="col-xs-12 all-lists-link genius-more">
				<a class="all-list-dark-blue" data-text="Ver todas mis listas">
					Ver todas mis listas
				</a>
			</div>
			<?php }?>


		</div>

		<?php
		// Obtener los libros leids
		$count = count($data["readList"]->books);
		if ($count > 0) {
		?>
		<div class="list-section" >
			<h2 class="list-title red-btm-border">recomendaciones</h2>
			<?php
			// Limit readed books by 4
			$count = $count > 4 ? 4 : $count;
			for ($c = 0; $c < $count; $c++) {
				// Get random book
				$randomB = $data["readList"]->books[array_rand($data["readList"]->books)];
				// Get related books
				if (count($randomB['relacionados'])) {
					$relacionado = $randomB['relacionados'][array_rand($randomB['relacionados'])];
			?>
			<div class="list-item list-recomm">

				<h4>Porque leíste <a href="<?php echo Url::base(); ?><?php echo 'libros/'.$randomB['id']?>"><?php echo $randomB['titulo'] ?></a></h4>
				<div class="col-xs-12 recomm-container no-padding-sides">
					<div class="img-panel">
						<a href="<?php echo Url::base(); ?><?php echo 'libros/'.$relacionado['id']; ?>">
							<img src="<?php echo !$relacionado['imagen']?'assets/images/generic_book.jpg':$relacionado['imagen']?>" alt="<?php echo $relacionado['titulo']?>">
						</a>
					</div>
					<div class="info-panel inf-pan-fix-wdth">
						<span class="book-title">
							<a href="<?php echo Url::base(); ?><?php echo 'libros/'.$relacionado['id']; ?>">
								<?php echo $relacionado['titulo']; ?>
							</a></span>
						<br><br>
						<?php $a=0; foreach ($relacionado['autores'] as $autor) { 
							if ($a<1) { ?>
							<span class="book-author"><?=$autor[0]['nombre'] ?></span>
						<?php } $a++; } ?>
						<div class="star-rate fix-bottom">
								<?php for ($k=0; $k<5; $k++) { 
									if ($k<$relacionado['rating']) { ?>
										<img src="assets/images/detail/estrella_amarilla_sm.svg" class="act-extra-book-star <?=($k>0)? 'act-extra-right':''?>">
									<?php } else { ?>	
										<img src="assets/images/detail/estrella_gris_sm.svg" class="act-extra-book-star <?=($k>0)? 'act-extra-right':''?>">
									<?php } ?>
								<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<?php
				}
			}
			?>
		</div>
		<?php
		}
		?>

<!-- Modal Create List-->
<div class="modal fade" id="create-list" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div id='create-list-dialog' class="modal-dialog" role="document">
    <div id="create-list-content" class="modal-content">
      <div class="modal-header" id='create-list-header'>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Crear una nueva lista</h4>
      </div>
      <div class="modal-body">
      		<p class='text-label'>Nombre</p>
      		<input type='text' id='name-list-n' class='modal-input-text no-margin-bottom' placeholder='Agrega un nombre a tu lista...'>
      		<p class='text-mini-label' >El nombre debe de ser de mínimo 3 caracteres.</p> 
      		<p class='list-error' id='name-list-error'>Escribe un nombre válido</p>

      		<div class='margin-butt'></div>      		
      		<p class='text-label'>Descripción</p>
      		<textarea class='modal-input-textarea' id='description-list-n' placeholder='Agrega una breve descripción...'></textarea> 
      		<p class='text-label no-margin-bottom'>Palabras clave</p>	      		     		
      		<p class='text-mini-label' >Separa cada palabra con una coma.</p>
      		<textarea id='keywords-list-n' class='modal-input-textarea no-margin-bottom' placeholder='Escribe una o más palabras clave...'></textarea>
      		<p class='text-mini-label' >Las palabras clave deben tener 5 o más caracteres.</p>      		   		
      		<div class='margin-butt'></div>  
      		<p class='text-label'>¿Quién puede ver mi lista?</p>
      		<select id='privacy-list-n' class='modal-input-select'>
      			<option value='public'>Todos</option>
      			<option value='private'>Sólo yo</option>
      		</select>
      	<div id='create-button' class='modal-buttons-create'>
      		<button class='btn btn-modal-create' onclick='createList()'>Crear lista</button>
      	</div>	
      </div>
    </div>
  </div>
</div>