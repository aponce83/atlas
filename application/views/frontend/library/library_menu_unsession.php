 <?php
 if (!$user_front) { ?>
	<div class="home-signup">
		<h1>¿Aún no eres parte de LIBROSMÉXICO.MX?</h1>
		<a href="registro-usuario" id="registro" class="btn btn-big signup-btn" style='width:277px;'>
			Regístrate
		</a>
	</div>
<?php } ?>
<?php
 $twquote= "'http://librosmexico.mx/','".utf8_decode($data['random_quote']['quote'])."'";
 if ($data['owner']) { 
 ?>
<div class="green-list-hov owner">
	<div class="col-xs-12 book-review-title no-padding-left">				
		<h2 class="list-title">LECTOR</h2>
	</div>
	<div class="col-xs-12 no-padding-left owner-profile">
		<div class="owner-image">
			<a href="<?php echo $data['owner']['url']; ?>">
				<img 
					src="<?php echo $data['owner']['image']; ?>" alt="<?php echo ($data['owner']['fullname']); ?>"
					class="portrait">
			</a>
		</div>
		<div class="owner-name">
			<div>
				<a href="<?php echo $data['owner']['url']; ?>">
					<?php echo ($data['owner']['fullname']); ?>
				</a>
			</div>
		</div>
	</div>
	
	<div class="clear"></div>
	<div class="col-xs-12 no-padding-left">
		<?php 
		echo View::factory('frontend/relationship/follow_button_snippet')->set('userfront_id', $data['owner']['id']);
		?>
	</div>
</div>
<?php } ?>
 <?php if($data['book']['libros_relacionados']){?>	
			<div class="col-xs-12 book-sections desk no-padding">
				<div class="col-xs-12 book-related-title no-padding-left desk">
					<h2>TE PUEDE INTERESAR</h2>
				</div>

				<div class="col-xs-12 book-related-content no-padding-left desk">
					<div class="col-lg-6 col-xs-12 no-padding-left"> 					
						<div class="col-xs-2 no-padding-left">
							<a href='libros/<?=$data['book']['libros_relacionados'][0]['id']?>'><img  src="<?=$data['book']['libros_relacionados'][0]['image'] ? 'assets/images/generic_book.jpg' : $data['book']['libros_relacionados'][0]['imagen']?>" class='coverbook-related'/></a>									
						</div>
						<div class="col-xs-9">
							<?php 
							echo "<a href='libros/".$data['book']['libros_relacionados'][0]['id']."'class='pointer title-rel'>".$data['book']['libros_relacionados'][0]['titulo']."</a>";	                              		    
							$autores = NULL;
							$index = 0;
							$author_count = 0;
							foreach ($data['book']['libros_relacionados'][0]['autores'] as $author) {							
								foreach ($author as $autor){
									if($author_count<3){						
											if($autores!=NULL)
											 $autores = $autores.', '.$autor['nombre'];
											else
											 $autores = $autor['nombre'];
											$author_count++;
									}
									else
										break;
								}					
							}?>	
							<p class='author-rate author-rel'><?=$autores;?></p>  
							<div class="star-rate">
							  <form>
							  	<?php for($i=5; $i>0;$i--){ 
							  			if($i<=$data['book']['libros_relacionados'][0]['ranking']){?>
							   			 	<input class="star star-<?=$i?>" id="star-<?=$i?>" type="radio" name="star">
							    		 	<label class="star star-<?=$i?> check" for="star-<?=$i?>"></label>
							    		 <?php }
							    		 else{?>
							   			 	<input class="star star-<?=$i?>" id="star-<?=$i?>" type="radio" name="star">
							    		 	<label class="star star-<?=$i?>" for="star-<?=$i?>" onclick='setRank(<?=$i?>)'></label>							    		 
								    <?php }
									}?>
							  </form>
							</div> 					
						</div>				
					</div>
					<div class="col-lg-6 col-xs-12 no-padding-left"> 					
						<div class="col-xs-2 no-padding-left">
							<a href='libros/<?=$data['book']['libros_relacionados'][1]['id']?>'><img  src="<?=$data['book']['libros_relacionados'][1]['image'] ? 'assets/images/generic_book.jpg' : $data['book']['libros_relacionados'][1]['imagen']?>" class='coverbook-related'/></a>									
						</div>
						<div class="col-xs-9">
							<?php 
							echo "<a href='libros/".$data['book']['libros_relacionados'][1]['id']."'class='pointer title-rel'>".$data['book']['libros_relacionados'][1]['titulo']."</a>";	                              		    
							$autores = NULL;
							$index = 0;
							$author_count = 0;
							foreach ($data['book']['libros_relacionados'][1]['autores'] as $author) {							
								foreach ($author as $autor){
									if($author_count<3){						
											if($autores!=NULL)
											 $autores = $autores.', '.$autor['nombre'];
											else
											 $autores = $autor['nombre'];
											$author_count++;
									}
									else
										break;
								}					
							}?>	
							<p class='author-rate author-rel'><?=$autores;?></p>  
							<div class="star-rate">
							  <form>
							  	<?php for($i=5; $i>0;$i--){ 
							  			if($i<=$data['book']['libros_relacionados'][1]['ranking']){?>
							   			 	<input class="star star-<?=$i?>" id="star-<?=$i?>" type="radio" name="star">
							    		 	<label class="star star-<?=$i?> check" for="star-<?=$i?>"></label>
							    		 <?php }
							    		 else{?>
							   			 	<input class="star star-<?=$i?>" id="star-<?=$i?>" type="radio" name="star">
							    		 	<label class="star star-<?=$i?>" for="star-<?=$i?>" onclick='setRank(<?=$i?>)'></label>							    		 
								    <?php }
									}?>
							  </form>
							</div> 					
						</div>				
					</div>	
					<div class="col-lg-6 col-xs-12 no-padding-left"> 					
						<div class="col-xs-2 no-padding-left">
							<a href='libros/<?=$data['book']['libros_relacionados'][2]['id']?>'><img  src="<?=$data['book']['libros_relacionados'][2]['image'] ? 'assets/images/generic_book.jpg' : $data['book']['libros_relacionados'][2]['imagen']?>" class='coverbook-related'/></a>									
						</div>
						<div class="col-xs-9">
							<?php 
							echo "<a href='libros/".$data['book']['libros_relacionados'][2]['id']."'class='pointer title-rel'>".$data['book']['libros_relacionados'][2]['titulo']."</a>";	                              		    
							$autores = NULL;
							$index = 0;
							$author_count = 0;
							foreach ($data['book']['libros_relacionados'][2]['autores'] as $author) {							
								foreach ($author as $autor){
									if($author_count<3){						
											if($autores!=NULL)
											 $autores = $autores.', '.$autor['nombre'];
											else
											 $autores = $autor['nombre'];
											$author_count++;
									}
									else
										break;
								}					
							}?>	
							<p class='author-rate author-rel'><?=$autores;?></p>  
							<div class="star-rate">
							  <form>
							  	<?php for($i=5; $i>0;$i--){ 
							  			if($i<=$data['book']['libros_relacionados'][2]['ranking']){?>
							   			 	<input class="star star-<?=$i?>" id="star-<?=$i?>" type="radio" name="star">
							    		 	<label class="star star-<?=$i?> check" for="star-<?=$i?>"></label>
							    		 <?php }
							    		 else{?>
							   			 	<input class="star star-<?=$i?>" id="star-<?=$i?>" type="radio" name="star">
							    		 	<label class="star star-<?=$i?>" for="star-<?=$i?>" onclick='setRank(<?=$i?>)'></label>							    		 
								    <?php }
									}?>
							  </form>
							</div> 					
						</div>				
					</div>			
					<div class="col-lg-6 col-xs-12 no-padding-left"> 					
						<div class="col-xs-2 no-padding-left">
							<a href='libros/<?=$data['book']['libros_relacionados'][3]['id']?>'><img  src="<?=$data['book']['libros_relacionados'][3]['image'] ? 'assets/images/generic_book.jpg' : $data['book']['libros_relacionados'][3]['imagen']?>" class='coverbook-related'/></a>									
						</div>
						<div class="col-xs-9">
							<?php 
							echo "<a href='libros/".$data['book']['libros_relacionados'][3]['id']."'class='pointer title-rel'>".$data['book']['libros_relacionados'][3]['titulo']."</a>";	                              		    
							$autores = NULL;
							$index = 0;
							$author_count = 0;
							foreach ($data['book']['libros_relacionados'][3]['autores'] as $author) {							
								foreach ($author as $autor){
									if($author_count<3){						
											if($autores!=NULL)
											 $autores = $autores.', '.$autor['nombre'];
											else
											 $autores = $autor['nombre'];
											$author_count++;
									}
									else
										break;
								}					
							}?>	
							<p class='author-rate author-rel'><?=$autores;?></p>  
							<div class="star-rate">
							  <form>
							  	<?php for($i=5; $i>0;$i--){ 
							  			if($i<=$data['book']['libros_relacionados'][3]['ranking']){?>
							   			 	<input class="star star-<?=$i?>" id="star-<?=$i?>" type="radio" name="star">
							    		 	<label class="star star-<?=$i?> check" for="star-<?=$i?>"></label>
							    		 <?php }
							    		 else{?>
							   			 	<input class="star star-<?=$i?>" id="star-<?=$i?>" type="radio" name="star">
							    		 	<label class="star star-<?=$i?>" for="star-<?=$i?>" onclick='setRank(<?=$i?>)'></label>							    		 
								    <?php }
									}?>
							  </form>
							</div> 					
						</div>				
					</div>
				</div>			
			</div>
		<?php }?>	
		 <!--book-related-end-->
 <div class='col-xs-12 book-sections related-phrase desk no-padding'>
 	<div class="col-xs-12 book-related-phrase no-padding-left desk">				
			<h2>CITA DEL DÍA</h2>
	</div>			
	<div class="col-xs-12 book-phrase-content no-padding-left desk">
			"<?php $quote = utf8_decode($data['random_quote']['quote']);
			echo $quote; ?>"
	</div>
	<div class="col-xs-12 no-padding-left text-right">
			-<?=$data['random_quote']['author']?>
	</div>	
	<div class="col-xs-12 no-padding-left">
			<span style='color:#efc84e'>Compartir <img onclick='shareFB("Cita del día","<?=$quote?>","https://librosmexico.mx/assets/images/libros_logo100x.png","https://librosmexico.mx")' src='assets/images/detail/facebook_cita.svg' class='social-medios-mini pointer'><!--<img src='assets/images/detail/twitter_cita.svg' class='social-medios-mini pointer' onclick="shareTW(<?=$twquote?>)">-->
				<?php
                    echo '<a class="a2a_button_twitter" target="_blank" onclick="window.open(&#34http://www.addtoany.com/add_to/twitter?linkurl='.urlencode(URL::base(true)).'&amp;linkname='.urlencode('"'.$quote.'"')." desde @LibrosMexicoMX".'&amp;linknote=&#34,&#34_blank&#34,&#34top=200, left=200, width=450, height=500&#34)" rel="nofollow" aria-label="Twitter"><span class="a2a_svg a2a_s__default a2a_s_twitter" style="width: 13px; line-height: 13px; height: 13px; border-radius: 2px; background-size: 13px; background-image: url(\'assets/images/twitternew.svg\'); display: inline-block; vertical-align:middle; cursor:pointer!important;"></span></a>';
                ?>
			</span>

	</div>	
</div>	
<link rel="stylesheet" type="text/css" href="assets/styles/book_detail.min.css"/>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
<script async src="//static.addtoany.com/menu/page.js"></script>
