<link rel="stylesheet" href="assets/styles/biblioteca_general.css">
<link rel="stylesheet" type="text/css" href="assets/styles/custom_f.css"/>
<link rel="stylesheet" type="text/css" href="assets/styles/right_side.css"/>

<div class="library-content">
	<div class="lists-panel col-md-4">
		<!-- contenido del panel derecho -->
		<?php include 'reader_library_menu.php';?>
	</div>


	<div class="books-panel col-md-8 col-xs-12" data-setmobile="0">
		<!-- contenido del panel derecho -->
		<?php include 'reader_library_data.php';?>
	</div>
</div>