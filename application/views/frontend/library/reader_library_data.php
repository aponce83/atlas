<?php 
	$class[0]='';
	$class[1]='no-margin-right';

	$class[2]='classic-list';
	$class[3]='dynamic-list';
	$class[4]='genius-list';

	$class[5]='flecha_left_verde.svg';
	$class[6]='flecha_naranja.svg';
	$class[7]='flecha_azul.svg';

	$class[8] ='default';
	$class[9] ='myList';
	$class[10]='intelligent';	

	$class[11]='hidden-list';
	$classic_counter=0;
	$dynamic_counter=0;

	$counter = 1;
	foreach ($data['list'] as $list){
?>
		<?php 
			   if($list->locked==1 && $list->type=='classic')
			   {
					$addClass=$class[2];
					$im=$class[5];
					$rowClass=$class[8];
			   }
			   else if($list->type=='classic') {
			   		$addClass=$class[3];
					$im=$class[6];
					$rowClass=$class[9];

					if($classic_counter<2)
					{
						$hidden =$class[0];
						$classic_counter++;
					}
					else 
						$hidden = $class[11];								   		
			   }
				else if($list->type=='dynamic')
				{
					$addClass=$class[4];
					$im=$class[7];
					$rowClass=$class[10];
					if($dynamic_counter<2)
					{
						$hidden =$class[0];
						$dynamic_counter++;
					}
					else 
						$hidden = $class[11];	
				}
				else{
						$hidden = $class[11];	
						$addClass=$class[0];
						$im=$class[0];
						$rowClass=$class[0];	
						}										
		?>

	<div class='list-info-container no-padding col-lg-12 <?=$rowClass?> <?=$list->id?>' data-listname='<?=$list->name?>'>
		<div class='section-list-title col-lg-12'>
	<a href="lista/<?php echo $list->getUser();?>/<?=$list->slug?>"><?=$list->name?> (<?=$list->num_books?>)</a>

			
			
		</div>	

		<div class='book-container-list'>
			
				<div class="row">
					<div class="col-xs-12 fix-5px-sides list-wrapper">
						<?php 
							$counter = 1;	     	
							$aux_break = 0;
					     	foreach($list->books as $item)      
					     	{
					     		if ($item['imagen'] == null) {
					     			$item['imagen'] = "assets/images/generic_book.jpg";
					     		}
					     		if($counter > 4 ){ ?>
					     			<div class="item-list-wrp remove-mobile">
											<?php echo ( Model::factory('List')->is_new_book_in_list( $item['id_libro'] , $list->id ) ) ? '<div class="is_new_book">Nuevo</div>' : ''; ?>
					     				<a href="libros/<?php echo $item['id_libro']; ?>">
					     					<img src="<?php echo $item['imagen']; ?>" class='mini-list-book'>
					     				</a>
					     			</div>	     			
					     		<?php  }
					     		else{ ?>
					     			<div class="item-list-wrp">
					     				<?php echo ( Model::factory('List')->is_new_book_in_list( $item['id_libro'] , $list->id ) ) ? '<div class="is_new_book">Nuevo</div>' : ''; ?>
					     				<a href="libros/<?php echo $item['id_libro']; ?>">
						     				<img src="<?php echo $item['imagen']; ?>" class='mini-list-book'>
						     			</a>
						     		</div>	 
						     		<?php $counter++;
						     	}

						    if (++$aux_break == 7) break;

						    }

				    if ($list->num_books == 0) {
				    ?>
				    	<p class="empty-list"><?=$data['user']['fullname']?> no ha agregado libros a esta lista</p>
				    	<?php
				    }
					     	?>
					</div>
					
				</div>
			
	     		


		</div>	

		<?php echo "<div class='section-see-list $addClass col-lg-12'>";?>
		<a href="lista/<?php echo $list->getUser();?>/<?=$list->slug?>">ver lista <img src= 'assets/images/detail/<?=$im?>' class='arrow_list'></a>
		</div>	
	</div>	
<?php };?>


<!-- Modal Delete List-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Eliminar Lista</h4>
      </div>
      <div class="modal-body">
      	<h4 class='modal-text'> </h4>
      	<div id='create-button' class='modal-buttons'>
      		<button id='erase' class='btn modal-delete' onclick='eraseList(this.value)'>Borrar</button>
      		<button class='btn btn-modal' data-dismiss="modal" aria-label="Close">Cancelar</button>
      	</div>	
      </div>
    </div>
  </div>
</div>

<script>
	$(window).load(function() {
    setTimeout(function(){ fixVerticalAlign();  }, 6000);
	});
	$(document).ready(function(){
		//alineación vertical-middle para las portadas de los libros en resize
		$(window).resize(function(){
			fixVerticalAlign();
		});
	});
	function fixVerticalAlign() {
		$('.list-wrapper').each(function(){
			var wrpHeight = $(this).height();
			$(this).children('.item-list-wrp').each(function(){
				var itemHeight = $(this).height();
				var fixTop = (wrpHeight - itemHeight) / 2;
				$(this).css('top', fixTop);
			});
		});	
	}
</script>
