<?php
$prefix_url = URL::base(true).PI_API_V1.'/';
foreach ($services as $k => $s) {
  $label_class = 'label-success';
  $attr_class = 'text-success';
  if (strtolower($s['endpoint']['method']) == 'post') {
    $label_class = 'label-primary';
    $attr_class = 'text-primary';
  }
  $url_print = str_replace( array('{', '}'), array('<strong class="'.$attr_class.'">{', '}</strong>'), $s['endpoint']['url'] );
?>
<div class="row" id="#service_<?php echo $k; ?>">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title"><?php echo $s['title']; ?></h4>
      </div>
      <div class="panel-body">
        <div class="service-description">
          <p>
            <?php echo $s['desc']; ?>
          </p>
        </div>
        <hr>
        <div class="service-enpoint">
          <h5>Endpoint</h5>
          <span class="label <?php echo $label_class; ?>"><?php echo $s['endpoint']['method']; ?></span>
          <span class=""><?php echo $prefix_url.$url_print; ?></span>
        </div>
        <hr>
        <?php
        if (count($s['endpoint']['attr']) > 0) {
        ?>
        <div class="service-attributes">
          <h5>Atributos</h5>
          <ul class="list-unstyled">
            <?php
            foreach ($s['endpoint']['attr'] as $key => $value) {
            ?>
            <li><strong class="text-primary"><?php echo $key ?></strong>: <?php echo $value; ?></li>
            <?php
            }
            ?>
          </ul>
        </div>
        <hr>
        <?php
        }
        ?>
        <div class="service-response">
          <?php
          if ($s['response']) {
          ?>
          <h5>Respuestas</h5>
          <?php 
            if ($s['response']['success']) {
            ?>
            <h4>Éxito</h4>
            <pre><code>
<?php echo LM::json_format($s['response']['success']); ?>
            </code></pre>
          <?php
            }
            ?>
          <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
}
?>
