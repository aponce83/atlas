<!-- Perfil -->
<div class="row">
  <div class="col-md-6 center">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h3 class="panel-title">
          <?php echo $data['api_user']['fullname']; ?>
        </h3>
      </div>
      <div class="panel-body">
        <table class="table">
          <tr>
            <th>Mail</th>
            <td><?php echo $data['api_user']['email']; ?></td>
          </tr>
          <tr>
            <th>Estatus</th>
            <td><?php echo $data['api_user']['status']; ?></td>
          </tr>
          <tr>
            <th colspan="2">
              ¿Qué aplicaciones vas a desarrollar?
            </th>
          </tr>
          <tr>
            <td colspan="2">
              <?php echo $data['api_user']['description']; ?>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- Llaves -->
<div class="row">
  <div class="col-md-12 center">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h3 class="panel-title">
          API KEYS
        </h3>
      </div>
      <div class="panel-body">
        <?php
        if ($data['api_user']['status'] == 'en espera') {
        ?>
        <p>Cuando ya seas aprobado aquí se mostrarán tus llaves</p>
        <?php
        }
        ?>
        <?php
        if ($data['api_user']['status'] == 'aprobado') {
        ?>
          <table class="table">
            <thead>
              <tr>
                <th>API_ID</th>
                <th>API_KEY</th>
                <th>API_SIGNATURE</th>
                <th>CURL</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($data['api_user_keys'] as $api_user_key) {
              ?>
              <tr>
                <td><?php echo $api_user_key['api_id'];?></td>
                <td><?php echo $api_user_key['api_key'];?></td>
                <td>
                  <?php
                  $api_signature = md5(date('YmdHis').$api_user_key['api_id'].$api_user_key['api_key']);
                  echo $api_signature;
                  ?>
                </td>
                <td>
                  <code>
                  <?php
echo 'curl --data "api_id='.$api_user_key['api_id'].'&amp;api_signature='.$api_signature.'&amp;current_date='.date('YmdHis').'" '.URL::base(true).PI_API_V1.'/userfront/13';
                  ?>
                  </code>
                </td>
              </tr>
              <?php
              }
              ?>
            </tbody>
          </table>
        <?php
        }
        ?>
      </div>
    </div>
  </div>
</div>