<div class="row">
  <div class="col-md-6 center">
    <div class="panel panel-white">
      <div class="panel-body">
        <form 
          class="m-t-md" 
          action="api/v1/apiuser/do_login"
          method="POST"
          id="form_api_v1_apiuser_start">
          <?php
          if ($data['admin_login_error']) {
          ?>
          <div class="alert alert-danger" role="alert">
            Tu correo electrónico o tu contraseña es incorrecta.
          </div>
          <?php
          }
          ?>
          <div class="form-group" id="form-email-group">
            <input 
              type="email" 
              class="form-control"
              name="email"
              placeholder="Mail" 
              required="" 
              autocomplete="off">
          </div>
          <div class="form-group">
            <input 
              type="password" 
              class="form-control" 
              name="password"
              placeholder="Contraseña"
              required="" 
              autocomplete="off">
          </div>
          <button 
            type="submit" 
            class="btn btn-success btn-block m-t-xs">
            Iniciar sesión
          </button>
          <p class="text-center m-t-xs text-sm">
            ¿No tienes cuenta?
          </p>
          <a 
            href="api/v1/apiuser/register" 
            class="btn btn-default btn-block m-t-xs">
            Crear cuenta
          </a>
        </form>
      </div>
    </div>
  </div>
</div>
