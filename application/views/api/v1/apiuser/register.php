<div class="row">
  <div class="col-md-6 center">
    <div class="panel panel-white">
      <div class="panel-body">
        <p>
          Llena los siguientes campos para crear una cuenta, una vez que tu cuenta
          que se haya creado, estarás en espera de aprobación para tener acceso al
          API.
        </p>
        <form 
          class="m-t-md" 
          action="api/v1/apiuser/create"
          method="POST"
          id="form_api_v1_apiuser_create">
          <div class="form-group">
            <input
              type="text"
              class="form-control"
              name="fullname"
              placeholder="Nombre completo" 
              required="" 
              autocomplete="off"
              maxlength="50">
          </div>
          <div class="form-group" id="form-email-group">
            <input 
              type="email" 
              class="form-control"
              name="email"
              placeholder="Mail" 
              required="" 
              autocomplete="off">
            <small 
              class="hide text-danger">
              El correo ya está registrado
            </small>
          </div>
          <div class="form-group">
            <input 
              type="password" 
              class="form-control" 
              name="password"
              placeholder="Contraseña"
              required="" 
              autocomplete="off">
            <small>
              Más de 6 caractéres, difícil de adivinar, incluye mayúsculas, minúsculas, números y signos</small>
            <div class="progress progress-xs">
              <div
                class="progress-bar"
                id="progress-bar-password"
                role="progressbar" 
                aria-valuenow="40"
                aria-valuemin="0" 
                aria-valuemax="100">
              </div>
            </div>
          </div>
          <div class="form-group">
            <textarea 
              class="form-control"
              name="reasons"
              placeholder="¿Qué tipo de aplicaciones vas a desarrollar?" 
              required="" 
              autocomplete="off"></textarea>
          </div>
          <label>
            <div class="checker">
              <span>
                <input type="checkbox" id="form_api_v1_apiuser_create_checkbox">
              </span>
            </div> Acepto las <a href="politicas-de-privacidad" target="_blank">políticas de privacidad</a> y <a href="terminos-y-condiciones" target="_blank">los términos y condiciones</a>.
          </label>
          <button 
            type="submit" 
            class="btn btn-success btn-block m-t-xs">
            Crear cuenta
          </button>
          <p class="text-center m-t-xs text-sm">
            ¿Ya tienes una cuenta?
          </p>
          <a 
            href="api/v1/apiuser/login" 
            class="btn btn-default btn-block m-t-xs">
            Inicia sesión
          </a>
        </form>
      </div>
    </div>
  </div>
</div>
<script>

  function check_password_strength() {
    var password = $('#form_api_v1_apiuser_create input[type=password]').val();
    var strength = 0;
    
    if (password.length == 0) {
      set_password_bar(g_previous_class, '0%');
      return 0;
    }
    if (password.length > 7) { 
      strength += 1;
    }
    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
      strength += 1;
    } 
    if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) {
      strength += 1;
    } 
    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
      strength += 1;
    }
    if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,",%,&,@,#,$,^,*,?,_,~])/)) {
      strength += 1;
    }

    if (strength == 0) {
      set_password_bar('progress-bar-danger', '5%');
    } else if (strength == 1) {
      set_password_bar('progress-bar-danger', '15%');
    } else if (strength > 1 && strength < 3) {
      set_password_bar('progress-bar-warning', '50%');
    } else if (strength == 4) {
      set_password_bar('progress-bar-warning', '75%');
    } else if (strength == 5) {
      set_password_bar('progress-bar-success', '100%');
    }
    return strength;
  }

  var g_previous_class = null;
  function set_password_bar(n_class, n_width) {
    if ($('#progress-bar-password').hasClass(g_previous_class)) {
      $('#progress-bar-password').removeClass(g_previous_class);
    }
    $('#progress-bar-password').addClass(n_class);
    $('#progress-bar-password').css('width', n_width);
    g_previous_class = n_class;
  }

  $(function() {
    $('#form_api_v1_apiuser_create input[type=password]').keyup(function(){
      check_password_strength();
    });

    $('#form_api_v1_apiuser_create').submit(function(e) {
      var checked_conditions = $('#form_api_v1_apiuser_create_checkbox').is(":checked");
      if (!checked_conditions) {
        alert("Para crear tu cuenta tienes que aceptar las políticas de privacidad y los términos y condiciones.");
        e.preventDefault();
      }
      var password_strength = check_password_strength();
      if (password_strength <= 1) {
        alert('Tu contraseña es muy débil, por favor escribe una contraseña más difícil de adivinar.');
        e.preventDefault();
      }

      var action = $(this).attr('action');
      var method = $(this).attr('method');
      $.ajax({
        url: action,
        type: method,
        data: $(this).serialize(),
        dataType: "json",
        success: function (data) {
          if (data.header.status == 'ERROR') {
            if (data.header.error.code == 2002) {
              if (!$('#form-email-group').hasClass('has-error')) {
                $('#form-email-group').addClass('has-error');
                $('#form-email-group small').removeClass('hide');
                $('#form-email-group small').addClass('show');
                $('#form-email-group input[type=email]').keyup(function(e){
                  var val = $('#form-email-group input[type=email]').val();
                  if (val.length > 1) {
                    $('#form-email-group small').addClass('hide');
                    $('#form-email-group small').removeClass('show');
                    $('#form-email-group').removeClass('has-error');
                  }
                });
              }
            }
          } else {
            window.location = 'api/v1/apiuser/profile';
          }
        }
      });
      e.preventDefault();
    });
  });
</script>
