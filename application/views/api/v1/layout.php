<!DOCTYPE html>
<html>
<head>
  <!-- Title -->
  <title>
    Libros México
    <?php 
    if (isset($data)) {
      if (isset($data['page_title'])) {
        echo "| {$data['page_title']}";
      }
    }
    ?>
  </title>
  
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta charset="UTF-8">
  
  <base href="<?php echo URL::base(); ?>">

  <!-- Styles -->
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
  <link href="assets/modernui-framework/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
  <link href="assets/modernui-framework/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
  <link href="assets/modernui-framework/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <link href="assets/modernui-framework/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
  <link href="assets/modernui-framework/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/> 
  <link href="assets/modernui-framework/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>  
  <link href="assets/modernui-framework/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>  
  <link href="assets/modernui-framework/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
  <link href="assets/modernui-framework/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>
  <link href="assets/modernui-framework/plugins/slidepushmenus/css/component.css" rel="stylesheet" type="text/css"/>

  <link href="assets/modernui-framework/plugins/datatables/css/jquery.datatables.min.css" rel="stylesheet" type="text/css"/> 
  <link href="assets/modernui-framework/plugins/datatables/css/jquery.datatables_themeroller.css" rel="stylesheet" type="text/css"/> 

  <link href="assets/modernui-framework/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet" type="text/css"/> 
  <link href="assets/modernui-framework/plugins/metrojs/MetroJs.min.css" rel="stylesheet" type="text/css"/>  
  <link href="assets/modernui-framework/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>  

  <!-- Theme Styles -->
  <link href="assets/modernui-framework/css/modern.min.css" rel="stylesheet" type="text/css"/>
  <link href="assets/modernui-framework/css/themes/green.css" class="theme-color" rel="stylesheet" type="text/css"/>
  <link href="assets/modernui-framework/css/custom.css" rel="stylesheet" type="text/css"/>

  <link href="assets/modernui-framework/plugins/x-editable/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet" type="text/css">
  
  <script src="assets/modernui-framework/plugins/3d-bold-navigation/js/modernizr.js"></script>
  <script src="assets/modernui-framework/plugins/offcanvasmenueffects/js/snap.svg-min.js"></script>
  
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- Javascripts -->
  <script src="assets/modernui-framework/plugins/jquery/jquery-2.1.4.min.js"></script>
  <script src="assets/modernui-framework/plugins/jquery-ui/jquery-ui.min.js"></script>
  <script src="assets/modernui-framework/plugins/pace-master/pace.min.js"></script>
  <script src="assets/modernui-framework/plugins/jquery-blockui/jquery.blockui.js"></script>
  <script src="assets/modernui-framework/plugins/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/modernui-framework/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <script src="assets/modernui-framework/plugins/switchery/switchery.min.js"></script>
  <script src="assets/modernui-framework/plugins/uniform/jquery.uniform.min.js"></script>
  <script src="assets/modernui-framework/plugins/offcanvasmenueffects/js/classie.js"></script>
  <script src="assets/modernui-framework/plugins/offcanvasmenueffects/js/main.js"></script>
  <script src="assets/modernui-framework/plugins/waves/waves.min.js"></script>
  <script src="assets/modernui-framework/plugins/3d-bold-navigation/js/main.js"></script>
  <script src="assets/modernui-framework/plugins/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/modernui-framework/plugins/jquery-counterup/jquery.counterup.min.js"></script>
  <script src="assets/modernui-framework/plugins/toastr/toastr.min.js"></script>
  <script src="assets/modernui-framework/plugins/flot/jquery.flot.min.js"></script>
  <script src="assets/modernui-framework/plugins/flot/jquery.flot.time.min.js"></script>
  <script src="assets/modernui-framework/plugins/flot/jquery.flot.symbol.min.js"></script>
  <script src="assets/modernui-framework/plugins/flot/jquery.flot.resize.min.js"></script>
  <script src="assets/modernui-framework/plugins/flot/jquery.flot.tooltip.min.js"></script>
  <script src="assets/modernui-framework/plugins/curvedlines/curvedLines.js"></script>
  <script src="assets/modernui-framework/plugins/metrojs/MetroJs.min.js"></script>
  <script src="assets/modernui-framework/js/modern.min.js"></script>
  <script src="assets/modernui-framework/plugins/x-editable/bootstrap3-editable/js/bootstrap-editable.js"></script>
  <script src="assets/modernui-framework/plugins/datatables/js/jquery.datatables.min.js"></script>

  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.1.0/styles/default.min.css">
  <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.1.0/highlight.min.js"></script>

  <link rel="icon" href="favicon.ico" type="image/x-icon" />
</head>
<body class="page-header-fixed">

<div class="overlay"></div>

<main class="page-content content-wrap">
  <div class="navbar">
    <div class="navbar-inner">
      <div class="sidebar-pusher">
        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
          <i class="fa fa-bars"></i>
        </a>
      </div>
      <div class="logo-box">
        <a href="api/v1" class="logo-text"><span>LibrosMéxico</span></a>
      </div><!-- Logo Box -->
      
      <div class="topmenu-outer">
        <div class="top-menu">
          <ul class="nav navbar-nav navbar-left">
            <li>  
              <a href="javascript:void(0);" class="waves-effect waves-button waves-classic sidebar-toggle"><i class="fa fa-bars"></i></a>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
          <?php
          if ($api_user) {
          ?>
            <li>
              <a href="api/v1/apiuser/profile" class="log-out waves-effect waves-button waves-classic">
               <?php echo $api_user['fullname']; ?>
              </a>
            </li>
            <li>
              <a href="api/v1/apiuser/logout" class="log-out waves-effect waves-button waves-classic">
                <i class="fa fa-sign-out"></i> Cerrar sesión
              </a>
            </li>
          <?php
          } else {
          ?>
           <li>
              <a href="api/v1/apiuser/login" class="log-out waves-effect waves-button waves-classic">
                Iniciar sesión
              </a>
            </li>
            <li>
              <a href="api/v1/apiuser/register" class="log-out waves-effect waves-button waves-classic">
                Registrarse
              </a>
            </li>
          <?php
          }
          ?>
          </ul><!-- Nav -->
        </div><!-- Top Menu -->
      </div>
    </div>
  </div><!-- Navbar -->
  <div class="page-sidebar sidebar">
    <div class="page-sidebar-inner slimscroll">
      <ul class="menu accordion-menu">
        <li class="active">
          <a href="api/v1" class="waves-effect waves-button">
            <span class="menu-icon glyphicon glyphicon-home"></span>
            <p>Inicio</p>
          </a>
        </li>
        <li>
          <a href="api/v1/doc/usuarios" class="waves-effect waves-button">
            <span class="menu-icon glyphicon glyphicon-user"></span>
            <p>Usuarios</p>
          </a>
        </li>
        <li>
          <a href="api/v1/doc/atlas" class="waves-effect waves-button">
            <span class="menu-icon glyphicon glyphicon-map-marker"></span>
            <p>Atlas</p>
          </a>
        </li>
        <li>
          <a href="api/v1/doc/libros" class="waves-effect waves-button">
            <span class="menu-icon glyphicon glyphicon-book"></span>
            <p>Libros</p>
          </a>
        </li>
      </ul>
    </div><!-- Page Sidebar Inner -->
  </div><!-- Page Sidebar -->
  <div class="page-inner">
    <div class="page-title">
      <h3><?php echo isset($data['page_title']) ? $data['page_title'] : 'LibrosMéxico.mx'; ?></h3>
    </div>
    <div id="main-wrapper">
      <?php 
        echo $content; 
      ?>
    </div><!-- Main Wrapper -->
    <div class="page-footer">
      <p class="no-s">
        <?php echo date('Y'); ?> &copy; <a href="https://librosmexico.mx" target="_blank">LibrosMéxico.mx</a>
      </p>
    </div>
  </div><!-- Page Inner -->
</main><!-- Page Content -->
<div class="cd-overlay"></div>
<script>
  hljs.configure({
    tabReplace: '  ',
  });
  hljs.initHighlighting();
</script>
</body>
</html>