<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Introducción</h4>
      </div>
      <div class="panel-body">
        <p>Bienvenido al API de LibrosMéxico.mx, en esta sección podrás encontrar
        toda la información necesaria para conectarte a los servicios que proporcionamos</p>
        <p>
        Para realizar la conexión con los servicios es necesario seguir los siguientes procedimientos:
        <ol>
          <li>Obtener una cuenta de acceso <a href="api/v1/apiuser/register">aquí</a>.</li>
          <li>Esperar a qué tu cuenta sea aprobada.</li>
          <li>Una vez que tengas la cuenta aprobada podrás hacer las peticiones
          necesarias para utilizar el sistema, usando tus <b>llaves de acceso</b>.</li>
        </ol>
        </p>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Entidades</h4>
      </div>
      <div class="panel-body">
        <p>La plataforma pone a tu disposición las siguientes entidades para que
        realices tus consultas sobre ellas:
        <ul>
          <li>Usuarios: puedes consultar la información básica de usuarios cuyo perfil sea público.</li>
          <li>Listas: puedes consultar la información de las listas públicas.</li>
          <li>Libros: puedes consultar la información básica de un libro.</li>
          <li>Lugares: puedes consultar la información de los lugares que tenemos disponibles en el atlas.</li>
        </ul>
        </p>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-white">
      <div class="panel-heading clearfix">
        <h4 class="panel-title">Autenticación</h4>
      </div>
      <div class="panel-body">
        <p>Para hacer cualquier petición al sistema tienes que autenticarte, una vez que tengas tus llaves de acceso podrás hacer las solicitudes que quieras.</p>
        <h5>¿Cómo autenticarse?</h5>
        <p>Todas tus peticiones se tienen que enviar a través de <span class="label label-primary">POST</span> con las siguientes variables:</p>
        <ul class="list-unstyled">
          <li>
            <strong class="text-primary">current_date</strong>: Fecha actual en el formato <code>YmdHis</code>.
          </li>
          <li>
            <strong class="text-primary">api_id</strong>: Tu id de acceso, se obtiene una vez que se ha aprobado tu acceso, 
            puedes revisar el estatus de acceso <a href="api/v1/apiuser/profile">aquí</a>.
          </li>
          <li>
            <strong class="text-primary">api_signature</strong>: Tu firma de acceso, 
            esta se genera de la siguiente manera 
            <code>api_signature = md5(<strong class="text-primary">current_date</strong> + <strong class="text-primary">api_id</strong> + <strong class="text-success">api_key</strong>)</code>.
          </li>
        </ul>
        <p>Al igual que el <strong class="text-primary">api_id</strong>, la <strong class="text-success">api_key</strong> podrás encontrarla en <a href="api/v1/apiuser/profile">tu perfil</a> una vez que has sido aprobado.</p>
        <p><span class="text-danger">Solo podrás autenticarte si tu perfil está aprobado.</span></p>
        <p>Algunos servicios requieren que se envíen otras variables a parte de las variables de autenticación, se asume que siempre enviarás las variables de autenticación por lo cual no estarán descritas en la documentación.</p>
        <h5>Ejemplo</h5>
        <h6>CURL</h6>
        <p>A continuación se describe un ejemplo usando CURL</p>
        <code>
          curl --data "api_id=<strong class="text-primary">api_id</strong>&amp;api_signature=<strong class="text-primary">api_signature</strong>&amp;current_date=<strong class="text-primary">current_date</strong>" <?php echo URL::base(true).PI_API_V1; ?>/userfront/{<strong class="text-primary">id</strong>}';
        </code>
        <hr>
        <h4>Error de autenticación</h4>
        <p>Si realizas una petición al servidor con las llaves incorrectas se mostrará el siguiente error:</p>
        <pre>
          <code class="javascript">
            {
              "header" : {
                "status" : "ERROR",
                "error" : {
                  "code" : 9999,
                  "message" : "Las credenciales son inválidas"
                }
              },
              "body" : []
            }
          </code>
        </pre>
      </div>
    </div>
  </div>
</div>

