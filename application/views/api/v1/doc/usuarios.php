<?php
  $services = array();
  $services[] = array('title' => 'Obtener información de un usuario',
    'desc' => 'Este método se utiliza para obtener el nombre, score y url del usuario, siempre que su perfil sea público. <br> Si el usuario no existe el sistema aún regresará el estatus de <code>SUCCESS</code> pero el <code>body.userfront</code> estará vacío.',
    'endpoint' => array('method' => 'post',
      'url' => 'userfront/{id}', 
      'attr' => array('id' => 'id del usuario')
    ),
    'response' => array('success' => '{ "header" : { "status" : "SUCCESS", "error" : { "code" : null, "message" : null } }, "body" : { "userfront" : { "score" : "0", "fullname" : "Diego Villaseñor", "url" : "http://localhost/librosmexico.mx/lector/{id}/circulo" } } }')
  );
?>
<?php include Kohana::find_file('views', 'api/v1/services_layout'); ?>