<?php
  $services = array();
  $services[] = array('title' => 'Obtener información de un lugar',
    'desc' => 'Se utiliza para obtener la información de un lugar. <br> Si el lugar no existe el sistema aún regresará el estatus de <code>SUCCESS</code> pero el <code>body.location</code> estará vacío.',
    'endpoint' => array('method' => 'post',
      'url' => 'atlas/{id}', 
      'attr' => array('id' => 'id del lugar')
    ),
    'response' => array('success' => '{ "header" : { "status" : "SUCCESS", "error" : { "code" : null, "message" : null } }, "body" : { "location" : { "slug" : "biblioteca-publica-municipal-venustiano-carranza", "zip" : "91700", "owner_id" : null, "tags" : [ { "slug" : "biblioteca", "id" : "1", "name" : "Biblioteca" } ], "image" : "places/biblioteca_4552.png", "facebook" : null, "street" : "Zaragoza 397 entre Esteban Morales y Francisco Can", "latitude" : "19.19789", "city" : null, "contact" : null, "town" : "Zona Centro", "name" : "Biblioteca Pública Municipal Venustiano Carranza", "twitter" : null, "state" : "Veracruz de Ignacio de la Llav", "id" : "49341", "int_number" : null, "email" : "carranzaveracruz2006@hotmail.com", "longitude" : "-96.13516", "phone" : "(229) 200 22 45", "municipal_office" : "Veracruz", "webpage" : "", "ext_number" : null, "description" : null } } }')
  );
  $services[] = array('title' => 'Buscar un lugar usando latitud y longitud',
    'desc' => 'Se busca un lugar a partir de su <code>latitud</code> y <code>longitud</code>.',
    'endpoint' => array('method' => 'post',
      'url' => 'atlas/search/{latitud}/{longitud}/{página:1}', 
      'attr' => array('latitud' => 'Latitud en grados decimales e.g.: 19.4326',
        'longitud' => 'Longitud en grados decimales e.g.: -99.1332',
        'página' => 'Página de resultados empezando por el 0. Valor predeterminado: 0')
    ),
    'response' => array('success' => '{ "header" : { "status" : "SUCCESS", "error" : { "code" : null, "message" : null } }, "body" : { "locations" : [ { "slug" : null, "zip" : "08040", "owner_id" : null, "tags" : [ { "slug" : "sala-de-lectura", "id" : "6", "name" : "Sala de lectura" } ], "image" : null, "facebook" : null, "street" : "Lenguas indígenas y Chinantecos", "latitude" : "-34.6036844", "city" : "", "contact" : null, "town" : "Lic. Carlos Zapata Vela", "name" : "Juan Rulfo", "twitter" : null, "state" : "Distrito Federal", "id" : "53391", "int_number" : "", "email" : "psic_gab@yahoo.com.mx", "longitude" : "-58.3815591", "phone" : "56 54 21 72", "municipal_office" : "Iztacalco ", "webpage" : null, "ext_number" : "", "description" : "" }, { "slug" : null, "zip" : "11430", "owner_id" : null, "tags" : [ { "slug" : "sala-de-lectura", "id" : "6", "name" : "Sala de lectura" } ], "image" : null, "facebook" : null, "street" : "Calz. Legaria y Lago Gran Oso", "latitude" : "-34.6036844", "city" : "", "contact" : null, "town" : "Pensil ", "name" : "C.S.S Legaria", "twitter" : null, "state" : "Distrito Federal", "id" : "53394", "int_number" : "", "email" : "julio.avendano@imss.gob.mx", "longitude" : "-58.3815591", "phone" : "55 27 39 55", "municipal_office" : "Miguel Hidalgo", "webpage" : null, "ext_number" : "s/n", "description" : "" }, { "slug" : null, "zip" : "00000", "owner_id" : null, "tags" : [ { "slug" : "sala-de-lectura", "id" : "6", "name" : "Sala de lectura" } ], "image" : null, "facebook" : null, "street" : "", "latitude" : "-34.6036844", "city" : "", "contact" : null, "town" : " ", "name" : "", "twitter" : null, "state" : "Distrito Federal", "id" : "53436", "int_number" : "", "email" : "tonapink@hotmail.com", "longitude" : "-58.3815591", "phone" : "", "municipal_office" : "", "webpage" : null, "ext_number" : "", "description" : "" } ] } }')
  );
?>
<?php include Kohana::find_file('views', 'api/v1/services_layout'); ?>