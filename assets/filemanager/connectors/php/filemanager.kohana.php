<?php

/**
 *	We need to include Kohana in order to get the authenticaded user
 *
 */


define('EXT', '.php');
define('DOCROOT', $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR);

$application = DOCROOT.'application';
$modules = DOCROOT.'modules';
$system = DOCROOT.'system';

define('APPPATH', $application.DIRECTORY_SEPARATOR);
define('MODPATH', $modules.DIRECTORY_SEPARATOR);
define('SYSPATH', $system.DIRECTORY_SEPARATOR);

unset($application, $modules, $system);

if ( ! defined('KOHANA_START_TIME'))
{
	define('KOHANA_START_TIME', microtime(TRUE));
}

if ( ! defined('KOHANA_START_MEMORY'))
{
	define('KOHANA_START_MEMORY', memory_get_usage());
}


require SYSPATH.'classes/kohana/core'.EXT;

if (is_file(APPPATH.'classes/kohana'.EXT))
{
	require APPPATH.'classes/kohana'.EXT;
}
else
{
	require SYSPATH.'classes/kohana'.EXT;
}

date_default_timezone_set('America/Mexico_City');

setlocale(LC_ALL, 'es_MX.utf-8');

spl_autoload_register(array('Kohana', 'auto_load'));

ini_set('unserialize_callback_func', 'spl_autoload_call');

I18n::lang('es-mx');

if (isset($_SERVER['KOHANA_ENV']))
{
	Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
}
Kohana::$environment = ($_SERVER['SERVER_NAME']=='clubdigital.comingsoon.no') ? Kohana::STAGING : Kohana::DEVELOPMENT;

Kohana::init(array(
	'base_url'   => '/',
));

Kohana::$log->attach(new Log_File(APPPATH.'logs'));

Kohana::$config->attach(new Config_File);

Kohana::modules(array(
	'auth'       => MODPATH.'auth',       // Basic authentication
	'cache'      => MODPATH.'cache',      // Caching with multiple backends
	// 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
	'database'   => MODPATH.'database',   // Database access
	'image'      => MODPATH.'image',      // Image manipulation
	// 'orm'        => MODPATH.'orm',        // Object Relationship Mapping
	// 'unittest'   => MODPATH.'unittest',   // Unit testing
	'userguide'  => MODPATH.'userguide',  // User guide and API documentation
	'contento'   => MODPATH.'contento',   // Contento modules: Auth, Pagination, Model, etc
	));

Session::$default = 'database';
Cookie::$salt     = 'caDruYUFrAWr6neBr5*PaWe$';
Cookie::$httponly = TRUE;