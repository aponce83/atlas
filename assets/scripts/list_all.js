
function reloadPage(from) {
	var order = "";
	var limit = "";
	var url = $("#page-url").val();
	if (from == "desktop") {
		order = $("#list_order").val();
		limit = $("#list_show").val();
	} else if (from == "mobile") {
		order = $('input[name=book-order]:checked').val();
		limit = $('input[name=book-show]:checked').val();
	}
	url += "?";
	if (order != "-") {
		url += "expression="+order;
	}
	if (limit != "-") {
		url += "&limit="+limit;
	}
	window.location.href = url;

}
