var exito = 'success';
var error = 'error';
var warning = 'warn';

function setNotify( messageText, messageType ) {

	$.notify( messageText, {
		className: messageType,
		clickToHide: false,
		autoHide: true,
		autoHideDelay: 5000,
		hideDuration: 500,
		globalPosition: 'left bottom'
	});

}
