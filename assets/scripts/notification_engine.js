
$(document).ready(function(){

	var totalHeight = $(document).height();
	$('#notifWrp').height(totalHeight);

	$('.not-close-lnk').click(function(){
		$('#notifWrp').fadeOut(400, function(){
			$('#notifWrp').css('opacity', 0);
		});
	});


	$('.notif-share-social-tw').click(function(){
		var name = $(this).attr('data-name');
		var description = $(this).attr('data-description');
		var url = 'http://www.librosmexico.mx/';

		shareTW(url, name, description);
	});



	//compartir modal de trofeo
	$('.notif-share-social-fb').click(function(){
		var name = $(this).attr('data-name');
		var description = $(this).attr('data-description');
		var image = $(this).attr('data-image');
			image = image.replace('.svg', '.png');
		var url = 'http://www.librosmexico.mx/';

		shareFBNoAdward(name, description, image, url );
		
	});



});

function shareTW(url, titulo, autor){
	titulo = 'Gané el trofeo '+titulo+' '+autor+ ' en LIBROSMEXICO.MX';
    var params = {
        access_token: "65efccf36ae26915b2362f75c2b09b8856732202",
        longUrl: url,
        format: 'json'
    };
    $.getJSON('https://api-ssl.bitly.com/v3/shorten', params, function (response, status_txt) {
        var urlbit =response.data.url;
        var len = 140 - (urlbit.length)+7;  

		if (titulo.length>len) {
			titulo = titulo.substring(0,len);
			titulo = '"'+titulo+'..."';
		} else {
			titulo = '"'+titulo+'"';
		}

		var device = navigator.userAgent;
		var shareURL = 'http://twitter.com/intent/tweet?text='+titulo+' '+urlbit;

		if (device.match(/Iphone/i)|| device.match(/Ipod/i)|| device.match(/Android/i)|| device.match(/J2ME/i)|| device.match(/BlackBerry/i)|| device.match(/iPhone|iPad|iPod/i)|| device.match(/Opera Mini/i)|| device.match(/IEMobile/i)|| device.match(/Mobile/i)|| device.match(/Windows Phone/i)|| device.match(/windows mobile/i)|| device.match(/windows ce/i)|| device.match(/webOS/i)|| device.match(/palm/i)|| device.match(/bada/i)|| device.match(/series60/i)|| device.match(/nokia/i)|| device.match(/symbian/i)|| device.match(/HTC/i)){
			location.target="_newtab";
			location.href = shareURL; 
		}	else {
			
			window.open('http://twitter.com/intent/tweet?text='+titulo+' '+urlbit,"_blank","top=200, left=500, width=400, height=400");

		}
			
    });
}

function shareFBNoAdward(title, desc, img, url){
    var product_name   =    title;
    var description    =    desc;
    var share_image    =    img;
    var share_url      =    url;
    var share_caption  =    'librosmexico.mx';
    
    FB.ui({
        method: 'feed',
        appId: '394615860726938',   
        name: title,
        link: share_url,
        picture: share_image,
        caption : share_caption,
        description: description
    }, function(response) {
        if(response && response.post_id){
            
        }
        else{}
    }); 
}


function checkForNotification( userId )
{
	$.ajax({
	  	type: "POST",
	  	dataType: "json",
	  	url: "check-notification",
	  	data:{
	      	user_id: userId,
	  	},
	}).done(function(data){
  	if( data.flag == 1 )
  	{
    	//hay notificación por mostrar
    	notifBuilder(data);
  	}                             
	}).fail(function(data){
  		//fail actions                      
	});
	/**********/
	$.ajax({
				type:"POST",
				url:'api/social/pending_comments',
				data:{ user:userId },
				error: function(){
						
				},
				success: function(response) {
					var resp = $.parseJSON( response );
					if (resp.pending)
					{
							setNotify('Hay comentarios nuevos en tu muro',exito);
					}
				}
		});
}


function unsetNoty( idNotification )
{
	$.ajax({
  		type: "POST",
  		dataType: "json",
  		url: "unset-notification",
  		data:{
      		noty_id: idNotification,
  		},
	}).done(function(data){
  		//ok                      
	}).fail(function(data){
  		//fail actions                      
	});
}

function showNotification( notifParams )
{
	//se obtienen parámetros de nombre y descripción de la notificacion
	var notifName = notifParams['name'];
	var notifDescription = notifParams['description'];
	var notifImage = notifParams['image'];


	//se insertan los textos obtenidos en la pantalla de notificacion
	$('#awardName').text(notifName);
	$('#awardDesc').text(notifDescription);
	$('#adwardImage').attr('src', 'assets/images/'+notifImage);

	$('.notif-share-social-fb').attr('data-name', 'Gané el trofeo '+notifName );
	$('.notif-share-social-fb').attr('data-description',notifDescription );
	$('.notif-share-social-fb').attr('data-image', $('#adwardImage')[0].src );

	$('.notif-share-social-tw').attr('data-name', notifName );
	$('.notif-share-social-tw').attr('data-description',notifDescription );
	$('.a2a_button_twitter').attr('onclick', 'window.open("http://www.addtoany.com/add_to/twitter?linkurl='+encodeURIComponent(window.location.origin)+'&linkname='+encodeURIComponent('Gané el trofeo "'+notifName+'" por '+notifDescription+' en @LibrosMexicoMX')+'&linknote=","_blank","top=200, left=200, width=450, height=500")');

	//se obtiene valor del scroll actual
	var scrollHeigh = $(window).scrollTop();

	//se establece el top de la ventana de notificacion
	$('#notifWrp').css('top', scrollHeigh-100);
	$('#notifWrp').show();
	$("#notifWrp").animate({top: scrollHeigh, opacity: 1}, 300);

	

}


function notifBuilder(data)
{
	var obj = jQuery.parseJSON( data.content );
    
    var idNotification = data.idNoty;
    var notifParams = [];

    notifParams['name'] = obj.name;
    notifParams['description'] = obj.description;
    notifParams['image'] = obj.image;
              
    //función para mostrar notificación
    showNotification( notifParams );

    //se marca la notificacion como mostrada
    unsetNoty( idNotification );
}


