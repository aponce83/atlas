                                var preguntaActual = 0;
                                var respuestasTrivia = [];
                                var total_preguntas = 0;
                                var triviaelegida = 0;

                                function reset()
                                {
                                        preguntaActual = 0;
                                        respuestasTrivia = [];
                                        total_preguntas = 0;
                                        triviaelegida = 0;
                                }


                                function changeContent()
                                {
                                    changecontentbynumpage(parseInt(preguntaActual)+1);
                                }

                                function changecontentbynumpage(paginaclickeada)
                                {
                                        var trivia =  document.getElementById('TriviaElegida').value;
                                        //La variable global PreguntaActual tiene la pregunta donde estamos, 0 es la portada
                                        //PreguntaActual es donde estoy
                                        total_preguntas = parseInt(document.getElementById('numpreguntas'+trivia).value);
                                        //alert ("Numero de preguntas: "+total_preguntas);
                                        var preguntaSiguiente = paginaclickeada;
                                        var Respuesta = $('#respuestaSelect'+trivia+''+(preguntaActual)).val();
                                        respuestasTrivia[preguntaActual-1]= Respuesta;
                                        //alert("el ctn actual es"+"#ctn"+trivia+''+preguntaActual+". El que sigue es: "+"#ctn"+trivia+''+preguntaSiguiente);
                                        //ocultar todo
                                        var i = 0;
                                        for (i = 0; i<total_preguntas; i++)
                                        {
                                            $("#ctn"+trivia+''+i).hide();//lo oculto
                                            document.getElementById('ctn'+trivia+''+i).hidden = true;
                                        }

                                        $("#ctn"+trivia+''+preguntaActual).hide();//lo oculto
                                        document.getElementById('ctn'+trivia+''+preguntaActual).hidden = true;
                                        $("#ctn"+trivia+''+preguntaSiguiente).show();//muestro el siguiente (Que es pregunta actual mas uno)
                                        document.getElementById('ctn'+trivia+''+preguntaSiguiente).hidden = false;
                                        $("#Paginas"+trivia).show();//seguimos mostrando el paginador de la trivia
                                        document.getElementById('Paginas'+trivia).hidden = false;
                                         $("#Paginasalt"+trivia).show();//seguimos mostrando el paginador de la trivia
                                        document.getElementById('Paginasalt'+trivia).hidden = false;
                                        $("#keywords"+trivia).hide();//ocultamos las palabras clave
                                        //document.getElementById('keywords'+trivia).hidden = true;
                                        $(document.getElementById("PaginaLabel"+trivia+""+preguntaActual)).removeClass('active'); //cambiamos el boton activo 
                                        $(document.getElementById("PaginaLabel"+trivia+""+preguntaActual)).addClass('passed ');
                                        $(document.getElementById("PaginaLabel"+trivia+""+preguntaSiguiente)).addClass('active');

                                        //cambiamos el title
                                        $('#modal-trivia-titulo'+trivia).hide();
                                        document.getElementById('modal-trivia-titulo-alt'+trivia).hidden = false;
                                        $("#modal-trivia-titulo-alt"+trivia).show();
                                        preguntaActual = preguntaSiguiente; //decimos que pregunta actual sera la que sigue
                                        
                                }
                                function display(obj)
                                {

                                    var trivia =  document.getElementById('TriviaElegida').value;
                                    document.getElementById('respuestaSelect'+trivia+''+preguntaActual).value=obj.value;
                                    //document.getElementById('respuestaSelectTxt'+trivia+''+preguntaActual).value=ansLabel;
                                    var Respuesta = $('#respuestaSelect'+trivia+''+(preguntaActual)).val();
                                        respuestasTrivia[preguntaActual-1]= Respuesta;
                                    if (preguntaActual != total_preguntas)
                                    {
                                        changeContent();
                                    }
                                    else
                                    {
                                        viewResults();
                                    }
                                    
                                }
                                
                                function viewResults()
                                {
                                    var trivia =  document.getElementById('TriviaElegida').value;
                                    if (respuestasTrivia.length > 0)
                                    {
                                        
                                        var triviaId = <?php echo json_encode($trivia['trivia_id']);?>;
                                        var puntaje = 0;
                                        var total_preguntas = respuestasTrivia.length;
                                        var aciertos = 0;
                                        var triviaIMG = <?php echo json_encode($trivias_list['trivia']['trivia_name']);?>;
                                        //alert("triviaId: "+triviaId);

                                        for (var i = 0; i<respuestasTrivia.length; i++)//para cada respuesta
                                        {
                                            if (respuestasTrivia[i].split("|")[1] == 1)//checamos que sea correcta
                                            {
                                                puntaje = puntaje + parseInt(respuestasTrivia[i].split("|")[2]);//acumulamos los valores
                                                aciertos++;
                                            }
                                        }
                                        var porcentaje_calif = (aciertos*100)/total_preguntas;
                                        //alert("Si respondo, el score es: "+aciertos+ " de "+total_preguntas+" un "+porcentaje_calif+"%correcto.");
                                       

                                        var data = {triviaid: triviaId, score: puntaje, like: 0, time: '00:03:15',name: 'Johnny Bravo'};
                                        if (porcentaje_calif >= 0 && porcentaje_calif < 10)
                                        {
                                            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">Este no es tu tema, intenta con otra trivia.</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
                                        }
                                        if (porcentaje_calif >= 10 && porcentaje_calif < 20)
                                        {
                                            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">Buen intento.</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
                                        }
                                        if (porcentaje_calif >= 20 && porcentaje_calif < 40)
                                        {
                                            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">Buen intento.</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
                                        }
                                        if (porcentaje_calif >= 40 && porcentaje_calif < 60)
                                        {
                                            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">¡Bien hecho!</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
                                        }
                                        if (porcentaje_calif >= 60 && porcentaje_calif < 80)
                                        {
                                            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">¡Bien hecho!</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
                                        }
                                        if (porcentaje_calif >= 80 && porcentaje_calif < 100)
                                        {
                                            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">¡Muy bien!</p><p class="result-gral-trivia">Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.</p>';
                                        }
                                        if (porcentaje_calif == 100)
                                        {
                                            document.getElementById('resultado'+trivia).innerHTML = '<p class="congratulation-trivia">¡Excelente trabajo, felicitaciones!</p><p class="result-gral-trivia">Tuviste todas las respuestas correctas.</p>';
                                        }

                                        var labelAns=[];
                                        var respuestasContent = '<div class= "tabla-result"><center><table >';
                                        for (var i = 0; i<respuestasTrivia.length; i++)//para cada respuesta
                                        {
                                            if (respuestasTrivia[i].split("|")[1] == 1)
                                            {
                                                labelAns[i] = respuestasTrivia[i].split("|")[3];//pintamos los enunciados
                                                respuestasContent = respuestasContent + '<tr valign="middle"><td><span class="trivia-results-ok">'+labelAns[i]+'</span></td><td><img src="assets/images/backend/correct.png"></td><td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td></tr>';
                                            }
                                            else
                                            {
                                                labelAns[i] = respuestasTrivia[i].split("|")[3];//pintamos los enunciados
                                                respuestasContent = respuestasContent + '<tr valign="middle"><td><span class="trivia-results-mal">'+labelAns[i]+'</span></td><td><img src="assets/images/backend/incorrect.png"></td><td><span class="trivia-results-neutro">Respuesta correcta: '+respuestasTrivia[i].split("|")[4]+'</span></td></tr>';
                                            }
                                            
                                        }
                                        
                                        respuestasContent = respuestasContent + '</table></center></div>'
                                        /*var respuestasContent = '<center><p class="trivia-results-title">Tus respuestas<p></center>';

                                        for (var i = 0; i<respuestasTrivia.length; i++)//para cada respuesta
                                        {
                                            if (respuestasTrivia[i].split("|")[1] == 1)
                                            {
                                                labelAns[i] = respuestasTrivia[i].split("|")[3];//pintamos los enunciados
                                                respuestasContent = respuestasContent + '<div class="row-results"><center><div class="col-left"><span class="trivia-results-ok">'+labelAns[i]+'</span></div><div class="col-cen"><img src="assets/images/backend/correct.png"></div><div class="col-right">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</div></center></div>';
                                            }
                                            else
                                            {
                                                labelAns[i] = respuestasTrivia[i].split("|")[3];//pintamos los enunciados
                                                respuestasContent = respuestasContent + '<div class="row-results"><center><div class="col-left"><span class="trivia-results-mal">'+labelAns[i]+'</span></div><div class="col-cen"><img src="assets/images/backend/incorrect.png"></div><div class="col-right"><span class="trivia-results-neutro">Respuesta correcta: '+respuestasTrivia[i].split("|")[4]+'</span></div></center></div>';
                                            }
                                            
                                        }*/
                                        //respuestasContent = respuestasContent + "</div>";
                                        var titulo = 'Tuviste '+aciertos+' de '+total_preguntas+' respuestas correctas.';
                                        document.getElementById('respuestas-fin'+trivia).innerHTML = respuestasContent;

                                        /*document.getElementById('resultado'+trivia).innerHTML =  '<center>'+
                                                                                              + '<p><p class="share-results-trivia">Comparte tus resultados</p>'
                                                                                              //+ '<img onclick="shareFB(&quot;'+titulo+'&quot;,&quot;'+"Descripcion"+'&quot;,&quot;'+<?php echo $trivia['books'][0]['imagen'];?>+'&quot;,&quot;http://librosmexico.mx/trivias&quot;)" src="assets/images/detail/facebook_cita.svg" class="social-medios-mini pointer" style="cursor:pointer!important;">&nbsp'+
                                                                                              + '&nbsp<img src="assets/images/detail/twitter_cita.svg" class="social-medios-mini pointer" style="cursor:pointer!important;">'
                                                                                              + '</p>'
                                                                                              + '</center>';*/
                                        //trivia =  parseInt(document.getElementById('TriviaElegida').value);
                                        
                                        trivia = trivia -1;
                                        //alert(trivia);
                                        var triviasList = <?php echo json_encode($trivias_list);?>;
                                        
                                        triviaId = triviasList[trivia]['trivia_id'];
                                        var titulofb = triviasList[trivia]['trivia_name'];
                                        var descfb = 'Obtuve '+aciertos+' aciertos de '+total_preguntas+' preguntas contestando la trivia '+triviasList[trivia]['trivia_name']+' en LIBROSMEXICO.MX';
                                        var imglibrofb = "";
                                        
                                        if (triviasList[trivia]['books'][0] != null)
                                        {
                                            //alert(triviasList[trivia]['books'][0]['imagen']);
                                            imglibrofb = triviasList[trivia]['books'][0]['imagen'];
                                        }
                                        else
                                        {
                                            //alert("No existe");
                                            imglibrofb="http://cdn.librosmexico.mx/assets/images/generic_book.jpg";
                                        }


                                        var titulotw = 'Obtuve '+aciertos+' aciertos de '+total_preguntas+' preguntas contestando la trivia '+triviasList[trivia]['trivia_name']+' en LIBROSMEXICO.MX';


                                        var sharecontent= ' <center>'+
                                                          '<p><p class="share-results-trivia">Comparte tus resultados</p>'+
                                                          '<img onclick="shareFB(&quot;'+titulofb+'&quot;,&quot;'+descfb+'&quot;,&quot;'+imglibrofb+'&quot;,&quot;http://librosmexico.mx/trivias&quot;)" src="assets/images/detail/facebook_cita.svg" class="social-medios-mini pointer" style="cursor:pointer!important;">'+
                                                          '&nbsp<img onclick="shareTW(&quot;http://librosmexico.mx/trivias&quot;,&quot;'+descfb+'&quot;)" src="assets/images/detail/twitter_cita.svg" class="social-medios-mini pointer" style="cursor:pointer!important;">'+
                                                          '</p>'+
                                                          '</center>';

                                        document.getElementById('sharebox'+(trivia+1)).innerHTML = sharecontent;


                                        
                                    }
                                    else
                                    {
                                        //alert("Si respondo, <br>No hay respuestas porque no hay pregutnas");
                                        var triviaId = <?php echo json_encode($trivias_list);?>;

                                        var data = {triviaid: triviaId, score: puntaje, like: 0, time: '00:00:00',name: 'Johnny Bravo'};
                                        document.getElementById('resultado'+trivia).innerHTML = '<ol><li>No hubo preguntas</li></ol>';
                                        preguntaActual = 0;
                                    }
                                    trivia = trivia +1;

                                    $("#ctn"+trivia+''+preguntaActual).hide();
                                    document.getElementById('ctn'+trivia+''+preguntaActual).hidden = true;
                                    $("#ctnfinal"+trivia).show();
                                    document.getElementById('ctnfinal'+trivia).hidden = false;
                                    $("#fin"+trivia).show();
                                    document.getElementById('fin'+trivia).hidden = false;
                                    $("#next"+trivia).hide();
                                    document.getElementById('next'+trivia).hidden = true;
                                    document.getElementById('Paginasalt'+trivia).hidden = true;
                                    $("#Paginasalt"+trivia).hide();
                                    onClickFin();
                                    reset();
                                }

                                function select_trivia(triviapos)
                                {
                                        reset();
                                        document.getElementById('TriviaElegida').value=parseInt(triviapos);

                                }


                                function onClickFin()
                                {
                                    if (respuestasTrivia.length > 0)
                                    {
                                        
                                        var trivia =  parseInt(document.getElementById('TriviaElegida').value);
                                        trivia = trivia -1;
                                        var triviasList = <?php echo json_encode($trivias_list);?>;
                                        triviaId = triviasList[trivia]['trivia_id'];

                                        var puntaje = 0;
                                        var aciertos = 0;
                                        for (var i = 0; i<respuestasTrivia.length; i++)//para cada respuesta
                                        {
                                            if (respuestasTrivia[i].split("|")[1] == 1)//checamos que sea correcta
                                            {
                                                puntaje = puntaje + parseInt(respuestasTrivia[i].split("|")[2]);//acumulamos los valores
                                                aciertos++;
                                            }
                                        }
                                        //alert("Si respondo, <br>Las respuestas son: "+respuestasTrivia);

                                        var data = {triviaid: triviaId, score: puntaje, like: 0, time: '00:03:15'};
                                        postajax('trivias/contestar_commit', data);
                                    }
                                    else
                                    {
                                        //alert("Si respondo, <br>No hay respuestas porque no hay pregutnas");
                                        var trivia =  parseInt(document.getElementById('TriviaElegida').value);
                                        trivia = trivia -1;
                                        var triviasList = <?php echo json_encode($trivias_list);?>;
                                        triviaId = triviasList[trivia]['trivia_id'];

                                        var data = {triviaid: triviaId, score: 0, like: 0, time: '00:00:00'};
                                        preguntaActual = 0;
                                        //post('trivias/contestar_commit', data);
                                        postajax('trivias/contestar_commit', data);

                                    }
                                    
                                }

                                function post(path, params, method) 
                                {
                                    method = method || "post"; // Set method to post by default if not specified.

                                    // The rest of this code assumes you are not using a library.
                                    // It can be made less wordy if you use one.
                                    var form = document.createElement("form");
                                    form.setAttribute("method", method);
                                    form.setAttribute("action", path);

                                    for(var key in params) 
                                    {
                                        if(params.hasOwnProperty(key)) 
                                        {
                                            var hiddenField = document.createElement("input");
                                            hiddenField.setAttribute("type", "hidden");
                                            hiddenField.setAttribute("name", key);
                                            hiddenField.setAttribute("value", params[key]);

                                            form.appendChild(hiddenField);
                                        }
                                    }

                                    document.body.appendChild(form);
                                    form.submit();
                                }
                                
                                function postformsearch(path, params, method)
                                {
                                    var form = document.createElement("form");
                                    form.setAttribute("method", method);
                                    form.setAttribute("action", path);

                                    for(var key in params) 
                                    {
                                        if(params.hasOwnProperty(key)) 
                                        {
                                            var hiddenField = document.createElement("input");
                                            hiddenField.setAttribute("type", "hidden");
                                            hiddenField.setAttribute("name", key);
                                            hiddenField.setAttribute("value", params[key]);

                                            form.appendChild(hiddenField);
                                        }
                                    }

                                    document.body.appendChild(form);
                                    form.submit();
                                }

                                function postajax(path, params)
                                {
                                    event.preventDefault();
                                    var request = $.ajax({
                                            'url': path,
                                            'type':'POST',
                                            'data': params,
                                            'async': true,
                                            'dataType': "json"
                                        });
                                    return false;
                                }

                                function shareFB(title, desc, img, url)
                                {
                                    var product_name   =    title;
                                    var description    =    desc;
                                    var share_image    =    img;
                                    var share_url      =    url;
                                    var share_caption  =    'librosmexico.mx';
                                    FB.ui({
                                        method: 'feed',
                                        appId: '394615860726938',   
                                        name: title,
                                        link: share_url,
                                        picture: share_image,
                                        caption : share_caption,
                                        description: description
                                        }, function(response) {
                                            if(response && response.post_id){
            
                                            }
                                            else{}
                                        }); 
                                }

                                function shareTW(url, titulo)
                                {
                                    //titulo = titulo + ' en LIBROSMEXICO.MX';
                                    var params = {
                                                    access_token: "65efccf36ae26915b2362f75c2b09b8856732202",
                                                    longUrl: url,
                                                    format: 'json'
                                                 };
                                    $.getJSON('https://api-ssl.bitly.com/v3/shorten', params, function (response, status_txt) {
                                    var urlbit =response.data.url;
                                    var len = 140 - (urlbit.length)+7;  

                                    if (titulo.length>len) 
                                    {
                                        titulo = titulo.substring(0,len);
                                        titulo = '"'+titulo+'..."';
                                    } 
                                    else 
                                    {
                                        titulo = '"'+titulo+'"';
                                    }
                                    var device = navigator.userAgent;
                                    var shareURL = 'http://twitter.com/intent/tweet?text='+titulo+' '+urlbit;

                                    if (device.match(/Iphone/i)|| device.match(/Ipod/i)|| device.match(/Android/i)|| device.match(/J2ME/i)|| device.match(/BlackBerry/i)|| device.match(/iPhone|iPad|iPod/i)|| device.match(/Opera Mini/i)|| device.match(/IEMobile/i)|| device.match(/Mobile/i)|| device.match(/Windows Phone/i)|| device.match(/windows mobile/i)|| device.match(/windows ce/i)|| device.match(/webOS/i)|| device.match(/palm/i)|| device.match(/bada/i)|| device.match(/series60/i)|| device.match(/nokia/i)|| device.match(/symbian/i)|| device.match(/HTC/i)){
                                        location.target="_newtab";
                                        location.href = shareURL; 
                                    }   
                                    else 
                                    {
            
                                        window.open('http://twitter.com/intent/tweet?text='+titulo+' '+urlbit,"_blank","top=200, left=500, width=400, height=400");
                                    }
            
                                    });
                                }

                                function likeItList(id)
                                {
                                    var ban;
                                    var count;
                                    ban = $(".list-like-"+id).data('like');
                                    count = parseInt($('.like-count-'+id).data('count'));
                                    $.ajax({
                                        url: "like-list",
                                        method: "POST",
                                        data:{list: id,
                                             option: ban}
                                    }).done(function(){
                                        if(ban=="0"){
                                            count = count +1; 
                                            $('.list-like-'+id).text('Ya no me gusta');
                                            $('.list-like-'+id).data('like','1');   

                                            $('.like-count-'+id).data('count',count);   
                                            $('.like-count-'+id).text(' | '+count+' me gusta'); 
                                        }
                                    else{ 
                                            count = count -1;       
                                            $('.list-like-'+id).text('Me gusta');
                                            $('.list-like-'+id).data('like','0');   
                                            $('.like-count-'+id).data('count',count);       
                                            if(count<=0)
                                                 $('.like-count-'+id).text('');
                                            else    
                                                $('.like-count-'+id).text(' | '+count+' me gusta');             
                                        }
                                     });
                                }