var spinnerOpts = {
  lines: 7 // The number of lines to draw
  , length: 0 // The length of each line
  , width: 3 // The line thickness
  , radius: 4 // The radius of the inner circle
  , scale: 1 // Scales overall size of the spinner
  , corners: 1 // Corner roundness (0..1)
  , color: '#FFF' // #rgb or #rrggbb or array of colors
  , opacity: 0.25 // Opacity of the lines
  , rotate: 0 // The rotation offset
  , direction: 1 // 1: clockwise, -1: counterclockwise
  , speed: 1 // Rounds per second
  , trail: 60 // Afterglow percentage
  , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
  , zIndex: 2e9 // The z-index (defaults to 2000000000)
  , top: '50%' // Top position relative to parent
  , left: '50%' // Left position relative to parent
  , shadow: false // Whether to render a shadow
  , hwaccel: false // Whether to use hardware acceleration
  , position: 'absolute' // Element positioning
}
/**
 * Follow a user
 * @param  {int} id of the user that I'm sending the request
 * @param  {object} e  button information
 */
function follow_user(id, e) {
  $(e).prop("disabled", true);
  var spinner = $(e).find(".btn-spinner");
  $(spinner).spin(spinnerOpts);
  $.ajax({
    url: "follow-user",
    type: "post",
    data: { following_id: id },
    dataType: "json",
    success: function (data) {
      if (data["status"] == "SUCCESS") {
        $(e).addClass("success");
        if (data['relationship_status'] == 'aceptado') {
          $(e).find(".btn-label").text("Siguiendo");  
        } else {
          $(e).find(".btn-label").text("En espera");  
        }
      } else if (data["status"] == "ERROR") {
        $(e).prop("disabled", false);
      }
      $(spinner).spin(false);
    }
  });
}

/**
 * Acepta la solicitud de seguimiento
 * @param  {int} id of the user that I'm sending the request
 * @param  {object} e  button information
 */
function accept_following(id, e) {
  $(e).prop("disabled", true);
  var spinner = $(e).find(".btn-spinner");
  $(spinner).spin(spinnerOpts);
  $.ajax({
    url: "accept-follow-user",
    type: "post",
    data: { userfront_id: id },
    dataType: "json",
    success: function (data) {
      if (data["status"] == "SUCCESS") {
        $(e).addClass("success");
        $(e).find(".btn-label").text("Solicitud aceptada");
        $(e).parent().find('.btn-reject').hide();
      } else if (data["status"] == "ERROR") {
        $(e).prop("disabled", false);
      }
      $(spinner).spin(false);
    }
  });
}

/**
 * Rechaza la solicitud de seguimiento
 * @param  {int} id of the user that I'm sending the request
 * @param  {object} e  button information
 */
function reject_following(id, e) {
  $(e).prop("disabled", true);
  var spinner = $(e).find(".btn-spinner");
  $(spinner).spin(spinnerOpts);
  $.ajax({
    url: "reject-follow-user",
    type: "post",
    data: { userfront_id: id },
    dataType: "json",
    success: function (data) {
      if (data["status"] == "SUCCESS") {
        $(e).addClass("success");
        $(e).find(".btn-label").text("Solicitud rechazada");
        $(e).parent().find('.btn-accept').hide();
      } else if (data["status"] == "ERROR") {
        $(e).prop("disabled", false);
      }
      $(spinner).spin(false);
    }
  });
}



/**
 * Crea el boton para seguir a una persona
 */
function create_follow_button () {
  if ($('.follow-button-snippet').length > 0) {
    $('.follow-button-snippet').each(function(i, element) {
      var userfront_id = $(element).data('userfront_id');
      if (userfront_id) {
        $.ajax({
          url: "relationship/get_relationship_status",
          type: "post",
          data: { following_id: userfront_id },
          dataType: "json",
          success: function (data) {
            if (data.status == 'SUCCESS') {
              var html = '';
              if (data.relationship_status == 'sin_relacion' || data.relationship_status == 'te_sigue') {
                html = '<button class="btn-anadir btn-small" onclick="follow_user(' + userfront_id + ', this);"> <span class="btn-label">Seguir</span><span class="btn-spinner"></span> </button>';
              } else if (data.relationship_status == 'le_sigues' || data.relationship_status == 'son_amigos') {
                html = '<p class="following">Siguiendo</p>';
              } else if (data.relationship_status == 'en_espera') {
                html = '<p class="following_waiting_approval">Solicitud de seguimiento enviada</p>';
              }
              $(element).empty();
              $(element).html(html);
            }
          }
        });
      }
    });
  }
}

$(function() {
  create_follow_button();
});