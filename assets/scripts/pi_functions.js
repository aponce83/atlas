$( document ).ready(function() {

	$('.list-desc').hide();

    $('#addList').change(function(){
    	if( $(this).val() == 'new_list' )
    	{
    		$('.add-list-btn-wrp').hide();
    		$('.create-list-btn-wrp').fadeIn();

    		$('.list-desc').hide();
    	}
    	else
    	{
    		$('.create-list-btn-wrp').hide();
    		$('.add-list-btn-wrp').fadeIn();

    		$('.list-desc').hide();
    		var currentID = $(this).val();
    		$('#listDesc'+ currentID).show();
    	}
    });
});