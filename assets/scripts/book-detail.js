function setRank(rank){
	var user_id = $('#actual-user-id').val();
	user_id = $.trim(user_id);
	if (user_id.length <= 0) {
		return;
	}
	$(".stch").each(function(){
		$(this).removeClass("stch");
	});
	var book = $('#actual-book').val();
	$.ajax({
		  url: "rank_book",
		  method: "POST",
		  data:{ranking: rank,
		  		book:book}
		}).done(function(data){
			console.log(JSON.stringify(data));
			var new_avg_rank = JSON.stringify(data);
			new_avg_rank = new_avg_rank.replace(/"/g, "");
			new_avg_rank = replaceAll(new_avg_rank,"\\", "");
			$('.rate').text(new_avg_rank);
			// Marca todas las estrellas que aparecen en la vista
			$('input.star-'+rank).prop('checked', true);
			loadLastRatings();
	});
}

function likeIt(id){
	var ban;
	var count;
	$('.comment-'+id).each(function(){
		ban = $(this).data('like');
	});
	$('.like-count-'+id).each(function(){
		count = parseInt($(this).data('count'));
	});
	$.ajax({
	  url: "like-comment",
	  method: "POST",
	  data:{comment: id,
	  		option: ban}
	}).done(function(){
		if(ban=="0"){
			count = count +1; 
			$('.comment-'+id).text('Ya no me gusta');
			$('.comment-'+id).data('like','1');	
			$('.like-count-'+id).data('count',count);	
			$('.like-count-'+id).text('| '+count+' me gusta');	
		}
		else{ 
			count = count -1;		
			$('.comment-'+id).text('Me gusta');
			$('.comment-'+id).data('like','0');	
			$('.like-count-'+id).data('count',count);		
			if(count<=0)
				$('.like-count-'+id).text('');
			else	
				$('.like-count-'+id).text('| '+count+' me gusta');				
		}
	});
}

function addComment(){

	var review= $('#comment').val();
	var image;

	if($('#actual-pic').val())
		image = $('#actual-pic').val();		
	else
		image = 'assets/images/detail/usuario-default.svg';

	var book = $('#actual-book').val();
	if (review.length<5)
	{
		$('#comment').css('border','1px solid #cc4242');
	}
	else{	
		var rank = $('.rate').text();
		if(rank!=""){
			rank = rank.replace(/"/g, "");
			rank = replaceAll(rank,"\\", "");
		}
		$.ajax({
		  url: "review_book",
		  method: "POST",
		  data:{book_id: book,
		  		comment: review}
		}).done(function(data){

		var id= JSON.stringify(data);
		id = id.replace(/"/g, "");
		id = replaceAll(id,"\\", "");

		var index = 5;
		var name=$('#actual-user').val();

		var f = new Date();
		var date = f.getDate() + "/" + (f.getMonth()+1) + "/" + f.getFullYear()	
		var comment;
		comment = '<div class="col-xs-12 people-review"><div class="col-xs-2 div-photo"><img src='+image+' class="img-circle people-photo"/></div>';
		comment += '<div class="col-xs-10 desk-mini no-padding"><div class="col-xs-10 star-rate-desk no-padding"><span style="float:left"><a class="profile">'+name+'</a><span class="rate-width">lo calificó con </span>';
		for(index=5;index>0;index--)
		{
			if(index<=rank){
				comment+='<input class="check star star-'+index+'" id="star-'+index+'" type="radio" name="star"/>';
				comment+='<label class="check star star-'+index+'" for="star-'+index+'"></label>';
			}
			else{
				comment+='<input class="star star-'+index+'" id="star-'+index+'" type="radio" name="star"/>';
				comment+='<label class="star star-'+index+'" for="star-'+index+'"></label>';
			}
		}
		comment += '</span></div><div class="col-xs-2 no-padding time-ago">ahora</div><div class="col-xs-12 no-padding review-book">'+review+'</div><div class="col-xs-12 no-padding">';								
		comment += '<span class="pointer btn-like comment-'+id+'" onclick="likeIt('+id+')" data-like="0">Me gusta</span>';
		comment += 	'<span class="like-count-'+id+'" data-count="0"></span>';
		comment += '</div></div><div class="col-sm-10 col-xs-8 rank-mini"><div class="col-xs-12 no-padding word-wrap" ><a class="profile">'+name+'</a><span class="rate-width">lo calificó con </span></div>';
		comment += '<div class="col-xs-12 star-rate-desk no-padding" style="margin-bottom:5px;"><span style="float:left">';
		for(index=5;index>0;index--)
		{
			if(index<=rank){
				comment+='<input class="check star star-'+index+'" id="star-'+index+'" type="radio" name="star"/>';
				comment+='<label class="check star star-'+index+'" for="star-'+index+'"></label>';
			}
			else{
				comment+='<input class="star star-'+index+'" id="star-'+index+'" type="radio" name="star"/>';
				comment+='<label class="star star-'+index+'" for="star-'+index+'"></label>';
			}
		}
		comment += '</span></div></div><div class="col-xs-12 rank-mini review-book">'+review+'</div><div class="col-xs-12 rank-mini like">';
		comment += '<span class="pointer btn-like comment-'+id+'" onclick="likeIt('+id+')" data-like="0">Me gusta</span>';
		comment += 	'<span class="like-count-'+id+'" data-count="0"></span>';
		comment += '</div></div>';

		$(comment).insertAfter("#div-review");
		$("#comment").val("");
		$('#comment').css('border','1px solid #3db29c');
		setNotify('Se agregó tu reseña',exito);
		}).fail(function(){
			setNotify('Ocurrio un Error',error);
		});
	}
}

function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}

function addToList(list,index){
	var book = $('#actual-book').val();
	$.ajax({
		  url: "add_to_list",
		  method: "POST",
		  data:{book_id: book,
		  		list:list}
		}).done(function(){
			$("#add-to-my-library").children("#p"+index).children("label").attr('onclick','setChecked('+index+');removeToList('+list+',"'+index+'")');		
			addLibraryOutMini();
			setNotify('El libro se ha agregado correctamente a tu lista.', exito);
		}).fail(function() { setNotify('Hubo un problema al agregar el libro, intenta más tarde.', error);
	});	
}
function removeToList(list,index){
	var book = $('#actual-book').val();
	$.ajax({
		  url: "remove_to_list",
		  method: "POST",
		  data:{book_id: book,
		  		list:list}
		}).done(function(){
			$("#add-to-my-library").children("#p"+index).children("label").attr('onclick','setChecked('+index+');addToList('+list+',"'+index+'")');			
			addLibraryOutMini();
			setNotify('El libro se ha eliminado correctamente de tu lista.', exito);
		}).fail(function() { setNotify('Hubo un problema al eliminar el libro, intenta más tarde.', error);
	});		
}
function createMiniList(index){
	var name=$('#list-name-mini').val();
	if($('#list-name-mini').val().length < 5)
	   $('#list-name-mini').css('border','1px solid #cc4242');
	else{
		$.ajax({
			  url: "ajax_create_list",
			  method: "POST",
			  data:{name:$('#list-name-mini').val(),
					type: "mini"
				}
			}).done(function(data){
				id = JSON.stringify(data);
				id = id.replace(/"/g, "");
				id = replaceAll(id,"\\", "");

				$('#add-to-my-library').children(".remove").remove();
				var num = $('#add-to-my-library').children("p").length-2;
				var paraph="<p id='p"+num+"' class='check-it' style='word-wrap: break-word;'><input  class='tick' id='check-"+num+"' type='checkbox' checked/><label  onclick='setChecked("+num+"); removeToList("+id+","+num+")' id='l"+num+"'class='tick' for='check-"+num+"')'></label><span style='word-wrap: break-word;'>"+name+"</span></p>";
				num = num-1;		
				$(paraph).insertAfter("#p"+num);		
				addToList(id,num);				
				setNotify('Tu lista se ha creado correctamente.', exito);		
		});

	  $('#list-name-mini').val("");
	  $('#list-name-mini').css('border','1px solid #3db29c');
	}
}

function menuActive(id,info,hide){
	$("a").removeClass('active');
	$("#"+id).addClass('active');
	$("."+info).css('display','inline-block');
	$("."+info).show();
	$("."+info).css('display','inline-block');	
	$("."+hide).hide();
}
function miniToggle(id,mini,image)
{
	$("#"+id).toggle();
	if ($("#"+id).is(":visible")){
		$("."+mini).css("borderBottom","1px solid #cc4242");
		$("#"+image).attr("src","assets/images/detail/flecha_roja_arriba.svg");
	}		
	else{
		$("."+mini).css("borderBottom","1px solid #dedddd");
		$("#"+image).attr("src","assets/images/detail/flecha_gris.svg");	
	}

}
function newList(){
	$(".text-list").toggle();
	if ($(".text-list").is(":visible"))
		$("#create-list").addClass("check-it");
	else
		$("#create-list").removeClass("check-it");		
}

function borderHover(){
	$(".book-review-content").animate({borderColor:"#3db29c"},0);
	$(".review-publish").animate({borderColor:"#3db29c"},400);
}

function borderHoverOut(){
	$(".book-review-content").animate({borderColor:"#dedddd"},0);
	$(".review-publish").animate({borderColor:"#dedddd"},400);
}

function setChecked(id){
	if ($("#check-"+id).is(":checked")) 	
		$("#p"+id).removeClass("check-it");
	else 
		$("#p"+id).addClass("check-it");
}

function hoverCheck(id){
	$("#l"+id).animate({content:"url(assets/images/detail/checkbox_hover.svg)"},0);
}

function hoverCheckOut(id){
	$("#l"+id).css("content","url(assets/images/detail/checkbox.svg)");
}

function addLibrary(){
	$( "#btn-add" ).animate({
		borderRadius: "10px",
		backgroundColor:"#354f5e",
		color: "#3db29c",
		zIndex: "99",
		position: "absolute"
	  }, 0);
	$(".add").hide();
	$(".add-list").show();
}

function addLibraryOut(){
	$( "#btn-add" ).animate({
		padding:"8px 10px 8px 10px",
		borderRadius: "30px",
		backgroundColor: "#FFF",
		borderColor: "#354f5e",
		border:"1px solid #354f5e",		
		color: "#354f5e",
		width: "175px"

	  }, 0);
	$(".add").show();
	$(".add-list").hide();
}
function addLibraryMini(){
	$( "#btn-add-mini" ).animate({
		borderRadius: "10px",
		backgroundColor:"#354f5e",
		color: "#3db29c",
		zIndex: "99",
		position: "absolute"
	  }, 0);
	$(".add-mini").hide();
	$(".add-list-mini").show();
}

function addLibraryOutMini(){
	$( "#btn-add-mini" ).animate({
		padding:"8px 10px 8px 10px",
		borderRadius: "30px",
		backgroundColor: "#FFF",
		borderColor: "#354f5e",
		border:"1px solid #354f5e",		
		color: "#354f5e",
		width: "100%"
	  }, 0);
	$(".add-mini").show();
	$(".add-list-mini").hide();
}

function toggleDet(){
	$(".detail-hidden").toggle();
	if ($(".detail-hidden").is(":visible"))
		$("#ch-see-more").text("ver menos ");
	else
		$("#ch-see-more").text("ver más ");	
}
function toggleDetMini(){
	$(".detail-hidden").toggle();
	if ($(".detail-hidden").is(":visible"))
		$("#ch-see-more-mini").text("ver menos ");
	else
		$("#ch-see-more-mini").text("ver más ");	
}
function clickCopy(){
	var ref = $(".get-cop option:selected").val();
	if(ref!='0'){
		location.target='_newtab';		
		location.href=ref;
	}
} 
function displayReviews(){
 $('.people-review-hide').toggle();
 $('.see-more-review ').remove();
} 

function clickCopyMini(){
	var ref = $(".copy-mini option:selected").val();
	if(ref!='0'){
		location.target='_newtab';
		location.href=ref;
	}
} 


function newListMini(){
	$(".text-list-mini").toggle();
	if ($(".text-list-mini").is(":visible"))
		$("#create-list-mini").addClass("check-it");
	else
		$("#create-list-mini").removeClass("check-it");		
}
function createMiniListMini(index){
	var name=$('#list-name-min').val();
	if($('#list-name-min').val().length < 5)
	   $('#list-name-min').css('border','1px solid #cc4242');
	else{
		$.ajax({
			  url: "ajax_create_list",
			  method: "POST",
			  data:{name:$('#list-name-min').val(),
					type: "mini"}
			}).done(function(data){
				id = JSON.stringify(data);
				id = id.replace(/"/g, "");
				id = replaceAll(id,"\\", "");
				$('#add-to-my-library-mini').children(".remove").remove();
				var num = $('#add-to-my-library-mini').children("p").length-2;
				var paraph="<p id='p"+num+"mini' class='check-it' style='word-wrap: break-word;'><input class='tick' id='check-"+num+"mini' type='checkbox' checked/><label class='tick'  onclick='setChecked(\""+num+"mini\"); removeToList("+id+",'"+num+"mini')' id='lm"+num+"' for='check-"+num+"mini''></label><span style='word-wrap: break-word;'>"+name+"</span></p>";
				num = num-1;		
				$(paraph).insertAfter("#p"+num+"mini");		
		});
	  $('#list-name-min').val("");
	  $('#list-name-min').css('border','1px solid #3db29c');		
	}
}
function shareTW(url, titulo, autor){
	titulo = 'Estoy viendo '+titulo+ ' de '+autor+ ' en LIBROSMEXICO.MX';
    var params = {
        access_token: "65efccf36ae26915b2362f75c2b09b8856732202",
        longUrl: url,
        format: 'json'
    };
    $.getJSON('https://api-ssl.bitly.com/v3/shorten', params, function (response, status_txt) {
        var urlbit =response.data.url;
        var len = 140 - ((urlbit.length)+7);  

		if (titulo.length>len) {
			titulo = titulo.substring(0,len);
			titulo = '"'+titulo+'..."';
		} else {
			titulo = '"'+titulo+'"';
		}

		var device = navigator.userAgent;
		var shareURL = 'http://twitter.com/intent/tweet?text='+titulo+' '+urlbit;

		if (device.match(/Iphone/i)|| device.match(/Ipod/i)|| device.match(/Android/i)|| device.match(/J2ME/i)|| device.match(/BlackBerry/i)|| device.match(/iPhone|iPad|iPod/i)|| device.match(/Opera Mini/i)|| device.match(/IEMobile/i)|| device.match(/Mobile/i)|| device.match(/Windows Phone/i)|| device.match(/windows mobile/i)|| device.match(/windows ce/i)|| device.match(/webOS/i)|| device.match(/palm/i)|| device.match(/bada/i)|| device.match(/series60/i)|| device.match(/nokia/i)|| device.match(/symbian/i)|| device.match(/HTC/i)){
			location.target="_newtab";
			location.href = shareURL; 
		}	else {
			
			window.open('http://twitter.com/intent/tweet?text='+titulo+' '+urlbit,"_blank","top=200, left=500, width=400, height=400");

		}
			
    });


	$.ajax({
            type: "POST",
            url: "sharetw-notify",
            data:{
                        
            }
        }).done(function(){
                    
        }).fail(function(){
                                     
        });



}
function likeItList(id){
	var ban;
	var count;
	ban = $(".list-like-"+id).data('like');
	count = parseInt($('.like-count-'+id).data('count'));

	$.ajax({
	  url: "like-list",
	  method: "POST",
	  data:{list: id,
	  		option: ban}
	}).done(function(){
		if(ban=="0"){
			count = count +1; 
			$('.list-like-'+id).text('Ya no me gusta');
			$('.list-like-'+id).data('like','1');	

			$('.like-count-'+id).data('count',count);	
			$('.like-count-'+id).text(' | '+count+' me gusta');	
		}
		else{ 
			count = count -1;		
			$('.list-like-'+id).text('Me gusta');
			$('.list-like-'+id).data('like','0');	
			$('.like-count-'+id).data('count',count);		
			if(count<=0)
				$('.like-count-'+id).text('');
			else	
				$('.like-count-'+id).text(' | '+count+' me gusta');				
		}
	});
}
/**
 * If this book has an online version, call this function to
 * show it on the PDF
 * @param  {DOMelement} o that triggered this function
 */
function showPdf(o) {
	var url = $(o).data("href");
	var target = $(o).data("target");
	if (navigator.userAgent.match(/Mobi/)) {
		url = "http://pro.librosmexico.mx/adjuntos/"+url;
		window.open(url, '_blank');
		return;
	}

	$(target).modal('show');
	var url = $(o).data("href");
	if ($(target+" iframe").length == 0) {
		$('<iframe>', {
	   src: url,
	   id:  'the-pdf',
	   frameborder: 0,
	   scrolling: 'no',
	   style: "width: 100%; height: 640px"
	   }).appendTo(target + ' .modal-pdf');
	}
	$('.modal .close').on('click', function(e){
	  e.preventDefault();
		$(target).modal('hide');
	});
}
function registerPage(){
	showModalRegister();
}
