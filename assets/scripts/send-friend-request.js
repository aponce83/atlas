var spinnerOpts = {
   lines: 7 // The number of lines to draw
  , length: 0 // The length of each line
  , width: 3 // The line thickness
  , radius: 4 // The radius of the inner circle
  , scale: 1 // Scales overall size of the spinner
  , corners: 1 // Corner roundness (0..1)
  , color: '#FFF' // #rgb or #rrggbb or array of colors
  , opacity: 0.25 // Opacity of the lines
  , rotate: 0 // The rotation offset
  , direction: 1 // 1: clockwise, -1: counterclockwise
  , speed: 1 // Rounds per second
  , trail: 60 // Afterglow percentage
  , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
  , zIndex: 2e9 // The z-index (defaults to 2000000000)
  , top: '50%' // Top position relative to parent
  , left: '50%' // Left position relative to parent
  , shadow: false // Whether to render a shadow
  , hwaccel: false // Whether to use hardware acceleration
  , position: 'absolute' // Element positioning
}
/**
 * Requests friendship
 * @param  {int} id of the user that I'm sending the request
 * @param  {object} e  button information
 */
function requestFriendship(id, e) {
  $(e).prop("disabled", true);
  var spinner = $(e).find(".btn-spinner");
  $(spinner).spin(spinnerOpts);
  $.ajax({
    url: "request-friendship",
    type: "post",
    data: { friend_id: id },
    dataType: "json",
    success: function (data) {
      if (data["status"] == "SUCCESS") {
        $(e).addClass("success");
        $(e).find(".btn-label").text("Solicitud enviada");
      } else if (data["status"] == "ERROR") {
        $(e).prop("disabled", false);
      }
      $(spinner).spin(false);
    }
  });
}
