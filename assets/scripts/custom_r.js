/*
 *=============================================================================
 *========================= Detalles de Lista =================================
 *=============================================================================
*/

function validateFormAddBook(){
  n = $(".chk-add-book > input:checked").length;

  if( n === 0){
    alert('Debe seleccionar al menos un libro');
    return false;
  } else{
    return true;
  }
}

function advanced_search_book( param_val, word_val)
{
  if( typeof xhr != "undefined" ){
    if(xhr && xhr.readyState != 4){
      xhr.abort();
      console.log('cancelar peticion ajax');
    }
  }

  $(".ajax-loader").show();
  $("#no-results").hide();
  $("#see_more_results").hide();
  $("#search_result").html("");
  $("#total-results").html("");

  xhr = $.ajax({
    type: "POST",
    url: 'advanced_search_book',
    dataType: 'json',
    data: { book_param: param_val,
            word_search: word_val},
    success: function(response) {

      if(response == 'error'){
        $("#search_result").html('<div class="list-msg">No hay resultados para el criterio de búsqueda</div>');
        return;
      }

      console.log(response.cuerpo);
      //var resp= $.parseJSON(response);
      var resp = response.cuerpo;

      var row='';
      var autor = '';
      var editorial = '';
      var img = 'assets/images/generic_book.jpg';
      var rating = 0;
      var amarilla = '<img src="assets/images/detail/estrella_amarilla_xm.svg">';
      var gris = '<img src="assets/images/detail/estrella_gris_xm.svg">';
      var visibility = 'showrow';


      for( var i in resp )
      {
        if( typeof resp[i].autores[1] !== 'undefined' )
          autor = resp[i].autores[1][0].nombre;

        if( typeof resp[i].editorial !== 'undefined' )
          editorial = resp[i].editorial[0].nombre;

        img = ( resp[i].imagen !== null ) ? resp[i].imagen : 'assets/images/generic_book.jpg';

        row += '<div class="row  each_list '+ visibility +'">';

        row += '  <div class="col-sm-2  col-xs-3 img_list">';
        row += '    <div class="chk-add-book">';
        row += '      <input type="checkbox" name="new_books[]" value="'+ resp[i].id_libro +'">';
        row += '    </div>';
        row += '    <a href="libros/'+resp[i].id_libro+'">';
        row += '      <img alt="'+ resp[i].titulo +'" src="'+ img +'">';
        row += '    </a>';
        row += '  </div>';

        row += '  <div class="col-sm-10  col-xs-9">';
        row += '      <div class="col-sm-5">';
        row += '        <p class="book_title"><a href="libros/'+resp[i].id_libro+'">'+ resp[i].titulo +'</a></p>';
        row += '        <p class="book_autor">'+ autor +'</p>';
        row += '      </div>';
        row += '      <div class="col-sm-4">';
        row += '        <p class="book_editorial">'+ editorial +'</p>';
        row += '      </div>';
        row += '      <div class="col-sm-3">';
        row += '        <div class="list-rating">';

        //rating = Math.floor((Math.random() * 10) + 1);
        console.log(resp[i].rating);
        rating = resp[i].rating;
        
        row += (rating >= 1) ? amarilla : gris;
        row += (rating >= 2) ? amarilla : gris;
        row += (rating >= 3) ? amarilla : gris;
        row += (rating >= 4) ? amarilla : gris;
        row += (rating >= 5) ? amarilla : gris;
                  
        row += '        </div>';
        row += '      </div>';
        row += '  </div>';
        row += '</div>';

        /*if( i >= 2 )
          visibility = 'hiderow';*/
      }

      var total_resultados = response.encabezado.registros;
      var se_muestran = 30;

      if(total_resultados <= 30){
        se_muestran = total_resultados;
      }else{
        $("#see_more_results").show();
      }
        

      $("#search_result").html(row);
      $("#no-results").hide();

      $("#total-results").html('Se muestran ' + se_muestran + ' resultados de un total de ' + total_resultados);
      $("#see_more_results").attr("href", "buscar/libro?"+param_val+"="+word_val);
    },
    complete: function(){
      $(".ajax-loader").hide();
    },
    error: function(response){
      console.log('Error: ' + response);
      $("#no-results").show();
    },
  });
}

$(document).ready(function(){

  //Limpiar el cuadro modal al abrirse
  $('#addBook').on('show.bs.modal', function (e) {
    $("#add_book_search").val("");
    $("#search_result").html("");
    $("#see_more_results").hide();
    $("#no-results").hide();
    $("#total-results").html("");
  });

  //Mostrar más resultados en el cuadro modal
  $("#see_more_results").click(function(e){
    /*e.preventDefault();
    $(".hiderow").addClass('showrow');
    $(".hiderow").removeClass('hiderow');

    $("#see_more_results").hide();*/
  });

  //Busqueda predictiva para agregar libro
  $("#add_book_search").keypress(function(e) {
    
    var keynum;
    var keychar;
    if(window.event){ // IE
      keynum = e.keyCode;
    }else{ // Netscape/Firefox/Opera
      if(e.which)
        keynum = e.which;
    }
    keychar = String.fromCharCode(keynum); //caracter presionado

    var param_val = $("#search_param_add_book").val();
    var word_val = $(this).val() + keychar;

    if( word_val.length >= 3 ) //hasta el tercer caracter comienza a buscar
    {
      advanced_search_book( param_val, word_val);
    }
  });

  $("#search_param_add_book").change(function(){

    var param_val = $("#search_param_add_book").val();
    var word_val = $("#add_book_search").val();

    if( word_val != '' )
      advanced_search_book( param_val, word_val);

  });

  $("#search_param_add_book").change(function() {
    switch ($(this).val()) {
      case 'titulo':
        $("#add_book_search").attr("placeholder","Busca un libro...");
      break;
      case 'autores':
        $("#add_book_search").attr("placeholder","Busca un autor...");
      break;
      case 'editorial':
        $("#add_book_search").attr("placeholder","Busca una editorial...");
      break;
      case 'isbn':
        $("#add_book_search").attr("placeholder","Busca un ISBN...");
      break;
      case 'keywords':
        $("#add_book_search").attr("placeholder","Escribe una palabra clave...");
      break;
      case 'cadena':
        $("#add_book_search").attr("placeholder","Busca por libro, autor, editorial, ISBN o palabras clave...");
      break;
    }
  });

  //Botón buscar libros con Ajax
  $("#btn_add_book_search").click(function()
  {
    $("#search_result").html("");

    $(".ajax-loader").show();

    var param_val = $("#search_param_add_book").val();
    var word_val = $("#add_book_search").val();


    advanced_search_book( param_val, word_val);

  });
  
  //Eliminar libros (Abrir cuadro modal de confirmación)
  $("#delBook").on('show.bs.modal', function(event){
    //Obtiene los datos del libro(id, título, usuario, slug) del botón para pasarlos al botón eliminar
    var button = $(event.relatedTarget);
    var title = button.data('book-name');
    var id = button.data('bookid');
    var userid = button.data('userid');
    var listslug = button.data('listslug');
    //Los datos recuperados del libro se pasan al botón para armar la url de eliminación
    $(this).find(".del-book-title").text(title);
    $(this).find(".del-book-ok").data('bookid', id);
    $(this).find(".del-book-ok").data('userid', userid);
    $(this).find(".del-book-ok").data('listslug', listslug);
  });

  //Botón para eliminar los libros (envía a la url de eliminación)
  $(".del-book-ok").click(function(){
    var bookid = $(this).data('bookid');
    var userid = $(this).data('userid'); 
    var listslug = $(this).data('listslug');
    
    window.location.href="lista/"+userid+"/"+listslug+"/del/"+bookid;
  });
  
  //Buscar Libros
  $("#search_book").keyup(function( event ){
    table_list.search( this.value ).draw();
  });

  //Ordenar Libros
  $("#list_order, [name='book-order']").change(function(){
    
    var order = $(this).val();
    switch( order )
    {
      case 'num':
        table_list.order( [ 0, 'asc' ] ).draw();
        break;
      case 'title':
        table_list.order( [ 2, 'asc' ] ).draw();
        break;
      case 'editorial':
        table_list.order( [ 3, 'asc' ] ).draw();
        break;
      case 'popular':
        table_list.order( [ 4, 'desc' ] ).draw();
        break;
      case 'autor':
        table_list.order( [ 6, 'asc' ] ).draw();
        break;
      case 'recent':
        table_list.order( [ 7, 'desc' ] ).draw();
        break;
    }
  });

  //Número de libros a mostrar por página 
  $("#list_show, [name='book-show']").change(function(){
    
    var show = $(this).val();
    table_list.page.len( parseInt(show) ).draw();

    $("#info_pag").html( $("#table_list_info").html() );
    $("#info_pag_xs").html( $("#table_list_info").html() );
  });
  
  //Tabla que muestra los libros de la lista
  var table_list = $('#table_list').DataTable({
    responsive: true,
    //"scrollX": true,
    "lengthMenu": [ [5, 10, 20, 30, -1], [5, 10, 20, 30, "Todos"] ],
    //"lengthMenu": false,
    //"searching": false,
    //"ordering": false,
    //"info": false,
    "columnDefs": [ {
      "targets": [0,1,5],
      "orderable": false
    } ],
    "language": {
      "emptyTable":     "No hay datos disponibles en la tabla",
      "info":           " _START_ - _END_ de _TOTAL_ ",
      "infoEmpty":      "Mostrando 0 de 0 de 0 filas",
      "infoFiltered":   "(Filtradas de _MAX_ filas totales)",
      "infoPostFix":    "",
      "thousands":      ",",
      "lengthMenu":     "Mostrar _MENU_ libros",
      "loadingRecords": "Cargando...",
      "processing":     "Procesando...",
      "search":         "Buscar:",
      "zeroRecords":    "No se encontraron registros",
      "paginate": {
          "first":      "Primero",
          "last":       "Último",
          "next":       "Siguiente",
          "previous":   "Anterior"
      },
      "aria": {
          "sortAscending":  ": activate to sort column ascending",
          "sortDescending": ": activate to sort column descending"
      }
    }
  });

  
  //Tabla que muestra los TODOS libros de las listas
  var table_list_all = $('#table_list_all').DataTable({
    responsive: true,
    //"scrollX": true,
    "lengthMenu": [ [5, 10, 20, 30, -1], [5, 10, 20, 30, "Todos"] ],
    //"lengthMenu": false,
    //"searching": false,
    //"ordering": false,
    //"info": false,
    "columnDefs": [ {
      "targets": [0,1,2],
      "orderable": false
    } ],
    "language": {
      "emptyTable":     "No hay datos disponibles en la tabla",
      "info":           " _START_ - _END_ de _TOTAL_ ",
      "infoEmpty":      "Mostrando 0 de 0 de 0 filas",
      "infoFiltered":   "(Filtradas de _MAX_ filas totales)",
      "infoPostFix":    "",
      "thousands":      ",",
      "lengthMenu":     "Mostrar _MENU_ libros",
      "loadingRecords": "Cargando...",
      "processing":     "Procesando...",
      "search":         "Buscar:",
      "zeroRecords":    "No se encontraron registros",
      "paginate": {
          "first":      "Primero",
          "last":       "Último",
          "next":       "Siguiente",
          "previous":   "Anterior"
      },
      "aria": {
          "sortAscending":  ": activate to sort column ascending",
          "sortDescending": ": activate to sort column descending"
      }
    }
  });

  $(".dataTables_length").css("display", "none");
  $(".dataTables_filter").css("display", "none");

  $("#info_pag").html( $("#table_list_info").html() );
  $("#info_pag_xs").html( $("#table_list_info").html() );

  $("#rating_xs").html( $("#rating_sm").html() );
});