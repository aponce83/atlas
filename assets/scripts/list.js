function createList(){
	if(listValidation('-n')){
		$.ajax({
				url: "ajax_create_list",
				method: "POST",
				data:{name:$('#name-list-n').val(),
					description: $('#description-list-n').val(),
					keywords:$('#keywords-list-n').val(),
					privacy: $('#privacy-list-n option:selected').val()} })
			.done(function(data) {
				setNotify('Tu lista se ha creado correctamente.', exito);
				setTimeout(function(){
					location.reload();
				}, 1500);
			})
			.fail(function(){
				setNotify('Hubo un problema al crear la lista, intente más tarde', error);
			});
		$('#create-list').modal('toggle');
		}
}
function createDynamicList(){
	
	if(listValidation()){
		$(".loader-dynamics").show();
		$.ajax({
				url: "ajax_create_list",
				method: "POST",
				data:{name:$('#name-list').val(),
					description: $('#description-list').val(),
					keywords:$('#keywords-list').val(),
					privacy: $('#privacy-list option:selected').val(),
					type: "dynamic",
					params: "" } })
			.done(function(data) {
				setNotify('Tu lista se ha creado correctamente.', exito);
				//location.reload();
				$(".loader-dynamics").hide();
				$('#create-dynamic-list').modal('toggle');
			})
			.fail(function(){
				setNotify('Hubo un problema al crear la lista, intente más tarde', error);
				$(".loader-dynamics").hide();
				$('#create-dynamic-list').modal('toggle');
			});
		}
}

function listValidation(){

  var ban=0;
  var field = $("[id^='name-list']").val();
  if(field.length<3){
    $("[id^='name-list']").css("border","1px solid #cc4242");
    $('#name-list-error').show();
    ban++;
  }
  else{
    $("[id^='name-list']").css("border","1px solid #354f5e");
    $('#name-list-error').hide();
  }
  if(ban!=0)
    return false;
  else
    return true;

}

function deleteListModal(id,name,type)
{
	//Agrega el texto correspondiente al modal de la lista a eliminar
	$('.modal-text').text('¿Estás seguro que quieres eliminar la lista "'+name+'"');
	$('#erase').attr("value",id);
	$('#myModal').modal('toggle');

	if(type=='classic')
		$('#erase').data("type","me-list");
	else
		$('#erase').data("type","genius");
}

function eraseList(val)
{
	$('.'+val).remove();
	$('#myModal').modal('hide');	
	$.ajax({
			  url: "delete_list",
			  method: "POST",
			  data:{list: val}
	}).done(function(data) {
		setNotify('La lista se ha borrado correctamente.', exito);
	})
	.fail(function(){
		setNotify('Ocurrió un problema al borrar la lista, intenta más tarde.', error);
	}); 

	var type = $('#erase').data("type");
	var counter = $('.'+type).length;
	if(counter<=2)
	{		
		$('.'+type+'-more').remove();
	}
	counter = 0;
	$('.'+type+'-wrp').children('.list-item').each(function(){
		if(!$(this).hasClass('list-hidden')){
			counter++;
		}		
		else if($(this).hasClass('list-hidden') && counter<2){
			$(this).removeClass('list-hidden');
			counter++;
		}
	});

}