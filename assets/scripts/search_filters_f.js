/*
 *=============================================================================
 *======================= Resultados de Búsqueda ==============================
 *=============================================================================
*/

var editoriales_Conaculta = "conaculta|SEP / Dirección General de Publicaciones y Bibliotecas|SEP / Dirección General de Publicaciones y Medios|Sep / Dirección General De Divulgación|Secretaría de Cultura / Dirección General de Publicaciones";

$(document).ready(function()
{
    filters_ajax('any');

  //------------Efecto hover de los libros//------------
  $(".search-book-img").mouseenter(function(){
    $(this).find(".book_with_cover").show();
  });
  $(".search-book-img").mouseleave(function(){
    $(".book_with_cover").hide();
  });

  icheck_format(); //Formato de checkboxes
  icheck_authors_filter(); //Funcionalidad de los checkboxes de autor
  icheck_editorials_filter(); //Funcionalidad de los radios de editorial
  icheck_type_filter(); //Funcionalidad de los radios de tipo producto
  icheck_keywords_filter(); //Funcionalidad de los checkboxes de keyword
  icheck_coleccion_filter(); //Funcionalidad de los checkboxes de coleccion
  icheck_price_filter();

  remove_filters_tags(); //Agrega funcionalidad a las etiquetas de filtrado

  $("#show_more_author").hide();
  $("#show_more_editorials").hide();

  $("#show_more_author").click(function( event ){
    event.preventDefault();
    filters_author_ajax(false);
  });

  $("#title-predictive-search").keyup(function(e){
    var key = e.which;
    title = escapeHtml( $(this).val() );
    if( title.length >= 2 ) //hasta el segundo caracter comienza a buscar
    {
      sessionStorage.setItem("titulo", title);

      sessionStorage.setItem("predictive-title", title);

      if( window.innerWidth > 991 ){
        filters_ajax('any');
      }

      if(key == 13){
        sessionStorage.setItem("titulo", title);
        sessionStorage.setItem("pagina", 1); //Restablecer paginación a 1

        url_param = window.location.search.substring(1);
        history.replaceState(null, null, 'buscar/libro/1?'+url_param); //modificar url para que muestre pagina 1

        filters_ajax('any');
      }
    }else{
      $("[data-spantitulo]").remove();  //span:data(spantitulo)
      sessionStorage.setItem("titulo", "");
    }
  });

  $("#author-predictive-search").keyup(function(e){

    var key = e.which;
    autor = escapeHtml( $(this).val() );
    if( autor.length >= 2 ) //hasta el segundo caracter comienza a buscar
    {
      if( sessionStorage.getItem("titulo") == 'e' )
        sessionStorage.setItem("titulo", "");

      //sessionStorage.setItem("autores", autor);
      sessionStorage.setItem("predictive-autores", autor);
      filters_author_ajax();

      if(key == 13){
        sessionStorage.setItem("autores", autor);
        sessionStorage.setItem("pagina", 1); //Restablecer paginación a 1

        url_param = window.location.search.substring(1);
        history.replaceState(null, null, 'buscar/libro/1?'+url_param); //modificar url para que muestre pagina 1

        filters_ajax('autor');
      }
    }
    $("#show_more_author").show();
  });

  $("#show_more_editorials").click(function( event ){
    event.preventDefault();
    filters_editorial_ajax(false);
  });

  $("#editorial-predictive-search").keyup(function(e){

    var key = e.which;
    editorial = escapeHtml( $(this).val() );
    if( editorial.length >= 2 ) //hasta el segundo caracter comienza a buscar
    {
      if( sessionStorage.getItem("titulo") == 'e' )
        sessionStorage.setItem("titulo", "");

      //sessionStorage.setItem("editorial", editorial);
      sessionStorage.setItem("predictive-editorial", editorial);
      filters_editorial_ajax();

      if(key == 13){

        sessionStor_editorial = sessionStorage.getItem("editorial");
        if( sessionStor_editorial == null) sessionStor_editorial = '';

        val = keep_conaculta(editorial, sessionStor_editorial)

        /*if( sessionStor_editorial == editoriales_Conaculta ){
          sessionStorage.setItem("editorial", sessionStor_editorial+"|"+editorial);
        }else{

        }*/

        sessionStorage.setItem("editorial", val);

        sessionStorage.setItem("pagina", 1); //Restablecer paginación a 1

        url_param = window.location.search.substring(1);
        history.replaceState(null, null, 'buscar/libro/1?'+url_param); //modificar url para que muestre pagina 1

        filters_ajax('editorial');
      }
    }
  });

  $("#show_more_keywords").click(function( event ){
    event.preventDefault();
    filters_keyword_ajax(false);
  });

  $("#keyword-predictive-search").keyup(function(e){

    var key = e.which;
    keyword = escapeHtml( $(this).val() );
    if( keyword.length >= 2 ) //hasta el segundo caracter comienza a buscar
    {
      if( sessionStorage.getItem("titulo") == 'e' )
        sessionStorage.setItem("titulo", "");

      //sessionStorage.setItem("keywords", keyword);
      sessionStorage.setItem("predictive-keywords", keyword);
      filters_keyword_ajax();

      if(key == 13){
        sessionStorage.setItem("keywords", keyword);
        sessionStorage.setItem("pagina", 1); //Restablecer paginación a 1

        url_param = window.location.search.substring(1);
        history.replaceState(null, null, 'buscar/libro/1?'+url_param); //modificar url para que muestre pagina 1

        filters_ajax('keyword');
      }
    }
  });

  $("#show_more_colecciones").click(function( event ){
    event.preventDefault();
    filters_coleccion_ajax(false);
  });

  $("#coleccion-predictive-search").keyup(function(e){

    var key = e.which;
    coleccion = escapeHtml( $(this).val() );
    if( coleccion.length >= 2 ) //hasta el segundo caracter comienza a buscar
    {
      if( sessionStorage.getItem("titulo") == 'e' )
        sessionStorage.setItem("titulo", "");

      sessionStorage.setItem("coleccion", coleccion);
      filters_coleccion_ajax();

      if(key == 13){
        sessionStorage.setItem("pagina", 1);  //Restablecer paginación a 1

        url_param = window.location.search.substring(1);
        history.replaceState(null, null, 'buscar/libro/1?'+url_param); //modificar url para que muestre pagina 1

        filters_ajax('coleccion');
      }
    }
  });

  //Filtros por fecha de colofón
  $("#filter-colofon-arrow").click(function(){
    if( !filter_is_open('colofon') ) //Si esta cerrada y se esta abriendo
    {
      //Si no es el catalogo completo,
      if( !(sessionStorage.getItem("search_param") == 'catalogo' && ( sessionStorage.getItem("search_val") == 'todo' || sessionStorage.getItem("search_val") == 'novedades' ) ) )
      {
        filters_colofon_ajax(); //actualiza el rango de fechas de colofon
      }
      else //Si es el catalogo completo
      {
        //Verifica si ya se selecciono otro filtro
        if( $( "input:checked" ).length >= 1 )
        {
          filters_colofon_ajax();
        }
        else //Si no se ha seleccionado otro filtro, muestra un rango de fechas preestablecido 1555 - actual
        {
          var d = new Date();
          var current_year = d.getFullYear();
          var min = 1555;
          $( "#slider-colofon" ).slider({
            range: true,
            min: min,
            max: current_year,
            values: [ min, current_year ],
            slide: function( event, ui ) {
              $( "#amount" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
            },
            stop: function( event, ui ){
              fecha_c1 = ui.values[ 0 ]+'-01-01';
              fecha_c2 = ui.values[ 1 ]+'-12-31';
              sessionStorage.setItem( "fecha_colofon_1" , fecha_c1 );
              sessionStorage.setItem( "fecha_colofon_2" , fecha_c2 );

              $("[data-spancolofon]").remove();

              add_filters_tags('Año Colofon', 'colofon', ui.values[ 0 ] + '-' + ui.values[ 1 ]);

              filters_ajax( 'colofon' );
            }
          });
          $("#amount").val( min + " - " + current_year );

        }

      }
    }
  });

  //Filtros por tipo de producto
  $("#filter-type-arrow").click(function(){
    if( !filter_is_open('type') ) //Si esta cerrada y se esta abriendo
    {
      //Si no es el catalogo completo,
      if( !(sessionStorage.getItem("search_param") == 'catalogo' && ( sessionStorage.getItem("search_val") == 'todo' || sessionStorage.getItem("search_val") == 'novedades' ) ) )
      {
        filters_type_ajax();
      }
      else //Si es el catalogo completo
      {
        //Verifica si ya se selecciono otro filtro
        if( $( "input:checked" ).length >= 1 )
        {
          filters_type_ajax();
        }
      }
    }
  });

  //Filtros por palabras clave
  $("#filter-keywords-arrow").click(function(){
    if( !filter_is_open('keywords') )
    {
      filters_keyword_ajax();
    }
  });

  //Filtros por Coleccion
  $("#filter-coleccion-arrow").click(function(){
    if( !filter_is_open('coleccion') )
    {
      filters_coleccion_ajax();
    }
  });

  if( document.URL.search("editorial=conaculta") > 0){
    setTimeout(function(){
      $("#filter-coleccion-arrow").trigger("click");
    }, 500);
  }

  if( document.URL.search("precio_vigente=1") > 0){
    setTimeout(function(){
      $("#filter-price-arrow").trigger("click");
    }, 500);
  }


  //Filtros por Tema
  $(".dewey-link").click(function(event){
    event.preventDefault();
    var codigo_dewey = $( this ).data('dewey');

    sessionStorage.setItem("temas", codigo_dewey);
    if( sessionStorage.getItem("titulo") == "e" ){
      sessionStorage.removeItem("titulo")
    }

    filters_ajax('all');
  });

  //número de libros por página
  $("#list_show").change(function(){
    n = $(this).val();
    sessionStorage.setItem("registros_por_pagina", n);
    filters_ajax('any');
  });

  //orden de los libros
  $("#list_order").change(function(){
    ord = $(this).val();
    sessionStorage.setItem("orden", ord);
    if( ord == 'fecha_colofon'){
      sessionStorage.setItem("orden_dir", 'DESC');
    }else{
      sessionStorage.setItem("orden_dir", 'ASC');
    }
    filters_ajax('any');
  });

  //Precio único
  $("#list_precio").change(function(){
    precio_vigente = $(this).val();
    if( precio_vigente == 'todos' ){
      sessionStorage.setItem("precio_vigente", "");
      sessionStorage.setItem("titulo", "e");
    }else{
      sessionStorage.setItem("precio_vigente", precio_vigente);
    }

    filters_ajax('any');
  });

});

function filter_is_open( filter ){
  var currentStatus = $("#filter-"+filter+"-arrow").parent().attr('data-status');
  if( currentStatus == 'open' )
    return true;
  else
    return false;
}

function icheck_format(){
  //------------Formato de checkboxes------------
  $(".add-list").find(":checkbox").iCheck({
    checkboxClass: 'icheckbox_minimal-green',
    increaseArea: '20%' // optional
  });
  $(".add-list").find(":radio").iCheck({
    radioClass: 'iradio_minimal-green',
    increaseArea: '20%' // optional
  });
}

function icheck_authors_filter(){

  //------------Filtro de Autores------------
  $("input[name='autores']").on('ifClicked', function(event)
  {
    var array_autores = [];

    $( "input[name='autores']:checked" ).each(function( index ) {
      array_autores.push( $( this ).data('autores') ) ;
    });

    //Si se desmarca el checkbox
    if( $(this).parent().find("input[name='autores']").is(':checked') ) {
      //elimina tags de filtrado
      $("[data-spanautores='"+$(this).data('autores')+"']").remove();
      //elimina autores del array de autores
      for( var i = 0 ; i < array_autores.length ; i++ ){
        if( array_autores[i] == $(this).data('autores')){
          delete array_autores[i];
        }
      }
    }
    //Si se esta seleccionado el checkbox
    else
    {
      array_autores.push( $( this ).data('autores') ) ; //se agrega el autor seleccionado al array de autores

      if( sessionStorage.getItem("titulo") == "e" )
        sessionStorage.setItem("titulo", "");
    }

    var autores = array_autores.join('|'); //prepara cadena de autores para ser enviado a la petición del servicio web

    //Si no quedan autores seleccionados y el párametro inicial de búsqueda es por autor
    if ( autores === '' && sessionStorage.getItem("search_param")  === 'autores' )
    {
      autores = sessionStorage.getItem("search_val"); //Se toma el párametro inicial de búsqueda como filtro
    }

    sessionStorage.setItem("autores", autores); //alamcena los autores en Storage
    sessionStorage.setItem("pagina", 1);        //Restablece paginación a 1

    url_param = window.location.search.substring(1);
    history.replaceState(null, null, 'buscar/libro/1?'+url_param); //modificar url para que muestre pagina 1

    filters_ajax( 'autor' ); //ejecuta el filtrado

  });
}

function icheck_editorials_filter(){

  //------------Filtro de Editoriales------------
  $("input[name='editorial']").on('ifClicked', function(event){

    //var array_editoriales = sessionStorage.getItem("editorial").split("|");
    var array_editoriales = [];

    sessionStor_editorial = sessionStorage.getItem("editorial");

    if( sessionStor_editorial !== null ){
      if( sessionStor_editorial.indexOf( editoriales_Conaculta ) >= 0  ){
        array_editoriales.push("conaculta");
        array_editoriales.push("SEP / Dirección General de Publicaciones y Bibliotecas");
        array_editoriales.push("SEP / Dirección General de Publicaciones y Medios");
        array_editoriales.push("Sep / Dirección General De Divulgación");
        array_editoriales.push("Secretaría de Cultura / Dirección General de Publicaciones");
      }
    }


    $( "input[name='editorial']:checked" ).each(function( index ) {
      array_editoriales.push( $( this ).data('editorial') ) ;
    });

    //Si se desmarca el checkbox
    if( $(this).parent().find("input[name='editorial']").is(':checked') ) {
      //elimina tags de filtrado
      $("[data-spaneditorial='"+$(this).data('editorial')+"']").remove();
      //elimina editoriales del array de editoriales
      for( var i = 0 ; i < array_editoriales.length ; i++ ){
        if( array_editoriales[i] == $(this).data('editorial')){
          delete array_editoriales[i];
        }
      }
    }
    //Si se esta seleccionado el checkbox
    else
    {
      array_editoriales.push( $( this ).data('editorial') ) ; //se agrega la editorial seleccionada al array de editoriales

      if( sessionStorage.getItem("titulo") == "e" )
        sessionStorage.setItem("titulo", "");
    }

    var editoriales = array_editoriales.join('|'); //prepara cadena de editoriales para ser enviado a la petición del servicio web

    //Si no quedan editoriales seleccionadas y el párametro inicial de búsqueda es por editorial
    if ( editoriales === '' && sessionStorage.getItem("search_param")  === 'editorial' )
    {
      editoriales = sessionStorage.getItem("search_val"); //Se toma el párametro inicial de búsqueda como filtro
    }

    //Si no quedan editoriales seleccionadas y esta en el catálogo
    if ( editoriales === '' && sessionStorage.getItem("search_param")  === 'catalogo' )
    {
      sessionStorage.setItem("titulo", "e"); //Se toma el párametro inicial de búsqueda como filtro
    }

    //limpiar cadena (quitar espacios enblanco y "|" inecesarios )
    editoriales.trim();
    if( editoriales.substr(-1) == "|" ){
      editoriales = editoriales.substr(0, editoriales.length-1);
    }
    if( editoriales.substr(0,1) == "|" ){
      editoriales = editoriales.substr(1, editoriales.length);
    }
    editoriales = editoriales.replace("||", "|");

    sessionStorage.setItem("editorial", editoriales); //alamcena los editoriales en Storage
    sessionStorage.setItem("pagina", 1);              //Restablece paginación a 1

    url_param = window.location.search.substring(1);
    history.replaceState(null, null, 'buscar/libro/1?'+url_param); //modificar url para que muestre pagina 1

    filters_ajax( 'editorial' ); //ejecuta el filtrado

  });
}

function icheck_type_filter(){
  var tipo_producto = '';

  $("input[name='type']").on('ifClicked', function(event){

    $("[data-spantype]").remove(); //Elimina tags de filtrado de Tipo de producto

    if( $(this).parent().find("input[name='type']").is(':checked') )
    {
      $(this).iCheck('uncheck');
      tipo_producto = '';
    }
    else
    {
      tipo_producto = $( this ).data('type');
      tipo = $(this).data('name');
      add_filters_tags('Tipo', 'type', tipo); //Agrega nuevo tag de filtrado para Tipo de producto
    }

    sessionStorage.setItem("tipo_producto", tipo_producto); //Almacena en Session Storage el parámetro de filtrado
    sessionStorage.setItem("pagina", 1);                    //Restablece paginación a 1

    url_param = window.location.search.substring(1);
    history.replaceState(null, null, 'buscar/libro/1?'+url_param); //modificar url para que muestre pagina 1

    filters_ajax( 'type' ); //ejecuta el filtrado

  });
}

function icheck_price_filter(){
  var precio_vigente = '';

  $("input[name='precio_vigente']").on('ifClicked', function(event){

    $("[data-spanprecio_vigente]").remove(); //Elimina tags de filtrado de precio vigente

    if( $(this).parent().find("input[name='precio_vigente']").is(':checked') )
    {
      //$(this).iCheck('uncheck');
      //precio_vigente = '';
    }
    else
    {
      precio_vigente = $( this ).data('precio');
      //tipo = $(this).data('precio');
      add_filters_tags('Precio vigente', 'precio_vigente', precio_vigente); //Agrega nuevo tag de filtrado para Tipo de producto
    }

    if( precio_vigente == '1' || precio_vigente == '0'){
      sessionStorage.setItem("precio_vigente", precio_vigente); //Almacena en Session Storage el parámetro de filtrado
      sessionStorage.setItem("titulo", "e");
    }else{
      sessionStorage.setItem("precio_vigente", ""); //Almacena en Session Storage el parámetro de filtrado
      sessionStorage.setItem("titulo", "e");
    }

    sessionStorage.setItem("pagina", 1);                    //Restablece paginación a 1

    url_param = window.location.search.substring(1);
    history.replaceState(null, null, 'buscar/libro/1?'+url_param); //modificar url para que muestre pagina 1

    filters_ajax( 'all' ); //ejecuta el filtrado

  });
}

function icheck_keywords_filter(){

  //------------Filtro de Palabras Clave------------
  $("input[name='keyword']").on('ifClicked', function(event)
  {
    var array_keyword = [];

    //Todos las palabaras claves previamente seleccionadas, las agrega al array de keyword
    $( "input[name='keyword']:checked" ).each(function( index ) {
      array_keyword.push( $( this ).data('keyword') ) ;
    });

    //Si se desmarca el checkbox
    if( $(this).parent().find("input[name='keyword']").is(':checked') ) {
      //elimina tags de filtrado
      $("[data-spankeyword='"+$(this).data('keyword')+"']").remove();
      //elimina palabras clave del array de palabras clave
      for( var i = 0 ; i < array_keyword.length ; i++ ){
        if( array_keyword[i] == $(this).data('keyword')){
          delete array_keyword[i];
        }
      }
    }
    //Si se esta seleccionado el checkbox
    else
    {
      //add_filters_tags('Palabras clave', 'keyword', $(this).data('keyword')); //se agrega tag de filtrado

      array_keyword.push( $( this ).data('keyword') ) ; //se agrega la palabra clave seleccionada al array de palabras clave

      if( sessionStorage.getItem("titulo") == "e" )
        sessionStorage.setItem("titulo", "");
    }

    var keywords = array_keyword.join('|'); //prepara cadena de keywords para ser enviada a la petición del servicio web

    //Si no quedan palabras clave seleccionadas y el párametro inicial de búsqueda es por keywords
    if ( keywords === '' && sessionStorage.getItem("search_param")  === 'keywords' )
    {
      keywords = sessionStorage.getItem("search_val"); //Se toma el párametro inicial de búsqueda como filtro
    }

    sessionStorage.setItem("keywords", keywords); //alamcena las palabras clave en Storage
    sessionStorage.setItem("pagina", 1);                    //Restablece paginación a 1

    url_param = window.location.search.substring(1);
    history.replaceState(null, null, 'buscar/libro/1?'+url_param); //modificar url para que muestre pagina 1

    filters_ajax( 'keyword' ); //ejecuta el filtrado

  });
}

function icheck_coleccion_filter(){
  var coleccion = '';

  $("input[name='coleccion']").on('ifClicked', function(event){

    $("[data-spancoleccion]").remove(); //Elimina tags de filtrado de Coleccion

    if( $(this).parent().find("input[name='coleccion']").is(':checked') )
    {
      $(this).iCheck('uncheck');
      coleccion = '';
    }
    else
    {
      coleccion = $( this ).data('coleccion');
      add_filters_tags('Coleccion', 'coleccion', coleccion); //Agrega nuevo tag de filtrado para coleccion
    }

    //Si no quedan colecciones seleccionados y el párametro inicial de búsqueda es por colecciones
    if ( coleccion === '' && sessionStorage.getItem("search_param")  === 'coleccion' )
    {
      coleccion = sessionStorage.getItem("search_val"); //Se toma el párametro inicial de búsqueda como filtro
    }

    sessionStorage.setItem("coleccion", coleccion); //Almacena en Session Storage el parámetro de filtrado
    sessionStorage.setItem("pagina", 1);            //Restablece paginación a 1

    url_param = window.location.search.substring(1);
    history.replaceState(null, null, 'buscar/libro/1?'+url_param); //modificar url para que muestre pagina 1

    filters_ajax( 'coleccion' ); //ejecuta el filtrado

  });
}

function add_filters_tags(label, name, value){
  //agrega tag de filtrado
  span = '';
  span += '<span class="label-filter" data-span'+name+'="'+value+'" data-filter="'+name+'" data-value="'+value+'">';
  //console.log(label + ' - ' + name + ' - ' + value );
  if( label == 'Editorial' && sessionStorage.getItem("editorial") == editoriales_Conaculta){
    //Si es catálogo histórico conaculta no coloca la X para borrar etiqueta
  }else if( sessionStorage.getItem("search_param") == 'editorial' && sessionStorage.getItem("search_val") == editoriales_Conaculta && name == 'editorial' && ( value == 'conaculta' || value == 'SEP / Dirección General de Publicaciones y Bibliotecas' || value == 'SEP / Dirección General de Publicaciones y Medios' || value == 'Sep / Dirección General De Divulgación' || value == 'Secretaría de Cultura / Dirección General de Publicaciones')){
    //Si es catálogo histórico conaculta no coloca la X para borrar etiqueta
  }else if( sessionStorage.getItem("search_param") == 'precio_vigente' && sessionStorage.getItem("search_val") == '1' && name == 'precio_vigente'  ){
    //Si es precio único no coloca la X para borrar etiqueta
  }
  else{
    span += '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
  }

  if (label == "Año Colofon") {
    label = "Fecha de colofón";
  }
  span += label+': ' + value;
  span += '</span>';

  $(".search-filter").find("div").append(span);

  remove_filters_tags(); //Agrega funcionalidad a la nueva etiqueta

  $(".txt-filter").show();
}

function remove_filters_tags(){
  $(".glyphicon-remove").click(function(){

    $(this).parent().remove();
    var param = $("#main_search").val();
    var selected_item = $("#form_main_search #search_param option:selected").val();

    //Si es etiqueta de Autores
    if( $(this).parent().data('spanautores') !== undefined )
    {
      $("[data-autores='"+$(this).parent().data('spanautores')+"']").iCheck('uncheck');

      if( selected_item == "autores" ) {
        $("#form_main_search #txt_book_search").val("");
      }

      var array_autores = [];

      //Todos los autores previamente seleccionados, los agrega al array de autores
      $( "input[name='autores']:checked" ).each(function( index ) {
        array_autores.push( $( this ).data('autores') ) ;
      });

      var autores = array_autores.join('|'); //prepara cadena de autores para ser enviado a la petición del servicio web

      //Si no quedan autores seleccionados y el párametro inicial de búsqueda es por autor
      if ( autores === '' && sessionStorage.getItem("search_param")  === 'autores' )
      {
        autores = sessionStorage.getItem("search_val"); //Se toma el párametro inicial de búsqueda como filtro
      }

      sessionStorage.setItem("autores", autores); //alamcena los autores en Storage
    }
    //Si es etiqueta de Editorial
    else if( $(this).parent().data('spaneditorial') !== undefined )
    {
      $("[data-editorial='"+$(this).parent().data('spaneditorial')+"']").iCheck('uncheck');

      //editorial = '';

      if( selected_item == "editorial" ) {
        $("#form_main_search #txt_book_search").val("");
      }

      var array_editoriales = [];

      //Todas las editoriales previamente seleccionadas, los agrega al array de editoriales
      $( "input[name='editorial']:checked" ).each(function( index ) {
        array_editoriales.push( $( this ).data('editorial') ) ;
      });

      var editorial = array_editoriales.join('|'); //prepara cadena de autores para ser enviado a la petición del servicio web



      /*if( sessionStorage.getItem("search_param")  === 'editorial' )
      {
        editorial = sessionStorage.getItem("search_val"); //Se toma el párametro inicial de búsqueda como filtro
      }*/

      //Si no quedan editoriales seleccionadas y el párametro inicial de búsqueda es por editorial
      if ( editorial === '' && sessionStorage.getItem("search_param")  === 'editorial' )
      {
        editorial = sessionStorage.getItem("search_val"); //Se toma el párametro inicial de búsqueda como filtro
      }

      sessionStorage.setItem("editorial", editorial); //alamcena los autores en Storage
    }
    //Si es etiqueta de Tipo de producto
    else if( $(this).parent().data('spantype') !== undefined )
    {
      $("[data-type='"+$(this).parent().data('spantype')+"']").iCheck('uncheck');

      tipo_producto = '';

      if( sessionStorage.getItem("search_param")  === 'tipo_producto' )
      {
        tipo_producto = sessionStorage.getItem("search_val"); //Se toma el párametro inicial de búsqueda como filtro
      }

      sessionStorage.setItem("tipo_producto", tipo_producto); //alamcena los autores en Storage
    }
    //Si es etiqueta de Precio único
    else if( $(this).parent().data('spanprecio_vigente') !== undefined )
    {
      precio_vigente = '';
      $("[name='precio_vigente").iCheck('uncheck');
      sessionStorage.setItem("precio_vigente", precio_vigente);
    }
    //Si es etiqueta de Fecha de colofón
    else if( $(this).parent().data('spancolofon') !== undefined )
    {
      today = new Date();
      current_year = today.getFullYear();

      sessionStorage.removeItem("fecha_colofon_1");
      sessionStorage.removeItem("fecha_colofon_2");

      $("#slider-colofon").slider("destroy");

      $("#amount").val( "1985 - " + current_year );

      $("#filter-colofon-arrow").parent().children('.container-resizable').fadeOut(300);
      $("#filter-colofon-arrow").css('background-image', 'url(assets/images/black_arrow_down.svg)');
      $("#filter-colofon-arrow").parent().attr('data-status', 'closed');
    }
    else if( $(this).parent().data('filter') == 'fecha_colofon_1' || $(this).parent().data('filter') == 'fecha_colofon_2' )
    {
      $("span:data(spanfecha_colofon_1)").remove();
      $("span:data(spanfecha_colofon_2)").remove();

      today = new Date();
      current_year = today.getFullYear();

      sessionStorage.removeItem("fecha_colofon_1");
      sessionStorage.removeItem("fecha_colofon_2");

      $("#slider-colofon").slider("destroy");

      $("#amount").val( "1985 - " + current_year );

      $("#filter-colofon-arrow").parent().children('.container-resizable').fadeOut(300);
      $("#filter-colofon-arrow").css('background-image', 'url(assets/images/black_arrow_down.svg)');
      $("#filter-colofon-arrow").parent().attr('data-status', 'closed');

    }
    //Si es etiqueta de Palabras clave
    else if( $(this).parent().data('spankeyword') !== undefined )
    {
      if( selected_item == "keywords" ) {
        $("#form_main_search #txt_book_search").val("");
      }

      $("[data-keyword='"+$(this).parent().data('spankeyword')+"']").iCheck('uncheck');

      var keyword = [];

      //Todos los keywords previamente seleccionados, los agrega al array de keywords
      $( "input[name='keyword']:checked" ).each(function( index ) {
        keyword.push( $( this ).data('keyword') ) ;
      });

      var keywords = keyword.join('|'); //prepara cadena de keywords para ser enviado a la petición del servicio web

      //Si no quedan keywords seleccionados y el párametro inicial de búsqueda es por keyword
      if ( keywords === '' && sessionStorage.getItem("search_param")  === 'keywords' )
      {
        keywords = sessionStorage.getItem("search_val"); //Se toma el párametro inicial de búsqueda como filtro
      }

      sessionStorage.setItem("keywords", keywords); //alamcena los keywords en Storage
    }
    //Si es etiqueta de Tema
    else if( $(this).parent().data('spantheme') !== undefined )
    {
      temas = '';

      if( sessionStorage.getItem("search_param")  === 'temas' )
      {
        temas = sessionStorage.getItem("search_val"); //Se toma el párametro inicial de búsqueda como filtro
      }

      sessionStorage.setItem("temas", temas); //alamcena el tema en Storage
    }
    else
    {
      filtro = $(this).parent().data("filter");

      sessionStorage.setItem(filtro, "");
    }

    if_no_labels(); //En caso de eliminar todas las etiquetas de filtrado

    sessionStorage.setItem("pagina", 1);            //Restablece paginación a 1

    url_param = window.location.search.substring(1);
    history.replaceState(null, null, 'buscar/libro/1?'+url_param); //modificar url para que muestre pagina 1

    filters_ajax( 'all' ); //ejecuta el filtrado

  });
}

function filters_ajax( type_filter ){

  //Si existe otra petición ajax, la cancela
  if( typeof xhr != "undefined" ){
    if(xhr && xhr.readyState != 4){
      xhr.abort();
    }
  }

  if( type_filter === undefined )
    type_filter = 'all';

  var params = sessionStorage_getItems( 'results' , false ); //Obtiene parametros almacenados en sessionStorage

  $(".search-filter").find(".label-filter").remove(); //Elimina etiquetas de filtrado

  var isthere_autores = false;
  var isthere_editorial = false;
  var isthere_titulo = false;
  var isthere_keywords = false;
  var isthere_isbn = false;
  var isthere_cadena = false;

  //Agrega Nuevas etiquetas de filtrado basado en los parámetros de búsqueda
  for ( var i in params ) {
    if( isRealValue(params[i]) ){
      //Evita parámetros transparentes al usuario

      if( !(params[i] == '' || i == 'origen' || i == 'registros_por_pagina' || i == 'pagina' || i == 'min' || i == 'attr' || i== 'orden' || i == 'orden_dir' || i == 'todos' || ( i == 'titulo' && params[i] == 'e') ) )
      {
        //Parametro de autores
        if( i == 'autores' ){
          array_autores = params[i].split("|"); //separa la cadena en un array
          for( var j in array_autores )
          {
            if(array_autores[j] != '')
              add_filters_tags( 'Autor', 'autores', valToSpanish(array_autores[j], j));
          }
          isthere_autores = true;
        }
        //Parametro de editoriales
        else if( i == 'editorial' ){
          array_editoriales = params[i].split("|"); //separa la cadena en un array
          for( var j in array_editoriales )
          {
            if(array_editoriales[j] != '')
              add_filters_tags( 'Editorial', 'editorial', valToSpanish(array_editoriales[j], j));
          }
          isthere_editorial = true;
        }
        //Parametro de palabras clave
        else if( i == 'keywords' ){
          array_keywords = params[i].split("|"); //separa la cadena en un array
          for( var j in array_keywords )
          {
            if(array_keywords[j] != '')
              add_filters_tags( 'Palabras clave', 'keywords', valToSpanish(array_keywords[j], j));
          }
          isthere_keywords = true;
        }
        else if( i == 'titulo'){
          isthere_titulo = true;
          add_filters_tags( paramToSpanish(i), i, valToSpanish(params[i], i));
        }
        else if( i == 'isbn' ){
          isthere_isbn = true;
          add_filters_tags( paramToSpanish(i), i, valToSpanish(params[i], i));
        }
        else if( i == 'cadena' ){
          isthere_cadena = true;
          add_filters_tags( paramToSpanish(i), i, valToSpanish(params[i], i));
        }
        else{
          add_filters_tags( paramToSpanish(i), i, valToSpanish(params[i], i));
        }

      }

    }
  }

  if( sessionStorage.getItem("search_param") == 'autores' && !isthere_autores )
    $("#form_main_search #txt_book_search").val("");

  if( sessionStorage.getItem("search_param") == 'editorial' && !isthere_editorial )
    $("#form_main_search #txt_book_search").val("");

  if( sessionStorage.getItem("search_param") == 'titulo' && !isthere_titulo )
    $("#form_main_search #txt_book_search").val("");

  if( sessionStorage.getItem("search_param") == 'keywords' && !isthere_keywords )
    $("#form_main_search #txt_book_search").val("");

  if( sessionStorage.getItem("search_param") == 'isbn' && !isthere_isbn )
    $("#form_main_search #txt_book_search").val("");

  if( sessionStorage.getItem("search_param") == 'cadena' && !isthere_cadena )
    $("#form_main_search #txt_book_search").val("");

  $(".loader-book-data").show();
  $("#tab-results").find("a").trigger("click");

  xhr = $.ajax({
    type: "POST",
    url: 'search_filter',
    dataType: 'json',
    data: params,
    success: function(response) {

      var row='';

      if(response == 'error'){
        $(".ajax-loader").hide();
        $("#info_pag").html( '' );
        $(".search-pagination").html( '' );
        row += '<div class="list-msg">No hay resultados para el criterio de búsqueda</div>';
        $(".search-book-data").html(row);
      }

      resp = response.cuerpo;

      var autor = '';
      var editorial = '';
      var img = 'assets/images/generic_book.jpg';
      var tipo = '';

      row +='<div class="ajax-loader  loader-book-data">';
      row += '  <img src="assets/images/ajax-loader-t.gif" width="100">';
      row += '</div>';

      for( var i in resp )
      {
        autor = '';
        editorial = '';
        img = 'assets/images/generic_book.jpg';
        tipo = '';

        if(resp[i].autores.length == 0){
          //continue;
        }

        if( typeof resp[i].autores[1] !== 'undefined' )
          autor = resp[i].autores[1];//[0].nombre;

        if( typeof resp[i].editorial !== 'undefined' )
          editorial = resp[i].editorial;//[0].nombre;

        //img = ( resp[i].imagen !== null ) ? resp[i].imagen : 'assets/images/generic_book.jpg';
        if( resp[i].imagen !== null ) {
          img = resp[i].imagen;
          book_cover = 'book_with_cover';
        } else {
          img = 'assets/images/generic_book.jpg';
          book_cover = 'book_without_cover';
        }

        catalogo_tipo_prod =  catalogo_tipo_producto();
        tipo = catalogo_tipo_prod[resp[i].tipo];

        row += '<div class="col-lg-3 col-sm-4 col-xs-12  search-book-data-container">';
        row += '  <div class="search-book-img">';

        if( resp[i].tipo != 10 )
          row += '    <div class="label-tipo"><span>'+tipo+'</span></div>';

        row += '    <a href="libros/'+resp[i].id_libro+'"><img src="'+ img +'"></a>';
        row += '    <div class="search-book-info  hidden-xs '+book_cover+' ">';
        row += '      <a href="libros/'+resp[i].id_libro+'">';
        row += '        <div class="search-book-info-mask">';
        row += '          <p class="title">'+ resp[i].titulo +'</p>';

        for( var a in autor ){
          if( a < 1 )
            row += '          <p class="autor">'+ autor[a].nombre +'</p>';
          else{
            row += '          <p>...</p>';
            break;
          }
        }

        for( var e in editorial ){
          if( e < 1 )
            row += '          <p class="editorial">'+ editorial[e].nombre +'</p>';
          else{
            row += '          <p>...</p>';
            break;
          }
        }

        row += '          <div class="key-words">';

        if( resp[i].keywords.length > 0 )
          row += '            <p>Palabras clave:</p>';

        for ( var j in resp[i].keywords )
        {
          if( j <= 5 )
          {
            if( resp[i].keywords[j] != '')
              row += '<a href="buscar/libro?keywords='+resp[i].keywords[j]+'"><span class="sp-keyword">'+ resp[i].keywords[j] +'</span></a>&nbsp; ';
          }
          else
          {
            row += '<span>...</span>';
            break;
          }

        }

        row += '          </div>';
        row += '        </div>';
        row += '      </a>';
        row += '    </div>';
        row += '    <div class="search-book-info-xs  visible-xs">';
        row += '      <p class="title"><a href="libros/'+resp[i].id_libro+'">'+ resp[i].titulo +'</a></p>';
        if( autor != '' ){
          row += '      <p class="autor">'+ autor[0].nombre +'</p>';
        }
        if ( editorial != ' '){
          row += '      <p class="editorial">'+ editorial[0].nombre +'</p>';
        }

        row += '    </div>';
        row += '    <div class="visible-xs" style="clear: both"></div>';
        row += '  </div>';
        row += '</div>';

      }

      //----Información de página ------
      count_response = response.encabezado['registros'];

      registros_por_pagina = sessionStorage.getItem("registros_por_pagina");

      pagina = 1;
      if ( isRealValue(sessionStorage.getItem("pagina")) ) {
        pagina = sessionStorage.getItem("pagina");
      }

      total_paginas = Math.ceil( count_response / registros_por_pagina );

      fin = pagina * registros_por_pagina;
      inicio = fin - registros_por_pagina + 1;

      if( pagina == total_paginas )
        fin = count_response;

      sessionStorage.setItem("registros_por_pagina", registros_por_pagina);
      sessionStorage.setItem("total_registros", count_response);
      sessionStorage.setItem("pagina", pagina);
      sessionStorage.setItem("total_paginas", total_paginas);
      sessionStorage.setItem("fin", fin);
      sessionStorage.setItem("inicio", inicio);

      var infopag = inicio+' - '+fin+' de '+number_format(count_response, 0, '.', ',')+' resultados | pág. '+pagina+' de '+number_format(total_paginas, 0, '.', ',');
      var infopagxs = number_format(count_response, 0, '.', ',')+' resultados';
      $("#info_pag").html( infopag );
      $("#info_pag_xs").html( infopagxs );

      //------- Paginación ------
      var pagination = '';
      pagination += '<div class="col-sm-6 col-sm-offset-6">';

      current_url = document.URL;

      if( current_url.indexOf('localhost:8888') >0 ){
        url =  '/librosmexico/buscar/libro/';
      }else{
        url =  '/buscar/libro/';
      }

      //Conserva en la url el catalogo que se esta consultando
      url_catalogo = '';
      if ( current_url.indexOf('editorial=conaculta') > 0 ) {
        url_catalogo = '?editorial=conaculta';
      }else if ( current_url.indexOf('catalogo=novedades') > 0 ) {
        url_catalogo = '?catalogo=novedades';
      }
      else if ( current_url.indexOf('precio_vigente=1') > 0 ) {
        url_catalogo = '?precio_vigente=1';
      }else{
        url_catalogo = '?';
      }

      //Conserva en la url los criterios de busqueda
      url_search = '';
      if ( current_url.indexOf('normal_search=') > 0 ) {
        txt_book_search = QueryString.txt_book_search;
        search_param = QueryString.search_param;
        url_search = '&txt_book_search='+txt_book_search+'&search_param='+search_param+'&normal_search=';
      }

      url_search += '&pagination=true';

      // << anterior
      if ( pagina > 1 ) {
        pagination += '<a href="' + url + '1' + url_catalogo + url_search + '"><div class="flechadoble-i"></div></a> '; // <<
        pagination += ' <span>';
        pagination +=   '<a href="'+ url + (parseInt(pagina,10)-1) + url_catalogo + url_search +'">';
        pagination +=     'anterior';                                                                      // anterior
        pagination +=   '</a>';
        pagination += '</span>';
      }

      //Dos páginas anteriores
      if ( pagina > 2 ) {
        pagination += '<span>| <a href="'+ url + (parseInt(pagina,10)-2) + url_catalogo + url_search +'">' + (parseInt(pagina,10)-2) + '</a></span>';
      }

      //Una página anterior
      if ( pagina > 1 ) {
        pagination += '<span>| <a href="'+ url + (parseInt(pagina,10)-1) + url_catalogo + url_search +'">' + (parseInt(pagina,10)-1) + '</a></span>';
      }

      //Página Actual
      pagination += '  <span class="current-page">| '+pagina+' </span>';

      //Página siguiente
      if( pagina < total_paginas )
        pagination += '  <span>| <a href="' + url + (parseInt(pagina,10)+1) + url_catalogo + url_search +'">' + (parseInt(pagina,10)+1) + '</a></span>';

      //Dos páginas adelante
      if( pagina < (total_paginas-1) )
        pagination += '  <span>| <a href="' + url + (parseInt(pagina,10)+2) + url_catalogo + url_search + '">' + (parseInt(pagina,10)+2) + '</a></span>';

      //  siguiente
      if( pagina < total_paginas )
      {
        pagination += '  <span> | ';
        pagination += '    <a href="' + url + (parseInt(pagina,10)+1) + url_catalogo + url_search +'">';
        //pagination += '      siguiente<span class="flechasencilla-d"></span>';
        pagination += '      siguiente';
        pagination += '    </a>';
        pagination += '  </span>';
      }

      // >>
      if( pagina < total_paginas )
        pagination += '  <a href="' + url + total_paginas + url_catalogo + url_search +'"><div class="flechadoble-d"></div></a>';

      pagination += '</div>';

      if( total_paginas > 1 )
        $(".search-pagination").html( pagination );
      else
        $(".search-pagination").html( '' );

      //---- Listado de  Libros
      $(".search-book-data").html(row);

      $(".search-book-img").mouseenter(function(){
        $(this).find(".book_with_cover").show();
      });
      $(".search-book-img").mouseleave(function(){
        $(".book_with_cover").hide();
      });

      if( type_filter === 'all' )
      {
        filters_author_ajax();
        filters_editorial_ajax();
        icheck_price_filter();

        if( filter_is_open('type') )
          filters_type_ajax();

        if( filter_is_open('colofon') )
          filters_colofon_ajax();

        if( filter_is_open('keywords') )
          filters_keyword_ajax();

        if( filter_is_open('coleccion') )
          filters_coleccion_ajax();
      }
      else if( type_filter === 'any' )
      {
        //no actualiza filtros
      }
      else
      {
        if( type_filter !== 'autor' )
          filters_author_ajax();

        if( type_filter !== 'editorial' )
          filters_editorial_ajax();

        if( type_filter !== 'type' && filter_is_open('type') )
          filters_type_ajax();

        if( type_filter !== 'colofon' && filter_is_open('colofon') )
          filters_colofon_ajax();

        if( type_filter !== 'keyword' && filter_is_open('keywords') )
          filters_keyword_ajax();

        if( type_filter !== 'coleccion' && filter_is_open('coleccion') )
          filters_coleccion_ajax();
      }
    },
    complete: function(){
      $(".loader-book-data").hide();

      //Al dar click a un libro, se agrega a la url el parámetro &tobook=true para conservar filtros
      $(".search-book-info-mask").click(function(){
        if( document.URL.indexOf('tobook=true') < 0 ){
          url = document.URL + '&tobook=true';
          history.replaceState(null, null, url);
        }
      });


      $(".search-pagination a").click(function(){
        if( document.URL.indexOf('pagination=true') < 0 ){
          url = document.URL + '&pagination=true';
          history.replaceState(null, null, url);
        }
      });
    },
    error: function(response){
      row = '';

      if( isRealValue(response.responseText) ){

        if(response.responseText == 'error' || (response.responseText.indexOf("null result")!= -1) ){
          row += '<div class="list-msg">No hay resultados para el criterio de búsqueda</div>';
          $(".search-book-data").html(row);
          $("#info_pag").html( '' );
          $(".search-pagination").html( '' );
        }
        else{
          console.log('la respuesta es: '+response.responseText);
          //setNotify('Hubo un problema al cargar los resultados, intente más tarde', error);
          console.log("Error en la petición Ajax de filters_ajax (o se canceló la petición)");
        }

      }



      $(".loader-book-data").hide();
    },
  });
}

function filters_author_ajax( refresh ){

  if( typeof xhr_aut != "undefined" ){
    if(xhr_aut && xhr_aut.readyState != 4){
      xhr_aut.abort();
      console.log('cancelar peticion ajax');
    }
  }

  if( refresh === undefined )
    refresh = true;

  var params = sessionStorage_getItems( 'autores' , refresh );

  $(".loader-authors").show();

  xhr_aut = $.ajax({
    type: "POST",
    url: 'search_author_filter',
    data: params,
    success: function(response) {

      var row='';

      if(response == 'error'){
        $(".loader-authors").hide();

        x = $("[data-msg='noautores']");

        if( x.length == 0){
          row += '<p class="list-msg" data-msg="noautores">No hay más autores</p>';
          $("#filter-authors").append(row);
        }

        $("#show_more_author").hide();
      }

      var resp= $.parseJSON(response);

      var autor = '';
      var checked= '';

      autores_param = sessionStorage.getItem("autores");

      var t = 0;
      for( var i in resp )
      {
        if( typeof resp[i] !== 'undefined' )
          autor = resp[i];

        if( autores_param !== null )
          checked = ( autores_param.indexOf(autor) >= 0 ) ? 'checked' : '';

        row += '<p>';
        row += '  <input type="checkbox" name="autores" data-autores="'+autor+'" '+ checked +' />';
        row += '  <span class="item-filter" data-name="'+autor+'">'+ autor.substring(0,40) +'</span>';
        row += '</p>';

        t++;

      }

      if(refresh)
      {
        $("#filter-authors").html(row);
        if( t <= 3)
          $("#show_more_author").hide();
        else
          $("#show_more_author").show();
      }
      else
      {
        $("#filter-authors").append(row);
        $("#filter-authors").animate({ scrollTop: $('#filter-authors')[0].scrollHeight}, 1000);
      }

      icheck_format();
      icheck_editorials_filter();
      icheck_authors_filter();
      icheck_type_filter();
      icheck_keywords_filter();
      icheck_coleccion_filter();
      icheck_price_filter();

    },
    complete: function(){
      $(".loader-authors").hide();
    },
    error: function(){
      console.log('Error ajax al actualizar lista de autores');
      $("#filter-authors").html("");
      $(".loader-authors").hide();
    },
  });
}

function filters_editorial_ajax( refresh ){

  if( typeof xhr_edit != "undefined" ){
    if(xhr_edit && xhr_edit.readyState != 4){
      xhr_edit.abort();
      console.log('cancelar peticion ajax');
    }
  }

  if( refresh === undefined )
    refresh = true;

  var params = sessionStorage_getItems( 'editorial' , refresh );

  $(".loader-editorials").show();

  xhr_edit = $.ajax({
    type: "POST",
    url: 'search_editorial_filter',
    data: params,
    success: function(response) {

      var row='';

      if(response == 'error'){
        $(".ajax-loader").hide();

        if( !refresh )
          row += '<p class="list-msg">No hay más Editoriales</p>';

        $("#filter-editorials").append(row);
        $("#show_more_editorials").hide();
      }

      var resp= $.parseJSON(response);

      var editorial = '';
      var checked = '';

      editorial_param = sessionStorage.getItem("editorial");

      var t = 0;
      for( var i in resp )
      {
        if( typeof resp[i] !== 'undefined' )
          editorial = resp[i];

        if( editorial_param !== null )
          checked = ( editorial_param.indexOf(editorial) >= 0 ) ? 'checked' : '';

        row += '<p class="p-item">';
        row += '  <input type="checkbox" name="editorial" data-editorial="'+editorial+'" '+ checked +' />';
        row += '  <span class="item-filter" data-name="'+editorial+'">'+ editorial.substring(0,80) +'</span>';
        row += '  <span class="clear"></span>';
        row += '</p>';

        t++;
      }

      if( refresh )
      {
        $("#filter-editorials").html(row);
        if( t <= 3)
          $("#show_more_editorials").hide();
        else
          $("#show_more_editorials").show();
      }
      else
      {
        $("#filter-editorials").append(row);
        $("#filter-editorials").animate({ scrollTop: $('#filter-editorials')[0].scrollHeight}, 1000);
      }

      icheck_format();
      icheck_editorials_filter();
      icheck_authors_filter();
      icheck_type_filter();
      icheck_keywords_filter();
      icheck_coleccion_filter();
      icheck_price_filter();

    },
    complete: function(){
      $(".loader-editorials").hide();
    },
    error: function(response){
      console.log('Error ajax al actualizar lista de Editoriales');
      $(".loader-editorials").hide();
    },
  });
}

function filters_type_ajax( refresh ){

  if( typeof xhr_type != "undefined" ){
    if(xhr_type && xhr_type.readyState != 4){
      xhr_type.abort();
      console.log('cancelar peticion ajax');
    }
  }

  if( refresh === undefined )
    refresh = true;

  var params = sessionStorage_getItems( 'type' , false );

  $(".loader-type").show();

  xhr_type = $.ajax({
    type: "POST",
    url: 'search_type_filter',
    data: params,
    success: function(response) {

      var row='';

      if(response == 'error'){
        $(".ajax-loader").hide();
        row += '<p class="list-msg"></p>';
        $("#filter-type").html(row);
      }

      var resp= $.parseJSON(response);

      var type = '';
      var checked = '';
      var typesubstring = '';

      type_param = sessionStorage.getItem("tipo_producto");

      for( var i in resp )
      {
        type = '';
        typesubstring = '';

        if( typeof resp[i].name !== 'undefined' || typeof(resp[i].name) !== null)
          type = resp[i].name;

        if( isRealValue(type) )
        {
          typesubstring = type.substring(0,40);

          type_id = resp[i].id;

          if( type_param !== null )
            checked = ( type_param == type_id ) ? 'checked' : '';

          row += '<p>';
          row += '  <input type="radio" name="type" data-type="'+type_id+'" data-name="'+type+'" '+ checked +' />';
          row += '  <span class="item-filter" data-name="'+type+'">'+ typesubstring +'</span>';
          row += '</p>';
        }

      }

      if( refresh )
        $("#filter-type").html(row);
      else
        $("#filter-type").append(row);

      icheck_format();
      icheck_editorials_filter();
      icheck_authors_filter();
      icheck_type_filter();
      icheck_keywords_filter();
      icheck_coleccion_filter();
      icheck_price_filter();

    },
    complete: function(){
      $(".loader-type").hide();
    },
    error: function(response){
      console.log('Error ajax al actualizar lista de Tipos de productos');
      $(".loader-type").hide();
    },
  });
}

function filters_colofon_ajax(){

  if( typeof xhr_colof != "undefined" ){
    if(xhr_colof && xhr_colof.readyState != 4){
      xhr_colof.abort();
      console.log('cancelar peticion ajax');
    }
  }

  var params = sessionStorage_getItems( 'colofon' , false );

  $(".loader-colofon").show();

  xhr_colof = $.ajax({
    type: "POST",
    url: 'search_colofon_filter',
    dataType: 'json',
    data: params,
    success: function(response) {

      var row='';

      if(response == 'error'){
        $(".loader-colofon").hide();
        alert('Sin resultados');
      }

      min = response.fecha_colofon_1;
      max = response.fecha_colofon_2;

      $( "#slider-colofon" ).slider({
        range: true,
        min: min,
        max: max,
        values: [ min, max ],
        slide: function( event, ui ) {
          $( "#amount" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
        },
        stop: function( event, ui ){
          fecha_c1 = ui.values[ 0 ]+'-01-01';
          fecha_c2 = ui.values[ 1 ]+'-12-31';
          sessionStorage.setItem( "fecha_colofon_1" , fecha_c1 );
          sessionStorage.setItem( "fecha_colofon_2" , fecha_c2 );

          $("[data-spancolofon]").remove();

          add_filters_tags('Año Colofon', 'colofon', ui.values[ 0 ] + '-' + ui.values[ 1 ]);

          filters_ajax( 'colofon' );
        }
      });

      if( min == 3000)
        $("#amount").val( "Sin resultados" );
      else
        $("#amount").val( min + " - " + max );
    },
    complete: function(response){
      $(".loader-colofon").hide();
    },
    error: function(response){
      console.log('Error ajax al actualizar rango de años de colofón');
      $(".loader-colofon").hide();
    },
  });
}

function filters_keyword_ajax( refresh ){

  if( typeof xhr_keyw != "undefined" ){
    if(xhr_keyw && xhr_keyw.readyState != 4){
      xhr_keyw.abort();
      console.log('cancelar peticion ajax');
    }
  }

  if( refresh === undefined )
    refresh = true;

  var params = sessionStorage_getItems( 'keywords' , refresh );

  $(".loader-keywords").show();

  xhr_keyw = $.ajax({
    type: "POST",
    url: 'search_keywords_filter',
    data: params,
    success: function(response) {

      var row='';

      if(response == 'error'){
        $(".loader-keywords").hide();

        /*row += '<p class="list-msg">No hay más Palabras clave</p>';
        $("#filter-keywords").append(row);*/
        x = $("[data-msg='nokeywords']");

        if( x.length == 0){
          row += '<p class="list-msg" data-msg="nokeywords">No hay más palabras clave</p>';
          $("#filter-keywords").append(row);
        }

        $("#show_more_keywords").hide();
      }

      var resp= $.parseJSON(response);

      var keyword = '';
      var checked= '';

      keywords_param = sessionStorage.getItem("keywords");

      var t = 0;
      for( var i in resp )
      {
        if( typeof resp[i] !== 'undefined' )
          keyword = resp[i];

        if( keywords_param !== null )
          checked = ( keywords_param.indexOf(keyword) >= 0 ) ? 'checked' : '';

        row += '<p>';
        row += '  <input type="checkbox" name="keyword" data-keyword="'+keyword+'" '+ checked +' />';
        row += '  <span class="item-filter" data-name="'+keyword+'">'+ keyword.substring(0,40) +'</span>';
        row += '</p>';

        t++;

      }

      if(refresh)
      {
        $("#filter-keywords").html(row);
        if( t <= 3)
          $("#show_more_keywords").hide();
        else
          $("#show_more_keywords").show();
      }
      else
      {
        $("#filter-keywords").append(row);
        $("#filter-keywords").animate({ scrollTop: $('#filter-keywords')[0].scrollHeight}, 1000);
      }


      icheck_format();
      icheck_authors_filter();
      icheck_editorials_filter();
      icheck_authors_filter();
      icheck_type_filter();
      icheck_keywords_filter();
      icheck_coleccion_filter();
      icheck_price_filter();

    },
    complete: function(){
      $(".loader-keywords").hide();
    },
    error: function(response){
      console.log('Error ajax al actualizar lista de palabras clave');
      $(".loader-keywords").hide();
    },
  });
}

function filters_coleccion_ajax( refresh ){

  if( typeof xhr_edit != "undefined" ){
    if(xhr_edit && xhr_edit.readyState != 4){
      xhr_edit.abort();
      console.log('cancelar peticion ajax');
    }
  }

  if( refresh === undefined )
    refresh = true;

  var params = sessionStorage_getItems( 'coleccion' , refresh );

  $(".loader-coleccion").show();

  xhr_edit = $.ajax({
    type: "POST",
    url: 'search_coleccion_filter',
    data: params,
    success: function(response) {

      var row='';

      if(response == 'error'){
        $(".ajax-loader").hide();

        if( !refresh )
          row += '<p class="list-msg">No hay más Editoriales</p>';

        $("#filter-coleccion").append(row);
        $("#show_more_colecciones").hide();
      }

      var resp = $.parseJSON(response);

      var coleccion = '';
      var checked = '';


      editorial_param = sessionStorage.getItem("coleccion");

      var t = 0;
      for( var i in resp ) {


        coleccion = '';
        checked = '';

        if( isRealValue(resp[i]) ) {
          coleccion = resp[i];
          coleccion = $.trim(coleccion);
        }

        if (coleccion == '') {
          continue;
        }

        if( editorial_param !== null ) {
          checked = ( editorial_param.indexOf(coleccion) >= 0 ) ? 'checked' : '';
        }

        row += '<p class="p-item">';
        row += '  <input type="radio" name="coleccion" data-coleccion="'+coleccion+'" '+ checked +' />';
        row += '  <span class="item-filter" data-name="'+coleccion+'">'+ coleccion.substring(0,80) +'</span>';
        row += '  <span class="clear"></span>';
        row += '</p>';

        t++;
      }

      if( refresh )
      {
        $("#filter-coleccion").html(row);
        if( t <= 3)
          $("#show_more_colecciones").hide();
        else
          $("#show_more_colecciones").show();
      }
      else
      {
        $("#filter-coleccion").append(row);
        $("#filter-coleccion").animate({ scrollTop: $('#filter-coleccion')[0].scrollHeight}, 1000);
      }

      icheck_format();
      icheck_editorials_filter();
      icheck_authors_filter();
      icheck_type_filter();
      icheck_keywords_filter();
      icheck_coleccion_filter();
      icheck_price_filter();

    },
    complete: function(){
      $(".loader-coleccion").hide();
    },
    error: function(response){
      console.log('Error ajax al actualizar lista de Colecciones');
      $(".loader-coleccion").hide();
    },
  });
}

function isRealValue(obj){
 return obj && obj !== "null" && obj!== "undefined";
}

function if_no_labels(){
  numero_de_filtros = $(".label-filter").length;
  if( numero_de_filtros == 0 )
  {
    if( sessionStorage.getItem("search_param")  == 'catalogo' && sessionStorage.getItem("search_val")  == 'novedades' ){

      var today = new Date();
      var colofon1 = (today.getFullYear()-500)+'-'+(today.getMonth()+1)+'-'+today.getDate();
      var colofon2 = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

      sessionStorage_removeItem();
      sessionStorage.setItem("fecha_colofon_1", colofon1);
      sessionStorage.setItem("fecha_colofon_2", colofon2);
      sessionStorage.setItem("orden", "fecha_colofon");
      sessionStorage.setItem("orden_dir", "desc");

    }else{
      sessionStorage_removeItem();
      sessionStorage.setItem("titulo", "e");
    }

    $("#form_main_search #txt_book_search").val("");

    $(".txt-filter").hide();
  }
}

function sessionStorage_removeItem(){
  sessionStorage.removeItem("titulo");
  sessionStorage.removeItem("autores");
  sessionStorage.removeItem("editorial");
  sessionStorage.removeItem("isbn");
  sessionStorage.removeItem("tipo_producto");
  sessionStorage.removeItem("keywords");
  sessionStorage.removeItem("fecha_colofon_1");
  sessionStorage.removeItem("fecha_colofon_2");
  sessionStorage.removeItem("cadena");
  sessionStorage.removeItem("temas");
  sessionStorage.removeItem("coleccion");
  //-----Busqueda Avanzada------
  sessionStorage.removeItem("subtitulo");
  sessionStorage.removeItem("sinopsis");
  sessionStorage.removeItem("edicion");
  sessionStorage.removeItem("encuadernacion");
  sessionStorage.removeItem("no_volumen");
  sessionStorage.removeItem("nacional");

  sessionStorage.removeItem("ilustraciones_1");
  sessionStorage.removeItem("ilustraciones_2");
  sessionStorage.removeItem("peso_1");
  sessionStorage.removeItem("peso_2");
  sessionStorage.removeItem("alto_1");
  sessionStorage.removeItem("alto_2");
  sessionStorage.removeItem("ancho_1");
  sessionStorage.removeItem("ancho_2");
  sessionStorage.removeItem("grosor_1");
  sessionStorage.removeItem("grosor_2");
  sessionStorage.removeItem("paginas_1");
  sessionStorage.removeItem("paginas_2");
  sessionStorage.removeItem("fecha_publicacion_1");
  sessionStorage.removeItem("fecha_publicacion_2");
  sessionStorage.removeItem("fecha_actualizacion_1");
  sessionStorage.removeItem("fecha_actualizacion_2");
  sessionStorage.removeItem("iva");
  sessionStorage.removeItem("disponibilidad");
  sessionStorage.removeItem("audiencia");
  sessionStorage.removeItem("nivel_lectura");
  sessionStorage.removeItem("nivel_academico");
  sessionStorage.removeItem("idioma_original");
  sessionStorage.removeItem("pais");
  sessionStorage.removeItem("precio_vigente");
  sessionStorage.removeItem("con_portada");

  sessionStorage.removeItem("reimpresion");
  sessionStorage.removeItem("curso");
  sessionStorage.removeItem("estado");
  sessionStorage.removeItem("asignatura");
  //-----Paginación-------
  sessionStorage.removeItem("total_registros");
  sessionStorage.removeItem("pagina");
  sessionStorage.removeItem("total_paginas");
  sessionStorage.removeItem("fin");
  sessionStorage.removeItem("inicio");
  sessionStorage.removeItem("pagina_autores");
  sessionStorage.removeItem("pagina_editoriales");
  sessionStorage.removeItem("pagina_keywords");
  sessionStorage.removeItem("pagina_coleccion");
  //-----Orden-----
  sessionStorage.removeItem("orden");
  sessionStorage.removeItem("orden_dir");
  //-----Otros-----
  sessionStorage.removeItem("attr");
}

function sessionStorage_getItems( filter , refresh ){
  if( filter === undefined )
    filter = '';

  if( refresh === undefined )
    refresh = true;

  var titulo = sessionStorage.getItem("titulo");
  var autor = '';
  var editorial = '';
  var keyword = '';

  if( filter == 'autores' && refresh )
    autor = sessionStorage.getItem("predictive-autores");
  else
    autor = sessionStorage.getItem("autores");

  if( filter == 'editorial' && refresh )
    editorial = sessionStorage.getItem("predictive-editorial");
  else
    editorial = sessionStorage.getItem("editorial");

  if( filter == 'keywords' && refresh )
    keywords = sessionStorage.getItem("predictive-keywords");
  else
    keywords = sessionStorage.getItem("keywords");



  var isbn = sessionStorage.getItem("isbn");
  /*console.log(isbn);
  //alert(typeof(isbn));
  if(isbn == null)
    alert('null');*/

  var cadena = sessionStorage.getItem("cadena");
  var tipo = sessionStorage.getItem("tipo_producto");
  var colofon1 = sessionStorage.getItem("fecha_colofon_1");
  var colofon2 = sessionStorage.getItem("fecha_colofon_2");
  var tema = sessionStorage.getItem("temas");
  var coleccion = sessionStorage.getItem("coleccion");
  var subtitulo = sessionStorage.getItem("subtitulo");
  var sinopsis = sessionStorage.getItem("sinopsis");
  var edicion = sessionStorage.getItem("edicion");
  var encuadernacion = sessionStorage.getItem("encuadernacion");
  var no_volumen = sessionStorage.getItem("no_volumen");
  var nacional = sessionStorage.getItem("nacional");
  var ilustraciones_1 = sessionStorage.getItem("ilustraciones_1");
  var ilustraciones_2 = sessionStorage.getItem("ilustraciones_2");
  var peso_1 = sessionStorage.getItem("peso_1");
  var peso_2 = sessionStorage.getItem("peso_2");
  var alto_1 = sessionStorage.getItem("alto_1");
  var alto_2 = sessionStorage.getItem("alto_2");
  var ancho_1 = sessionStorage.getItem("ancho_1");
  var ancho_2 = sessionStorage.getItem("ancho_2");
  var grosor_1 = sessionStorage.getItem("grosor_1");
  var grosor_2 = sessionStorage.getItem("grosor_2");
  var paginas_1 = sessionStorage.getItem("paginas_1");
  var paginas_2 = sessionStorage.getItem("paginas_2");
  var fecha_publicacion_1 = sessionStorage.getItem("fecha_publicacion_1");
  var fecha_publicacion_2 = sessionStorage.getItem("fecha_publicacion_2");
  var fecha_actualizacion_1 = sessionStorage.getItem("fecha_actualizacion_1");
  var fecha_actualizacion_2 = sessionStorage.getItem("fecha_actualizacion_2");
  var iva = sessionStorage.getItem("iva");
  var disponibilidad = sessionStorage.getItem("disponibilidad");
  var audiencia = sessionStorage.getItem("audiencia");
  var nivel_lectura = sessionStorage.getItem("nivel_lectura");
  var nivel_academico = sessionStorage.getItem("nivel_academico");
  var idioma_original = sessionStorage.getItem("idioma_original");
  var pais = sessionStorage.getItem("pais");
  var precio_vigente = sessionStorage.getItem("precio_vigente");
  var con_portada = sessionStorage.getItem("con_portada");


  var reimpresion = sessionStorage.getItem("reimpresion");
  var curso = sessionStorage.getItem("curso");
  var estado = sessionStorage.getItem("estado");
  var asignatura = sessionStorage.getItem("asignatura");

  var reg_x_pag = sessionStorage.getItem("registros_por_pagina");

  var orden = '';
  var orden_dir = '';


  //var pag = 1;
  var pag = 1;
  if ( isRealValue(sessionStorage.getItem("pagina")) ) {
    pag = sessionStorage.getItem("pagina");
  }

  switch(filter) {
    case 'autores':
      if(refresh)
        sessionStorage.setItem("pagina_autores", 1);
      else if( sessionStorage.getItem("pagina_autores") === null )
        sessionStorage.setItem("pagina_autores", 2);
      else{
        pag = parseInt( sessionStorage.getItem("pagina_autores"), 10 ) + 1;
        sessionStorage.setItem("pagina_autores", pag);
      }
      pag = sessionStorage.getItem("pagina_autores");
      break;
    case 'editorial':
      if( refresh )
        sessionStorage.setItem("pagina_editoriales", 1);
      else if( sessionStorage.getItem("pagina_editoriales") === null )
        sessionStorage.setItem("pagina_editoriales", 2);
      else{
        pag = parseInt( sessionStorage.getItem("pagina_editoriales"), 10 ) + 1;
        sessionStorage.setItem("pagina_editoriales", pag);
      }
      pag = sessionStorage.getItem("pagina_editoriales");
      break;
    case 'keywords':
      if( refresh )
        sessionStorage.setItem("pagina_keywords", 1);
      else if( sessionStorage.getItem("pagina_keywords") === null )
        sessionStorage.setItem("pagina_keywords", 2);
      else{
        pag = parseInt( sessionStorage.getItem("pagina_keywords"), 10 ) + 1;
        sessionStorage.setItem("pagina_keywords", pag);
      }
      //pag = sessionStorage.getItem("pagina_keywords");
      //reg_x_pag = ( sessionStorage.getItem("total_registros") < 2000 ) ? sessionStorage.getItem("total_registros") : 2000;
      //pag = 1;
      pag = sessionStorage.getItem("pagina_keywords");
      break;
    case 'coleccion':
      if( refresh )
        sessionStorage.setItem("pagina_coleccion", 1);
      else if( sessionStorage.getItem("pagina_coleccion") === null )
        sessionStorage.setItem("pagina_coleccion", 2);
      else{
        pag = parseInt( sessionStorage.getItem("pagina_coleccion"), 10 ) + 1;
        sessionStorage.setItem("pagina_coleccion", pag);
      }
      //pag = sessionStorage.getItem("pagina_coleccion");
      reg_x_pag = ( sessionStorage.getItem("total_registros") < 8000 ) ? sessionStorage.getItem("total_registros") : 8000;
      pag = 1;
      break;
    case 'colofon':
      reg_x_pag = ( sessionStorage.getItem("total_registros") < 8000 ) ? sessionStorage.getItem("total_registros") : 8000;
      pag = 1;
      break;
    case 'type':
      reg_x_pag = ( sessionStorage.getItem("total_registros") < 8000 ) ? sessionStorage.getItem("total_registros") : 8000;
      pag = 1;
      break;
    case 'results':
      orden = sessionStorage.getItem("orden");
      orden_dir = sessionStorage.getItem("orden_dir");
      //alert('pag: ' + pag);
      break;
    default:
      pag = sessionStorage.getItem("pagina");//1;
  }

  var params = {
    autores: autor,
    titulo: titulo,
    editorial: editorial,
    tipo_producto: tipo,
    fecha_colofon_1: colofon1,
    fecha_colofon_2: colofon2,
    isbn: isbn,
    keywords: keywords,
    cadena: cadena,
    temas: tema,
    coleccion: coleccion,

    subtitulo: subtitulo,
    sinopsis: sinopsis,
    edicion: edicion,
    encuadernacion: encuadernacion,
    no_volumen: no_volumen,
    nacional: nacional,

    ilustraciones_1: ilustraciones_1,
    ilustraciones_2: ilustraciones_2,
    peso_1: peso_1,
    peso_2: peso_2,
    alto_1: alto_1,
    alto_2: alto_2,
    ancho_1: ancho_1,
    ancho_2: ancho_2,
    grosor_1: grosor_1,
    grosor_2: grosor_2,
    paginas_1: paginas_1,
    paginas_2: paginas_2,

    fecha_publicacion_1: fecha_publicacion_1,
    fecha_publicacion_2: fecha_publicacion_2,
    fecha_actualizacion_1: fecha_actualizacion_1,
    fecha_actualizacion_2: fecha_actualizacion_2,

    iva: iva,
    disponibilidad: disponibilidad,
    audiencia: audiencia,
    nivel_academico: nivel_academico,
    nivel_lectura: nivel_lectura,
    idioma_original: idioma_original,
    pais: pais,
    precio_vigente: precio_vigente,
    con_portada: con_portada,

    reimpresion: reimpresion,
    curso: curso,
    estado: estado,
    asignatura: asignatura,

    registros_por_pagina: reg_x_pag,
    pagina: pag,
    orden: orden,
    orden_dir: orden_dir,
  };

  return params;
}

function number_format(number, decimals, dec_point, thousands_sep) {

  number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}

function paramToSpanish(param){

  switch(param){
    case 'tipo_producto':
      param = 'Tipo';
      break;
    case 'titulo':
      param = 'Título';
      break;
    case 'codigo_barras':
      param = 'Códito de barras';
      break;
    case 'no_volumen':
      param = 'No. volumen';
      break;
    case 'keywords':
      param = 'Palabras Clave';
      break;
    case 'ilustraciones_1':
      param = 'Ilustraciones (min)';
      break;
    case 'ilustraciones_2':
      param = 'Ilustraciones (max)';
      break;
    case 'idioma_original':
      param = 'Idioma original';
      break;
    case 'peso_1':
      param = 'Peso (min)';
      break;
    case 'peso_2':
      param = 'Peso (max)';
      break;
    case 'alto_1':
      param = 'Alto (min)';
      break;
    case 'alto_2':
      param = 'Alto (max)';
      break;
    case 'ancho_1':
      param = 'Ancho (min)';
      break;
    case 'ancho_2':
      param = 'Ancho (max)';
      break;
    case 'grosor_1':
      param = 'Grosor (min)';
      break;
    case 'grosor_2':
      param = 'Grosor (max)';
      break;
    case 'paginas_1':
      param = 'Páginas (min)';
      break;
    case 'paginas_2':
      param = 'Páginas (max)';
      break;
    case 'nivel_lectura':
      param = 'Nivel de lectura';
      break;
    case 'nivel_academico':
      param = 'Nivel académico';
      break;
    case 'fecha_colofon_1':
      param = 'Fecha de colofón (min)';
      break;
    case 'fecha_colofon_2':
      param = 'Fecha de colofón (max)';
      break;
    case 'fecha_publicacion_1':
      param = 'Fecha de publicación (min)';
      break;
    case 'fecha_publicacion_2':
      param = 'Fecha de publicación (max)';
      break;
    case 'fecha_actualizacion_1':
      param = 'Fecha de actualización (min)';
      break;
    case 'fecha_actualizacion_2':
      param = 'Fecha de actualización (max)';
      break;
    case 'cadena':
      param = 'Todos los campos';
      break;
    case 'coleccion':
      param = 'Colección';
      break;
    case 'precio_vigente':
      param = 'Precio único';
      break;
    case 'con_portada':
      param = 'Con portada';
      break;
    default:
      param = param.capitalizeFirstLetter();
  }

  return param;
}

function valToSpanish(code, catalog){

  switch(catalog){
    case 'tipo_producto':
      tipo_producto = catalogo_tipo_producto();
      val = tipo_producto[code];
      break;
    case 'encuadernacion':
      encuadernacion = catalogo_encuadernacion();
      val = encuadernacion[code];
      break;
    case 'nacional':
      nacional = catalogo_nacional();
      val = nacional[code];
      break;
    case 'iva':
      iva = catalogo_iva();
      val = iva[code];
      break;
    case 'disponibilidad':
      disponibilidad = catalogo_disponibilidad();
      val = disponibilidad[code];
      break;
    case 'audiencia':
      audiencia = catalogo_audiencia();
      val = audiencia[code];
      break;
    case 'nivel_academico':
      nivel_academico = catalogo_nivel_academico();
      val = nivel_academico[code];
      break;
    case 'nivel_lectura':
      nivel_lectura = catalogo_nivel_lectura();
      val = nivel_lectura[code];
      break;
    case 'pais':
      pais = catalogo_paises();
      val = pais[code];
      break;
    case 'precio_vigente':
      //precio = precio_vigente();
      precio = {
        0: 'Sin vigencia',
        1: 'Vigente'
      };
      val = precio[code];
      break;
    case 'con_portada':
      //portada = con_portada();
      portada = {
        0: 'No',
        1: 'Si'
      };
      val = portada[code];
      break;
    case 'idioma_original':
      idioma = catalogo_idiomas();
      val = idioma[code];
      break;
    case 'temas':
      temas = catalogo_temas();
      val = temas[code];
      break;
    default:
      val = code;
      break;
  }
  return val;
}

String.prototype.capitalizeFirstLetter = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

function escapeHtml(text) {
  'use strict';
  return text.replace(/[\"&<>]/g, function (a) {
    return { '"': '&quot;', '&': '&amp;', '<': '&lt;', '>': '&gt;' }[a];
  });
}

var QueryString = function () {
  // This function is anonymous, is executed immediately and
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  }
  return query_string;
}();

function keep_conaculta(val, sessionStor_editorial){

  if( sessionStor_editorial.indexOf(editoriales_Conaculta) >= 0 ){

    //Si no estan las editoriales de conaculta, las agrega
    if( val.indexOf(editoriales_Conaculta) < 0 ){
      val = editoriales_Conaculta+"|"+val
    }

    //limpiar cadena (quitar espacios enblanco y "|" inecesarios )
    val.trim();
    if( val.substr(-1) == "|" ){
      val = val.substr(0, val.length-1);
    }
    if( val.substr(0,1) == "|" ){
      val = val.substr(1, val.length);
    }
    val = val.replace("||", "|");
  }

  return val;

}

/*
 *=============================================================================
 *========================== Búsqueda Avanzada ================================
 *=============================================================================
*/
$(document).ready(function(){
  $(".chosen-select").chosen({no_results_text: "Sin resultados"});

  chosen_functions();

  $("#clear_content").click(function(){
    $("#conten").html("");
    $(".input-type").find("input").val("");
    $(".chosen-select").val("0");
    $(".chosen-select").trigger("chosen:updated");

  });

  //--Botón de Búsqueda
  $("[name='advanced_search']").click(function(event){
    event.preventDefault();

    sessionStor_editorial = sessionStorage.getItem("editorial");
    if(sessionStor_editorial == null) sessionStor_editorial = '';

    sessionStorage_removeItem();

    $(".input-filtro").each(function(){

      chosen_select = $(this).find(".chosen-select");

      param = escapeHtml( chosen_select.val() );
      val = escapeHtml( $(this).find(".input-type").find(".input-single").val() );

      if(param == "editorial"){

        //Evitar que se quiten los filtros de editorial conaculta (si es el catalogo conaculta)
        val = keep_conaculta(val, sessionStor_editorial)

        sessionStorage.setItem("editorial", val);

      }else{

        sessionStorage.setItem(param, val);

      }


      if( $('[data-filter="'+param+'"]').length > 0){
        $('[data-filter="'+param+'"]').remove();
      }

      chosen_select_hidden = $(this).find(".chosen-select-hidden");
      if( chosen_select_hidden.length > 0 ){
        param2 = escapeHtml( chosen_select_hidden.val() );
        val2 = escapeHtml( $(this).find(".input-type").find(".input-double").val() );
        sessionStorage.setItem(param2, val2);
      }

    });

    $('#advanced-search').modal('hide');

    filters_ajax( 'all' );
  });

  //Funciones al abrir el caudro modal
  $('#advanced-search').on('show.bs.modal', function (e) {
    $("#clear_content").trigger("click");

    //Recuperar valores de filtrado
    var f = 1;
    var label_autores = 0;
    var label_editorial = 0;
    var label_keywords = 0;
    //Recorre todas la etiquetas de filtrado
    $(".label-filter").each(function(){

      param = $(this).data('filter');
      value = sessionStorage.getItem(param);

      if( param.substring( param.length - 2 ) != '_2' )
      {
          if( param == 'autores' ){
            label_autores++;
            if(label_autores > 1)
              return true;
          }
          if( param == 'editorial' ){
            label_editorial++;
            if(label_editorial > 1)
              return true;
          }
          if( param == 'keywords' ){
            label_keywords++;
            if(label_keywords > 1)
              return true;
          }

          //Para la primer etiqueta de filtrado ocupa el input predeterminado, para los demas agrega nuevos campos
          num = (f == 1) ? 1 : addFilter();

          selector = "#select-"+num;
          $(selector).val(param);
          $(selector).trigger("chosen:updated");
          $(selector).trigger("change");

          //campos de tipo Select
          if( param == 'encuadernacion' || param == 'disponibilidad' || param == 'nivel_academico' || param == 'nivel_lectura' || param == 'audiencia' || param == 'iva' || param == 'nacional' || param == 'pais' || param == 'idioma_original' || param == 'temas' || param == 'precio_vigente'){
            $(selector).parents(".input-filtro").find(".input-type").find("select").val(value);
            $(selector).parents(".input-filtro").find(".input-type").find("select").trigger("chosen:updated");
            $(selector).parents(".input-filtro").find(".input-type").find("select").trigger("change");
          }
          //campos con rango que utilizan 2 textbox
          else if( param.substring( param.length - 2 ) == '_1' ){
            $(selector).parents(".input-filtro").find(".input-type").find("input:nth-child(1)").val(value);

            param_2 = param.substring( 0 , param.length - 2 ) + '_2';
            value_2 = sessionStorage.getItem(param_2);

            $(selector).parents(".input-filtro").find(".input-type").find("input:nth-child(3)").val(value_2);
          }
          //campos con un solo textbox
          else{
            $(selector).parents(".input-filtro").find(".input-type").find("input:nth-child(1)").val(value);
          }
          f++;
      }
    });
  });
});

function set_current_params( param, first_taken ){
  if( isRealValue(sessionStorage.getItem( param )) ){

    if(first_taken)
      num = addFilter();
    else{
      num = 1;
      first_taken = true;
    }
    selector = "#select-"+num;
    $(selector).val( param );
    $(selector).trigger("chosen:updated");
    $(selector).trigger("change");

    $(selector).parents(".input-filtro").find(".input-type").find("input").val(sessionStorage.getItem( param ));

  }
}

function chosen_functions(){
  $(".filter-remove").click(function(){
    $(this).parent().parent().remove();
  });

  $(".chosen-select").change(function(){

    filter_field = $(this).val();
    input = '';
    switch(filter_field)
    {
      //Select Box
      case 'nacional':
      case 'encuadernacion':
      case 'iva':
      case 'disponibilidad':
      case 'audiencia':
      case 'nivel_academico':
      case 'nivel_lectura':
      case 'idioma_original':
      case 'tipo_producto':
      case 'pais':
      case 'precio_vigente':
      case 'con_portada':
        array = catalogos( filter_field );
        input  = '<select name="val[]" class="form-control  input-single  chosen-select-js">';
        for( var arr in array ){
          input += '<option value="'+arr+'">'+array[arr]+'</option>';
        }
        input += '</select>';
        break;
      //Temas - Select Box con grupos
      case 'temas':
        array = catalogos( filter_field );
        input  = '<select name="val[]" class="form-control  input-single  chosen-select-js">';
        for( var arr in array ){
          if( arr % 100 == 0 || arr == '000' ){
            if( arr != '000' ){
              input += '</optgroup>';
            }
            input += '<optgroup label="'+array[arr]+'">';
            input += '<option value="'+arr+'">'+array[arr]+'</option>';
          }
          else{
            input += '<option value="'+arr+'">'+array[arr]+'</option>';
          }
        }
        input += '</optgroup>';
        input += '</select>';
        break;
      //Text Box
      case 'edicion':
      case 'no_volumen':
        input = '<input type="text" name="val[]" class="form-control  input-single" placeholder="Escribe un número entero...">';
        break;
      //Rangos
      case 'ilustraciones_1':
      case 'peso_1':
      case 'alto_1':
      case 'ancho_1':
      case 'grosor_1':
      case 'paginas_1':
        num_2 = filter_field.substring(0, filter_field.length-1 );
        input  = '<div class="form-inline">';
        input += '  <input type="text" name="val[]" class="form-control  input-single" placeholder="Número mínimo...">';
        input += '  <input type="hidden" name="param[]" value="'+num_2+'2" class="chosen-select-hidden">';
        input += '  <input type="text" name="val[]" class="form-control  input-double" placeholder="Número máximo...">';
        input += '</div>';
        break;
      //Fechas
      case 'fecha_actualizacion_1':
      case 'fecha_colofon_1':
      case 'fecha_publicacion_1':
        num_2 = filter_field.substring(0, filter_field.length-1 );
        input  = '<div class="form-inline">';
        input += '  <input type="text" name="val[]" class="form-control  input-single" placeholder="aaaa-mm-dd">';
        input += '  <input type="hidden" name="param[]" value="'+num_2+'2" class="chosen-select-hidden">';
        input += '  <input type="text" name="val[]" class="form-control  input-double" placeholder="aaaa-mm-dd">';
        input += '</div>';
        break;
      default:
        input = '<input type="text" name="val[]" class="form-control  input-single" placeholder="Escribe la palabra a buscar...">';
        break;
    }
    $(this).parents(".input-filtro").find(".input-type").html(input);

    $(".chosen-select-js").chosen({no_results_text: "Sin resultados"});

  });
}

var num_filters = 1;
function addFilter(){
  num_filters++;
  var inner = document.getElementById("conten").innerHTML;
  var html = '';
  html += '<div class="input-filtro">';
  html += '  <div class="col-xs-12  col-sm-4">';
  html += '    <button type="button" class="filter-remove"></button>';
  html += '    <select id="select-'+num_filters+'" name="param[]" class="form-control  chosen-select" data-placeholder="Seleccionar filtro...">';

  html += '      <option value="0">Seleccionar filtro...</option>';
  html += '      <option value="alto_1">Alto</option>';
  html += '      <option value="ancho_1">Ancho</option>';
  html += '      <option value="audiencia">Audiencia</option>';
  html += '      <option value="autores">Autores</option>';
  html += '      <option value="coleccion">Colección</option>';
  html += '      <option value="disponibilidad">Disponibilidad</option>';
  html += '      <option value="edicion">Edición</option>';
  html += '      <option value="editorial">Editorial</option>';
  html += '      <option value="encuadernacion">Encuadernación</option>';
  html += '      <option value="fecha_actualizacion_1">Fecha de actualización</option>';
  html += '      <option value="fecha_colofon_1">Fecha de colofón</option>';
  html += '      <option value="fecha_publicacion_1">Fecha de publicación</option>';
  html += '      <option value="grosor_1">Grosor</option>';
  html += '      <option value="idioma_original">Idioma original</option>';
  html += '      <option value="ilustraciones_1">Ilustraciones</option>';
  html += '      <option value="iva">IVA</option>';
  html += '      <option value="nacional">Nacional</option>';
  html += '      <option value="nivel_academico">Nivel académico</option>';
  html += '      <option value="nivel_lectura">Nivel de lectura</option>';
  html += '      <option value="paginas_1">Número de páginas</option>';
  html += '      <option value="no_volumen">Número de volumen</option>';
  html += '      <option value="pais">País</option>';
  html += '      <option value="keywords">Palabras clave</option>';
  html += '      <option value="peso_1">Peso</option>';
  html += '      <option value="con_portada">Portada</option>';
  html += '      <option value="precio_vigente">Precio vigente</option>';
  html += '      <option value="sinopsis">Sinopsis</option>';
  html += '      <option value="subtitulo">Subtítulo</option>';
  html += '      <option value="temas">Temas</option>';
  html += '      <option value="tipo_producto">Tipo de libro</option>';
  html += '      <option value="titulo">Título</option>';
  html += '      <option value="cadena">Todo</option>';

  //html += '      <option value="reimpresion">Reimpresión</option>';
  //html += '      <option value="curso">Curso</option>';
  //html += '      <option value="estado">Estado</option>';
  //html += '      <option value="asignatura">Asignatura</option>';

  html += '    </select>';
  html += '  </div>';
  html += '  <div class="input-type  col-xs-12  col-sm-8">';
  html += '    <input type="text" name="val[]" class="form-control" placeholder="Escribe una palabra o frase...">';
  html += '  </div>';
  html += '  <div class="clear"></div>';
  html += '</div>';
  //document.getElementById("conten").innerHTML = inner + html;

  $("#conten").append(html);

  var selnum = "#select-"+num_filters;
  $(selnum).chosen({no_results_text: "Sin resultados"});

  chosen_functions();

  return num_filters;
}



// --------------  Fer -----------------------

$(document).ready(function(){

  //se coloca en el data-height de los contenedores el valor inicial de su altura
  $('form.add-list').each(function(){
    var currentHeight = $(this).height();
    $(this).attr('data-height', currentHeight );
  });

  //se colapsan todos los contenedores de filtros
  $('.container-resizable').css('display', 'none');

  //funcionalidad de 'ver todas las opciones' en la parte de filtros del buscador
  $('.show-options-link').click(function(){
    var currentLink = $(this);
    var filtersContainer = $(this).closest('.container-resizable').children('form.add-list');
    var currentStatus = filtersContainer.attr('data-status');
    //se obtiene la altura inicial
    var initialHeight = filtersContainer.attr('data-height');

    if( currentStatus=='closed' )
    {
      //contenedor cerrado, entonces abrirlo
      //se muestran todos los elementos ocultos
      filtersContainer.children('.hidden-filter-item').show();
      //se obtiene altura total del contenedor de filtros
      var totalHeight = filtersContainer.height();
      //regresar altura del contenedor al tamaño inicial
      filtersContainer.height( initialHeight );
      //se hace animación para expandir
      filtersContainer.animate({
          height: totalHeight,
      }, 300, function() {
          // Animation complete.
          currentLink.text('Ver menos');
      });
      filtersContainer.attr('data-status', 'open');
    }
    else
    {
      //contenedor abierto, entonces cerrarlo

      filtersContainer.animate({
          height: initialHeight,
      }, 200, function() {
          // Animation complete.
          currentLink.text('Ver todas las opciones');
          $(this).children('.hidden-filter-item').attr('style', '');
          $(this).attr('style', '');
      });

      filtersContainer.attr('data-status', 'closed');
    }

  });

  //funcionalidad para colapsar o expandir sección
  $('.filter-title').click(function(){
    var currentStatus = $(this).parent().attr('data-status');
    if(currentStatus=='closed' )
    {
      //sección cerrada, entonces expandir
      $(this).parent().children('.container-resizable').fadeIn(400);
      $(this).parent().attr('data-status', 'open');
      if( document.URL.search("editorial=conaculta") > 0){
        $(this).css('background-image', 'url(assets/images/desplegable_up.png)');
      }else{
        $(this).css('background-image', 'url(assets/images/black_arrow_up.svg)');
      }
    }
    else
    {
      //sección abierta, entonces colapsarla
      $(this).parent().children('.container-resizable').fadeOut(300);
      $(this).parent().attr('data-status', 'closed');
      if( document.URL.search("editorial=conaculta") > 0){
        $(this).css('background-image', 'url(assets/images/desplegable.png)');
      }else{
        $(this).css('background-image', 'url(assets/images/black_arrow_down.svg)');
      }
    }
  });

  // **funcionalidad de slide para temas de búsqueda

  theme_section_width();

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    theme_section_width();
  });

  function theme_section_width(){
    //se obtiene ancho de la columna contenedora del slide
    var totalWidth = $('.topics-wrapper').width();

    //se coloca el ancho de la columna en su data-width
    $('.topics-wrapper').attr('data-width', totalWidth);

    //se establece el ancho del topics-container
    var widthContainer = 3*totalWidth;

    //se coloca el ancho total del topics-container
    $('.topics-container').width(widthContainer);

    //a cada contenedor de nivel se le asigna el ancho de la columna contenedora
    $('.topics-container section').children('.container-level').width(totalWidth);
  }

  //se establece el left del contenedor por default a cero (0)
  var topicsContainer = $('.topics-container');
  topicsContainer.css('left', 0);

  //se avanza de nivel
  $('.container-level .next-level').click(function(){
    var dataWidth = $('.topics-wrapper').attr('data-width');

    //se determina que sección se deja visible
    var menuTarget = $(this).data('target');
    var currentTarget = $('#'+menuTarget);
    currentTarget.closest('section').children('.column-mode').hide();
    currentTarget.show();

    topicsContainer.animate({left: parseInt(topicsContainer.css('left')) - parseInt(dataWidth) }, 400);

    // se suma una unidad al attr data-panel
    var currentPanel = $('.topics-container').attr('data-panel');
    $('.topics-container').attr('data-panel', parseInt(currentPanel)+1);

  });

  //se retrocede de nivel
  $('.container-level .previous-level').click(function(){
    var dataWidth = $('.topics-wrapper').attr('data-width');
    topicsContainer.animate({left: parseInt(topicsContainer.css('left')) + parseInt(dataWidth) }, 400);

    // se resta una unidad al attr data-panel
    var currentPanel = $('.topics-container').attr('data-panel');
    $('.topics-container').attr('data-panel', parseInt(currentPanel)-1);

  });

  // **funcionalidad de slide para temas de búsqueda

  $(window).resize(function(){

    //**** re acomodo de la parte de temas dewey****

    //se obtiene ancho de la columna contenedora del slide
    var totalWidth = $('.topics-wrapper').width();

    //se coloca el ancho de la columna en su data-width
    $('.topics-wrapper').attr('data-width', totalWidth);

    //se establece el ancho del topics-container
    var widthContainer = 3*totalWidth;

    //se coloca el ancho total del topics-container
    $('.topics-container').width(widthContainer);

    //a cada contenedor de nivel se le asigna el ancho de la columna contenedora
    $('.topics-container section').children('.container-level').width(totalWidth);

    // se reajusta el left
    //se obtiene el panel activo
    var currentPanel = $('.topics-container').attr('data-panel');
    currentPanel = parseInt(currentPanel);
    //se obtiene el nuevo left del topicsContainer
    var newLeft = 0;
    switch(currentPanel) {
      case 1:
        newLeft = 0;
        break;
      case 2:
        newLeft = parseInt(totalWidth)*(-1);
        break;
      case 3:
        newLeft = (parseInt(totalWidth)*(-1)*2);
        break;

    }
    //se ejecuta el left del topicsContainer
    topicsContainer.css('left', newLeft);

  });

});