$(document).ready(function(){	

	//se agrega el data-height a las listas desplegables
	$('.lists-wrp').each(function(){
		$(this).attr('data-height', $(this).height() );
	});

	//se da click al botón 'ver todas mis listas'; se muestran o se ocultan las listas dependiendo el status de la lista; 'closed', 'open'
	$('.all-lists-link a').click(function(){
		var currentLink = $(this);
		var parentContainer = $(this).closest('.list-section').children('.lists-wrp');
		//se obtiene el status actual del contenedor
		var currentStatus = $(this).closest('.list-section').attr('data-status');

		var isMobile = checkMobileSize();
		if( isMobile )
		{
			//funcionalidad mostrar todas las listas, versión móvil
			if( currentStatus == 'closed' )
			{
				currentLink.closest('.list-section').children('.hidden-list').fadeIn(500);
				currentLink.text('Ver menos') ;
				currentLink.closest('.list-section').attr('data-status', 'open');
			}
			if( currentStatus == 'open' )
			{
				currentLink.closest('.list-section').children('.hidden-list').fadeOut(300);
				//se cambia el texto del botón
				var originalText = currentLink.data('text');
				currentLink.text(originalText);
				currentLink.closest('.list-section').attr('data-status', 'closed');
			}
		}
		else
		{
			//funcionalidad mostrar todas las listas, versión escritorio
			if( currentStatus == 'closed' )
			{
				//mostrar elementos
				parentContainer.children('.list-hidden').show();

				//obtener altura total del contenedor
				var totalHeight = parentContainer.height();

				//se regresa al tamaño inicial del contenedor
				parentContainer.height(parentContainer.data('height'));

				//ejecutar ejecutar animación con la altura obtenida
				parentContainer.animate({height: totalHeight }, 400, function(){
					//animación completada
					//se cambia el status del contenedor a 'open'
					$(this).closest('.list-section').attr('data-status', 'open');
					//se cambia el texto del botón
					currentLink.text('Ver menos') ;
				});
			}
			if( currentStatus == 'open' )
			{
				//ejecutar ejecutar animación con la altura inicial del contenedor
				parentContainer.animate({height: parseInt(parentContainer.data('height')) }, 200, function(){
					//animación completada
					//se elimina el atributo style del contenedor
					parentContainer.attr('style', '');
					//se cambia el status del contenedor a 'open'
					parentContainer.children('.list-hidden').attr('style', '');
					$(this).closest('.list-section').attr('data-status', 'closed');
					//se cambia el texto del botón
					var originalText = currentLink.data('text');
					currentLink.text(originalText) ;
				});
			}
		}

		
	});

	
	//reacomodo de elementos en la versión mobile de la biblioteca general
	//se llama a la función que detecta el tamaño del documento
	var isMobile = checkMobileSize();
	if( isMobile )
	{
		//revisar que no se haya hecho ya el ajuste
		var setMobile = $('.books-panel').attr('data-setmobile');
		if( setMobile == 0 )
		{
			mobileAdjust();
		}
	}
		

	$(window).resize(function(){
	    var isMobile = checkMobileSize();
	    var setMobile = $('.books-panel').attr('data-setmobile');
	    if( isMobile )
	    {
	    	//revisar que no se haya hecho ya el ajuste
	    	if( setMobile == 0 )
	    	{
	    		mobileAdjust();
	    	}
	    }
	    else
	    {
	    	if( setMobile == 1 )
	    		location.reload();
	    }

	    //alineación vertical-middle para las portadas de los libros
	    //fixVerticalAlign();
	    	
	});








	//funcionalidad para buscar una lista
	$('.search-list').keyup(function(){

		var windowSize = $(window).width();
		if(windowSize<=480)
		{
			//funcionalidad mobile
			var currentText = $(this).val();
			var container = $(this).closest('.list-section');
			if(currentText=='')
			{
				//reacomodar como al inicio
				//mostrar las primeras 2 listas y mostrar el link de ver más
				var listsContainer = $(this).closest('.list-section');
				var counter = 0;
				listsContainer.children('.list-info-container').each(function(){
					if(counter<2)
					{
						//si tiene la clase que las oculta, entonces mostrarla
						if( $(this).hasClass('hidden-list') )
							$(this).removeClass('hidden-list');
					}
					else
					{
						//sino tiene la clase que las oculta, entonces ponérsela
						if( !$(this).hasClass('hidden-list') )
							$(this).addClass('hidden-list');
					}
					counter++;
				});
				$(this).closest('.list-section').children('.all-lists-link').fadeIn(400);
			}
			else
			{
				//hacer filtrado
				container.children('.list-info-container').each(function(){
					var currentListName = $(this).data('listname');
					if( (currentListName.toLowerCase().search(currentText.toLowerCase()) != -1) )
					{
						//si coincide la búsqueda
						if( $(this).hasClass('hidden-list') )
						{
							//si la lista está oculta, entonces la muestra
							$(this).removeClass('hidden-list');
						}
					}
					else
					{
						//no coincide la búsqueda
				    	if( !$(this).hasClass('hidden-list') )
				    	{
				    		$(this).addClass('hidden-list');
				    	}
					}


				});
				$(this).closest('.list-section').children('.all-lists-link').fadeOut(400);
			}
		}
		else
		{
			//funciónalidad desktop
			var currentText = $(this).val();
		    var container = $(this).closest('.list-section').children('.lists-wrp');

		    if( currentText=='' )
			{
				//reacomodar como al inicio
				var listsContainer = $(this).closest('.list-section').children('.lists-wrp');
				var counter = 0;
				listsContainer.children('.list-item').each(function(){
					if(counter<2)
					{
						//si tiene la clase que las oculta, entonces mostrarla
						if( $(this).hasClass('list-hidden') )
							$(this).removeClass('list-hidden');
					}
					else
					{
						//si no tiene la clase que las oculta, entonces se las pone
						if( !$(this).hasClass('list-hidden') )
							$(this).addClass('list-hidden');
					}
					counter++;
				});
				$(this).closest('.list-section').children('.all-lists-link').fadeIn(400);
			}
			else
			{
				//hacer filtrado
				container.children('.list-item').each(function(){
			    	var currentListName = $(this).data('listname');
			    	if( (currentListName.toLowerCase().search(currentText.toLowerCase()) != -1) )
				    {
				    	//si coincide la búsqueda
				    	if( $(this).hasClass('list-hidden') )
				    	{
				    		//si la lista actual está oculta, entonces la muestro
				    		$(this).removeClass('list-hidden');
				    	}
				    }
				    else
				    {
				    	//no coincide la búsqueda
				    	if( !$(this).hasClass('list-hidden') )
				    	{
				    		$(this).addClass('list-hidden');
				    	}
				    }
			    });
			    $(this).closest('.list-section').children('.all-lists-link').fadeOut(400);
			}

		}

		

	    

	});

	$('.search-list').focus(function(){


		var currentText = $(this).val();
		var containerStatus = $(this).closest('.list-section').attr('data-status');

		//se debe compactar el list container
		if(containerStatus=='open')
		{
			//hacer trigger al botón de ver menos
			$(this).closest('.list-section').children('.all-lists-link').children('a').trigger('click');
			$(this).closest('.list-section').children('.all-lists-link').fadeOut(400);
		}

		if( currentText == '' )
		{
			//si está en forma compacta, entonces ocultar el link de ver mas listas
			if( (containerStatus=='closed') && ( $(this).closest('.list-section').children('.all-lists-link').css('display')=='block' ) )
			{
				//$(this).closest('.list-section').children('.all-lists-link').fadeOut(400);
			}

		}
	});


	//*** se elimina funcionalidad del evento blur del buscador porque está implementada ya en el keyup **
	// $('.search-list').blur( function(){
	// 	//cuando pierde el foco, si está vacío el buscador, entonces reacomodar todo como al inicio

	// 	//verificar si el buscador está vacío
	// 	var currentText = $(this).val();
	// 	if( currentText=='' )
	// 	{
	// 		//reacomodar como al inicio
	// 		var listsContainer = $(this).closest('.list-section').children('.lists-wrp');
	// 		var counter = 0;
	// 		listsContainer.children('.list-item').each(function(){
	// 			if(counter<2)
	// 			{
	// 				//si tiene la clase que las oculta, entonces mostrarla
	// 				if( $(this).hasClass('list-hidden') )
	// 					$(this).removeClass('list-hidden');
	// 			}
	// 			else
	// 			{
	// 				//si no tiene la clase que las oculta, entonces se las pone
	// 				if( !$(this).hasClass('list-hidden') )
	// 					$(this).addClass('list-hidden');
	// 			}
	// 			counter++;
	// 		});
	// 		$(this).closest('.list-section').children('.all-lists-link').fadeIn(400);
	// 	}
		
	// });









});

function checkMobileSize() {
	var windowSize = $(window).width();
	if( windowSize <= 480 )
		return true;
	else
		return false;
}

function mobileAdjust() {

	//se hace el ajuste de la sección 'mis libros'
	$('.books-panel .default').each(function(){
		$('.green-list-hov').append( $(this) );

	});

	//se hace el ajuste de la sección 'mis listas'
	$('.books-panel .myList').each(function(){
		$('.orange-list-hov').append( $(this) );

	});

	//se hace el ajuste de la sección 'listas inteligentes'
	$('.books-panel .intelligent').each(function(){
		$('.dark-blue-list-hov').append( $(this) );

	});

	//se colocan al final de su contenedor los links de 'ver todas mis listas'
	$('.all-lists-link').each(function(){
		$(this).parent().append( $(this) );
	});

	//se establece que ya se realizó el ajuste para mobile
	$('.books-panel').attr('data-setmobile', 1);
}





