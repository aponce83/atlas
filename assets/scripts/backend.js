
	
$(document).ready(function(){
	
	//new events
	$('.language').on('change', function() {
		window.location.href = $(this).attr('data-action') + '?language_id=' + $(this).val();
	});
	
	// zebra tables
	$('table.zebra tr:even').addClass('even'); 
	
	/* identity options */
	$('#header .identity').on('mouseenter', function(){
		$('#header .identity ul').show();
	}).on('mouseleave', function(){
		$('#header .identity ul').hide();
	});
	
	/* menu options */
	$('#menu > li').on('mouseenter', function(){
		$(this).find('ul').show();
	}).on('mouseleave', function(){
		$(this).find('ul').hide();
	});
	
	/* dropdown*/
	$('div.dropdown').on('mouseenter', function(){
		$(this).addClass('hover').find('ul').show();
	}).on('mouseleave', function(){
		$(this).removeClass('hover').find('ul').hide();
	});
	
	/* message */
	$('div.message a.close').on('click', function(e){
		e.preventDefault();
		$(this).parent().fadeOut();
	});
	
	/* tooltips */
	$('*[data-tooltip]').on('mouseover', function(){
		var elem = $(this);
		var tip  = $('#tooltip');
		var text = elem.data('tooltip');
			tip.text(text);
		var left = parseInt(elem.offset().left-tip.width()/2-2)+'px';
		var top  = parseInt(elem.offset().top+elem.height()+9)+'px';
			tip.css({'left': left, 'top': top}).show(0);
	}).on('mouseout', function(){
		$('#tooltip').hide(0);
	});
	
	/* delete */
	$('td a.delete').on('click', function(e){
		e.preventDefault();
		var itemname = $(this).parent().parent().find('td[data-itemname]').data('itemname');
		var url = $(this).attr('href').split('?id=');
		$('#modal-confirm .box-body p span').html('Est� a punto de borrar <strong>'+itemname+'</strong>');
		$('#modal-overlay').fadeIn(300, function(){
			$('#modal-confirm').data('action', url[0]).data('itemid', url[1]).show(0);
		});
	});
	
	/* bulk action */
	$('div.box-table button.bulk').on('click', function(){
		if ($(this).hasClass('disabled')) {
			return false;
		}
		if ( ! $('form[name="list"] select[name="command"]').val().length) {
			alert('Debe seleccionar una acci�n');
			return;
		}
		var command = $('div.box-table div.bulk select option:selected').text().toLowerCase();
		var value   = $('div.box-table div.bulk select option:selected').val();
		var count   = $('div.box-table input.select:checked').length;
		var action  = (value=='delete') ? $(this).data('action').replace('/status', '/delete') : $(this).data('action');		
		var status  = (command=='activar' || command=='publicar') ? 1 : 0;
		$('#modal-confirm .box-body p span').html('Est� a punto de <strong>'+command+'</strong> '+count+' elementos.');
		$('#modal-overlay').fadeIn(300, function(){
			$('#modal-confirm').data('action', action).data('status', status).show(0);
		});
	});
	
	/* modal */
	$('#modal-confirm a.close, #modal-confirm button.cancel').on('click', function(e){
		e.preventDefault();
		$('#modal-confirm').data('action', '').data('itemid', '').hide(0);
		$('#modal-overlay').fadeOut();
	});
	$('#modal-confirm button.accept').on('click', function(){
		if ( ! $(this).hasClass('disabled')) {
			var action = $('#modal-confirm').data('action');
			var id     = $('#modal-confirm').data('itemid');
			var status = $('#modal-confirm').data('status');
			var token  = $('form[name="list"]').data('token');
			if (id.length) {
				$('form[name="list"] input.select').attr('checked', false);
			}
			$(this).addClass('disabled');
			$('form[name="list"] input[name="id"]').val(id);
			$('form[name="list"] input[name="status"]').val(status);
			$('form[name="list"] input[name="csrf_token"]').val(token);
			$('form[name="list"]').attr('action', action).attr('method', 'post').submit();
		}
	});
	
	/* blank */
	$('a.blank').on('click', function(e){
		e.preventDefault();
	});
	
	/* filters */
	$('.box-search').each(function(){
		var obj = $(this);
			obj.height(obj.parent().height());
	});
	
	$('input[name="select-all"]').on('click', function(){
		var table = $(this).parent().parent().parent().parent();
		var box   = table.parent();
		if ($(this).is(':checked')) {
			table.find('input.select').attr('checked', true);
			table.find('tr:gt(0)').addClass('selected');
			box.find('button.bulk').removeClass('disabled');
		} else {
			table.find('input.select').attr('checked', false);
			table.find('tr:gt(0)').removeClass('selected');
			box.find('button.bulk').addClass('disabled');
		}
	});
	$('input.select').on('click', function(){
		var row   = $(this).parent().parent();
		var table = row.parent().parent();
		var box   = table.parent();
		var total = table.find('input.select').length;
		($(this).is(':checked')) ? row.addClass('selected') : row.removeClass('selected');
		var checked = table.find('input.select:checked').length;
		(checked==0) ? box.find('button.bulk').addClass('disabled') : box.find('button.bulk').removeClass('disabled');
		(checked==total) ? table.find('input[name="select-all"]').attr('checked', true) : table.find('input[name="select-all"]').attr('checked', false);
	});
	
	$('button.disabled, a.disabled').on('click', function(e){
		e.preventDefault();
	});
	
	$('#content button.add').on('click', function(){
		self.location.href = $(this).attr('data-action');
	});
	$('form.form button.cancel').on('click', function(){
		var url = self.location.href.split('/');
			url.pop();
			url.push('index');
		self.location.href = url.join('/');
	});
	
	
	
	/* form validation */
	$('form.validate').on('submit', function(){
		var success  = true;
		var form     = $(this);
		// prevent submitting the form twice
		if (form.find('button.disabled').length>0)
			return false;
		// remove error
		form.off('keydown').on('keydown, change', 'input, select', function(){ // ensure that the same handler isn't binded twice
			$(this).removeClass('error');
		});
		// validate non empty elements
		form.find('input.required, select.required, textarea.required').each(function(){
			if ($.trim($(this).val()) == '') {
				alert($(this).attr('title'));
				$(this).focus().addClass('error');
				success = false;
				return false;
			}
		});
		// additional validations to run only if passed non empty validation
		if (success) {
			// validate email
			form.find('input, select, textarea').each(function(){
				/*
				if ($(this).hasClass('email') && $.trim($(this).val())!='' && !$(this).val().isEmail()) {
					alert('Escriba una direcci�n de correo electr�nico v�lida');
					$(this).focus().addClass('error');
					success = false;
					return false;
				}
				*/
			});
		}
		// form didn't pass validations
		if (!success) 
			return false;
		form.find('button[type="submit"]').addClass('disabled');
		return true;
	});
	
	
	// date picker
	$.datepicker.setDefaults($.datepicker.regional['es']);
	$("input.date").datepicker({ dateFormat: "yy-mm-dd", firstDay: 0, changeMonth: true, changeYear: true, yearRange: '1940:2015' });
	
	
	$(document).scroll(function(){
		($('html').offset().top <= -43) ? $('#menu').addClass('sticky') : $('#menu').removeClass('sticky');
	});
	
	$('div.box-header p.filters a').on('click', function(e){
		e.preventDefault();
		$('form[name="list"] input[name="status"]').val($(this).data('status'));
		$('form[name="list"] input[name="page"]').val(1);
		$('form[name="list"]').submit();
	});
	
	$('div.box-table div.pagination a').on('click', function(e){
		e.preventDefault();
		if ( ! $(this).hasClass('disabled')) {
			$('form[name="list"] input[name="page"]').val($(this).data('page'));
			$('form[name="list"]').submit();
		}
	});
	
	if ($('.nested-sortable').length) {
		$('.nested-sortable').nestedSortable({
			disableNesting: 'no-nest',
			forcePlaceholderSize: true,
			handle: 'div',
			helper:	'clone',
			items: 'li',
			maxLevels: 3,
			opacity: .6,
			placeholder: 'placeholder',
			revert: 250,
			tabSize: 25,
			tolerance: 'pointer',
			toleranceElement: '> div'
		});
		$('div.box-table button.serialize').click(function(){
			var serialized = $('.nested-sortable').nestedSortable('serialize');
			var action = $('div.box-table button.sort').data('action');
			var token  = $('form[name="list"]').data('token');
			$('form[name="list"] input[name="serialized"]').val(serialized);
			$('form[name="list"] input[name="csrf_token"]').val(token);
			$('form[name="list"]').attr('action', action).attr('method', 'post').submit();
		})
		
		$('div.box-table button.sort').on('click', function(){
			$(this).addClass('disabled');
			$('div.box-table div.sortable-head, div.box-table .nested-sortable').fadeIn(300);
		});
		$('div.sortable-head button.cancel').on('click', function(){
			$('div.box-table button.sort').removeClass('disabled');
			$('div.box-table div.sortable-head, div.box-table .nested-sortable').fadeOut(300);
		});
	}
	
	if ($('.sortable').length) {
		$('.sortable').sortable({
			handle: '.handle'
		});
	}
	
	/*
	$('a.surveys.add').on('click', function(e){
		e.preventDefault();
		var choices = $(this).parent().parent().find('ul.choices li');
		var next = 1;
		choices.each(function(i){
			var _next = $(this).find('input').attr('name').replace('option_', '');
				_next = parseInt(_next.replace('n_', '')) + 1;
			next = (_next > next) ? _next : next;
		});
		var option = '<li><span class="handle"></span><input type="text" name="option_n_'+next+'" class="required" title="Escriba el texto de la opci�n" /><a href="#" class="delete">Borrar</a></li>';
		$(this).parent().parent().find('ul.choices').append(option);
	});
	*/
	$('#add-event-date').on('click', function(e){
		e.preventDefault();
		var option = '<li><input type="text" name="date[]" value="" class="required date" title="Seleccione la fecha" /><a href="#" class="delete">Borrar</a></li>';
		$('#event-dates').append(option);
	});
	$('#event-dates').delegate('input.date', 'focusin', function(){
		$(this).datepicker({ dateFormat: "yy-mm-dd", firstDay: 0, changeMonth: true, changeYear: true, yearRange: '1940:2015' });
	});
	
	$('ul.choices li a.delete').live('click', function(e){
		e.preventDefault();
		if (window.confirm('�Est� seguro que desea eliminar el elemento seleccionado?')) {
			$(this).parent().remove();
		}
	});

	//add courses material
	$('#add-course-file').on('click', function(e){
		e.preventDefault();
		var counter = $('input[name="file_counter"]');
		var next = parseInt(counter.val()) + 1;
		var html = '<li> \
						<span class="handle" title="Cambiar posici&oacute;n"></span> \
						<div class="field"> \
							<label>Material</label> \
							<input type="text" name="file_title_'+next+'" value="" class="required" title="Escriba el t�tulo del proyecto" /> \
						</div> \
						<div class="field"> \
							<label>Archivo</label> \
							<input type="file" name="file_name_'+next+'" class="required" title="Seleccione el archivo" /> \
						</div> \
						<a href="#" class="delete" title="Borrar">Borrar</a> \
					</li>';
		$('#table-rows').append(html);
		$('#table-rows ul.sortable2').sortable({
			handle: '.handle'
		});
		counter.val(next);
	});
	
	// List docs
	$('#list-docs ul.sortable2').sortable({
		handle: '.handle'
	});
	$('#add-list-row').live('click', function(e){
		e.preventDefault();
		
		var html = '<li> \
						<span class="handle" title="Cambiar posici&oacute;n"></span> \
						<div class="field"> \
							<label>T�tulo</label> \
							<input type="text" name="list_row_title[]" value="" class="required" title="Escriba el t�tulo del archivo" /> \
						    <label>Archivo</label> \
                            <input type="hidden" name="list_file_o_name[]" value="" /> \
                            <input type="file" name="list_file_name[]" class="required" title="Seleccione el archivo" /> \
						</div> \
						<a href="#" class="delete" title="Borrar">Borrar</a> \
					</li>';
		$('#list-docs').append(html);
		
		$('#list-docs ul.sortable2').sortable({
			handle: '.handle'
		});
	});	
	
		
	// Table docs
	$('#table-rows ul.sortable2').sortable({
		handle: '.handle'
	});
	$('#add-table-row').live('click', function(e){
		e.preventDefault();

		var counter = $('input[name="row_counter"]');
		var next = parseInt(counter.val()) + 1;
		
		var html = '<li> \
						<span class="handle" title="Cambiar posici&oacute;n"></span> \
						<div class="field"> \
							<label>T�tulo</label> \
							<input type="hidden" name="row_index_' + parseInt(counter.val()) + '" value="<?=$i?>" /> \
							<input type="text" name="row_title[]" value="" class="required" title="Escriba el t�tulo del reng�n" /> \
							<label>Subt�tulo</label> \
							<input type="text" name="row_subtitle[]" value="" class="required" title="Escriba el subt�tulo del reng�n" /> \
						</div> \
						<a href="#" class="add file-group" title="Agregar grupo de archivos">Agregar grupo</a> \
						<a href="#" class="delete" title="Borrar">Borrar</a> \
						<ul class="sortable2"><input type="hidden" name="group_counter_' + parseInt(counter.val()) + '" value="0" /></ul> \
					</li>';
		$('#table-rows').append(html);
		
		$('#table_rows ul.sortable2').sortable({
			handle: '.handle'
		});
		counter.val(next);
	});
	$('#table-rows li a.file-group').live('click', function(e){
		e.preventDefault();		
				
		var elem = $(this).parent();		
		var item = elem.find('input').attr('name').replace('row_index_','');
		
		var counter = $('input[name="group_counter_' + item + '"]');
		var next = parseInt(counter.val()) + 1;
		
		var list = elem.find('ul.sortable2').first();
		var html = '<li> \
						<span class="handle" title="Cambiar posici&oacute;n"></span> \
						<div class="field"> \
							<label>Grupo</label> \
							<input type="hidden" name="row_index2_' + parseInt(counter.val()) + '" value="0" /> \
							<input type="text" name="group_title_'+item+'[]" value="" class="required" title="Escriba el nombre del grupo" /> \
						</div> \
						<a href="#" class="add file-in-group" title="Agregar archivos">Agregar archivos</a> \
						<a href="#" class="delete" title="Borrar">Borrar</a> \
						<ul class="sortable2 group_files" style="margin-left: -25px;"></ul> \
					</li>';
		list.append(html);
		
		counter.val(next);
	});
	
	$('#table-rows li a.file-in-group').live('click', function(e){
		e.preventDefault();
		
		var counter = $('input[name="file_counter"]');
		var next = parseInt(counter.val()) + 1;
				
		var elem = $(this).parent();
		var item = elem.parent().parent().find('input').attr('name').replace('row_index_','');
		var subitem = elem.find('input').attr('name').replace('row_index2_','');
		console.log($(this).parent());
		var list = elem.find('.group_files');
		var html = '<li> \
						<span class="handle" title="Cambiar posici&oacute;n"></span> \
						<div class="field"> \
							<label>T�tulo</label> \
							<input type="text" name="file_title_'+item+'_'+subitem+'[]" value="" class="required" title="Escriba el t�tulo del archivo" /> \
						</div> \
						<div class="field">  \
							<label>Archivo</label>  \
							<input type="hidden" name="file_o_'+item+'_'+subitem+'[]" value="" />  \
							<input type="file" name="file_name_'+item+'_'+subitem+'[]" /> <a href="" target="_blank"></a> \
						</div> \
						<a href="#" class="delete" title="Borrar">Borrar</a> \
					</li>';
		list.append(html);
		
		counter.val(next);
	});
	
	// Questions-answers
	$('#challenge-questions ul.sortable2').sortable({
		handle: '.handle'
	});
	$('#add-challenge-question').on('click', function(e){
		e.preventDefault();
		var counter = $('input[name="question_counter"]');
		var next = parseInt(counter.val()) + 1;
		var html = '<li> \
						<span class="handle" title="Cambiar posici&oacute;n"></span> \
						<div class="field"> \
							<label>Pregunta</label> \
							<input type="text" name="question_title_'+next+'" value="" class="required" title="Escriba la pregunta" /> \
						</div> \
						<a href="#" class="add challenge-answer" title="Agregar respuesta">Agregar respuesta</a> \
						<a href="#" class="delete" title="Borrar">Borrar</a> \
						<ul class="sortable2"></ul> \
					</li>';
		$('#challenge-questions').append(html);
		$('#challenge-questions ul.sortable2').sortable({
			handle: '.handle'
		});
		counter.val(next);
	});
	$('#challenge-questions li a.challenge-answer').live('click', function(e){
		e.preventDefault();
		var elem = $(this).parent();
		var item = elem.find('input').attr('name').replace('question_title_','');
		var list = elem.find('ul.sortable2');
		var next = elem.find('ul.sortable2 li').length + 1;
		var html = '<li> \
						<span class="handle" title="Cambiar posici&oacute;n"></span> \
						<div class="field"> \
							<label>Respuesta</label> \
							<input type="text" name="answer_title_'+item+'[]" value="" class="required" title="Escriba la respuesta" /> \
							<input type="radio" name="answer_correct_'+item+'" value="'+next+'" /> \
						</div> \
						<a href="#" class="delete" title="Borrar">Borrar</a> \
					</li>';
		list.append(html);
	});
	
	// Tools
	$('#add-challenge-tool').on('click', function(e){
		e.preventDefault();
		var counter = $('input[name="tool_counter"]');
		var next = parseInt(counter.val()) + 1;
		var html = '<li> \
						<span class="handle" title="Cambiar posici&oacute;n"></span> \
						<div class="field"> \
							<label>Herramienta</label> \
							<input type="text" name="tool_name_'+next+'" value="" class="required" title="Escriba el t�tulo de la herramienta" /> \
						</div> \
						<div class="field"> \
							<label>URL</label> \
							<input type="text" name="tool_url_'+next+'" value="" class="required" title="Escriba la direcci�n de la herramienta" /> \
						</div> \
						<a href="#" class="delete" title="Borrar">Borrar</a> \
						<ul class="sortable2"></ul> \
					</li>';
		$('#challenge-tools').append(html);
		counter.val(next);
	});
	
	
	if ($('textarea.richtext').length) {
		$('textarea.richtext').ckeditor({
			toolbar:
			[
				[ 'Bold','Italic','Underline','Strike','Subscript','Superscript' ],
				[ 'HorizontalRule','-','Link','Unlink','-','Image', 'Table', 'Youtube' ], 
				[ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ],
				[ 'TextColor','BGColor' ], 
				['Source']
			],
			language: 'es',
			filebrowserBrowseUrl : '/assets/filemanager/index.html',
			width: 650
		});
	}
	
	//small rich text
	if ($('textarea.richtext_small').length) {
		$('textarea.richtext_small').ckeditor({
			toolbar:
			[
				[ 'Bold','Italic','Underline','Strike','Subscript','Superscript' ],
				[ 'HorizontalRule','-','Link','Unlink','-','Image', 'Table' ], 
				[ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ],
				[ 'TextColor','BGColor' ], 
				['Source']
			],
			language: 'es',
			filebrowserBrowseUrl : '/assets/filemanager/index.html',
			width: 558
		});
	}
	
	//covert to richtext
	if ($('textarea.richtext_small2').length) {
		$('textarea.richtext_small2').ckeditor({
			toolbar:
			[
				[ 'Bold','Italic','Underline','Strike','Subscript','Superscript' ],
				[ 'HorizontalRule','-','Link','Unlink','-','Image', 'Table' ], 
				[ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ],
				[ 'TextColor','BGColor' ], 
				['Source']
			],
			language: 'es',
			filebrowserBrowseUrl : '/assets/filemanager/index.html',
			width: 438
		});
	}		
	
	/*
	$('textarea.richtext').tinymce({
			// Location of TinyMCE script
			script_url : '/assets/scripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "simple",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)
			//content_css : "css/content.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	*/
	$("input.uploadify_pdf").uploadify({
		'width'           : 101, 
		'height'          : 27,
		'buttonImage'     : '/assets/images/backend/browse.png',
		'fileTypeDesc'    : 'PDF',
        'fileTypeExts'    : '*.pdf; *.doc; *.docx; *.gif; *.jpg; *.jpeg; *.png', 
		'fileSizeLimit'   : '50MB',
		'auto'            : true,
		'multi'           : false,
		'removeCompleted' : true,
		'swf'             : '/assets/uploadify/uploadify.swf',
		'uploader'        : '/assets/uploadify/uploadify.php',
		'onUploadSuccess' : function(file, data, response) {
			var original = $('#'+this.original.attr('id'));
			original.parent().find('input.file').val(file.name);
        }
    });
	$("input.uploadify").uploadify({
		'width'           : 101, 
		'height'          : 27,
		'buttonImage'     : '/assets/images/backend/browse.png',
		'fileTypeDesc'    : 'Image Files',
        'fileTypeExts'    : '*.gif; *.jpg; *.jpeg; *.png', 
		'fileSizeLimit'   : '3MB',
		'auto'            : true,
		'multi'           : false,
		'removeCompleted' : true,
		'swf'             : '/assets/uploadify/uploadify.swf',
		'uploader'        : '/assets/uploadify/uploadify.php',
		'onUploadSuccess' : function(file, data, response) {
			var original = $('#'+this.original.attr('id'));
			original.parent().find('input.file').val(file.name);
			original.parent().find('div.preview img').attr('src', '/assets/files/tmp/'+file.name);
        }
    });
	
	$("input.uploadify_mp3").uploadify({
		'width'           : 101, 
		'height'          : 27,
		'buttonImage'     : '/assets/images/backend/browse.png',
		'fileTypeDesc'    : 'Mp3',
        'fileTypeExts'    : '*.mp3', 
		'fileSizeLimit'   : '50MB',
		'auto'            : true,
		'multi'           : false,
		'removeCompleted' : true,
		'swf'             : '/assets/uploadify/uploadify.swf',
		'uploader'        : '/assets/uploadify/uploadify.php',
		'onUploadSuccess' : function(file, data, response) {
			var original = $('#'+this.original.attr('id'));
			original.parent().find('input.file').val(file.name);
        }
    });	 
	
	$('div.select p').on('click', function(){
		var list = $(this).parent().find('ul');
		if (list.is(':visible')) { 
			list.hide();
			$('#select-overlay').remove();
		} else {
			list.show();
		}
		$('body').append('<div id="select-overlay"></div>');
	});
	$('div.select ul span.option').on('click', function(){
		var value = $(this).data('value');
		var id = $(this).data('id');
		$('#'+id).hide();
		$('#'+id+' span.option').removeClass('selected');
		$(this).addClass('selected');
		$('#'+id).parent().find('p').text($(this).text());
		$('#'+id).parent().find('input[type="hidden"]').val(value);
		$('#select-overlay').remove();
	});
	$('#select-overlay').live('click', function(){ 
		$(this).remove();
		$('div.select > ul').hide();
	});
	
	$('.dashboardTable ul li a.report').on('click', function(e){
		e.preventDefault();
		$(this).hide();
		$(this).parent().find('form').slideDown();
	});
	$('.dashboardTable fieldset .close').on('click', function(e){
		e.preventDefault();
		var form = $(this).parent().parent();
		form.slideUp(function(){
			form.parent().find('a.report').show();
		});
	});
	
	$('.dashboardTable form button').on('click', function(e){
		e.preventDefault();
		var form = $(this).parent().parent();
		if(form.find('input[name="from"]').val()==''){
			alert('Seleccione la fecha de inicio');
			form.find('input[name="from"]').focus();
			return;
		}
		if(form.find('input[name="to"]').val()==''){
			alert('Seleccione la fecha de terminaci�n');
			form.find('input[name="to"]').focus();
			return;
		}
		var url = '/admin/reports/dashboard/index?report='+form.attr('name')+'&'+form.serialize();
		if ($(this).hasClass('popup')) {
			window.open(url, 'report', 'width=960,height=680,menubar=0,scrollbars=1');
		} else {
			self.location.href = url+'&export=xls';
		}
		//alert($(this).serialize());
	});
});