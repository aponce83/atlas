/**
 * Carga los libros recomendados
 */
function load_recommended() {
  $.ajax({
    url: "ajax/homepage/recommended",
    type: "post",
    data: {},
    dataType: "json",
    success: function (data) {

      if ( data.status == "OK" ) {
        var desktop_html = '';
        var autor = '';
        var editorial = '';
        var img = 'assets/images/generic_book.jpg';
        var books = [];
        //Libros recomendados o novedades
        books = ( data.recommended.readList.books.length > 0 )  ?  data.recommended.readList.books  :  data.recommended.novelties;

        for (var i in books) {
          if( i < 4 ){

            if( data.recommended.readList.books.length > 0 ){

                main_book = data.recommended.readList.books[i];

                if ( main_book.relacionados !== null) {
                  book = main_book.relacionados[0];
                } else {
                  continue;
                }

                recommended = true;

            }else{

                book = data.recommended.novelties[i];
                recommended = false;
                $('#title-recommended').html('NOVEDADES');
            }

            img = ( book.imagen !== null ) ? book.imagen : 'assets/images/generic_book.jpg';
            autor = ( typeof book.autores[1] !== 'undefined' )  ?  book.autores[1]  :  '';
            editorial = ( typeof book.editorial !== 'undefined' )  ?  book.editorial  :  '';
            id = ( recommended ) ? book.id : book.id_libro;

            desktop_html += '<div class="row renowned-book-item">';
            if( recommended ){
              desktop_html += ' <div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding reason-act-book-col">';
              desktop_html += '   Porque leíste <a target="_blank"  href="/libros/'+main_book.id_libro+'">'+main_book.titulo+'</a>';
              desktop_html += ' </div>';
            }
            desktop_html += ' <div class="col col-xs-2 col-sm-3 col-md-3 col-lg-2 no-padding">';
            desktop_html += '   <a target="_blank"  href="/libros/'+id+'"><img src=" '+img+' " alt=" '+book.titulo+' " class="activity-extra-book" /></a>';
            desktop_html += ' </div>';
            desktop_html += ' <div class="col col-xs-10 col-sm-9 col-md-9 col-lg-10 renowned-act-info-col">';
            desktop_html += '   <div class="row renowned-book-act-info">';
            desktop_html += '     <a target="_blank"  href="/libros/'+id+'" class="act-book-title">'+book.titulo+'</a>';
            for( var a in autor ){
              if( a < 1 ){
                desktop_html += '       <span class="act-book-author">'+autor[a].nombre+'</span>';
              }else{
                desktop_html += '       <span>...</span>';
              }
            }
            desktop_html += '   </div>';
            desktop_html += '   <div class="row no-margin">';
            for ( var k = 0 ; k <= 5 ; k++ ){
              extra_right = ( k > 0) ? "act-extra-right" : "";

              if ( k < book.rating ){
                desktop_html += '         <img src="assets/images/detail/estrella_amarilla_sm.svg" class="act-extra-book-star '+ extra_right +' ">';
              }else{
                desktop_html += '         <img src="assets/images/detail/estrella_gris_sm.svg"     class="act-extra-book-star '+ extra_right +' ">';
              }
            }
            desktop_html += '   </div>';
            desktop_html += ' </div>';
            desktop_html += '</div>';
          }
        }

        $('#load-recommended').html(desktop_html);

      } // end data.status
    }
  });
}

/**
 * Carga los libros destacados
 */
function load_highlights() {
  $.ajax({
    url: "ajax/homepage/highlights",
    type: "post",
    data: {with_rating: true},
    dataType: "json",
    success: function (data) {
      if (data.status == "OK") {
        if (data.highlights != null) {
          var desktop_html = '';
          var autor = '';
          var editorial = '';
          var img = 'assets/images/generic_book.jpg';
          for (var i in data.highlights) {
            if( i < 4 ){
              var book = data.highlights[i];

              autor = '';
              editorial = '';
              img = ( book.imagen !== null ) ? book.imagen : 'assets/images/generic_book.jpg';
              if( typeof book.autores[1] !== 'undefined' ){
                autor = book.autores[1];
              }
              if( typeof book.editorial !== 'undefined' ){
                editorial = book.editorial;
              }
              desktop_html += '<div class="row renowned-book-item">';
              desktop_html += ' <div class="col col-xs-2 col-sm-3 col-md-3 col-lg-2 no-padding">';
              desktop_html += '   <a target="_blank"  href="/libros/'+book.id_libro+'"><img src=" '+img+' " alt=" '+book.titulo+' " class="activity-extra-book" /></a>';
              desktop_html += ' </div>';
              desktop_html += ' <div class="col col-xs-10 col-sm-9 col-md-9 col-lg-10 renowned-act-info-col">';
              desktop_html += '   <div class="row renowned-book-act-info">';
              desktop_html += '     <a target="_blank"  href="/libros/'+book.id_libro+'" class="act-book-title">'+book.titulo+'</a>';
                for( var a in autor ){
                  if( a < 1 ){
                    desktop_html += '       <span class="act-book-author">'+autor[a].nombre+'</span>';
                  }else{
                    desktop_html += '       <span>...</span>';
                  }
                }
              desktop_html += '   </div>';
              desktop_html += '   <div class="row no-margin">';
                for ( var k = 0 ; k <= 5 ; k++ ){
                  extra_right = ( k > 0) ? "act-extra-right" : "";

                  if ( k < book.rating ){
                    desktop_html += '         <img src="assets/images/detail/estrella_amarilla_sm.svg" class="act-extra-book-star '+ extra_right +' ">';
                  }else{
                    desktop_html += '         <img src="assets/images/detail/estrella_gris_sm.svg"     class="act-extra-book-star '+ extra_right +' ">';
                  }
                }
              desktop_html += '   </div>';
              desktop_html += ' </div>';
              desktop_html += '</div>';
            }
          }

          $('#load-highlights').html(desktop_html);
        }
      }
    }
  });
}