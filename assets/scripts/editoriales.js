$(document).ready(function(){
  
  //Tabla que muestra los libros de la lista
  var table_editoriales = $('#table_editoriales').DataTable({
    responsive: true,
    //"scrollX": true,
    "lengthMenu": [ [50, 100, 200, -1], [50, 100, 200, "Todos"] ],
    //"lengthMenu": false,
    //"searching": false,
    //"ordering": false,
    //"info": false,
    "order": [[0, "asc"]],
    "columnDefs": [ {
      "targets": [0],
      "orderable": true
    } ],
    "language": {
      "emptyTable":     "No hay datos disponibles en la tabla",
      "info":           " _START_ - _END_ de _TOTAL_ ",
      "infoEmpty":      "Mostrando 0 de 0 de 0 filas",
      "infoFiltered":   "(Filtradas de _MAX_ filas totales)",
      "infoPostFix":    "",
      "thousands":      ",",
      "lengthMenu":     "Mostrar _MENU_ libros",
      "loadingRecords": "Cargando...",
      "processing":     "Procesando...",
      "search":         "Buscar:",
      "zeroRecords":    "No se encontraron registros",
      "paginate": {
          "first":      "Primero",
          "last":       "Último",
          "next":       "Siguiente",
          "previous":   "Anterior"
      },
      "aria": {
          "sortAscending":  ": activate to sort column ascending",
          "sortDescending": ": activate to sort column descending"
      }
    }
  });
  
  $(".dataTables_length").css("display", "none");
  $(".dataTables_filter").css("display", "none");

  $("#info_pag").html( $("#table_editoriales_info").html() );

  //Número de Editoriales a mostrar por página 
  $("#list_show, [name='book-show']").change(function(){
    
    var show = $(this).val();
    table_editoriales.page.len( parseInt(show) ).draw();

    $("#info_pag").html( $("#table_editoriales_info").html() );
  });

  //Búsqueda de Editoriales
  $("#search_editorial").keyup(function( event ){
    table_editoriales.search( this.value ).draw();
    $("#info_pag").html( $("#table_editoriales_info").html() );
  });

  //filtrar editoriales
  $("[id^='abc']").click(function(e){
    e.preventDefault();
    //alert($(this).attr("id"));
    var initial = $(this).attr("id").substring(4);
    //alert(initial);
    $("#table_editoriales tr.row-edit").show();
    table_editoriales.page.len( -1 ).draw();
    $("#table_editoriales tr.row-edit").each(function(){
      //console.log($(this).data("abc")+' : '+initial);
      if( $(this).data("abc") != initial)
        $(this).hide();
    });
  });

});
