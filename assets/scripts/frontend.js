$(document).ready(function(){
	
	

	/* form validation */
	$('form.validate').on('submit', function(){
		var success  = true;
		var form     = $(this);
		// prevent submitting the form twice
		if (form.find('button.disabled').length>0)
			return false;
		// remove error
		form.off('keydown').on('keydown, change', 'input, select', function(){ // ensure that the same handler isn't binded twice
			$(this).removeClass('error');
		});
		// validate non empty elements
		form.find('input.required, select.required, textarea.required').each(function(){
			if ($.trim($(this).val()) == '') {
				alert($(this).attr('title'));
				$(this).focus().addClass('error');
				success = false;
				return false;
			}
		});
		// additional validations to run only if passed non empty validation
		if (success) {
			// validate email
			form.find('input, select, textarea').each(function(){
				
				if ($(this).hasClass('email') && $.trim($(this).val())!='' && !$(this).val().isEmail()) {
					alert('Escriba una dirección de correo electrónico válida');
					$(this).focus().addClass('error');
					success = false;
					return false;
				}
				
			});
		}
		// form didn't pass validations
		if (!success) 
			return false;
		form.find('button[type="submit"]').addClass('disabled');
		alert('Recibimos tu mensaje, nos pondremos en contacto contigo a la brevedad posible. Gracias');
		return true;
	});	

});



String.prototype.isEmail = function(){
	return (this.valueOf().search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1);
}
//
// SCORM 1.2 API Implementation
//
function SCORMapi1_2() {
	//
    // API Methods definition
    //
    var Initialized = false;
    function LMSInitialize (param) {
        errorCode = "0";
        if (param == "") {
            if (!Initialized) {
                Initialized = true;
                errorCode = "0";
                //console.log("LMSInitialize", param, "", errorCode);
                return "true";
            } else {
				errorCode = "101";
            }
        } else {
	           errorCode = "201";
        }
        //console.log("LMSInitialize", param, "", errorCode);
        return "false";
    }

	function LMSFinish (param) {
        errorCode = "0";
        if (param == "") {
            if (Initialized) {
                Initialized = false;
                result = StoreData(cmi,true);
                if (nav.event != '') {
                    if (nav.event == 'continue') {
                        setTimeout('scorm_get_next();',500);
                    } else {
                        setTimeout('scorm_get_prev();',500);
                    }
                } else {
                    setTimeout('scorm_get_next();',500);
                }
                //console.log("LMSFinish", "AJAXResult", result, 0);
                result = ('true' == result) ? 'true' : 'false';
                errorCode = (result == 'true')? '0' : '101';
                //console.log("LMSFinish", "result", result, 0);
                //console.log("LMSFinish", param, "", 0);
                // trigger TOC update
				/*
                var sURL = "<?php echo $CFG->wwwroot; ?>" + "/mod/scorm/prereqs.php?a=<?php echo $scorm->id ?>&scoid=<?php echo $scoid ?>&attempt=<?php echo $attempt ?>&mode=<?php echo $mode ?>&currentorg=<?php echo $currentorg ?>&sesskey=<?php echo sesskey(); ?>";
                YAHOO.util.Connect.asyncRequest('GET', sURL, this.connectPrereqCallback, null);
				*/
                return result;
            } else {
                errorCode = "301";
            }
        } else {
            errorCode = "201";
        }
        //console.log("LMSFinish", param, "", errorCode);
        return "false";
    }
	this.LMSInitialize = LMSInitialize;
    this.LMSFinish = LMSFinish;
}

var API = new SCORMapi1_2();