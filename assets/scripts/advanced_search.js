/*
 *=============================================================================
 *========================== Búsqueda Avanzada ================================
 *=============================================================================
*/
$(document).ready(function(){
  $(".chosen-select").chosen({no_results_text: "Sin resultados"});

  var window_height = window.innerHeight;
  var header_height = $("header").height();
  var footer_height = $("footer").height();

  var secction_height = window_height - header_height - footer_height;

  $(".advance-search-container").css("min-height", secction_height);
  
  $(".filter-remove").click(function(){
    $(this).parent().parent().remove();
  });

  $("#clear_content").click(function(){
    $("#conten").html('');
    
  });
  //$(".filter-remove").parent(".input-filtro").remove();

});